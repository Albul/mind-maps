<map version="freeplane 1.3.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="English" STYLE_REF="main_node" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1426663014779">
<hook NAME="AutomaticEdgeColor" COUNTER="4"/>
<hook NAME="MapStyle" zoom="1.003">
    <properties show_icon_for_attributes="true" show_note_icons="true"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
<stylenode TEXT="main_node" BACKGROUND_COLOR="#ccccff">
<font SIZE="18" BOLD="true"/>
<edge STYLE="sharp_bezier" WIDTH="4"/>
</stylenode>
<stylenode TEXT="second_node" BACKGROUND_COLOR="#00cccc" STYLE="bubble">
<font SIZE="14"/>
<edge WIDTH="4"/>
</stylenode>
<stylenode TEXT="third_node" BACKGROUND_COLOR="#ffbdf4" STYLE="bubble">
<font SIZE="11" BOLD="true"/>
</stylenode>
<stylenode TEXT="orange_node" BACKGROUND_COLOR="#ff9900" STYLE="bubble">
<font BOLD="true"/>
<edge WIDTH="2"/>
</stylenode>
<stylenode TEXT="gray_node" BACKGROUND_COLOR="#9ec19e">
<font BOLD="true"/>
</stylenode>
<stylenode TEXT="yellow_node" BACKGROUND_COLOR="#ffff66" STYLE="bubble">
<edge WIDTH="thin"/>
</stylenode>
<stylenode TEXT="example_node" BACKGROUND_COLOR="#afcdfa">
<edge STYLE="bezier" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="pre_last_node" BACKGROUND_COLOR="#99ff99" STYLE="bubble">
<edge STYLE="bezier" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="last_node" BACKGROUND_COLOR="#ffffff">
<edge STYLE="bezier" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="warning_node" BACKGROUND_COLOR="#fda6a6">
<icon BUILTIN="messagebox_warning"/>
</stylenode>
<stylenode TEXT="pre_last_important_node" BACKGROUND_COLOR="#ff9999" STYLE="bubble">
<font BOLD="true"/>
<edge STYLE="bezier" WIDTH="thin"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook URI="../../Images/english-large.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Phonetics" STYLE_REF="second_node" POSITION="right" ID="ID_1032138227" CREATED="1426659961057" MODIFIED="1426663093912" HGAP="50" VSHIFT="20">
<hook URI="../../Images/phonetics.jpg" SIZE="1.0" NAME="ExternalObject"/>
<edge COLOR="#b40101"/>
<node TEXT="C" STYLE_REF="orange_node" ID="ID_1544834487" CREATED="1426659990546" MODIFIED="1426661063199" VGAP="9">
<node TEXT="[k]" STYLE_REF="yellow_node" ID="ID_1908739774" CREATED="1426659993964" MODIFIED="1426661071728"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Before a, o, u
    </p>
  </body>
</html>
</richcontent>
<node TEXT="a" STYLE_REF="gray_node" ID="ID_1113485618" CREATED="1426660048576" MODIFIED="1426661006011">
<node TEXT="cab" STYLE_REF="pre_last_node" ID="ID_769982142" CREATED="1426660097946" MODIFIED="1426661014427"/>
<node TEXT="cat" STYLE_REF="pre_last_node" ID="ID_671685129" CREATED="1426660101027" MODIFIED="1426661014429"/>
</node>
<node TEXT="o" STYLE_REF="gray_node" ID="ID_178945772" CREATED="1426660105735" MODIFIED="1426661006015">
<node TEXT="coast" STYLE_REF="pre_last_node" ID="ID_1636729371" CREATED="1426660108786" MODIFIED="1426661014430"/>
<node TEXT="cock" STYLE_REF="pre_last_node" ID="ID_1561123972" CREATED="1426660115116" MODIFIED="1426661014431"/>
</node>
<node TEXT="u" STYLE_REF="gray_node" ID="ID_603581090" CREATED="1426660119475" MODIFIED="1426661006016">
<node TEXT="cube" STYLE_REF="pre_last_node" ID="ID_1310178356" CREATED="1426660122167" MODIFIED="1426661014431"/>
<node TEXT="cute" STYLE_REF="pre_last_node" ID="ID_1836640767" CREATED="1426660127736" MODIFIED="1426661014432"/>
</node>
</node>
<node TEXT="[s]" STYLE_REF="yellow_node" ID="ID_1863897615" CREATED="1426659999264" MODIFIED="1429592388185"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Before e, i, y
    </p>
  </body>
</html>
</richcontent>
<edge WIDTH="1"/>
<font BOLD="true" ITALIC="true"/>
<node TEXT="e" STYLE_REF="gray_node" ID="ID_1087085630" CREATED="1426660948798" MODIFIED="1426661006016">
<node TEXT="center" STYLE_REF="pre_last_node" ID="ID_158643305" CREATED="1426660951475" MODIFIED="1426661014433"/>
<node TEXT="cell" STYLE_REF="pre_last_node" ID="ID_1892995311" CREATED="1426660956496" MODIFIED="1426661014434"/>
</node>
<node TEXT="i" STYLE_REF="gray_node" ID="ID_1353485476" CREATED="1426660959808" MODIFIED="1426661006017">
<node TEXT="city" STYLE_REF="pre_last_node" ID="ID_1114880692" CREATED="1426660962540" MODIFIED="1426661014435"/>
<node TEXT="cigar" STYLE_REF="pre_last_node" ID="ID_1822080404" CREATED="1426660966373" MODIFIED="1426661014436"/>
</node>
<node TEXT="y" STYLE_REF="gray_node" ID="ID_1765084848" CREATED="1426660970277" MODIFIED="1426661006018">
<node TEXT="cyber" STYLE_REF="pre_last_node" ID="ID_1676121601" CREATED="1426660975925" MODIFIED="1426661014437"/>
<node TEXT="cynical" STYLE_REF="pre_last_node" ID="ID_233755080" CREATED="1426660980024" MODIFIED="1426661014438"/>
</node>
</node>
</node>
</node>
<node TEXT="Grammar" STYLE_REF="second_node" POSITION="left" ID="ID_1456099269" CREATED="1426663003697" MODIFIED="1426663281669" HGAP="50" VSHIFT="-40">
<edge COLOR="#4a4a4a"/>
<hook URI="../../Images/grammar.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Conditional clauses" STYLE_REF="third_node" ID="ID_1107730713" CREATED="1429613251815" MODIFIED="1429613844539" VGAP="20">
<node TEXT="0. General truth" STYLE_REF="orange_node" ID="ID_10250923" CREATED="1429613267087" MODIFIED="1429613844539"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Condition always fulfills
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Form" STYLE_REF="gray_node" ID="ID_989851958" CREATED="1429613349591" MODIFIED="1429613464612">
<node TEXT="IF + present simple, present simple" STYLE_REF="pre_last_important_node" ID="ID_635957159" CREATED="1429613357959" MODIFIED="1429613926620"/>
<node TEXT="IF + S + V1, S + V1" STYLE_REF="pre_last_node" ID="ID_770888013" CREATED="1429613363815" MODIFIED="1429613891331" BACKGROUND_COLOR="#ff9999">
<font BOLD="true"/>
</node>
</node>
<node TEXT="Examples" STYLE_REF="gray_node" ID="ID_1456644626" CREATED="1429613372854" MODIFIED="1429613464613">
<node TEXT="If you heat water it boils" STYLE_REF="example_node" ID="ID_626042161" CREATED="1429613380825" MODIFIED="1429613840948"/>
<node TEXT="You get orange if you add yellow to red" STYLE_REF="example_node" ID="ID_1189963889" CREATED="1429613380825" MODIFIED="1429613840947"/>
</node>
</node>
<node TEXT="1. Real present conditions" STYLE_REF="orange_node" ID="ID_1796888704" CREATED="1429613398422" MODIFIED="1446706131188"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Condition possible to fulfill
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Form" STYLE_REF="gray_node" ID="ID_1749372699" CREATED="1429613413479" MODIFIED="1429613464611">
<node TEXT="IF + present simple, future simple" STYLE_REF="pre_last_important_node" ID="ID_1120172119" CREATED="1429613419283" MODIFIED="1429613926621"/>
<node TEXT="S + will V1 + IF + S + V1" STYLE_REF="pre_last_important_node" ID="ID_1569785727" CREATED="1429613419283" MODIFIED="1429613926621"/>
</node>
<node TEXT="Use" STYLE_REF="gray_node" ID="ID_571740593" CREATED="1429613716143" MODIFIED="1446706131187">
<node TEXT="Conditional Sentences Type I refer to the future. An action in the future will only happen if a certain condition is fulfilled by that time. We don&apos;t know for sure whether the condition actually will be fulfilled or not, but the conditions seems rather realistic &#x2013; so we think it is likely to happen" STYLE_REF="pre_last_node" ID="ID_774830596" CREATED="1429613730959" MODIFIED="1429613815644"/>
</node>
<node TEXT="Examples" STYLE_REF="gray_node" ID="ID_1714701569" CREATED="1429613723270" MODIFIED="1446706129448">
<node TEXT="If I meet the boss, I will talk to him" STYLE_REF="example_node" ID="ID_533129752" CREATED="1429613757482" MODIFIED="1429613823868"/>
<node TEXT="If I get drunk, I won&apos;t be able to drive" STYLE_REF="example_node" ID="ID_933683140" CREATED="1429613757482" MODIFIED="1429613823870"/>
<node TEXT="If they go to the disco, they will listen to loud music" STYLE_REF="example_node" ID="ID_1870733494" CREATED="1429613757482" MODIFIED="1429613823870"/>
<node TEXT="If you wait a minute, I will ask my parents" STYLE_REF="example_node" ID="ID_1144931521" CREATED="1429613757483" MODIFIED="1429613823871"/>
<node TEXT="If we travel to London, we will visit the museums" STYLE_REF="example_node" ID="ID_1996335909" CREATED="1429613757483" MODIFIED="1429613823872"/>
<node TEXT="If my friends come , I will be very happy" STYLE_REF="example_node" ID="ID_1637044058" CREATED="1429613757484" MODIFIED="1429613823873"/>
<node TEXT="If the sun shines , we will walk to the town" STYLE_REF="example_node" ID="ID_608016113" CREATED="1429613757484" MODIFIED="1429613823873"/>
<node TEXT="If he has a temperature, he will see the doctor" STYLE_REF="example_node" ID="ID_70120582" CREATED="1429613757484" MODIFIED="1429613823874"/>
<node TEXT="If I don&apos;t make breakfast tomorrow morning, my girlfriend won&apos;t love me anymore" STYLE_REF="example_node" ID="ID_99563264" CREATED="1429613757485" MODIFIED="1429613823875"/>
<node TEXT="If you eat too much junk food, you won&apos;t lose weight" STYLE_REF="example_node" ID="ID_1575149884" CREATED="1429613757485" MODIFIED="1429613823876"/>
<node TEXT="If Tom doesn&apos;t tidy up his room, Victoria won&apos;t help him with the muffins" STYLE_REF="example_node" ID="ID_1430228103" CREATED="1429613757487" MODIFIED="1446706129446"/>
<node TEXT="If Dick doesn&apos;t buy the book, his friends will be angry with him" STYLE_REF="example_node" ID="ID_702381435" CREATED="1429613757487" MODIFIED="1429613823877"/>
<node TEXT="If we take the bus, we won&apos;t arrive in time" STYLE_REF="example_node" ID="ID_247827188" CREATED="1429613757488" MODIFIED="1429613823878"/>
<node TEXT="If she doesn&apos;t read the novel, she won&apos;t pass the literature test" STYLE_REF="example_node" ID="ID_414962985" CREATED="1429613757488" MODIFIED="1429613823879"/>
<node TEXT="If it rains , the children will not go for a walk" STYLE_REF="example_node" ID="ID_1909180446" CREATED="1429613757489" MODIFIED="1429613823880"/>
<node TEXT="I will lend you my laptop if you promise to be careful with it" STYLE_REF="example_node" ID="ID_129732261" CREATED="1429613757495" MODIFIED="1429613823881"/>
<node TEXT="If they aren&apos;t careful with the spelling of the new words,&#xa;they won&apos;t get a good mark" STYLE_REF="example_node" ID="ID_1282308042" CREATED="1429613757495" MODIFIED="1429613934299"/>
<node TEXT="If Phil does that, Sophia will scream" STYLE_REF="example_node" ID="ID_1732055140" CREATED="1429613757496" MODIFIED="1429613823884"/>
<node TEXT="The girls should hurry if they haven&apos;t booked the tickets yet" STYLE_REF="example_node" ID="ID_1301450376" CREATED="1429613757497" MODIFIED="1429613823884"/>
<node TEXT="Lisa must look after her baby brother if her father goes jogging" STYLE_REF="example_node" ID="ID_748369302" CREATED="1429613757500" MODIFIED="1429613823884"/>
<node TEXT="If I listen to classical music, I can write good poems" STYLE_REF="example_node" ID="ID_1386603793" CREATED="1429613757500" MODIFIED="1429613823885"/>
</node>
</node>
<node TEXT="2. Unreal present conditions" STYLE_REF="orange_node" ID="ID_1061202997" CREATED="1429614027133" MODIFIED="1429614068676"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Condition in theory possible to fulfill
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Form" STYLE_REF="gray_node" ID="ID_1397042488" CREATED="1429614048902" MODIFIED="1429614074172">
<node TEXT="IF + past simple, would + infinitive" STYLE_REF="pre_last_important_node" ID="ID_718536176" CREATED="1429614147561" MODIFIED="1429614296709"/>
<node TEXT="S + would V1 + IF + S + V2" STYLE_REF="pre_last_important_node" ID="ID_613461203" CREATED="1429614147561" MODIFIED="1429614296708"/>
</node>
<node TEXT="Use" STYLE_REF="gray_node" ID="ID_1347838186" CREATED="1429614054703" MODIFIED="1429614074172">
<node TEXT="Conditional Sentences Type II refer to situations in the present. An action could happen if the present situation were different. I don&apos;t really expect the situation to change, however. I just imagine &#x201e;what would happen if &#x2026;&#x201c;" STYLE_REF="pre_last_node" ID="ID_183658274" CREATED="1429614154943" MODIFIED="1429614290157"/>
</node>
<node TEXT="Examples" STYLE_REF="gray_node" ID="ID_1178921019" CREATED="1429614060871" MODIFIED="1429622690921">
<node TEXT="If I were you, I would buy these shoes" STYLE_REF="example_node" ID="ID_601071064" CREATED="1429614174969" MODIFIED="1429614233060"/>
<node TEXT="My brother would buy a sports car if he had the money" STYLE_REF="example_node" ID="ID_914633467" CREATED="1429614174969" MODIFIED="1429614233061"/>
<node TEXT="If I felt better, I would go to the cinema with you" STYLE_REF="example_node" ID="ID_1584348198" CREATED="1429614174970" MODIFIED="1429614233063"/>
<node TEXT="If you went by bike more often, you wouldn&apos;t be so flabby" STYLE_REF="example_node" ID="ID_1640597169" CREATED="1429614174970" MODIFIED="1429614233065"/>
<node TEXT="She wouldn&apos;t talk to you if she were mad at you" STYLE_REF="example_node" ID="ID_880669149" CREATED="1429614174971" MODIFIED="1429614233066"/>
<node TEXT="If he had more time, he would learn karate" STYLE_REF="example_node" ID="ID_1619537516" CREATED="1429614174971" MODIFIED="1429614233068"/>
<node TEXT="If I played the lottery, I would have a chance to hit the jackpot" STYLE_REF="example_node" ID="ID_162293574" CREATED="1429614174972" MODIFIED="1429614233069"/>
<node TEXT="If I hit the jackpot, I would be rich" STYLE_REF="example_node" ID="ID_121623371" CREATED="1429614174972" MODIFIED="1429614233071"/>
<node TEXT="If I were rich, my life would change completely" STYLE_REF="example_node" ID="ID_1499635141" CREATED="1429614174972" MODIFIED="1429614233073"/>
<node TEXT="I would buy a lonely island if I found a nice one" STYLE_REF="example_node" ID="ID_154556898" CREATED="1429614174973" MODIFIED="1429614233075"/>
<node TEXT="If I owned a lonely island, I would build a huge house by the beach" STYLE_REF="example_node" ID="ID_1324669553" CREATED="1429614174973" MODIFIED="1429614233077"/>
<node TEXT="I would invite all my friends if I had a house by the beach" STYLE_REF="example_node" ID="ID_357118012" CREATED="1429614174974" MODIFIED="1429614233079"/>
<node TEXT="I would pick my friends up in my yacht if they&#xa;wanted to spend their holidays on my island" STYLE_REF="example_node" ID="ID_1320546887" CREATED="1429614174974" MODIFIED="1429622690920"/>
<node TEXT="We would have great parties if my friends came to my island" STYLE_REF="example_node" ID="ID_1040544215" CREATED="1429614174974" MODIFIED="1429614233084"/>
<node TEXT="If we liked to go shopping in a big city, we would charter a helicopter" STYLE_REF="example_node" ID="ID_667619096" CREATED="1429614174976" MODIFIED="1429614233085"/>
<node TEXT="But if my friends&apos; holidays were over, I would feel very lonely on my lonely island" STYLE_REF="example_node" ID="ID_1423205692" CREATED="1429614174976" MODIFIED="1429614233085"/>
<node TEXT="If you did a paper round, you would earn a little extra money" STYLE_REF="example_node" ID="ID_1121858487" CREATED="1429614174977" MODIFIED="1429614233086"/>
<node TEXT="If she didn&apos;t bully her classmates, she would have more friends" STYLE_REF="example_node" ID="ID_1691981786" CREATED="1429614174978" MODIFIED="1429614233087"/>
<node TEXT="If you dropped this bottle, it wouldn&apos;t break" STYLE_REF="example_node" ID="ID_23283780" CREATED="1429614174978" MODIFIED="1429614233087"/>
<node TEXT="If it weren&apos;t so late, we would play a game of chess" STYLE_REF="example_node" ID="ID_1901891872" CREATED="1429614174979" MODIFIED="1429614233088"/>
<node TEXT="If we didn&apos;t visit this museum, you wouldn&apos;t write a good report" STYLE_REF="example_node" ID="ID_631801172" CREATED="1429614174979" MODIFIED="1429614233089"/>
<node TEXT="If we recycled these cans, we could reduce rubbish" STYLE_REF="example_node" ID="ID_985560090" CREATED="1429614174979" MODIFIED="1429614233090"/>
<node TEXT="Would you like a sports car if you had the money?" STYLE_REF="example_node" ID="ID_1149418673" CREATED="1429614174980" MODIFIED="1429614233091"/>
<node TEXT="Denise wouldn&apos;t need a dictionary if she learned the new words" STYLE_REF="example_node" ID="ID_1939251598" CREATED="1429614174980" MODIFIED="1429614233059"/>
</node>
</node>
<node TEXT="3. Unreal past conditions" STYLE_REF="orange_node" ID="ID_1536800044" CREATED="1429614304581" MODIFIED="1429614472231"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Condition not possible to fulfill (too late)
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Form" STYLE_REF="gray_node" ID="ID_1368636192" CREATED="1429614331286" MODIFIED="1429614481995">
<node TEXT="IF + past perfect, S + would + present perfect" STYLE_REF="pre_last_important_node" ID="ID_1898126790" CREATED="1429614365776" MODIFIED="1429614493820"/>
<node TEXT="S + would have V3 + IF + S + had V3" STYLE_REF="pre_last_important_node" ID="ID_1126560679" CREATED="1429614365776" MODIFIED="1429614493821"/>
</node>
<node TEXT="Use" STYLE_REF="gray_node" ID="ID_523011059" CREATED="1429614338607" MODIFIED="1429614482075">
<node TEXT="Conditional Sentences Type III refer to situations in the past. An action could have happened in the past if a certain condition had been fulfilled. Things were different then, however. We just imagine, what would have happened if the situation had been fulfilled" STYLE_REF="pre_last_node" ID="ID_1728237606" CREATED="1429614357839" MODIFIED="1429614487435"/>
</node>
<node TEXT="Examples" STYLE_REF="gray_node" ID="ID_215837062" CREATED="1429614345374" MODIFIED="1429614573556">
<node TEXT="If i hadn&apos;t spent all money, I wouldn&apos;t have gone to the bank" STYLE_REF="example_node" ID="ID_1574644367" CREATED="1429614514521" MODIFIED="1429614572404"/>
<node TEXT="If the police had come earlier, they would have arrested the burglar" STYLE_REF="example_node" ID="ID_703268121" CREATED="1429614514521" MODIFIED="1429614572407"/>
<node TEXT="If you had bought fresh green vegetable, your salad would have tasted better" STYLE_REF="example_node" ID="ID_273270710" CREATED="1429614514533" MODIFIED="1429614572410"/>
<node TEXT="If he had spoken more slowly, Peggy would have understood him" STYLE_REF="example_node" ID="ID_1577763758" CREATED="1429614514533" MODIFIED="1429614572414"/>
<node TEXT="If the teacher had explained the homework, I would have done it" STYLE_REF="example_node" ID="ID_237853512" CREATED="1429614514534" MODIFIED="1429614572417"/>
<node TEXT="If you had checked the car, it would not have&#xa;broken down in the middle of the desert" STYLE_REF="example_node" ID="ID_689411750" CREATED="1429614514534" MODIFIED="1429614572424"/>
<node TEXT="If my parents had not been so tired, they would have watched the film on TV" STYLE_REF="example_node" ID="ID_504808014" CREATED="1429614514535" MODIFIED="1429614572427"/>
<node TEXT="If Tom had eaten more salad, he would not have caught a cold" STYLE_REF="example_node" ID="ID_1624350607" CREATED="1429614514536" MODIFIED="1429614572429"/>
<node TEXT="If Fred hadn&apos;t cheated at the test, his teacher wouldn&apos;t have phoned his father" STYLE_REF="example_node" ID="ID_176223969" CREATED="1429614514537" MODIFIED="1429614572430"/>
<node TEXT="If I hadn&apos;t switched off the radio, I would have known about the second goal" STYLE_REF="example_node" ID="ID_1882251187" CREATED="1429614514537" MODIFIED="1429614573555"/>
<node TEXT="If it hadn&apos;t rained , the children would have played outside" STYLE_REF="example_node" ID="ID_951615203" CREATED="1429614514538" MODIFIED="1429614572433"/>
<node TEXT="They might have arrived on time if they hadn&apos;t missed the train" STYLE_REF="example_node" ID="ID_1495310193" CREATED="1429614514538" MODIFIED="1429614572434"/>
<node TEXT="If the men had recharged the batteries, they&#xa;would have had enough power for their laptop" STYLE_REF="example_node" ID="ID_574554763" CREATED="1429614514539" MODIFIED="1429614572436"/>
<node TEXT="Your friends wouldn&apos;t have laughed about you if you &apos;d combed your hair" STYLE_REF="example_node" ID="ID_528788727" CREATED="1429614514539" MODIFIED="1429614572437"/>
<node TEXT="The dog would not have attacked you if you hadn&apos;t teased him" STYLE_REF="example_node" ID="ID_990474061" CREATED="1429614514540" MODIFIED="1429614572438"/>
<node TEXT="If we had closed the window yesterday, it would not have been so cold in here" STYLE_REF="example_node" ID="ID_1587333820" CREATED="1429614514541" MODIFIED="1429614572439"/>
<node TEXT="If you &apos;d turned down the volume of your stereo,&#xa;the neighbors would not have complained" STYLE_REF="example_node" ID="ID_497226499" CREATED="1429614514541" MODIFIED="1429614572440"/>
<node TEXT="He &apos;d have caught the plane if he &apos;d hurried" STYLE_REF="example_node" ID="ID_609118958" CREATED="1429614514542" MODIFIED="1429614572404"/>
</node>
</node>
</node>
<node TEXT="Comparatives / superlatives" ID="ID_788637154" CREATED="1429615000893" MODIFIED="1429615005130">
<node TEXT="One syllable" ID="ID_1102775771" CREATED="1429615016045" MODIFIED="1429615016578"/>
</node>
<node TEXT="Must / have to / should" ID="ID_628016737" CREATED="1429615050642" MODIFIED="1429615056395"/>
<node TEXT="Questions" ID="ID_616538230" CREATED="1429615182481" MODIFIED="1446706116884">
<node TEXT="Question tags" ID="ID_794869602" CREATED="1429615195350" MODIFIED="1446706116883"/>
</node>
<node TEXT="Prepositions" STYLE_REF="third_node" ID="ID_1145548094" CREATED="1447221883177" MODIFIED="1447222176943">
<node TEXT="Time" STYLE_REF="orange_node" ID="ID_1471442418" CREATED="1447221889552" MODIFIED="1447222186509">
<node TEXT="at" STYLE_REF="gray_node" ID="ID_276676944" CREATED="1447221894048" MODIFIED="1447222192909">
<node TEXT="12:30 ," STYLE_REF="pre_last_node" ID="ID_687418840" CREATED="1447221896152" MODIFIED="1447222205798"/>
<node TEXT="5 pm" STYLE_REF="pre_last_node" ID="ID_1856381155" CREATED="1447222029502" MODIFIED="1447222205805"/>
<node TEXT="noon" STYLE_REF="pre_last_node" ID="ID_259193436" CREATED="1447222035448" MODIFIED="1447222205809"/>
<node TEXT="midnight" STYLE_REF="pre_last_node" ID="ID_1801329534" CREATED="1447222038127" MODIFIED="1447222205810"/>
</node>
<node TEXT="on" STYLE_REF="gray_node" ID="ID_910830117" CREATED="1447221899872" MODIFIED="1447222192910">
<node TEXT="Saturday" STYLE_REF="pre_last_node" ID="ID_1817112867" CREATED="1447221906104" MODIFIED="1447222205811"/>
<node TEXT="New Year&apos;s day" STYLE_REF="pre_last_node" ID="ID_1534145751" CREATED="1447221922048" MODIFIED="1447222205812"/>
<node TEXT="December 5th" STYLE_REF="pre_last_node" ID="ID_1915740033" CREATED="1447221934008" MODIFIED="1447222205813"/>
</node>
<node TEXT="in" STYLE_REF="gray_node" ID="ID_785978982" CREATED="1447221975825" MODIFIED="1447222192911">
<node TEXT="September" STYLE_REF="pre_last_node" ID="ID_998686977" CREATED="1447221980520" MODIFIED="1447222205814"/>
<node TEXT="the summer" STYLE_REF="pre_last_node" ID="ID_1143271871" CREATED="1447221984720" MODIFIED="1447222205818"/>
<node TEXT="1995" OBJECT="java.lang.Long|1995" STYLE_REF="pre_last_node" ID="ID_1986442270" CREATED="1447221998871" MODIFIED="1447222205823"/>
<node TEXT="the 1960s" STYLE_REF="pre_last_node" ID="ID_240934392" CREATED="1447222007655" MODIFIED="1447222205824"/>
<node TEXT="the Middle Ages" STYLE_REF="pre_last_node" ID="ID_1453276954" CREATED="1447222018009" MODIFIED="1447222205825"/>
</node>
</node>
</node>
</node>
<node TEXT="Vocabulary" STYLE_REF="second_node" POSITION="right" ID="ID_246243167" CREATED="1426663102248" MODIFIED="1426663874688" VGAP="43">
<edge COLOR="#730773"/>
<hook URI="../../Images/vocabulary.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Idioms" STYLE_REF="third_node" ID="ID_899696201" CREATED="1426663360880" MODIFIED="1426663874686">
<font SIZE="12" BOLD="false"/>
<hook URI="../../Images/idiom.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Products" STYLE_REF="orange_node" ID="ID_511371031" CREATED="1427357071684" MODIFIED="1427357095920">
<node TEXT="Pie" STYLE_REF="gray_node" ID="ID_867931460" CREATED="1427357096908" MODIFIED="1427357128194">
<node TEXT="It&apos;s as easy as pie" STYLE_REF="yellow_node" ID="ID_698494316" CREATED="1427357129724" MODIFIED="1427357163144">
<font BOLD="true" ITALIC="true"/>
<node TEXT="If something is very easy" STYLE_REF="pre_last_node" ID="ID_692683513" CREATED="1427357164940" MODIFIED="1427357262472"/>
</node>
<node TEXT="Pie-eyed" STYLE_REF="yellow_node" ID="ID_208866038" CREATED="1427357140260" MODIFIED="1427357163143">
<font BOLD="true" ITALIC="true"/>
<node TEXT="If someone is very drunk" STYLE_REF="pre_last_node" ID="ID_976839073" CREATED="1427357181956" MODIFIED="1427357262473"/>
<node TEXT="He&apos;s pie-eyed" STYLE_REF="example_node" ID="ID_775101663" CREATED="1427357194603" MODIFIED="1427357266793"/>
</node>
<node TEXT="To have a finger in many pies" STYLE_REF="yellow_node" ID="ID_1161379231" CREATED="1427357147795" MODIFIED="1427357163142">
<font BOLD="true" ITALIC="true"/>
<node TEXT="If someone is involved in lots of different activities" STYLE_REF="pre_last_node" ID="ID_1002691426" CREATED="1427357201093" MODIFIED="1427357262473"/>
<node TEXT="I&apos;ve got my fingers in many pies" STYLE_REF="example_node" ID="ID_1429352532" CREATED="1427357236723" MODIFIED="1427357266792"/>
</node>
</node>
<node TEXT="Egg" STYLE_REF="gray_node" ID="ID_609389406" CREATED="1427357102284" MODIFIED="1427357128194">
<node TEXT="He&apos;s a bad egg" STYLE_REF="yellow_node" ID="ID_918712910" CREATED="1427357276676" MODIFIED="1427357313871">
<font BOLD="true" ITALIC="true"/>
<node TEXT="If we want ot say that someone is bad or dishonest" STYLE_REF="pre_last_node" ID="ID_1945418285" CREATED="1427357319508" MODIFIED="1427357413977"/>
</node>
<node TEXT="Don&apos;t put all your eggs in one basket" STYLE_REF="yellow_node" ID="ID_369305161" CREATED="1427357284163" MODIFIED="1427357313870">
<font BOLD="true" ITALIC="true"/>
<node TEXT="It means you should never rely on only one plan - in case it goes wrong" STYLE_REF="pre_last_node" ID="ID_992353374" CREATED="1427357339692" MODIFIED="1427357413976"/>
</node>
<node TEXT="He got egg on his face" STYLE_REF="yellow_node" ID="ID_211991844" CREATED="1427357297868" MODIFIED="1427357313870">
<font BOLD="true" ITALIC="true"/>
<node TEXT="If someone makes a mistake which makes him look stupid" STYLE_REF="pre_last_node" ID="ID_1554566989" CREATED="1427357380477" MODIFIED="1427357413977"/>
</node>
</node>
<node TEXT="Tea" STYLE_REF="gray_node" ID="ID_96703090" CREATED="1427357112996" MODIFIED="1427357128194">
<node TEXT="It&apos;s not my cup of tea" STYLE_REF="yellow_node" ID="ID_1246005256" CREATED="1427357435325" MODIFIED="1427357479143">
<font BOLD="true" ITALIC="true"/>
<node TEXT="If there&apos;s something we don&apos;t like very much for&#xa;example a sport or a type of music" STYLE_REF="pre_last_node" ID="ID_806908721" CREATED="1427357481195" MODIFIED="1427357561936"/>
</node>
<node TEXT="I wouldn&apos;t do it for all the tea in China" STYLE_REF="yellow_node" ID="ID_1337871885" CREATED="1427357444684" MODIFIED="1427357479143">
<font BOLD="true" ITALIC="true"/>
<node TEXT="If there is something we would never do" STYLE_REF="pre_last_node" ID="ID_447418167" CREATED="1427357515267" MODIFIED="1427357558047"/>
</node>
<node TEXT="It&apos;s as good as a chocolate teapot" STYLE_REF="yellow_node" ID="ID_1263958548" CREATED="1427357461044" MODIFIED="1427357479142">
<font BOLD="true" ITALIC="true"/>
<node TEXT="If something is completely useless" STYLE_REF="pre_last_node" ID="ID_1252691303" CREATED="1427357531979" MODIFIED="1427357558047"/>
</node>
</node>
<node TEXT="Bean" STYLE_REF="gray_node" ID="ID_1161301604" CREATED="1427357117261" MODIFIED="1427357128192">
<node TEXT="To be full of beans" STYLE_REF="yellow_node" ID="ID_1252157747" CREATED="1427357575444" MODIFIED="1427357610344">
<font BOLD="true" ITALIC="true"/>
<node TEXT="If someone is full of energy (coffee :)" STYLE_REF="pre_last_node" ID="ID_1603729608" CREATED="1427357660292" MODIFIED="1427357779650"/>
<node TEXT="He is full of beans" STYLE_REF="example_node" ID="ID_1838570191" CREATED="1427357674028" MODIFIED="1427357773729"/>
</node>
<node TEXT="I haven&apos;t got a bean" STYLE_REF="yellow_node" ID="ID_1821138512" CREATED="1427357587501" MODIFIED="1427357610344">
<font BOLD="true" ITALIC="true"/>
<node TEXT="If you have no maney at all" STYLE_REF="pre_last_node" ID="ID_659565765" CREATED="1427357696708" MODIFIED="1427357779649"/>
</node>
<node TEXT="Spill the beans" STYLE_REF="yellow_node" ID="ID_1546421642" CREATED="1427357597195" MODIFIED="1427357610343">
<font BOLD="true" ITALIC="true"/>
<node TEXT="If we want someone to tell us something secret" STYLE_REF="pre_last_node" ID="ID_268621316" CREATED="1427357718788" MODIFIED="1427357779648"/>
<node TEXT="Come on, spill the beans" STYLE_REF="example_node" ID="ID_464947205" CREATED="1427357756366" MODIFIED="1427357773728"/>
</node>
</node>
</node>
<node TEXT="Fruits" STYLE_REF="orange_node" ID="ID_1194274870" CREATED="1426663688527" MODIFIED="1426663694969">
<node TEXT="To go bananas" STYLE_REF="yellow_node" ID="ID_1930501101" CREATED="1426663721845" MODIFIED="1426663898594">
<font BOLD="true" ITALIC="true"/>
<node TEXT="If someone is very emotional and starts shouting and behaving in crazy way" STYLE_REF="pre_last_node" ID="ID_1683428092" CREATED="1426663760527" MODIFIED="1426663820681">
<icon BUILTIN="idea"/>
</node>
<node TEXT="He&apos;s gone bananas" STYLE_REF="example_node" ID="ID_32237794" CREATED="1426663792114" MODIFIED="1426664060227"/>
<node TEXT="I went bananas" STYLE_REF="example_node" ID="ID_1911118030" CREATED="1426663806756" MODIFIED="1426664060229"/>
</node>
<node TEXT="It&apos;s a case of sour grapes" STYLE_REF="yellow_node" ID="ID_1281371047" CREATED="1426663730777" MODIFIED="1426663898644">
<font BOLD="true" ITALIC="true"/>
<node TEXT="If somebody pretends not to be impressed by something because they are jealous" STYLE_REF="pre_last_node" ID="ID_110088463" CREATED="1426663923147" MODIFIED="1426663988800">
<icon BUILTIN="idea"/>
</node>
</node>
<node TEXT="A second bite of the cherry" STYLE_REF="yellow_node" ID="ID_377682890" CREATED="1426663741849" MODIFIED="1426663898644">
<font BOLD="true" ITALIC="true"/>
<node TEXT="If we try to do something a second time because we faild at first" STYLE_REF="pre_last_node" ID="ID_1861572625" CREATED="1426663997215" MODIFIED="1426664050425">
<icon BUILTIN="idea"/>
</node>
<node TEXT="Please, give me a second bite of the cherry" STYLE_REF="example_node" ID="ID_39107586" CREATED="1426664029965" MODIFIED="1426664054508"/>
</node>
</node>
<node TEXT="Vegetables" STYLE_REF="orange_node" ID="ID_946455413" CREATED="1426663698368" MODIFIED="1427049354706">
<node TEXT="It&apos;s a hot potato" STYLE_REF="yellow_node" ID="ID_1876987285" CREATED="1426664078440" MODIFIED="1426664114747">
<font BOLD="true" ITALIC="true"/>
<node TEXT="If there is a delicate issue and people can&apos;t agree" STYLE_REF="pre_last_node" ID="ID_1649339601" CREATED="1426667769187" MODIFIED="1426667830194">
<icon BUILTIN="idea"/>
</node>
<node TEXT="Like hot topic" STYLE_REF="pre_last_node" ID="ID_1221744695" CREATED="1426667812507" MODIFIED="1426667830196">
<icon BUILTIN="idea"/>
</node>
</node>
<node TEXT="A carrot and stick" STYLE_REF="yellow_node" ID="ID_615822702" CREATED="1426664087890" MODIFIED="1427049354706">
<font BOLD="true" ITALIC="true"/>
<node TEXT="If someone tries to presuade us to do something using an incentive and a threat" STYLE_REF="pre_last_node" ID="ID_1267622994" CREATED="1426667851614" MODIFIED="1426667977189"/>
<node TEXT="It&apos;s a carrot and stick" STYLE_REF="example_node" ID="ID_303507237" CREATED="1426667996087" MODIFIED="1426668009909"/>
</node>
<node TEXT="Like two peas in a pod" STYLE_REF="yellow_node" ID="ID_605660017" CREATED="1426664095984" MODIFIED="1426664114745">
<font BOLD="true" ITALIC="true"/>
<node TEXT="If two people look very similar" STYLE_REF="pre_last_node" ID="ID_1520309157" CREATED="1426667928255" MODIFIED="1426667977188"/>
<node TEXT="They&apos;re like two peas in a pod" STYLE_REF="example_node" ID="ID_595076425" CREATED="1426667956506" MODIFIED="1426667973507"/>
</node>
</node>
<node TEXT="Animals" STYLE_REF="orange_node" ID="ID_945543480" CREATED="1426668042055" MODIFIED="1426668060300">
<node TEXT="Horse" STYLE_REF="gray_node" ID="ID_451437452" CREATED="1426668072251" MODIFIED="1426668222814">
<node TEXT="To eat like a horse" STYLE_REF="yellow_node" ID="ID_30445338" CREATED="1426668177539" MODIFIED="1426668228755">
<font BOLD="true" ITALIC="true"/>
<node TEXT="If someone eats a lot" STYLE_REF="pre_last_node" ID="ID_252807858" CREATED="1426668231082" MODIFIED="1426668258069"/>
<node TEXT="He eats like a horse" STYLE_REF="example_node" ID="ID_374560456" CREATED="1426668246266" MODIFIED="1426668263326"/>
</node>
<node TEXT="Straight from the horse&apos;s mouth" STYLE_REF="yellow_node" ID="ID_1664941946" CREATED="1426668188319" MODIFIED="1426668228757">
<font BOLD="true" ITALIC="true"/>
<node TEXT="When we&apos;ve got some information directly from the person responsible for it" STYLE_REF="pre_last_node" ID="ID_1042248433" CREATED="1426668265216" MODIFIED="1426668372182"/>
<node TEXT="My boss is going to fire Maks. Yes, it&apos;s true. I heard it straight from the horse&apos;s mouth" STYLE_REF="example_node" ID="ID_628370265" CREATED="1426668306921" MODIFIED="1426668387956"/>
</node>
<node TEXT="To flog a dead horse" STYLE_REF="yellow_node" ID="ID_1826856158" CREATED="1426668203758" MODIFIED="1426668228758">
<font BOLD="true" ITALIC="true"/>
<node TEXT="To waste effort on something that we have no chance of succeding at" STYLE_REF="pre_last_node" ID="ID_757573946" CREATED="1426668389483" MODIFIED="1426668489510"/>
<node TEXT="I sometimes feel like I&apos;m flogging a dead horse when I&apos;m teaching" STYLE_REF="example_node" ID="ID_1837915762" CREATED="1426668452844" MODIFIED="1426668479910"/>
</node>
</node>
<node TEXT="Dog" STYLE_REF="gray_node" ID="ID_888804189" CREATED="1426668075917" MODIFIED="1426668222816">
<node TEXT="To be in the dog house" STYLE_REF="yellow_node" ID="ID_647498844" CREATED="1427358093348" MODIFIED="1427358127031">
<font BOLD="true" ITALIC="true"/>
<node TEXT="If someoneis unpopular because he has done something wrong" STYLE_REF="pre_last_node" ID="ID_1696198597" CREATED="1427358129228" MODIFIED="1427358325834"/>
<node TEXT="He is in the dog house" STYLE_REF="example_node" ID="ID_833487397" CREATED="1427358167195" MODIFIED="1427358311561"/>
</node>
<node TEXT="The hair of the dog" STYLE_REF="yellow_node" ID="ID_68126361" CREATED="1427358101716" MODIFIED="1427358127031">
<font BOLD="true" ITALIC="true"/>
<node TEXT="A little bit of alcohol that someone needs to feel better after drinkig too much" STYLE_REF="pre_last_node" ID="ID_1491167831" CREATED="1427358184764" MODIFIED="1427358325834"/>
<node TEXT="You should have the hair of the dog" STYLE_REF="example_node" ID="ID_1801558578" CREATED="1427358250155" MODIFIED="1427358311561"/>
</node>
<node TEXT="To make a dog&apos;s dinner of something" STYLE_REF="yellow_node" ID="ID_1446948764" CREATED="1427358108005" MODIFIED="1427358127030">
<font BOLD="true" ITALIC="true"/>
<node TEXT="He has made a mess of something" STYLE_REF="pre_last_node" ID="ID_590143937" CREATED="1427358291053" MODIFIED="1427358325832"/>
<node TEXT="He has made a dog&apos;s dinner of that" STYLE_REF="example_node" ID="ID_794462467" CREATED="1427358264470" MODIFIED="1427358311560"/>
</node>
</node>
<node TEXT="Cat" STYLE_REF="gray_node" ID="ID_105362814" CREATED="1426668531613" MODIFIED="1426668538374">
<node TEXT="There isn&apos;t enough room to swing a cat" STYLE_REF="yellow_node" ID="ID_625164386" CREATED="1427356513418" MODIFIED="1427356743439">
<font BOLD="true" ITALIC="true"/>
<node TEXT="If a room or a space is very small" STYLE_REF="pre_last_node" ID="ID_664242779" CREATED="1427356595212" MODIFIED="1427356735075"/>
</node>
<node TEXT="To let the cat out of the bag" STYLE_REF="yellow_node" ID="ID_1599010089" CREATED="1427356528420" MODIFIED="1427356743438">
<font BOLD="true" ITALIC="true"/>
<node TEXT="If someone lets a secret be known, especially by mistake" STYLE_REF="pre_last_node" ID="ID_617204521" CREATED="1427356619202" MODIFIED="1427356735074"/>
<node TEXT="They let the cat out of the bag" STYLE_REF="example_node" ID="ID_1791874657" CREATED="1427356656253" MODIFIED="1427356756696"/>
</node>
<node TEXT="To put a cat among the pigeons" STYLE_REF="yellow_node" ID="ID_1811997326" CREATED="1427356544108" MODIFIED="1427356743440">
<font BOLD="true" ITALIC="true"/>
<node TEXT="If there&apos;s something which causes trouble or makes people very angry" STYLE_REF="pre_last_node" ID="ID_1670456553" CREATED="1427356669276" MODIFIED="1427356735073"/>
<node TEXT="It put a cat among the pigeons" STYLE_REF="example_node" ID="ID_30808430" CREATED="1427356708477" MODIFIED="1427356727186"/>
</node>
</node>
<node TEXT="Pig" STYLE_REF="gray_node" ID="ID_1648805693" CREATED="1427040946461" MODIFIED="1427041657957">
<node TEXT="Pigs might fly" STYLE_REF="yellow_node" ID="ID_957634050" CREATED="1427040959103" MODIFIED="1427041665660">
<font BOLD="true" ITALIC="true"/>
<node TEXT="When someone says something that we think will never happen" STYLE_REF="pre_last_node" ID="ID_1958223097" CREATED="1427040972190" MODIFIED="1427041672646"/>
</node>
<node TEXT="This place is a pigsty" STYLE_REF="yellow_node" ID="ID_270354126" CREATED="1427041488261" MODIFIED="1427041665660">
<font BOLD="true" ITALIC="true"/>
<node TEXT="When some place is very dirty" STYLE_REF="pre_last_node" ID="ID_1232113977" CREATED="1427041514126" MODIFIED="1427041672646"/>
</node>
<node TEXT="To make a pig&apos;s ear of something" STYLE_REF="yellow_node" ID="ID_153436983" CREATED="1427041554164" MODIFIED="1427041665660">
<font BOLD="true" ITALIC="true"/>
<node TEXT="If someone does something very badly we can say" STYLE_REF="pre_last_node" ID="ID_331768086" CREATED="1427041598380" MODIFIED="1427041672646"/>
<node TEXT="He made a pig&apos;s ear of it" STYLE_REF="example_node" ID="ID_165948351" CREATED="1427041646452" MODIFIED="1427041678480"/>
</node>
</node>
<node TEXT="Bird" STYLE_REF="gray_node" ID="ID_352404113" CREATED="1427356811285" MODIFIED="1427356849287">
<node TEXT="Birdbrain" STYLE_REF="yellow_node" ID="ID_1619593080" CREATED="1427356820268" MODIFIED="1427356857086">
<font BOLD="true" ITALIC="true"/>
<node TEXT="We call someone isn&apos;t very bright" STYLE_REF="pre_last_node" ID="ID_1217348836" CREATED="1427356861462" MODIFIED="1427357036796"/>
<node TEXT="He is birdbrain" STYLE_REF="example_node" ID="ID_715296408" CREATED="1427356893860" MODIFIED="1427357041681"/>
</node>
<node TEXT="To have a bird&apos;s eye view" STYLE_REF="yellow_node" ID="ID_381627766" CREATED="1427356825212" MODIFIED="1427356857088">
<font BOLD="true" ITALIC="true"/>
<node TEXT="To be able to see very clearly from a high place" STYLE_REF="pre_last_node" ID="ID_1740844551" CREATED="1427356902133" MODIFIED="1427357036795"/>
</node>
<node TEXT="A little bird told me" STYLE_REF="yellow_node" ID="ID_1272714857" CREATED="1427356835804" MODIFIED="1427356857088">
<font BOLD="true" ITALIC="true"/>
<node TEXT="When I want to give you some information but I doesn&apos;t want you to know who told me" STYLE_REF="pre_last_node" ID="ID_566021112" CREATED="1427356971588" MODIFIED="1427357036792"/>
</node>
</node>
<node TEXT="Bee" STYLE_REF="gray_node" ID="ID_1931733001" CREATED="1427357805635" MODIFIED="1427358089897">
<node TEXT="Busy as a bee" STYLE_REF="yellow_node" ID="ID_776330550" CREATED="1427357820861" MODIFIED="1427357861134">
<font BOLD="true" ITALIC="true"/>
<node TEXT="If someone is very busy or is moving around quickly doing lots of things" STYLE_REF="pre_last_node" ID="ID_729643868" CREATED="1427357862740" MODIFIED="1427358045188"/>
<node TEXT="They&apos;re busy as a bee" STYLE_REF="example_node" ID="ID_1525940280" CREATED="1427357900976" MODIFIED="1427358039162"/>
</node>
<node TEXT="The bee&apos;s knees" STYLE_REF="yellow_node" ID="ID_139825211" CREATED="1427357829284" MODIFIED="1427358089897">
<font BOLD="true" ITALIC="true"/>
<node TEXT="If we think that something es excellent, or of the very highest quality" STYLE_REF="pre_last_node" ID="ID_1233515999" CREATED="1427357912140" MODIFIED="1427358045188"/>
<node TEXT="It&apos;s the bee&apos;s knees" STYLE_REF="example_node" ID="ID_544213206" CREATED="1427357943579" MODIFIED="1427358039162"/>
</node>
<node TEXT="To have a bee in your bonnet" STYLE_REF="yellow_node" ID="ID_154178080" CREATED="1427357841564" MODIFIED="1427357861135">
<font BOLD="true" ITALIC="true"/>
<node TEXT="If someone is very worried or concerned about something and they talk about it all the time" STYLE_REF="pre_last_node" ID="ID_1384162829" CREATED="1427357952396" MODIFIED="1427358045184"/>
<node TEXT="They&apos;ve got a bee in their bonnet" STYLE_REF="example_node" ID="ID_1858264262" CREATED="1427358016628" MODIFIED="1427358039160"/>
</node>
</node>
<node TEXT="Monkey" STYLE_REF="gray_node" ID="ID_584548106" CREATED="1427358370852" MODIFIED="1427358423633">
<node TEXT="To monkey around" STYLE_REF="yellow_node" ID="ID_1752465873" CREATED="1427358432165" MODIFIED="1427358460319">
<font BOLD="true" ITALIC="true"/>
<node TEXT="To behave in a sily or careless way" STYLE_REF="pre_last_node" ID="ID_1744463271" CREATED="1427358621413" MODIFIED="1427358647223"/>
</node>
<node TEXT="Monkey business" STYLE_REF="yellow_node" ID="ID_1845053418" CREATED="1427358377870" MODIFIED="1427358460320">
<font BOLD="true" ITALIC="true"/>
<node TEXT="We can call bad or dishonest behaviour" STYLE_REF="pre_last_node" ID="ID_1833020518" CREATED="1427358494940" MODIFIED="1427358586594"/>
<node TEXT="Enough of this monkey business" STYLE_REF="example_node" ID="ID_1444025506" CREATED="1427358518451" MODIFIED="1427358581409"/>
</node>
<node TEXT="To make a monkey out of someone" STYLE_REF="yellow_node" ID="ID_1571617045" CREATED="1427358440339" MODIFIED="1427358460320">
<font BOLD="true" ITALIC="true"/>
<node TEXT="If we are trying to make someone look stupid" STYLE_REF="pre_last_node" ID="ID_56902158" CREATED="1427358546028" MODIFIED="1427358586593"/>
<node TEXT="We are making a monkey out of them" STYLE_REF="example_node" ID="ID_1320794160" CREATED="1427358563221" MODIFIED="1427358581408"/>
</node>
</node>
<node TEXT="Insect" STYLE_REF="gray_node" ID="ID_1974331697" CREATED="1427358401733" MODIFIED="1427358423633">
<node TEXT="Knee-high to a gresshopper" STYLE_REF="yellow_node" ID="ID_57993835" CREATED="1427358881659" MODIFIED="1427358933064">
<font BOLD="true" ITALIC="true"/>
<node TEXT="When we want to talk about someone who isvery youngorvery small" STYLE_REF="pre_last_node" ID="ID_641479824" CREATED="1427358948532" MODIFIED="1427359176899"/>
<node TEXT="They are knee-high to a grasshopper" STYLE_REF="example_node" ID="ID_333276485" CREATED="1427358970725" MODIFIED="1427359171377"/>
</node>
<node TEXT="To have butterflies in your stomach" STYLE_REF="yellow_node" ID="ID_581816746" CREATED="1427358900189" MODIFIED="1427359040626">
<font BOLD="true" ITALIC="true"/>
<node TEXT="If we are very nervous about something we have to do" STYLE_REF="pre_last_node" ID="ID_1301219692" CREATED="1427359034755" MODIFIED="1427359176898"/>
<node TEXT="I have butterflies in my stomach" STYLE_REF="example_node" ID="ID_187558029" CREATED="1427359082976" MODIFIED="1427359171377"/>
</node>
<node TEXT="To have ants in your pants" STYLE_REF="yellow_node" ID="ID_1961009955" CREATED="1427358913476" MODIFIED="1427358933063">
<font BOLD="true" ITALIC="true"/>
<node TEXT="If you are so excited about something that you can&apos;t keep still" STYLE_REF="pre_last_node" ID="ID_265087675" CREATED="1427359098212" MODIFIED="1427359176897"/>
<node TEXT="You have got ants in your pants" STYLE_REF="example_node" ID="ID_1989919223" CREATED="1427359152213" MODIFIED="1427359171376"/>
</node>
</node>
<node TEXT="Fish" STYLE_REF="gray_node" ID="ID_1357498388" CREATED="1427358408397" MODIFIED="1427358423632">
<node TEXT="There&apos;s something fishy about this" STYLE_REF="yellow_node" ID="ID_567521761" CREATED="1427358661244" MODIFIED="1427358701944">
<font BOLD="true" ITALIC="true"/>
<node TEXT="We can say that a strange or suspicious situation is fishy" STYLE_REF="pre_last_node" ID="ID_1262171680" CREATED="1427358704523" MODIFIED="1427358838430"/>
</node>
<node TEXT="A big fish in a small pund" STYLE_REF="yellow_node" ID="ID_1234560885" CREATED="1427358676492" MODIFIED="1427358701944">
<font BOLD="true" ITALIC="true"/>
<node TEXT="We can say that someone who&apos;s important or&#xa;powerful - but only in a small organisation" STYLE_REF="pre_last_node" ID="ID_1519282940" CREATED="1427358735997" MODIFIED="1427358842114"/>
</node>
<node TEXT="A fish out of water" STYLE_REF="yellow_node" ID="ID_530523830" CREATED="1427358687439" MODIFIED="1427358701943">
<font BOLD="true" ITALIC="true"/>
<node TEXT="If someone is very unhappy and uncomfortable&#xa;because he&apos;s in an unfamiliar place" STYLE_REF="pre_last_node" ID="ID_1874497092" CREATED="1427358782844" MODIFIED="1427358845360"/>
<node TEXT="I feel like fish outta water" STYLE_REF="example_node" ID="ID_360405250" CREATED="1427359235298" MODIFIED="1427359800713"/>
</node>
<node TEXT="Fine kettle of fish" STYLE_REF="yellow_node" ID="ID_168697501" CREATED="1427359208974" MODIFIED="1427359777038">
<font BOLD="true" ITALIC="true"/>
<node TEXT="Good situation" STYLE_REF="pre_last_node" ID="ID_1616084235" CREATED="1427359211502" MODIFIED="1427359794364"/>
</node>
<node TEXT="Fresh off the boat" STYLE_REF="yellow_node" ID="ID_1491730203" CREATED="1427359257690" MODIFIED="1427359777040">
<font BOLD="true" ITALIC="true"/>
<node TEXT="It is new emmigrant" STYLE_REF="pre_last_node" ID="ID_171347530" CREATED="1427359266179" MODIFIED="1427359794364"/>
</node>
<node TEXT="Like a dead fish" STYLE_REF="yellow_node" ID="ID_1824050227" CREATED="1427359298875" MODIFIED="1427359777041">
<font BOLD="true" ITALIC="true"/>
<node TEXT="She didn&apos;t participate in action :)" STYLE_REF="pre_last_node" ID="ID_1051555749" CREATED="1427359323124" MODIFIED="1427359794364"/>
<node TEXT="She was like a dead fish" STYLE_REF="example_node" ID="ID_1426436071" CREATED="1427359313324" MODIFIED="1427359800713"/>
</node>
<node TEXT="Whale of a time" STYLE_REF="yellow_node" ID="ID_688790395" CREATED="1427359340285" MODIFIED="1427359777041">
<font BOLD="true" ITALIC="true"/>
<node TEXT="To describe something very large and impressive" STYLE_REF="pre_last_node" ID="ID_1357472251" CREATED="1427359388796" MODIFIED="1427359794364"/>
<node TEXT="We had a whale of a time in Puerto Rico" STYLE_REF="example_node" ID="ID_1855996241" CREATED="1427359399685" MODIFIED="1427359800714"/>
</node>
<node TEXT="Big fish" STYLE_REF="yellow_node" ID="ID_1553008549" CREATED="1427359415686" MODIFIED="1427359777041">
<font BOLD="true" ITALIC="true"/>
<node TEXT="The boss" STYLE_REF="pre_last_node" ID="ID_679467357" CREATED="1427359433941" MODIFIED="1427359794363"/>
</node>
<node TEXT="The world is your oyster" STYLE_REF="yellow_node" ID="ID_1019926833" CREATED="1427359459154" MODIFIED="1427359777041">
<font BOLD="true" ITALIC="true"/>
<node TEXT="Anything you wish is yours for the taking" STYLE_REF="pre_last_node" ID="ID_1468031766" CREATED="1427359496156" MODIFIED="1427359794362"/>
<node TEXT="This piece of advice, usually given to youngsters, suggests that their future holds great riches, the way an oyster contains a pearl" STYLE_REF="pre_last_node" ID="ID_1542939071" CREATED="1427359504437" MODIFIED="1427359794361"/>
</node>
<node TEXT="Fish taco" STYLE_REF="yellow_node" ID="ID_1043817045" CREATED="1427359612060" MODIFIED="1427359777041">
<font BOLD="true" ITALIC="true"/>
<node TEXT="A term for a nasty, smelly pussy" STYLE_REF="pre_last_node" ID="ID_32631500" CREATED="1427359618282" MODIFIED="1427359794360"/>
</node>
</node>
<node TEXT="Busy as a beaver" STYLE_REF="yellow_node" ID="ID_1690488995" CREATED="1427047892817" MODIFIED="1427048516176">
<font BOLD="true" ITALIC="true"/>
</node>
<node TEXT="Counting sheep" STYLE_REF="yellow_node" ID="ID_1297743779" CREATED="1427047906440" MODIFIED="1427048516176">
<font BOLD="true" ITALIC="true"/>
<node TEXT="To sleep" STYLE_REF="pre_last_node" ID="ID_1417813674" CREATED="1427047919257" MODIFIED="1427048524222"/>
<node TEXT="He is counting sheep" STYLE_REF="example_node" ID="ID_388446106" CREATED="1427047925672" MODIFIED="1427048529113"/>
</node>
<node TEXT="Crocodile tears" STYLE_REF="yellow_node" ID="ID_239204974" CREATED="1427047939672" MODIFIED="1427048516176">
<font BOLD="true" ITALIC="true"/>
<node TEXT="Tears or expressions of sorrow that are insincere" STYLE_REF="pre_last_node" ID="ID_167676634" CREATED="1427048328312" MODIFIED="1427048524207"/>
</node>
<node TEXT="Sly as a fox" STYLE_REF="yellow_node" ID="ID_1427067131" CREATED="1427047952201" MODIFIED="1427048516176">
<font BOLD="true" ITALIC="true"/>
</node>
</node>
<node TEXT="Girls" STYLE_REF="orange_node" ID="ID_451127559" CREATED="1427048591911" MODIFIED="1427048811380">
<node TEXT="The girl next door" STYLE_REF="yellow_node" ID="ID_851272273" CREATED="1427048630391" MODIFIED="1427048819177">
<font BOLD="true" ITALIC="true"/>
<node TEXT="Used to describe someone who is completely ordinary, not rich, famous, etc" STYLE_REF="pre_last_node" ID="ID_1003066757" CREATED="1427048753225" MODIFIED="1427049225291"/>
</node>
<node TEXT="Till the fat lady sings" STYLE_REF="yellow_node" ID="ID_1015275964" CREATED="1427048604287" MODIFIED="1427048819177">
<font BOLD="true" ITALIC="true"/>
<node TEXT="Till the end" STYLE_REF="pre_last_node" ID="ID_1922273129" CREATED="1427048768016" MODIFIED="1427048781437"/>
<node TEXT="I stay till the fat lady sings" STYLE_REF="example_node" ID="ID_1923997845" CREATED="1427048618039" MODIFIED="1427048792615"/>
</node>
<node TEXT="Let your hair down" STYLE_REF="yellow_node" ID="ID_911531354" CREATED="1427048644536" MODIFIED="1427048944762">
<font BOLD="true" ITALIC="true"/>
<node TEXT="The party gives you a chance to let your hair down at the end of the week" STYLE_REF="example_node" ID="ID_250719543" CREATED="1427048902048" MODIFIED="1427048963235"/>
<node TEXT="When she took off her glasses and let her hair down, she was incredibly beautiful" STYLE_REF="pre_last_node" ID="ID_1228184178" CREATED="1427049163281" MODIFIED="1427049177986"/>
<node TEXT="To relax and do what you want" STYLE_REF="pre_last_node" ID="ID_1050903955" CREATED="1427048914552" MODIFIED="1427048960000"/>
</node>
<node TEXT="Knocked up" STYLE_REF="yellow_node" ID="ID_844063586" CREATED="1427048679024" MODIFIED="1427048819177">
<font BOLD="true" ITALIC="true"/>
<node TEXT="Pregnant" STYLE_REF="pre_last_node" ID="ID_1857604964" CREATED="1427048827911" MODIFIED="1427048848014"/>
<node TEXT="She is knocked up" STYLE_REF="example_node" ID="ID_1716091568" CREATED="1427048680601" MODIFIED="1427048799615"/>
</node>
<node TEXT="ButterHead" STYLE_REF="yellow_node" ID="ID_882867547" CREATED="1427048695929" MODIFIED="1427048819177">
<font BOLD="true" ITALIC="true"/>
<node TEXT="Beautiful body but ugly face" STYLE_REF="pre_last_node" ID="ID_1179702709" CREATED="1427048703327" MODIFIED="1427048781437"/>
<node TEXT="She is ButterHead" STYLE_REF="example_node" ID="ID_1413539235" CREATED="1427048697584" MODIFIED="1427048799615"/>
</node>
</node>
<node TEXT="Business" STYLE_REF="orange_node" ID="ID_1993304910" CREATED="1429590377574" MODIFIED="1429590917444">
<node TEXT="In the red" STYLE_REF="yellow_node" ID="ID_611683160" CREATED="1429590399679" MODIFIED="1429590926763">
<font BOLD="true" ITALIC="true"/>
<node TEXT="Bad things" STYLE_REF="pre_last_node" ID="ID_1176179466" CREATED="1429590461231" MODIFIED="1429590946987"/>
<node TEXT="Our company in the red" STYLE_REF="example_node" ID="ID_1103460702" CREATED="1429590431136" MODIFIED="1429590937621"/>
</node>
<node TEXT="In the black" STYLE_REF="yellow_node" ID="ID_1046956317" CREATED="1429590414583" MODIFIED="1429590926763">
<font BOLD="true" ITALIC="true"/>
<node TEXT="Good things" STYLE_REF="pre_last_node" ID="ID_858042841" CREATED="1429590461231" MODIFIED="1429590946988"/>
<node TEXT="Our company in the black" STYLE_REF="example_node" ID="ID_52925986" CREATED="1429590439055" MODIFIED="1429590937621"/>
</node>
<node TEXT="Ballpark figure" STYLE_REF="yellow_node" ID="ID_1647685441" CREATED="1429590538439" MODIFIED="1429590926764">
<font BOLD="true" ITALIC="true"/>
<node TEXT="About" STYLE_REF="pre_last_node" ID="ID_1520899925" CREATED="1429590853959" MODIFIED="1429590946988"/>
<node TEXT="Oleksandr, ball-park figure, how many people live in Kiev?" STYLE_REF="example_node" ID="ID_1920957176" CREATED="1429590561022" MODIFIED="1429590937620"/>
</node>
<node TEXT="Go over the books" STYLE_REF="yellow_node" ID="ID_632040595" CREATED="1429590551030" MODIFIED="1429590926764">
<font BOLD="true" ITALIC="true"/>
<node TEXT="Exact data" STYLE_REF="pre_last_node" ID="ID_1796781859" CREATED="1429590882208" MODIFIED="1429590946989"/>
<node TEXT="Oleksandr, go over the books, how many rich people live in Ukraine?" STYLE_REF="example_node" ID="ID_1035244014" CREATED="1429590569999" MODIFIED="1429590937620"/>
</node>
<node TEXT="Off the books" STYLE_REF="yellow_node" ID="ID_244438898" CREATED="1429590741911" MODIFIED="1429590926764">
<font BOLD="true" ITALIC="true"/>
<node TEXT="Illegally, salary in an envelope" STYLE_REF="pre_last_node" ID="ID_1116867531" CREATED="1429590803072" MODIFIED="1429590946989"/>
<node TEXT="I&apos;m working off the books" STYLE_REF="example_node" ID="ID_1123564485" CREATED="1429590754873" MODIFIED="1429590937620"/>
</node>
<node TEXT="On the books" STYLE_REF="yellow_node" ID="ID_652424623" CREATED="1429590716476" MODIFIED="1429590926765">
<font BOLD="true" ITALIC="true"/>
<node TEXT="All the employees are working on the books, in our company" STYLE_REF="example_node" ID="ID_422160406" CREATED="1429590766255" MODIFIED="1429590937619"/>
</node>
</node>
<node TEXT="Color" STYLE_REF="orange_node" ID="ID_1952842191" CREATED="1429590970022" MODIFIED="1429591224172">
<node TEXT="See through rose-tinted glasses" STYLE_REF="yellow_node" ID="ID_853107038" CREATED="1429590978183" MODIFIED="1429591327091">
<font BOLD="true" ITALIC="true"/>
<node TEXT="You see the world through rose-tinted glasses" STYLE_REF="example_node" ID="ID_1833567331" CREATED="1429590985246" MODIFIED="1429591235924"/>
</node>
<node TEXT="Give the green light" STYLE_REF="yellow_node" ID="ID_479348499" CREATED="1429590994174" MODIFIED="1429591327092">
<font BOLD="true" ITALIC="true"/>
<node TEXT="The government give the army the green light to attack" STYLE_REF="example_node" ID="ID_1384565475" CREATED="1429591001102" MODIFIED="1429591235924"/>
</node>
<node TEXT="With flying colors" STYLE_REF="yellow_node" ID="ID_1214505084" CREATED="1429591011256" MODIFIED="1429591327092">
<font BOLD="true" ITALIC="true"/>
<node TEXT="Very successfully" STYLE_REF="pre_last_node" ID="ID_1841263378" CREATED="1429591072046" MODIFIED="1429591242957"/>
<node TEXT="He has passed his interview with flying colors." STYLE_REF="example_node" ID="ID_1732335141" CREATED="1429591017935" MODIFIED="1429591235925"/>
</node>
<node TEXT="Tickeld pink" STYLE_REF="yellow_node" ID="ID_1132478034" CREATED="1429591027449" MODIFIED="1429591327092">
<font BOLD="true" ITALIC="true"/>
<node TEXT="Very happy" STYLE_REF="pre_last_node" ID="ID_1658234486" CREATED="1429591050390" MODIFIED="1429591242956"/>
<node TEXT="My father was tickeld pink to find out that my wife is pregnant" STYLE_REF="example_node" ID="ID_440953045" CREATED="1429591033926" MODIFIED="1429591235925"/>
</node>
<node TEXT="Paint the town red" STYLE_REF="yellow_node" ID="ID_31687199" CREATED="1429591091912" MODIFIED="1429591327092">
<font BOLD="true" ITALIC="true"/>
<node TEXT="Hang out" STYLE_REF="pre_last_node" ID="ID_681771133" CREATED="1429591193408" MODIFIED="1429591242956"/>
<node TEXT="I&apos;m going to go paint the town red" STYLE_REF="example_node" ID="ID_816004721" CREATED="1429591097799" MODIFIED="1429591235925"/>
</node>
<node TEXT="Blue/white-collared worker" STYLE_REF="yellow_node" ID="ID_1444064949" CREATED="1429591110103" MODIFIED="1429591327092">
<font BOLD="true" ITALIC="true"/>
<node TEXT="The blue-collar employes worked long hours with hevy machinery" STYLE_REF="example_node" ID="ID_507694863" CREATED="1429591128375" MODIFIED="1429591235926"/>
</node>
<node TEXT="See things in black and white" STYLE_REF="yellow_node" ID="ID_1547615770" CREATED="1429591118872" MODIFIED="1429591327093">
<font BOLD="true" ITALIC="true"/>
<node TEXT="You see things in black and white" STYLE_REF="example_node" ID="ID_1728581358" CREATED="1429591140344" MODIFIED="1429591235926"/>
</node>
</node>
<node TEXT="Head" STYLE_REF="orange_node" ID="ID_537834568" CREATED="1429591336582" MODIFIED="1429591469370">
<node TEXT="Hard headed" STYLE_REF="yellow_node" ID="ID_1182374756" CREATED="1429591345550" MODIFIED="1429591477602">
<font BOLD="true" ITALIC="true"/>
<node TEXT="You are hard headed, you never hear good advices" STYLE_REF="example_node" ID="ID_1853758511" CREATED="1429591463703" MODIFIED="1429591587326"/>
</node>
<node TEXT="Hot headed" STYLE_REF="yellow_node" ID="ID_1264065825" CREATED="1429591360935" MODIFIED="1429591477602">
<font BOLD="true" ITALIC="true"/>
<node TEXT="You are hard headed, you get angry very quickly" STYLE_REF="example_node" ID="ID_695176568" CREATED="1429591511189" MODIFIED="1429591587325"/>
</node>
<node TEXT="Big headed" STYLE_REF="yellow_node" ID="ID_97536819" CREATED="1429591369303" MODIFIED="1429591477603">
<font BOLD="true" ITALIC="true"/>
<node TEXT="You are hard headed, you think that you the&#xa;best programmer in the world, but it isn&apos;t true" STYLE_REF="example_node" ID="ID_1241181602" CREATED="1429591531457" MODIFIED="1429591587324"/>
</node>
<node TEXT="Head over heels" STYLE_REF="yellow_node" ID="ID_1474277892" CREATED="1429591381359" MODIFIED="1429591477603">
<font BOLD="true" ITALIC="true"/>
<node TEXT="To fall in love" STYLE_REF="pre_last_node" ID="ID_1510440110" CREATED="1429591422629" MODIFIED="1429591591387"/>
<node TEXT="I&apos;m head over heels for someone" STYLE_REF="example_node" ID="ID_1899447728" CREATED="1429591393263" MODIFIED="1429591587324"/>
</node>
</node>
<node TEXT="Down" STYLE_REF="orange_node" ID="ID_163084078" CREATED="1429591625270" MODIFIED="1429591682635">
<node TEXT="Shot me down" STYLE_REF="yellow_node" ID="ID_839476151" CREATED="1429591633360" MODIFIED="1429591955226">
<font BOLD="true" ITALIC="true"/>
<node TEXT="To rejected" STYLE_REF="pre_last_node" ID="ID_479537437" CREATED="1429591792255" MODIFIED="1429591978037"/>
<node TEXT="She shot me down" STYLE_REF="example_node" ID="ID_1830903548" CREATED="1429591771239" MODIFIED="1429591969172"/>
</node>
<node TEXT="Down in the dumps" STYLE_REF="yellow_node" ID="ID_1649703415" CREATED="1429591642166" MODIFIED="1429591955226">
<font BOLD="true" ITALIC="true"/>
<node TEXT="Very sad and depressed" STYLE_REF="pre_last_node" ID="ID_798978327" CREATED="1429591828631" MODIFIED="1429591978037"/>
<node TEXT="Since, her cat died, she&apos;s been down in the dumps" STYLE_REF="example_node" ID="ID_1914161536" CREATED="1429591761679" MODIFIED="1429591969172"/>
</node>
<node TEXT="Down the drain" STYLE_REF="yellow_node" ID="ID_1095697113" CREATED="1429591650175" MODIFIED="1429591955227">
<font BOLD="true" ITALIC="true"/>
<node TEXT="Disappeared forever" STYLE_REF="pre_last_node" ID="ID_1048544205" CREATED="1429591859872" MODIFIED="1429591978036"/>
<node TEXT="All my problems went down the drain" STYLE_REF="example_node" ID="ID_553743412" CREATED="1429591752943" MODIFIED="1429591969172"/>
</node>
<node TEXT="Down-to-earth" STYLE_REF="yellow_node" ID="ID_359552501" CREATED="1429591658614" MODIFIED="1429591955227">
<font BOLD="true" ITALIC="true"/>
<node TEXT="Practical person" STYLE_REF="pre_last_node" ID="ID_80000073" CREATED="1429591938814" MODIFIED="1429591978036"/>
<node TEXT="I want a girl who&apos;s down-to-earth" STYLE_REF="example_node" ID="ID_1726529527" CREATED="1429591743839" MODIFIED="1429591969173"/>
</node>
<node TEXT="Down and out" STYLE_REF="yellow_node" ID="ID_676814021" CREATED="1429591669181" MODIFIED="1429591955227">
<font BOLD="true" ITALIC="true"/>
<node TEXT="Without money and help" STYLE_REF="pre_last_node" ID="ID_747263781" CREATED="1429591717351" MODIFIED="1429591978036"/>
<node TEXT="She is down and out" STYLE_REF="example_node" ID="ID_566360416" CREATED="1429591714319" MODIFIED="1429591969173"/>
</node>
<node TEXT="Down to the wire" STYLE_REF="yellow_node" ID="ID_561464378" CREATED="1429591678471" MODIFIED="1429591955227">
<font BOLD="true" ITALIC="true"/>
<node TEXT="The game came down to the wire" STYLE_REF="example_node" ID="ID_155334541" CREATED="1429591705246" MODIFIED="1429591969173"/>
<node TEXT="Tense game until the very end" STYLE_REF="pre_last_node" ID="ID_1173194500" CREATED="1429591909520" MODIFIED="1429591978035"/>
</node>
</node>
<node TEXT="Talking" STYLE_REF="orange_node" ID="ID_426108942" CREATED="1429592077480" MODIFIED="1429592299842">
<node TEXT="To shoot the breeze" STYLE_REF="yellow_node" ID="ID_180330677" CREATED="1429592085294" MODIFIED="1429592307722">
<font BOLD="true" ITALIC="true"/>
<node TEXT="To chat" STYLE_REF="pre_last_node" ID="ID_358987142" CREATED="1429592245039" MODIFIED="1429593095737"/>
<node TEXT="Cidny had nothing to do, so she called an old friend to shoot the breeze" STYLE_REF="example_node" ID="ID_1049779874" CREATED="1429592255630" MODIFIED="1429593103087"/>
</node>
<node TEXT="To speak the same language" STYLE_REF="yellow_node" ID="ID_676483241" CREATED="1429592092846" MODIFIED="1429592307723">
<font BOLD="true" ITALIC="true"/>
<node TEXT="To agree; to think alike" STYLE_REF="pre_last_node" ID="ID_794859601" CREATED="1429592213144" MODIFIED="1429593095736"/>
<node TEXT="My brother and I don&apos;t speak the same language when it comes to polotics" STYLE_REF="example_node" ID="ID_1066176079" CREATED="1429592222750" MODIFIED="1429593103087"/>
</node>
<node TEXT="To talk a mile a minute" STYLE_REF="yellow_node" ID="ID_1426836859" CREATED="1429592100599" MODIFIED="1429592307723">
<font BOLD="true" ITALIC="true"/>
<node TEXT="To speak fast" STYLE_REF="pre_last_node" ID="ID_1391695473" CREATED="1429592195807" MODIFIED="1429593095735"/>
<node TEXT="What did the doctor say? I&apos;m not sure He spoke&#xa;a mile a minute, and I didn&apos;t catch everything he said" STYLE_REF="example_node" ID="ID_683761509" CREATED="1429592202631" MODIFIED="1429593103086"/>
</node>
<node TEXT="To talk someone into something" STYLE_REF="yellow_node" ID="ID_991750672" CREATED="1429592110463" MODIFIED="1429592307724">
<font BOLD="true" ITALIC="true"/>
<node TEXT="To perswade" STYLE_REF="pre_last_node" ID="ID_1130889604" CREATED="1429592133638" MODIFIED="1429593095734"/>
<node TEXT="The saler tried to talk me into buying the mote expensive of shoes.&#xa;I guess he works on commision!" STYLE_REF="example_node" ID="ID_1043142914" CREATED="1429592143447" MODIFIED="1429593103084"/>
</node>
<node TEXT="To talk someone out of something" STYLE_REF="yellow_node" ID="ID_771168957" CREATED="1429592118830" MODIFIED="1429592307724">
<font BOLD="true" ITALIC="true"/>
<node TEXT="To dissuade" STYLE_REF="pre_last_node" ID="ID_221688373" CREATED="1429592163264" MODIFIED="1429593095732"/>
<node TEXT="My girlfriend talked me out of staying at home crying over my&#xa;old boyfriend, and they talked me into going out with them" STYLE_REF="example_node" ID="ID_1696155071" CREATED="1429592172663" MODIFIED="1429593103085"/>
</node>
<node TEXT="To talk down to someone" STYLE_REF="yellow_node" ID="ID_897673542" CREATED="1429592355318" MODIFIED="1429592391498">
<font BOLD="true" ITALIC="true"/>
<node TEXT="To speak condescendingly" STYLE_REF="pre_last_node" ID="ID_357721683" CREATED="1429592379845" MODIFIED="1429593095731"/>
</node>
</node>
</node>
<node TEXT="Difference between" STYLE_REF="third_node" ID="ID_312519758" CREATED="1426663557416" MODIFIED="1426663597904">
<font SIZE="12" BOLD="false"/>
<node TEXT="small / little" STYLE_REF="orange_node" ID="ID_552212152" CREATED="1429615415094" MODIFIED="1441277342525">
<node TEXT="Small" STYLE_REF="yellow_node" ID="ID_1636801310" CREATED="1429615421838" MODIFIED="1441277342525">
<node TEXT="Size" STYLE_REF="pre_last_node" ID="ID_1273820442" CREATED="1429615427879" MODIFIED="1429615899340"/>
<node TEXT="Physical demensions" STYLE_REF="pre_last_node" ID="ID_1395192004" CREATED="1429615427879" MODIFIED="1429615899339"/>
<node TEXT="Intensity" STYLE_REF="pre_last_node" ID="ID_1800314867" CREATED="1429615427879" MODIFIED="1429615899339"/>
<node TEXT="Somebody has small hands = &#x432;&#x456;&#x43d; &#x43c;&#x430;&#x454; &#x444;&#x456;&#x437;&#x438;&#x447;&#x43d;&#x43e; &#x43c;&#x430;&#x43b;&#x456; &#x440;&#x443;&#x43a;&#x438;" STYLE_REF="example_node" ID="ID_631469362" CREATED="1429615599093" MODIFIED="1429615614754"/>
</node>
<node TEXT="Little" STYLE_REF="yellow_node" ID="ID_141888613" CREATED="1429615448526" MODIFIED="1429615891891">
<node TEXT="Use with uncountables" STYLE_REF="pre_last_node" ID="ID_1766399859" CREATED="1429615456136" MODIFIED="1429615899339"/>
<node TEXT="Stature" STYLE_REF="pre_last_node" ID="ID_1572058699" CREATED="1429615456136" MODIFIED="1429615899339"/>
<node TEXT="Adjective of degree" STYLE_REF="pre_last_node" ID="ID_1697473491" CREATED="1429615456136" MODIFIED="1429615899338"/>
<node TEXT="Somebody has little hands = &#x432;&#x456;&#x43d; &#x43c;&#x43e;&#x436;&#x435; &#x440;&#x43e;&#x431;&#x438;&#x442;&#x438; &#x440;&#x443;&#x43a;&#x430;&#x43c;&#x438; &#x43d;&#x435; &#x442;&#x430;&#x43a; &#x431;&#x430;&#x433;&#x430;&#x442;&#x43e;" STYLE_REF="example_node" ID="ID_1859284290" CREATED="1429615439158" MODIFIED="1429615613415"/>
<node TEXT="Little taried" STYLE_REF="example_node" ID="ID_967002319" CREATED="1429615587344" MODIFIED="1429615607964"/>
</node>
</node>
<node TEXT="though / although" STYLE_REF="orange_node" ID="ID_939269713" CREATED="1429616244230" MODIFIED="1429616826419">
<node TEXT="Although == though" STYLE_REF="yellow_node" ID="ID_1522306961" CREATED="1429616258534" MODIFIED="1429616836376">
<node TEXT="&#x425;&#x43e;&#x447;&#x430;, &#x43d;&#x430;&#x432;&#x456;&#x442;&#x44c; &#x43a;&#x43e;&#x43b;&#x438; &#x431;" STYLE_REF="pre_last_node" ID="ID_1258982917" CREATED="1429616818381" MODIFIED="1429616844465"/>
</node>
<node TEXT="Though" STYLE_REF="yellow_node" ID="ID_789948413" CREATED="1429616320182" MODIFIED="1429616836376">
<node TEXT="Put in the middle of the sentence" STYLE_REF="pre_last_node" ID="ID_1148585771" CREATED="1429616327791" MODIFIED="1429616840569"/>
</node>
<node STYLE_REF="yellow_node" ID="ID_1850039948" CREATED="1429616277620" MODIFIED="1429616836375"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Although
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Put in the beginning of the sentence" STYLE_REF="pre_last_node" ID="ID_982187578" CREATED="1429616271886" MODIFIED="1429616840544"/>
</node>
</node>
<node TEXT="may / might" STYLE_REF="orange_node" ID="ID_898834565" CREATED="1429616947533" MODIFIED="1429617179825">
<node TEXT="May" STYLE_REF="yellow_node" ID="ID_1049441228" CREATED="1429616956837" MODIFIED="1429617332418">
<node TEXT="In most cases &#x201c;may&#x201d; is used when there is a&#xa;possibility but you have a conflicting idea" STYLE_REF="pre_last_node" ID="ID_1448161154" CREATED="1429616966060" MODIFIED="1429617189427"/>
<node TEXT="What you are saying may be true, but I thought otherwise" STYLE_REF="example_node" ID="ID_674685287" CREATED="1429616979790" MODIFIED="1429617196571"/>
<node TEXT="I may not be able to make it there but I&#x2019;ll try" STYLE_REF="example_node" ID="ID_421174832" CREATED="1429616987548" MODIFIED="1429617200137"/>
</node>
<node TEXT="Might" STYLE_REF="yellow_node" ID="ID_127441864" CREATED="1429617056293" MODIFIED="1429617336241">
<node TEXT="On the other hand a &#x201c;might&#x201d; means agreeing to that possibility" STYLE_REF="pre_last_node" ID="ID_860491435" CREATED="1429617051550" MODIFIED="1429617189430"/>
<node TEXT="I think the next Batman villain might be Mr. Freeze" STYLE_REF="example_node" ID="ID_1398205385" CREATED="1429617069949" MODIFIED="1429617106483"/>
<node TEXT="It is when you do not have a conflicting idea to propose that you use &#x201c;might&#x201d;.&#xa;Another way of saying this is that &#x201c;might&#x201d; uses a bigger possibility of something than &#x201c;may&#x201d;" STYLE_REF="pre_last_node" ID="ID_361799986" CREATED="1429617098677" MODIFIED="1429617189431"/>
</node>
</node>
<node TEXT="can / may" STYLE_REF="orange_node" ID="ID_276744477" CREATED="1429617315956" MODIFIED="1429617378595">
<node TEXT="Can" STYLE_REF="yellow_node" ID="ID_1812603239" CREATED="1429617324973" MODIFIED="1429617382522">
<node TEXT="Can is used when there is the question of ability involved. Can I go to the toilet? Of course I can go to the toilet. I have two legs and I know the direction where the toilet is, so I can go to the toilet. The camel can go without water for days because it has that hump where it can store water and all that, so it has the ability to skip drinking water unlike you and I. We cannot go without water because we do not function like that" STYLE_REF="pre_last_node" ID="ID_751862163" CREATED="1429617327444" MODIFIED="1429617386965"/>
</node>
<node TEXT="May" STYLE_REF="yellow_node" ID="ID_1299410881" CREATED="1429617357213" MODIFIED="1429617382522">
<node TEXT="On the other hand, May is used when there is the question of permission involved. I have to ask the teacher if I may excuse myself to go to the toilet. I have to ask my father if I may take the newspaper from him. My child has to ask me if he may skip his homework today. In all these examples, my child and I are able to do all of these things, so we can do all that, but we seek permission to do these, so it is a question of whether or not we may do these things" STYLE_REF="pre_last_node" ID="ID_875446169" CREATED="1429617359204" MODIFIED="1429617386962"/>
<node TEXT="May also involves chance. There may be a chance of rain today. A crocodile may live up to be a hundred. See, there is a chance that the crocodile may die when it is fifty, but there is also a chance that it lives to be a hundred. There is a chance that it may rain, but there is also a chance that it may not rain. In this case, the question of ability does not arise. It is not in the hands of the crocodile to decide that it will live a hundred years, but there is chance that it will" STYLE_REF="pre_last_node" ID="ID_1491168987" CREATED="1429617366869" MODIFIED="1429617386966"/>
</node>
</node>
<node TEXT="sunrise / dawn" STYLE_REF="orange_node" ID="ID_1325562507" CREATED="1429617603757" MODIFIED="1429618758722">
<node TEXT="Sunrise" STYLE_REF="yellow_node" ID="ID_726738877" CREATED="1429617609997" MODIFIED="1429617789324">
<node TEXT="The moment when the sun is exactly parallel to the horizon" STYLE_REF="pre_last_node" ID="ID_1718899226" CREATED="1429617630317" MODIFIED="1429617795949"/>
</node>
<node TEXT="Dawn" STYLE_REF="yellow_node" ID="ID_1486382164" CREATED="1429617618205" MODIFIED="1429618758722">
<node TEXT="Is the moment when the sun has yet not appeared on horizon, but there is still some light, it takes thirty minets before sunrise" STYLE_REF="pre_last_node" ID="ID_131006843" CREATED="1429617640597" MODIFIED="1429617800457"/>
<node TEXT="Dawn == daybreak (&#x441;&#x432;&#x456;&#x442;&#x430;&#x43d;&#x43e;&#x43a;)" STYLE_REF="pre_last_node" ID="ID_1158969724" CREATED="1429617769469" MODIFIED="1429617795949"/>
</node>
<node TEXT="Dawn &gt; sunrise &gt; sunset &gt; dusk" STYLE_REF="yellow_node" ID="ID_1846916251" CREATED="1429617761229" MODIFIED="1429617789287"/>
</node>
<node TEXT="turtle / tortoise / terrapin" STYLE_REF="orange_node" ID="ID_1189453644" CREATED="1429617933220" MODIFIED="1429617982635">
<node TEXT="Turtle" STYLE_REF="yellow_node" ID="ID_103004673" CREATED="1429617939637" MODIFIED="1429618095922">
<node TEXT="&#x412;&#x43e;&#x434;&#x43d;&#x430; &#x447;&#x435;&#x440;&#x435;&#x43f;&#x430;&#x445;&#x430;, &#x432;&#x438;&#x43b;&#x430;&#x437;&#x438;&#x442;&#x44c; &#x43d;&#x430; &#x437;&#x435;&#x43c;&#x43b;&#x44e; &#x442;&#x456;&#x43b;&#x44c;&#x43a;&#x438; &#x43f;&#x456;&#x434; &#x447;&#x430;&#x441; &#x43d;&#x435;&#x440;&#x435;&#x441;&#x442;&#x443;, &#x43d;&#x435; &#x432;&#x435;&#x433;&#x435;&#x442;&#x435;&#x440;&#x456;&#x430;&#x43d;&#x43a;&#x430;, &#x43d;&#x435;&#x43c;&#x43e;&#x436;&#x435; &#x432;&#x442;&#x44f;&#x433;&#x43d;&#x443;&#x442;&#x44c; &#x432; &#x441;&#x435;&#x431;&#x435; &#x43b;&#x430;&#x43f;&#x438; &#x456; &#x433;&#x43e;&#x43b;&#x43e;&#x432;&#x443;, &#x43c;&#x430;&#x454; &#x43f;&#x43b;&#x430;&#x432;&#x43d;&#x438;&#x43a;&#x456;" STYLE_REF="pre_last_node" ID="ID_1346015387" CREATED="1429617957229" MODIFIED="1429618101658"/>
</node>
<node TEXT="Tortoise" STYLE_REF="yellow_node" ID="ID_1613652577" CREATED="1429617945294" MODIFIED="1429618095923">
<node TEXT="&#x417;&#x435;&#x43c;&#x43b;&#x44f;&#x43d;&#x430; &#x447;&#x435;&#x440;&#x435;&#x43f;&#x430;&#x445;&#x430;, &#x43f;&#x43e;&#x432;&#x43d;&#x430; &#x432;&#x435;&#x433;&#x435;&#x442;&#x435;&#x440;&#x456;&#x430;&#x43d;&#x43a;&#x430;, &#x43f;&#x43e;&#x433;&#x430;&#x43d;&#x43e; &#x43f;&#x43b;&#x430;&#x432;&#x430;&#x454;, &#x43c;&#x43e;&#x436;&#x435; &#x43f;&#x43e;&#x432;&#x43d;&#x456;&#x441;&#x442;&#x44e; &#x441;&#x445;&#x43e;&#x432;&#x430;&#x442;&#x44c;&#x441;&#x44f; &#x432; &#x441;&#x432;&#x456;&#x439; &#x43f;&#x430;&#x43d;&#x446;&#x435;&#x440;" STYLE_REF="pre_last_node" ID="ID_1254242547" CREATED="1429617969108" MODIFIED="1429618101662"/>
</node>
<node TEXT="Terrapin" STYLE_REF="yellow_node" ID="ID_2258190" CREATED="1429617951157" MODIFIED="1429618095923">
<node TEXT="&#x41d;&#x430;&#x43f;&#x456;&#x432; &#x432;&#x43e;&#x434;&#x43d;&#x430; &#x43d;&#x430;&#x43f;&#x456;&#x432; &#x437;&#x435;&#x43c;&#x43b;&#x44f;&#x43d;&#x430; &#x447;&#x435;&#x440;&#x435;&#x43f;&#x430;&#x445;&#x430;, &#x43c;&#x43e;&#x436;&#x435; &#x441;&#x445;&#x43e;&#x432;&#x430;&#x442;&#x44c; &#x442;&#x456;&#x43b;&#x44c;&#x43a;&#x438; &#x43a;&#x456;&#x43d;&#x446;&#x456;&#x432;&#x43a;&#x438; &#x432; &#x43f;&#x430;&#x43d;&#x441;&#x435;&#x440;, &#x436;&#x438;&#x432;&#x435; &#x443; &#x43d;&#x435;&#x432;&#x435;&#x43b;&#x438;&#x43a;&#x438;&#x445; &#x432;&#x43e;&#x434;&#x43e;&#x439;&#x43c;&#x430;&#x445;" STYLE_REF="pre_last_node" ID="ID_1724669646" CREATED="1429617978445" MODIFIED="1429618101664"/>
</node>
</node>
<node TEXT="borrow / lend / rent / use" STYLE_REF="orange_node" ID="ID_370706517" CREATED="1429618131405" MODIFIED="1429618636245">
<node TEXT="Borrow" STYLE_REF="yellow_node" ID="ID_745345312" CREATED="1429618136904" MODIFIED="1429618643100">
<node TEXT="&#x41f;&#x43e;&#x437;&#x438;&#x447;&#x430;&#x442;&#x44c; &#x443; &#x43a;&#x43e;&#x433;&#x43e;&#x441;&#x44c;" STYLE_REF="pre_last_node" ID="ID_873104636" CREATED="1429618426725" MODIFIED="1429618660362"/>
<node TEXT="Can I borrow your car?" STYLE_REF="example_node" ID="ID_538296456" CREATED="1429618471079" MODIFIED="1429618667715"/>
</node>
<node TEXT="Lend" STYLE_REF="yellow_node" ID="ID_41376108" CREATED="1429618141791" MODIFIED="1429618643100">
<node TEXT="&#x41f;&#x43e;&#x437;&#x438;&#x447;&#x430;&#x442;&#x44c; &#x43a;&#x43e;&#x43c;&#x443;&#x441;&#x44c;" STYLE_REF="pre_last_node" ID="ID_1343133816" CREATED="1429618436261" MODIFIED="1429618660363"/>
<node TEXT="Can you land me your car?" STYLE_REF="example_node" ID="ID_1270301016" CREATED="1429618478384" MODIFIED="1429618667715"/>
</node>
<node TEXT="Rent" STYLE_REF="yellow_node" ID="ID_856877720" CREATED="1429618146454" MODIFIED="1429618643100">
<node TEXT="&#x41e;&#x440;&#x435;&#x43d;&#x434;&#x443;&#x432;&#x430;&#x44c;&#x442;&#x44c;, &#x43f;&#x43e;&#x437;&#x438;&#x447;&#x430;&#x442;&#x44c; &#x437;&#x430; &#x43f;&#x43b;&#x430;&#x442;&#x443;" STYLE_REF="pre_last_node" ID="ID_1338514637" CREATED="1429618446532" MODIFIED="1429618660363"/>
</node>
<node TEXT="Use" STYLE_REF="yellow_node" ID="ID_786916608" CREATED="1429618150711" MODIFIED="1429618643099">
<node TEXT="&#x412;&#x438;&#x43a;&#x43e;&#x440;&#x438;&#x441;&#x442;&#x43e;&#x432;&#x443;&#x432;&#x430;&#x442;&#x44c;. &#x421;&#x43a;&#x43e;&#x440;&#x456;&#x448;&#x435; &#x437;&#x430; &#x432;&#x441;&#x435; &#x43d;&#x435; &#x43f;&#x43e;&#x432;&#x435;&#x440;&#x442;&#x430;&#x442;&#x44c;" STYLE_REF="pre_last_node" ID="ID_1279228382" CREATED="1429618457519" MODIFIED="1429618660363"/>
</node>
</node>
<node TEXT="convince / persuade" STYLE_REF="orange_node" ID="ID_1164655195" CREATED="1429618777613" MODIFIED="1429619280019">
<node TEXT="Convince" STYLE_REF="yellow_node" ID="ID_562757944" CREATED="1429618782792" MODIFIED="1429619322185">
<node TEXT="&#x423;&#x431;&#x456;&#x434;&#x438;&#x442;&#x44c; &#x456; &#x437;&#x43c;&#x456;&#x43d;&#x438;&#x442;&#x44c; &#x447;&#x438;&#x44e;&#x441;&#x44e; &#x434;&#x443;&#x43c;&#x43a;&#x443;" STYLE_REF="pre_last_node" ID="ID_115594354" CREATED="1429618807833" MODIFIED="1429619326609"/>
<node TEXT="I will convince her father that I am a nice guy" STYLE_REF="example_node" ID="ID_1968690732" CREATED="1429619315308" MODIFIED="1429619331698"/>
</node>
<node TEXT="Persuade" STYLE_REF="yellow_node" ID="ID_1868300452" CREATED="1429618791409" MODIFIED="1429619322178">
<node TEXT="&#x423;&#x431;&#x456;&#x434;&#x438;&#x442;&#x44c; &#x456; &#x441;&#x43f;&#x43e;&#x43d;&#x443;&#x43a;&#x430;&#x442;&#x438; &#x43a;&#x43e;&#x433;&#x43e;&#x441;&#x44c; &#x437;&#x440;&#x43e;&#x431;&#x438;&#x442;&#x44c; &#x448;&#x43e;&#x441;&#x44c;" STYLE_REF="pre_last_node" ID="ID_1115800491" CREATED="1429618816045" MODIFIED="1429619326608"/>
<node TEXT="I know I will persuade my father to buy me this car" STYLE_REF="example_node" ID="ID_72637131" CREATED="1429619297396" MODIFIED="1429619331697"/>
</node>
</node>
<node TEXT="necessary / essential" STYLE_REF="orange_node" ID="ID_730378968" CREATED="1429619423828" MODIFIED="1429619617818">
<node TEXT="Necessary" STYLE_REF="yellow_node" ID="ID_110918247" CREATED="1429619428759" MODIFIED="1429619613674">
<node TEXT="&#x41d;&#x435;&#x43e;&#x431;&#x445;&#x456;&#x434;&#x43d;&#x438;&#x439; &#x430;&#x43b;&#x435; &#x437;&#x430;&#x43c;&#x456;&#x43d;&#x44f;&#x454;&#x43c;&#x438;&#x439;" STYLE_REF="pre_last_node" ID="ID_37361289" CREATED="1429619442085" MODIFIED="1429619629481"/>
</node>
<node TEXT="Essential" STYLE_REF="yellow_node" ID="ID_191744864" CREATED="1429619434376" MODIFIED="1429619613676">
<node TEXT="&#x41d;&#x435;&#x43e;&#x431;&#x445;&#x456;&#x434;&#x43d;&#x438;&#x439; &#x456; &#x441;&#x430;&#x43c;&#x438;&#x439; &#x433;&#x43e;&#x43b;&#x43e;&#x432;&#x43d;&#x438;&#x439;" STYLE_REF="pre_last_node" ID="ID_1923463283" CREATED="1429619448198" MODIFIED="1429619629481"/>
</node>
<node TEXT="To do well in this exam, a pencil is necessary,&#xa;but a good knowledge of subject is essential" STYLE_REF="example_node" ID="ID_103268701" CREATED="1429619453646" MODIFIED="1429619472082"/>
</node>
<node TEXT="wound / injury" STYLE_REF="orange_node" ID="ID_1697078282" CREATED="1429621081180" MODIFIED="1429621129634">
<node TEXT="Wound" STYLE_REF="yellow_node" ID="ID_333448035" CREATED="1429621085830" MODIFIED="1429621133512">
<node TEXT="&#x420;&#x430;&#x43d;&#x430; &#x432;&#x456;&#x434; &#x430;&#x433;&#x440;&#x435;&#x441;&#x43e;&#x440;&#x430;, &#x456; &#x44f;&#x43a;&#x430; &#x43a;&#x440;&#x43e;&#x432;&#x43e;&#x442;&#x43e;&#x447;&#x438;&#x43b;&#x430; &#x447;&#x438; &#x431;&#x443;&#x43b;&#x430; &#x440;&#x432;&#x430;&#x43d;&#x430;" STYLE_REF="pre_last_node" ID="ID_416747211" CREATED="1429621097871" MODIFIED="1429621137298"/>
</node>
<node TEXT="Injury" STYLE_REF="yellow_node" ID="ID_665770107" CREATED="1429621091754" MODIFIED="1429621133512">
<node TEXT="&#x420;&#x430;&#x43d;&#x430; &#x432;&#x456;&#x434; &#x456;&#x43d;&#x446;&#x438;&#x434;&#x435;&#x43d;&#x442;&#x443;, &#x456; &#x442;&#x430;&#x43a;&#x43e;&#x436; &#x432;&#x438;&#x432;&#x438;&#x445;&#x456;, &#x43f;&#x435;&#x440;&#x435;&#x43b;&#x43e;&#x43c;&#x438;, &#x431;&#x443;&#x434;&#x44c;-&#x44f;&#x43a;&#x438;&#x439; &#x443;&#x440;&#x43e;&#x43d;.!!" STYLE_REF="pre_last_node" ID="ID_1655780092" CREATED="1429621109527" MODIFIED="1429621137297"/>
</node>
</node>
<node TEXT="tall / high" STYLE_REF="orange_node" ID="ID_116365050" CREATED="1429622307628" MODIFIED="1429622392434">
<node TEXT="Tall" STYLE_REF="yellow_node" ID="ID_1410621444" CREATED="1429622313197" MODIFIED="1429622399344">
<node TEXT="Physical standing" STYLE_REF="pre_last_node" ID="ID_1385281192" CREATED="1429622324582" MODIFIED="1429622408594"/>
<node TEXT="Buildings" STYLE_REF="pre_last_node" ID="ID_1858152056" CREATED="1429622324582" MODIFIED="1429622408594"/>
<node TEXT="People" STYLE_REF="pre_last_node" ID="ID_118777240" CREATED="1429622324583" MODIFIED="1429622408594"/>
<node TEXT="The Pentagon is very big building, but it isn&apos;t very tall" STYLE_REF="example_node" ID="ID_1578348097" CREATED="1429622333533" MODIFIED="1429622414256"/>
</node>
<node TEXT="High" STYLE_REF="yellow_node" ID="ID_1335830050" CREATED="1429622318815" MODIFIED="1429622399345">
<node TEXT="Above averege" STYLE_REF="pre_last_node" ID="ID_1985343451" CREATED="1429622353989" MODIFIED="1429622408594"/>
<node TEXT="Above than others like it" STYLE_REF="pre_last_node" ID="ID_874395366" CREATED="1429622353989" MODIFIED="1429622408593"/>
<node TEXT="Ideas" STYLE_REF="pre_last_node" ID="ID_1914170363" CREATED="1429622353990" MODIFIED="1429622408593"/>
<node TEXT="Position" STYLE_REF="pre_last_node" ID="ID_32238940" CREATED="1429622353990" MODIFIED="1429622408594"/>
<node TEXT="High ground" STYLE_REF="example_node" ID="ID_1036278525" CREATED="1429622348310" MODIFIED="1429622430975"/>
<node STYLE_REF="example_node" ID="ID_65749889" CREATED="1429622425822" MODIFIED="1429622437097"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      High mountain
    </p>
  </body>
</html>
</richcontent>
</node>
<node STYLE_REF="example_node" ID="ID_216295451" CREATED="1429622433629" MODIFIED="1429622437097"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      High culture
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="personal / private" STYLE_REF="orange_node" ID="ID_3430407" CREATED="1429622449430" MODIFIED="1429622522111">
<node TEXT="Personal" STYLE_REF="yellow_node" ID="ID_1628273052" CREATED="1429622465876" MODIFIED="1429622513080">
<node TEXT="Many people would be aware of, you can share with your friends. Like:&#xa;hobbies, where you live, where you go to eat, etc" STYLE_REF="pre_last_node" ID="ID_189136678" CREATED="1429622498000" MODIFIED="1429622531576"/>
</node>
<node TEXT="Private" STYLE_REF="yellow_node" ID="ID_1703653046" CREATED="1429622473571" MODIFIED="1429622522111">
<node TEXT="No one else knows about, you keep it with yourself. Like: thoughts, habits, etc" STYLE_REF="pre_last_node" ID="ID_1213675651" CREATED="1429622523103" MODIFIED="1429622531576"/>
</node>
</node>
<node TEXT="suggest / propose / offer" STYLE_REF="orange_node" ID="ID_357144748" CREATED="1429622454997" MODIFIED="1429622460185">
<node TEXT="Suggest" STYLE_REF="yellow_node" ID="ID_1633777578" CREATED="1429622479413" MODIFIED="1429622513081">
<node TEXT="&#x41f;&#x440;&#x43e;&#x43f;&#x43e;&#x43d;&#x443;&#x432;&#x430;&#x442;&#x44c; &#x456;&#x434;&#x435;&#x44e;, &#x434;&#x443;&#x43c;&#x43a;&#x443;, &#x43d;&#x430;&#x442;&#x44f;&#x43a;&#x43d;&#x443;&#x442;&#x44c;, &#x440;&#x435;&#x43a;&#x43e;&#x43c;&#x435;&#x43d;&#x434;&#x443;&#x432;&#x430;&#x442;&#x44c;" STYLE_REF="pre_last_node" ID="ID_996847820" CREATED="1429622543207" MODIFIED="1429622644584"/>
<node TEXT="I suggested a good idea to my boss" STYLE_REF="example_node" ID="ID_1016462650" CREATED="1429622563334" MODIFIED="1429622626946"/>
<node TEXT="I suggest you to tidying up your room" STYLE_REF="example_node" ID="ID_981757476" CREATED="1429622615890" MODIFIED="1429622626945"/>
</node>
<node TEXT="Propose" STYLE_REF="yellow_node" ID="ID_1348129988" CREATED="1429622484789" MODIFIED="1429622513081">
<node TEXT="&#x41f;&#x440;&#x43e;&#x43f;&#x43e;&#x43d;&#x443;&#x432;&#x430;&#x442;&#x44c; &#x43a;&#x43e;&#x43d;&#x43a;&#x440;&#x435;&#x442;&#x43d;&#x443; &#x434;&#x456;&#x44e;, &#x430;&#x431;&#x43e; &#x43a;&#x43e;&#x43d;&#x43a;&#x440;&#x435;&#x442;&#x43d;&#x438;&#x439; &#x43f;&#x43b;&#x430;&#x43d; &#x434;&#x456;&#x439;, &#x442;&#x430;&#x43a;&#x43e;&#x436; &#x440;&#x43e;&#x431;&#x438;&#x442;&#x44c; &#x43f;&#x440;&#x43e;&#x43f;&#x43e;&#x437;&#x438;&#x446;&#x456;&#x44e; &#x434;&#x430;&#x43c;&#x456;. &#x41c;&#x430;&#x454; &#x431;&#x456;&#x43b;&#x44c; &#x43e;&#x444;&#x456;&#x446;&#x456;&#x439;&#x43d;&#x438;&#x439; &#x432;&#x456;&#x434;&#x442;&#x456;&#x43d;&#x43e;&#x43a;" STYLE_REF="pre_last_node" ID="ID_1990608677" CREATED="1429622549142" MODIFIED="1429622641707"/>
<node TEXT="He proposed a solution of that problem" STYLE_REF="example_node" ID="ID_1035222339" CREATED="1429622573949" MODIFIED="1429622626946"/>
<node STYLE_REF="example_node" ID="ID_1936229645" CREATED="1429622596686" MODIFIED="1429622626946"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      He proposed changing the logo of our company
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Offer" STYLE_REF="yellow_node" ID="ID_1911701726" CREATED="1429622489528" MODIFIED="1429622513081">
<node TEXT="&#x41f;&#x440;&#x43e;&#x43f;&#x43e;&#x43d;&#x443;&#x432;&#x430;&#x442;&#x44c; &#x449;&#x43e;&#x441;&#x44c; &#x434;&#x443;&#x436;&#x435; &#x43a;&#x43e;&#x43d;&#x43a;&#x440;&#x435;&#x442;&#x43d;&#x435;: &#x43a;&#x43e;&#x444;&#x435;, &#x43c;&#x430;&#x442;&#x435;&#x440;&#x456;&#x430;&#x43b;&#x44c;&#x43d;&#x443; &#x434;&#x43e;&#x43f;&#x43e;&#x43c;&#x43e;&#x433;&#x443;, &#x440;&#x43e;&#x431;&#x43e;&#x442;&#x443;. &#x41a;&#x43e;&#x43d;&#x43a;&#x440;&#x435;&#x442;&#x43d;&#x430; &#x43f;&#x440;&#x43e;&#x43f;&#x43e;&#x437;&#x438;&#x446;&#x456;&#x44f; &#x44f;&#x43a;&#x443; &#x43c;&#x43e;&#x436;&#x43d;&#x430; &#x43f;&#x440;&#x438;&#x439;&#x43d;&#x44f;&#x442;&#x438; &#x447;&#x438; &#x43d;&#x435;&#x43f;&#x440;&#x438;&#x439;&#x43d;&#x44f;&#x442;&#x438;" STYLE_REF="pre_last_node" ID="ID_1311955196" CREATED="1429622556373" MODIFIED="1429622638361"/>
<node TEXT="I offered her a cup of tea" STYLE_REF="example_node" ID="ID_424612554" CREATED="1429622586446" MODIFIED="1429622626946"/>
<node TEXT="My friend offered me to help with my car" STYLE_REF="example_node" ID="ID_636418251" CREATED="1429622605396" MODIFIED="1429622626946"/>
</node>
</node>
<node TEXT="famous / popular" STYLE_REF="orange_node" ID="ID_1938865974" CREATED="1441277294527" MODIFIED="1441278602565">
<node TEXT="popular" STYLE_REF="yellow_node" ID="ID_883626844" CREATED="1441277303167" MODIFIED="1441278598287"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Being popular is when an individual is liked by many people.
    </p>
    <p>
      Princess Diana can be considered as a truly popular personality who won the hearts of thousands of people. The key characteristic of a popular person is that he or she is not only known by people, but also loved.
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="famous" STYLE_REF="yellow_node" ID="ID_1555113659" CREATED="1441278564167" MODIFIED="1441278598288"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Famous is being known by a large number of people.
    </p>
    <p>
      For example, Hitler was famous throughout the world and even today his name is known by most of us.
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="Phrasal verbs" STYLE_REF="third_node" ID="ID_1763121179" CREATED="1426663574100" MODIFIED="1426663597907">
<font SIZE="12" BOLD="false"/>
<node TEXT="Put" STYLE_REF="orange_node" ID="ID_1270535431" CREATED="1429593249661" MODIFIED="1429594158379">
<node TEXT="Put up with something/someone" STYLE_REF="yellow_node" ID="ID_784977885" CREATED="1429594028237" MODIFIED="1429594164482">
<font BOLD="true" ITALIC="true"/>
<node TEXT="To tolerate" STYLE_REF="pre_last_node" ID="ID_574349989" CREATED="1429594134150" MODIFIED="1429594170433"/>
<node TEXT="I hate traffic, but I have to put up with it.&#xa;Why do you put up with her?" STYLE_REF="example_node" ID="ID_845622864" CREATED="1429594147174" MODIFIED="1429594177028"/>
</node>
<node TEXT="Put on something" STYLE_REF="yellow_node" ID="ID_1169920185" CREATED="1429594036726" MODIFIED="1429594164482">
<font BOLD="true" ITALIC="true"/>
<node TEXT="To cover your body with clothing or jewerly" STYLE_REF="pre_last_node" ID="ID_6080050" CREATED="1429594107589" MODIFIED="1429594170435"/>
<node TEXT="I never put on socks when I&apos;m at home.&#xa;If you&apos;re going to the pool you should put on your goggles." STYLE_REF="example_node" ID="ID_1039871108" CREATED="1429594119702" MODIFIED="1429594177028"/>
</node>
<node TEXT="Put something away" STYLE_REF="yellow_node" ID="ID_1988286697" CREATED="1429594045014" MODIFIED="1429594164482">
<font BOLD="true" ITALIC="true"/>
<node TEXT="To put in an appropriate place" STYLE_REF="pre_last_node" ID="ID_1049125778" CREATED="1429594088430" MODIFIED="1429594170435"/>
<node TEXT="Put away your books, it&apos;s time for the test.&#xa;Please put away your phones and other electronic devices" STYLE_REF="example_node" ID="ID_1213316089" CREATED="1429594098374" MODIFIED="1429594177027"/>
</node>
<node TEXT="Put together something" STYLE_REF="yellow_node" ID="ID_309481202" CREATED="1429594056527" MODIFIED="1429594164483">
<font BOLD="true" ITALIC="true"/>
<node TEXT="To assemble" STYLE_REF="pre_last_node" ID="ID_1338027952" CREATED="1429594066708" MODIFIED="1429594170436"/>
<node TEXT="I like putting together new furniture.&#xa;She&apos;s very good at putting together puzzles" STYLE_REF="example_node" ID="ID_336101720" CREATED="1429594075662" MODIFIED="1429594177025"/>
</node>
</node>
<node TEXT="Fall" STYLE_REF="orange_node" ID="ID_1213329392" CREATED="1429593256405" MODIFIED="1429594158380">
<node TEXT="Fall apart" STYLE_REF="yellow_node" ID="ID_304243475" CREATED="1429594220309" MODIFIED="1429598957575">
<font BOLD="true" ITALIC="true"/>
<node TEXT="To have nervous termination of relations" STYLE_REF="pre_last_node" ID="ID_568691421" CREATED="1429609817135" MODIFIED="1429610098438"/>
<node TEXT="She brakedown with him and after that he fall apart" STYLE_REF="example_node" ID="ID_114010534" CREATED="1429607694881" MODIFIED="1429610108341"/>
</node>
<node TEXT="Fall out" STYLE_REF="yellow_node" ID="ID_728961336" CREATED="1429594227781" MODIFIED="1429598957576">
<font BOLD="true" ITALIC="true"/>
<node TEXT="The effects of something bad" STYLE_REF="pre_last_node" ID="ID_260554935" CREATED="1429609786897" MODIFIED="1429610098439"/>
<node TEXT="Something falls out of my pocket.&#xa;The fall out from this attack was that many people were killed" STYLE_REF="example_node" ID="ID_297593023" CREATED="1429607706913" MODIFIED="1429610108340"/>
</node>
<node TEXT="Fall for" STYLE_REF="yellow_node" ID="ID_653810154" CREATED="1429594236158" MODIFIED="1429598957576">
<font BOLD="true" ITALIC="true"/>
<node TEXT="Getting caught by the trick" STYLE_REF="pre_last_node" ID="ID_1213284738" CREATED="1429609757577" MODIFIED="1429610098441"/>
<node TEXT="I fall for his trick.&#xa;I fall for her" STYLE_REF="example_node" ID="ID_73543728" CREATED="1429608613280" MODIFIED="1429610108339"/>
</node>
<node TEXT="Fall through" STYLE_REF="yellow_node" ID="ID_1019743995" CREATED="1429594243773" MODIFIED="1429598957576">
<font BOLD="true" ITALIC="true"/>
<node TEXT="The attempts were defeated" STYLE_REF="pre_last_node" ID="ID_1157124619" CREATED="1429609670000" MODIFIED="1429610098442"/>
<node TEXT="I spend a lot of time planing my trip, but in the last moment my boss told me that I have to go work because we have an emergency, so my plans fall through" STYLE_REF="example_node" ID="ID_49677000" CREATED="1429608623736" MODIFIED="1429610108337"/>
</node>
<node TEXT="Fall in!" STYLE_REF="yellow_node" ID="ID_723596889" CREATED="1429594251422" MODIFIED="1429598957576">
<font BOLD="true" ITALIC="true"/>
<node TEXT="To join, to follow" STYLE_REF="pre_last_node" ID="ID_1562074066" CREATED="1429609628616" MODIFIED="1429610098444"/>
<node TEXT="You have to fall in this program in our company" STYLE_REF="example_node" ID="ID_332004177" CREATED="1429608635666" MODIFIED="1429610108337"/>
</node>
<node TEXT="Fall in with" STYLE_REF="yellow_node" ID="ID_1346450710" CREATED="1429594266678" MODIFIED="1429598957577">
<font BOLD="true" ITALIC="true"/>
<node TEXT="To be involved in something without notice for yourself" STYLE_REF="pre_last_node" ID="ID_1402492644" CREATED="1429608681849" MODIFIED="1429610098444"/>
<node TEXT="Many kids fall in with gangs in America" STYLE_REF="example_node" ID="ID_840371669" CREATED="1429608653959" MODIFIED="1429610108336"/>
</node>
<node TEXT="Fall back on something" STYLE_REF="yellow_node" ID="ID_85377582" CREATED="1429594275302" MODIFIED="1429598957577">
<font BOLD="true" ITALIC="true"/>
<node TEXT="To rely on something; have a a backup" STYLE_REF="pre_last_node" ID="ID_1979397632" CREATED="1429608727081" MODIFIED="1429610098445"/>
<node TEXT="I always can fall back on my teacher&apos;s skills to get a job" STYLE_REF="example_node" ID="ID_82338351" CREATED="1429594286022" MODIFIED="1429610108333"/>
</node>
</node>
<node TEXT="Knock" STYLE_REF="orange_node" ID="ID_1956069897" CREATED="1429593258701" MODIFIED="1429594158380">
<node TEXT="Knock down" STYLE_REF="yellow_node" ID="ID_1257960734" CREATED="1429610348439" MODIFIED="1429611670451">
<font BOLD="true" ITALIC="true"/>
<node TEXT="To make it fall" STYLE_REF="pre_last_node" ID="ID_1088063326" CREATED="1429610965496" MODIFIED="1429611687806"/>
<node TEXT="You can knock down many walls inside a house to make more space" STYLE_REF="example_node" ID="ID_56604556" CREATED="1429610952311" MODIFIED="1429612389935"/>
</node>
<node TEXT="Knock over" STYLE_REF="yellow_node" ID="ID_77876059" CREATED="1429610356048" MODIFIED="1429611670451">
<font BOLD="true" ITALIC="true"/>
<node TEXT="To knock down accidentally" STYLE_REF="pre_last_node" ID="ID_1088404307" CREATED="1429610873399" MODIFIED="1429611687805"/>
<node TEXT="You bump into a table, you knock over their wine glass, spill it on their dress" STYLE_REF="example_node" ID="ID_1690820685" CREATED="1429610847312" MODIFIED="1429612389935"/>
</node>
<node TEXT="Knock up" STYLE_REF="yellow_node" ID="ID_624781974" CREATED="1429610366808" MODIFIED="1429611670451">
<font BOLD="true" ITALIC="true"/>
<node TEXT="To make someone pregnant, a pregnant girl" STYLE_REF="pre_last_node" ID="ID_591006297" CREATED="1429610812664" MODIFIED="1429611687804"/>
<node TEXT="She is knocked up == she is pregnant&#xa;I knocked up my girlfriend" STYLE_REF="example_node" ID="ID_839195345" CREATED="1429610833095" MODIFIED="1429612389934"/>
</node>
<node TEXT="Knock out" STYLE_REF="yellow_node" ID="ID_77637098" CREATED="1429610375735" MODIFIED="1429611670451">
<font BOLD="true" ITALIC="true"/>
</node>
<node TEXT="Knockout" STYLE_REF="yellow_node" ID="ID_1720219598" CREATED="1429610386968" MODIFIED="1429611670452">
<font BOLD="true" ITALIC="true"/>
<node TEXT="Very good person espacial woman" STYLE_REF="pre_last_node" ID="ID_486580043" CREATED="1429612042271" MODIFIED="1429612383109"/>
<node TEXT="If you see a woman walking down the street and you say, &quot;Wow, what a knockdown&quot;, that means she is gorgeous" STYLE_REF="example_node" ID="ID_1772285396" CREATED="1429610418560" MODIFIED="1429612389934"/>
</node>
<node TEXT="Knockoff" STYLE_REF="yellow_node" ID="ID_1875709127" CREATED="1429610396080" MODIFIED="1429611670450">
<font BOLD="true" ITALIC="true"/>
<node TEXT="A fake" STYLE_REF="pre_last_node" ID="ID_277443554" CREATED="1429610401543" MODIFIED="1429611687804"/>
<node TEXT="Many products from China are knockoff" STYLE_REF="example_node" ID="ID_1546334718" CREATED="1429610410865" MODIFIED="1429612389933"/>
</node>
</node>
<node TEXT="Look" STYLE_REF="orange_node" ID="ID_1701360555" CREATED="1429593277742" MODIFIED="1429594158381">
<node TEXT="Look up to" STYLE_REF="yellow_node" ID="ID_326225695" CREATED="1429612453951" MODIFIED="1429612817100">
<font BOLD="true" ITALIC="true"/>
<node TEXT="The role model" STYLE_REF="pre_last_node" ID="ID_1109793534" CREATED="1429612660951" MODIFIED="1429612825300"/>
<node TEXT="He you are child, you look up to your parents" STYLE_REF="example_node" ID="ID_1514916649" CREATED="1429612677127" MODIFIED="1429612830676"/>
</node>
<node TEXT="Look down on sombody" STYLE_REF="yellow_node" ID="ID_158021639" CREATED="1429612463375" MODIFIED="1429612817100">
<font BOLD="true" ITALIC="true"/>
<node TEXT="You make them inferior; you think that they are less than you" STYLE_REF="pre_last_node" ID="ID_266384291" CREATED="1429612508223" MODIFIED="1429612825302"/>
</node>
<node TEXT="Look over" STYLE_REF="yellow_node" ID="ID_858304362" CREATED="1429612471094" MODIFIED="1429612817099">
<font BOLD="true" ITALIC="true"/>
<node TEXT="To check it" STYLE_REF="pre_last_node" ID="ID_1115506987" CREATED="1429612477254" MODIFIED="1429612825301"/>
<node TEXT="I&apos;v wrote assei and give it to my friend to look over my essei" STYLE_REF="example_node" ID="ID_1875609773" CREATED="1429612491575" MODIFIED="1429612830675"/>
</node>
</node>
</node>
<node TEXT="How to say" STYLE_REF="third_node" ID="ID_1155744981" CREATED="1446706161585" MODIFIED="1446706194196">
<font SIZE="12" BOLD="false"/>
<node TEXT="Conclusions" STYLE_REF="orange_node" ID="ID_1740968252" CREATED="1446706195832" MODIFIED="1446706267309">
<node TEXT="Writing" STYLE_REF="yellow_node" ID="ID_1293449335" CREATED="1446706200976" MODIFIED="1446706415493">
<node TEXT="In conclusion," STYLE_REF="pre_last_node" ID="ID_213921942" CREATED="1446706215679" MODIFIED="1446706446317"/>
<node TEXT="Thus" STYLE_REF="pre_last_node" ID="ID_1640115992" CREATED="1446706220823" MODIFIED="1446706446319"/>
</node>
<node TEXT="Speeking" STYLE_REF="yellow_node" ID="ID_645543256" CREATED="1446706208023" MODIFIED="1446706415495">
<node TEXT="To sum up," STYLE_REF="pre_last_node" ID="ID_1528782219" CREATED="1446706231135" MODIFIED="1446706446320"/>
<node TEXT="So," STYLE_REF="pre_last_node" ID="ID_1611142888" CREATED="1446706235759" MODIFIED="1446706446321"/>
<node TEXT="In a nutshell," STYLE_REF="pre_last_node" ID="ID_1564184287" CREATED="1446706286119" MODIFIED="1446706446322"/>
<node TEXT="The bottom line is ..." STYLE_REF="pre_last_node" ID="ID_1060131202" CREATED="1446706299216" MODIFIED="1446706446324"/>
<node TEXT="For these reasons, ..." STYLE_REF="pre_last_node" ID="ID_311162072" CREATED="1446706335047" MODIFIED="1446706446325"/>
<node TEXT="All in all," STYLE_REF="pre_last_node" ID="ID_1195422198" CREATED="1446706347446" MODIFIED="1446706446326"/>
</node>
</node>
</node>
</node>
</node>
</map>
