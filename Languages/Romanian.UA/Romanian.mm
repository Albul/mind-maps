<map version="freeplane 1.3.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="&#x420;&#x443;&#x43c;&#x443;&#x43d;&#x441;&#x44c;&#x43a;&#x430; &#x43c;&#x43e;&#x432;&#x430;" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1427366257734" BACKGROUND_COLOR="#6daae0">
<font SIZE="14" BOLD="true"/>
<hook NAME="AutomaticEdgeColor" COUNTER="11"/>
<hook NAME="MapStyle">
    <properties show_icon_for_attributes="true" show_note_icons="true"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
<stylenode TEXT="main_node" BACKGROUND_COLOR="#ccccff">
<font SIZE="18" BOLD="true"/>
<edge STYLE="sharp_bezier" WIDTH="4"/>
</stylenode>
<stylenode TEXT="second_node" BACKGROUND_COLOR="#00cccc" STYLE="bubble">
<font SIZE="14"/>
</stylenode>
<stylenode TEXT="third_node" BACKGROUND_COLOR="#ffbdf4" STYLE="bubble">
<font SIZE="11" BOLD="true"/>
</stylenode>
<stylenode TEXT="orange_node" BACKGROUND_COLOR="#ff9900" STYLE="bubble">
<font BOLD="true"/>
</stylenode>
<stylenode TEXT="gray_node" BACKGROUND_COLOR="#9ec19e">
<font BOLD="true"/>
</stylenode>
<stylenode TEXT="yellow_node" BACKGROUND_COLOR="#ffff66" STYLE="bubble">
<edge WIDTH="1"/>
</stylenode>
<stylenode TEXT="example_node" BACKGROUND_COLOR="#afcdfa">
<edge STYLE="bezier" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="pre_last_node" BACKGROUND_COLOR="#99ff99" STYLE="bubble">
<edge STYLE="bezier" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="last_node" BACKGROUND_COLOR="#ffffff">
<edge STYLE="bezier" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="warning_node" BACKGROUND_COLOR="#fda6a6">
<icon BUILTIN="messagebox_warning"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<edge STYLE="horizontal" WIDTH="2"/>
<hook URI="../../Images/romania-flag-large.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x424;&#x43e;&#x442;&#x435;&#x442;&#x438;&#x43a;&#x430;" STYLE_REF="second_node" POSITION="left" ID="ID_1937380470" CREATED="1426404593926" MODIFIED="1427366284577">
<edge COLOR="#7c007c"/>
<hook URI="../../Images/phonetics.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x410;. &#x430;" STYLE_REF="orange_node" ID="ID_476166736" CREATED="1426356570704" MODIFIED="1426406622062">
<node TEXT="&#x430;" STYLE_REF="pre_last_node" ID="ID_1599873097" CREATED="1426356581674" MODIFIED="1426402815375"/>
</node>
<node TEXT="&#x102;, &#x103;" STYLE_REF="orange_node" ID="ID_247756090" CREATED="1426356578587" MODIFIED="1426406622061">
<node TEXT="&#x435;" STYLE_REF="pre_last_node" ID="ID_976408878" CREATED="1426356666576" MODIFIED="1426402815375"/>
</node>
<node TEXT="&#xc2;, &#xe2;" STYLE_REF="orange_node" ID="ID_959425668" CREATED="1426356687724" MODIFIED="1426406622060">
<node TEXT="&#x438;" STYLE_REF="pre_last_node" ID="ID_1446881717" CREATED="1426356693202" MODIFIED="1426402815375"/>
</node>
<node TEXT="B, b" STYLE_REF="orange_node" ID="ID_1269925058" CREATED="1426356731220" MODIFIED="1426406622059">
<node TEXT="&#x431;" STYLE_REF="pre_last_node" ID="ID_1947212117" CREATED="1426356732674" MODIFIED="1426402815375"/>
</node>
<node TEXT="C, c" STYLE_REF="orange_node" ID="ID_1226584722" CREATED="1426356136480" MODIFIED="1426406622058">
<node TEXT="&#x447;" STYLE_REF="pre_last_node" ID="ID_1692979277" CREATED="1426356200514" MODIFIED="1426402815374"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1055;&#1077;&#1088;&#1077;&#1076; i, e
    </p>
  </body>
</html>
</richcontent>
<node TEXT="cia, cea=&#x447;&#x430;" ID="ID_1860555282" CREATED="1426356221782" MODIFIED="1426356288098"/>
<node TEXT="cio=&#x447;&#x43e;" ID="ID_1798809335" CREATED="1426356232941" MODIFIED="1426356233879"/>
<node TEXT="ciu=&#x447;&#x443;" ID="ID_1769613328" CREATED="1426356240981" MODIFIED="1426356241390"/>
</node>
<node TEXT="&#x43a;" STYLE_REF="pre_last_node" ID="ID_186409943" CREATED="1426356243472" MODIFIED="1426402815374"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1042; &#1110;&#1085;&#1096;&#1080;&#1093; &#1074;&#1080;&#1087;&#1072;&#1076;&#1082;&#1072;&#1093;
    </p>
  </body>
</html>
</richcontent>
<node TEXT="chi = &#x43a;&#x456;" ID="ID_1050543433" CREATED="1426356269297" MODIFIED="1426356278717"/>
</node>
</node>
<node TEXT="D, d" STYLE_REF="orange_node" ID="ID_167833469" CREATED="1426356764940" MODIFIED="1426426944608">
<node TEXT="&#x434;" STYLE_REF="pre_last_node" ID="ID_1414732995" CREATED="1426356769106" MODIFIED="1426426944606"/>
</node>
<node TEXT="E, e" STYLE_REF="orange_node" ID="ID_213659161" CREATED="1426356083190" MODIFIED="1426406622056">
<node TEXT="&#x454;" STYLE_REF="pre_last_node" ID="ID_1621281275" CREATED="1426356097173" MODIFIED="1426402815374"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1053;&#1072; &#1087;&#1086;&#1095;&#1072;&#1090;&#1082;&#1091; &#1089;&#1083;&#1086;&#1074;&#1072;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&#x435;" STYLE_REF="pre_last_node" ID="ID_919666186" CREATED="1426356084934" MODIFIED="1426402815373"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1042; &#1091;&#1089;&#1110;&#1093; &#1110;&#1085;&#1096;&#1080;&#1093; &#1074;&#1080;&#1087;&#1072;&#1076;&#1082;&#1072;&#1093;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&#x44f;" STYLE_REF="pre_last_node" ID="ID_17627376" CREATED="1426403060523" MODIFIED="1426403098171"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1077;&#1072; - &#1095;&#1080;&#1090;&#1072;&#1108;&#1090;&#1100;&#1089;&#1103; [&#1103;]
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="F, f" STYLE_REF="orange_node" ID="ID_1216510795" CREATED="1426356797361" MODIFIED="1426406622055">
<node TEXT="&#x444;" STYLE_REF="pre_last_node" ID="ID_1814819479" CREATED="1426356801372" MODIFIED="1426402815373"/>
</node>
<node TEXT="G,g" STYLE_REF="orange_node" ID="ID_1410792220" CREATED="1426355689118" MODIFIED="1426406622054">
<node TEXT="&#x434;&#x436;" STYLE_REF="pre_last_node" ID="ID_908514267" CREATED="1426355709152" MODIFIED="1426402815373"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1055;&#1077;&#1088;&#1077;&#1076; i,e
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Gi, ge" ID="ID_1775198256" CREATED="1426355743825" MODIFIED="1426355790211"/>
</node>
<node TEXT="&#x433;" STYLE_REF="pre_last_node" ID="ID_1995196733" CREATED="1426355703179" MODIFIED="1426402815373"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1042; &#1110;&#1085;&#1096;&#1080;&#1093; &#1074;&#1080;&#1087;&#1072;&#1076;&#1082;&#1072;&#1093;
    </p>
  </body>
</html>
</richcontent>
<node TEXT="ghi=&#x433;&#x456;" ID="ID_1910438656" CREATED="1426355815544" MODIFIED="1426356295722"/>
<node TEXT="ghe=&#x433;&#x435;" ID="ID_1157749782" CREATED="1426355861568" MODIFIED="1426356309689"/>
</node>
</node>
<node TEXT="H, h" STYLE_REF="orange_node" ID="ID_285452732" CREATED="1426355928524" MODIFIED="1426406622053">
<node TEXT="&#x445;" STYLE_REF="pre_last_node" ID="ID_1699627528" CREATED="1426355942023" MODIFIED="1426402815372"/>
<node TEXT="&#x41d;&#x435; &#x447;&#x438;&#x442;&#x430;&#x454;&#x442;&#x44c;&#x441;&#x44f; &#x43f;&#x456;&#x441;&#x43b;&#x44f; c, g" STYLE_REF="pre_last_node" ID="ID_412854691" CREATED="1426355957110" MODIFIED="1426402815372"/>
</node>
<node TEXT="I, i" STYLE_REF="orange_node" ID="ID_1057659848" CREATED="1426356848486" MODIFIED="1426412020943">
<node TEXT="&#x44c;" STYLE_REF="pre_last_node" ID="ID_432956862" CREATED="1426402088068" MODIFIED="1426402815372"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1042; &#1082;&#1110;&#1085;&#1094;&#1110; &#1089;&#1083;&#1086;&#1074;&#1072; &#1074; &#1073;&#1077;&#1079;&#1091;&#1076;&#1072;&#1088;&#1085;&#1110;&#1081; &#1087;&#1086;&#1079;&#1080;&#1094;&#1110;&#1111;
    </p>
  </body>
</html>
</richcontent>
<node TEXT="vaci = [&#x432;&#x430;&#x447;&#x44c;]" ID="ID_778306215" CREATED="1426402144848" MODIFIED="1426402145789"/>
<node TEXT="&#x163;&#x103;ri = [&#x446;&#x44d;&#x440;&#x44c;]" ID="ID_1028567151" CREATED="1426402159863" MODIFIED="1426402160490"/>
</node>
<node TEXT="+&#x456; &#x432; &#x43a;&#x456;&#x43d;&#x446;&#x456; &#x441;&#x43b;&#x43e;&#x432;&#x430;" STYLE_REF="pre_last_node" ID="ID_302589195" CREATED="1426402311725" MODIFIED="1426402815372"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1052;&#1085;&#1086;&#1078;&#1080;&#1085;&#1072;
    </p>
  </body>
</html>
</richcontent>
<hook NAME="FirstGroupNode"/>
</node>
<node TEXT="+i &#x432; &#x43a;&#x456;&#x43d;&#x446;&#x456; &#x441;&#x43b;&#x43e;&#x432;&#x430;" STYLE_REF="pre_last_node" ID="ID_1694500463" CREATED="1426402273836" MODIFIED="1426402815371"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1042;&#1080;&#1079;&#1085;&#1072;&#1095;&#1077;&#1085;&#1080;&#1081; &#1072;&#1088;&#1090;&#1080;&#1082;&#1083;&#1100;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&#x41f;&#x440;&#x438;&#x43a;&#x43b;&#x430;&#x434;&#x438;" STYLE_REF="pre_last_node" ID="ID_995117763" CREATED="1426402612930" MODIFIED="1426402815371">
<hook NAME="SummaryNode"/>
<node TEXT="mini&#x15f;tru &apos;&#x43c;&#x438;&#x43d;&#x438;&#x441;&#x442;&#x440;&apos;&#xa;mini&#x15f;tri &apos;&#x43c;&#x438;&#x43d;&#x438;&#x441;&#x442;&#x440;&#x44b;&apos;&#xa;mini&#x15f;trii &apos;&#x434;&#x430;&#x43d;&#x43d;&#x44b;&#x435; &#x43c;&#x438;&#x43d;&#x438;&#x441;&#x442;&#x440;&#x44b;&apos;" ID="ID_1187251260" CREATED="1426402714626" MODIFIED="1426402929344"/>
<node TEXT="cop&#xed;l = &#x434;&#x438;&#x442;&#x438;&#x43d;&#x430;&#xa;cop&#xed;i = &#x434;&#x456;&#x442;&#x438;&#xa;cop&#xed;ii = &#x446;&#x456; &#x434;&#x456;&#x442;&#x438;" ID="ID_1244342592" CREATED="1426402723533" MODIFIED="1426402779076"/>
</node>
<node TEXT="&#x44e;" STYLE_REF="pre_last_node" ID="ID_208033916" CREATED="1426402848486" MODIFIED="1426403123934"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      IU - &#1095;&#1080;&#1090;&#1072;&#1108;&#1090;&#1100;&#1089;&#1103; [&#1102;]
    </p>
  </body>
</html>
</richcontent>
<node TEXT="iubire [&#x44e;&#x431;&#x438;&#x440;&#x435;]" ID="ID_474316626" CREATED="1426402878100" MODIFIED="1426402891755"/>
<node TEXT="oriunde = &#x43e;&#x440;&#x44e;&#x43d;&#x434;&#x435;" ID="ID_722118774" CREATED="1426402893445" MODIFIED="1426402894744"/>
</node>
</node>
<node TEXT="&#xce;, &#xee;" STYLE_REF="orange_node" ID="ID_946570195" CREATED="1426356716397" MODIFIED="1426406622051">
<node TEXT="&#x438;" STYLE_REF="pre_last_node" ID="ID_328317984" CREATED="1426356719067" MODIFIED="1426402815371"/>
</node>
<node TEXT="J, j" STYLE_REF="orange_node" ID="ID_146737215" CREATED="1426356507009" MODIFIED="1426406622050">
<node TEXT="&#x436;" STYLE_REF="pre_last_node" ID="ID_1189868424" CREATED="1426356508694" MODIFIED="1426402832864"/>
</node>
<node TEXT="K, k" STYLE_REF="orange_node" ID="ID_1823501559" CREATED="1426356438490" MODIFIED="1426406622049">
<node TEXT="&#x43a;" STYLE_REF="pre_last_node" ID="ID_232770383" CREATED="1426356440924" MODIFIED="1426402832864"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1042; &#1088;&#1091;&#1084;&#1080;&#1085;&#1089;&#1100;&#1082;&#1080;&#1093; &#1089;&#1083;&#1086;&#1074;&#1072;&#1093; &#1085;&#1077; &#1079;&#1091;&#1089;&#1090;&#1088;&#1110;&#1095;&#1072;&#1108;&#1090;&#1100;&#1089;&#1103;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="L, l" STYLE_REF="orange_node" ID="ID_1538956223" CREATED="1426356886868" MODIFIED="1426406622048">
<node TEXT="&#x43b;" STYLE_REF="pre_last_node" ID="ID_1020920648" CREATED="1426356889498" MODIFIED="1426402832863"/>
</node>
<node TEXT="M, m" STYLE_REF="orange_node" ID="ID_95785612" CREATED="1426356903717" MODIFIED="1426406622047">
<node TEXT="&#x43c;" STYLE_REF="pre_last_node" ID="ID_24716070" CREATED="1426356906475" MODIFIED="1426402832863"/>
</node>
<node TEXT="N, n" STYLE_REF="orange_node" ID="ID_1055970473" CREATED="1426356916039" MODIFIED="1426406622046">
<node TEXT="&#x43d;" STYLE_REF="pre_last_node" ID="ID_1379974087" CREATED="1426356918478" MODIFIED="1426402832863"/>
</node>
<node TEXT="O, o" STYLE_REF="orange_node" ID="ID_1288331668" CREATED="1426356926853" MODIFIED="1426406622045">
<node TEXT="&#x43e;" STYLE_REF="pre_last_node" ID="ID_1420497991" CREATED="1426356928379" MODIFIED="1426402832862"/>
<node TEXT="&#x432;&#x430;" STYLE_REF="pre_last_node" ID="ID_1991604071" CREATED="1426402975096" MODIFIED="1426403045259"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1086;&#1072; - &#1095;&#1080;&#1090;&#1072;&#1108;&#1090;&#1100;&#1089;&#1103; [&#1074;&#1072;]
    </p>
  </body>
</html>
</richcontent>
<node TEXT="soare [&#x441;&#x432;&#x430;&#x440;&#x435;] &apos;&#x441;&#x43e;&#x43d;&#x446;&#x435;&apos;" ID="ID_1882433203" CREATED="1426403023087" MODIFIED="1426403038084"/>
</node>
</node>
<node TEXT="P, p" STYLE_REF="orange_node" ID="ID_498145430" CREATED="1426356945375" MODIFIED="1426406622044">
<node TEXT="&#x43f;" STYLE_REF="pre_last_node" ID="ID_539931886" CREATED="1426356948012" MODIFIED="1426402832862"/>
</node>
<node TEXT="R, r" STYLE_REF="orange_node" ID="ID_723989928" CREATED="1426356951277" MODIFIED="1426406622043">
<node TEXT="&#x440;" STYLE_REF="pre_last_node" ID="ID_1489717139" CREATED="1426356957892" MODIFIED="1426402832861"/>
</node>
<node TEXT="S, s" STYLE_REF="orange_node" ID="ID_572433015" CREATED="1426356967762" MODIFIED="1426406622043">
<node TEXT="&#x441;" STYLE_REF="pre_last_node" ID="ID_1976800844" CREATED="1426356972699" MODIFIED="1426402832861"/>
</node>
<node TEXT="&#x15e;, &#x15f;" STYLE_REF="orange_node" ID="ID_370073248" CREATED="1426356417312" MODIFIED="1426406622042">
<node TEXT="&#x448;" STYLE_REF="pre_last_node" ID="ID_1733523132" CREATED="1426356419875" MODIFIED="1426402832861"/>
</node>
<node TEXT="T, t" STYLE_REF="orange_node" ID="ID_342249284" CREATED="1426356989346" MODIFIED="1426406622041">
<node TEXT="&#x442;" STYLE_REF="pre_last_node" ID="ID_1713883860" CREATED="1426356994288" MODIFIED="1426402832860"/>
</node>
<node TEXT="&#x162;, &#x163;" STYLE_REF="orange_node" ID="ID_1090584115" CREATED="1426356386768" MODIFIED="1426406622040">
<node TEXT="&#x446;" STYLE_REF="pre_last_node" ID="ID_465235046" CREATED="1426356389472" MODIFIED="1426402832860"/>
</node>
<node TEXT="U, u" STYLE_REF="orange_node" ID="ID_1909618444" CREATED="1426357012944" MODIFIED="1426406622040">
<node TEXT="&#x443;" STYLE_REF="pre_last_node" ID="ID_957440706" CREATED="1426357203371" MODIFIED="1426402832860"/>
</node>
<node TEXT="V, v" STYLE_REF="orange_node" ID="ID_984717387" CREATED="1426357024836" MODIFIED="1426406622039">
<node TEXT="&#x432;" STYLE_REF="pre_last_node" ID="ID_182258310" CREATED="1426357045616" MODIFIED="1426402832859"/>
</node>
<node TEXT="X, x" STYLE_REF="orange_node" ID="ID_530823483" CREATED="1426356340623" MODIFIED="1426406622038">
<node TEXT="&#x43a;&#x441;" STYLE_REF="pre_last_node" ID="ID_1369758860" CREATED="1426356342084" MODIFIED="1426402832858"/>
<node TEXT="&#x433;&#x437;" STYLE_REF="pre_last_node" ID="ID_1908303641" CREATED="1426356345873" MODIFIED="1426402832857"/>
</node>
<node TEXT="Z, z" STYLE_REF="orange_node" ID="ID_892336879" CREATED="1426357035316" MODIFIED="1426406622037">
<node TEXT="&#x437;" STYLE_REF="pre_last_node" ID="ID_1832953073" CREATED="1426357041556" MODIFIED="1426402832857"/>
</node>
</node>
<node TEXT="&#x41b;&#x456;&#x447;&#x431;&#x430;" STYLE_REF="second_node" POSITION="right" ID="ID_1598690503" CREATED="1426406802426" MODIFIED="1426406820148">
<edge COLOR="#007c7c"/>
<node TEXT="0 - 10" STYLE_REF="orange_node" ID="ID_615427176" CREATED="1426411592657" MODIFIED="1426411645487">
<node TEXT="zero" STYLE_REF="pre_last_node" ID="ID_943193222" CREATED="1426411631951" MODIFIED="1426411654711"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      0
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="unu" STYLE_REF="pre_last_node" ID="ID_1847547140" CREATED="1426411505968" MODIFIED="1426411654712"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      1
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="doi" STYLE_REF="pre_last_node" ID="ID_1343426909" CREATED="1426411486711" MODIFIED="1426411654713"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      2
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="trei" STYLE_REF="pre_last_node" ID="ID_1849005010" CREATED="1426411473176" MODIFIED="1426411654713"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      3
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="patru" STYLE_REF="pre_last_node" ID="ID_304905711" CREATED="1426411456767" MODIFIED="1426411654713"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      4
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="cinci" STYLE_REF="pre_last_node" ID="ID_469239348" CREATED="1426411441808" MODIFIED="1426411654714"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      5
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&#x219;ase" STYLE_REF="pre_last_node" ID="ID_1398394127" CREATED="1426411420521" MODIFIED="1426411654714"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      6
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&#x219;apte" STYLE_REF="pre_last_node" ID="ID_1699078149" CREATED="1426406915755" MODIFIED="1426411654715"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      7
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="opt" STYLE_REF="pre_last_node" ID="ID_1636976088" CREATED="1426406867875" MODIFIED="1426411654715"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      8
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="nou&#x103;" STYLE_REF="pre_last_node" ID="ID_153478402" CREATED="1426406857004" MODIFIED="1426411654715"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      9
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="zece" STYLE_REF="pre_last_node" ID="ID_134008587" CREATED="1426406812999" MODIFIED="1426411654716"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      10
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="11 - 19" STYLE_REF="orange_node" ID="ID_770327556" CREATED="1426411605622" MODIFIED="1426417962226"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1094;&#1080;&#1092;&#1088;&#1072;+sprezece
    </p>
  </body>
</html>
</richcontent>
<node TEXT="unsprezece" STYLE_REF="pre_last_node" ID="ID_50022526" CREATED="1426411610451" MODIFIED="1426411958455"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      11
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="doisprezece" STYLE_REF="pre_last_node" ID="ID_978195501" CREATED="1426411715880" MODIFIED="1426411958456"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      12
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="cincisprezece" STYLE_REF="pre_last_node" ID="ID_669756268" CREATED="1426411758165" MODIFIED="1426411958457"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      15
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&#x219;aisprezece" STYLE_REF="pre_last_node" ID="ID_15854802" CREATED="1426411788928" MODIFIED="1426411958457"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      16
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="nou&#x103;sprezece" STYLE_REF="pre_last_node" ID="ID_1697682362" CREATED="1426411889703" MODIFIED="1426411958457"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      19
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="20 - 99" STYLE_REF="orange_node" ID="ID_1132221546" CREATED="1426412026607" MODIFIED="1426417968290"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1094;&#1080;&#1092;&#1088;&#1072;+zeci &#537;i &#1094;&#1080;&#1092;&#1088;&#1072;
    </p>
  </body>
</html>
</richcontent>
<node TEXT="dou&#x103;zeci" STYLE_REF="pre_last_node" ID="ID_1759793673" CREATED="1426417849244" MODIFIED="1426418239829"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      20
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="dou&#x103;zeci &#x219;i patru" STYLE_REF="pre_last_node" ID="ID_1015339162" CREATED="1426417921084" MODIFIED="1426418239829"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      24
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="treizeci &#x219;i cinci" STYLE_REF="pre_last_node" ID="ID_155089323" CREATED="1426417994979" MODIFIED="1426418239826"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      35
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="patruzeci &#x219;i &#x219;ase" STYLE_REF="pre_last_node" ID="ID_684420064" CREATED="1426418036780" MODIFIED="1426418239827"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      46
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="cincizeci &#x219;i &#x219;apte" STYLE_REF="pre_last_node" ID="ID_702754255" CREATED="1426418063394" MODIFIED="1426418239827"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      57
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&#x219;aizeci &#x219;i &#x219;ase" STYLE_REF="pre_last_node" ID="ID_1025485055" CREATED="1426418115327" MODIFIED="1426418239828"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      66
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&#x219;aptezeci &#x219;i opt" STYLE_REF="pre_last_node" ID="ID_1074546118" CREATED="1426418138200" MODIFIED="1426418239828"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      78
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="optzeci &#x219;i doi" STYLE_REF="pre_last_node" ID="ID_1868491290" CREATED="1426418175053" MODIFIED="1426418239828"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      82
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="nouazeci &#x219;i unu" STYLE_REF="pre_last_node" ID="ID_555189058" CREATED="1426418208871" MODIFIED="1426419329872"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      91
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="100 - 999" STYLE_REF="orange_node" ID="ID_1518498148" CREATED="1426418287414" MODIFIED="1426418895266">
<node TEXT="una sut&#x103;" STYLE_REF="pre_last_node" ID="ID_1285971390" CREATED="1426418296756" MODIFIED="1426418911642"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      100
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="una sut&#x103; unu" STYLE_REF="pre_last_node" ID="ID_1797234513" CREATED="1426419498395" MODIFIED="1426419511612"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      101
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="dou&#x103; sute dou&#x103;zeci" STYLE_REF="pre_last_node" ID="ID_1096638268" CREATED="1426418518197" MODIFIED="1426418911643"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      220
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="trei sute" STYLE_REF="pre_last_node" ID="ID_1063861293" CREATED="1426418327988" MODIFIED="1426418911643"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      300
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="cinci sute" STYLE_REF="pre_last_node" ID="ID_825467959" CREATED="1426418369518" MODIFIED="1426418911643"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      500
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="nou&#x103; sute dou&#x103;zeci &#x219;i opt" STYLE_REF="pre_last_node" ID="ID_192368306" CREATED="1426418710572" MODIFIED="1426418911644"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      928
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="1000" OBJECT="java.lang.Long|1000" STYLE_REF="orange_node" ID="ID_1337052829" CREATED="1426418803702" MODIFIED="1426418895267">
<node TEXT="mie" STYLE_REF="pre_last_node" ID="ID_511178957" CREATED="1426418806431" MODIFIED="1426418911644"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      1000
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="mii opt sute dou&#x103;zeci &#x219;i cinci" STYLE_REF="pre_last_node" ID="ID_690473960" CREATED="1426418859087" MODIFIED="1426418911644"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      1825
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
</node>
</map>
