<map version="freeplane 1.3.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="OSI" STYLE_REF="main_node" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1419258791158" VGAP="0">
<hook NAME="AutomaticEdgeColor" COUNTER="7"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p style="text-align: center">
      <font size="4">Open System Interconnection</font>
    </p>
  </body>
</html>
</richcontent>
<hook URI="osi-model.png" SIZE="1.0" NAME="ExternalObject"/>
<hook NAME="MapStyle">
    <properties show_icon_for_attributes="true" show_note_icons="true"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
<stylenode TEXT="second_node" BACKGROUND_COLOR="#00cccc" STYLE="bubble">
<font SIZE="14"/>
</stylenode>
<stylenode TEXT="pre_last_node" BACKGROUND_COLOR="#99ff99" STYLE="bubble">
<edge STYLE="bezier" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="yellow_node" BACKGROUND_COLOR="#ffff66" STYLE="bubble"/>
<stylenode TEXT="orange_node" BACKGROUND_COLOR="#ff9900" STYLE="bubble">
<font BOLD="true"/>
</stylenode>
<stylenode TEXT="last_node" BACKGROUND_COLOR="#ffffff">
<edge STYLE="bezier" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="main_node" BACKGROUND_COLOR="#ccccff">
<font SIZE="18" BOLD="true"/>
<edge STYLE="sharp_bezier" WIDTH="4"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<node TEXT="1. &#x424;&#x438;&#x437;&#x438;&#x447;&#x435;&#x441;&#x43a;&#x438;&#x439;" STYLE_REF="second_node" POSITION="right" ID="ID_299914943" CREATED="1419012643726" MODIFIED="1419058682624" HGAP="70" VSHIFT="-40">
<edge STYLE="sharp_bezier" COLOR="#ff0000" WIDTH="4"/>
<hook URI="../../Images/atom.jpg" SIZE="1.0" NAME="ExternalObject"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      physical
    </p>
  </body>
</html>
</richcontent>
<node TEXT="&#x41e;&#x445;&#x432;&#x430;&#x442;&#x44b;&#x432;&#x430;&#x435;&#x442;" STYLE_REF="yellow_node" ID="ID_1589955571" CREATED="1419013022398" MODIFIED="1419013311487">
<node TEXT="&#x410;&#x43f;&#x430;&#x440;&#x430;&#x442;&#x43d;&#x44b;&#x435; &#x438;&#x43d;&#x442;&#x435;&#x440;&#x444;&#x435;&#x439;&#x441;&#x44b;" STYLE_REF="pre_last_node" ID="ID_507097600" CREATED="1419013069916" MODIFIED="1419013317881"/>
<node TEXT="&#x421;&#x440;&#x435;&#x434;&#x430; &#x43f;&#x435;&#x440;&#x435;&#x434;&#x430;&#x447;&#x438; &#x434;&#x430;&#x43d;&#x43d;&#x44b;&#x445;" STYLE_REF="pre_last_node" ID="ID_153000725" CREATED="1419013082562" MODIFIED="1419013317886"/>
<node TEXT="&#x421;&#x43f;&#x43e;&#x441;&#x43e;&#x431;&#x44b; &#x440;&#x430;&#x441;&#x43f;&#x43e;&#x441;&#x442;&#x440;&#x430;&#x43d;&#x435;&#x43d;&#x438;&#x44f; &#x441;&#x438;&#x433;&#x43d;&#x430;&#x43b;&#x430;" STYLE_REF="pre_last_node" ID="ID_272930753" CREATED="1419013094097" MODIFIED="1419013317889"/>
</node>
<node TEXT="&#x422;&#x443;&#x442; &#x440;&#x430;&#x431;&#x43e;&#x442;&#x430;&#x435;&#x442; &#x441;&#x435;&#x442;&#x435;&#x432;&#x43e;&#x439; &#x430;&#x434;&#x430;&#x43f;&#x442;&#x435;&#x440;" STYLE_REF="yellow_node" ID="ID_779854533" CREATED="1419013111334" MODIFIED="1419013311482"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1054;&#1085; &#1087;&#1086;&#1089;&#1088;&#1077;&#1076;&#1085;&#1080;&#1082; &#1084;&#1077;&#1078;&#1076;&#1091; &#1103;&#1076;&#1088;&#1086;&#1084; &#1080; &#1092;&#1080;&#1079; &#1085;&#1086;&#1089;&#1080;&#1090;&#1077;&#1083;&#1077;&#1084;
    </p>
  </body>
</html>
</richcontent>
<node TEXT="&#x41d;&#x430; &#x43e;&#x434;&#x43d;&#x43e;&#x43c; &#x43a;&#x43e;&#x43d;&#x446;&#x435; &#x43e;&#x43d; &#x43f;&#x440;&#x438;&#x43d;&#x438;&#x43c;&#x430;&#x435;&#x442; &#x437;&#x430;&#x43f;&#x440;&#x43e;&#x441;&#x44b; &#x43d;&#x430; &#x43f;&#x435;&#x440;&#x435;&#x434;&#x430;&#x447;&#x443;&#xa;&#x434;&#x430;&#x43d;&#x43d;&#x44b;&#x445; &#x43e;&#x442; &#x434;&#x440;&#x430;&#x439;&#x432;&#x435;&#x440;&#x430; &#x44f;&#x434;&#x440;&#x430; &#x438; &#x43f;&#x43e;&#x441;&#x44b;&#x43b;&#x430;&#x435;&#x442; &#x43f;&#x430;&#x43a;&#x435;&#x442;&#x44b; &#x441;&#x43e;&#x43e;&#x431;&#x449;&#x435;&#x43d;&#x438;&#x439;" STYLE_REF="pre_last_node" ID="ID_1490633488" CREATED="1419013176076" MODIFIED="1419013317892"/>
<node TEXT="&#x41d;&#x430; &#x434;&#x440;&#x443;&#x433;&#x43e;&#x43c; &#x43a;&#x43e;&#x43d;&#x446;&#x435; &#x43e;&#x43d; &#x43f;&#x440;&#x43e;&#x441;&#x43b;&#x443;&#x448;&#x438;&#x432;&#x430;&#x435;&#x442; &#x441;&#x435;&#x442;&#x44c; &#x43e;&#x436;&#x438;&#x434;&#x430;&#x44f; &#x441;&#x43e;&#x43e;&#x431;&#x449;&#x435;&#x43d;&#x438;&#x439;.&#xa;&#x41a;&#x43e;&#x433;&#x434;&#x430; &#x43e;&#x43d;&#x43e; &#x43f;&#x440;&#x438;&#x445;&#x43e;&#x434;&#x438;&#x442; &#x430;&#x434;&#x430;&#x43f;&#x442;&#x435;&#x440; &#x43f;&#x43e;&#x43c;&#x435;&#x449;&#x430;&#x435;&#x442; &#x435;&#x433;&#x43e; &#x432;&#x43e; &#x432;&#x43d;&#x443;&#x442;&#x440;&#x435;&#x43d;&#x43d;&#x438;&#x439;&#xa;&#x431;&#x443;&#x444;&#x435;&#x440; &#x438; &#x433;&#x435;&#x43d;&#x435;&#x440;&#x438;&#x440;&#x443;&#x435;&#x442; &#x437;&#x430;&#x43f;&#x440;&#x43e;&#x441; &#x43d;&#x430; &#x43f;&#x440;&#x435;&#x440;&#x44b;&#x432;&#x430;&#x43d;&#x438;&#x435;" STYLE_REF="pre_last_node" ID="ID_1085242741" CREATED="1419013233800" MODIFIED="1419013317905"/>
</node>
</node>
<node TEXT="2. &#x41a;&#x430;&#x43d;&#x430;&#x43b;&#x44c;&#x43d;&#x44b;&#x439;" STYLE_REF="second_node" POSITION="right" ID="ID_978916592" CREATED="1419012815401" MODIFIED="1419058624760" HGAP="90" VSHIFT="-60">
<edge STYLE="sharp_bezier" COLOR="#0000ff" WIDTH="4"/>
<hook URI="../../Images/channel.jpg" SIZE="1.0" NAME="ExternalObject"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      data link
    </p>
  </body>
</html>
</richcontent>
<node TEXT="&#x423;&#x43f;&#x440;&#x430;&#x432;&#x43b;&#x435;&#x43d;&#x438;&#x435; &#x43f;&#x435;&#x440;&#x435;&#x434;&#x430;&#x447;&#x43e;&#x439; &#x434;&#x430;&#x43d;&#x43d;&#x44b;&#x445; &#x43e;&#x442; &#x443;&#x437;&#x43b;&#x430; &#x43a; &#x443;&#x437;&#x43b;&#x443;" STYLE_REF="pre_last_node" ID="ID_517469459" CREATED="1419013326884" MODIFIED="1419013548210"/>
<node TEXT="&#x418;&#x43d;&#x442;&#x435;&#x440;&#x444;&#x435;&#x439;&#x441; &#x43c;&#x435;&#x436;&#x434;&#x443; &#x44f;&#x434;&#x440;&#x43e;&#x43c; &#x441;&#x438;&#x441;&#x442;&#x435;&#x43c;&#x44b; &#x438; &#x441;&#x435;&#x442;&#x435;&#x432;&#x44b;&#x43c;&#xa;&#x430;&#x434;&#x430;&#x43f;&#x442;&#x435;&#x440;&#x43e;&#x43c;" STYLE_REF="pre_last_node" ID="ID_195557025" CREATED="1419013347166" MODIFIED="1419013548229"/>
<node TEXT="&#x417;&#x434;&#x435;&#x441;&#x44c; &#x440;&#x430;&#x437;&#x431;&#x438;&#x432;&#x43a;&#x430; &#x441;&#x43e;&#x43e;&#x431;&#x449;&#x435;&#x43d;&#x438;&#x439; &#x43d;&#x430; &#x43a;&#x430;&#x434;&#x440;&#x44b;, &#x438;&#xa;&#x43f;&#x43e;&#x434;&#x433;&#x43e;&#x442;&#x43e;&#x432;&#x43a;&#x430; &#x43a;&#x430;&#x434;&#x440;&#x43e;&#x432; &#x43a; &#x43e;&#x442;&#x43f;&#x440;&#x430;&#x432;&#x43a;&#x435;" STYLE_REF="pre_last_node" ID="ID_1435587378" CREATED="1419013372751" MODIFIED="1419013548241"/>
<node TEXT="&#x417;&#x434;&#x435;&#x441;&#x44c; &#x440;&#x430;&#x431;&#x43e;&#x442;&#x430;&#x435;&#x442; &#x441;&#x435;&#x442;&#x435;&#x432;&#x43e;&#x439; &#x43c;&#x43e;&#x441;&#x442;" STYLE_REF="pre_last_node" ID="ID_1182896514" CREATED="1419013431157" MODIFIED="1419013548249"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1054;&#1085; &#1086;&#1073;&#1098;&#1077;&#1076;&#1080;&#1085;&#1103;&#1077;&#1090; &#1085;&#1077;&#1089;&#1082;&#1086;&#1083;&#1100;&#1082;&#1086; &#1087;&#1086;&#1076;&#1089;&#1077;&#1090;&#1077;&#1081;
    </p>
    <p>
      &#1074; &#1077;&#1076;&#1080;&#1085;&#1085;&#1091;&#1102; &#1089;&#1077;&#1090;&#1100;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="3. &#x421;&#x435;&#x442;&#x435;&#x432;&#x43e;&#x439;" STYLE_REF="second_node" POSITION="right" ID="ID_642199936" CREATED="1419012886357" MODIFIED="1419058670222" HGAP="100" VSHIFT="-110">
<edge STYLE="sharp_bezier" COLOR="#003333" WIDTH="4"/>
<hook URI="../../Images/network-neurons.jpg" SIZE="1.0" NAME="ExternalObject"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      network
    </p>
  </body>
</html>
</richcontent>
<node TEXT="&#x41f;&#x440;&#x435;&#x43e;&#x431;&#x440;&#x430;&#x437;&#x43e;&#x432;&#x430;&#x43d;&#x438;&#x435; &#x430;&#x434;&#x440;&#x435;&#x441;&#x43e;&#x432;" STYLE_REF="pre_last_node" ID="ID_628423762" CREATED="1419013594049" MODIFIED="1419017636707"/>
<node TEXT="&#x41c;&#x430;&#x440;&#x448;&#x440;&#x443;&#x442;&#x438;&#x437;&#x430;&#x446;&#x438;&#x44f;" STYLE_REF="pre_last_node" ID="ID_1662678819" CREATED="1419013604085" MODIFIED="1419017636699"/>
<node TEXT="&#x417;&#x434;&#x435;&#x441;&#x44c; &#x440;&#x430;&#x431;&#x43e;&#x442;&#x430;&#x44e;&#x442; &#x43c;&#x430;&#x440;&#x448;&#x440;&#x443;&#x442;&#x438;&#x437;&#x430;&#x442;&#x43e;&#x440;&#x44b;,&#xa;&#x43e;&#x43d;&#x438; &#x441;&#x43e;&#x435;&#x434;&#x438;&#x43d;&#x44f;&#x44e;&#x442;" STYLE_REF="pre_last_node" ID="ID_706759773" CREATED="1419013609217" MODIFIED="1419017636688">
<node TEXT="&#x413;&#x43e;&#x43c;&#x43e;&#x433;&#x435;&#x43d;&#x43d;&#x44b;&#x435; &#x441;&#x435;&#x442;&#x438;" STYLE_REF="yellow_node" ID="ID_246662418" CREATED="1419017618627" MODIFIED="1419258802221"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1054;&#1076;&#1085;&#1086;&#1088;&#1086;&#1076;&#1085;&#1099;&#1077;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&#x413;&#x435;&#x442;&#x435;&#x440;&#x43e;&#x433;&#x435;&#x43d;&#x43d;&#x44b;&#x435; &#x441;&#x435;&#x442;&#x438;" STYLE_REF="yellow_node" ID="ID_832311605" CREATED="1419017625061" MODIFIED="1419258802225"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1053;&#1077;&#1086;&#1076;&#1085;&#1086;&#1088;&#1086;&#1076;&#1085;&#1099;&#1077;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="7. &#x41f;&#x440;&#x438;&#x43a;&#x43b;&#x430;&#x434;&#x43d;&#x43e;&#x439;" STYLE_REF="second_node" POSITION="left" ID="ID_108233498" CREATED="1419012941463" MODIFIED="1419058646825" HGAP="140" VSHIFT="-10">
<edge STYLE="sharp_bezier" COLOR="#7c0000" WIDTH="4"/>
<hook URI="../../Images/sinapism.jpg" SIZE="1.0" NAME="ExternalObject"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      application
    </p>
  </body>
</html>
</richcontent>
<node TEXT="&#x421;&#x435;&#x442;&#x435;&#x432;&#x430;&#x44f; &#x444;&#x430;&#x439;&#x43b;&#x43e;&#x432;&#x430;&#x44f; &#x441;&#x438;&#x441;&#x442;&#x435;&#x43c;&#x430;" STYLE_REF="yellow_node" ID="ID_682405112" CREATED="1419020095303" MODIFIED="1419020155537"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      NFC
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&#x421;&#x435;&#x442;&#x435;&#x432;&#x44b;&#x435; &#x441;&#x435;&#x440;&#x432;&#x438;&#x441;&#x44b;" STYLE_REF="yellow_node" ID="ID_1680069857" CREATED="1419020112250" MODIFIED="1419020155524">
<node TEXT="&#x41f;&#x435;&#x440;&#x435;&#x441;&#x44b;&#x43b;&#x43a;&#x430; &#x444;&#x430;&#x439;&#x43b;&#x43e;&#x432;" STYLE_REF="pre_last_node" ID="ID_437028734" CREATED="1419020122510" MODIFIED="1419020164675"/>
<node TEXT="Email" STYLE_REF="pre_last_node" ID="ID_349850083" CREATED="1419020132601" MODIFIED="1419020164670"/>
<node TEXT="&#x421;&#x435;&#x442;&#x435;&#x432;&#x43e;&#x435; &#x443;&#x43f;&#x440;&#x430;&#x432;&#x43b;&#x435;&#x43d;&#x438;&#x435;" STYLE_REF="pre_last_node" ID="ID_1665082937" CREATED="1419020140811" MODIFIED="1419020164678"/>
<node TEXT="&#x412;&#x435;&#x431;" STYLE_REF="pre_last_node" ID="ID_511867813" CREATED="1419020169260" MODIFIED="1419020184829"/>
</node>
</node>
<node TEXT="6. &#x41f;&#x440;&#x435;&#x434;&#x441;&#x442;&#x430;&#x432;&#x438;&#x442;&#x435;&#x43b;&#x44c;&#x441;&#x43a;&#x438;&#x439;" STYLE_REF="second_node" POSITION="left" ID="ID_1916945554" CREATED="1419012929898" MODIFIED="1419058638923" HGAP="160" VSHIFT="10">
<edge STYLE="sharp_bezier" COLOR="#996600" WIDTH="4"/>
<hook URI="../../Images/lexus.jpg" SIZE="1.0" NAME="ExternalObject"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      presentation
    </p>
  </body>
</html>
</richcontent>
<node TEXT="&#x423;&#x434;&#x430;&#x43b;&#x435;&#x43d;&#x43d;&#x44b;&#x439; &#x432;&#x44b;&#x437;&#x43e;&#x432; &#x43f;&#x440;&#x43e;&#x446;&#x435;&#x434;&#x443;&#x440;" STYLE_REF="yellow_node" ID="ID_96976390" CREATED="1419019986112" MODIFIED="1419020072610"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      RPC
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&#x41e;&#x43f;&#x440;&#x435;&#x434;&#x438;&#x43b;&#x44f;&#x435;&#x442; &#x441;&#x43f;&#x43e;&#x441;&#x43e;&#x431;&#x44b;&#xa;&#x43e;&#x431;&#x440;&#x430;&#x431;&#x43e;&#x442;&#x43a;&#x438; &#x434;&#x430;&#x43d;&#x43d;&#x44b;&#x445;" STYLE_REF="yellow_node" ID="ID_1348596603" CREATED="1419020011367" MODIFIED="1419020072596">
<node TEXT="&#x428;&#x438;&#x444;&#x440;&#x43e;&#x432;&#x430;&#x43d;&#x438;&#x435;" STYLE_REF="pre_last_node" ID="ID_1948050733" CREATED="1419020038909" MODIFIED="1419020079951"/>
<node TEXT="&#x41a;&#x43e;&#x434;&#x438;&#x440;&#x43e;&#x432;&#x430;&#x43d;&#x438;&#x435;" STYLE_REF="pre_last_node" ID="ID_1171420345" CREATED="1419020048469" MODIFIED="1419020079956"/>
<node TEXT="&#x424;&#x43e;&#x440;&#x43c;&#x430;&#x442;&#x438;&#x440;&#x43e;&#x432;&#x430;&#x43d;&#x438;&#x435;" STYLE_REF="pre_last_node" ID="ID_92625122" CREATED="1419020052983" MODIFIED="1419020079959"/>
<node TEXT="&#x421;&#x436;&#x430;&#x442;&#x438;&#x435;" STYLE_REF="pre_last_node" ID="ID_1039180846" CREATED="1419020059963" MODIFIED="1419020079961"/>
</node>
</node>
<node TEXT="5. &#x421;&#x435;&#x430;&#x43d;&#x441;&#x43e;&#x432;&#x44b;&#x439;" STYLE_REF="second_node" POSITION="left" ID="ID_1034931719" CREATED="1419012915467" MODIFIED="1419058655547" HGAP="170" VSHIFT="30">
<edge STYLE="sharp_bezier" COLOR="#006666" WIDTH="4"/>
<hook URI="../../Images/procedure-1.jpg" SIZE="1.0" NAME="ExternalObject"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      session
    </p>
  </body>
</html>
</richcontent>
<node TEXT="&#x41e;&#x442;&#x432;&#x435;&#x447;&#x430;&#x435;&#x442; &#x437;&#x430; &#x432;&#x43e;&#x437;&#x43e;&#x431;&#x43d;&#x43e;&#x432;&#x43b;&#x435;&#x43d;&#x438;&#x435;&#xa;&#x43f;&#x440;&#x435;&#x440;&#x432;&#x430;&#x43d;&#x43d;&#x43e;&#x433;&#x43e; &#x441;&#x43e;&#x435;&#x434;&#x438;&#x43d;&#x435;&#x43d;&#x438;&#x44f;" STYLE_REF="pre_last_node" ID="ID_1638541609" CREATED="1419018131208" MODIFIED="1419018292514"/>
<node TEXT="&#x412;&#x432;&#x43e;&#x434;&#x438;&#x442; &#x43c;&#x435;&#x445;&#x430;&#x43d;&#x438;&#x437;&#x43c; &#x43a;&#x43e;&#x43d;&#x442;&#x440;&#x43e;&#x43b;&#x44c;&#x43d;&#x44b;&#x445; &#x442;&#x43e;&#x447;&#x435;&#x43a;" STYLE_REF="pre_last_node" ID="ID_788829269" CREATED="1419018165407" MODIFIED="1419018292501"/>
<node TEXT="&#x41f;&#x440;&#x43e;&#x446;&#x435;&#x434;&#x443;&#x440;&#x44b; &#x430;&#x443;&#x442;&#x435;&#x43d;&#x444;&#x438;&#x43a;&#x430;&#x446;&#x438;&#x438;&#xa;&#x438; &#x440;&#x435;&#x433;&#x438;&#x441;&#x442;&#x440;&#x430;&#x446;&#x438;&#x438; &#x432; &#x441;&#x438;&#x441;&#x442;&#x435;&#x43c;&#x435;" STYLE_REF="pre_last_node" ID="ID_992395420" CREATED="1419018181276" MODIFIED="1419018292488"/>
<node TEXT="&#x423;&#x43f;&#x440;&#x430;&#x432;&#x43b;&#x435;&#x43d;&#x438;&#x435; &#x43f;&#x43e;&#x442;&#x43e;&#x43a;&#x43e;&#x43c; &#x434;&#x430;&#x43d;&#x43d;&#x44b;&#x445;&#xa;&#x441; &#x43f;&#x43e;&#x43c;&#x43e;&#x449;&#x44c;&#x44e; &#x43f;&#x435;&#x440;&#x435;&#x434;&#x430;&#x447;&#x438; &#x43c;&#x430;&#x440;&#x43a;&#x435;&#x440;&#x430;" STYLE_REF="pre_last_node" ID="ID_549211398" CREATED="1419018226157" MODIFIED="1419018292478"/>
</node>
<node TEXT="4. &#x422;&#x440;&#x430;&#x43d;&#x441;&#x43f;&#x43e;&#x440;&#x442;&#x43d;&#x44b;&#x439;" STYLE_REF="second_node" POSITION="left" ID="ID_1773751419" CREATED="1419012901271" MODIFIED="1419058662969" VGAP="16" HGAP="120" VSHIFT="-90">
<edge STYLE="sharp_bezier" COLOR="#330033" WIDTH="4"/>
<hook URI="../../Images/lorry.jpg" SIZE="1.0" NAME="ExternalObject"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      transport
    </p>
  </body>
</html>
</richcontent>
<node TEXT="&#x41e;&#x441;&#x43e;&#x431;&#x435;&#x43d;&#x43d;&#x43e;&#x441;&#x442;&#x438;" STYLE_REF="yellow_node" ID="ID_760332211" CREATED="1419017702684" MODIFIED="1419018075400">
<node TEXT="&#x417;&#x434;&#x435;&#x441;&#x44c; &#x43c;&#x443;&#x43b;&#x44c;&#x442;&#x438;&#x43f;&#x43b;&#x435;&#x43a;&#x441;&#x438;&#x440;&#x43e;&#x432;&#x430;&#x43d;&#x438;&#x435;" STYLE_REF="pre_last_node" ID="ID_1784473271" CREATED="1419017753196" MODIFIED="1419018040682"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1057;&#1086;&#1077;&#1076;&#1080;&#1085;&#1077;&#1085;&#1080;&#1077; &#1082;&#1072;&#1082; &#1073;&#1091;&#1076;&#1090;&#1086; &#1080;&#1084;&#1077;&#1077;&#1090;
    </p>
    <p>
      &#1084;&#1086;&#1085;&#1086;&#1087;&#1086;&#1083;&#1100;&#1085;&#1099;&#1081; &#1076;&#1086;&#1089;&#1090;&#1091;&#1087; &#1082; &#1089;&#1077;&#1090;&#1080;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&#x420;&#x435;&#x442;&#x440;&#x430;&#x43d;&#x441;&#x43b;&#x44f;&#x446;&#x438;&#x44f;" STYLE_REF="pre_last_node" ID="ID_83162791" CREATED="1419017925300" MODIFIED="1419018040698"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1042; &#1089;&#1083;&#1091;&#1095;&#1072;&#1077;
    </p>
  </body>
</html>
</richcontent>
<node TEXT="&#x41f;&#x43e;&#x442;&#x435;&#x440;&#x438; &#x43f;&#x430;&#x43a;&#x435;&#x442;&#x430;" STYLE_REF="yellow_node" ID="ID_517441462" CREATED="1419017997645" MODIFIED="1419258812277"/>
<node TEXT="&#x41f;&#x43e;&#x432;&#x440;&#x435;&#x436;&#x434;&#x435;&#x43d;&#x438;&#x438; &#x43f;&#x430;&#x43a;&#x435;&#x442;&#x430;" STYLE_REF="yellow_node" ID="ID_895969449" CREATED="1419018017508" MODIFIED="1419258812281"/>
</node>
<node TEXT="&#x41f;&#x43e;&#x441;&#x43b;&#x435;&#x434;&#x43d;&#x438;&#x439; &#x443;&#x440;&#x43e;&#x432;&#x435;&#x43d;&#x44c; &#x43d;&#x430; &#x43a;&#x43e;&#x442;&#x43e;&#x440;&#x43e;&#x43c; &#x43f;&#x440;&#x43e;&#x432;&#x435;&#x440;&#x44f;&#x44e;&#x442;&#xa;&#x43e;&#x448;&#x438;&#x431;&#x43a;&#x438; &#x434;&#x430;&#x43d;&#x43d;&#x44b;&#x445; &#x441;&#x435;&#x430;&#x43d;&#x441;&#x430;" STYLE_REF="pre_last_node" ID="ID_270255943" CREATED="1419017935315" MODIFIED="1419018040705"/>
</node>
<node TEXT="&#x424;&#x443;&#x43d;&#x43a;&#x446;&#x438;&#x438;" STYLE_REF="yellow_node" ID="ID_552942704" CREATED="1419017720362" MODIFIED="1419018032329">
<node TEXT="&#x414;&#x43e;&#x441;&#x442;&#x430;&#x432;&#x43a;&#x430; &#x441;&#x43e;&#x43e;&#x431;&#x449;&#x435;&#x43d;&#x438;&#x439; &#x432;&#xa;&#x43d;&#x435;&#x43f;&#x43e;&#x432;&#x440;&#x435;&#x436;&#x434;&#x435;&#x43d;&#x43d;&#x43e;&#x43c; &#x432;&#x438;&#x434;&#x435;" STYLE_REF="pre_last_node" ID="ID_1578745319" CREATED="1419017822288" MODIFIED="1419018040713"/>
<node TEXT="&#x412; &#x43f;&#x440;&#x430;&#x432;&#x435;&#x43b;&#x44c;&#x43d;&#x43e;&#x43c; &#x43f;&#x43e;&#x440;&#x44f;&#x434;&#x43a;&#x435;" STYLE_REF="pre_last_node" ID="ID_156173427" CREATED="1419017845732" MODIFIED="1419018040721"/>
<node TEXT="&#x411;&#x435;&#x437; &#x434;&#x443;&#x431;&#x43b;&#x438;&#x440;&#x43e;&#x432;&#x430;&#x43d;&#x438;&#x44f;" STYLE_REF="pre_last_node" ID="ID_1082567199" CREATED="1419017854164" MODIFIED="1419018040724"/>
<node TEXT="&#x41f;&#x43e;&#x442;&#x43e;&#x43a;&#x43e;&#x432;&#x430;&#x44f; &#x43f;&#x435;&#x440;&#x435;&#x434;&#x430;&#x447;&#x430; &#x434;&#x430;&#x43d;&#x43d;&#x44b;&#x445;" STYLE_REF="pre_last_node" ID="ID_1475312356" CREATED="1419017861820" MODIFIED="1419018040726"/>
<node TEXT="&#x41e;&#x431;&#x435;&#x441;&#x43f;&#x435;&#x447;&#x435;&#x43d;&#x438;&#x435; &#x446;&#x435;&#x43b;&#x43e;&#x441;&#x442;&#x43d;&#x43e;&#x441;&#x442;&#x438; &#x43f;&#x43e;&#x442;&#x43e;&#x43a;&#x430;" STYLE_REF="pre_last_node" ID="ID_1335235923" CREATED="1419017876988" MODIFIED="1419018040729"/>
</node>
</node>
</node>
</map>
