<map version="freeplane 1.3.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="IP &#x430;&#x434;&#x440;&#x435;&#x441;&#x430;&#x446;&#x438;&#x44f;" STYLE_REF="main_node" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1419259013655">
<hook NAME="AutomaticEdgeColor" COUNTER="6"/>
<hook URI="../../Images/address-1-large.jpg" SIZE="1.0" NAME="ExternalObject"/>
<hook NAME="MapStyle">
    <properties show_icon_for_attributes="true" show_note_icons="true"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
<stylenode TEXT="second_node" BACKGROUND_COLOR="#00cccc" STYLE="bubble">
<font SIZE="14"/>
</stylenode>
<stylenode TEXT="pre_last_node" BACKGROUND_COLOR="#99ff99" STYLE="bubble">
<edge STYLE="bezier" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="yellow_node" BACKGROUND_COLOR="#ffff66" STYLE="bubble"/>
<stylenode TEXT="orange_node" BACKGROUND_COLOR="#ff9900" STYLE="bubble">
<font BOLD="true"/>
</stylenode>
<stylenode TEXT="last_node" BACKGROUND_COLOR="#ffffff">
<edge STYLE="bezier" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="main_node" BACKGROUND_COLOR="#ccccff">
<font SIZE="18" BOLD="true"/>
<edge STYLE="sharp_bezier" WIDTH="4"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<node LOCALIZED_STYLE_REF="defaultstyle.floating" POSITION="right" ID="ID_487745248" CREATED="1419061798177" MODIFIED="1419065914363" HGAP="13" VSHIFT="-320"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p style="text-align: center">
      &#1052;&#1072;&#1089;&#1082;&#1072; &#1087;&#1086;&#1076;&#1089;&#1077;&#1090;&#1080;
    </p>
  </body>
</html>
</richcontent>
<hook NAME="FreeNode"/>
<hook URI="../../Images/mask.jpg" SIZE="1.0" NAME="ExternalObject"/>
<cloud COLOR="#f0f0f0" SHAPE="ARC"/>
<font BOLD="true"/>
<richcontent TYPE="DETAILS">

<html>
  

  <head>

  </head>
  <body>
  </body>
</html>
</richcontent>
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="80" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_1608815244" MIDDLE_LABEL="&#x41f;&#x435;&#x440;&#x435;&#x43c;&#x435;&#x43d;&#x43d;&#x430;&#x44f; &#x434;&#x43b;&#x438;&#x43d;&#x430; &#x41c;&#x41f;&#xa;Variable length subnet mask" STARTINCLINATION="277;112;" ENDINCLINATION="10;-20;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="80" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_340707037" MIDDLE_LABEL="&#x424;&#x438;&#x43a;&#x441;&#x438;&#x440;&#x43e;&#x432;&#x430;&#x43d;&#x43d;&#x430;&#x44f; &#x434;&#x43b;&#x438;&#x43d;&#x430; &#x41c;&#x41f;&#xa;1, 2 &#x438;&#x43b;&#x438; 3 &#x43e;&#x43a;&#x442;&#x435;&#x442;&#x430;" STARTINCLINATION="-71;7;" ENDINCLINATION="25;-83;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
<node TEXT="Ip-&#x430;&#x434;&#x440;&#x435;&#x441;  [&#x43f;&#x43e;&#x431;&#x438;&#x442;&#x43e;&#x432;&#x43e;&#x435; AND] &#x41c;&#x41f;" STYLE_REF="pre_last_node" ID="ID_1695950437" CREATED="1419064735526" MODIFIED="1419065548409" HGAP="59" VSHIFT="-22">
<node TEXT="&#x410;&#x434;&#x440;&#x435;&#x441; &#x441;&#x435;&#x442;&#x438; / &#x43f;&#x43e;&#x434;&#x441;&#x435;&#x442;&#x438;" ID="ID_309629216" CREATED="1419065233615" MODIFIED="1419065245098"/>
<node TEXT="&#x410;&#x434;&#x440;&#x435;&#x441; &#x445;&#x43e;&#x441;&#x442;&#x430;" ID="ID_202674594" CREATED="1419065246336" MODIFIED="1419065252874"/>
</node>
<node TEXT="&#x41f;&#x440;&#x438;&#x43c;&#x435;&#x440;&#x44b;" STYLE_REF="pre_last_node" ID="ID_587766800" CREATED="1419065280814" MODIFIED="1419065552286" HGAP="66" VSHIFT="-66">
<node TEXT="192.168.1.2 AND 255.255.254.0 = 192.168.0.0" ID="ID_341503304" CREATED="1419065352339" MODIFIED="1419065529881"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Ip&#160;&#160;:&#160;&#160;&#160;&#160;11000000&#160;&#160;10101000&#160;&#160;00000001&#160;&#160;00000010
    </p>
    <p>
      &#1052;&#1055;:&#160;&#160;&#160;&#160;11111111&#160;&#160;11111111&#160;&#160;11111110&#160;&#160;00000000
    </p>
    <p>
      &#1057;&#1077;&#1090;&#1100;: 11000000&#160;&#160;10101000&#160;&#160;00000000&#160;&#160;00000000&#160;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="&#x411;&#x435;&#x441;&#x43a;&#x43b;&#x430;&#x441;&#x441;&#x43e;&#x432;&#x430;&#x44f; CIDR" STYLE_REF="second_node" POSITION="right" ID="ID_1608815244" CREATED="1419061740967" MODIFIED="1419064613160" HGAP="84" VSHIFT="79">
<edge COLOR="#ff0000"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Classless inter-domain routing
    </p>
  </body>
</html>
</richcontent>
<hook URI="../../Images/cidr.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x41c;&#x43e;&#x436;&#x435;&#x442; &#x432;&#x44b;&#x434;&#x435;&#x43b;&#x438;&#x442;&#x44c; &#x43b;&#x44e;&#x431;&#x43e;&#x439; &#x434;&#x438;&#x430;&#x43f;&#x430;&#x437;&#x43e;&#x43d; IP &#x430;&#x434;&#x440;&#x435;&#x441;&#x43e;&#x432;" STYLE_REF="yellow_node" ID="ID_1769240086" CREATED="1419061994397" MODIFIED="1419062904039"/>
<node TEXT="&#x41f;&#x440;&#x435;&#x444;&#x438;&#x43a;&#x441; - &#x44d;&#x442;&#x43e; &#x43c;&#x430;&#x441;&#x43a;&#x430; &#x43f;&#x43e;&#x434;&#x441;&#x435;&#x442;&#x438;" STYLE_REF="yellow_node" ID="ID_559950353" CREATED="1419062039582" MODIFIED="1419062904036">
<node TEXT="IP/&#x43f;&#x440;&#x435;&#x444;&#x438;&#x43a;&#x441;" STYLE_REF="pre_last_node" ID="ID_1508516818" CREATED="1419062063506" MODIFIED="1419063413613"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      192.168.1.2/16
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="&#x41f;&#x440;&#x435;&#x444;&#x438;&#x43a;&#x441;&#x44b;" STYLE_REF="yellow_node" ID="ID_473360839" CREATED="1419062803030" MODIFIED="1419062904029">
<node TEXT="/32 = &#x43a;&#x43e;&#x43d;&#x43a;&#x440;&#x435;&#x442;&#x43d;&#x44b;&#x439; IP" STYLE_REF="pre_last_node" ID="ID_151178960" CREATED="1419062809306" MODIFIED="1419062897253"/>
<node TEXT="/0 = &#x432;&#x441;&#x435; IP" STYLE_REF="pre_last_node" ID="ID_1730446922" CREATED="1419062838239" MODIFIED="1419062897250"/>
<node TEXT="/24 = &#x43a;&#x43b;&#x430;&#x441;&#x441; &#x421;" STYLE_REF="pre_last_node" ID="ID_676259933" CREATED="1419062852520" MODIFIED="1419062897248"/>
<node TEXT="/16 = &#x43a;&#x43b;&#x430;&#x441;&#x441; &#x412;" STYLE_REF="pre_last_node" ID="ID_677045894" CREATED="1419062865768" MODIFIED="1419062897246"/>
<node TEXT="/8 = &#x43a;&#x43b;&#x430;&#x441;&#x441; &#x410;" STYLE_REF="pre_last_node" ID="ID_743700754" CREATED="1419062878092" MODIFIED="1419062897238"/>
</node>
<node TEXT="&#x418;&#x435;&#x440;&#x430;&#x440;&#x445;&#x438;&#x44f;" STYLE_REF="yellow_node" ID="ID_332705806" CREATED="1419062907102" MODIFIED="1419063030160">
<node TEXT="4 &#x441;&#x43c;&#x435;&#x436;&#x43d;&#x44b;&#x445; &#x441;&#x435;&#x442;&#x438; &#x43a;&#x43b;&#x430;&#x441;&#x441;&#x430; &#x421;&#xa;&#x43e;&#x431;&#x44a;&#x435;&#x434;&#x438;&#x43d;&#x44f;&#x44e;&#x442;&#x441;&#x44f; &#x432; &#x43e;&#x434;&#x43d;&#x443; &#x441;&#x435;&#x442;&#x44c; /22" STYLE_REF="pre_last_node" ID="ID_1551843070" CREATED="1419062916082" MODIFIED="1419063019146"/>
<node TEXT="&#x421;&#x435;&#x442;&#x44c; &#x43a;&#x43b;&#x430;&#x441;&#x441;&#x430; &#x421; &#x43c;&#x43e;&#x436;&#x43d;&#x43e; &#x440;&#x430;&#x437;&#x431;&#x438;&#x442;&#x44c;&#xa;&#x43d;&#x430; &#x43c;&#x430;&#x43b;&#x435;&#x43d;&#x44c;&#x43a;&#x438;&#x435; &#x43f;&#x43e;&#x434;&#x441;&#x435;&#x442;&#x438;" STYLE_REF="pre_last_node" ID="ID_798308135" CREATED="1419062985017" MODIFIED="1419063019132"/>
</node>
<node TEXT="&#x41f;&#x440;&#x438;&#x43c;&#x435;&#x440;" STYLE_REF="yellow_node" ID="ID_1892202854" CREATED="1419063085203" MODIFIED="1419063331821"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      10.96.0.0/11 =&gt;
    </p>
    <p>
      &#1076;&#1080;&#1072;&#1087;&#1072;&#1079;&#1086;&#1085;: 10.96.0.0 - 10.127.255.255
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="&#x41a;&#x43b;&#x430;&#x441;&#x441;&#x43e;&#x432;&#x430;&#x44f;" STYLE_REF="second_node" POSITION="left" ID="ID_340707037" CREATED="1419061752576" MODIFIED="1419064615846" HGAP="74" VSHIFT="46">
<edge COLOR="#0000ff"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1055;&#1077;&#1088;&#1074;&#1099;&#1077; &#1073;&#1080;&#1090;&#1099; &#1086;&#1087;&#1088;&#1077;&#1076;&#1080;&#1083;&#1103;&#1102;&#1090;
    </p>
    <p>
      &#1082;&#1083;&#1072;&#1089;&#1089; &#1089;&#1077;&#1090;&#1080;
    </p>
  </body>
</html>
</richcontent>
<hook URI="../../Images/class-1.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x41a;&#x43b;&#x430;&#x441; &#x410;" STYLE_REF="orange_node" ID="ID_682252581" CREATED="1419062336559" MODIFIED="1419062370059">
<node TEXT="&#x41f;&#x435;&#x440;&#x432;&#x44b;&#x439; &#x431;&#x438;&#x442;: 0" STYLE_REF="pre_last_node" ID="ID_1810110768" CREATED="1419062376991" MODIFIED="1419062469405"/>
<node TEXT="C.X.X.X" STYLE_REF="pre_last_node" ID="ID_1701769247" CREATED="1419062402432" MODIFIED="1419062469402"/>
<node TEXT="128 &#x432;&#x43e;&#x437;&#x43c;&#x43e;&#x436;&#x43d;&#x44b;&#x445; &#x441;&#x435;&#x442;&#x435;&#x439;" STYLE_REF="pre_last_node" ID="ID_1852354025" CREATED="1419062414145" MODIFIED="1419062469399"/>
<node TEXT="16777216 &#x445;&#x43e;&#x441;&#x442;&#x43e;&#x432;" STYLE_REF="pre_last_node" ID="ID_439880794" CREATED="1419062432243" MODIFIED="1419062469395"/>
<node TEXT="255.0.0.0" STYLE_REF="pre_last_node" ID="ID_1805860132" CREATED="1419062452476" MODIFIED="1419062469387"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1052;&#1072;&#1089;&#1082;&#1072; &#1087;&#1086;&#1076;&#1089;&#1077;&#1090;&#1080;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="&#x412;" STYLE_REF="orange_node" ID="ID_1325285759" CREATED="1419062341934" MODIFIED="1419062370087">
<node TEXT="&#x411;&#x438;&#x442;: 10" STYLE_REF="pre_last_node" ID="ID_409568545" CREATED="1419062474472" MODIFIED="1419062558236"/>
<node TEXT="&#x421;.&#x421;.&#x425;.&#x425;" STYLE_REF="pre_last_node" ID="ID_550700667" CREATED="1419062482760" MODIFIED="1419062558232"/>
<node TEXT="16386 &#x441;&#x435;&#x442;&#x435;&#x439;" STYLE_REF="pre_last_node" ID="ID_584560201" CREATED="1419062506245" MODIFIED="1419062558227"/>
<node TEXT="65536 &#x445;&#x43e;&#x441;&#x442;&#x43e;&#x432;" STYLE_REF="pre_last_node" ID="ID_690506847" CREATED="1419062529439" MODIFIED="1419062558224"/>
<node TEXT="255.255.0.0" STYLE_REF="pre_last_node" ID="ID_1870691778" CREATED="1419062542415" MODIFIED="1419062558218"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1052;&#1055;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="&#x421;" STYLE_REF="orange_node" ID="ID_1587753622" CREATED="1419062348779" MODIFIED="1419062370090">
<node TEXT="&#x411;&#x438;&#x442;: 110" STYLE_REF="pre_last_node" ID="ID_939232685" CREATED="1419062562482" MODIFIED="1419062617273"/>
<node TEXT="&#x421;.&#x421;.&#x421;.&#x425;" STYLE_REF="pre_last_node" ID="ID_108598795" CREATED="1419062570578" MODIFIED="1419062617270"/>
<node TEXT="2097154 &#x441;&#x435;&#x442;&#x435;&#x439;" STYLE_REF="pre_last_node" ID="ID_392824334" CREATED="1419062581696" MODIFIED="1419062617266"/>
<node TEXT="256 &#x445;&#x43e;&#x441;&#x442;&#x43e;&#x432;" STYLE_REF="pre_last_node" ID="ID_457335791" CREATED="1419062596958" MODIFIED="1419062617264"/>
<node TEXT="255.255.255.0" STYLE_REF="pre_last_node" ID="ID_389029991" CREATED="1419062603943" MODIFIED="1419062617257"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1052;&#1055;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="D" STYLE_REF="orange_node" ID="ID_24334018" CREATED="1419062352546" MODIFIED="1419062370093">
<node TEXT="&#x411;&#x438;&#x442;: 1110" STYLE_REF="pre_last_node" ID="ID_273994589" CREATED="1419062622449" MODIFIED="1419062671396"/>
<node TEXT="224.0.0.0 - 239.0.0.0" STYLE_REF="pre_last_node" ID="ID_1741734461" CREATED="1419062630960" MODIFIED="1419062671393"/>
<node TEXT="Multicast" STYLE_REF="pre_last_node" ID="ID_1826374284" CREATED="1419062656813" MODIFIED="1419062671385"/>
</node>
<node TEXT="E" STYLE_REF="orange_node" ID="ID_1774152092" CREATED="1419062357928" MODIFIED="1419062370095">
<node TEXT="&#x411;&#x438;&#x442;: 11110" STYLE_REF="pre_last_node" ID="ID_1051397000" CREATED="1419062675577" MODIFIED="1419062724704"/>
<node TEXT="240.0.0.0 - 247.0.0.0" STYLE_REF="pre_last_node" ID="ID_1021051541" CREATED="1419062683788" MODIFIED="1419062724701"/>
<node TEXT="&#x417;&#x430;&#x440;&#x435;&#x437;&#x435;&#x440;&#x432;&#x438;&#x440;&#x43e;&#x432;&#x430;&#x43d;&#x43d;&#x43e;" STYLE_REF="pre_last_node" ID="ID_853179439" CREATED="1419062697687" MODIFIED="1419062724696"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1069;&#1082;&#1089;&#1087;&#1077;&#1088;&#1080;&#1084;&#1077;&#1085;&#1090;&#1072;&#1083;&#1100;&#1085;&#1086;&#1077;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
</node>
</map>
