<map version="freeplane 1.3.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="ARP" STYLE_REF="main_node" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1419259290753" VGAP="35">
<hook NAME="AutomaticEdgeColor" COUNTER="3"/>
<hook URI="arp-main.jpg" SIZE="1.0" NAME="ExternalObject"/>
<hook NAME="MapStyle">
    <properties show_icon_for_attributes="true" show_note_icons="true"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
<stylenode TEXT="second_node" BACKGROUND_COLOR="#00cccc" STYLE="bubble">
<font SIZE="14"/>
</stylenode>
<stylenode TEXT="pre_last_node" BACKGROUND_COLOR="#99ff99" STYLE="bubble">
<edge STYLE="bezier" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="yellow_node" BACKGROUND_COLOR="#ffff66" STYLE="bubble"/>
<stylenode TEXT="orange_node" BACKGROUND_COLOR="#ff9900" STYLE="bubble">
<font BOLD="true"/>
</stylenode>
<stylenode TEXT="last_node" BACKGROUND_COLOR="#ffffff">
<edge STYLE="bezier" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="main_node" BACKGROUND_COLOR="#ccccff">
<font SIZE="18" BOLD="true"/>
<edge STYLE="sharp_bezier" WIDTH="4"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<edge COLOR="#ccff33"/>
<node TEXT="&#x420;&#x435;&#x430;&#x43b;&#x438;&#x437;&#x430;&#x446;&#x438;&#x44f; ARP" STYLE_REF="second_node" POSITION="left" ID="ID_1891644432" CREATED="1419003222150" MODIFIED="1419259296943" HGAP="80" VSHIFT="-20">
<icon BUILTIN="executable"/>
<edge STYLE="sharp_bezier" COLOR="#660000" WIDTH="4"/>
<node TEXT="&#x41e;&#x43f;&#x440;&#x435;&#x434;&#x438;&#x43b;&#x435;&#x43d;&#x438;&#x435; &#x444;&#x438;&#x437; &#x430;&#x434;&#x440;&#x435;&#x441;&#x430;&#xa;&#x43f;&#x440;&#x438; &#x43f;&#x43e;&#x441;&#x44b;&#x43b;&#x43a;&#x435; &#x43f;&#x430;&#x43a;&#x435;&#x442;&#x430;" STYLE_REF="orange_node" ID="ID_547328779" CREATED="1419003370725" MODIFIED="1419259326280" VSHIFT="-10">
<font BOLD="false"/>
<node TEXT="1. &#x41a;&#x435;&#x448;" STYLE_REF="pre_last_node" ID="ID_1655113981" CREATED="1419003887018" MODIFIED="1419005348927"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1059;&#1076;&#1072;&#1083;&#1103;&#1077;&#1090;&#1089;&#1103; &#1087;&#1086; &#1080;&#1089;&#1090;&#1077;&#1095;&#1077;&#1085;&#1080;&#1102;
    </p>
    <p>
      &#1074;&#1088;&#1077;&#1084;&#1077;&#1085;&#1080; &#1078;&#1080;&#1079;&#1085;&#1080;
    </p>
  </body>
</html>
</richcontent>
<font BOLD="false"/>
</node>
<node TEXT="2. &#x428;&#x438;&#x440;&#x43e;&#x43a;&#x43e;&#x432;&#x435;&#x449;&#x430;&#x43d;&#x438;&#x435;" STYLE_REF="pre_last_node" ID="ID_47182089" CREATED="1419003892947" MODIFIED="1419005348919">
<font BOLD="false"/>
<node TEXT="&#x41d;&#x443;&#x436;&#x43d;&#x44b;&#x439; &#x41f;&#x43a; &#x43c;&#x43e;&#x436;&#x435;&#x442; &#x431;&#x44b;&#x442;&#x44c; &#x432;&#x44b;&#x43a;&#x43b;&#x44e;&#x447;&#x435;&#x43d;" STYLE_REF="yellow_node" ID="ID_121220841" CREATED="1419003901226" MODIFIED="1419259331569"/>
<node TEXT="&#x41e;&#x442;&#x432;&#x435;&#x442; &#x43c;&#x43e;&#x436;&#x435;&#x442; &#x431;&#x44b;&#x442;&#x44c; &#x43f;&#x43e;&#x442;&#x435;&#x440;&#x44f;&#x43d;" STYLE_REF="yellow_node" ID="ID_213184959" CREATED="1419003917791" MODIFIED="1419259331572"/>
</node>
</node>
<node TEXT="&#x41e;&#x442;&#x432;&#x435;&#x447;&#x430;&#x435;&#x442; &#x43d;&#x430; &#x437;&#x430;&#x43f;&#x440;&#x43e;&#x441;&#x44b; &#x43e;&#x442; &#x434;&#x440;&#x443;&#x433;&#x438;&#x445; &#x41f;&#x41a;" STYLE_REF="orange_node" ID="ID_1063413752" CREATED="1419003501132" MODIFIED="1419259326287">
<font BOLD="false"/>
<node TEXT="IF Ip &#x43d;&#x430;&#x448;, &#x442;&#x43e;&#x433;&#x434;&#x430; &#x43e;&#x442;&#x43f;&#x440;&#x430;&#x432;&#x438;&#x442;&#x44c;&#xa;&#x444;&#x438;&#x437; &#x430;&#x434;&#x440;&#x435;&#x441; &#x43e;&#x442;&#x43f;&#x440;&#x430;&#x432;&#x438;&#x442;&#x435;&#x43b;&#x44e;" STYLE_REF="pre_last_node" ID="ID_1996382326" CREATED="1419003927378" MODIFIED="1419005357015"/>
<node TEXT="IF Ip &#x43d;&#x435; &#x43d;&#x430;&#x448;, &#x442;&#x43e;&#x433;&#x434;&#x430; &#x438;&#x433;&#x43d;&#x43e;&#x440;&#x438;&#x440;&#x43e;&#x432;&#x430;&#x442;&#x44c;" STYLE_REF="pre_last_node" ID="ID_789907373" CREATED="1419004038546" MODIFIED="1419005357007"/>
</node>
</node>
<node TEXT="&#x420;&#x430;&#x437;&#x440;&#x435;&#x448;&#x435;&#x43d;&#x438;&#x435; &#x430;&#x434;&#x440;&#x435;&#x441;&#x43e;&#x432;" STYLE_REF="second_node" POSITION="right" ID="ID_429498147" CREATED="1419003259343" MODIFIED="1419259302078" VGAP="9" HGAP="50" VSHIFT="-30">
<edge COLOR="#003366"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1054;&#1090;&#1086;&#1073;&#1088;&#1072;&#1078;&#1077;&#1085;&#1080;&#1077; &#1074;&#1099;&#1089;&#1086;&#1082;&#1086; &#1091;&#1088;&#1086;&#1074;&#1085;&#1077;&#1074;&#1099;&#1093;
    </p>
    <p>
      &#1072;&#1076;&#1088;&#1077;&#1089;&#1086;&#1074; &#1091; &#1092;&#1080;&#1079; &#1072;&#1076;&#1088;&#1077;&#1089;&#1072;
    </p>
  </body>
</html>
</richcontent>
<node TEXT="&#x421; &#x43f;&#x43e;&#x43c;&#x43e;&#x449;&#x44c;&#x44e; &#x43f;&#x440;&#x44f;&#x43c;&#x43e;&#x433;&#x43e;&#xa;&#x43e;&#x442;&#x43e;&#x431;&#x440;&#x430;&#x436;&#x435;&#x43d;&#x438;&#x44f;" STYLE_REF="orange_node" ID="ID_1849485743" CREATED="1419004125278" MODIFIED="1419004802329">
<font BOLD="false"/>
<node TEXT="&#x412; Ip &#x437;&#x430;&#x448;&#x438;&#x442;&#x430; &#x447;&#x430;&#x441;&#x442;&#x44c; &#x444;&#x438;&#x437; &#x430;&#x434;&#x440;&#x435;&#x441;&#x430;" STYLE_REF="pre_last_node" ID="ID_1358899062" CREATED="1419004177738" MODIFIED="1419004221408"/>
<node TEXT="&#x444;&#x438;&#x437; &#x430;&#x434;&#x440;&#x435;&#x441; = F(Ip)" STYLE_REF="pre_last_node" ID="ID_1341843788" CREATED="1419004193559" MODIFIED="1419004221416"/>
<node TEXT="&#x41f;&#x440;&#x438;&#x43c;&#x435;&#x43d;&#x44f;&#x435;&#x442;&#x441;&#x44f; &#x432; ProNET" STYLE_REF="pre_last_node" ID="ID_65167293" CREATED="1419004206014" MODIFIED="1419004221420"/>
</node>
<node TEXT="&#x421; &#x43f;&#x43e;&#x43c;&#x43e;&#x449;&#x44c;&#x44e; &#x434;&#x438;&#x43d;&#x430;&#x43c;&#x438;&#x447;&#x435;&#x441;&#x43a;&#x43e;&#x433;&#x43e;&#xa;&#x441;&#x432;&#x44f;&#x437;&#x438;&#x432;&#x430;&#x43d;&#x438;&#x44f;" STYLE_REF="orange_node" ID="ID_1231035908" CREATED="1419004143862" MODIFIED="1419004802348">
<font BOLD="false"/>
<node TEXT="&#x41f;&#x440;&#x438;&#x43c;&#x435;&#x43d;&#x44f;&#x435;&#x442;&#x441;&#x44f; &#x432; Ethernet" STYLE_REF="pre_last_node" ID="ID_768592682" CREATED="1419004230465" MODIFIED="1419004749504"/>
<node TEXT="ARP" STYLE_REF="pre_last_node" ID="ID_1658651541" CREATED="1419004244054" MODIFIED="1419004749513"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Address resolution protocol
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&#x421;&#x443;&#x442;&#x44c;" STYLE_REF="pre_last_node" ID="ID_671061084" CREATED="1419004265405" MODIFIED="1419004749519"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1061; &#1093;&#1086;&#1095;&#1077;&#1090; &#1088;&#1072;&#1079;&#1088;&#1077;&#1096;&#1080;&#1090;&#1100; &#1072;&#1076;&#1088;&#1077;&#1089; IP1, &#1086;&#1085; &#1096;&#1080;&#1088;&#1086;&#1082;&#1086;&#1074;&#1077;&#1097;&#1072;&#1090;&#1077;&#1083;&#1100;&#1085;&#1086;
    </p>
    <p>
      &#1088;&#1072;&#1089;&#1087;&#1086;&#1089;&#1090;&#1088;&#1072;&#1085;&#1103;&#1077;&#1090; &#1089;&#1087;&#1077;&#1094;&#1080;&#1072;&#1083;&#1100;&#1085;&#1099;&#1081; &#1087;&#1072;&#1082;&#1077;&#1090;. &#1042; &#1082;&#1086;&#1090;&#1086;&#1088;&#1086;&#1084; &#1087;&#1088;&#1086;&#1089;&#1080;&#1090;
    </p>
    <p>
      Y &#1089; IP1 &#1086;&#1090;&#1074;&#1077;&#1090;&#1080;&#1090;&#1100; &#1091;&#1082;&#1072;&#1079;&#1072;&#1074; &#1077;&#1084;&#1091; &#1089;&#1074;&#1086;&#1081; &#1092;&#1080;&#1079; &#1072;&#1076;&#1088;&#1077;&#1089; F1.
    </p>
    <p>
      &#1042;&#1089;&#1077; &#1055;&#1050; &#1087;&#1086;&#1083;&#1091;&#1095;&#1072;&#1102;&#1090; &#1079;&#1072;&#1087;&#1088;&#1086;&#1089;, &#1085;&#1086; &#1090;&#1086;&#1083;&#1100;&#1082;&#1086; Y &#1086;&#1090;&#1074;&#1077;&#1095;&#1072;&#1077;&#1090;.
    </p>
    <p>
      &#1055;&#1086;&#1089;&#1083;&#1077; &#1101;&#1090;&#1086;&#1075;&#1086; X &#1087;&#1086;&#1089;&#1080;&#1083;&#1072;&#1077;&#1090; &#1087;&#1072;&#1082;&#1077;&#1090; &#1080;&#1089;&#1087;&#1086;&#1083;&#1100;&#1079;&#1091;&#1103; &#1092;&#1080;&#1079; &#1072;&#1076;&#1088;&#1077;&#1089;
    </p>
    <p>
      &#1087;&#1088;&#1103;&#1084;&#1086; &#1082; Y
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&#x41a;&#x44d;&#x448;" STYLE_REF="pre_last_node" ID="ID_607751977" CREATED="1419004457368" MODIFIED="1419004785669"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1050;&#1072;&#1078;&#1076;&#1099;&#1081; &#1055;&#1050; &#1080;&#1084;&#1077;&#1077;&#1090; &#1082;&#1101;&#1096; &#1088;&#1072;&#1079;&#1088;&#1077;&#1096;&#1086;&#1085;&#1085;&#1099;&#1093; &#1072;&#1076;&#1088;&#1077;&#1089;&#1086;&#1074;
    </p>
    <p>
      &#1080; &#1079;&#1072;&#1075;&#1083;&#1103;&#1076;&#1099;&#1074;&#1072;&#1077;&#1090; &#1090;&#1091;&#1076;&#1072; &#1087;&#1077;&#1088;&#1077;&#1076; &#1096;&#1080;&#1088;&#1086;&#1082;&#1086;&#1074;&#1077;&#1097;&#1072;&#1085;&#1080;&#1077;&#1084;
    </p>
  </body>
</html>
</richcontent>
<node TEXT="&#x414;&#x438;&#x43d;&#x430;&#x43c;&#x438;&#x447;&#x435;&#x441;&#x43a;&#x438;&#x435; &#x437;&#x430;&#x43f;&#x438;&#x441;&#x438;" STYLE_REF="yellow_node" ID="ID_1486611802" CREATED="1419004624861" MODIFIED="1419004808317">
<font BOLD="false"/>
<node TEXT="&#x41e;&#x441;&#x442;&#x430;&#x44e;&#x442;&#x441;&#x44f; &#x432; &#x43a;&#x44d;&#x448;&#x435; &#x43d;&#x430; 2 &#x43c;&#x438;&#x43d;" ID="ID_1344290861" CREATED="1419004671392" MODIFIED="1419004712741"/>
<node TEXT="&#x415;&#x441;&#x43b;&#x438; &#x43f;&#x440;&#x43e;&#x438;&#x437;&#x43e;&#x448;&#x43b;&#x430; &#x43f;&#x43e;&#x432;&#x442;&#x43e;&#x440;&#x43d;&#x430;&#x44f; &#x43f;&#x435;&#x440;&#x435;&#x434;&#x430;&#x447;&#x430;&#xa;&#x445;&#x440;&#x430;&#x43d;&#x435;&#x43d;&#x438;&#x435; &#x43f;&#x440;&#x43e;&#x434;&#x43b;&#x435;&#x432;&#x430;&#x435;&#x442;&#x441;&#x44f; &#x435;&#x449;&#x435; &#x43d;&#x430; 2 &#x43c;&#x438;&#x43d;" ID="ID_1749071121" CREATED="1419004713975" MODIFIED="1419004738690"/>
</node>
<node TEXT="&#x421;&#x442;&#x430;&#x442;&#x438;&#x447;&#x435;&#x441;&#x43a;&#x438;&#x435; &#x437;&#x430;&#x43f;&#x438;&#x441;&#x438;" STYLE_REF="yellow_node" ID="ID_1895011580" CREATED="1419004637856" MODIFIED="1419004808326"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      arp -s &lt;Ip-addr&gt; &lt;fiz-addr&gt;
    </p>
  </body>
</html>
</richcontent>
<font BOLD="false"/>
</node>
</node>
<node TEXT="&#x423;&#x442;&#x43e;&#x447;&#x43d;&#x435;&#x43d;&#x438;&#x435; ARP" STYLE_REF="pre_last_node" ID="ID_1050245304" CREATED="1419004495980" MODIFIED="1419004749524">
<node TEXT="&#x41f;&#x440;&#x438; &#x43f;&#x43e;&#x441;&#x438;&#x43b;&#x43a;&#x435; &#x448;&#x438;&#x440;&#x43e;&#x43a;&#x43e;&#x432;&#x435;&#x449;&#x430;&#x43d;&#x438;&#x44f; &#x425; &#x432;&#x43a;&#x43b;&#x44e;&#x447;&#x430;&#x435;&#x442;&#xa;&#x432; &#x437;&#x430;&#x43f;&#x440;&#x43e;&#x441; &#x441;&#x432;&#x43e;&#x439; Ip &#x438; &#x444;&#x438;&#x437; &#x430;&#x434;&#x440;&#x435;&#x441;&#x430;" STYLE_REF="yellow_node" ID="ID_1093527191" CREATED="1419004824188" MODIFIED="1419004953052">
<node TEXT="Y &#x441;&#x43e;&#x445;&#x440;&#x430;&#x43d;&#x44f;&#x435;&#x442; &#x44d;&#x442;&#x43e; &#x441;&#x43e;&#x43e;&#x442;&#x432;&#x435;&#x442;&#x441;&#x442;&#x432;&#x438;&#x435;" ID="ID_1426931877" CREATED="1419004908066" MODIFIED="1419004927630"/>
<node TEXT="&#x41a;&#x430;&#x436;&#x434;&#x44b;&#x439; &#x41f;&#x41a; &#x432; &#x441;&#x435;&#x442;&#x438; &#x441;&#x43e;&#x445;&#x440;&#x430;&#x43d;&#x44f;&#x435;&#x442; &#x44d;&#x442;&#x43e; &#x441;&#x43e;&#x43e;&#x442;&#x432;&#x435;&#x442;&#x441;&#x442;&#x432;&#x438;&#x435;" ID="ID_484418055" CREATED="1419004928706" MODIFIED="1419004946752"/>
</node>
<node TEXT="&#x41a;&#x43e;&#x433;&#x434;&#x430; &#x43d;&#x43e;&#x432;&#x44b;&#x439; &#x41f;&#x41a; &#x43f;&#x43e;&#x44f;&#x432;&#x43b;&#x44f;&#x435;&#x442;&#x441;&#x44f; &#x432; &#x441;&#x435;&#x442;&#x438; &#x43f;&#x440;&#x438; &#x437;&#x430;&#x433;&#x440;&#x443;&#x437;&#x43a;&#x435; &#x41e;&#x421;&#xa;&#x43e;&#x43d; &#x448;&#x438;&#x440;&#x43e;&#x43a;&#x43e;&#x432;&#x435;&#x449;&#x430;&#x442;&#x435;&#x43b;&#x44c;&#x43d;&#x43e; &#x440;&#x430;&#x441;&#x43f;&#x43e;&#x441;&#x442;&#x440;&#x430;&#x43d;&#x44f;&#x435;&#x442; &#x441;&#x432;&#x43e;&#x44e; &#x441;&#x432;&#x44f;&#x437;&#x43a;&#x443; Ip-&#x444;&#x438;&#x437;" STYLE_REF="yellow_node" ID="ID_1382862001" CREATED="1419004862007" MODIFIED="1419004953071"/>
</node>
<node TEXT="&#x421;&#x430;&#x43c;&#x43e;&#x43e;&#x431;&#x43e;&#x431;&#x449;&#x435;&#x43d;&#x43d;&#x44b;&#x439; &#x437;&#x430;&#x43f;&#x440;&#x43e;&#x441;" STYLE_REF="pre_last_node" ID="ID_1619187094" CREATED="1419004526824" MODIFIED="1419004764239"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1053;&#1091;&#1078;&#1077;&#1085; &#1076;&#1083;&#1103; &#1089;&#1090;&#1072;&#1088;&#1090;&#1086;&#1074;&#1086;&#1081; &#1082;&#1086;&#1085;&#1092;&#1080;&#1075;&#1091;&#1088;&#1072;&#1094;&#1080;&#1080;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="&#x421;&#x442;&#x440;&#x443;&#x43a;&#x442;&#x443;&#x440;&#x430; &#x444;&#x440;&#x435;&#x439;&#x43c;&#x430;" STYLE_REF="second_node" POSITION="right" ID="ID_849491471" CREATED="1419003285159" MODIFIED="1419259306927" HGAP="100" VSHIFT="-10">
<edge COLOR="#996600"/>
<hook URI="arp-frame.jpg" SIZE="1.0" NAME="ExternalObject"/>
</node>
</node>
</map>
