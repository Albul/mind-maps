<map version="freeplane 1.3.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Ip &#x430;&#x434;&#x440;&#x435;&#x441;" STYLE_REF="main_node" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1419259102466">
<hook NAME="AutomaticEdgeColor" COUNTER="2"/>
<hook NAME="MapStyle">
    <properties show_icon_for_attributes="true" show_note_icons="true"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
<stylenode TEXT="second_node" BACKGROUND_COLOR="#00cccc" STYLE="bubble">
<font SIZE="14"/>
</stylenode>
<stylenode TEXT="pre_last_node" BACKGROUND_COLOR="#99ff99" STYLE="bubble">
<edge STYLE="bezier" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="yellow_node" BACKGROUND_COLOR="#ffff66" STYLE="bubble"/>
<stylenode TEXT="orange_node" BACKGROUND_COLOR="#ff9900" STYLE="bubble">
<font BOLD="true"/>
</stylenode>
<stylenode TEXT="last_node" BACKGROUND_COLOR="#ffffff">
<edge STYLE="bezier" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="main_node" BACKGROUND_COLOR="#ccccff">
<font SIZE="18" BOLD="true"/>
<edge STYLE="sharp_bezier" WIDTH="4"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook URI="../../Images/address-1.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Ipv4" STYLE_REF="second_node" POSITION="right" ID="ID_1586126472" CREATED="1418845387853" MODIFIED="1419259081461">
<edge STYLE="sharp_bezier" COLOR="#003366" WIDTH="4"/>
<node TEXT="&#x427;&#x430;&#x441;&#x442;&#x43d;&#x44b;&#x435; IP &#x430;&#x434;&#x440;&#x435;&#x441;&#x430;" STYLE_REF="orange_node" ID="ID_607280230" CREATED="1418845489636" MODIFIED="1419259219274">
<icon BUILTIN="gohome"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1048;&#1093; &#1085;&#1077;&#1090;&#1091; &#1074; &#1075;&#1083;&#1086;&#1073;&#1072;&#1083;&#1100;&#1085;&#1086;&#1081; &#1089;&#1077;&#1090;&#1080;.
    </p>
    <p>
      &#1048;&#1093; &#1074;&#1099;&#1076;&#1077;&#1083;&#1080;&#1083;&#1080; &#1083;&#1086;&#1082;&#1072;&#1083;&#1100;&#1085;&#1099;&#1084; &#1089;&#1077;&#1090;&#1103;&#1084;
    </p>
  </body>
</html>
</richcontent>
<node TEXT="192.168/16" STYLE_REF="pre_last_node" ID="ID_88694359" CREATED="1418845545734" MODIFIED="1418847410316"/>
<node TEXT="172.16/12" STYLE_REF="pre_last_node" ID="ID_316195246" CREATED="1418845563762" MODIFIED="1418847410333"/>
<node TEXT="10/8/14 12:00 AM" OBJECT="org.freeplane.features.format.FormattedDate|2014-10-08T00:00+0300|datetime" STYLE_REF="pre_last_node" ID="ID_1875369206" CREATED="1418845579912" MODIFIED="1418847410343"/>
<node TEXT="127/8" STYLE_REF="pre_last_node" ID="ID_175913168" CREATED="1418845589262" MODIFIED="1418847410351"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      localhost
    </p>
  </body>
</html>
</richcontent>
<node TEXT="lo &#x432; Linux" STYLE_REF="yellow_node" ID="ID_1625546372" CREATED="1418884289736" MODIFIED="1419259125400"/>
<node TEXT="127.0.0.1 - &#x447;&#x430;&#x441;&#x442;&#x43e;" STYLE_REF="yellow_node" ID="ID_1885240122" CREATED="1418884303342" MODIFIED="1419259125405"/>
<node TEXT="&#x41d;&#x435; &#x442;&#x440;&#x435;&#x431;&#x443;&#x435;&#x442; &#x441;&#x435;&#x442;&#x435;&#x432;&#x43e;&#x439; &#x43a;&#x430;&#x440;&#x442;&#x44b;" STYLE_REF="yellow_node" ID="ID_576351501" CREATED="1418884320116" MODIFIED="1419259125408"/>
<node TEXT="&#x420;&#x430;&#x431;&#x43e;&#x442;&#x430; &#x43d;&#x430; &#x443;&#x440;&#x43e;&#x432;&#x43d;&#x435; &#x44f;&#x434;&#x440;&#x430;" STYLE_REF="yellow_node" ID="ID_1590408258" CREATED="1418884335600" MODIFIED="1419259125411"/>
<node TEXT="&#x414;&#x43b;&#x44f; &#x43c;&#x435;&#x436;&#x43f;&#x440;&#x43e;&#x446;&#x435;&#x441;&#x43d;&#x43e;&#x433;&#x43e; &#x432;&#x437;&#x430;&#x438;&#x43c;&#x43e;&#x434;&#x435;&#x439;&#x441;&#x442;&#x432;&#x438;&#x44f;" STYLE_REF="yellow_node" ID="ID_1755106697" CREATED="1418884345379" MODIFIED="1419259125412"/>
</node>
</node>
<node TEXT="&#x41e;&#x441;&#x43e;&#x431;&#x435;&#x43d;&#x43d;&#x43e;&#x441;&#x442;&#x438; &#x430;&#x434;&#x440;&#x435;&#x441;&#x430;" STYLE_REF="orange_node" ID="ID_1152790736" CREATED="1418847438139" MODIFIED="1419259219271">
<icon BUILTIN="info"/>
<node TEXT="32 &#x431;&#x438;&#x442;&#x43e;&#x432;&#x43e;&#x435; / 4 &#x431;&#x430;&#x439;&#x442;&#x43e;&#x432;&#x43e;&#x435; &#x447;&#x438;&#x441;&#x43b;&#x43e;" STYLE_REF="pre_last_node" ID="ID_83010775" CREATED="1418847449015" MODIFIED="1418885203355"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      192.169.20.60
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="&#x41e;&#x441;&#x43e;&#x431;&#x44b;&#x435; IP-&#x430;&#x434;&#x440;&#x435;&#x441;&#x430;" STYLE_REF="orange_node" ID="ID_279940003" CREATED="1418884398920" MODIFIED="1419259219273">
<icon BUILTIN="bookmark"/>
<node TEXT="255.255.255.255" STYLE_REF="pre_last_node" ID="ID_617332125" CREATED="1418884418170" MODIFIED="1418884547497"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1054;&#1075;&#1088;&#1072;&#1085;&#1080;&#1095;&#1077;&#1085;&#1086;&#1077; &#1074;&#1077;&#1097;&#1072;&#1085;&#1080;&#1077;
    </p>
  </body>
</html>
</richcontent>
<node TEXT="&#x414;&#x430;&#x442;&#x430;&#x433;&#x440;&#x430;&#x43c;&#x44b; &#x43d;&#x435; &#x443;&#x445;&#x43e;&#x434;&#x44f;&#x442; &#x434;&#x430;&#x43b;&#x44c;&#x448;&#x435; &#x43c;&#x430;&#x440;&#x448;&#x440;&#x443;&#x442;&#x438;&#x437;&#x430;&#x442;&#x43e;&#x440;&#x430;" STYLE_REF="yellow_node" ID="ID_290525375" CREATED="1418884656382" MODIFIED="1419259125413"/>
</node>
<node TEXT="&#x421;.&#x421;.&#x421;.255 / &#x421;.&#x421;.255.255" STYLE_REF="pre_last_node" ID="ID_1911554464" CREATED="1418884464092" MODIFIED="1418884547485"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1042;&#1077;&#1097;&#1072;&#1085;&#1080;&#1077; &#1085;&#1072; &#1089;&#1077;&#1090;&#1100;
    </p>
  </body>
</html>
</richcontent>
<node TEXT="&#x41c;&#x43e;&#x436;&#x435;&#x442; &#x431;&#x44b;&#x442;&#x44c; &#x43e;&#x442;&#x43a;&#x43b;&#x44e;&#x447;&#x435;&#x43d;&#x43e;.&#xa;&#x418;&#x437; &#x437;&#x430; &#x441;&#x442;&#x440;&#x430;&#x445;&#x430; &#x43e;&#x442;&#x43a;&#x430;&#x437;&#x430; &#x432; &#x43e;&#x431;&#x441;&#x43b;&#x443;&#x436;&#x438;&#x432;&#x430;&#x43d;&#x438;&#x438;" STYLE_REF="yellow_node" ID="ID_1567436806" CREATED="1418884625177" MODIFIED="1419259125414"/>
</node>
<node TEXT="&#x421;.&#x421;.&#x41f;&#x421;.255 / &#x421;.&#x41f;&#x421;.&#x41f;&#x421;.255" STYLE_REF="pre_last_node" ID="ID_828360890" CREATED="1418884506576" MODIFIED="1418884547473"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1042;&#1077;&#1097;&#1072;&#1085;&#1080;&#1077; &#1085;&#1072; &#1087;&#1086;&#1076;&#1089;&#1077;&#1090;&#1100;
    </p>
  </body>
</html>
</richcontent>
<node TEXT="&#x41d;&#x443;&#x436;&#x43d;&#x430; &#x43c;&#x430;&#x441;&#x43a;&#x430; &#x43f;&#x43e;&#x434;&#x441;&#x435;&#x442;&#x438;" STYLE_REF="yellow_node" ID="ID_603905860" CREATED="1418884613186" MODIFIED="1419259125417"/>
</node>
<node TEXT="&#x421;.&#x421;.255.255" STYLE_REF="pre_last_node" ID="ID_1008027086" CREATED="1418884562740" MODIFIED="1418884604140"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1042;&#1077;&#1097;&#1072;&#1085;&#1080;&#1077; &#1085;&#1072; &#1074;&#1089;&#1077; &#1087;&#1086;&#1076;&#1089;&#1077;&#1090;&#1080;
    </p>
  </body>
</html>
</richcontent>
<node TEXT="&#x41e;&#x442;&#x43f;&#x440;&#x430;&#x432;&#x43b;&#x435;&#x43d;&#x43e; &#x43d;&#x430; &#x441;&#x432;&#x430;&#x43b;&#x43a;&#x443;" STYLE_REF="yellow_node" ID="ID_88257236" CREATED="1418884605561" MODIFIED="1419259125421"/>
</node>
</node>
</node>
<node TEXT="Ipv6" STYLE_REF="second_node" POSITION="left" ID="ID_1506112417" CREATED="1418845399850" MODIFIED="1418845470944">
<edge STYLE="sharp_bezier" COLOR="#660000" WIDTH="4"/>
<node TEXT="&#x427;&#x430;&#x441;&#x442;&#x43d;&#x44b;&#x435; I&#x420; &#x430;&#x434;&#x440;&#x435;&#x441;&#x430;" STYLE_REF="orange_node" ID="ID_1547292249" CREATED="1418884762132" MODIFIED="1419259219268">
<icon BUILTIN="gohome"/>
<node TEXT="localhost" STYLE_REF="pre_last_node" ID="ID_1786405140" CREATED="1418884771788" MODIFIED="1418884811502"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      ::1/128
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="fcoo::/7" STYLE_REF="pre_last_node" ID="ID_1877898593" CREATED="1418884791979" MODIFIED="1418884811516"/>
</node>
<node TEXT="&#x41e;&#x441;&#x43e;&#x431;&#x435;&#x43d;&#x43d;&#x43e;&#x441;&#x442;&#x438; &#x430;&#x434;&#x440;&#x435;&#x441;&#x430;" STYLE_REF="orange_node" ID="ID_1505417814" CREATED="1418884820083" MODIFIED="1419259205511">
<icon BUILTIN="info"/>
<node TEXT="128 &#x431;&#x438;&#x442;&#x43d;&#x43e;&#x435; / 16 &#x431;&#x430;&#x439;&#x442;&#x43d;&#x43e;&#x435; &#x447;&#x438;&#x441;&#x43b;&#x43e;" STYLE_REF="pre_last_node" ID="ID_905187110" CREATED="1418884844460" MODIFIED="1418885229058"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      2001:0db8:11a3:09d7:1f34:8a2e:07a0:765d
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="8 &#x433;&#x440;&#x443;&#x43f; &#x43f;&#x43e; 4 &#x448;&#x435;&#x441;&#x442;&#x43d;&#x430;&#x434;&#x446;&#x430;&#x442;&#x435;&#x440;&#x438;&#x447;&#x43d;&#x44b;&#x445; &#x447;&#x438;&#x441;&#x435;&#x43b;" STYLE_REF="pre_last_node" ID="ID_1437768019" CREATED="1418884855539" MODIFIED="1418885036448"/>
<node TEXT="&#x413;&#x440;&#x443;&#x43f;&#x43f;&#x430; &#x43d;&#x443;&#x43b;&#x435;&#x439; 0000:0000:-&quot;-&#xa;&#x43c;&#x43e;&#x436;&#x435;&#x442; &#x437;&#x430;&#x43c;&#x435;&#x43d;&#x44f;&#x442;&#x441;&#x44f; &#x43d;&#x430; :" STYLE_REF="pre_last_node" ID="ID_168408651" CREATED="1418884879981" MODIFIED="1418885036485"/>
<node TEXT="http://[IPv6]:port/" STYLE_REF="pre_last_node" ID="ID_1573090067" CREATED="1418884936658" MODIFIED="1418885036503"/>
</node>
<node TEXT="&#x411;&#x435;&#x437;&#x43e;&#x43f;&#x430;&#x441;&#x43d;&#x43e;&#x441;&#x442;&#x44c;" STYLE_REF="orange_node" ID="ID_580046640" CREATED="1418885063266" MODIFIED="1419259219264">
<icon BUILTIN="clanbomber"/>
<node TEXT="IPSec" STYLE_REF="pre_last_node" ID="ID_1222714363" CREATED="1418885070285" MODIFIED="1418885099014"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1056;&#1072;&#1073;&#1086;&#1090;&#1072;&#1077;&#1090; &#1073;&#1077;&#1079; &#1087;&#1088;&#1080;&#1082;&#1083;&#1072;&#1076;&#1085;&#1086;&#1075;&#1086; &#1055;&#1054;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
</node>
</map>
