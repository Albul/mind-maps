<map version="freeplane 1.3.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="TCP/IP" STYLE_REF="main_node" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1427445058606">
<hook NAME="AutomaticEdgeColor" COUNTER="4"/>
<hook NAME="MapStyle">
    <properties show_icon_for_attributes="true" show_note_icons="true"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
<stylenode TEXT="second_node" BACKGROUND_COLOR="#00cccc" STYLE="bubble">
<font SIZE="14"/>
</stylenode>
<stylenode TEXT="pre_last_node" BACKGROUND_COLOR="#99ff99" STYLE="bubble">
<edge STYLE="bezier" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="yellow_node" BACKGROUND_COLOR="#ffff66" STYLE="bubble"/>
<stylenode TEXT="orange_node" BACKGROUND_COLOR="#ff9900" STYLE="bubble">
<font BOLD="true"/>
</stylenode>
<stylenode TEXT="last_node" BACKGROUND_COLOR="#ffffff">
<edge STYLE="bezier" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="main_node" BACKGROUND_COLOR="#ccccff">
<font SIZE="18" BOLD="true"/>
<edge STYLE="sharp_bezier" WIDTH="4"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook URI="comparison-tcp-osi.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x422;&#x430;&#x431;&#x43b;&#x438;&#x446;&#x430;" LOCALIZED_STYLE_REF="defaultstyle.floating" POSITION="right" ID="ID_1531632323" CREATED="1419059209826" MODIFIED="1427445018103" HGAP="-166" VSHIFT="-748">
<hook NAME="FreeNode"/>
<hook URI="tcp-ip-stack.jpg" SIZE="0.9230769" NAME="ExternalObject"/>
</node>
<node TEXT="1. &#x418;&#x43d;&#x442;&#x435;&#x440;&#x444;&#x435;&#x439;&#x441;&#x43d;&#x44b;&#x439;" STYLE_REF="second_node" POSITION="right" ID="ID_1091737402" CREATED="1419056878900" MODIFIED="1419058745720" HGAP="60" VSHIFT="-10">
<edge COLOR="#333333"/>
<hook URI="../../Images/interface-1.jpg" SIZE="1.0" NAME="ExternalObject"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Link and physical
    </p>
  </body>
</html>
</richcontent>
<node TEXT="&#x418;&#x43d;&#x442;&#x435;&#x440;&#x444;&#x435;&#x439;&#x441;&#x43d;&#x43e;&#x435; &#x443;&#x441;&#x442;&#x440;&#x43e;&#x439;&#x441;&#x442;&#x432;&#x43e;" STYLE_REF="pre_last_node" ID="ID_1546348218" CREATED="1419057083419" MODIFIED="1419057592080"/>
<node TEXT="&#x414;&#x440;&#x430;&#x439;&#x432;&#x435;&#x440; &#x44f;&#x434;&#x440;&#x430;" STYLE_REF="pre_last_node" ID="ID_1914630812" CREATED="1419057092766" MODIFIED="1419057592086"/>
</node>
<node TEXT="4. &#x41f;&#x440;&#x438;&#x43a;&#x43b;&#x430;&#x434;&#x43d;&#x43e;&#x439;" STYLE_REF="second_node" POSITION="left" ID="ID_1642715421" CREATED="1419056955118" MODIFIED="1419058735790" HGAP="130" VSHIFT="40">
<edge COLOR="#660066"/>
<hook URI="../../Images/sinapism.jpg" SIZE="1.0" NAME="ExternalObject"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Application
    </p>
  </body>
</html>
</richcontent>
<node TEXT="&#x421;&#x435;&#x442;&#x435;&#x432;&#x44b;&#x435; &#x441;&#x435;&#x440;&#x432;&#x438;&#x441;&#x44b;" STYLE_REF="yellow_node" ID="ID_344141135" CREATED="1419057464864" MODIFIED="1419057525703">
<node TEXT="Http" STYLE_REF="pre_last_node" ID="ID_1774723227" CREATED="1419057527529" MODIFIED="1419057544688"/>
<node TEXT="Email" STYLE_REF="pre_last_node" ID="ID_1721388968" CREATED="1419057533354" MODIFIED="1419057544685"/>
<node TEXT="Ftp" STYLE_REF="pre_last_node" ID="ID_1025647795" CREATED="1419057537579" MODIFIED="1419057544680"/>
</node>
<node TEXT="&#x423;&#x434;&#x430;&#x43b;&#x435;&#x43d;&#x43d;&#x44b;&#x439; &#x432;&#x44b;&#x437;&#x43e;&#x432; &#x43f;&#x440;&#x43e;&#x446;&#x435;&#x434;&#x443;&#x440;" STYLE_REF="yellow_node" ID="ID_1271630682" CREATED="1419057488579" MODIFIED="1419057550672"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      RPC
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&#x421;&#x435;&#x442;&#x435;&#x432;&#x430;&#x44f; &#x444;&#x430;&#x439;&#x43b;&#x43e;&#x432;&#x430;&#x44f; &#x441;&#x438;&#x441;&#x442;&#x435;&#x43c;&#x430;" STYLE_REF="yellow_node" ID="ID_858733923" CREATED="1419057496812" MODIFIED="1419057556933"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      NFS
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&#x41f;&#x440;&#x43e;&#x433;&#x440;&#x430;&#x43c;&#x43c;&#x44b; &#x440;&#x430;&#x431;&#x43e;&#x442;&#x430;&#x44e;&#x442; &#x441;" STYLE_REF="yellow_node" ID="ID_1745533877" CREATED="1419057508496" MODIFIED="1419057525717">
<node TEXT="TCP" STYLE_REF="pre_last_node" ID="ID_710992041" CREATED="1419057558524" MODIFIED="1419057577995"/>
<node TEXT="UDP" STYLE_REF="pre_last_node" ID="ID_1362434059" CREATED="1419057562332" MODIFIED="1419057577992"/>
<node TEXT="ICMP" STYLE_REF="pre_last_node" ID="ID_1083488422" CREATED="1419057565687" MODIFIED="1419057577984"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1044;&#1072;&#1078;&#1077;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="2. &#x41c;&#x435;&#x436;&#x441;&#x435;&#x442;&#x435;&#x432;&#x43e;&#x439;" STYLE_REF="second_node" POSITION="right" ID="ID_1296617495" CREATED="1419056929275" MODIFIED="1419058720788" HGAP="70" VSHIFT="-60">
<edge COLOR="#0000ff"/>
<hook URI="../../Images/network-neurons.jpg" SIZE="1.0" NAME="ExternalObject"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Internetwork
    </p>
  </body>
</html>
</richcontent>
<node TEXT="ICMP" STYLE_REF="yellow_node" ID="ID_1727931305" CREATED="1419057098041" MODIFIED="1419057286988"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Internet control
    </p>
    <p>
      message protocol
    </p>
  </body>
</html>
</richcontent>
<node TEXT="&#x41f;&#x435;&#x440;&#x435;&#x434;&#x430;&#x447;&#x430; &#x441;&#x43e;&#x43e;&#x431;&#x449;&#x435;&#x43d;&#x438;&#x439; &#x43e;&#x431; &#x43e;&#x448;&#x438;&#x431;&#x43a;&#x430;&#x445;" STYLE_REF="pre_last_node" ID="ID_1923919760" CREATED="1419057111148" MODIFIED="1419057245369"/>
<node TEXT="&#x41f;&#x440;&#x438;&#x43c;&#x435;&#x440;&#x44b;" STYLE_REF="pre_last_node" ID="ID_1695197524" CREATED="1419057125453" MODIFIED="1419057245378">
<node TEXT="Host not found" STYLE_REF="yellow_node" ID="ID_1325751465" CREATED="1419057133295" MODIFIED="1419258696636"/>
<node TEXT="Network not reachable" STYLE_REF="yellow_node" ID="ID_707987026" CREATED="1419057143217" MODIFIED="1419258696640"/>
</node>
</node>
<node TEXT="IP" STYLE_REF="yellow_node" ID="ID_1426045367" CREATED="1419057107432" MODIFIED="1419057236897">
<node TEXT="&#x418;&#x43d;&#x43a;&#x430;&#x43f;&#x441;&#x443;&#x43b;&#x44f;&#x446;&#x438;&#x44f; &#x43f;&#x430;&#x43a;&#x435;&#x442;&#x430; &#x442;&#x440;&#x430;&#x43d;&#x441;&#x43f;&#x43e;&#x440;&#x442;&#x43d;&#x43e;&#x433;&#x43e;&#xa;&#x443;&#x440;&#x43e;&#x432;&#x43d;&#x44f; &#x432; IP &#x434;&#x430;&#x442;&#x430;&#x433;&#x440;&#x430;&#x43c;&#x443;" STYLE_REF="pre_last_node" ID="ID_374162948" CREATED="1419057160850" MODIFIED="1419057245380"/>
<node TEXT="&#x41c;&#x430;&#x440;&#x448;&#x440;&#x443;&#x442;&#x438;&#x437;&#x430;&#x446;&#x438;&#x44f;" STYLE_REF="pre_last_node" ID="ID_1786826201" CREATED="1419057188770" MODIFIED="1419057245388"/>
<node TEXT="&#x41f;&#x440;&#x435;&#x43e;&#x431;&#x440;&#x430;&#x437;&#x43e;&#x432;&#x430;&#x43d;&#x438;&#x435; &#x430;&#x434;&#x440;&#x435;&#x441;&#x43e;&#x432;" STYLE_REF="pre_last_node" ID="ID_554758611" CREATED="1419057200938" MODIFIED="1419057245391"/>
<node TEXT="&#x41e;&#x431;&#x44a;&#x435;&#x434;&#x438;&#x43d;&#x435;&#x43d;&#x438;&#x435; &#x441;&#x435;&#x433;&#x43c;&#x435;&#x43d;&#x442;&#x43e;&#x432; &#x441;&#x435;&#x442;&#x438; &#x432; &#x435;&#x434;&#x438;&#x43d;&#x43d;&#x443;&#x44e; &#x441;&#x435;&#x442;&#x44c;" STYLE_REF="pre_last_node" ID="ID_614919171" CREATED="1419057210483" MODIFIED="1419057245393"/>
</node>
</node>
<node TEXT="3. &#x422;&#x440;&#x430;&#x43d;&#x441;&#x43f;&#x43e;&#x440;&#x442;&#x43d;&#x44b;&#x439;" STYLE_REF="second_node" POSITION="left" ID="ID_12171782" CREATED="1419056940968" MODIFIED="1419058730171" HGAP="100" VSHIFT="-80">
<edge COLOR="#003333"/>
<hook URI="../../Images/lorry.jpg" SIZE="1.0" NAME="ExternalObject"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Transport
    </p>
  </body>
</html>
</richcontent>
<node TEXT="&#x424;&#x443;&#x43d;&#x43a;&#x446;&#x438;&#x438;" STYLE_REF="yellow_node" ID="ID_85749619" CREATED="1419057296454" MODIFIED="1419057383033">
<node TEXT="&#x41e;&#x431;&#x435;&#x441;&#x43f;&#x435;&#x447;&#x438;&#x432;&#x430;&#x442;&#x44c; &#x43d;&#x430;&#x434;&#x435;&#x436;&#x43d;&#x443;&#x44e;&#xa;&#x43f;&#x435;&#x440;&#x435;&#x434;&#x430;&#x447;&#x443; &#x434;&#x430;&#x43d;&#x43d;&#x44b;&#x445;" STYLE_REF="pre_last_node" ID="ID_1897431026" CREATED="1419057308059" MODIFIED="1419057389610">
<node TEXT="&#x41f;&#x440;&#x430;&#x432;&#x435;&#x43b;&#x44c;&#x43d;&#x44b;&#x439; &#x43f;&#x43e;&#x440;&#x44f;&#x434;&#x43e;&#x43a;" STYLE_REF="yellow_node" ID="ID_1428396720" CREATED="1419057351011" MODIFIED="1419258684660"/>
<node TEXT="&#x411;&#x435;&#x437; &#x434;&#x443;&#x431;&#x43b;&#x438;&#x440;&#x43e;&#x432;&#x430;&#x43d;&#x438;&#x44f;" STYLE_REF="yellow_node" ID="ID_1584284897" CREATED="1419057358632" MODIFIED="1419258684664"/>
<node TEXT="&#x41d;&#x435;&#x43f;&#x43e;&#x432;&#x440;&#x435;&#x436;&#x434;&#x435;&#x43d;&#x43d;&#x44b;&#x435; &#x434;&#x430;&#x43d;&#x43d;&#x44b;&#x435;" STYLE_REF="yellow_node" ID="ID_600991627" CREATED="1419057366119" MODIFIED="1419258684666"/>
</node>
<node TEXT="&#x412;&#x432;&#x43e;&#x434;&#x438;&#x442; &#x43f;&#x43e;&#x43d;&#x44f;&#x442;&#x438;&#x435; &#x43f;&#x43e;&#x440;&#x442;&#x430;" STYLE_REF="pre_last_node" ID="ID_274874074" CREATED="1419057332428" MODIFIED="1419057389624"/>
<node TEXT="&#x41c;&#x443;&#x43b;&#x44c;&#x442;&#x438;&#x43f;&#x43b;&#x435;&#x43a;&#x441;&#x438;&#x440;&#x43e;&#x432;&#x430;&#x43d;&#x438;&#x435;" STYLE_REF="pre_last_node" ID="ID_860583648" CREATED="1419057344344" MODIFIED="1419057389626"/>
</node>
<node TEXT="&#x41f;&#x440;&#x43e;&#x442;&#x43e;&#x43a;&#x43e;&#x43b;&#x44b;" STYLE_REF="yellow_node" ID="ID_1458202014" CREATED="1419057303960" MODIFIED="1419057383041">
<node TEXT="TCP" STYLE_REF="pre_last_node" ID="ID_178072596" CREATED="1419057403901" MODIFIED="1419057419786"/>
<node TEXT="UDP" STYLE_REF="pre_last_node" ID="ID_421996446" CREATED="1419057408668" MODIFIED="1419058536236">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="80" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_1296617495" MIDDLE_LABEL="&#x41d;&#x435; &#x433;&#x430;&#x440;&#x430;&#x43d;&#x442;&#x438;&#x440;&#x443;&#x435;&#x442; &#x434;&#x43e;&#x441;&#x442;&#x430;&#x432;&#x43a;&#x443;.&#xa;&#x421;&#x43a;&#x43e;&#x440;&#x435;&#x435; &#x44f;&#x432;&#x43b;&#x44f;&#x435;&#x442;&#x441;&#x44f; &#x43c;&#x435;&#x436;&#x441;&#x435;&#x442;&#x435;&#x432;&#x44b;&#x43c; &#x443;&#x440;&#x43e;&#x432;&#x43d;&#x435;&#x43c;" STARTINCLINATION="-6;94;" ENDINCLINATION="10;313;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
</node>
</node>
</node>
</node>
</map>
