<map version="freeplane 1.3.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="&#x427;&#x430;&#x441;&#x442;&#x43d;&#x44b;&#x435; IP-&#x430;&#x434;&#x440;&#x435;&#x441;&#x430;" STYLE_REF="main_node" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1419255770483" VGAP="11">
<hook NAME="AutomaticEdgeColor" COUNTER="4"/>
<hook URI="../../Images/private-house-large.jpg" SIZE="1.0" NAME="ExternalObject"/>
<hook NAME="MapStyle">
    <properties show_icon_for_attributes="true" show_note_icons="true"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
<stylenode TEXT="second_node" BACKGROUND_COLOR="#00cccc" STYLE="bubble">
<font SIZE="14"/>
</stylenode>
<stylenode TEXT="pre_last_node" BACKGROUND_COLOR="#99ff99" STYLE="bubble">
<edge STYLE="bezier" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="yellow_node" BACKGROUND_COLOR="#ffff66" STYLE="bubble"/>
<stylenode TEXT="orange_node" BACKGROUND_COLOR="#ff9900" STYLE="bubble">
<font BOLD="true"/>
</stylenode>
<stylenode TEXT="last_node" BACKGROUND_COLOR="#ffffff">
<edge STYLE="bezier" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="main_node" BACKGROUND_COLOR="#ccccff">
<font SIZE="18" BOLD="true"/>
<edge STYLE="sharp_bezier" WIDTH="4"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<node TEXT="&#x412;&#x44b;&#x445;&#x43e;&#x434; &#x432; &#x438;&#x43d;&#x442;&#x435;&#x440;&#x43d;&#x435;&#x442;" STYLE_REF="second_node" POSITION="left" ID="ID_1680491023" CREATED="1419145679569" MODIFIED="1419148949213" VGAP="33">
<edge COLOR="#003366"/>
<hook URI="../../Images/exit.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x41f;&#x440;&#x43e;&#x43a;&#x441;&#x438; &#x441;&#x435;&#x440;&#x432;&#x435;&#x440;" STYLE_REF="orange_node" ID="ID_368165305" CREATED="1419146174926" MODIFIED="1419149045944" VGAP="13"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Proxy server
    </p>
  </body>
</html>
</richcontent>
<hook URI="../../Images/server-1.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x41f;&#x440;&#x43e;&#x442;&#x43e;&#x43a;&#x43e;&#x43b;&#x44b; &#x441; &#x44d;&#x441;&#x442;&#x430;&#x444;&#x435;&#x442;&#x43d;&#x43e;&#x439;&#xa;&#x43f;&#x435;&#x440;&#x435;&#x434;&#x430;&#x447;&#x435;&#x439; &#x438;&#x43d;&#x444;&#x43e;&#x440;&#x43c;&#x430;&#x446;&#x438;&#x438;" STYLE_REF="yellow_node" ID="ID_897359959" CREATED="1419146240271" MODIFIED="1419146501916">
<node TEXT="Usenet" STYLE_REF="pre_last_node" ID="ID_1252398010" CREATED="1419146297913" MODIFIED="1419146317367"/>
<node TEXT="SMTP" STYLE_REF="pre_last_node" ID="ID_1592712265" CREATED="1419146306945" MODIFIED="1419146317363"/>
<node TEXT="IRC" STYLE_REF="pre_last_node" ID="ID_642148335" CREATED="1419146311922" MODIFIED="1419146317355"/>
</node>
<node TEXT="&#x412;&#x438;&#x434;&#x44b;" STYLE_REF="yellow_node" ID="ID_1646897452" CREATED="1419146278537" MODIFIED="1419146293458">
<node TEXT="&#x41f;&#x440;&#x43e;&#x437;&#x440;&#x430;&#x447;&#x43d;&#x44b;&#x439;" STYLE_REF="pre_last_node" ID="ID_1577993880" CREATED="1419146324418" MODIFIED="1419146459501"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1058;&#1088;&#1072;&#1092;&#1080;&#1082; &#1087;&#1077;&#1088;&#1077;&#1085;&#1072;&#1087;&#1088;&#1072;&#1074;&#1083;&#1103;&#1077;&#1090;&#1089;&#1103; &#1085;&#1072; &#1087;&#1088;&#1086;&#1082;&#1089;&#1080;
    </p>
    <p>
      &#1085;&#1077; &#1103;&#1074;&#1085;&#1086; (&#1089;&#1088;&#1077;&#1076;&#1089;&#1090;&#1074;&#1072;&#1084;&#1080; &#1084;&#1072;&#1088;&#1096;&#1088;&#1091;&#1090;&#1080;&#1079;&#1072;&#1090;&#1086;&#1088;&#1072;)
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&#x41e;&#x431;&#x440;&#x430;&#x442;&#x43d;&#x44b;&#x439;" STYLE_REF="pre_last_node" ID="ID_288891133" CREATED="1419146331641" MODIFIED="1419146407573"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1052;&#1077;&#1078;&#1089;&#1077;&#1090;&#1077;&#1074;&#1086;&#1081; &#1101;&#1082;&#1088;&#1072;&#1085; &#1085;&#1072; &#1087;&#1088;&#1080;&#1082;&#1083;&#1072;&#1076;&#1085;&#1086;&#1084; &#1091;&#1088;&#1086;&#1074;&#1085;&#1077;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="&#x41d;&#x435;&#x434;&#x43e;&#x441;&#x442;&#x430;&#x442;&#x43a;&#x438;" STYLE_REF="yellow_node" ID="ID_427972424" CREATED="1419146264346" MODIFIED="1419146293456">
<icon BUILTIN="subtraction"/>
<node TEXT="&#x41d;&#x435;&#x432;&#x43e;&#x437;&#x43c;&#x43e;&#x436;&#x43d;&#x43e; &#x43f;&#x43e;&#x434;&#x434;&#x435;&#x440;&#x436;&#x438;&#x432;&#x430;&#x442;&#x44c; &#x432;&#x441;&#x435; &#x43f;&#x440;&#x43e;&#x442;&#x43e;&#x43a;&#x43e;&#x43b;&#x44b;" STYLE_REF="pre_last_node" ID="ID_1730358689" CREATED="1419146508947" MODIFIED="1419146608721"/>
<node TEXT="&#x41d;&#x435; &#x432;&#x43e;&#x437;&#x43c;&#x43e;&#x436;&#x43d;&#x43e; &#x432;&#x44b;&#x445;&#x43e;&#x434;&#x438;&#x442;&#x44c; &#x432; &#x438;&#x43d;&#x435;&#x442;&#xa;&#x43f;&#x43e; &#x43d;&#x435;&#x43f;&#x43e;&#x434;&#x434;&#x435;&#x440;&#x436;&#x438;&#x432;&#x430;&#x435;&#x43c;&#x44b;&#x445; &#x43f;&#x440;&#x43e;&#x442;&#x43e;&#x43a;&#x43e;&#x43b;&#x430;&#x445;" STYLE_REF="pre_last_node" ID="ID_613306425" CREATED="1419146527993" MODIFIED="1419146608751"/>
<node TEXT="&#x421;&#x443;&#x449;&#x435;&#x441;&#x442;&#x432;&#x443;&#x44e;&#x442; &#x43f;&#x440;&#x43e;&#x442; &#x43a;&#x43e;&#x442;&#x43e;&#x440;&#x44b;&#x435;&#xa;&#x440;&#x435;&#x430;&#x43b;&#x438;&#x437;&#x443;&#x44e;&#x442; &#x441;&#x432;&#x43e;&#x439; &#x43f;&#x440;&#x43e;&#x442; &#x43f;&#x43e;&#x432;&#x435;&#x440;&#x445; HTTP" STYLE_REF="pre_last_node" ID="ID_1515424194" CREATED="1419146563979" MODIFIED="1419146608764"/>
<node TEXT="&#x421;&#x43b;&#x43e;&#x436;&#x43d;&#x43e;&#x441;&#x442;&#x44c; &#x43f;&#x440;&#x43e;&#x43a;&#x441;&#x438;" STYLE_REF="pre_last_node" ID="ID_1450697289" CREATED="1419146521021" MODIFIED="1419146608739"/>
</node>
<node TEXT="&#x414;&#x43e;&#x441;&#x442;&#x43e;&#x438;&#x43d;&#x441;&#x442;&#x432;&#x430;" STYLE_REF="yellow_node" ID="ID_175516000" CREATED="1419146270416" MODIFIED="1419146293450">
<icon BUILTIN="addition"/>
<node TEXT="HTTP &#x438;&#x437;&#x43d;&#x430;&#x447;&#x430;&#x43b;&#x44c;&#x43d;&#x43e; &#x43f;&#x43e;&#x434;&#x434;&#x435;&#x440;&#x436;&#x438;&#x432;&#x430;&#x435;&#x442;&#x441;&#x44f; &#x43f;&#x440;&#x43e;&#x43a;&#x441;&#x438;" STYLE_REF="pre_last_node" ID="ID_696263457" CREATED="1419146788824" MODIFIED="1419148213440"/>
<node TEXT="&#x41a;&#x43e;&#x43d;&#x442;&#x440;&#x43e;&#x43b;&#x44c; &#x434;&#x43e;&#x441;&#x442;&#x443;&#x43f;&#x430;" STYLE_REF="pre_last_node" ID="ID_1172672996" CREATED="1419146805530" MODIFIED="1419148213454"/>
<node TEXT="&#x423;&#x447;&#x435;&#x442; &#x442;&#x440;&#x430;&#x444;&#x438;&#x43a;&#x430;" STYLE_REF="pre_last_node" ID="ID_1557748730" CREATED="1419146814299" MODIFIED="1419148213462"/>
<node TEXT="&#x424;&#x438;&#x43b;&#x44c;&#x442;&#x440;&#x430;&#x446;&#x438;&#x44f; &#x442;&#x440;&#x430;&#x444;&#x438;&#x43a;&#x430;" STYLE_REF="pre_last_node" ID="ID_975571993" CREATED="1419146820021" MODIFIED="1419148213470"/>
<node TEXT="&#x41a;&#x435;&#x448;&#x438;&#x440;&#x43e;&#x432;&#x430;&#x43d;&#x438;&#x435;" STYLE_REF="pre_last_node" ID="ID_756738779" CREATED="1419146827639" MODIFIED="1419148213478"/>
<node TEXT="&#x421;&#x436;&#x430;&#x442;&#x438;&#x435; &#x434;&#x430;&#x43d;&#x43d;&#x44b;&#x445;" STYLE_REF="pre_last_node" ID="ID_1350777594" CREATED="1419146840153" MODIFIED="1419148213487"/>
<node TEXT="&#x41f;&#x43e;&#x432;&#x44b;&#x448;&#x430;&#x435;&#x442; &#x431;&#x435;&#x437;&#x43e;&#x43f;&#x430;&#x441;&#x43d;&#x43e;&#x441;&#x442;&#x44c;, &#x431;&#x430;&#x43d; &#x43d;&#x430;&#xa;&#x43d;&#x435;&#x43f;&#x43e;&#x434;&#x434;&#x435;&#x440;&#x436;&#x438;&#x432;&#x430;&#x435;&#x43c;&#x44b;&#x435; &#x43f;&#x43e;&#x440;&#x442;&#x44b;" STYLE_REF="pre_last_node" ID="ID_90882268" CREATED="1419146847427" MODIFIED="1419148213494"/>
</node>
</node>
<node TEXT="NAT" STYLE_REF="orange_node" ID="ID_707646983" CREATED="1419146184455" MODIFIED="1419150606443" VGAP="13"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Network address translation
    </p>
  </body>
</html>
</richcontent>
<hook URI="../../Images/nato-1.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x41e;&#x441;&#x43e;&#x431;&#x435;&#x43d;&#x43d;&#x43e;&#x441;&#x442;&#x438;" STYLE_REF="yellow_node" ID="ID_290716149" CREATED="1419150118852" MODIFIED="1419150606436">
<icon BUILTIN="list"/>
<node TEXT="&#x420;&#x435;&#x430;&#x43b;&#x438;&#x437;&#x443;&#x435;&#x442;&#x441;&#x44f; &#x43c;&#x430;&#x440;&#x448;&#x440;&#x443;&#x442;&#x438;&#x437;&#x430;&#x442;&#x43e;&#x440;&#x43e;&#x43c;" STYLE_REF="pre_last_node" ID="ID_798213247" CREATED="1419150168974" MODIFIED="1419150522550"/>
<node TEXT="&#x412; &#x43f;&#x430;&#x43a;&#x435;&#x442;&#x435; &#x438;&#x437; &#x43b;&#x43e;&#x43a; &#x441;&#x435;&#x442;&#x438;, &#x430;&#x434;&#x440;&#x435;&#x441; &#x43e;&#x442;&#x43f;&#x440;&#x430;&#x432;&#x438;&#x442;&#x435;&#x43b;&#x44f;&#xa;&#x437;&#x430;&#x43c;&#x435;&#x43d;&#x44f;&#x435;&#x442; &#x441;&#x432;&#x43e;&#x438;&#x43c; &#x432;&#x43d;&#x435;&#x448;&#x43d;&#x438;&#x43c; IP" STYLE_REF="pre_last_node" ID="ID_1560733418" CREATED="1419150188918" MODIFIED="1419150522569"/>
<node TEXT="&#x41f;&#x43e;&#x43b;&#x443;&#x447;&#x438;&#x432; &#x43e;&#x442;&#x432;&#x435;&#x442; &#x43e;&#x442; &#x441;&#x435;&#x440;&#x432;&#x435;&#x440;&#x430;, &#x43f;&#x43e; &#x442;&#x430;&#x431;&#x43b;&#x438;&#x446;&#x435; &#x43e;&#x442;&#x43a;&#x440;&#x44b;&#x442;&#x44b;&#x445;&#xa;&#x441;&#x43e;&#x435;&#x434;&#x438;&#x43d;&#x435;&#x43d;&#x438;&#x439; &#x432;&#x43e;&#x441;&#x442;&#x430;&#x43d;&#x430;&#x432;&#x43b;&#x438;&#x432;&#x430;&#x435;&#x442; &#x430;&#x434;&#x440;&#x435;&#x441;&#x430;&#x442;&#x430; &#x438;&#xa;&#x440;&#x435;&#x442;&#x440;&#x430;&#x43d;&#x441;&#x43b;&#x438;&#x440;&#x443;&#x435;&#x442; &#x435;&#x43c;&#x443; &#x43e;&#x442;&#x432;&#x435;&#x442;" STYLE_REF="pre_last_node" ID="ID_1552799197" CREATED="1419150233399" MODIFIED="1419150522582"/>
</node>
<node TEXT="&#x422;&#x438;&#x43f;&#x44b; NAT" STYLE_REF="yellow_node" ID="ID_1885690285" CREATED="1419150145649" MODIFIED="1419150161859">
<node TEXT="&#x421;&#x442;&#x430;&#x442;&#x438;&#x447;&#x435;&#x441;&#x43a;&#x438;&#x439;" STYLE_REF="pre_last_node" ID="ID_631105304" CREATED="1419150558127" MODIFIED="1419150752610"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      IP &#1074;&#1089;&#1077;&#1093; &#1074; &#1083;&#1086;&#1082; &#1089;&#1077;&#1090;&#1080; &#1086;&#1090;&#1086;&#1073;&#1088;&#1072;&#1078;&#1072;&#1102;&#1090;&#1089;&#1103;
    </p>
    <p>
      &#1085;&#1072; &#1086;&#1076;&#1085;&#1080; &#1074;&#1085;&#1077;&#1096;&#1085;&#1080;&#1081; IP
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&#x414;&#x438;&#x43d;&#x430;&#x43c;&#x438;&#x447;&#x435;&#x441;&#x43a;&#x438;&#x439;" STYLE_REF="pre_last_node" ID="ID_1940725648" CREATED="1419150563377" MODIFIED="1419150775593"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1042;&#1099;&#1073;&#1086;&#1088; &#1080;&#1079; &#1087;&#1091;&#1083;&#1072; &#1074;&#1085;&#1077;&#1096;&#1085;&#1080;&#1093; IP
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="PAT" STYLE_REF="pre_last_node" ID="ID_1214568475" CREATED="1419150567932" MODIFIED="1419154541759"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Port address tanslation
    </p>
  </body>
</html>
</richcontent>
<hook URI="pat.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="1 &#x43b;&#x43e;&#x43a; &#x445;&#x43e;&#x441;&#x442; =&gt; &#x432; 1 &#x432;&#x43d;&#x435;&#x448;&#x43d;&#x438;&#x439; IP" ID="ID_1039030571" CREATED="1419150777305" MODIFIED="1419150825751"/>
<node TEXT="&#x41d;&#x43e;&#x43c;&#x435;&#x440; &#x43f;&#x43e;&#x440;&#x442;&#x430; &#x438;&#x441;&#x445;&#x43e;&#x434;&#x44f;&#x449;&#x435;&#x433;&#x43e; &#x43f;&#x430;&#x43a;&#x435;&#x442;&#x430;&#xa;&#x437;&#x430;&#x43c;&#x435;&#x43d;&#x44f;&#x435;&#x442;&#x441;&#x44f; &#x443;&#x43d;&#x438;&#x43a;&#x430;&#x43b;&#x44c;&#x43d;&#x44b;&#x43c; &#x437;&#x43d;&#x430;&#x447;&#x435;&#x43d;&#x438;&#x435;&#x43c;" ID="ID_1198917207" CREATED="1419150827254" MODIFIED="1419150857931"/>
<node TEXT="&#x42d;&#x442;&#x43e;&#x442; &#x43f;&#x43e;&#x440;&#x442; &#x430;&#x441;&#x43e;&#x446;&#x438;&#x438;&#x440;&#x443;&#x435;&#x442;&#x441;&#x44f; &#x441; &#x447;&#x430;&#x441;&#x442;&#x43d;&#x44b;&#x43c; IP &#x430;&#x434;&#x440;&#x435;&#x441;&#x43e;&#x43c;" ID="ID_576086888" CREATED="1419150870038" MODIFIED="1419150900004"/>
</node>
</node>
<node TEXT="&#x414;&#x43e;&#x441;&#x442;&#x43e;&#x438;&#x43d;&#x441;&#x442;&#x432;&#x430;" STYLE_REF="yellow_node" ID="ID_1817302952" CREATED="1419150130756" MODIFIED="1419150343131">
<icon BUILTIN="addition"/>
<node TEXT="&#x42d;&#x43a;&#x43e;&#x43d;&#x43e;&#x43c;&#x438;&#x44f; IP-&#x430;&#x434;&#x440;&#x435;&#x441;&#x43e;&#x432;" STYLE_REF="pre_last_node" ID="ID_1874246687" CREATED="1419150346947" MODIFIED="1419150499170"/>
<node TEXT="&#x41e;&#x433;&#x440;&#x430;&#x43d;&#x438;&#x447;&#x435;&#x43d;&#x43d;&#x43e;&#x435; &#x43e;&#x431;&#x440;&#x430;&#x449;&#x435;&#x43d;&#x438;&#x435; &#x441;&#x43d;&#x430;&#x440;&#x443;&#x436;&#x438;&#xa;&#x43a; &#x432;&#x43d;&#x443;&#x442;&#x440;&#x435;&#x43d;&#x43d;&#x438;&#x43c; &#x445;&#x43e;&#x441;&#x442;&#x430;&#x43c;" STYLE_REF="pre_last_node" ID="ID_1085174182" CREATED="1419150357189" MODIFIED="1419150486124"/>
</node>
<node TEXT="&#x41d;&#x435;&#x434;&#x43e;&#x441;&#x442;&#x430;&#x442;&#x43a;&#x438;" STYLE_REF="yellow_node" ID="ID_987492011" CREATED="1419150139468" MODIFIED="1419150345402">
<icon BUILTIN="subtraction"/>
<node TEXT="&#x421;&#x442;&#x430;&#x440;&#x44b;&#x435; &#x43f;&#x440;&#x43e;&#x442;&#x43e;&#x43a;&#x43e;&#x43b;&#x44b; &#x43d;&#x435; &#x43f;&#x43e;&#x434;&#x434;&#x435;&#x440;&#x436;&#x438;&#x432;&#x430;&#x44e;&#x442;&#x441;&#x44f;" STYLE_REF="pre_last_node" ID="ID_347635650" CREATED="1419150388051" MODIFIED="1419150486112"/>
<node TEXT="&#x414;&#x43e;&#x43f;&#x43e;&#x43b;&#x43d;&#x438;&#x442;&#x435;&#x43b;&#x44c;&#x43d;&#x44b;&#x435; &#x43d;&#x430;&#x441;&#x442;&#x440;&#x43e;&#x439;&#x43a;&#x438; &#x434;&#x43b;&#x44f;&#xa;&#x43f;&#x438;&#x440;&#x438;&#x43d;&#x433;&#x43e;&#x432;&#x44b;&#x445; &#x441;&#x435;&#x442;&#x435;&#x439; P2P" STYLE_REF="pre_last_node" ID="ID_386245176" CREATED="1419150413738" MODIFIED="1419150486100"/>
<node TEXT="&#x414;&#x43b;&#x44f; &#x441;&#x435;&#x440;&#x432;&#x435;&#x440;&#x430; &#x438;&#x43b;&#x44e;&#x437;&#x438;&#x44f; DOS-&#x430;&#x442;&#x430;&#x43a;&#x438;&#xa;&#x43a;&#x43e;&#x433;&#x434;&#x430; &#x43c;&#x43d;&#x43e;&#x433;&#x43e; &#x43e;&#x431;&#x440;&#x430;&#x449;&#x435;&#x43d;&#x438;&#x439; &#x438;&#x437; &#x437;&#x430; NAT" STYLE_REF="pre_last_node" ID="ID_391514997" CREATED="1419150438680" MODIFIED="1419150486087"/>
</node>
</node>
<node TEXT="&#x421;&#x435;&#x442;&#x435;&#x432;&#x43e;&#x439; &#x442;&#x443;&#x43d;&#x43d;&#x435;&#x43b;&#x44c;" STYLE_REF="orange_node" ID="ID_1917063790" CREATED="1419146193712" MODIFIED="1419154657102" VGAP="13">
<hook URI="../../Images/tunnel.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x422;&#x435;&#x445;&#x43d;&#x43e;&#x43b;&#x43e;&#x433;&#x438;&#x438;" STYLE_REF="yellow_node" ID="ID_1356541341" CREATED="1419148295083" MODIFIED="1419148563572">
<node TEXT="PPPoE" STYLE_REF="pre_last_node" ID="ID_1182493886" CREATED="1419148328675" MODIFIED="1419148380540"/>
<node TEXT="VPN" STYLE_REF="pre_last_node" ID="ID_298613896" CREATED="1419148335915" MODIFIED="1419148380538"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Virtual private network
    </p>
  </body>
</html>
</richcontent>
<node TEXT="&#x423;&#x437;&#x435;&#x43b; - &#x443;&#x437;&#x435;&#x43b;" ID="ID_959142995" CREATED="1419148352947" MODIFIED="1419148358641"/>
<node TEXT="&#x423;&#x437;&#x435;&#x43b; - &#x441;&#x435;&#x442;&#x44c;" ID="ID_1486478627" CREATED="1419148359805" MODIFIED="1419148364086"/>
<node TEXT="&#x421;&#x435;&#x442;&#x44c; - &#x441;&#x435;&#x442;&#x44c;" ID="ID_1083818001" CREATED="1419148365301" MODIFIED="1419148370163"/>
</node>
<node TEXT="Hamachi" STYLE_REF="pre_last_node" ID="ID_1613615732" CREATED="1419148347756" MODIFIED="1419148380533"/>
</node>
<node TEXT="&#x41e;&#x441;&#x43e;&#x431;&#x435;&#x43d;&#x43d;&#x43e;&#x441;&#x442;&#x438;" STYLE_REF="yellow_node" ID="ID_1218714030" CREATED="1419148301173" MODIFIED="1419154911535">
<icon BUILTIN="list"/>
<hook URI="tunnel.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x421;&#x43e;&#x437;&#x434;&#x430;&#x435;&#x442; &#x437;&#x430;&#x449;&#x438;&#x449;&#x435;&#x43d;&#x43d;&#x43e;&#x435; &#x43b;&#x43e;&#x433;&#x438;&#x447;&#x435;&#x441;&#x43a;&#x43e;&#x435; &#x441;&#x43e;&#x435;&#x434;&#x438;&#x43d;&#x435;&#x43d;&#x438;&#x435;" STYLE_REF="pre_last_node" ID="ID_1048389045" CREATED="1419148481284" MODIFIED="1419148560054"/>
<node TEXT="&#x418;&#x43d;&#x43a;&#x430;&#x43f;&#x441;&#x443;&#x43b;&#x44f;&#x446;&#x438;&#x44f; &#x43f;&#x440;&#x43e;&#x442;&#x43e;&#x43a;&#x43e;&#x43b;&#x43e;&#x432;" STYLE_REF="pre_last_node" ID="ID_937297837" CREATED="1419148499561" MODIFIED="1419148560074"/>
<node TEXT="&#x41f;&#x430;&#x43a;&#x435;&#x442;&#x44b; &#x441;&#x435;&#x442;&#x435;&#x432;&#x43e;&#x433;&#x43e; &#x443;&#x440;&#x43e;&#x432;&#x43d;&#x44f; &#x437;&#x430;&#x432;&#x43e;&#x440;&#x430;&#x447;&#x438;&#x432;&#x430;&#x44e;&#x442;&#x441;&#x44f;&#xa;&#x432; &#x43f;&#x430;&#x43a;&#x435;&#x442;&#x44b; &#x432;&#x44b;&#x441;&#x448;&#x435;&#x433;&#x43e; &#x443;&#x440;&#x43e;&#x432;&#x43d;&#x44f;" STYLE_REF="pre_last_node" ID="ID_1755648113" CREATED="1419148510023" MODIFIED="1419148560086"/>
<node TEXT="&#x412;&#x445;&#x43e;&#x434; &#x432; &#x43a;&#x43e;&#x440;&#x43f;&#x43e;&#x440;&#x430;&#x442;&#x438;&#x432;&#x43d;&#x443;&#x44e; &#x43b;&#x43e;&#x43a;&#x430;&#x43b;&#x44c;&#x43d;&#x443;&#x44e; &#x441;&#x435;&#x442;&#x44c;&#xa;&#x447;&#x435;&#x440;&#x435;&#x437; &#x438;&#x43d;&#x442;&#x435;&#x440;&#x43d;&#x435;&#x442;" STYLE_REF="pre_last_node" ID="ID_715565340" CREATED="1419148532865" MODIFIED="1419148560098"/>
</node>
<node TEXT="&#x41a;&#x43e;&#x43c;&#x43f;&#x43e;&#x43d;&#x435;&#x442;&#x44b;&#xa;&#x442;&#x443;&#x43d;&#x435;&#x43b;&#x44f;" STYLE_REF="yellow_node" ID="ID_1416521417" CREATED="1419148310561" MODIFIED="1419148323871">
<node TEXT="1. &#x418;&#x43d;&#x438;&#x446;&#x438;&#x430;&#x442;&#x43e;&#x440; &#x442;&#x443;&#x43d;&#x435;&#x43b;&#x44f;" STYLE_REF="pre_last_node" ID="ID_996170462" CREATED="1419148392074" MODIFIED="1419148465153"/>
<node TEXT="2. &#x41c;&#x430;&#x440;&#x448;&#x440;&#x443;&#x442;&#x438;&#x437;&#x438;&#x440;&#x443;&#x435;&#x43c;&#x430;&#x44f; &#x441;&#x435;&#x442;&#x44c;" STYLE_REF="pre_last_node" ID="ID_273150001" CREATED="1419148403903" MODIFIED="1419148465150"/>
<node TEXT="3. &#x422;&#x443;&#x43d;&#x435;&#x43b;&#x44c;&#x43d;&#x44b;&#x439; &#x43a;&#x43e;&#x43c;&#x43c;&#x443;&#x442;&#x430;&#x442;&#x43e;&#x440;" STYLE_REF="pre_last_node" ID="ID_676382137" CREATED="1419148424591" MODIFIED="1419148465147"/>
<node TEXT="4. &#x422;&#x443;&#x43d;&#x435;&#x43b;&#x44c;&#x43d;&#x44b;&#x439; &#x442;&#x435;&#x440;&#x43c;&#x438;&#x43d;&#x430;&#x442;&#x43e;&#x440;" STYLE_REF="pre_last_node" ID="ID_1648428926" CREATED="1419148437333" MODIFIED="1419148465142"/>
</node>
</node>
</node>
<node TEXT="&#x41e;&#x441;&#x43e;&#x431;&#x435;&#x43d;&#x43d;&#x43e;&#x441;&#x442;&#x438;" STYLE_REF="second_node" POSITION="right" ID="ID_597347388" CREATED="1419145736343" MODIFIED="1419154844139" VSHIFT="-20">
<icon BUILTIN="list"/>
<edge COLOR="#336600"/>
<node TEXT="IP-&#x430;&#x434;&#x440;&#x435;&#x441; &#x43f;&#x440;&#x438;&#x43d;&#x430;&#x434;&#x43b;&#x435;&#x436;&#x430;&#x449;&#x438;&#x439; &#x43a; &#x441;&#x43f;&#x435;&#x446;&#x438;&#x430;&#x43b;&#x44c;&#x43d;&#x43e;&#x43c;&#x443;&#xa;&#x434;&#x438;&#x430;&#x43f;&#x430;&#x437;&#x43e;&#x43d;&#x443;, &#x43d;&#x435; &#x438;&#x441;&#x43f;&#x43e;&#x43b;&#x44c;&#x437;&#x443;&#x435;&#x43c;&#x43e;&#x43c;&#x443; &#x432; &#x441;&#x435;&#x442;&#x438; &#x418;&#x43d;&#x442;&#x435;&#x440;&#x43d;&#x435;&#x442;" STYLE_REF="pre_last_node" ID="ID_701701287" CREATED="1419145739035" MODIFIED="1419148228171"/>
<node TEXT="&#x422;&#x430;&#x43a;&#x438;&#x435; &#x430;&#x434;&#x440;&#x435;&#x441;&#x430; &#x43f;&#x440;&#x435;&#x434;&#x43d;&#x430;&#x437;&#x43d;&#x430;&#x447;&#x435;&#x43d;&#x44b; &#x434;&#x43b;&#x44f; &#x43f;&#x440;&#x438;&#x43c;&#x435;&#x43d;&#x435;&#x43d;&#x438;&#x44f; &#x432; &#x43b;&#x43e;&#x43a;&#x430;&#x43b;&#x44c;&#x43d;&#x44b;&#x445; &#x441;&#x435;&#x442;&#x44f;&#x445;" STYLE_REF="pre_last_node" ID="ID_236829391" CREATED="1419145939106" MODIFIED="1419148228183"/>
<node TEXT="&#x418;&#x445; &#x441;&#x443;&#x449;&#x435;&#x441;&#x442;&#x432;&#x43e;&#x432;&#x430;&#x43d;&#x438;&#x435; &#x441;&#x432;&#x44f;&#x437;&#x430;&#x43d;&#x43e; &#x441; &#x434;&#x435;&#x444;&#x438;&#x446;&#x438;&#x442;&#x43e;&#x43c; IP-&#x430;&#x434;&#x440;&#x435;&#x441;&#x43e;&#x432;" STYLE_REF="pre_last_node" ID="ID_1730728127" CREATED="1419145968477" MODIFIED="1419148228186"/>
<node STYLE_REF="yellow_node" ID="ID_358757179" CREATED="1419145753711" MODIFIED="1419145877593"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1044;&#1088;&#1091;&#1075;&#1080;&#1077; &#1085;&#1072;&#1079;&#1074;&#1099;
    </p>
  </body>
</html>
</richcontent>
<node TEXT="&#x412;&#x43d;&#x443;&#x442;&#x440;&#x435;&#x43d;&#x43d;&#x438;&#x439;" STYLE_REF="pre_last_node" ID="ID_268154645" CREATED="1419145838688" MODIFIED="1419145882519"/>
<node TEXT="&#x412;&#x43d;&#x443;&#x442;&#x440;&#x435;&#x441;&#x435;&#x442;&#x435;&#x432;&#x44b;&#x439;" STYLE_REF="pre_last_node" ID="ID_1047991900" CREATED="1419145853624" MODIFIED="1419145882526"/>
<node TEXT="&#x41b;&#x43e;&#x43a;&#x430;&#x43b;&#x44c;&#x43d;&#x44b;&#x439;" STYLE_REF="pre_last_node" ID="ID_263454678" CREATED="1419145858331" MODIFIED="1419145882529"/>
<node TEXT="&#x421;&#x435;&#x440;&#x44b;&#x439;" STYLE_REF="pre_last_node" ID="ID_909574206" CREATED="1419145869474" MODIFIED="1419145882531"/>
</node>
</node>
<node TEXT="&#x414;&#x438;&#x430;&#x43f;&#x430;&#x437;&#x43e;&#x43d;&#x44b;" STYLE_REF="second_node" POSITION="right" ID="ID_1745566819" CREATED="1419146039493" MODIFIED="1419154850240" HGAP="40" VSHIFT="-90">
<edge COLOR="#660066"/>
<node TEXT="IPv4" STYLE_REF="yellow_node" ID="ID_1281075246" CREATED="1419146048871" MODIFIED="1419146155650">
<node TEXT="10.0.0.0 &#x2014; 10.255.255.255 /8" STYLE_REF="pre_last_node" ID="ID_1282550739" CREATED="1419146080372" MODIFIED="1419146162650"/>
<node TEXT="172.16.0.0 &#x2014; 172.31.255.255 /12" STYLE_REF="pre_last_node" ID="ID_1358602379" CREATED="1419146085311" MODIFIED="1419146162656"/>
<node TEXT="192.168.0.0 &#x2014; 192.168.255.255 / 16" STYLE_REF="pre_last_node" ID="ID_1131567421" CREATED="1419146095584" MODIFIED="1419146162658"/>
</node>
<node TEXT="IPv6" STYLE_REF="yellow_node" ID="ID_131711616" CREATED="1419146054955" MODIFIED="1419146155642">
<node TEXT="fc00::/7" STYLE_REF="pre_last_node" ID="ID_726157110" CREATED="1419146117265" MODIFIED="1419146162660"/>
</node>
</node>
</node>
</map>
