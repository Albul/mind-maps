<map version="freeplane 1.3.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="&#x411;&#x430;&#x437;&#x43e;&#x432;&#x44b;&#x435; &#x441;&#x435;&#x442;&#x435;&#x432;&#x44b;&#x435; &#x442;&#x435;&#x445;&#x43d;&#x43e;&#x43b;&#x43e;&#x433;&#x438;&#x438;" STYLE_REF="main_node" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1419259395904" VGAP="34">
<hook NAME="AutomaticEdgeColor" COUNTER="6"/>
<hook URI="../../Images/network-1-large.jpg" SIZE="1.0" NAME="ExternalObject"/>
<hook NAME="MapStyle">
    <properties show_icon_for_attributes="true" show_note_icons="true"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
<stylenode TEXT="second_node" BACKGROUND_COLOR="#00cccc" STYLE="bubble">
<font SIZE="14"/>
</stylenode>
<stylenode TEXT="pre_last_node" BACKGROUND_COLOR="#99ff99" STYLE="bubble">
<edge STYLE="bezier" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="yellow_node" BACKGROUND_COLOR="#ffff66" STYLE="bubble"/>
<stylenode TEXT="orange_node" BACKGROUND_COLOR="#ff9900" STYLE="bubble">
<font BOLD="true"/>
</stylenode>
<stylenode TEXT="last_node" BACKGROUND_COLOR="#ffffff">
<edge STYLE="bezier" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="main_node" BACKGROUND_COLOR="#ccccff">
<font SIZE="18" BOLD="true"/>
<edge STYLE="sharp_bezier" WIDTH="4"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<edge COLOR="#660000"/>
<node TEXT="Ethernet" STYLE_REF="second_node" POSITION="right" ID="ID_1614314378" CREATED="1418973829301" MODIFIED="1418975360257" VGAP="12">
<edge COLOR="#660000"/>
<hook URI="../../Images/cable-ethernet.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x41a;&#x43e;&#x43c;&#x43f;&#x43e;&#x43d;&#x435;&#x43d;&#x442;&#x44b;" STYLE_REF="orange_node" ID="ID_865175494" CREATED="1418973858033" MODIFIED="1418983707240" VGAP="13">
<node TEXT="&#x41c;&#x43e;&#x441;&#x442;" STYLE_REF="yellow_node" ID="ID_52264189" CREATED="1418975699995" MODIFIED="1418984450132">
<hook URI="../../Images/bridge-1.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x41d;&#x435; &#x43a;&#x43e;&#x43f;&#x438;&#x440;&#x443;&#x435;&#x442; &#x448;&#x443;&#x43c;&#x44b;" STYLE_REF="pre_last_node" ID="ID_1939527192" CREATED="1418983497425" MODIFIED="1418983532695"/>
<node TEXT="&#x41f;&#x43e;&#x432;&#x442;&#x43e;&#x440;&#x44f;&#x435;&#x442; &#x43f;&#x430;&#x43a;&#x435;&#x442;&#x44b; eth" STYLE_REF="pre_last_node" ID="ID_1910552995" CREATED="1418983515096" MODIFIED="1418983532686"/>
</node>
<node TEXT="&#x418;&#x43d;&#x442;&#x435;&#x440;&#x444;&#x435;&#x439;&#x441; &#x41f;&#x41a;" STYLE_REF="yellow_node" ID="ID_975562244" CREATED="1418975709938" MODIFIED="1418975924374"/>
<node TEXT="&#x422;&#x440;&#x430;&#x43d;&#x441;&#x438;&#x432;&#x435;&#x440;" STYLE_REF="yellow_node" ID="ID_234832669" CREATED="1418975716592" MODIFIED="1418983707232">
<node TEXT="&#x41f;&#x440;&#x438;&#x43d;&#x438;&#x43c;&#x430;&#x435;&#x442; &#x430;&#x43d;&#x430;&#x43b;&#x43e;&#x433;&#x43e;&#x432;&#x44b;&#x435; &#x441;&#x438;&#x433;&#x43d;&#x430;&#x43b;&#x44b; &#x43e;&#x442; &#x415;-&#x43a;&#x430;&#x431;&#x435;&#x43b;&#x44f;" STYLE_REF="pre_last_node" ID="ID_1135122129" CREATED="1418983759347" MODIFIED="1418983858883"/>
<node TEXT="&#x41f;&#x440;&#x435;&#x43e;&#x431;&#x440;&#x430;&#x437;&#x43e;&#x432;&#x44b;&#x432;&#x430;&#x435;&#x442; &#x438;&#x445; &#x432; &#x446;&#x438;&#x444;&#x440;&#x43e;&#x432;&#x44b;&#x435; &#x434;&#x430;&#x43d;&#x43d;&#x44b;&#x435;" STYLE_REF="pre_last_node" ID="ID_1433654280" CREATED="1418983779609" MODIFIED="1418983858878"/>
<node TEXT="&#x41f;&#x435;&#x440;&#x435;&#x434;&#x430;&#x435;&#x442; &#x446;&#x438;&#x444;&#x440;&#x43e;&#x432;&#x44b;&#x435; &#x434;&#x430;&#x43d;&#x43d;&#x44b;&#x435; &#x438;&#x43d;&#x442;&#x435;&#x440;&#x444;&#x435;&#x439;&#x441;&#x443;" STYLE_REF="pre_last_node" ID="ID_775546770" CREATED="1418983795189" MODIFIED="1418983858874"/>
<node TEXT="&#x418; &#x43d;&#x430;&#x43e;&#x431;&#x43e;&#x440;&#x43e;&#x442;: &#x41f;&#x41a; &#x446;&#x438;&#x444; &#x434;&#x430;&#x43d;&#x43d;&#x44b;&#x435; -&gt; &#x43a;&#x430;&#x431;&#x435;&#x43b;&#x44c; &#x430;&#x43d;&#x430;&#x43b;&#x43e;&#x433; &#x434;&#x430;&#x43d;&#x43d;&#x44b;&#x435;" STYLE_REF="pre_last_node" ID="ID_1025324566" CREATED="1418983816002" MODIFIED="1418983858865"/>
</node>
<node TEXT="&#x41f;&#x43e;&#x432;&#x442;&#x43e;&#x440;&#x438;&#x442;&#x435;&#x43b;&#x44c;" STYLE_REF="yellow_node" ID="ID_1285680396" CREATED="1418975723303" MODIFIED="1418984902423">
<hook URI="../../Images/parrot-1.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x41d;&#x435;&#x434;&#x43e;&#x441;&#x442;&#x430;&#x442;&#x43a;&#x438;" STYLE_REF="pre_last_node" ID="ID_196133633" CREATED="1418983568455" MODIFIED="1418983692678">
<icon BUILTIN="subtraction"/>
<node TEXT="&#x41a;&#x43e;&#x43f;&#x438;&#x440;&#x443;&#x435;&#x442; &#x448;&#x443;&#x43c;&#x44b;" STYLE_REF="yellow_node" ID="ID_1335750515" CREATED="1418983667599" MODIFIED="1419259458057"/>
<node TEXT="&#x427;&#x430;&#x441;&#x442;&#x43e; &#x432;&#x44b;&#x445;&#x43e;&#x434;&#x44f;&#x442; &#x438;&#x437; &#x441;&#x442;&#x440;&#x43e;&#x44f;" STYLE_REF="yellow_node" ID="ID_1176745892" CREATED="1418983676875" MODIFIED="1419259458059"/>
</node>
<node TEXT="&#x41f;&#x435;&#x440;&#x435;&#x434;&#x430;&#x435;&#x442; &#x441;&#x438;&#x433;&#x43d;&#x430;&#x43b; &#x43e;&#x442; &#x43e;&#x434;&#x43d;&#x43e;&#x433;&#x43e; &#x43a;&#x430;&#x431;&#x435;&#x43b;&#x44f; &#x43a; &#x434;&#x440;&#x443;&#x433;&#x43e;&#x43c;&#x443;" STYLE_REF="pre_last_node" ID="ID_193301162" CREATED="1418983573119" MODIFIED="1418983692697"/>
<node TEXT="&#x422;&#x43e;&#x43b;&#x44c;&#x43a;&#x43e; 2 &#x43f;&#x43e;&#x432;&#x442;&#x43e;&#x440;&#x438;&#x442;&#x435;&#x43b;&#x44f; &#x43c;&#x43e;&#x433;&#x443;&#x442; &#x431;&#x44b;&#x442;&#x44c; &#x43c;&#x435;&#x436;&#x434;&#x443;&#xa;&#x43b;&#x44e;&#x431;&#x44b;&#x43c;&#x438; 2-&#x43c;&#x44f; &#x41f;&#x41a;" STYLE_REF="pre_last_node" ID="ID_1535149198" CREATED="1418983595558" MODIFIED="1418983692712"/>
<node TEXT="&#x41c;&#x410;&#x425; &#x434;&#x43b;&#x438;&#x43d;&#x430; eth v1-v2 = 1500 &#x43c;" STYLE_REF="pre_last_node" ID="ID_1020047161" CREATED="1418983624033" MODIFIED="1418983692725"/>
<node TEXT="&#x414;&#x43e;&#x441;&#x442;&#x430;&#x442;&#x43e;&#x447;&#x43d;&#x43e; &#x434;&#x435;&#x448;&#x43e;&#x432;&#x44b;&#x439;" STYLE_REF="pre_last_node" ID="ID_340469929" CREATED="1418983644761" MODIFIED="1418983692729">
<icon BUILTIN="addition"/>
</node>
</node>
<node TEXT="&#x41a;&#x430;&#x431;&#x435;&#x43b;&#x44c;" STYLE_REF="yellow_node" ID="ID_165711846" CREATED="1418975730622" MODIFIED="1418976083847"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1044;&#1083;&#1103; eth v1-v2
    </p>
  </body>
</html>
</richcontent>
<hook URI="../../Images/cable-coaxial.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x41c;&#x410;&#x425; &#x434;&#x43b;&#x438;&#x43d;&#x430; 500 &#x43c;" STYLE_REF="pre_last_node" ID="ID_819015129" CREATED="1418975779063" MODIFIED="1418975931389"/>
<node TEXT="&#x41f;&#x43e;&#x43b;&#x43d;&#x43e;&#x441;&#x442;&#x44c;&#x44e; &#x43f;&#x430;&#x441;&#x441;&#x438;&#x432;&#x43d;&#x44b;&#x439;" STYLE_REF="pre_last_node" ID="ID_480148764" CREATED="1418975790028" MODIFIED="1418975931384"/>
<node TEXT="&#x41a;&#x43e;&#x430;&#x43a;&#x441;&#x438;&#x430;&#x43b;&#x44c;&#x43d;&#x44b;&#x439; &#x438;&#x43b;&#x438; &#x415;-&#x43a;&#x430;&#x431;&#x435;&#x43b;&#x44c;" STYLE_REF="pre_last_node" ID="ID_1260979232" CREATED="1418975800466" MODIFIED="1418975982070">
<node TEXT="1. &#x412;&#x43d;&#x435;&#x448;&#x43d;&#x44f;&#x44f; &#x438;&#x437;&#x43e;&#x43b;&#x44f;&#x446;&#x438;&#x43e;&#x43d;&#x43d;&#x430;&#x44f; &#x43e;&#x431;&#x43e;&#x43b;&#x43e;&#x447;&#x43a;&#x430;" STYLE_REF="yellow_node" ID="ID_1640049767" CREATED="1418975836866" MODIFIED="1419259448810"/>
<node TEXT="2. &#x41f;&#x43e;&#x43b;&#x438;&#x454;&#x442;&#x438;&#x43b;&#x435;&#x43d;&#x43e;&#x432;&#x430;&#x44f; &#x43e;&#x431;&#x43e;&#x43b;&#x43e;&#x447;&#x43a;&#x430;" STYLE_REF="yellow_node" ID="ID_992137742" CREATED="1418975854411" MODIFIED="1419259445417"/>
<node TEXT="3. &#x41c;&#x435;&#x442;&#x430;&#x43b;&#x438;&#x447;&#x435;&#x441;&#x43a;&#x430;&#x44f; &#x43e;&#x431;&#x43e;&#x43b;&#x43e;&#x447;&#x43a;&#x430;" STYLE_REF="yellow_node" ID="ID_1646654573" CREATED="1418975872778" MODIFIED="1419259445419"/>
<node TEXT="4. &#x426;&#x435;&#x43d;&#x442;&#x440;&#x430;&#x43b;&#x44c;&#x43d;&#x44b;&#x439; &#x43f;&#x440;&#x43e;&#x432;&#x43e;&#x434;" STYLE_REF="yellow_node" ID="ID_719742257" CREATED="1418975887193" MODIFIED="1419259445420"/>
</node>
</node>
</node>
<node TEXT="&#x421;&#x432;&#x43e;&#x439;&#x441;&#x442;&#x432;&#x430;" STYLE_REF="orange_node" ID="ID_1062485064" CREATED="1418973873283" MODIFIED="1418975356688">
<node TEXT="&#x41e;&#x431;&#x43d;&#x430;&#x440;&#x443;&#x436;&#x435;&#x43d;&#x438;&#x435; &#x43a;&#x43e;&#x43b;&#x438;&#x437;&#x438;&#x439; &#x438; &#x432;&#x43e;&#x441;&#x442;&#x430;&#x43d;&#x43e;&#x432;&#x43b;&#x435;&#x43d;&#x438;&#x435;" STYLE_REF="pre_last_node" ID="ID_1613039358" CREATED="1418974621979" MODIFIED="1418974961013"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1050;&#1086;&#1075;&#1076;&#1072; 2 &#1090;&#1088;&#1072;&#1085;&#1089;&#1080;&#1074;&#1077;&#1088;&#1072; &#1085;&#1072;&#1095;&#1080;&#1085;&#1072;&#1102;&#1090; &#1087;&#1077;&#1088;&#1077;&#1076;&#1072;&#1095;&#1091;
    </p>
    <p>
      &#1086;&#1076;&#1085;&#1086;&#1074;&#1088;&#1077;&#1084;&#1077;&#1085;&#1085;&#1086;
    </p>
  </body>
</html>
</richcontent>
<node TEXT="&#x415;&#x441;&#x43b;&#x438; &#x43a;&#x43e;&#x43b;&#x438;&#x437;&#x438;&#x44f; &#x43f;&#x440;&#x43e;&#x438;&#x437;&#x43e;&#x448;&#x43b;&#x430;" STYLE_REF="yellow_node" ID="ID_1731926265" CREATED="1418974804511" MODIFIED="1418974970637">
<node TEXT="1. &#x410;&#x432;&#x430;&#x440;&#x438;&#x439;&#x43d;&#x430;&#x44f; &#x43e;&#x441;&#x442;&#x430;&#x43d;&#x43e;&#x432;&#x43a;&#x430; &#x43f;&#x435;&#x440;&#x435;&#x434;&#x430;&#x447;&#x438;" STYLE_REF="pre_last_node" ID="ID_100396426" CREATED="1418974839551" MODIFIED="1419259438274"/>
<node TEXT="2. &#x414;&#x432;&#x43e;&#x438;&#x447;&#x43d;&#x430;&#x44f; &#x44d;&#x43a;&#x441;&#x43f;&#x43e;&#x442;&#x435;&#x43d;&#x446;&#x438;&#x430;&#x43b;&#x44c;&#x43d;&#x430;&#x44f; &#x441;&#x43b;&#x443;&#x447;&#x430;&#x439;&#x43d;&#x430;&#x44f; &#x437;&#x430;&#x434;&#x435;&#x440;&#x436;&#x43a;&#x430;.&#xa;&#x41f;&#x43e;&#x441;&#x43b;&#x435; &#x447;&#x435;&#x433;&#x43e; &#x43e;&#x431;&#x440;&#x430;&#x442;&#x43d;&#x43e; &#x43f;&#x43e;&#x43f;&#x44b;&#x442;&#x43a;&#x430; &#x43f;&#x435;&#x440;&#x435;&#x434;&#x430;&#x447;&#x438;" STYLE_REF="pre_last_node" ID="ID_1059888687" CREATED="1418974853311" MODIFIED="1419259438278"/>
</node>
<node TEXT="&#x41a;&#x430;&#x436;&#x434;&#x44b;&#x439; &#x441;&#x43b;&#x435;&#x434;&#x438;&#x442; &#x437;&#x430; &#x441;&#x43e;&#x441;&#x442;&#x43e;&#x44f;&#x43d;&#x438;&#x435;&#x43c;&#xa;&#x43a;&#x430;&#x431;&#x435;&#x43b;&#x44f; &#x447;&#x442;&#x43e;&#x431;&#x44b; &#x443;&#x437;&#x43d;&#x430;&#x442;&#x44c; &#x43e; &#x43f;&#x43e;&#x43c;&#x435;&#x445;&#x430;&#x445;" STYLE_REF="yellow_node" ID="ID_543381332" CREATED="1418974812785" MODIFIED="1418974970656"/>
</node>
<node TEXT="&#x41c;&#x43d;&#x43e;&#x436;&#x435;&#x441;&#x442;&#x432;&#x435;&#x43d;&#x43d;&#x44b;&#x439; &#x434;&#x43e;&#x441;&#x442;&#x443;&#x43f; &#x441; &#x43a;&#x43e;&#x43d;&#x442;&#x440;&#x43e;&#x43b;&#x435;&#x43c; &#x43d;&#x435;&#x441;&#x443;&#x449;&#x435;&#x439;" STYLE_REF="pre_last_node" ID="ID_624048349" CREATED="1418974923267" MODIFIED="1418974961035"/>
<node TEXT="&#x41c;&#x435;&#x445;&#x430;&#x43d;&#x438;&#x437;&#x43c; &#x43d;&#x435; &#x433;&#x430;&#x440;&#x430;&#x43d;&#x442;&#x438;&#x440;&#x43e;&#x432;&#x430;&#x43d;&#x43d;&#x43e;&#x439; &#x434;&#x43e;&#x441;&#x442;&#x430;&#x432;&#x43a;&#x438;" STYLE_REF="pre_last_node" ID="ID_967614612" CREATED="1418974687170" MODIFIED="1418975356683" HGAP="10"/>
<node TEXT="&#x428;&#x438;&#x440;&#x43e;&#x43a;&#x43e;&#x432;&#x435;&#x449;&#x430;&#x442;&#x435;&#x43b;&#x44c;&#x43d;&#x430;&#x44f;" STYLE_REF="pre_last_node" ID="ID_183011846" CREATED="1418974701512" MODIFIED="1418974961030"/>
</node>
<node TEXT="&#x410;&#x434;&#x440;&#x435;&#x441;&#x430;&#x446;&#x438;&#x44f;" STYLE_REF="orange_node" ID="ID_1219274633" CREATED="1418973878500" MODIFIED="1418975360246">
<node TEXT="&#x424;&#x438;&#x437;&#x438;&#x447;&#x435;&#x441;&#x43a;&#x438;&#x439; &#x438;&#x43b;&#x438; &#x41c;&#x410;&#x421; &#x430;&#x434;&#x440;&#x435;&#x441;&#xa;48 &#x431;&#x438;&#x442;&#x43d;&#x43e;&#x435; &#x447;&#x438;&#x441;&#x43b;&#x43e;" STYLE_REF="pre_last_node" ID="ID_1371278875" CREATED="1418975240424" MODIFIED="1418975348266"/>
<node TEXT="&#x41c;&#x43e;&#x436;&#x435;&#x442; &#x431;&#x44b;&#x442;&#x44c; &#x438;&#x437;&#x43c;&#x435;&#x43d;&#x435;&#x43d; &#x41e;&#x421;" STYLE_REF="pre_last_node" ID="ID_1524424100" CREATED="1418975274779" MODIFIED="1418975348261"/>
<node TEXT="&#x422;&#x438;&#x43f;&#x44b; &#x430;&#x434;&#x440;&#x435;&#x441;&#x43e;&#x432;" STYLE_REF="pre_last_node" ID="ID_1927345427" CREATED="1418975293137" MODIFIED="1418975348251">
<node TEXT="&#x413;&#x440;&#x443;&#x43f;&#x43f;&#x43e;&#x432;&#x43e;&#x439; &#x430;&#x434;&#x440;&#x435;&#x441;" STYLE_REF="yellow_node" ID="ID_1268390767" CREATED="1418975301562" MODIFIED="1419259431394"/>
<node TEXT="&#x428;&#x438;&#x440;&#x43e;&#x43a;&#x43e;&#x432;&#x435;&#x449;&#x430;&#x442;&#x435;&#x43b;&#x44c;&#x43d;&#x44b;&#x439; &#x430;&#x434;&#x440;&#x435;&#x441;" STYLE_REF="yellow_node" ID="ID_1534496405" CREATED="1418975313312" MODIFIED="1419259431398"/>
<node TEXT="&#x424;&#x438;&#x437; &#x430;&#x434;&#x440;&#x435;&#x441; 1-&#x433;&#x43e; &#x438;&#x442;&#x440;&#x435;&#x444;&#x435;&#x439;&#x441;&#x430;" STYLE_REF="yellow_node" ID="ID_1219745087" CREATED="1418975325263" MODIFIED="1419259431400"/>
</node>
</node>
<node TEXT="&#x412;&#x438;&#x434;&#x44b;" STYLE_REF="orange_node" ID="ID_1820297183" CREATED="1418973883869" MODIFIED="1418974668278">
<node TEXT="Ethernet V1-V2, 10 &#x41c;&#x431;&#x438;&#x442;/&#x441;" STYLE_REF="pre_last_node" ID="ID_1711408303" CREATED="1418975429234" MODIFIED="1418975567231"/>
<node TEXT="Fast Ethernet, 100 &#x41c;&#x431;&#x438;&#x442;/&#x441;" STYLE_REF="pre_last_node" ID="ID_1442363157" CREATED="1418975465761" MODIFIED="1418975567242"/>
<node TEXT="Gigabit Eth, 1 &#x413;&#x431;&#x438;&#x442;/&#x441;" STYLE_REF="pre_last_node" ID="ID_1901525565" CREATED="1418975488683" MODIFIED="1418975567247"/>
<node TEXT="Ethernet 10 G, 10 &#x413;&#x431;&#x438;&#x442;/&#x441;" STYLE_REF="pre_last_node" ID="ID_798592841" CREATED="1418975504891" MODIFIED="1418975567253"/>
<node TEXT="Ethernet 40 G, 40 &#x413;&#x431;&#x438;&#x442;/&#x441;" STYLE_REF="pre_last_node" ID="ID_1567392587" CREATED="1418975526601" MODIFIED="1418975567258"/>
</node>
<node TEXT="&#x424;&#x43e;&#x440;&#x43c;&#x430;&#x442; &#x43a;&#x430;&#x434;&#x440;&#x430;" STYLE_REF="orange_node" ID="ID_718055847" CREATED="1418973892726" MODIFIED="1418974668283">
<node TEXT="" ID="ID_861593275" CREATED="1418984088257" MODIFIED="1427445170002">
<hook URI="../../../../../D:/Documents/MindMaps/Network-Internet/Basic%20networking%20technology.RU/format.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
</node>
</node>
<node TEXT="&#x422;&#x438;&#x43f;&#x44b; &#x441;&#x432;&#x44f;&#x437;&#x438;" STYLE_REF="second_node" POSITION="left" ID="ID_30674447" CREATED="1418888390033" MODIFIED="1418985105851" VGAP="43">
<edge COLOR="#660000"/>
<node TEXT="&#x421;&#x438;&#x43c;&#x43f;&#x43b;&#x435;&#x43a;&#x441;&#x43d;&#x430;&#x44f;" STYLE_REF="orange_node" ID="ID_1864712475" CREATED="1418888431112" MODIFIED="1419259516602"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Simplex
    </p>
  </body>
</html>
</richcontent>
<hook URI="../../Images/radio.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x41f;&#x435;&#x440;&#x435;&#x434;&#x430;&#x447;&#x430; &#x438;&#x43d;&#x444;&#x43e;&#x440;&#x43c;&#x430;&#x446;&#x438;&#x438; &#x442;&#x43e;&#x43b;&#x44c;&#x43a;&#x43e;&#xa;&#x432; &#x43e;&#x434;&#x43d;&#x43e;&#x43c; &#x43d;&#x430;&#x43f;&#x440;&#x430;&#x432;&#x43b;&#x435;&#x43d;&#x438;&#x438;" STYLE_REF="pre_last_node" ID="ID_383261676" CREATED="1418888453916" MODIFIED="1418888487338"/>
<node TEXT="&#x41d;&#x430;&#x43f;&#x440;&#x438;&#x43c;&#x435;&#x440;: &#x440;&#x430;&#x434;&#x438;&#x43e;" STYLE_REF="pre_last_node" ID="ID_242149003" CREATED="1418888477575" MODIFIED="1418888487327"/>
</node>
<node TEXT="&#x41f;&#x43e;&#x43b;&#x443;&#x434;&#x443;&#x43f;&#x43b;&#x435;&#x43a;&#x441;&#x43d;&#x430;&#x44f;" STYLE_REF="orange_node" ID="ID_1559235814" CREATED="1418888497738" MODIFIED="1419259516610"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Half-duplex
    </p>
  </body>
</html>
</richcontent>
<hook URI="../../Images/radio-set.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x41f;&#x435;&#x440;&#x435;&#x434;&#x430;&#x447;&#x430; &#x438;&#x43d;&#x444;. &#x43f;&#x43e; &#x43e;&#x434;&#x43d;&#x43e;&#x43c;&#x443; &#x43a;&#x430;&#x43d;&#x430;&#x43b;&#x443; &#x432; &#x43e;&#x431;&#x43e;&#x438;&#x445;&#xa;&#x43d;&#x430;&#x43f;&#x440;&#x430;&#x432;&#x43b;&#x435;&#x43d;&#x438;&#x44f;&#x445; &#x441; &#x440;&#x430;&#x437;&#x434;&#x435;&#x43b;&#x435;&#x43d;&#x438;&#x435;&#x43c; &#x432;&#x43e; &#x432;&#x440;&#x435;&#x43c;&#x435;&#x43d;&#x438;" STYLE_REF="pre_last_node" ID="ID_518546108" CREATED="1418888522730" MODIFIED="1418888591623"/>
<node TEXT="&#x41f;&#x440;&#x438;&#x43c;&#x435;&#x440;: &#x440;&#x430;&#x446;&#x438;&#x44f;" STYLE_REF="pre_last_node" ID="ID_549583060" CREATED="1418888572740" MODIFIED="1418888591650"/>
</node>
<node TEXT="&#x414;&#x443;&#x43f;&#x43b;&#x435;&#x43a;&#x441;&#x43d;&#x430;&#x44f;" STYLE_REF="orange_node" ID="ID_823184736" CREATED="1418888595024" MODIFIED="1419259516614"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1055;&#1086;&#1083;&#1085;&#1086;&#1076;&#1091;&#1087;&#1083;&#1077;&#1082;&#1089;&#1085;&#1072;&#1103;
    </p>
  </body>
</html>
</richcontent>
<hook URI="../../Images/phone.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x41f;&#x435;&#x440;&#x435;&#x434;&#x430;&#x447;&#x430; &#x438; &#x43f;&#x440;&#x438;&#x435;&#x43c; &#x43e;&#x434;&#x43d;&#x43e;&#x432;&#x440;&#x435;&#x43c;&#x435;&#x43d;&#x43d;&#x43e;" STYLE_REF="pre_last_node" ID="ID_1091727968" CREATED="1418888622545" MODIFIED="1418888673749"/>
<node TEXT="2 &#x438;&#x43b;&#x438; 1 &#x43a;&#x430;&#x43d;&#x430;&#x43b; &#x441;&#x432;&#x44f;&#x437;&#x438;" STYLE_REF="pre_last_node" ID="ID_885856123" CREATED="1418888638673" MODIFIED="1418888673755"/>
<node TEXT="&#x41f;&#x440;&#x438;&#x43c;&#x435;&#x440;: &#x442;&#x435;&#x43b;&#x435;&#x444;&#x43e;&#x43d;" STYLE_REF="pre_last_node" ID="ID_1285285399" CREATED="1418888655226" MODIFIED="1418888673739"/>
</node>
</node>
<node TEXT="&#x422;&#x438;&#x43f;&#x44b; &#x441;&#x435;&#x442;&#x435;&#x439;" STYLE_REF="second_node" POSITION="left" ID="ID_1408000627" CREATED="1418886752847" MODIFIED="1418972967660" VGAP="22">
<edge COLOR="#660000"/>
<node TEXT="&#x41a;&#x43e;&#x43c;&#x443;&#x442;&#x430;&#x446;&#x438;&#x44f; &#x43a;&#x430;&#x43d;&#x430;&#x43b;&#x43e;&#x432;" STYLE_REF="orange_node" ID="ID_64205298" CREATED="1418886819891" MODIFIED="1419259516616">
<hook URI="../../Images/channel.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x41a;&#x430;&#x43d;&#x430;&#x43b; &#x43c;&#x435;&#x436;&#x434;&#x443; &#x434;&#x432;&#x443;&#x43c;&#x44f; &#x442;&#x43e;&#x447;&#x43a;&#x430;&#x43c;&#x438;" STYLE_REF="pre_last_node" ID="ID_1567234487" CREATED="1418886838505" MODIFIED="1418887302148"/>
<node TEXT="&#x422;&#x435;&#x43b;&#x435;&#x444;&#x43e;&#x43d;&#x43d;&#x430;&#x44f; &#x441;&#x435;&#x442;&#x44c; &#x421;&#x428;&#x410;" STYLE_REF="pre_last_node" ID="ID_697946401" CREATED="1418886849480" MODIFIED="1418887302098"/>
<node TEXT="&#x413;&#x430;&#x440;&#x430;&#x43d;&#x442;&#x438;&#x440;&#x43e;&#x432;&#x430;&#x43d;&#x43d;&#x430;&#x44f; &#x43f;&#x440;&#x43e;&#x43f;&#x443;&#x441;&#x43a;&#x43d;&#x430;&#x44f;&#xa;&#x441;&#x43f;&#x43e;&#x441;&#x43e;&#x431;&#x43d;&#x43e;&#x441;&#x442;&#x44c;" STYLE_REF="pre_last_node" ID="ID_219602074" CREATED="1418886863229" MODIFIED="1418887517328">
<icon BUILTIN="addition"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1044;&#1083;&#1103; &#1090;&#1077;&#1083;&#1077;&#1092;&#1086;&#1085;&#1086;&#1074; 64 &#1050;&#1073;/&#1089;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&#x424;&#x438;&#x43a;&#x441;&#x438;&#x440;&#x43e;&#x432;&#x430;&#x43d;&#x43d;&#x430;&#x44f; &#x441;&#x442;&#x43e;&#x438;&#x43c;&#x43e;&#x441;&#x442;&#x44c; &#x437;&#x430; &#x43a;&#x430;&#x43d;&#x430;&#x43b;&#xa;&#x43d;&#x435; &#x437;&#x430;&#x432;&#x438;&#x441;&#x438;&#x43c;&#x43e; &#x43e;&#x442; &#x442;&#x440;&#x430;&#x444;&#x438;&#x43a;&#x430;" STYLE_REF="pre_last_node" ID="ID_1629319272" CREATED="1418887273482" MODIFIED="1418887302044"/>
</node>
<node TEXT="&#x41a;&#x43e;&#x43c;&#x443;&#x442;&#x430;&#x446;&#x438;&#x44f; &#x43f;&#x430;&#x43a;&#x435;&#x442;&#x43e;&#x432;" STYLE_REF="orange_node" ID="ID_728147730" CREATED="1418886830256" MODIFIED="1419259516619">
<hook URI="../../Images/box.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x412;&#x435;&#x441;&#x44c; &#x442;&#x440;&#x430;&#x444;&#x438;&#x43a; &#x434;&#x435;&#x43b;&#x438;&#x442;&#x441;&#x44f; &#x43d;&#x430; &#x43f;&#x430;&#x43a;&#x435;&#x442;&#x44b;" STYLE_REF="pre_last_node" ID="ID_1630666975" CREATED="1418887334012" MODIFIED="1418887396442"/>
<node TEXT="&#x41a;&#x430;&#x436;&#x434;&#x44b;&#x439; &#x443;&#x437;&#x435;&#x43b; &#x43f;&#x440;&#x438;&#x43d;&#x438;&#x43c;&#x430;&#x435;&#x442; &#x43f;&#x430;&#x43a;&#x435;&#x442;&#x44b;&#xa;&#x438; &#x435;&#x441;&#x43b;&#x438; &#x43e;&#x43d;&#x438; &#x430;&#x434;&#x440;&#x435;&#x441;&#x43e;&#x432;&#x430;&#x43d;&#x44b; &#x43d;&#x435; &#x435;&#x43c;&#x443;,&#xa;&#x43e;&#x43d; &#x43f;&#x435;&#x440;&#x435;&#x441;&#x438;&#x43b;&#x430;&#x435;&#x442; &#x438;&#x445;" STYLE_REF="pre_last_node" ID="ID_186991082" CREATED="1418887349848" MODIFIED="1418887396412"/>
<node TEXT="&#x41d;&#x438;&#x437;&#x43a;&#x430;&#x44f; &#x441;&#x442;&#x43e;&#x438;&#x43c;&#x43e;&#x441;&#x442;&#x44c; &#x441;&#x435;&#x442;&#x435;&#x432;&#x44b;&#x445; &#x443;&#x441;&#x43b;&#x443;&#x433;" STYLE_REF="pre_last_node" ID="ID_1675454350" CREATED="1418887410247" MODIFIED="1418887496145">
<icon BUILTIN="addition"/>
</node>
<node TEXT="&#x411;&#x43e;&#x43b;&#x44c;&#x448;&#x43e;&#x435; &#x447;&#x438;&#x441;&#x43b;&#x43e; &#x441;&#x43e;&#x435;&#x434;&#x438;&#x43d;&#x435;&#x43d;&#x438;&#x439;&#xa;&#x43c;&#x43e;&#x436;&#x435;&#x442; &#x440;&#x430;&#x431;&#x43e;&#x442;&#x430;&#x442;&#x44c; &#x43e;&#x434;&#x43d;&#x43e;&#x432;&#x440;&#x435;&#x43c;&#x435;&#x43d;&#x43d;&#x43e;" STYLE_REF="pre_last_node" ID="ID_117374056" CREATED="1418887420343" MODIFIED="1418887501192">
<icon BUILTIN="addition"/>
</node>
<node TEXT="&#x41d;&#x435;&#x433;&#x430;&#x440;&#x430;&#x43d;&#x442;&#x438;&#x440;&#x43e;&#x432;&#x430;&#x43d;&#x43d;&#x430;&#x44f; &#x43f;&#x440;&#x43e;&#x43f;&#x443;&#x441;&#x43a;&#x43d;&#x430;&#x44f; &#x441;&#x43f;&#x43e;&#x441;&#x43e;&#x431;&#x43d;&#x43e;&#x441;&#x442;&#x44c;" STYLE_REF="pre_last_node" ID="ID_1546016627" CREATED="1418887444232" MODIFIED="1418887503584">
<icon BUILTIN="subtraction"/>
</node>
</node>
</node>
<node TEXT="&#x41e;&#x431;&#x44a;&#x435;&#x43c;&#x44b; &#x441;&#x435;&#x442;&#x435;&#x439;" STYLE_REF="second_node" POSITION="left" ID="ID_650325348" CREATED="1418972656415" MODIFIED="1418974463634" VGAP="13">
<edge COLOR="#660000"/>
<node TEXT="&#x41b;&#x43e;&#x43a;&#x430;&#x43b;&#x44c;&#x43d;&#x44b;&#x435; &#x441;&#x435;&#x442;&#x438;" STYLE_REF="orange_node" ID="ID_552980098" CREATED="1418972771459" MODIFIED="1419259516621">
<hook URI="../../Images/building-1.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x421;&#x435;&#x442;&#x44c; - &#x43f;&#x430;&#x441;&#x438;&#x432;&#x43d;&#x430;" STYLE_REF="pre_last_node" ID="ID_650246652" CREATED="1418972794984" MODIFIED="1418972870983"/>
<node TEXT="&#x421;&#x43a;&#x43e;&#x440;&#x43e;&#x441;&#x442;&#x44c;: 4 &#x41c;&#x431;&#x438;&#x442; - 2 &#x413;&#x431;&#x438;&#x442;/&#x441;" STYLE_REF="pre_last_node" ID="ID_1421198303" CREATED="1418972808267" MODIFIED="1418972870977"/>
<node TEXT="&#x41e;&#x431;&#x43b;&#x430;&#x441;&#x442;&#x44c; &#x43f;&#x43e;&#x43a;&#x440;&#x44b;&#x442;&#x438;&#x44f;: &#x437;&#x434;&#x430;&#x43d;&#x438;&#x435; &#x438;&#x43b;&#x438; &#x441;&#x442;&#x443;&#x434; &#x433;&#x43e;&#x440;&#x43e;&#x434;&#x43e;&#x43a;" STYLE_REF="pre_last_node" ID="ID_1692475892" CREATED="1418972835550" MODIFIED="1418972870965"/>
</node>
<node TEXT="&#x413;&#x43e;&#x440;&#x43e;&#x434;&#x441;&#x43a;&#x438;&#x435; &#x441;&#x435;&#x442;&#x438;" STYLE_REF="orange_node" ID="ID_804930109" CREATED="1418972758464" MODIFIED="1419259516624">
<hook URI="../../Images/city.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x41e;&#x431;&#x43b;&#x430;&#x441;&#x442;&#x44c; &#x440;&#x430;&#x437;&#x43c;&#x435;&#x440;&#x43e;&#x43c; &#x441; &#x431;&#x43e;&#x43b;&#x44c;&#x448;&#x43e;&#x439; &#x433;&#x43e;&#x440;&#x43e;&#x434;" STYLE_REF="pre_last_node" ID="ID_832221100" CREATED="1418972884799" MODIFIED="1418972955319"/>
<node TEXT="&#x421;&#x43a;&#x43e;&#x440;&#x43e;&#x441;&#x442;&#x44c;: 56 - 100 &#x41c;&#x431;&#x438;&#x442;/&#x441;" STYLE_REF="pre_last_node" ID="ID_950666525" CREATED="1418972901935" MODIFIED="1418972955333"/>
<node TEXT="&#x410;&#x43a;&#x442;&#x438;&#x432;&#x43d;&#x44b;&#x435; &#x43a;&#x43e;&#x43c;&#x43c;&#x443;&#x442;&#x438;&#x440;&#x443;&#x44e;&#x449;&#x438;&#x435; &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442;&#x44b;" STYLE_REF="pre_last_node" ID="ID_747095206" CREATED="1418972929111" MODIFIED="1418972955339"/>
</node>
<node TEXT="&#x413;&#x43b;&#x43e;&#x431;&#x430;&#x43b;&#x44c;&#x43d;&#x44b;&#x435; &#x441;&#x435;&#x442;&#x438;" STYLE_REF="orange_node" ID="ID_1297289948" CREATED="1418972687139" MODIFIED="1419259516627">
<hook URI="../../Images/earth.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x411;&#x43e;&#x43b;&#x44c;&#x448;&#x438;&#x435; &#x440;&#x430;&#x441;&#x442;&#x43e;&#x44f;&#x43d;&#x438;&#x439;" STYLE_REF="pre_last_node" ID="ID_1539224229" CREATED="1418972697512" MODIFIED="1418972955344"/>
<node TEXT="&#x421;&#x43a;&#x43e;&#x440;&#x43e;&#x441;&#x442;&#x44c;: 9.6 - 45 &#x41c;&#x431;&#x438;&#x442;/&#x441;" STYLE_REF="pre_last_node" ID="ID_1299703821" CREATED="1418972708340" MODIFIED="1418972955352"/>
<node TEXT="&#x413;&#x440;&#x443;&#x43f;&#x43f;&#x430; &#x441;&#x43b;&#x43e;&#x436;&#x43d;&#x44b;&#x445; &#x43c;&#x430;&#x440;&#x448;&#x440;&#x443;&#x442;&#x438;&#x437;&#x430;&#x442;&#x43e;&#x440;&#x43e;&#x432; &#x43f;&#x430;&#x43a;&#x435;&#x442;&#x43e;&#x432;" STYLE_REF="pre_last_node" ID="ID_1465363140" CREATED="1418972731101" MODIFIED="1418972955358"/>
</node>
</node>
<node TEXT="ProNET" STYLE_REF="second_node" POSITION="right" ID="ID_893400736" CREATED="1418973098820" MODIFIED="1418973128435">
<edge COLOR="#660000"/>
<node TEXT="&#x422;&#x43e;&#x43f;&#x43e;&#x43b;&#x43e;&#x433;&#x438;&#x44f; &#x441;&#x435;&#x442;&#x438; - &#x43a;&#x43e;&#x43b;&#x44c;&#x446;&#x43e;" STYLE_REF="pre_last_node" ID="ID_1296975445" CREATED="1418973130041" MODIFIED="1418973424088"/>
<node TEXT="&#x422;&#x435;&#x445;&#x43d;&#x43e;&#x43b;&#x43e;&#x433;&#x438;&#x44f; &#x434;&#x43e;&#x441;&#x442;&#x443;&#x43f;&#x430; - &#x43f;&#x435;&#x440;&#x435;&#x434;&#x430;&#x447;&#x430; &#x43c;&#x430;&#x440;&#x43a;&#x435;&#x440;&#x430;" STYLE_REF="pre_last_node" ID="ID_684014013" CREATED="1418973249880" MODIFIED="1418973424065"/>
<node TEXT="&#x410;&#x434;&#x440;&#x435;&#x441;&#x430; &#x443;&#x441;&#x442;&#x430;&#x43d;&#x430;&#x432;&#x43b;&#x438;&#x432;&#x430;&#x44e;&#x442; &#x432;&#x440;&#x443;&#x447;&#x43d;&#x443;&#x44e;" STYLE_REF="pre_last_node" ID="ID_1018215720" CREATED="1418973267108" MODIFIED="1418973424036"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1054;&#1090; 0 &#1076;&#1086; 255
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&#x424;&#x438;&#x437;&#x438;&#x447;&#x435;&#x441;&#x43a;&#x43e;&#x435; &#x441;&#x43e;&#x435;&#x434;&#x438;&#x43d;&#x435;&#x43d;&#x438;&#x435; &#x43e;&#x43f;&#x440;&#x435;&#x434;&#x438;&#x43b;&#x44f;&#x435;&#x442; &#x43f;&#x43e;&#x441;&#x43b;&#x435;&#x434;&#x43e;&#x432;&#x430;&#x442;&#x435;&#x43b;&#x44c;&#x43d;&#x43e;&#x441;&#x442;&#x44c;&#xa;&#x43f;&#x43e; &#x43a;&#x43e;&#x442;&#x43e;&#x440;&#x43e;&#x439; &#x43f;&#x435;&#x440;&#x435;&#x434;&#x430;&#x435;&#x442;&#x441;&#x44f; &#x43c;&#x430;&#x440;&#x43a;&#x435;&#x440;" STYLE_REF="pre_last_node" ID="ID_1227698542" CREATED="1418973310090" MODIFIED="1418973424007"/>
<node TEXT="&#x41f;&#x41a; &#x43c;&#x43e;&#x436;&#x435;&#x442; &#x431;&#x44b;&#x442;&#x44c; &#x432; 3&#x445; &#x440;&#x435;&#x436;&#x438;&#x43c;&#x430;&#x445;" STYLE_REF="pre_last_node" ID="ID_868481898" CREATED="1418973358741" MODIFIED="1418973423992">
<node TEXT="&#x420;&#x435;&#x436;&#x438;&#x43c; &#x43a;&#x43e;&#x43f;&#x438;&#x440;&#x43e;&#x432;&#x430;&#x43d;&#x438;&#x44f;" STYLE_REF="yellow_node" ID="ID_166077848" CREATED="1418973376376" MODIFIED="1419259404266"/>
<node TEXT="&#x420;&#x435;&#x436; &#x43f;&#x435;&#x440;&#x435;&#x434;&#x430;&#x447;&#x438; (&#x43c;&#x430;&#x440;&#x43a;&#x435;&#x440;&#x430;)" STYLE_REF="yellow_node" ID="ID_1709503125" CREATED="1418973385133" MODIFIED="1419259404270"/>
<node TEXT="&#x420;&#x435;&#x436; &#x432;&#x43e;&#x441;&#x442;&#x430;&#x43d;&#x43e;&#x432;&#x43b;&#x435;&#x43d;&#x438;&#x44f;" STYLE_REF="yellow_node" ID="ID_1576895495" CREATED="1418973398617" MODIFIED="1419259404272"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1050;&#1086;&#1075;&#1076;&#1072; &#1084;&#1072;&#1088;&#1082;&#1077;&#1088; &#1091;&#1090;&#1088;&#1072;&#1095;&#1077;&#1085;&#1086;. &#1050;&#1072;&#1082;&#1072;&#1103;&#1090;&#1086; &#1080;&#1079; &#1084;&#1072;&#1096;&#1080;&#1085; &#1079;&#1072;&#1087;&#1091;&#1089;&#1082;&#1072;&#1077;&#1090;
    </p>
    <p>
      &#1088;&#1072;&#1085;&#1076;&#1086;&#1084;&#1085;&#1080;&#1081; &#1090;&#1072;&#1081;&#1084;&#1077;&#1088;. &#1055;&#1086; &#1080;&#1089;&#1090;&#1077;&#1095;&#1077;&#1085;&#1080;&#1102; &#1082;&#1086;&#1090;&#1086;&#1088;&#1086;&#1075;&#1086; &#1084;&#1072;&#1096;&#1080;&#1085;&#1072;
    </p>
    <p>
      &#1087;&#1088;&#1080;&#1089;&#1074;&#1072;&#1080;&#1074;&#1072;&#1077;&#1090; &#1089;&#1077;&#1073;&#1077; (&#1085;&#1086;&#1074;&#1099;&#1081;) &#1084;&#1072;&#1088;&#1082;&#1077;&#1088;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
</node>
</map>
