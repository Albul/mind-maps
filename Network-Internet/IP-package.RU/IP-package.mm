<map version="freeplane 1.3.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="IP-&#x43f;&#x430;&#x43a;&#x435;&#x442;" STYLE_REF="main_node" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1419258914638">
<hook NAME="AutomaticEdgeColor" COUNTER="3"/>
<hook URI="../../Images/box.jpg" SIZE="1.0" NAME="ExternalObject"/>
<hook NAME="MapStyle">
    <properties show_icon_for_attributes="true" show_note_icons="true"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
<stylenode TEXT="second_node" BACKGROUND_COLOR="#00cccc" STYLE="bubble">
<font SIZE="14"/>
</stylenode>
<stylenode TEXT="pre_last_node" BACKGROUND_COLOR="#99ff99" STYLE="bubble">
<edge STYLE="bezier" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="yellow_node" BACKGROUND_COLOR="#ffff66" STYLE="bubble"/>
<stylenode TEXT="orange_node" BACKGROUND_COLOR="#ff9900" STYLE="bubble">
<font BOLD="true"/>
</stylenode>
<stylenode TEXT="last_node" BACKGROUND_COLOR="#ffffff">
<edge STYLE="bezier" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="main_node" BACKGROUND_COLOR="#ccccff">
<font SIZE="18" BOLD="true"/>
<edge STYLE="sharp_bezier" WIDTH="4"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<node TEXT="IPv4" STYLE_REF="second_node" POSITION="right" ID="ID_640040588" CREATED="1419158242053" MODIFIED="1427444904887" VGAP="25" HGAP="40" VSHIFT="100">
<edge COLOR="#003366"/>
<hook URI="../../../../../D:/Documents/MindMaps/Network-Internet/IP-package.RU/ipv-4.jpg" SIZE="0.5602241" NAME="ExternalObject"/>
<node TEXT="&#x412;&#x435;&#x440;&#x441;&#x438;&#x44f; IP" STYLE_REF="orange_node" ID="ID_622875484" CREATED="1419158515622" MODIFIED="1419159338329"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1042;&#1077;&#1088;&#1089;&#1080;&#1103; &#1087;&#1088;&#1086;&#1090;&#1086;&#1082;&#1086;&#1083;&#1072; IP
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&#x414;&#x43b;&#x438;&#x43d;&#x430; &#x437;&#x430;&#x433;&#x43e;&#x43b;&#x43e;&#x432;&#x43a;&#x430;" STYLE_REF="orange_node" ID="ID_1131606432" CREATED="1419158602386" MODIFIED="1419158708424"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Internet Header Length
    </p>
  </body>
</html>
</richcontent>
<node TEXT="&#x41f;&#x43e;&#x441;&#x43a;&#x43e;&#x43b;&#x44c;&#x43a;&#x443; &#x447;&#x438;&#x441;&#x43b;&#x43e; &#x43e;&#x43f;&#x446;&#x438;&#x439; &#x43d;&#x435; &#x43f;&#x43e;&#x441;&#x442;&#x43e;&#x44f;&#x43d;&#x43d;&#x43e;,&#xa;&#x443;&#x43a;&#x430;&#x437;&#x430;&#x43d;&#x438;&#x435; &#x440;&#x430;&#x437;&#x43c;&#x435;&#x440;&#x430; &#x432;&#x430;&#x436;&#x43d;&#x43e; &#x434;&#x43b;&#x44f; &#x43e;&#x442;&#x434;&#x435;&#x43b;&#x435;&#x43d;&#x438;&#x44f;&#xa;&#x437;&#x430;&#x433;&#x43e;&#x43b;&#x43e;&#x432;&#x43a;&#x430; &#x43e;&#x442; &#x434;&#x430;&#x43d;&#x43d;&#x44b;&#x445;" STYLE_REF="pre_last_node" ID="ID_720215296" CREATED="1419158644228" MODIFIED="1419158711610"/>
</node>
<node TEXT="&#x422;&#x438;&#x43f; &#x43e;&#x431;&#x441;&#x43b;&#x443;&#x436;&#x438;&#x432;&#x430;&#x43d;&#x438;&#x44f;" STYLE_REF="orange_node" ID="ID_615901771" CREATED="1419158720042" MODIFIED="1419158783170"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Differentiated Services Code Point (DSCP)
    </p>
  </body>
</html>
</richcontent>
<node TEXT="&#x41f;&#x435;&#x440;&#x432;&#x44b;&#x445; 3 &#x431;&#x438;&#x442;&#x430; - &#x43f;&#x440;&#x438;&#x43e;&#x440;&#x438;&#x442;&#x435;&#x442;" STYLE_REF="pre_last_node" ID="ID_277816913" CREATED="1419158784242" MODIFIED="1419158997190">
<node TEXT="0 - &#x43e;&#x431;&#x44b;&#x447;&#x43d;&#x44b;&#x439; &#x43f;&#x440;&#x438;&#x43e;&#x440;&#x438;&#x442;&#x435;&#x442;" ID="ID_1752187093" CREATED="1419158865803" MODIFIED="1419158877877"/>
<node TEXT="7 - &#x441;&#x430;&#x43c;&#x44b;&#x439; &#x432;&#x44b;&#x441;&#x43e;&#x43a;&#x438;&#x439; / &#x443;&#x43f;&#x440;&#x430;&#x432;&#x43b;&#x435;&#x43d;&#x438;&#x435; &#x441;&#x435;&#x442;&#x44c;&#x44e;" ID="ID_173686286" CREATED="1419158878924" MODIFIED="1419158898589"/>
</node>
<node TEXT="4-&#x439; &#x431;&#x438;&#x442; D" STYLE_REF="pre_last_node" ID="ID_1669246627" CREATED="1419158900860" MODIFIED="1419158997196"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1052;&#1080;&#1085; &#1079;&#1072;&#1076;&#1077;&#1088;&#1078;&#1082;&#1072; &#1087;&#1088;&#1080; &#1087;&#1077;&#1088;&#1077;&#1076;&#1072;&#1095;&#1077;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="5-&#x439; &#x431;&#x438;&#x442; T" STYLE_REF="pre_last_node" ID="ID_841468817" CREATED="1419158919828" MODIFIED="1419158997198"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1052;&#1072;&#1082;&#1089; &#1087;&#1088;&#1086;&#1087;&#1091;&#1089;&#1082;&#1085;&#1072;&#1103; &#1089;&#1087;&#1086;&#1089;&#1086;&#1073;&#1085;&#1086;&#1089;&#1090;&#1100;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="6-&#x439; &#x431;&#x438;&#x442; R" STYLE_REF="pre_last_node" ID="ID_1380614468" CREATED="1419158941978" MODIFIED="1419159010281"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1052;&#1072;&#1082;&#1089; &#1085;&#1072;&#1076;&#1077;&#1078;&#1085;&#1086;&#1089;&#1090;&#1100;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="&#x420;&#x430;&#x437;&#x43c;&#x435;&#x440; &#x43f;&#x430;&#x43a;&#x435;&#x442;&#x430;" STYLE_REF="orange_node" ID="ID_602565930" CREATED="1419159067490" MODIFIED="1419160983626"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Total Length
    </p>
  </body>
</html>
</richcontent>
<node TEXT="16-&#x431;&#x438;&#x442;&#x43d;&#x44b;&#x439; &#x43f;&#x43e;&#x43b;&#x43d;&#x44b;&#x439; &#x440;&#x430;&#x437;&#x43c;&#x435;&#x440; &#x43f;&#x430;&#x43a;&#x435;&#x442;&#x430; &#x432; &#x431;&#x430;&#x439;&#x442;&#x430;&#x445;,&#xa;&#x432;&#x43a;&#x43b;&#x44e;&#x447;&#x430;&#x44f; &#x437;&#x430;&#x433;&#x43e;&#x43b;&#x43e;&#x432;&#x43e;&#x43a; &#x438; &#x434;&#x430;&#x43d;&#x43d;&#x44b;&#x435;" STYLE_REF="pre_last_node" ID="ID_1816999189" CREATED="1419159094390" MODIFIED="1419159146589"/>
<node TEXT="&#x41c;&#x438;&#x43d; 20 &#x431;&#x430;&#x439;&#x442;&#x43e;&#x432; (&#x441;&#x430;&#x43c; &#x437;&#x430;&#x433;&#x43e;&#x43b;&#x43e;&#x432;&#x43e;&#x43a;)" STYLE_REF="pre_last_node" ID="ID_1587240718" CREATED="1419159100248" MODIFIED="1419159146601"/>
<node TEXT="&#x41c;&#x430;&#x43a;&#x441; 65535 &#x431;&#x430;&#x439;&#x442;" STYLE_REF="pre_last_node" ID="ID_1641627850" CREATED="1419159106117" MODIFIED="1419159146604"/>
</node>
<node TEXT="&#x418;&#x434;&#x435;&#x43d;&#x442;&#x438;&#x444;&#x438;&#x43a;&#x430;&#x442;&#x43e;&#x440; &#x444;&#x440;&#x430;&#x433;&#x43c;&#x435;&#x43d;&#x442;&#x430;" STYLE_REF="orange_node" ID="ID_1446273721" CREATED="1419159156867" MODIFIED="1419159235657">
<node TEXT="&#x423;&#x43d;&#x438;&#x43a;&#x430;&#x43b;&#x44c;&#x43d;&#x43e;&#x435; &#x446;&#x435;&#x43b;&#x43e;&#x435; &#x447;&#x438;&#x441;&#x43b;&#x43e; - &#x438;&#x441;&#x43f;&#x43e;&#x43b;&#x44c;&#x437;&#x443;&#x435;&#x442;&#x441;&#x44f; &#x434;&#x43b;&#x44f;&#xa;&#x438;&#x434;&#x435;&#x43d;&#x442;&#x438;&#x444;&#x438;&#x43a;&#x430;&#x446;&#x438;&#x438; &#x444;&#x440;&#x430;&#x433;&#x43c;&#x435;&#x43d;&#x442;&#x43e;&#x432; &#x43f;&#x430;&#x43a;&#x435;&#x442;&#x430;,&#xa;&#x435;&#x441;&#x43b;&#x438; &#x43e;&#x43d; &#x431;&#x44b;&#x43b; &#x444;&#x440;&#x430;&#x433;&#x43c;&#x435;&#x43d;&#x442;&#x438;&#x440;&#x43e;&#x432;&#x430;&#x43d;" STYLE_REF="pre_last_node" ID="ID_211745510" CREATED="1419159214484" MODIFIED="1419159239132"/>
</node>
<node TEXT="R, DF, MF" STYLE_REF="orange_node" ID="ID_1580686884" CREATED="1419159247777" MODIFIED="1419159332316"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1060;&#1083;&#1072;&#1075;&#1080; &#1082;&#1086;&#1085;&#1090;&#1088;&#1086;&#1083;&#1103; &#1085;&#1072;&#1076; &#1092;&#1088;&#1072;&#1075;&#1084;&#1077;&#1085;&#1090;&#1072;&#1094;&#1080;&#1077;
    </p>
  </body>
</html>
</richcontent>
<node TEXT="R" STYLE_REF="pre_last_node" ID="ID_1649101146" CREATED="1419159347924" MODIFIED="1419159388249"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1047;&#1072;&#1088;&#1077;&#1079;&#1077;&#1088;&#1074;&#1080;&#1088;&#1086;&#1074;&#1072;&#1085;, &#1076;&#1086;&#1083;&#1078;&#1077;&#1085; &#1073;&#1099;&#1090;&#1100; &#1088;&#1072;&#1074;&#1077;&#1085; 0
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="DF" STYLE_REF="pre_last_node" ID="ID_448059337" CREATED="1419159357940" MODIFIED="1419159388257"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1053;&#1077; &#1092;&#1088;&#1072;&#1075;&#1084;&#1077;&#1085;&#1090;&#1080;&#1088;&#1086;&#1074;&#1072;&#1090;&#1100;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="MF" STYLE_REF="pre_last_node" ID="ID_152047424" CREATED="1419159371061" MODIFIED="1419159416698"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1059; &#1087;&#1072;&#1082;&#1077;&#1090;&#1072; &#1077;&#1097;&#1105; &#1077;&#1089;&#1090;&#1100; &#1092;&#1088;&#1072;&#1075;&#1084;&#1077;&#1085;&#1090;&#1099;.
    </p>
    <p>
      &#1045;&#1089;&#1083;&#1080; &#1086;&#1085; == 0 &#1090;&#1086; &#1101;&#1090;&#1086; &#1087;&#1086;&#1089;&#1083;&#1077;&#1076;&#1085;&#1080;&#1081; &#1092;&#1088;&#1072;&#1075;&#1084;&#1077;&#1085;&#1090;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="&#x421;&#x43c;&#x435;&#x449;&#x435;&#x43d;&#x438;&#x435; &#x444;&#x440;&#x430;&#x433;&#x43c;&#x435;&#x43d;&#x442;&#x430;" STYLE_REF="orange_node" ID="ID_648740748" CREATED="1419159430857" MODIFIED="1419159538767">
<node TEXT="&#x421;&#x43c;&#x435;&#x449;&#x435;&#x43d;&#x438;&#x435; &#x43a;&#x43e;&#x442;&#x43e;&#x440;&#x43e;&#x435; &#x441;&#x434;&#x435;&#x43b;&#x430;&#x43d;&#x43d;&#x43e; &#x432; &#x44d;&#x442;&#x43e;&#x43c; &#x444;&#x440;&#x430;&#x433;&#x43c;&#x435;&#x43d;&#x442;&#x435;,&#xa;&#x43e;&#x442;&#x43d;&#x43e;&#x441;&#x438;&#x442;&#x435;&#x43b;&#x44c;&#x43d;&#x43e; &#x43f;&#x43e;&#x43b;&#x43d;&#x43e;&#x439; &#x434;&#x430;&#x439;&#x442;&#x430;&#x433;&#x440;&#x430;&#x43c;&#x43c;&#x44b;" STYLE_REF="pre_last_node" ID="ID_1063732397" CREATED="1419159505284" MODIFIED="1419159558325"/>
<node TEXT="&#x41f;&#x435;&#x440;&#x432;&#x44b;&#x439; &#x444;&#x440;&#x430;&#x433;&#x43c;&#x435;&#x43d;&#x442; &#x432; &#x43f;&#x43e;&#x441;&#x43b;&#x435;&#x434;&#x43e;&#x432;&#x430;&#x442;&#x435;&#x43b;&#x44c;&#x43d;&#x43e;&#x441;&#x442;&#x438;&#xa;&#x438;&#x43c;&#x435;&#x435;&#x442; &#x43d;&#x443;&#x43b;&#x435;&#x432;&#x43e;&#x435; &#x441;&#x43c;&#x435;&#x449;&#x435;&#x43d;&#x438;&#x435;" STYLE_REF="pre_last_node" ID="ID_1063799078" CREATED="1419159491872" MODIFIED="1419159558342"/>
</node>
<node TEXT="&#x412;&#x440;&#x435;&#x43c;&#x44f; &#x436;&#x438;&#x437;&#x43d;&#x438; / TTL" STYLE_REF="orange_node" ID="ID_110667282" CREATED="1419159583542" MODIFIED="1419159604613"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Time to live
    </p>
  </body>
</html>
</richcontent>
<node TEXT="&#x41c;&#x430;&#x43a;&#x441; &#x43a;&#x43e;&#x43b;&#x438;&#x447;&#x435;&#x441;&#x442;&#x432;&#x43e; &#x445;&#x43e;&#x43f;&#x43e;&#x432;&#xa;&#x43a;&#x43e;&#x442;&#x43e;&#x440;&#x43e;&#x435; &#x43c;&#x43e;&#x436;&#x435;&#x442; &#x43f;&#x440;&#x43e;&#x439;&#x442;&#x438; &#x43f;&#x430;&#x43a;&#x435;&#x442;" STYLE_REF="pre_last_node" ID="ID_1427978603" CREATED="1419162663009" MODIFIED="1419162726601"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Hop - &#1091;&#1095;&#1072;&#1089;&#1090;&#1086;&#1082; &#1084;&#1077;&#1078;&#1076;&#1091; &#1084;&#1072;&#1088;&#1096;&#1088;&#1091;&#1090;&#1080;&#1079;&#1072;&#1090;&#1086;&#1088;&#1072;&#1084;&#1080;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&#x41c;&#x430;&#x440;&#x448;&#x440;&#x443;&#x442;&#x438;&#x437;&#x430;&#x442;&#x43e;&#x440; &#x434;&#x435;&#x43b;&#x430;&#x435;&#x442; TTL--; &#x43f;&#x440;&#x438; &#x43f;&#x435;&#x440;&#x435;&#x445;&#x43e;&#x434;&#x435;" STYLE_REF="pre_last_node" ID="ID_633872100" CREATED="1419162753207" MODIFIED="1419162792624"/>
<node TEXT="&#x41d;&#x435; &#x43f;&#x43e;&#x437;&#x432;&#x43e;&#x43b;&#x44f;&#x435;&#x442; &#x43f;&#x430;&#x43a;&#x435;&#x442;&#x430;&#x43c; &#x431;&#x43b;&#x443;&#x436;&#x434;&#x430;&#x442;&#x44c; &#x43f;&#x43e; &#x441;&#x435;&#x442;&#x438; &#x432;&#x435;&#x447;&#x43d;&#x43e;" STYLE_REF="pre_last_node" ID="ID_1961805888" CREATED="1419162768442" MODIFIED="1419162792616"/>
<node TEXT="IF TTL == 0" STYLE_REF="pre_last_node" ID="ID_1862071950" CREATED="1419162796531" MODIFIED="1419162874855">
<node TEXT="1. &#x41f;&#x430;&#x43a;&#x435;&#x442; &#x431;&#x443;&#x434;&#x435;&#x442; &#x443;&#x43d;&#x438;&#x447;&#x442;&#x43e;&#x436;&#x435;&#x43d;" STYLE_REF="yellow_node" ID="ID_1835417505" CREATED="1419162831679" MODIFIED="1419258941422"/>
<node TEXT="2. &#x41e;&#x442;&#x43f;&#x440;&#x430;&#x432;&#x438;&#x442;&#x435;&#x43b;&#x44e; &#x43f;&#x43e;&#x441;&#x438;&#x43b;&#x430;&#x435;&#x442;&#x441;&#x44f; ICMP Time Exceeded" STYLE_REF="yellow_node" ID="ID_685346145" CREATED="1419162841789" MODIFIED="1419258941426"/>
</node>
<node TEXT="&#x417;&#x43d;&#x430;&#x447;&#x435;&#x43d;&#x438;&#x44f;" STYLE_REF="pre_last_node" ID="ID_1710277418" CREATED="1419162892711" MODIFIED="1419168301963">
<node TEXT="&#x41c;&#x430;&#x43a;&#x441; TTL=255" STYLE_REF="yellow_node" ID="ID_1266672007" CREATED="1419162897773" MODIFIED="1419258941428"/>
<node TEXT="&#x41e;&#x431;&#x44b;&#x447;&#x43d;&#x43e; TTL=64" STYLE_REF="yellow_node" ID="ID_1709125494" CREATED="1419162908613" MODIFIED="1419258941431"/>
<node TEXT="TTL=1 &#x435;&#x441;&#x43b;&#x438; &#x43d;&#x443;&#x436;&#x43d;&#x43e; &#x447;&#x442;&#x43e;&#x431;&#x44b;&#xa;&#x43f;&#x430;&#x43a;&#x435;&#x442; &#x43d;&#x435; &#x43f;&#x43e;&#x43a;&#x438;&#x43d;&#x443;&#x43b; &#x441;&#x435;&#x442;&#x44c;" STYLE_REF="yellow_node" ID="ID_557950589" CREATED="1419162934839" MODIFIED="1419258941434"/>
</node>
</node>
<node TEXT="&#x41f;&#x440;&#x43e;&#x442;&#x43e;&#x43a;&#x43e;&#x43b;" STYLE_REF="orange_node" ID="ID_1764136497" CREATED="1419159623320" MODIFIED="1419159691512"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1044;&#1072;&#1085;&#1085;&#1099;&#1077; &#1082;&#1072;&#1082;&#1086;&#1075;&#1086; &#1087;&#1088;&#1086;&#1090;&#1086;&#1082;&#1086;&#1083;&#1072; &#1089;&#1086;&#1076;&#1077;&#1088;&#1078;&#1080;&#1090; &#1087;&#1072;&#1082;&#1077;&#1090;
    </p>
    <p>
      &#1053;&#1072;&#1087;&#1088;&#1080;&#1084;&#1077;&#1088;: TCP, UDP, ICMP
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&#x41a;&#x43e;&#x43d;&#x442;&#x440;&#x43e;&#x43b;&#x44c;&#x43d;&#x430;&#x44f; &#x441;&#x443;&#x43c;&#x43c;&#x430; &#x437;&#x430;&#x433;&#x43e;&#x43b;&#x43e;&#x432;&#x43a;&#x430;" STYLE_REF="orange_node" ID="ID_1900974604" CREATED="1419159694342" MODIFIED="1419159839073">
<node TEXT="&#x41a;&#x430;&#x436;&#x434;&#x44b;&#x439; &#x445;&#x43e;&#x441;&#x442; &#x441;&#x440;&#x430;&#x432;&#x43d;&#x438;&#x432;&#x430;&#x435;&#x442; &#x43a;&#x43e;&#x43d;&#x442;&#x440; &#x441;&#x443;&#x43c;&#x43c;&#x443; &#x437;&#x430;&#x433;&#x43e;&#x43b;&#x43e;&#x432;&#x43a;&#x430;&#xa;&#x441;&#x43e; &#x437;&#x43d;&#x430;&#x447;&#x435;&#x43d;&#x438;&#x435;&#x43c; &#x44d;&#x442;&#x43e;&#x433;&#x43e; &#x43f;&#x43e;&#x43b;&#x44f; &#x438; &#x43e;&#x442;&#x431;&#x440;&#x430;&#x441;&#x44b;&#x432;&#x430;&#x435;&#x442; &#x43f;&#x430;&#x43a;&#x435;&#x442;,&#xa;&#x435;&#x441;&#x43b;&#x438; &#x43e;&#x43d;&#x438; &#x43d;&#x435; &#x441;&#x43e;&#x432;&#x43f;&#x430;&#x434;&#x430;&#x44e;&#x442;" STYLE_REF="pre_last_node" ID="ID_1110773539" CREATED="1419159865213" MODIFIED="1419159893648"/>
<node TEXT="&#x41f;&#x43e;&#x441;&#x43a;&#x43e;&#x43b;&#x44c;&#x43a;&#x443; TTL &#x443;&#x43c;&#x435;&#x43d;&#x44c;&#x448;&#x430;&#x435;&#x442;&#x441;&#x44f; &#x43d;&#x430; &#x43a;&#x430;&#x436;&#x434;&#x43e;&#x43c;&#xa;&#x448;&#x430;&#x433;&#x435;, &#x441;&#x443;&#x43c;&#x43c;&#x430; &#x442;&#x43e;&#x436;&#x435; &#x434;&#x43e;&#x43b;&#x436;&#x43d;&#x430; &#x432;&#x44b;&#x447;&#x438;&#x441;&#x43b;&#x44f;&#x442;&#x44c;&#x441;&#x44f;&#xa;&#x43d;&#x430; &#x43a;&#x430;&#x436;&#x434;&#x43e;&#x43c; &#x448;&#x430;&#x433;&#x435;" STYLE_REF="pre_last_node" ID="ID_574725405" CREATED="1419159745749" MODIFIED="1419159825892"/>
<node TEXT="&#x426;&#x435;&#x43b;&#x43e;&#x441;&#x442;&#x43d;&#x43e;&#x441;&#x442;&#x44c; &#x434;&#x430;&#x43d;&#x43d;&#x44b;&#x445; IP &#x437;&#x434;&#x435;&#x441;&#x44c; &#x43d;&#x435; &#x43f;&#x440;&#x43e;&#x432;&#x435;&#x440;&#x44f;&#x435;&#x442;&#x441;&#x44f;" STYLE_REF="pre_last_node" ID="ID_1956298372" CREATED="1419159785778" MODIFIED="1419159834020"/>
</node>
<node TEXT="&#x41f;&#x430;&#x440;&#x430;&#x43c;&#x435;&#x442;&#x440;&#x44b;" STYLE_REF="orange_node" ID="ID_978134432" CREATED="1419159899077" MODIFIED="1419160264396"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1053;&#1077; &#1086;&#1073;&#1103;&#1079;&#1072;&#1090;&#1077;&#1083;&#1100;&#1085;&#1086; &#1087;&#1086;&#1083;&#1077;
    </p>
    <p>
      &#1076;&#1086;&#1087;&#1086;&#1083;&#1085;&#1080;&#1090;&#1077;&#1083;&#1100;&#1085;&#1099;&#1093; &#1086;&#1087;&#1094;&#1080;&#1081;
    </p>
  </body>
</html>
</richcontent>
<node TEXT="&#x41a;&#x43e;&#x43f;&#x438;&#x440;&#x43e;&#x432;&#x430;&#x442;&#x44c;" STYLE_REF="pre_last_node" ID="ID_1082268836" CREATED="1419160173691" MODIFIED="1419160310534"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      1 &#1073;&#1080;&#1090;
    </p>
  </body>
</html>
</richcontent>
<node TEXT="&#x423;&#x441;&#x442;&#x430;&#x43d;&#x430;&#x432;&#x43b;&#x438;&#x432;&#x430;&#x435;&#x442;&#x441;&#x44f; &#x432; 1 &#x435;&#x441;&#x43b;&#x438; &#x442;&#x440;&#x435;&#x431;&#x443;&#x435;&#x442;&#x441;&#x44f; &#x43a;&#x43e;&#x43f;&#x438;&#x440;&#x43e;&#x432;&#x430;&#x442;&#x44c;&#xa;&#x43e;&#x43f;&#x446;&#x438;&#x438; &#x432; &#x437;&#x430;&#x433;&#x43e;&#x43b;&#x43e;&#x432;&#x43a;&#x438; &#x432;&#x441;&#x435;&#x445; &#x444;&#x440;&#x430;&#x433;&#x43c;&#x435;&#x43d;&#x442;&#x43e;&#x432;" STYLE_REF="yellow_node" ID="ID_1687042002" CREATED="1419160180787" MODIFIED="1419258947382"/>
</node>
<node TEXT="&#x41a;&#x43b;&#x430;&#x441;&#x441; &#x43e;&#x43f;&#x446;&#x438;&#x438;" STYLE_REF="pre_last_node" ID="ID_1195455534" CREATED="1419160199355" MODIFIED="1419160310543"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      2 &#1073;&#1080;&#1090;&#1072;
    </p>
  </body>
</html>
</richcontent>
<node TEXT="0 &#x434;&#x43b;&#x44f; &#xab;&#x443;&#x43f;&#x440;&#x430;&#x432;&#x43b;&#x44f;&#x44e;&#x449;&#x438;&#x445;&#xbb; &#x43e;&#x43f;&#x446;&#x438;&#x439;" STYLE_REF="yellow_node" ID="ID_895935810" CREATED="1419160375068" MODIFIED="1419258947391"/>
<node TEXT="1 &#x437;&#x430;&#x440;&#x435;&#x437;&#x435;&#x440;&#x432;&#x438;&#x440;&#x43e;&#x432;&#x430;&#x43d;&#x43d;&#x43e;" STYLE_REF="yellow_node" ID="ID_123083399" CREATED="1419160391969" MODIFIED="1419258947395"/>
<node TEXT="2 &#x434;&#x43b;&#x44f; &#x43e;&#x43f;&#x446;&#x438;&#x439; &#xab;&#x438;&#x437;&#x43c;&#x435;&#x440;&#x435;&#x43d;&#x438;&#x439; &#x438; &#x43e;&#x442;&#x43b;&#x430;&#x434;&#x43a;&#x438;&#xbb;" STYLE_REF="yellow_node" ID="ID_744437673" CREATED="1419160412081" MODIFIED="1419258947398"/>
<node TEXT="3 &#x437;&#x430;&#x440;&#x435;&#x437;&#x435;&#x440;&#x432;&#x438;&#x440;&#x43e;&#x432;&#x430;&#x43d;&#x43d;&#x43e;" STYLE_REF="yellow_node" ID="ID_957870938" CREATED="1419160416128" MODIFIED="1419258947401"/>
</node>
<node TEXT="&#x41d;&#x43e;&#x43c;&#x435;&#x440; &#x43e;&#x43f;&#x446;&#x438;&#x438;" STYLE_REF="pre_last_node" ID="ID_1101259275" CREATED="1419160339416" MODIFIED="1419160352054"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      5 &#1073;&#1080;&#1090;
    </p>
  </body>
</html>
</richcontent>
<node TEXT="7 - &#x437;&#x430;&#x43f;&#x438;&#x441;&#x44c; &#x43c;&#x430;&#x440;&#x448;&#x440;&#x443;&#x442;&#x430;" STYLE_REF="yellow_node" ID="ID_1798550953" CREATED="1419160358094" MODIFIED="1419258947403"/>
<node TEXT="4 - &#x432;&#x440;&#x435;&#x43c;&#x435;&#x43d;&#x43d;&#x44b;&#x435; &#x43c;&#x435;&#x442;&#x43a;&#x438;" STYLE_REF="yellow_node" ID="ID_68621340" CREATED="1419160366765" MODIFIED="1419258947404"/>
</node>
<node TEXT="&#x420;&#x430;&#x437;&#x43c;&#x435;&#x440; &#x43e;&#x43f;&#x446;&#x438;&#x438;" STYLE_REF="pre_last_node" ID="ID_655802464" CREATED="1419160226309" MODIFIED="1419160310546"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      8 &#1073;&#1080;&#1090;
    </p>
  </body>
</html>
</richcontent>
<node TEXT="&#x41c;&#x43e;&#x436;&#x435;&#x442; &#x43d;&#x435; &#x443;&#x43a;&#x430;&#x437;&#x44b;&#x432;&#x430;&#x442;&#x44c;&#x441;&#x44f; &#x434;&#x43b;&#x44f; &#x43e;&#x43f;&#x446;&#x438;&#x439; &#x431;&#x435;&#x437; &#x430;&#x440;&#x433;&#x443;&#x43c;&#x435;&#x43d;&#x442;&#x43e;&#x432;" STYLE_REF="yellow_node" ID="ID_471790140" CREATED="1419160244177" MODIFIED="1419258947405"/>
</node>
<node TEXT="&#x410;&#x440;&#x433;&#x443;&#x43c;&#x435;&#x43d;&#x442;&#x44b; &#x43e;&#x43f;&#x446;&#x438;&#x438;" STYLE_REF="pre_last_node" ID="ID_481627697" CREATED="1419160268340" MODIFIED="1419160310549"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1055;&#1077;&#1088;&#1077;&#1084;&#1077;&#1085;&#1085;&#1086;&#1077; &#1082;&#1086;&#1083;&#1080;&#1095;&#1077;&#1089;&#1090;&#1074;&#1086; &#1073;&#1080;&#1090;
    </p>
  </body>
</html>
</richcontent>
<node TEXT="&#x414;&#x43e;&#x43f;&#x43e;&#x43b;&#x43d;&#x438;&#x442;&#x435;&#x43b;&#x44c;&#x43d;&#x44b;&#x435; &#x434;&#x430;&#x43d;&#x43d;&#x44b;&#x435;,&#xa;&#x438;&#x441;&#x43f;&#x43e;&#x43b;&#x44c;&#x437;&#x443;&#x435;&#x43c;&#x44b;&#x435; &#x43e;&#x43f;&#x446;&#x438;&#x435;&#x439;" STYLE_REF="yellow_node" ID="ID_1228867258" CREATED="1419160298061" MODIFIED="1419258947406"/>
</node>
</node>
</node>
<node TEXT="IPv6" STYLE_REF="second_node" POSITION="left" ID="ID_836990116" CREATED="1419158247037" MODIFIED="1427444867144" VGAP="23" HGAP="40" VSHIFT="-20">
<edge COLOR="#003333"/>
<hook URI="../../../../../D:/Documents/MindMaps/Network-Internet/IP-package.RU/ipv-6.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x412;&#x435;&#x440;&#x441;&#x438;&#x44f;" STYLE_REF="orange_node" ID="ID_1038700098" CREATED="1419160666767" MODIFIED="1419161045286"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1076;&#1083;&#1103; IPv6 = 6 (0110)
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&#x41f;&#x440;&#x438;&#x43e;&#x440;&#x438;&#x442;&#x435;&#x442;" STYLE_REF="orange_node" ID="ID_1666954816" CREATED="1419160720371" MODIFIED="1419160796213"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Traffic Class
    </p>
  </body>
</html>
</richcontent>
<node TEXT="&#x421;&#x442;&#x430;&#x440;&#x448;&#x438;&#x435; 6 &#x431;&#x438;&#x442;" STYLE_REF="pre_last_node" ID="ID_1206314626" CREATED="1419160749661" MODIFIED="1419160802071"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1048;&#1089;&#1087;&#1086;&#1083;&#1100;&#1079;&#1091;&#1102;&#1090;&#1089;&#1103; DSCP &#1076;&#1083;&#1103; &#1082;&#1083;&#1072;&#1089;&#1089;&#1080;&#1092;&#1080;&#1082;&#1072;&#1094;&#1080;&#1080; &#1087;&#1072;&#1082;&#1077;&#1090;&#1086;&#1074;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&#x41e;&#x441;&#x442;&#x430;&#x432;&#x448;&#x438;&#x435;&#x441;&#x44f; &#x434;&#x432;&#x430; &#x431;&#x438;&#x442;&#x430;" STYLE_REF="pre_last_node" ID="ID_573287288" CREATED="1419160768901" MODIFIED="1419160802079"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1048;&#1089;&#1087;&#1086;&#1083;&#1100;&#1079;&#1091;&#1102;&#1090;&#1089;&#1103; ECN &#1076;&#1083;&#1103; &#1082;&#1086;&#1085;&#1090;&#1088;&#1086;&#1083;&#1103; &#1087;&#1077;&#1088;&#1077;&#1075;&#1088;&#1091;&#1079;&#1082;&#1080;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="&#x41c;&#x435;&#x442;&#x43a;&#x430; &#x43f;&#x43e;&#x442;&#x43e;&#x43a;&#x430;" STYLE_REF="orange_node" ID="ID_953090263" CREATED="1419160848841" MODIFIED="1419160868206"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Flow Label
    </p>
  </body>
</html>
</richcontent>
<node TEXT="&#x41f;&#x43e;&#x442;&#x43e;&#x43a; - &#x43f;&#x43e;&#x441;&#x43b;&#x435;&#x434;&#x43e;&#x432;&#x430;&#x442;&#x435;&#x43b;&#x44c;&#x43d;&#x43e;&#x441;&#x442;&#x44c; &#x43f;&#x430;&#x43a;&#x435;&#x442;&#x43e;&#x432;" STYLE_REF="pre_last_node" ID="ID_395832372" CREATED="1419168501244" MODIFIED="1419168580280"/>
<node TEXT="&#x414;&#x43e;&#x43f;&#x443;&#x441;&#x43a;&#x430;&#x435;&#x442;&#x441;&#x44f; &#x441;&#x443;&#x449;&#x435;&#x441;&#x442;&#x432;&#x43e;&#x432;&#x430;&#x43d;&#x438;&#x435; &#x43d;&#x435;&#x441;&#x43a;&#x43e;&#x43b;&#x44c;&#x43a;&#x438;&#x445;&#xa;&#x43f;&#x43e;&#x442;&#x43e;&#x43a;&#x43e;&#x432; &#x43c;&#x435;&#x436;&#x434;&#x443; &#x43e;&#x442;&#x43f;&#x440;&#x430;&#x432;&#x438;&#x442;&#x435;&#x43b;&#x435;&#x43c; &#x438; &#x43f;&#x43e;&#x43b;&#x443;&#x447;&#x430;&#x442;&#x435;&#x43b;&#x435;&#x43c;" STYLE_REF="pre_last_node" ID="ID_348793010" CREATED="1419168571064" MODIFIED="1419168580258"/>
<node TEXT="&#x41f;&#x440;&#x438; &#x43f;&#x43e;&#x43b;&#x443;&#x447;&#x435;&#x43d;&#x438;&#x438; &#x43f;&#x435;&#x440;&#x432;&#x43e;&#x433;&#x43e; &#x43f;&#x430;&#x43a;&#x435;&#x442;&#x430; &#x441; &#x43c;&#x435;&#x442;&#x43a;&#x43e;&#x439; &#x43f;&#x43e;&#x442;&#x43e;&#x43a;&#x430;&#xa;&#x43c;&#x430;&#x440;&#x448;&#x440;&#x443;&#x442;&#x438;&#x437;&#x430;&#x442;&#x43e;&#x440; &#x430;&#x43d;&#x430;&#x43b;&#x438;&#x437;&#x438;&#x440;&#x443;&#x435;&#x442; &#x434;&#x43e;&#x43f;&#x43e;&#x43b;&#x43d;&#x438;&#x442;&#x435;&#x43b;&#x44c;&#x43d;&#x44b;&#x435; &#x437;&#x430;&#x433;&#x43e;&#x43b;&#x43e;&#x432;&#x43a;&#x438;,&#xa;&#x432;&#x44b;&#x43f;&#x43e;&#x43b;&#x43d;&#x44f;&#x435;&#x442; &#x43f;&#x440;&#x435;&#x434;&#x43f;&#x438;&#x441;&#x430;&#x43d;&#x43d;&#x44b;&#x435; &#x442;&#x430;&#x43c; &#x444;&#x443;&#x43d;&#x43a;&#x446;&#x438;&#x438; &#x438; &#x437;&#x430;&#x43f;&#x43e;&#x43c;&#x438;&#x43d;&#x430;&#x435;&#x442;&#xa;&#x440;&#x435;&#x437;&#x443;&#x43b;&#x44c;&#x442;&#x430;&#x442;&#x44b; &#x43e;&#x431;&#x440;&#x430;&#x431;&#x43e;&#x442;&#x43a;&#x438; &#x432; &#x43b;&#x43e;&#x43a;&#x430;&#x43b;&#x44c;&#x43d;&#x43e;&#x43c; &#x43a;&#x44d;&#x448;&#x435;" STYLE_REF="pre_last_node" ID="ID_1558887901" CREATED="1419168677668" MODIFIED="1419168719832"/>
<node TEXT="&#x41a;&#x43b;&#x44e;&#x447;&#x43e;&#x43c; &#x434;&#x43b;&#x44f; &#x442;&#x430;&#x43a;&#x43e;&#x439; &#x437;&#x430;&#x43f;&#x438;&#x441;&#x438; &#x44f;&#x432;&#x43b;&#x44f;&#x435;&#x442;&#x441;&#x44f;&#xa;&#x43a;&#x43e;&#x43c;&#x431;&#x438;&#x43d;&#x430;&#x446;&#x438;&#x44f; &#x430;&#x434;&#x440;&#x435;&#x441;&#x430; &#x438;&#x441;&#x442;&#x43e;&#x447;&#x43d;&#x438;&#x43a;&#x430; &#x438; &#x43c;&#x435;&#x442;&#x43a;&#x438; &#x43f;&#x43e;&#x442;&#x43e;&#x43a;&#x430;" STYLE_REF="pre_last_node" ID="ID_496234602" CREATED="1419168756879" MODIFIED="1419168832167"/>
<node TEXT="&#x41f;&#x43e;&#x441;&#x43b;&#x435;&#x434;&#x443;&#x44e;&#x449;&#x438;&#x435; &#x43f;&#x430;&#x43a;&#x435;&#x442;&#x44b; &#x441; &#x442;&#x43e;&#x439; &#x436;&#x435; &#x43a;&#x43e;&#x43c;&#x431;&#x438;&#x43d;&#x430;&#x446;&#x438;&#x435;&#x439;&#xa;&#x430;&#x434;&#x440;&#x435;&#x441;&#x430; &#x438;&#x441;&#x442;&#x43e;&#x447;&#x43d;&#x438;&#x43a;&#x430; &#x438; &#x43c;&#x435;&#x442;&#x43a;&#x438; &#x43f;&#x43e;&#x442;&#x43e;&#x43a;&#x430; &#x43e;&#x431;&#x440;&#x430;&#x431;&#x430;&#x442;&#x44b;&#x432;&#x430;&#x44e;&#x442;&#x441;&#x44f;&#xa;&#x441; &#x443;&#x447;&#x451;&#x442;&#x43e;&#x43c; &#x43a;&#x44d;&#x448;&#x430; &#x431;&#x435;&#x437; &#x434;&#x435;&#x442;&#x430;&#x43b;&#x44c;&#x43d;&#x43e;&#x433;&#x43e; &#x430;&#x43d;&#x430;&#x43b;&#x438;&#x437;&#x430; &#x432;&#x441;&#x435;&#x445; &#x43f;&#x43e;&#x43b;&#x435;&#x439; &#x437;&#x430;&#x433;&#x43e;&#x43b;&#x43e;&#x432;&#x43a;&#x430;" STYLE_REF="pre_last_node" ID="ID_813831251" CREATED="1419168779409" MODIFIED="1419168884615"/>
</node>
<node TEXT="&#x420;&#x430;&#x437;&#x43c;&#x435;&#x440; &#x43f;&#x43e;&#x43b;&#x44f; &#x434;&#x430;&#x43d;&#x43d;&#x44b;&#x445;" STYLE_REF="orange_node" ID="ID_1286244024" CREATED="1419160899076" MODIFIED="1419160922091"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Payload Lengt
    </p>
  </body>
</html>
</richcontent>
<node TEXT="&#x412; &#x43e;&#x442;&#x43b;&#x438;&#x447;&#x438;&#x435; &#x43e;&#x442; IPv4, &#x44d;&#x442;&#x43e; &#x43f;&#x43e;&#x43b;&#x435; &#x43d;&#x435;&#xa;&#x432;&#x43a;&#x43b;&#x44e;&#x447;&#x430;&#x435;&#x442; &#x437;&#x430;&#x433;&#x43e;&#x43b;&#x43e;&#x432;&#x43e;&#x43a; &#x43f;&#x430;&#x43a;&#x435;&#x442;&#x430; (16 &#x431;&#x438;&#x442;)" STYLE_REF="pre_last_node" ID="ID_761591095" CREATED="1419160936812" MODIFIED="1419162172411"/>
</node>
<node TEXT="&#x421;&#x43b;&#x435;&#x434;&#x443;&#x44e;&#x449;&#x438;&#x439; &#x437;&#x430;&#x433;&#x43e;&#x43b;&#x43e;&#x432;&#x43e;&#x43a; / Next Header" STYLE_REF="orange_node" ID="ID_1351684573" CREATED="1419161030962" MODIFIED="1427444937037"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1047;&#1072;&#1076;&#1072;&#1105;&#1090; &#1090;&#1080;&#1087; &#1088;&#1072;&#1089;&#1096;&#1080;&#1088;&#1077;&#1085;&#1085;&#1086;&#1075;&#1086; &#1079;&#1072;&#1075;&#1086;&#1083;&#1086;&#1074;&#1082;&#1072;, &#1082;&#1086;&#1090;&#1086;&#1088;&#1099;&#1081; &#1080;&#1076;&#1105;&#1090; &#1089;&#1083;&#1077;&#1076;&#1091;&#1102;&#1097;&#1080;&#1084;
    </p>
  </body>
</html>
</richcontent>
<hook URI="ipv6nextheader.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x412;&#x438;&#x434;&#x44b;" STYLE_REF="yellow_node" ID="ID_197283213" CREATED="1419162220993" MODIFIED="1419162342445">
<node TEXT="Hop-by-Hop Options" STYLE_REF="pre_last_node" ID="ID_823649444" CREATED="1419162227217" MODIFIED="1419162381645"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1055;&#1072;&#1088;&#1072;&#1084;&#1077;&#1090;&#1088;&#1099;, &#1082;&#1086;&#1090;&#1086;&#1088;&#1099;&#1077; &#1076;&#1086;&#1083;&#1078;&#1085;&#1099; &#1073;&#1099;&#1090;&#1100;
    </p>
    <p>
      &#1086;&#1073;&#1088;&#1072;&#1073;&#1086;&#1090;&#1072;&#1085;&#1099; &#1082;&#1072;&#1078;&#1076;&#1099;&#1084; &#1090;&#1088;&#1072;&#1085;&#1079;&#1080;&#1090;&#1085;&#1099;&#1084; &#1091;&#1079;&#1083;&#1086;&#1084;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Destination Options" STYLE_REF="pre_last_node" ID="ID_1854381374" CREATED="1419162255946" MODIFIED="1419162381653"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1055;&#1072;&#1088;&#1072;&#1084;&#1077;&#1090;&#1088;&#1099; &#1082;&#1086;&#1090;&#1086;&#1088;&#1099;&#1077; &#1076;&#1086;&#1083;&#1078;&#1085;&#1099;
    </p>
    <p>
      &#1073;&#1099;&#1090;&#1100; &#1086;&#1073;&#1088;&#1072;&#1073;&#1086;&#1090;&#1072;&#1085;&#1099; &#1090;&#1086;&#1083;&#1100;&#1082;&#1086; &#1087;&#1086;&#1083;&#1091;&#1095;&#1072;&#1090;&#1077;&#1083;&#1077;&#1084;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Routing" STYLE_REF="pre_last_node" ID="ID_1329045251" CREATED="1419162276485" MODIFIED="1419162381657"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1055;&#1086;&#1079;&#1074;&#1086;&#1083;&#1103;&#1077;&#1090; &#1086;&#1090;&#1087;&#1088;&#1072;&#1074;&#1080;&#1090;&#1077;&#1083;&#1102; &#1086;&#1087;&#1088;&#1077;&#1076;&#1077;&#1083;&#1103;&#1090;&#1100;
    </p>
    <p>
      &#1089;&#1087;&#1080;&#1089;&#1086;&#1082; &#1091;&#1079;&#1083;&#1086;&#1074;, &#1082;&#1086;&#1090;&#1086;&#1088;&#1099;&#1077; &#1087;&#1072;&#1082;&#1077;&#1090; &#1076;&#1086;&#1083;&#1078;&#1077;&#1085; &#1087;&#1088;&#1086;&#1081;&#1090;&#1080;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Fragment" STYLE_REF="pre_last_node" ID="ID_1433599642" CREATED="1419162403383" MODIFIED="1419169682741"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1047;&#1072;&#1075;&#1086;&#1083;&#1086;&#1074;&#1086;&#1082; &#1089;&#1086;&#1076;&#1077;&#1088;&#1078;&#1080;&#1090; &#1080;&#1085;&#1092;&#1086;&#1088;&#1084;&#1072;&#1094;&#1080;&#1102;
    </p>
    <p>
      &#1087;&#1086; &#1092;&#1088;&#1072;&#1075;&#1084;&#1077;&#1085;&#1090;&#1072;&#1094;&#1080;&#1080; &#1087;&#1072;&#1082;&#1077;&#1090;&#1072;
    </p>
  </body>
</html>
</richcontent>
<hook URI="ipv6-fragment.jpg" SIZE="0.6920415" NAME="ExternalObject"/>
<node TEXT="Next Header (8 &#x431;&#x438;&#x442;)" STYLE_REF="yellow_node" ID="ID_91966006" CREATED="1419169791719" MODIFIED="1419258931398"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1058;&#1080;&#1087; &#1089;&#1083;&#1077;&#1076;&#1091;&#1102;&#1097;&#1077;&#1075;&#1086; &#1088;&#1072;&#1089;&#1096;&#1080;&#1088;&#1077;&#1085;&#1085;&#1086;&#1075;&#1086; &#1079;&#1072;&#1075;&#1086;&#1083;&#1086;&#1074;&#1082;&#1072;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Reserved (8 &#x431;&#x438;&#x442;)" STYLE_REF="yellow_node" ID="ID_1193639476" CREATED="1419169769769" MODIFIED="1419258931404"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1047;&#1072;&#1088;&#1077;&#1079;&#1077;&#1088;&#1074;&#1080;&#1088;&#1086;&#1074;&#1072;&#1085;&#1086;, &#1076;&#1086;&#1083;&#1078;&#1085;&#1086; &#1073;&#1099;&#1090;&#1100; 0
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Fragment Offset (13 &#x431;&#x438;&#x442;)" STYLE_REF="yellow_node" ID="ID_1901097843" CREATED="1419169603419" MODIFIED="1419258931407"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1057;&#1084;&#1077;&#1097;&#1077;&#1085;&#1080;&#1077; &#1092;&#1088;&#1072;&#1075;&#1084;&#1077;&#1085;&#1090;&#1072; &#1074; &#1074;&#1086;&#1089;&#1100;&#1084;&#1080;-&#1086;&#1082;&#1090;&#1077;&#1090;&#1085;&#1099;&#1093; &#1073;&#1083;&#1086;&#1082;&#1072;&#1093;
    </p>
    <p>
      &#1086;&#1090;&#1085;&#1086;&#1089;&#1080;&#1090;&#1077;&#1083;&#1100;&#1085;&#1086; &#1085;&#1072;&#1095;&#1072;&#1083;&#1072; &#1092;&#1088;&#1072;&#1075;&#1084;&#1077;&#1085;&#1090;&#1080;&#1088;&#1091;&#1077;&#1084;&#1086;&#1081; &#1095;&#1072;&#1089;&#1090;&#1080; &#1087;&#1072;&#1082;&#1077;&#1090;&#1072;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Res (2 &#x431;&#x438;&#x442;&#x430;)" STYLE_REF="yellow_node" ID="ID_962643933" CREATED="1419169703117" MODIFIED="1419258931408"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1047;&#1072;&#1088;&#1077;&#1079;&#1077;&#1088;&#1074;&#1080;&#1088;&#1086;&#1074;&#1072;&#1085;&#1086;, &#1076;&#1086;&#1083;&#1078;&#1085;&#1086; &#1073;&#1099;&#1090;&#1100; 0
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="M (1 &#x431;&#x438;&#x442;)" STYLE_REF="yellow_node" ID="ID_1579157426" CREATED="1419169731397" MODIFIED="1419258931410"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1041;&#1091;&#1076;&#1091;&#1090; &#1083;&#1080; &#1077;&#1097;&#1105; &#1092;&#1088;&#1072;&#1075;&#1084;&#1077;&#1085;&#1090;&#1099;.
    </p>
    <p>
      &#1045;&#1089;&#1083;&#1080; 0, &#1090;&#1086; &#1101;&#1090;&#1086; &#1087;&#1086;&#1089;&#1083;&#1077;&#1076;&#1085;&#1080;&#1081; &#1092;&#1088;&#1072;&#1075;&#1084;&#1077;&#1085;&#1090;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Identification (32 &#x431;&#x438;&#x442;&#x430;)" STYLE_REF="yellow_node" ID="ID_948351313" CREATED="1419169753638" MODIFIED="1419258931411"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1063;&#1080;&#1089;&#1083;&#1086;, &#1080;&#1076;&#1077;&#1085;&#1090;&#1080;&#1092;&#1080;&#1094;&#1080;&#1088;&#1091;&#1102;&#1097;&#1077;&#1077; &#1086;&#1088;&#1080;&#1075;&#1080;&#1085;&#1072;&#1083;&#1100;&#1085;&#1099;&#1081; &#1087;&#1072;&#1082;&#1077;&#1090;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Authentication Header (AH)" STYLE_REF="pre_last_node" ID="ID_237755005" CREATED="1419162453567" MODIFIED="1419162524522"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1057;&#1086;&#1076;&#1077;&#1088;&#1078;&#1080;&#1090; &#1080;&#1085;&#1092;&#1086;&#1088;&#1084;&#1072;&#1094;&#1080;&#1102;, &#1080;&#1089;&#1087;&#1086;&#1083;&#1100;&#1079;&#1091;&#1077;&#1084;&#1091;&#1102;
    </p>
    <p>
      &#1076;&#1083;&#1103; &#1072;&#1091;&#1090;&#1077;&#1085;&#1090;&#1080;&#1092;&#1080;&#1082;&#1072;&#1094;&#1080;&#1102; &#1073;&#1086;&#1083;&#1100;&#1096;&#1077;&#1081; &#1095;&#1072;&#1089;&#1090;&#1080; &#1087;&#1072;&#1082;&#1077;&#1090;&#1072;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Encapsulating Security Payload (ESP)" STYLE_REF="pre_last_node" ID="ID_503095505" CREATED="1419162494163" MODIFIED="1419162524515"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1054;&#1089;&#1091;&#1097;&#1077;&#1089;&#1090;&#1074;&#1083;&#1103;&#1077;&#1090; &#1096;&#1080;&#1092;&#1088;&#1086;&#1074;&#1072;&#1085;&#1080;&#1077; &#1076;&#1072;&#1085;&#1085;&#1099;&#1093;
    </p>
    <p>
      &#1076;&#1083;&#1103; &#1073;&#1077;&#1079;&#1086;&#1087;&#1072;&#1089;&#1085;&#1099;&#1093; &#1087;&#1086;&#1076;&#1082;&#1083;&#1102;&#1095;&#1077;&#1085;&#1080;&#1081; (IPsec)
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="&#x41f;&#x440;&#x435;&#x434;&#x435;&#x43b;&#x44c;&#x43d;&#x43e;&#x435; &#x447;&#x438;&#x441;&#x43b;&#x43e; &#x448;&#x430;&#x433;&#x43e;&#x432;" STYLE_REF="orange_node" ID="ID_1316107110" CREATED="1419161100409" MODIFIED="1419161187898"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Hop Limit
    </p>
  </body>
</html>
</richcontent>
<node TEXT="&#x410;&#x43d;&#x430;&#x43b;&#x43e;&#x433; &#x43f;&#x43e;&#x43b;&#x44f; TTL" STYLE_REF="pre_last_node" ID="ID_1436219443" CREATED="1419161197063" MODIFIED="1419161672270"/>
</node>
</node>
</node>
</map>
