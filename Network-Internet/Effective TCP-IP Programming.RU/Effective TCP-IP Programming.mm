<map version="freeplane 1.3.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node STYLE_REF="main_node" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1425030040485" VGAP="32"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p style="text-align: center">
      &#1057;&#1086;&#1074;&#1077;&#1090;&#1099; &#1087;&#1086; &#1089;&#1077;&#1090;&#1077;&#1074;&#1086;&#1084;&#1091;
    </p>
    <p style="text-align: center">
      &#1087;&#1088;&#1086;&#1075;&#1088;&#1072;&#1084;&#1084;&#1080;&#1088;&#1086;&#1074;&#1072;&#1085;&#1080;&#1102;
    </p>
  </body>
</html>
</richcontent>
<hook NAME="AutomaticEdgeColor" COUNTER="5"/>
<hook NAME="MapStyle">
    <properties show_icon_for_attributes="true" show_note_icons="true"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
<stylenode TEXT="second_node" BACKGROUND_COLOR="#00cccc" STYLE="bubble">
<font SIZE="14"/>
</stylenode>
<stylenode TEXT="pre_last_node" BACKGROUND_COLOR="#99ff99" STYLE="bubble">
<edge STYLE="bezier" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="yellow_node" BACKGROUND_COLOR="#ffff66" STYLE="bubble"/>
<stylenode TEXT="orange_node" BACKGROUND_COLOR="#ff9900" STYLE="bubble">
<font BOLD="true"/>
</stylenode>
<stylenode TEXT="last_node" BACKGROUND_COLOR="#ffffff">
<edge STYLE="bezier" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="main_node" BACKGROUND_COLOR="#ccccff">
<font SIZE="18" BOLD="true"/>
<edge STYLE="sharp_bezier" WIDTH="4"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook URI="../../Images/idea-large.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x420;&#x430;&#x431;&#x435;&#x440;&#x438;&#x442;&#x435;&#x441;&#x44c; &#x441; &#x43e;&#x43f;&#x435;&#x440;&#x430;&#x446;&#x438;&#x435;&#x439; &#x437;&#x430;&#x43f;&#x438;&#x441;&#x438; &#x432; TCP" STYLE_REF="second_node" POSITION="left" ID="ID_1413634596" CREATED="1419260641508" MODIFIED="1425030812834">
<edge COLOR="#000000"/>
<font SIZE="11" BOLD="true"/>
<hook URI="../../Images/record-1.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x420;&#x430;&#x431;&#x43e;&#x442;&#x430; send / write" STYLE_REF="orange_node" ID="ID_738411261" CREATED="1419260874447" MODIFIED="1419261166960">
<node TEXT="&#x41f;&#x440;&#x43e;&#x441;&#x442;&#x43e; &#x43a;&#x43e;&#x43f;&#x438;&#x440;&#x443;&#x44e;&#x442; &#x434;&#x430;&#x43d;&#x43d;&#x44b;&#x435; &#x432; &#x43e;&#x447;&#x435;&#x440;&#x435;&#x434;&#x44c; &#x43d;&#x430; &#x43f;&#x435;&#x440;&#x435;&#x434;&#x430;&#x447;&#x443;" STYLE_REF="pre_last_node" ID="ID_1774404653" CREATED="1419260890759" MODIFIED="1419260983368"/>
<node TEXT="&#x421;&#x440;&#x430;&#x437;&#x443; &#x43f;&#x43e;&#x441;&#x43b;&#x435; &#x44d;&#x442;&#x43e;&#x433;&#x43e; &#x432;&#x43e;&#x437;&#x432;&#x440;&#x430;&#x449;&#x430;&#x44e;&#x442; &#x443;&#x43f;&#x440;&#x430;&#x432;&#x43b;&#x435;&#x43d;&#x438;&#x435;" STYLE_REF="pre_last_node" ID="ID_787433802" CREATED="1419260913390" MODIFIED="1419260983372"/>
<node TEXT="&#x41d;&#x435;&#x442; &#x433;&#x430;&#x440;&#x430;&#x43d;&#x442;&#x438;&#x438; &#x447;&#x442;&#x43e; &#x434;&#x430;&#x43d;&#x43d;&#x44b;&#x435; &#x431;&#x443;&#x434;&#x443;&#x442; &#x434;&#x43e;&#x441;&#x442;&#x430;&#x432;&#x43b;&#x435;&#x43d;&#x43d;&#x44b;" STYLE_REF="pre_last_node" ID="ID_860139186" CREATED="1419260951151" MODIFIED="1419260983375"/>
<node TEXT="&#x41e;&#x448;&#x438;&#x431;&#x43a;&#x438; &#x432;&#x43e; &#x432;&#x440;&#x435;&#x43c;&#x44f; &#x437;&#x430;&#x43f;&#x438;&#x441;&#x438; &#x440;&#x435;&#x434;&#x43a;&#x438;" STYLE_REF="pre_last_node" ID="ID_1400252420" CREATED="1419260954184" MODIFIED="1419260983378"/>
<node TEXT="&#x41e;&#x448;&#x438;&#x431;&#x43a;&#x438;" STYLE_REF="pre_last_node" ID="ID_951667377" CREATED="1419260963832" MODIFIED="1419260983380">
<node TEXT="&#x41d;&#x435;&#x432;&#x435;&#x440;&#x43d;&#x44b;&#x439; &#x434;&#x435;&#x441;&#x43a;&#x440;&#x438;&#x43f;&#x442;&#x43e;&#x440; &#x441;&#x43e;&#x43a;&#x435;&#x442;&#x430;" STYLE_REF="yellow_node" ID="ID_1993601946" CREATED="1419260967263" MODIFIED="1419261058841"/>
<node TEXT="&#x414;&#x435;&#x441;&#x43a;&#x440;&#x438;&#x43f;&#x442;&#x43e;&#x440; &#x443;&#x43a;&#x430;&#x437;&#x438;&#x432;&#x430;&#x435;&#x442; &#x43d;&#x435; &#x43d;&#x430; &#x441;&#x43e;&#x43a;&#x435;&#x442;" STYLE_REF="yellow_node" ID="ID_295624583" CREATED="1419260985696" MODIFIED="1419261058837"/>
<node TEXT="&#x423;&#x43a;&#x430;&#x437;&#x430;&#x43d;&#x43d;&#x44b;&#x439; &#x441;&#x43e;&#x43a;&#x435;&#x442; &#x43d;&#x435; &#x441;&#x443;&#x449;&#x435;&#x441;&#x442;&#x432;&#x443;&#x435;&#x442;&#xa;&#x438;&#x43b;&#x438; &#x43d;&#x435; &#x43f;&#x43e;&#x434;&#x441;&#x43e;&#x435;&#x434;&#x438;&#x43d;&#x435;&#x43d;" STYLE_REF="yellow_node" ID="ID_677224501" CREATED="1419260997704" MODIFIED="1419261058833"/>
<node TEXT="&#x410;&#x434;&#x440;&#x435;&#x441; &#x431;&#x443;&#x444;&#x435;&#x440;&#x430; &#x443;&#x43a;&#x430;&#x437;&#x430;&#x43d; &#x43d;&#x430; &#x43d;&#x435;&#x434;&#x43e;&#x43f;&#x443;&#x441;&#x442;&#x438;&#x43c;&#x44b;&#x439; &#x430;&#x434;&#x440;&#x435;&#x441;" STYLE_REF="yellow_node" ID="ID_797119304" CREATED="1419261023072" MODIFIED="1419261058832"/>
<node TEXT="&#x421;&#x431;&#x440;&#x43e;&#x441; &#x441;&#x43e;&#x435;&#x434;&#x438;&#x43d;&#x435;&#x43d;&#x438;&#x44f; &#x445;&#x43e;&#x441;&#x442;&#x43e;&#x43c;" STYLE_REF="yellow_node" ID="ID_227024426" CREATED="1419261039864" MODIFIED="1419261058831"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      EPIPE
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="&#x424;&#x430;&#x43a;&#x442;&#x43e;&#x440;&#x44b; &#x432;&#x43b;&#x438;&#x44f;&#x44e;&#x449;&#x438;&#x435;&#xa;&#x43d;&#x430; &#x441;&#x442;&#x440;&#x430;&#x442;&#x435;&#x433;&#x438;&#x44e; &#x43e;&#x442;&#x43f;&#x440;&#x430;&#x432;&#x43a;&#x438;" STYLE_REF="orange_node" ID="ID_1605145314" CREATED="1419261103576" MODIFIED="1419262292410" VGAP="23">
<node TEXT="MAX &#x447;&#x438;&#x441;&#x43b;&#x43e; &#x431;&#x430;&#x439;&#x442; &#x43a;&#x43e;&#x442;&#x43e;&#x440;&#x43e;&#x435; &#x43c;&#x43e;&#x436;&#x435;&#x442; &#x43f;&#x43e;&#x441;&#x43b;&#x430;&#x442;&#x44c; TCP,&#xa;&#x44d;&#x442;&#x43e; &#x43c;&#x438;&#x43d;&#x438;&#x43c;&#x430;&#x43b;&#x44c;&#x43d;&#x430;&#x44f; &#x438;&#x437; &#x432;&#x435;&#x43b;&#x438;&#x447;&#x438;&#x43d;" STYLE_REF="yellow_node" ID="ID_351393257" CREATED="1419261241569" MODIFIED="1419262292405">
<node TEXT="MSS" STYLE_REF="pre_last_node" ID="ID_856357977" CREATED="1419261286322" MODIFIED="1419261815604" BACKGROUND_COLOR="#ffcccc">
<font BOLD="false"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Maximum sigment size
    </p>
  </body>
</html>
</richcontent>
<node TEXT="MSS &#x432;&#x44b;&#x447;&#x438;&#x441;&#x43b;&#x44f;&#x435;&#x442;&#x441;&#x44f; &#x43d;&#x430; &#x43e;&#x441;&#x43d;&#x43e;&#x432;&#x435; MTU" STYLE_REF="pre_last_node" ID="ID_145398359" CREATED="1419261348723" MODIFIED="1419261823694"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Maximum transmission unit
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&#x41a;&#x430;&#x436;&#x434;&#x430;&#x44f; &#x441;&#x442;&#x43e;&#x440;&#x43e;&#x43d;&#x430; &#x443;&#x43a;&#x430;&#x437;&#x438;&#x432;&#x430;&#x435;&#x442; &#x43f;&#x440;&#x438;&#x435;&#x43c;&#x43b;&#x438;&#x43c;&#x44b;&#x439; &#x434;&#x43b;&#x44f; &#x43d;&#x435;&#x435; MSS" STYLE_REF="pre_last_node" ID="ID_1836570612" CREATED="1419261384771" MODIFIED="1419261823698"/>
<node TEXT="&#x41d;&#x430; &#x434;&#x440;&#x443;&#x433;&#x43e;&#x43c; &#x43a;&#x43e;&#x43d;&#x446;&#x435; &#x43e;&#x431;&#x44f;&#x437;&#x430;&#x43d;&#x44b; &#x443;&#x434;&#x43e;&#x432;&#x43b;&#x435;&#x442;&#x432;&#x43e;&#x440;&#x438;&#x442;&#x44c; &#x43f;&#x440;&#x435;&#x434;&#x43b;&#x43e;&#x436;&#x435;&#x43d;&#x43d;&#x44b;&#x439; MSS" STYLE_REF="pre_last_node" ID="ID_281461492" CREATED="1419261409275" MODIFIED="1419261823701"/>
</node>
<node TEXT="&#x41e;&#x43a;&#x43d;&#x43e; &#x43f;&#x435;&#x440;&#x435;&#x434;&#x430;&#x447;&#x438;" STYLE_REF="pre_last_node" ID="ID_1350050621" CREATED="1419261303522" MODIFIED="1419261833340" BACKGROUND_COLOR="#ffcccc">
<font BOLD="false"/>
</node>
<node TEXT="&#x41e;&#x43a;&#x43d;&#x43e; &#x43f;&#x435;&#x440;&#x435;&#x433;&#x440;&#x443;&#x437;&#x43a;&#x438;" STYLE_REF="pre_last_node" ID="ID_1013886328" CREATED="1419261311378" MODIFIED="1419261815606" BACKGROUND_COLOR="#ffcccc">
<font BOLD="false"/>
<node TEXT="&#x41d;&#x435; &#x434;&#x430;&#x435;&#x442; &#x43f;&#x440;&#x435;&#x432;&#x44b;&#x441;&#x438;&#x442;&#x44c; &#x43f;&#x440;&#x43e;&#x43f;&#x443;&#x441;&#x43a;&#x43d;&#x443;&#x44e; &#x441;&#x43f;&#x43e;&#x441;&#x43e;&#x431;&#x43d;&#x43e;&#x441;&#x442;&#x44c; &#x441;&#x435;&#x442;&#x438;" STYLE_REF="pre_last_node" ID="ID_333256860" CREATED="1419261468452" MODIFIED="1419261823705"/>
<node TEXT="&#x423;&#x43f;&#x440;&#x430;&#x432;&#x43b;&#x44f;&#x435;&#x442;&#x441;&#x44f; &#x430;&#x43b;&#x433;&#x43e;&#x440;&#x438;&#x442;&#x43c;&#x43e;&#x43c; &#x443;&#x43f;&#x440;&#x430;&#x432;&#x43b;&#x435;&#x43d;&#x438;&#x44f; &#x43f;&#x435;&#x440;&#x435;&#x433;&#x440;&#x443;&#x437;&#x43a;&#x43e;&#x439;" STYLE_REF="pre_last_node" ID="ID_1190720659" CREATED="1419261490644" MODIFIED="1419261823707">
<node TEXT="1. &#x41c;&#x435;&#x434;&#x43b;&#x435;&#x43d;&#x43d;&#x44b;&#x439; &#x441;&#x442;&#x430;&#x440;&#x442;" STYLE_REF="yellow_node" ID="ID_1627609002" CREATED="1419261567220" MODIFIED="1419261847909" BACKGROUND_COLOR="#ffccff">
<node TEXT="&#x41f;&#x43e;&#x441;&#x442;&#x435;&#x43f;&#x435;&#x43d;&#x43d;&#x43e;&#x435; &#x443;&#x432;&#x435;&#x43b;&#x438;&#x447;&#x435;&#x43d;&#x438;&#x435; &#x447;&#x430;&#x441;&#x442;&#x43e;&#x442;&#x44b; &#x43f;&#x435;&#x440;&#x435;&#x434;&#x430;&#x447;&#x438;&#xa;&#x441;&#x435;&#x433;&#x43c;&#x435;&#x43d;&#x442;&#x43e;&#x432; &#x434;&#x43e; &#x43f;&#x43e;&#x440;&#x43e;&#x433;&#x43e;&#x432;&#x43e;&#x439; &#x432;&#x435;&#x43b;&#x438;&#x447;&#x438;&#x43d;&#x44b;" STYLE_REF="yellow_node" ID="ID_1509859345" CREATED="1419261608333" MODIFIED="1419261859925" BACKGROUND_COLOR="#ccffcc"/>
<node TEXT="&#x41d;&#x430;&#x440;&#x43e;&#x441;&#x442;&#x430;&#x43d;&#x438;&#x435; &#x447;&#x430;&#x441;&#x442;&#x43e;&#x442;&#x44b; - &#x44d;&#x43a;&#x441;&#x43f;&#x43e;&#x442;&#x435;&#x43d;&#x446;&#x438;&#x430;&#x43b;&#x44c;&#x43d;&#x43e;" STYLE_REF="yellow_node" ID="ID_200989221" CREATED="1419261646597" MODIFIED="1419261859929" BACKGROUND_COLOR="#ccffcc"/>
</node>
<node TEXT="2. &#x410;&#x43b;&#x433;&#x43e;&#x440;&#x438;&#x442;&#x43c;&#x44b; &#x438;&#x437;&#x431;&#x435;&#x436;&#x430;&#x43d;&#x438;&#x44f; &#x43f;&#x435;&#x440;&#x435;&#x433;&#x440;&#x443;&#x437;&#x43a;&#x438;" STYLE_REF="yellow_node" ID="ID_1630742677" CREATED="1419261577229" MODIFIED="1419261847912" BACKGROUND_COLOR="#ffccff">
<node TEXT="&#x412;&#x43a;&#x43b;&#x44e;&#x447;&#x430;&#x435;&#x442;&#x441;&#x44f; &#x432; &#x440;&#x430;&#x431;&#x43e;&#x442;&#x443; &#x43a;&#x43e;&#x433;&#x434;&#x430; &#x434;&#x43e;&#x441;&#x442;&#x438;&#x433;&#x43d;&#x443;&#x442;&#xa;&#x43f;&#x43e;&#x440;&#x43e;&#x433; &#x43c;&#x435;&#x434;&#x43b;&#x435;&#x43d;&#x43d;&#x43e;&#x433;&#x43e; &#x441;&#x442;&#x430;&#x440;&#x442;&#x430;" STYLE_REF="yellow_node" ID="ID_1125879067" CREATED="1419261666446" MODIFIED="1419261859930" BACKGROUND_COLOR="#ccffcc"/>
<node TEXT="&#x417;&#x43e;&#x43d;&#x434;&#x438;&#x440;&#x443;&#x435;&#x442; - &#x43d;&#x435; &#x443;&#x432;&#x435;&#x43b;&#x438;&#x447;&#x438;&#x43b;&#x430;&#x441;&#x44c; &#x43b;&#x438;&#xa;&#x43f;&#x440;&#x43e;&#x43f;&#x443;&#x441;&#x43a;&#x43d;&#x430;&#x44f; &#x441;&#x43f;&#x43e;&#x441;&#x43e;&#x431;&#x43d;&#x43e;&#x441;&#x442;&#x44c; &#x441;&#x435;&#x442;&#x438;" STYLE_REF="yellow_node" ID="ID_1502538063" CREATED="1419261701086" MODIFIED="1419261859936" BACKGROUND_COLOR="#ccffcc"/>
<node TEXT="&#x41e;&#x43a;&#x43d;&#x43e; &#x43f;&#x435;&#x440;&#x435;&#x433;&#x440;&#x443;&#x437;&#x43a;&#x438; &#x443;&#x432;&#x435;&#x43b;&#x438;&#x447;&#x438;&#x432;&#x430;&#x435;&#x442;&#x441;&#x44f;&#xa;&#x43b;&#x438;&#x43d;&#x435;&#x439;&#x43d;&#x43e; &#x43f;&#x43e; &#x43e;&#x434;&#x43d;&#x43e;&#x43c;&#x443; &#x43f;&#x430;&#x43a;&#x435;&#x442;&#x443; &#x437;&#x430; RTT" STYLE_REF="yellow_node" ID="ID_929239657" CREATED="1419261725934" MODIFIED="1419261859939" BACKGROUND_COLOR="#ccffcc"/>
<node TEXT="&#x41c;&#x43e;&#x436;&#x435;&#x442; &#x441;&#x443;&#x437;&#x438;&#x442;&#x44c; &#x43e;&#x43a;&#x43d;&#x43e; &#x43f;&#x435;&#x440;&#x435;&#x433;&#x440;&#x443;&#x437;&#x43a;&#x438;" STYLE_REF="yellow_node" ID="ID_1912722583" CREATED="1419261761775" MODIFIED="1419261859940" BACKGROUND_COLOR="#ccffcc"/>
</node>
</node>
</node>
</node>
<node TEXT="&#x410;&#x43b;&#x433;&#x43e;&#x440;&#x438;&#x442;&#x43c; &#x41d;&#x435;&#x439;&#x433;&#x43b;&#x430;" STYLE_REF="yellow_node" ID="ID_1752170731" CREATED="1419261881783" MODIFIED="1419261895255">
<node TEXT="&#x422;&#x440;&#x435;&#x431;&#x443;&#x435;&#x442; &#x447;&#x442;&#x43e;&#x431;&#x44b; &#x43d;&#x435; &#x431;&#x44b;&#x43b;&#x43e; &gt; 1 &#x43d;&#x435;&#x43f;&#x43e;&#x434;&#x442;&#x432;&#x435;&#x440;&#x436;&#x434;&#x435;&#x43d;&#x43d;&#x43e;&#x433;&#x43e;&#xa;&#x43c;&#x430;&#x43b;&#x435;&#x43d;&#x44c;&#x43a;&#x43e;&#x433;&#x43e; &#x43f;&#x430;&#x43a;&#x435;&#x442;&#x430; &lt; MSS" ID="ID_194961926" CREATED="1419261931224" MODIFIED="1419262050460" BACKGROUND_COLOR="#ffcccc"/>
<node TEXT="&#x421;&#x43c;&#x44b;&#x441;&#x43b; &#x432; &#x442;&#x43e;&#x43c;, &#x447;&#x442;&#x43e;&#x431;&#x44b; &#x43d;&#x435; &#x434;&#x430;&#x442;&#x44c; &#x437;&#x430;&#x431;&#x438;&#x442;&#x44c;&#xa;&#x441;&#x435;&#x442;&#x44c; &#x43c;&#x430;&#x43b;&#x435;&#x43d;&#x44c;&#x43a;&#x438;&#x43c;&#x438; &#x441;&#x435;&#x433;&#x43c;&#x435;&#x43d;&#x442;&#x430;&#x43c;&#x438;" ID="ID_512599129" CREATED="1419261980472" MODIFIED="1419262050457" BACKGROUND_COLOR="#ffcccc"/>
<node TEXT="&#x41f;&#x43e;&#x437;&#x432;&#x43e;&#x43b;&#x44f;&#x435;&#x442; &#x438;&#x437;&#x431;&#x435;&#x436;&#x430;&#x442;&#x44c; SWS" ID="ID_1212410365" CREATED="1419262022880" MODIFIED="1419262050454" BACKGROUND_COLOR="#ffcccc"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Silly window syndrome
    </p>
  </body>
</html>
</richcontent>
<node TEXT="SWS - &#x43a;&#x43e;&#x433;&#x434;&#x430;  &#x43e;&#x442;&#x43f;&#x440;&#x430;&#x432;&#x43b;&#x44f;&#x44e;&#x442;&#x441;&#x44f;&#xa;&#x43d;&#x435;&#x431;&#x43e;&#x43b;&#x44c;&#x448;&#x438;&#x435; &#x43e;&#x431;&#x44a;&#x435;&#x43c;&#x44b; &#x434;&#x430;&#x43d;&#x43d;&#x44b;&#x445;" STYLE_REF="pre_last_node" ID="ID_637134679" CREATED="1419262122945" MODIFIED="1419262201534"/>
</node>
<node TEXT="&#x41e;&#x442;&#x43a;&#x43b;&#x44e;&#x447;&#x435;&#x43d;&#x438;&#x435; &#x430;&#x43b;&#x433;&#x43e;&#x440;&#x438;&#x442;&#x43c;&#x430;" ID="ID_874900307" CREATED="1419262690550" MODIFIED="1419262758637" BACKGROUND_COLOR="#ffcccc"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      const int on = 1;
    </p>
    <p>
      setsockopt(s, IPPROTO_TCP, TCP_NODELAY, &amp;on, sizeof(on));
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="&#x414;&#x430;&#x43d;&#x43d;&#x44b;&#x435; &#x43e;&#x442;&#x43f;&#x440;&#x430;&#x432;&#x43b;&#x44f;&#x442;&#x441;&#x44f; &#x43f;&#x440;&#x438;&#xa;&#x432;&#x44b;&#x43f;&#x43e;&#x43b;&#x43d;&#x435;&#x43d;&#x438;&#x438; &#x43e;&#x434;&#x43d;&#x43e;&#x433;&#x43e; &#x438;&#x437; &#x443;&#x441;&#x43b;&#x43e;&#x432;&#x438;&#x439;" STYLE_REF="yellow_node" ID="ID_168851614" CREATED="1419262209539" MODIFIED="1419262251497">
<node TEXT="&#x41c;&#x43e;&#x436;&#x43d;&#x430; &#x43f;&#x43e;&#x441;&#x43b;&#x430;&#x442;&#x44c; &#x43f;&#x43e;&#x43b;&#x43d;&#x44b;&#x439; &#x441;&#x435;&#x433;&#x43c;&#x435;&#x43d;&#x442; = MSS" STYLE_REF="pre_last_node" ID="ID_538959505" CREATED="1419262268595" MODIFIED="1419262286009"/>
<node TEXT="&#x421;&#x43e;&#x435;&#x434;&#x438;&#x43d;&#x435;&#x43d;&#x438;&#x435; &#x43f;&#x440;&#x43e;&#x441;&#x442;&#x430;&#x438;&#x432;&#x430;&#x435;&#x442; &#x438; &#x43c;&#x43e;&#x436;&#x43d;&#x43e; &#x43e;&#x43f;&#x443;&#x441;&#x442;&#x43e;&#x448;&#x438;&#x442;&#x44c; &#x431;&#x443;&#x444;&#x435;&#x440; &#x43f;&#x435;&#x440;&#x435;&#x434;&#x430;&#x447;&#x438;" STYLE_REF="pre_last_node" ID="ID_1817205878" CREATED="1419262307163" MODIFIED="1419262506837"/>
<node TEXT="&#x410;&#x43b;&#x433;&#x43e;&#x440;&#x438;&#x442;&#x43c; &#x41d;&#x435;&#x439;&#x433;&#x43b;&#x430; &#x43e;&#x442;&#x43a;&#x43b;&#x44e;&#x447;&#x435;&#x43d; &#x438; &#x43c;&#x43e;&#x436;&#x43d;&#x430; &#x43e;&#x43f;&#x443;&#x441;&#x442;&#x43e;&#x448;&#x438;&#x442;&#x44c; &#x431;&#x443;&#x444;&#x435;&#x440; &#x43f;&#x435;&#x440;&#x435;&#x434;&#x430;&#x447;&#x438;" STYLE_REF="pre_last_node" ID="ID_878255863" CREATED="1419262334619" MODIFIED="1419262506843"/>
<node TEXT="&#x415;&#x441;&#x442;&#x44c; &#x441;&#x440;&#x43e;&#x447;&#x43d;&#x44b;&#x435; &#x434;&#x430;&#x43d;&#x43d;&#x44b;&#x435; &#x434;&#x43b;&#x44f; &#x43e;&#x442;&#x43f;&#x440;&#x430;&#x432;&#x43a;&#x438;" STYLE_REF="pre_last_node" ID="ID_1285483584" CREATED="1419262361180" MODIFIED="1419262506845"/>
<node TEXT="&#x415;&#x441;&#x442;&#x44c; &#x43c;&#x430;&#x43b;&#x435;&#x43d;&#x44c;&#x43a;&#x438;&#x439; &#x441;&#x435;&#x433;&#x43c;&#x435;&#x43d;&#x442; &#x43d;&#x43e; &#x43e;&#x442;&#x43f;&#x440;&#x430;&#x432;&#x43a;&#x430; &#x435;&#x433;&#x43e; &#x437;&#x430;&#x434;&#x435;&#x440;&#x436;&#x430;&#x43d;&#x430; &#x43d;&#x430; &#x434;&#x43e;&#x43b;&#x433;&#x43e;, &#x434;&#x43b;&#x44f; BSD 5 - 60 c" STYLE_REF="pre_last_node" ID="ID_687290928" CREATED="1419262379211" MODIFIED="1419262506846"/>
<node TEXT="&#x41e;&#x43a;&#x43d;&#x43e; &#x43f;&#x440;&#x438;&#x435;&#x43c;&#x430; &#x43d;&#x430; &#x434;&#x440;&#x443;&#x433;&#x43e;&#x43c; &#x43a;&#x43e;&#x43d;&#x446;&#x435; &#x43e;&#x442;&#x43a;&#x440;&#x44b;&#x442;&#x43e; &#x43d;&#x435; &#x43c;&#x435;&#x43d;&#x438;&#x435; &#x447;&#x435;&#x43c; &#x43d;&#x430; &#x43f;&#x43e;&#x43b;&#x43e;&#x432;&#x438;&#x43d;&#x443;" STYLE_REF="pre_last_node" ID="ID_696075549" CREATED="1419262418756" MODIFIED="1419262506848"/>
<node TEXT="&#x41d;&#x435;&#x43e;&#x431;&#x445;&#x43e;&#x434;&#x438;&#x43c;&#x43e; &#x43f;&#x43e;&#x432;&#x442;&#x43e;&#x440;&#x43d;&#x43e; &#x43f;&#x435;&#x440;&#x435;&#x434;&#x430;&#x442;&#x44c; &#x441;&#x435;&#x433;&#x43c;&#x435;&#x43d;&#x442;" STYLE_REF="pre_last_node" ID="ID_1863240035" CREATED="1419262449748" MODIFIED="1419262506849"/>
<node TEXT="&#x41f;&#x43e;&#x441;&#x43b;&#x430;&#x442;&#x44c; &#x410;&#x421;&#x41a; &#x43d;&#x430; &#x43f;&#x440;&#x438;&#x439;&#x43d;&#x44f;&#x442;&#x44b;&#x435; &#x434;&#x430;&#x43d;&#x43d;&#x44b;&#x435;" STYLE_REF="pre_last_node" ID="ID_888934462" CREATED="1419262463260" MODIFIED="1419262506850"/>
<node TEXT="&#x41d;&#x443;&#x436;&#x43d;&#x43e; &#x43e;&#x431;&#x44f;&#x432;&#x438;&#x442;&#x44c; &#x43e;&#x431; &#x43e;&#x431;&#x43d;&#x43e;&#x432;&#x43b;&#x435;&#x43d;&#x438;&#x438; &#x43e;&#x43a;&#x43d;&#x430;" STYLE_REF="pre_last_node" ID="ID_44642101" CREATED="1419262478588" MODIFIED="1419262506851"/>
</node>
</node>
</node>
<node TEXT="UDP-connected" STYLE_REF="second_node" POSITION="right" ID="ID_1074725391" CREATED="1419255935036" MODIFIED="1419261186440">
<edge COLOR="#006666"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1056;&#1072;&#1079;&#1073;&#1077;&#1088;&#1080;&#1090;&#1077;&#1089;&#1100;, &#1095;&#1090;&#1086; &#1090;&#1072;&#1082;&#1086;&#1077; &#1087;&#1086;&#1076;&#1089;&#1086;&#1077;&#1076;&#1080;&#1085;&#1077;&#1085;&#1085;&#1099;&#1081; UDP-&#1089;&#1086;&#1082;&#1077;&#1090;
    </p>
  </body>
</html>
</richcontent>
<font SIZE="11" BOLD="true"/>
<node TEXT="UDP-&#x441;&#x43e;&#x43a;&#x435;&#x442; &#x441; &#x432;&#x44b;&#x437;&#x432;&#x430;&#x43d;&#x43d;&#x44b;&#x43c; connect" STYLE_REF="pre_last_node" ID="ID_224067037" CREATED="1419256029404" MODIFIED="1419256251935"/>
<node TEXT="&#x41f;&#x43e;&#x437;&#x432;&#x43e;&#x43b;&#x44f;&#x435;&#x442; &#x434;&#x435;&#x43b;&#x430;&#x442;&#x44c; send &amp; write&#xa;&#x432;&#x43c;&#x435;&#x441;&#x442;&#x43e; sendto" STYLE_REF="pre_last_node" ID="ID_1737965182" CREATED="1419256055181" MODIFIED="1419256251929"/>
<node TEXT="&#x415;&#x441;&#x43b;&#x438; &#x43f;&#x43e;&#x440;&#x442; &#x43d;&#x430; &#x434;&#x440;&#x443;&#x433;&#x43e;&#x43c; &#x43a;&#x43e;&#x43d;&#x446;&#x435; &#x43d;&#x435; &#x434;&#x43e;&#x441;&#x442;&#x443;&#x43f;&#x435;&#x43d; &#x442;&#x43e;&#xa;&#x432;&#x435;&#x440;&#x43d;&#x435;&#x442; ICMP msg, sendto - &#x43d;&#x435; &#x432;&#x435;&#x440;&#x43d;&#x435;&#x442;" STYLE_REF="pre_last_node" ID="ID_1782264927" CREATED="1419256098573" MODIFIED="1419256251923"/>
<node TEXT="&#x41f;&#x43e;&#x432;&#x44b;&#x448;&#x435;&#x43d;&#x438;&#x435; &#x43f;&#x440;&#x43e;&#x438;&#x437;&#x432;&#x43e;&#x434;&#x438;&#x442;&#x435;&#x43b;&#x44c;&#x43d;&#x43e;&#x441;&#x442;&#x438;" STYLE_REF="pre_last_node" ID="ID_1714087627" CREATED="1419256142390" MODIFIED="1419256251919">
<node TEXT="connect &#x434;&#x435;&#x43b;&#x430;&#x435;&#x442; &#x441;&#x438;&#x43d;&#x445;&#x440;&#x43e;&#x43d;&#x438;&#x437;&#x430;&#x446;&#x438;&#x44e; 1 &#x440;&#x430;&#x437; &#x43d;&#x430; &#x441;&#x43e;&#x435;&#x434;&#x438;&#x43d;&#x435;&#x43d;&#x438;&#x435;" STYLE_REF="yellow_node" ID="ID_1578590394" CREATED="1419256181758" MODIFIED="1419256257246"/>
<node TEXT="sendto &#x434;&#x435;&#x43b;&#x430;&#x435;&#x442; &#x441;&#x438;&#x43d;&#x445;&#x440;&#x43e;&#x43d;&#x438;&#x437;&#x430;&#x446;&#x438;&#x44e; &#x43a;&#x430;&#x436;&#x434;&#x44b;&#x439; &#x440;&#x430;&#x437;" STYLE_REF="yellow_node" ID="ID_455740025" CREATED="1419256221214" MODIFIED="1419256257251"/>
</node>
</node>
<node TEXT="&#x41d;&#x430; &#x447;&#x442;&#x43e; &#x432;&#x43b;&#x438;&#x44f;&#x44e;&#x442; &#x440;&#x430;&#x437;&#x43c;&#x435;&#x440;&#x44b; &#x431;&#x443;&#x444;&#x435;&#x440;&#x43e;&#x432;" STYLE_REF="second_node" POSITION="right" ID="ID_1916647557" CREATED="1419256267910" MODIFIED="1425030326546">
<edge COLOR="#996600"/>
<font SIZE="11" BOLD="true"/>
<hook URI="../../Images/buf.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x412;&#x43b;&#x438;&#x44f;&#x44e;&#x442; &#x43d;&#x430; &#x43f;&#x440;&#x43e;&#x438;&#x437;&#x432;&#x43e;&#x434;&#x438;&#x442;&#x435;&#x43b;&#x44c;&#x43d;&#x43e;&#x441;&#x442;&#x44c;" STYLE_REF="pre_last_node" ID="ID_1936603647" CREATED="1419256315287" MODIFIED="1419256631298"/>
<node TEXT="&#x420;&#x430;&#x437;&#x43c;&#x435;&#x440; &#x431;&#x443;&#x444; &gt;= &#x43f;&#x43e;&#x43b;&#x43e;&#x441;&#x430; &#x43f;&#x440;&#x43e;&#x43f;&#x443;&#x441;&#x43a;&#x430;&#x43d;&#x438;&#x44f; * &#x437;&#x430;&#x434;&#x435;&#x440;&#x436;&#x43a;&#x430;" STYLE_REF="pre_last_node" ID="ID_812410634" CREATED="1419256328919" MODIFIED="1419256631308"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      BWD = bandwidth * RTT
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="BWD - &#x43e;&#x431;&#x44a;&#x435;&#x43c; &#x442;&#x440;&#x443;&#x431;&#x44b; &#x432; &#x431;&#x438;&#x442;&#x430;&#x445;" STYLE_REF="pre_last_node" ID="ID_845819995" CREATED="1419256412448" MODIFIED="1419256631312"/>
<node TEXT="&#x427;&#x442;&#x43e;&#x431;&#x44b; &#x432; &#x431;&#x443;&#x444; &#x432;&#x43b;&#x435;&#x437;&#x430;&#x43b;&#x43e; &#x441;&#x442;&#x43e;&#x43b;&#x44c;&#x43a;&#x43e; &#x441;&#x435;&#x433;&#x43c;&#x435;&#x43d;&#x442;&#x43e;&#x432; &#x441;&#x43a;&#x43e;&#x43b;&#x44c;&#x43a;&#x43e;&#xa;&#x43d;&#x430;&#x445;&#x43e;&#x434;&#x438;&#x442;&#x441;&#x44f; &#x432; &#x437;&#x430;&#x43f;&#x43e;&#x43b;&#x43d;&#x435;&#x43d;&#x43d;&#x43e;&#x439; &#x442;&#x440;&#x443;&#x431;&#x435;" STYLE_REF="pre_last_node" ID="ID_830274274" CREATED="1419256449224" MODIFIED="1419256835834">
<hook URI="buf-size.jpg" SIZE="1.0" NAME="ExternalObject"/>
</node>
<node TEXT="&#x420;&#x430;&#x437;&#x43c;&#x435;&#x440; &#x431;&#x443;&#x444; &gt; 3 MSS [&#x41e;&#x431;&#x44f;&#x437;&#x430;&#x442;&#x435;&#x43b;&#x44c;&#x43d;&#x43e;]" STYLE_REF="pre_last_node" ID="ID_968634021" CREATED="1419256497767" MODIFIED="1419256631320"/>
<node TEXT="setsockopt(s, SOL_SOCKET, SO_RCVBUF, (char *) bufsz, sizeof(bufsz));" STYLE_REF="pre_last_node" ID="ID_1555737154" CREATED="1419256534864" MODIFIED="1419256631321"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1059;&#1089;&#1090;&#1072;&#1085;&#1086;&#1074;&#1082;&#1072; &#1088;&#1072;&#1079;&#1084;&#1077;&#1088;&#1072; &#1073;&#1091;&#1092;&#1077;&#1088;&#1072;.
    </p>
    <p>
      &#1053;&#1091;&#1078;&#1085;&#1086; &#1076;&#1077;&#1083;&#1072;&#1090;&#1100; &#1087;&#1077;&#1088;&#1077;&#1076; bind &amp; listen
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="&#x41f;&#x440;&#x43e;&#x433;&#x440;&#x430;&#x43c;&#x43c;&#x430; &#x440;&#x430;&#x431;&#x43e;&#x442;&#x430;&#x44e;&#x449;&#x430;&#x44f; &#x432; &#x43b;&#x43e;&#x43a; &#x441;&#x435;&#x442;&#x438;&#xa;&#x43c;&#x43e;&#x436;&#x435;&#x442; &#x43d;&#x435; &#x440;&#x430;&#x431;&#x43e;&#x442;&#x430;&#x442;&#x44c; &#x432; &#x433;&#x43b;&#x43e;&#x431; &#x441;&#x435;&#x442;&#x438;" STYLE_REF="second_node" POSITION="right" ID="ID_145694807" CREATED="1419259732244" MODIFIED="1419261186432">
<edge COLOR="#7c0000"/>
<font SIZE="11" BOLD="true"/>
<node TEXT="&#x41d;&#x435;&#x434;&#x43e;&#x441;&#x442;&#x430;&#x442;&#x43e;&#x447;&#x43d;&#x430;&#x44f; &#x43f;&#x440;&#x43e;&#x438;&#x437;&#x432;&#x43e;&#x434;&#x438;&#x442;&#x435;&#x43b;&#x44c;&#x43d;&#x43e;&#x441;&#x442;&#x44c; &#x43f;&#x440;&#x43e;&#x433;&#x440;&#x430;&#x43c;&#x43c;&#x438;" STYLE_REF="pre_last_node" ID="ID_1121793888" CREATED="1419259895622" MODIFIED="1419260018954"/>
<node TEXT="&#x41a;&#x43e;&#x434; &#x440;&#x430;&#x431;&#x43e;&#x442;&#x430;&#x432;&#x448;&#x438;&#x439; &#x432; &#x43b;&#x43e;&#x43a; &#x441;&#x435;&#x442;&#x438; &#x43c;&#x43e;&#x436;&#x435;&#x442; &#x43d;&#x435; &#x440;&#x430;&#x431;&#x43e;&#x442;&#x430;&#x442;&#x44c; &#x432; &#x433;&#x43b;&#x43e;&#x431; &#x441;&#x435;&#x442;&#x438;" STYLE_REF="pre_last_node" ID="ID_972261059" CREATED="1419259969863" MODIFIED="1419259995703"/>
</node>
<node TEXT="&#x41d;&#x435; &#x437;&#x430;&#x448;&#x438;&#x432;&#x430;&#x439;&#x442;&#x435; IP-&#x430;&#x434;&#x440;&#x435;&#x441;&#x430;&#xa;&#x438; &#x43d;&#x43e;&#x43c;&#x435;&#x440;&#x430; &#x43f;&#x43e;&#x440;&#x442;&#x43e;&#x432; &#x432; &#x43a;&#x43e;&#x434;" STYLE_REF="second_node" POSITION="left" ID="ID_1174868748" CREATED="1419170761541" MODIFIED="1419263596365">
<edge COLOR="#0000cc"/>
<font SIZE="11" BOLD="true"/>
<node TEXT="&#x410;&#x43b;&#x44c;&#x442;&#x435;&#x440;&#x43d;&#x430;&#x442;&#x438;&#x432;&#x44b;" STYLE_REF="orange_node" ID="ID_68383292" CREATED="1419170805196" MODIFIED="1419262620555">
<node TEXT="&#x418;&#x437; &#x438;&#x440;&#x433;&#x443;&#x43c;&#x435;&#x43d;&#x442;&#x43e;&#x432; &#x43a;&#x43e;&#x43c;&#x430;&#x43d;&#x434;&#x43d;&#x43e;&#x438; &#x441;&#x442;&#x440;&#x43e;&#x43a;&#x438;" STYLE_REF="pre_last_node" ID="ID_717089730" CREATED="1419170824288" MODIFIED="1419262625926"/>
<node TEXT="&#x427;&#x435;&#x440;&#x435;&#x437; GUI" STYLE_REF="pre_last_node" ID="ID_189123216" CREATED="1419170835516" MODIFIED="1419262625930"/>
<node TEXT="&#x421; &#x43f;&#x43e;&#x43c;&#x43e;&#x449;&#x44c;&#x44e; &#x444;&#x443;&#x43d;&#x43a;&#x446;&#x438;&#x439;" STYLE_REF="pre_last_node" ID="ID_1851307348" CREATED="1419170842099" MODIFIED="1419262625932"/>
</node>
<node TEXT="&#x424;&#x443;&#x43d;&#x43a;&#x446;&#x438;&#x438;" STYLE_REF="orange_node" ID="ID_1724240589" CREATED="1419170813968" MODIFIED="1419263556256">
<hook NAME="FirstGroupNode"/>
<node TEXT="struct hostent *gethostbyname(const char *name);" STYLE_REF="pre_last_node" ID="ID_470231663" CREATED="1419262813128" MODIFIED="1419263595129">
<hook NAME="FirstGroupNode"/>
</node>
<node TEXT="... *gethostbyname2(const char *name, int af);" STYLE_REF="pre_last_node" ID="ID_981876429" CREATED="1419262885840" MODIFIED="1419263237586"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      int af:
    </p>
    <p>
      AF_INET - ipv4
    </p>
    <p>
      AF_INET6 - ipv6
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&#x41f;&#x440;&#x435;&#x43e;&#x431;&#x440;&#x430;&#x437;&#x43e;&#x432;&#x430;&#x43d;&#x438;&#x435; &#x438;&#x43c;&#x435;&#x43d;&#x438; &#x445;&#x43e;&#x441;&#x442;&#x430; &#x432; &#x430;&#x434;&#x440;&#x435;&#x441;" ID="ID_1602896925" CREATED="1419263595124" MODIFIED="1419263615907">
<hook NAME="SummaryNode"/>
</node>
<node TEXT="... *gethostbyaddr(const char *addr, int len, int type);" STYLE_REF="pre_last_node" ID="ID_693375976" CREATED="1419263000153" MODIFIED="1419263118337">
<hook NAME="FirstGroupNode"/>
</node>
<node TEXT="struct servent *getservbyname(const char *name, const char *proto);" STYLE_REF="pre_last_node" ID="ID_1368139842" CREATED="1419263121667" MODIFIED="1419263515766">
<hook NAME="FirstGroupNode"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      IF *proto != NULL -&gt; &#1074;&#1086;&#1079;&#1074;&#1088;&#1072;&#1097;&#1072;&#1077;&#1090; &#1089;&#1077;&#1088;&#1074;&#1080;&#1089; &#1089;&#1086;&#1086;&#1090;&#1074;&#1077;&#1090;&#1089;&#1074;&#1091;&#1102;&#1097;&#1080;&#1081; &#1079;&#1072;&#1076;&#1072;&#1085;&#1086;&#1084;&#1091;
    </p>
    <p>
      &#1080;&#1084;&#1077;&#1085;&#1080; &#1080; &#1090;&#1080;&#1087;&#1091; &#1087;&#1088;&#1086;&#1090;&#1086;&#1082;&#1086;&#1083;&#1072;, &#1080;&#1085;&#1072;&#1095;&#1077; &#1087;&#1077;&#1088;&#1074;&#1099;&#1081; &#1085;&#1072;&#1081;&#1076;&#1077;&#1085;&#1099;&#1081; &#1089;&#1077;&#1088;&#1074;&#1080;&#1089; &#1089; &#1080;&#1084;&#1077;&#1085;&#1077;&#1084; name
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="... getservbyport(int port, const char *proto);" STYLE_REF="pre_last_node" ID="ID_1981124801" CREATED="1419263179531" MODIFIED="1419263247450"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      int port - &#1074; &#1089;&#1077;&#1090;&#1077;&#1074;&#1086;&#1084; &#1087;&#1086;&#1088;&#1103;&#1076;&#1082;&#1077;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&#x41f;&#x440;&#x435;&#x43e;&#x431;&#x440;&#x430;&#x437;&#x43e;&#x432;&#x430;&#x43d;&#x438;&#x435; &#x441;&#x438;&#x43c;&#x432;&#x43e;&#x43b;&#x438;&#x447;&#x435;&#x441;&#x43a;&#x43e;&#x435; &#x438;&#x43c;&#x44f;&#xa;&#x441;&#x435;&#x440;&#x432;&#x438;&#x441;&#x430; &#x432; &#x43d;&#x43e;&#x43c;&#x435;&#x440; &#x43f;&#x43e;&#x440;&#x442;&#x430; &#x438; &#x43d;&#x430;&#x43e;&#x431;&#x43e;&#x440;&#x43e;&#x442;" ID="ID_498871053" CREATED="1419263280730" MODIFIED="1419263314531">
<hook NAME="SummaryNode"/>
</node>
</node>
</node>
<node TEXT="&#x41e;&#x431;&#x43d;&#x443;&#x43b;&#x44f;&#x439;&#x442;&#x435; &#x441;&#x442;&#x440;&#x443;&#x43a;&#x442;&#x443;&#x440;&#x443; sockaddr_in" STYLE_REF="second_node" POSITION="left" ID="ID_584587762" CREATED="1419170713922" MODIFIED="1419263709799">
<edge COLOR="#cc0000"/>
<font SIZE="11" BOLD="true"/>
</node>
<node TEXT="&#x41d;&#x435; &#x432;&#x43e;&#x441;&#x43f;&#x440;&#x438;&#x43d;&#x438;&#x43c;&#x430;&#x439;&#x442;&#x435; &#x43e;&#x447;&#x435;&#x43d;&#x44c;&#xa;&#x441;&#x435;&#x440;&#x44c;&#x435;&#x437;&#x43d;&#x43e; &#x43c;&#x43e;&#x434;&#x435;&#x43b;&#x44c; OSI" STYLE_REF="second_node" POSITION="right" ID="ID_1383767434" CREATED="1419260037008" MODIFIED="1419261186428">
<edge COLOR="#00007c"/>
<font SIZE="11" BOLD="true"/>
<node TEXT="&#x421;&#x43b;&#x43e;&#x436;&#x43d;&#x43e;&#x441;&#x442;&#x44c; &#x438; &#x43d;&#x435;&#x435;&#x444;&#x435;&#x43a;&#x442;&#x438;&#x432;&#x43d;&#x43e;&#x441;&#x442;&#x44c;" STYLE_REF="pre_last_node" ID="ID_1659923109" CREATED="1419260129456" MODIFIED="1419260229284"/>
<node TEXT="&#x41e;&#x43d;&#x430; &#x441;&#x43f;&#x440;&#x43e;&#x435;&#x43a;&#x442;&#x438;&#x440;&#x43e;&#x432;&#x430;&#x43d;&#x43d;&#x430; &#x442;&#x435;&#x43e;&#x440;&#x435;&#x442;&#x438;&#x43a;&#x430;&#x43c;&#x438;" STYLE_REF="pre_last_node" ID="ID_1043854241" CREATED="1419260138232" MODIFIED="1419260229280"/>
<node TEXT="&#x411;&#x43e;&#x43b;&#x435;&#x435; &#x43f;&#x440;&#x438;&#x433;&#x43e;&#x434;&#x43d;&#x430; &#x434;&#x43b;&#x44f; &#x442;&#x435;&#x43b;&#x435;&#x444;&#x43e;&#x43d;&#x43e;&#x432;&#xa;&#x430; &#x43d;&#x435; &#x43a;&#x43e;&#x43c;&#x43f;&#x44e;&#x442;&#x435;&#x440;&#x43d;&#x44b;&#x445; &#x441;&#x435;&#x442;&#x435;&#x439;" STYLE_REF="pre_last_node" ID="ID_1260147845" CREATED="1419260164529" MODIFIED="1419260229276"/>
<node TEXT="&#x420;&#x430;&#x437;&#x43c;&#x44b;&#x442;&#x430;&#x44f; &#x444;&#x443;&#x43d;&#x43a;&#x446;&#x438;&#x43e;&#x43d;&#x430;&#x43b;&#x44c;&#x43d;&#x43e;&#x441;&#x442;&#x44c;" STYLE_REF="pre_last_node" ID="ID_1915560399" CREATED="1419260186488" MODIFIED="1419260229273"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1053;&#1077; &#1087;&#1086;&#1085;&#1103;&#1090;&#1085;&#1086; &#1074; &#1082;&#1072;&#1082;&#1086;&#1084; &#1080;&#1079; &#1091;&#1088;&#1086;&#1074;&#1085;&#1077;&#1081; &#1084;&#1077;&#1089;&#1090;&#1086;
    </p>
    <p>
      &#1086;&#1087;&#1088;&#1077;&#1076;&#1077;&#1083;&#1077;&#1085;&#1085;&#1086;&#1081; &#1092;&#1091;&#1085;&#1082;&#1094;&#1080;&#1080;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&#x41f;&#x440;&#x43e;&#x442;&#x43e;&#x43a;&#x43e;&#x43b; TCP/IP &#x431;&#x43e;&#x43b;&#x435;&#x435; &#x441;&#x43e;&#x432;&#x435;&#x440;&#x448;&#x435;&#x43d;&#x43d;&#x44b;&#x439;" STYLE_REF="pre_last_node" ID="ID_814837346" CREATED="1419260327938" MODIFIED="1419260399883">
<hook URI="through-network.jpg" SIZE="1.0" NAME="ExternalObject"/>
</node>
</node>
<node TEXT="&#x411;&#x443;&#x434;&#x44c;&#x442;&#x435; &#x433;&#x43e;&#x442;&#x43e;&#x432;&#x44b; &#x43a; &#x43d;&#x435;&#x43a;&#x43e;&#x440;&#x435;&#x43a;&#x442;&#x43d;&#x43e;&#x43c;&#x443;&#xa;&#x43f;&#x43e;&#x432;&#x435;&#x434;&#x435;&#x43d;&#x438;&#x44e; &#x43f;&#x430;&#x440;&#x442;&#x43d;&#x435;&#x440;&#x430;" STYLE_REF="second_node" POSITION="right" ID="ID_72230916" CREATED="1419170903690" MODIFIED="1419261186422">
<edge COLOR="#006600"/>
<font SIZE="11" BOLD="true"/>
<node TEXT="&#x41f;&#x440;&#x43e;&#x432;&#x435;&#x440;&#x43a;&#x430; &#x437;&#x430;&#x432;&#x435;&#x440;&#x448;&#x435;&#x43d;&#x438;&#x44f; &#x440;&#x430;&#x431;&#x43e;&#x442;&#x44b; &#x43a;&#x43b;&#x438;&#x435;&#x43d;&#x442;&#x430;" STYLE_REF="pre_last_node" ID="ID_1972531959" CREATED="1419260469995" MODIFIED="1419260595485"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1050;&#1083;&#1080;&#1077;&#1085;&#1090; &#1084;&#1086;&#1078;&#1077;&#1090; &#1074;&#1099;&#1082;&#1083;&#1102;&#1095;&#1080;&#1090;&#1100; &#1082;&#1086;&#1084;&#1087; &#1085;&#1077; &#1079;&#1072;&#1082;&#1088;&#1099;&#1074; &#1087;&#1088;&#1086;&#1075;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&#x41f;&#x440;&#x43e;&#x432;&#x435;&#x440;&#x43a;&#x430; &#x43a;&#x43e;&#x440;&#x440;&#x435;&#x43a;&#x442;&#x43d;&#x43e;&#x441;&#x442;&#x438; &#x432;&#x445;&#x43e;&#x434;&#x43d;&#x43e;&#x439; &#x438;&#x43d;&#x444;&#x43e;&#x440;&#x43c;&#x430;&#x446;&#x438;&#x438;" STYLE_REF="pre_last_node" ID="ID_509610227" CREATED="1419260530108" MODIFIED="1419260555460">
<node TEXT="&#x421;&#x431;&#x438;&#x442;&#x44b;&#x435; &#x443;&#x43a;&#x430;&#x437;&#x430;&#x442;&#x435;&#x43b;&#x438;" STYLE_REF="yellow_node" ID="ID_597020102" CREATED="1419260561892" MODIFIED="1419260600756"/>
<node TEXT="&#x41f;&#x435;&#x440;&#x435;&#x43f;&#x43e;&#x43b;&#x43d;&#x435;&#x43d;&#x438;&#x435; &#x431;&#x443;&#x444;&#x435;&#x440;&#x430;" STYLE_REF="yellow_node" ID="ID_517104377" CREATED="1419260570644" MODIFIED="1419260600760"/>
</node>
</node>
<node TEXT="&#x420;&#x430;&#x437;&#x43b;&#x438;&#x447;&#x430;&#x439;&#x442;&#x435; &#x43f;&#x440;&#x43e;&#x442;&#x43e;&#x43a;&#x43e;&#x43b;&#x44b; &#x442;&#x440;&#x435;&#x431;&#x443;&#x44e;&#x449;&#x438;&#x435;&#xa;&#x438; &#x43d;&#x435; &#x442;&#x440;&#x435;&#x431;&#x443;&#x44e;&#x449;&#x438;&#x435; &#x43b;&#x43e;&#x433;&#x438;&#x447;&#x435;&#x441;&#x43a;&#x43e;&#x433;&#x43e; &#x441;&#x43e;&#x435;&#x434;&#x438;&#x43d;&#x435;&#x43d;&#x438;&#x44f;" STYLE_REF="second_node" POSITION="left" ID="ID_1629258193" CREATED="1419350830015" MODIFIED="1419350860307">
<edge COLOR="#7c007c"/>
<font SIZE="11" BOLD="true"/>
<node TEXT="Connectionless protocols" STYLE_REF="orange_node" ID="ID_310221551" CREATED="1419350873166" MODIFIED="1419351084026">
<hook URI="../../Images/letter.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="UPD" STYLE_REF="yellow_node" ID="ID_430450777" CREATED="1419350908576" MODIFIED="1419351238019"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      User datagram protocol
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true"/>
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="140" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_1949572918" MIDDLE_LABEL="&#x422;&#x435;&#x43e;&#x440;&#x435;&#x442;&#x438;&#x447;&#x435;&#x441;&#x43a;&#x438; &#x431;&#x44b;&#x441;&#x442;&#x440;&#x435;&#x435; &#x447;&#x435;&#x43c; TCP.&#xa;&#x41d;&#x43e; &#x43d;&#x430; &#x43f;&#x440;&#x430;&#x43a;&#x442;&#x438;&#x43a;&#x435; &#x43d;&#x435; &#x432;&#x441;&#x435;&#x433;&#x434;&#x430;" STARTINCLINATION="4;66;" ENDINCLINATION="47;-55;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
<node TEXT="&#x41d;&#x435; &#x43d;&#x430;&#x434;&#x435;&#x436;&#x43d;&#x43e;&#x441;&#x442;&#x44c;" STYLE_REF="pre_last_node" ID="ID_297200736" CREATED="1419350937968" MODIFIED="1419350976983"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      +2 &#1082; IP
    </p>
  </body>
</html>
</richcontent>
<node TEXT="&#x41a;&#x43e;&#x43d;&#x442;&#x440;&#x43e;&#x43b;&#x44c;&#x43d;&#x430;&#x44f; &#x441;&#x443;&#x43c;&#x43c;&#x430; &#x434;&#x430;&#x43d;&#x43d;&#x44b;&#x445;" ID="ID_1545044625" CREATED="1419350949079" MODIFIED="1419351065486" BACKGROUND_COLOR="#ccccff"/>
<node TEXT="&#x41f;&#x43e;&#x43d;&#x44f;&#x442;&#x438;&#x435; &#x43f;&#x43e;&#x440;&#x442;&#x430;" ID="ID_1491587711" CREATED="1419350953743" MODIFIED="1419351065488" BACKGROUND_COLOR="#ccccff"/>
</node>
<node TEXT="&#x41f;&#x43e;&#x442;&#x435;&#x440;&#x438; &#x434;&#x430;&#x43d;&#x43d;&#x44b;&#x445; &#x434;&#x430;&#x436;&#x435; &#x43d;&#x430; localhost" STYLE_REF="pre_last_node" ID="ID_1328537358" CREATED="1419350942072" MODIFIED="1419350976987"/>
</node>
</node>
<node TEXT="Connection-oriented protocols" STYLE_REF="orange_node" ID="ID_13117284" CREATED="1419350875687" MODIFIED="1419351092553"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      1. &#1059;&#1089;&#1090;&#1072;&#1085;&#1086;&#1074;&#1082;&#1072; &#1089;&#1086;&#1077;&#1076;&#1080;&#1085;&#1077;&#1085;&#1080;&#1103;
    </p>
    <p>
      2. &#1054;&#1073;&#1084;&#1077;&#1085; &#1076;&#1072;&#1085;&#1085;&#1099;&#1084;&#1080;
    </p>
    <p>
      3. &#1056;&#1072;&#1079;&#1088;&#1099;&#1074; &#1089;&#1086;&#1077;&#1076;&#1080;&#1085;&#1077;&#1085;&#1080;&#1103;
    </p>
  </body>
</html>
</richcontent>
<hook URI="../../Images/phone.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="TCP" STYLE_REF="yellow_node" ID="ID_1949572918" CREATED="1419350913704" MODIFIED="1419350968991"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Transmission control protocol
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true"/>
<node TEXT="&#x41d;&#x430;&#x434;&#x435;&#x436;&#x43d;&#x43e;&#x441;&#x442;&#x44c;" STYLE_REF="pre_last_node" ID="ID_1859055151" CREATED="1419350985696" MODIFIED="1419351167779">
<hook URI="../../Images/shield-small.png" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x412; &#x43a;&#x430;&#x436;&#x434;&#x44b;&#x439; &#x441;&#x435;&#x433;&#x43c;&#x435;&#x43d;&#x442; &#x432;&#x43a;&#x43b;&#x44e;&#x447;&#x435;&#x43d;&#x430;&#xa;&#x43a;&#x43e;&#x43d;&#x442;&#x440;&#x43e;&#x43b;&#x44c;&#x43d;&#x430;&#x44f; &#x441;&#x443;&#x43c;&#x43c;&#x430; &#x435;&#x433;&#x43e; &#x434;&#x430;&#x43d;&#x43d;&#x44b;&#x445;" STYLE_REF="yellow_node" ID="ID_277199877" CREATED="1419351025833" MODIFIED="1419351058325" BACKGROUND_COLOR="#ccccff"/>
<node TEXT="&#x412; &#x437;&#x430;&#x433;&#x43e;&#x43b;&#x43e;&#x432;&#x43a;&#x435; &#x43a;&#x430;&#x436;&#x434;&#x43e;&#x433;&#x43e; &#x441;&#x435;&#x433;&#x43c;&#x435;&#x43d;&#x442;&#x430;&#xa;&#x43f;&#x435;&#x440;&#x435;&#x434;&#x430;&#x435;&#x442;&#x441;&#x44f; &#x43d;&#x43e;&#x43c;&#x435;&#x440; &#x43f;&#x435;&#x440;&#x432;&#x43e;&#x433;&#x43e; &#x431;&#x430;&#x439;&#x442;&#x430;" STYLE_REF="yellow_node" ID="ID_477292809" CREATED="1419351031336" MODIFIED="1419351058331" BACKGROUND_COLOR="#ccccff"/>
<node TEXT="&#x41c;&#x435;&#x445;&#x430;&#x43d;&#x438;&#x437;&#x43c; &#x43f;&#x43e;&#x434;&#x442;&#x432;&#x435;&#x440;&#x436;&#x434;&#x435;&#x43d;&#x438;&#x44f; &#x438; &#x43a;&#x43e;&#x43d;&#x442;&#x440;&#x43e;&#x43b;&#x44c;&#x43d;&#x43e;&#x439; &#x43f;&#x435;&#x440;&#x435;&#x434;&#x430;&#x447;&#x438;" STYLE_REF="yellow_node" ID="ID_824382736" CREATED="1419351036025" MODIFIED="1419351129977" BACKGROUND_COLOR="#ccccff"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1043;&#1072;&#1088;&#1072;&#1085;&#1090;&#1080;&#1088;&#1091;&#1077;&#1090; &#1095;&#1090;&#1086; &#1076;&#1072;&#1085;&#1085;&#1099;&#1077; &#1082;&#1086;&#1075;&#1076;&#1072;-&#1085;&#1080;&#1073;&#1091;&#1076;&#1100;
    </p>
    <p>
      &#1073;&#1091;&#1076;&#1091;&#1090; &#1076;&#1086;&#1089;&#1090;&#1072;&#1074;&#1083;&#1077;&#1085;&#1099;
    </p>
  </body>
</html>
</richcontent>
<hook NAME="FirstGroupNode"/>
<node TEXT="&#x41e;&#x43a;&#x43d;&#x43e; &#x43f;&#x440;&#x438;&#x435;&#x43c;&#x430;" ID="ID_1517912562" CREATED="1419351102305" MODIFIED="1419351257111" BACKGROUND_COLOR="#ccffff"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1050;&#1083;&#1080;&#1077;&#1085;&#1090;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&#x41e;&#x43a;&#x43d;&#x43e; &#x43f;&#x435;&#x440;&#x435;&#x434;&#x430;&#x447;&#x438;" ID="ID_232870739" CREATED="1419351120285" MODIFIED="1419351257113" BACKGROUND_COLOR="#ccffff"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1057;&#1077;&#1088;&#1074;&#1077;&#1088;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="" ID="ID_769693331" CREATED="1419351129975" MODIFIED="1419351167777" HGAP="30" VSHIFT="170">
<hook NAME="SummaryNode"/>
<hook URI="windows.jpg" SIZE="1.0" NAME="ExternalObject"/>
</node>
</node>
</node>
</node>
</node>
<node TEXT="&#x420;&#x430;&#x437;&#x431;&#x435;&#x440;&#x438;&#x442;&#x435;&#x441;&#x44c; &#x441; &#x430;&#x43a;&#x443;&#x440;&#x430;&#x442;&#x43d;&#x44b;&#x43c;&#xa;&#x440;&#x430;&#x437;&#x43c;&#x44b;&#x43a;&#x430;&#x43d;&#x438;&#x435;&#x43c; &#x422;&#x421;P-&#x441;&#x43e;&#x435;&#x434;&#x438;&#x43d;&#x435;&#x43d;&#x438;&#x44f;" STYLE_REF="second_node" POSITION="left" ID="ID_1022729914" CREATED="1425018671671" MODIFIED="1425030690119">
<edge COLOR="#007c7c"/>
<font SIZE="11" BOLD="true"/>
<hook URI="../../Images/tidy.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="TCP-&#x441;&#x43e;&#x435;&#x434;&#x438;&#x43d;&#x435;&#x43d;&#x438;&#x44f; &#x43f;&#x43e;&#x43b;&#x43d;&#x43e;&#x434;&#x443;&#x43f;&#x43b;&#x435;&#x43a;&#x441;&#x43d;&#x44b;&#x435;" STYLE_REF="orange_node" ID="ID_619124232" CREATED="1425018808030" MODIFIED="1425019879974" VSHIFT="-10"/>
<node TEXT="int shutdown(int s, int how);" STYLE_REF="orange_node" ID="ID_446855687" CREATED="1425018840944" MODIFIED="1425019879977">
<node TEXT="int how" STYLE_REF="pre_last_node" ID="ID_1613754840" CREATED="1425019133112" MODIFIED="1425019982929" VGAP="13">
<node TEXT="0 / POSIX = SHUT_RD / WINSOCK = SD_RECEIVE" STYLE_REF="yellow_node" ID="ID_181952824" CREATED="1425019140808" MODIFIED="1425019982927">
<node TEXT="&#x417;&#x430;&#x43a;&#x440;&#x44b;&#x432;&#x430;&#x435;&#x442; &#x43f;&#x440;&#x438;&#x43d;&#x438;&#x43c;&#x430;&#x44e;&#x449;&#x430;&#x44f; &#x441;&#x442;&#x43e;&#x440;&#x43e;&#x43d;&#x430; &#x441;&#x43e;&#x435;&#x434;&#x438;&#x43d;&#x435;&#x43d;&#x438;&#x44f;" STYLE_REF="pre_last_node" ID="ID_1834058424" CREATED="1425019178856" MODIFIED="1425019953823"/>
<node TEXT="&#x412;&#x435;&#x440;&#x442;&#x430;&#x435;&#x442; EOF &#x435;&#x441;&#x43b;&#x438; &#x43f;&#x440;&#x43e;&#x433;&#x440;&#x430;&#x43c;&#x43c;&#x430; &#x434;&#x435;&#x43b;&#x430;&#x435;&#x442;&#xa;&#x43f;&#x43e;&#x43f;&#x44b;&#x442;&#x43a;&#x443; &#x447;&#x442;&#x435;&#x43d;&#x438;&#x44f;" STYLE_REF="pre_last_node" ID="ID_1234065294" CREATED="1425019196312" MODIFIED="1425019994243"/>
<node TEXT="&#x412; UNIX &#x43f;&#x440;&#x438;&#x43d;&#x44f;&#x442;&#x44b;&#x435; &#x43d;&#x43e; &#x43d;&#x435; &#x43f;&#x440;&#x43e;&#x447;&#x438;&#x442;&#x430;&#x43d;&#x43d;&#x44b;&#x435; &#x434;&#x430;&#x43d;&#x43d;&#x44b;&#x435; &#x443;&#x43d;&#x438;&#x447;&#x442;&#x43e;&#x436;&#x430;&#x44e;&#x442;&#x441;&#x44f;" STYLE_REF="pre_last_node" ID="ID_470588540" CREATED="1425019246056" MODIFIED="1425019953812"/>
<node TEXT="&#x41c;&#x43e;&#x436;&#x43d;&#x43e; &#x43f;&#x440;&#x438;&#x43c;&#x435;&#x43d;&#x438;&#x442;&#x44c; &#x43a; UDP &#x441;&#x43e;&#x435;&#x434;&#x438;&#x43d;&#x435;&#x43d;&#x438;&#x44e;&#xa;&#x434;&#x43b;&#x44f; &#x437;&#x430;&#x43f;&#x440;&#x435;&#x442;&#x430; &#x43f;&#x440;&#x438;&#x435;&#x43c;&#x430; &#x434;&#x430;&#x442;&#x430;&#x433;&#x440;&#x430;&#x43c;" STYLE_REF="pre_last_node" ID="ID_968189869" CREATED="1425019622160" MODIFIED="1425019953808"/>
</node>
<node TEXT="1 / POSIX = SHUT_WR / WIN = SD_SEND" STYLE_REF="yellow_node" ID="ID_826358362" CREATED="1425019657129" MODIFIED="1425019890504">
<node TEXT="&#x417;&#x430;&#x43a;&#x440;&#x44b;&#x432;&#x430;&#x435;&#x442; &#x43e;&#x442;&#x43f;&#x440;&#x430;&#x432;&#x43b;&#x44f;&#x44e;&#x449;&#x430;&#x44f; &#x441;&#x442;&#x43e;&#x440;&#x43e;&#x43d;&#x430; &#x441;&#x43e;&#x435;&#x434;&#x438;&#x43d;&#x435;&#x43d;&#x438;&#x44f;" STYLE_REF="pre_last_node" ID="ID_774665123" CREATED="1425019678192" MODIFIED="1425019953807"/>
<node TEXT="&#x41f;&#x43e;&#x441;&#x43b;&#x435; &#x44d;&#x442;&#x43e;&#x433;&#x43e; &#x43f;&#x43e;&#x43f;&#x44b;&#x442;&#x43a;&#x430; &#x432;&#x44b;&#x43f;&#x43e;&#x43b;&#x43d;&#x438;&#x442;&#x44c; &#x437;&#x430;&#x43f;&#x438;&#x441;&#x44c; &#x434;&#x430;&#x435;&#x442; &#x43e;&#x448;&#x438;&#x431;&#x43a;&#x443;" STYLE_REF="pre_last_node" ID="ID_1779618382" CREATED="1425019746080" MODIFIED="1425019953806"/>
<node TEXT="&#x41f;&#x43e;&#x441;&#x43b;&#x435; &#x442;&#x43e;&#x433;&#x43e; &#x43a;&#x430;&#x43a; &#x432;&#x441;&#x44f; &#x438;&#x43d;&#x444;&#x430; &#x437; &#x431;&#x443;&#x444;&#x435;&#x440;&#x430; &#x43e;&#x442;&#x43f;&#x440;&#x430;&#x432;&#x43b;&#x435;&#x43d;&#x430; &#x43f;&#x43e;&#x441;&#x438;&#x43b;&#x430;&#x442;&#x441;&#x44f; FIN" STYLE_REF="pre_last_node" ID="ID_1535227685" CREATED="1425019806273" MODIFIED="1425019953805"/>
</node>
<node TEXT="2 / POSIX = SHUT_RDWR / WIN = SD_BOTH" STYLE_REF="yellow_node" ID="ID_993529953" CREATED="1425019844905" MODIFIED="1425019890506">
<node TEXT="&#x417;&#x430;&#x43a;&#x440;&#x44b;&#x432;&#x430;&#x435;&#x442; &#x43e;&#x431;&#x435; &#x441;&#x442;&#x43e;&#x440;&#x43e;&#x43d;&#x44b; &#x441;&#x43e;&#x435;&#x434;&#x43d;&#x435;&#x43d;&#x438;&#x44f;" STYLE_REF="pre_last_node" ID="ID_245067635" CREATED="1425019914464" MODIFIED="1425019953804"/>
<node TEXT="&#x422;&#x43e;&#x436;&#x435; &#x447;&#x442;&#x43e; &#x432;&#x44b;&#x437;&#x432;&#x430;&#x442;&#x44c; &#x434;&#x432;&#x430; &#x440;&#x430;&#x437;&#x430; &#x441; &#x43f;&#x430;&#x440;&#x430;&#x43c;&#x435;&#x442;&#x440;&#x430;&#x43c;&#x438; 0 &#x438; 1" STYLE_REF="pre_last_node" ID="ID_664728311" CREATED="1425019929920" MODIFIED="1425019953802"/>
</node>
</node>
<node TEXT="return;" STYLE_REF="pre_last_node" ID="ID_1175844822" CREATED="1425020052680" MODIFIED="1425020091358">
<node TEXT="0" OBJECT="java.lang.Long|0" ID="ID_1676491914" CREATED="1425020059553" MODIFIED="1425020085962"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1054;&#1082;
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="-1" OBJECT="java.lang.Long|-1" ID="ID_1312765024" CREATED="1425020062281" MODIFIED="1425020078976"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1054;&#1096;&#1080;&#1073;&#1082;&#1072;
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
</node>
<node TEXT="shtdown != close" STYLE_REF="orange_node" ID="ID_1489459457" CREATED="1425018896136" MODIFIED="1425020348888">
<node TEXT="shutdown" STYLE_REF="yellow_node" ID="ID_1436950778" CREATED="1425020161889" MODIFIED="1425020348886">
<node TEXT="&#x41d;&#x435; &#x437;&#x430;&#x43a;&#x440;&#x44b;&#x432;&#x430;&#x435;&#x442; &#x441;&#x43e;&#x43a;&#x435;&#x442; &#x43f; &#x43e;&#x43d;&#x430;&#x441;&#x442;&#x43e;&#x44f;&#x449;&#x435;&#x43c;&#x443;&#xa;&#x440;&#x435;&#x441;&#x443;&#x440;&#x441;&#x44b; &#x43d;&#x435; &#x43e;&#x441;&#x432;&#x43e;&#x431;&#x43e;&#x436;&#x434;&#x430;&#x44e;&#x442;&#x441;&#x44f;" STYLE_REF="pre_last_node" ID="ID_1261385308" CREATED="1425020174176" MODIFIED="1425020329774"/>
<node TEXT="&#x420;&#x430;&#x441;&#x43f;&#x43e;&#x441;&#x442;&#x440;&#x430;&#x43d;&#x44f;&#x435;&#x442;&#x441;&#x44f; &#x43d;&#x430; &#x432;&#x441;&#x435; &#x43f;&#x440;&#x43e;&#x446;&#x435;&#x441;&#x44b; &#x43a;&#x43e;&#x442;&#x43e;&#x440;&#x44b;&#x435; &#x435;&#x433;&#x43e; &#x43e;&#x442;&#x43a;&#x440;&#x44b;&#x43b;&#x438;" STYLE_REF="pre_last_node" ID="ID_1821940847" CREATED="1425020249385" MODIFIED="1425020329787"/>
<node TEXT="&#x41f;&#x440;&#x438; how=1, &#x43f;&#x43e;&#x448;&#x43b;&#x435;&#x442; EOF &#x434;&#x430;&#x436;&#x435; &#x435;&#x441;&#x43b;&#x438;&#xa;&#x441;&#x43e;&#x43a; &#x43e;&#x442;&#x43a;&#x440;&#x44b;&#x442; &#x434;&#x440;&#x443;&#x433;&#x438;&#x43c;&#x438; &#x43f;&#x440;&#x43e;&#x446;&#x435;&#x441;&#x430;&#x43c;&#x438;" STYLE_REF="pre_last_node" ID="ID_165059679" CREATED="1425020270264" MODIFIED="1425020329790"/>
</node>
<node TEXT="close" STYLE_REF="yellow_node" ID="ID_1005088513" CREATED="1425020350897" MODIFIED="1425020466119">
<node TEXT="&#x417;&#x430;&#x43a;&#x440;&#x44b;&#x432;&#x430;&#x435;&#x442; &#x441;&#x43e;&#x43a;&#x435;&#x442; &#x442;&#x43e;&#x43b;&#x44c;&#x43a;&#x43e; &#x432; &#x434;&#x430;&#x43d;&#x43d;&#x43e;&#x43c; &#x43f;&#x440;&#x43e;&#x446;&#x435;&#x441;&#x441;&#x435;,&#xa;&#x432;&#x441;&#x435; &#x43e;&#x441;&#x442;&#x430;&#x43b;&#x44c;&#x43d;&#x44b;&#x435; &#x43c;&#x43e;&#x433;&#x443;&#x442; &#x43f;&#x440;&#x43e;&#x434;&#x43e;&#x43b;&#x436;&#x430;&#x442;&#x44c; &#x438;&#x441;&#x43f;&#x43e;&#x43b;&#x44c;&#x437;&#x43e;&#x432;&#x430;&#x442;&#x44c;" STYLE_REF="pre_last_node" ID="ID_1414177770" CREATED="1425020355392" MODIFIED="1425020481301"/>
<node TEXT="TCP &#x43d;&#x435; &#x43f;&#x43e;&#x448;&#x43b;&#x435;&#x442; FIN &#x43f;&#x43e;&#x43a;&#x430; &#x441;&#x43e;&#x43a;&#x435;&#x442; &#x438;&#x441;&#x43f;&#x43e;&#x43b;&#x44c;&#x437;&#x443;&#x44e;&#x442;&#xa;&#x434;&#x440;&#x443;&#x433;&#x438;&#x435; &#x43f;&#x440;&#x43e;&#x446;&#x435;&#x441;&#x441;&#x44b;" STYLE_REF="pre_last_node" ID="ID_608155907" CREATED="1425020397528" MODIFIED="1425020481312"/>
</node>
<node TEXT="" ID="ID_713368088" CREATED="1425022943802" MODIFIED="1425022948783">
<hook URI="shutdown-close.jpg" SIZE="0.85227275" NAME="ExternalObject"/>
</node>
</node>
</node>
<node TEXT="&#x427;&#x442;&#x43e;&#x431;&#x44b; &#x445;&#x43e;&#x440;&#x43e;&#x448;&#x43e; &#x438;&#x437;&#x432;&#x435;&#x441;&#x442;&#x43d;&#x44b;&#x435; &#x2116; &#x43f;&#x43e;&#x440;&#x442;&#x43e;&#x432;&#xa;&#x43d;&#x430;&#x437;&#x43d;&#x430;&#x447;&#x430;&#x43b;&#x438;&#x441;&#x44c; &#x441; &#x43f;&#x43e;&#x43c;&#x43e;&#x449;&#x44c;&#x44e; tcpmux" STYLE_REF="second_node" POSITION="right" ID="ID_1254006649" CREATED="1425020574504" MODIFIED="1425020649229">
<edge COLOR="#7c7c00"/>
<font SIZE="11" BOLD="true"/>
<node TEXT="IANA" STYLE_REF="orange_node" ID="ID_681655063" CREATED="1425020678152" MODIFIED="1425021023958">
<node TEXT="&#x41e;&#x440;&#x433;&#x430;&#x43d;&#x438;&#x437;&#x430;&#x446;&#x438;&#x44f; &#x443;&#x442;&#x432;&#x435;&#x440;&#x436;&#x434;&#x430;&#x435;&#x442; &#x43e;&#x431;&#x449;&#x435;&#x43f;&#x440;&#x438;&#x43d;&#x44f;&#x442;&#x44b;&#x435; &#x2116; &#x43f;&#x43e;&#x440;&#x442;&#x43e;&#x432;" STYLE_REF="pre_last_node" ID="ID_704020473" CREATED="1425021007304" MODIFIED="1425021028365"/>
</node>
<node TEXT="&#x41f;&#x43e;&#x440;&#x442;&#x44b;" STYLE_REF="orange_node" ID="ID_444078221" CREATED="1425020755465" MODIFIED="1425020985592">
<node TEXT="0 - 1023" STYLE_REF="yellow_node" ID="ID_1202930995" CREATED="1425020763601" MODIFIED="1425020979877">
<node TEXT="&#x41e;&#x444;&#x438;&#x446;&#x438;&#x430;&#x43b;&#x44c;&#x43d;&#x44b;&#x435;" STYLE_REF="pre_last_node" ID="ID_1505434320" CREATED="1425020774721" MODIFIED="1425020994497"/>
<node TEXT="&#x425;&#x43e;&#x440;&#x43e;&#x448;&#x43e; &#x438;&#x437;&#x432;&#x435;&#x441;&#x442;&#x43d;&#x44b;&#x435;" STYLE_REF="pre_last_node" ID="ID_1729098991" CREATED="1425020791881" MODIFIED="1425020994500"/>
</node>
<node TEXT="1024 - 49151" STYLE_REF="yellow_node" ID="ID_1258972181" CREATED="1425020799017" MODIFIED="1425020979879">
<node TEXT="&#x417;&#x430;&#x440;&#x435;&#x433;&#x435;&#x441;&#x442;&#x440;&#x438;&#x440;&#x43e;&#x432;&#x430;&#x43d;&#x43d;&#x44b;&#x435;" STYLE_REF="pre_last_node" ID="ID_218877324" CREATED="1425020810152" MODIFIED="1425020994501"/>
</node>
<node TEXT="49152 - 65635" STYLE_REF="yellow_node" ID="ID_238101660" CREATED="1425020819697" MODIFIED="1425020979880">
<node TEXT="&#x414;&#x438;&#x43d;&#x430;&#x43c;&#x438;&#x447;&#x435;&#x441;&#x43a;&#x438;&#x435;" STYLE_REF="pre_last_node" ID="ID_423131989" CREATED="1425020952560" MODIFIED="1425020994502"/>
<node TEXT="&#x427;&#x430;&#x441;&#x442;&#x43d;&#x44b;&#x435;" STYLE_REF="pre_last_node" ID="ID_327910302" CREATED="1425020959552" MODIFIED="1425020994503"/>
</node>
</node>
</node>
<node TEXT="&#x421;&#x435;&#x440;&#x432;&#x435;&#x440; &#x434;&#x43e;&#x43b;&#x436;&#x435;&#x43d; &#x443;&#x441;&#x442;&#x430;&#x43d;&#x430;&#x432;&#x43b;&#x438;&#x432;&#x430;&#x442;&#x44c;&#xa;&#x43e;&#x43f;&#x446;&#x438;&#x44e; SO_REUSEADDR" STYLE_REF="second_node" POSITION="left" ID="ID_1529374144" CREATED="1425021058017" MODIFIED="1425021119053">
<edge COLOR="#ff0000"/>
<font SIZE="11" BOLD="true"/>
<node TEXT="&#x420;&#x430;&#x437;&#x440;&#x435;&#x448;&#x430;&#x435;&#x442; &#x43f;&#x440;&#x438;&#x432;&#x44f;&#x437;&#x43a;&#x443; &#x43a; &#x443;&#x436;&#x435; &#x438;&#x441;&#x43f;&#x43e;&#x43b;&#x44c;&#x437;&#x443;&#x435;&#x43c;&#x43e;&#x43c;&#x443; &#x43f;&#x43e;&#x440;&#x442;&#x443;" STYLE_REF="pre_last_node" ID="ID_502177478" CREATED="1425021145121" MODIFIED="1425021241281"/>
<node TEXT="&#x41f;&#x43e;&#x437;&#x432;&#x43e;&#x43b;&#x44f;&#x435;&#x442; &#x43f;&#x435;&#x440;&#x435;&#x437;&#x430;&#x43f;&#x443;&#x441;&#x442;&#x438;&#x442;&#x44c; &#x441;&#x435;&#x440;&#x432;&#x435;&#x440;&#xa;&#x441;&#x440;&#x430;&#x437;&#x443; &#x43f;&#x43e;&#x441;&#x43b;&#x435; &#x435;&#x433;&#x43e; &#x437;&#x430;&#x432;&#x435;&#x440;&#x448;&#x435;&#x43d;&#x438;&#x44f;" STYLE_REF="pre_last_node" ID="ID_1720346042" CREATED="1425021178417" MODIFIED="1425021241287"/>
<node TEXT="&#x41f;&#x440;&#x438;&#x447;&#x438;&#x43d;&#x430; &#x432; TIME-WAIT &#x430;&#x43a;&#x442;&#x438;&#x432;&#x43d;&#x43e; &#x437;&#x430;&#x43a;&#x440;&#x44b;&#x432;&#x430;&#x44e;&#x449;&#x435;&#x439; &#x441;&#x442;&#x43e;&#x440;&#x43e;&#x43d;&#x44b;" STYLE_REF="pre_last_node" ID="ID_1078422953" CREATED="1425021202441" MODIFIED="1425021241289"/>
</node>
<node TEXT="&#x41d;&#x435; &#x43f;&#x440;&#x438;&#x440;&#x44b;&#x432;&#x430;&#x435;&#x442;&#x435; &#x441;&#x43e;&#x441;&#x442;&#x43e;&#x44f;&#x43d;&#x438;&#x435; TIME-WAIT&#xa;&#x434;&#x43b;&#x44f; &#x437;&#x430;&#x43a;&#x440;&#x44b;&#x442;&#x43e;&#x433;&#x43e; &#x441;&#x43e;&#x435;&#x434;&#x438;&#x43d;&#x435;&#x43d;&#x438;&#x44f;" STYLE_REF="second_node" POSITION="right" ID="ID_689582856" CREATED="1425021269432" MODIFIED="1425021358077">
<edge COLOR="#0000ff"/>
<font SIZE="11" BOLD="true"/>
<node TEXT="&#x417;&#x430;&#x447;&#x435;&#x43c; &#x43d;&#x443;&#x436;&#x43d;&#x43e;" STYLE_REF="yellow_node" ID="ID_642287420" CREATED="1425021376921" MODIFIED="1425022814582">
<node TEXT="&#x41d;&#x435; &#x434;&#x430;&#x442;&#x44c; &#x43f;&#x440;&#x43e;&#x43f;&#x430;&#x441;&#x442;&#x44c; &#x43f;&#x43e;&#x441;&#x43b;&#x435;&#x434;&#x43d;&#x435;&#x43c;&#x443; &#x410;&#x421;&#x41a;" STYLE_REF="pre_last_node" ID="ID_640968054" CREATED="1425021521441" MODIFIED="1425022616912"/>
<node TEXT="&#x414;&#x430;&#x442;&#x44c; &#x432;&#x440;&#x435;&#x43c;&#x44f; &#x438;&#x441;&#x447;&#x435;&#x437;&#x43d;&#x443;&#x442;&#x44c; &#x437;&#x430;&#x431;&#x43b;&#x443;&#x434;&#x438;&#x432;&#x448;&#x438;&#x43c;&#x441;&#x44f; &#x441;&#x435;&#x433;&#x43c;&#x435;&#x43d;&#x442;&#x430;&#x43c;" STYLE_REF="pre_last_node" ID="ID_1630073538" CREATED="1425021543209" MODIFIED="1425022616911"/>
<node TEXT="" ID="ID_484231476" CREATED="1425022810882" MODIFIED="1425022818618">
<hook URI="time-wait.jpg" SIZE="1.0" NAME="ExternalObject"/>
</node>
</node>
<node TEXT="&#x41c;&#x43e;&#x436;&#x435;&#x442; &#x431;&#x44b;&#x442;&#x44c; &#x43e;&#x442;&#x43c;&#x435;&#x43d;&#x435;&#x43d;&#x43e;" STYLE_REF="yellow_node" ID="ID_1814428443" CREATED="1425021366905" MODIFIED="1425021401856">
<node TEXT="&#x421;&#x43b;&#x443;&#x447;&#x430;&#x439;&#x43d;&#x43e;" STYLE_REF="pre_last_node" ID="ID_1929822210" CREATED="1425021448065" MODIFIED="1425021515450">
<node TEXT="&#x41f;&#x440;&#x438; &#x43f;&#x43e;&#x43b;&#x443;&#x447;&#x435;&#x43d;&#x438;&#x438; RST &#x432;&#x43e; &#x432;&#x440;&#x435;&#x43c;&#x44f; TIME-WAIT" ID="ID_1839233631" CREATED="1425021454937" MODIFIED="1425021482855"/>
</node>
<node TEXT="&#x41d;&#x430;&#x43c;&#x435;&#x440;&#x435;&#x43d;&#x43d;&#x43e;" STYLE_REF="pre_last_node" ID="ID_1076384420" CREATED="1425021486729" MODIFIED="1425021515448">
<node TEXT="&#x41e;&#x43f;&#x446;&#x438;&#x44f; SO_LINGER" ID="ID_128238083" CREATED="1425021493257" MODIFIED="1425021507030"/>
</node>
</node>
<node TEXT="&#x41e;&#x441;&#x43e;&#x431;&#x435;&#x43d;&#x43d;&#x43e;&#x441;&#x442;&#x438;" STYLE_REF="yellow_node" ID="ID_989513999" CREATED="1425021387338" MODIFIED="1425021401861">
<node TEXT="&#x412; &#x441;&#x43e;&#x441;&#x442;&#x43e;&#x44f;&#x43d;&#x438;&#x435; TIME-WAIT &#x43f;&#x435;&#x440;&#x435;&#x445;&#x43e;&#x434;&#x438;&#x442;&#x44c; &#x442;&#x43e;&#x43b;&#x44c;&#x43a;&#x43e; &#x43e;&#x434;&#x43d;&#x430; &#x441;&#x442;&#x43e;&#x440;&#x43e;&#x43d;&#x430;" STYLE_REF="pre_last_node" ID="ID_547474466" CREATED="1425021570394" MODIFIED="1425022616911">
<node TEXT="&#x412;&#x44b;&#x43f;&#x43e;&#x43b;&#x43d;&#x44f;&#x44e;&#x449;&#x430;&#x44f; &#x430;&#x43a;&#x442;&#x438;&#x432;&#x43d;&#x43e;&#x435; &#x437;&#x430;&#x43a;&#x440;&#x44b;&#x442;&#x438;&#x435;" ID="ID_224903686" CREATED="1425021606481" MODIFIED="1425022656469"/>
</node>
<node TEXT="&#x415;&#x441;&#x43b;&#x438; &#x432; &#x441;&#x43e;&#x441;&#x442;&#x43e;&#x44f;&#x43d;&#x438;&#x438; TIME-WAIT &#x43f;&#x440;&#x438;&#x445;&#x43e;&#x434;&#x438;&#x442; &#x43d;&#x43e;&#x432;&#x44b;&#x439; &#x43f;&#x430;&#x43a;&#x435;&#x442;" STYLE_REF="pre_last_node" ID="ID_1275237763" CREATED="1425021613186" MODIFIED="1425022616911">
<node TEXT="&#x422;&#x430;&#x439;&#x43c;&#x435;&#x440; &#x43d;&#x430; 2 MSL &#x43f;&#x435;&#x440;&#x435;&#x437;&#x430;&#x43f;&#x443;&#x441;&#x43a;&#x430;&#x435;&#x442; TIME-WAIT" ID="ID_1152151975" CREATED="1425021651530" MODIFIED="1425022680133"/>
</node>
<node TEXT="TIME-WAIT == 2 MSL" STYLE_REF="pre_last_node" ID="ID_1779545161" CREATED="1425021670137" MODIFIED="1425022616910"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Maximum segment lifetime
    </p>
  </body>
</html>

</richcontent>
<node TEXT="&#x412; BSD" STYLE_REF="yellow_node" ID="ID_603567959" CREATED="1425021705713" MODIFIED="1425022693503">
<node TEXT="== 1 &#x43c;&#x438;&#x43d;" ID="ID_1905257297" CREATED="1425021726585" MODIFIED="1425021753629"/>
</node>
<node TEXT="&#x41f;&#x43e; RFC" STYLE_REF="yellow_node" ID="ID_148628952" CREATED="1425021720777" MODIFIED="1425022693502">
<node TEXT="== 4 &#x43c;&#x438;&#x43d;" ID="ID_247234313" CREATED="1425021759793" MODIFIED="1425022606453"/>
</node>
</node>
</node>
</node>
<node TEXT="&#x417;&#x430;&#x43f;&#x438;&#x441;&#x438;&#x432;&#x430;&#x439;&#x442;&#x435; 1 &#x431;&#x43e;&#x43b;&#x44c;&#x448;&#x43e;&#x439; &#x431;&#x43b;&#x43e;&#x43a;&#xa;&#x432;&#x43c;&#x435;&#x441;&#x442;&#x43e; &#x43d;&#x435;&#x441;&#x43a;&#x43e;&#x43b;&#x44c;&#x43a;&#x438;&#x445; &#x43c;&#x430;&#x43b;&#x435;&#x43d;&#x44c;&#x43a;&#x438;&#x445;" STYLE_REF="second_node" POSITION="left" ID="ID_792410" CREATED="1425023006106" MODIFIED="1425030527427">
<edge COLOR="#027b02"/>
<font SIZE="11" BOLD="true"/>
<hook URI="../../Images/elephant.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x41c;&#x435;&#x445;&#x430;&#x43d;&#x438;&#x437;&#x43c; &#x43e;&#x442;&#x43b;&#x43e;&#x436;&#x435;&#x43d;&#x44b;&#x445; &#x43f;&#x43e;&#x434;&#x442;&#x432;&#x435;&#x440;&#x436;&#x434;&#x435;&#x43d;&#x438;&#x439;" STYLE_REF="orange_node" ID="ID_1974178666" CREATED="1425023539034" MODIFIED="1425023624911">
<node TEXT="&#x41f;&#x440;&#x438; &#x43f;&#x43e;&#x43b;&#x443;&#x447;&#x435;&#x43d;&#x438;&#x438; &#x43f;&#x430;&#x43a;&#x435;&#x442;&#x430;, TCP &#x437;&#x430;&#x434;&#x435;&#x440;&#x436;&#x438;&#x432;&#x430;&#x435;&#x442; &#x43e;&#x442;&#x43f;&#x440;&#x430;&#x432;&#x43a;&#x443;&#xa;&#x410;&#x421;&#x41a; &#x432; &#x43d;&#x430;&#x434;&#x435;&#x436;&#x434;&#x435; &#x43f;&#x43e;&#x441;&#x43b;&#x430;&#x442;&#x44c; &#x43e;&#x442;&#x432;&#x435;&#x442; &#x43f;&#x440;&#x438;&#x43b;&#x43e;&#x436;&#x435;&#x43d;&#x438;&#x44f; &#x432;&#x43c;&#x435;&#x441;&#x442;&#x435; &#x441; &#x410;&#x421;&#x41a;" STYLE_REF="pre_last_node" ID="ID_1131211907" CREATED="1425023557738" MODIFIED="1425023634215"/>
<node TEXT="&#x412; BSD &#x437;&#x430;&#x434;&#x435;&#x440;&#x436;&#x43a;&#x430; == 200&#x43c;&#x441;" STYLE_REF="pre_last_node" ID="ID_906143053" CREATED="1425023601450" MODIFIED="1425023630649"/>
</node>
<node TEXT="&#x41f;&#x440;&#x438; &#x437;&#x430;&#x43f;&#x438;&#x441;&#x438; &#x43c;&#x435;&#x43b;&#x43a;&#x438;&#x445; &#x447;&#x430;&#x441;&#x442;&#x435;&#x439;" STYLE_REF="orange_node" ID="ID_1486004831" CREATED="1425023248137" MODIFIED="1425023317096">
<node TEXT="&#x410;&#x43b;&#x433;&#x43e;&#x440;&#x438;&#x442;&#x43c; &#x41d;&#x435;&#x439;&#x433;&#x43b;&#x430; &#x43a;&#x43e;&#x43d;&#x444;&#x43b;&#x438;&#x43a;&#x442;&#x443;&#x435;&#x442; &#x441; &#x43c;&#x435;&#x445;&#x430;&#x43d;&#x438;&#x437;&#x43c;&#x43e;&#x43c;&#xa;&#x43e;&#x442;&#x43b;&#x43e;&#x436;&#x435;&#x43d;&#x44b;&#x445; &#x43f;&#x43e;&#x434;&#x442;&#x432;&#x435;&#x440;&#x436;&#x434;&#x435;&#x43d;&#x438;&#x439;" STYLE_REF="pre_last_node" ID="ID_1953711608" CREATED="1425023262058" MODIFIED="1425023322264">
<node TEXT="" ID="ID_1980297997" CREATED="1425023469115" MODIFIED="1425023476090">
<hook URI="one-big.jpg" SIZE="0.8097166" NAME="ExternalObject"/>
</node>
<node TEXT="&#x41d;&#x430; &#x43a;&#x430;&#x436;&#x434;&#x43e;&#x43c; &#x442;&#x430;&#x43a;&#x442;&#x435; &#x437;&#x430;&#x43f;&#x440;&#x43e;&#x441;-&#x43e;&#x442;&#x432;&#x435;&#x442; &#x442;&#x435;&#x440;&#x44f;&#x435;&#x442;&#x441;&#x44f; &#x43e;&#x43a;&#x43e;&#x43b;&#x43e; 200 &#x43c;&#x441;" ID="ID_1841787274" CREATED="1425023269545" MODIFIED="1425023271614"/>
</node>
<node TEXT="&#x41a;&#x430;&#x436;&#x434;&#x430;&#x44f; &#x437;&#x430;&#x43f;&#x438;&#x441;&#x44c; &#x442;&#x440;&#x435;&#x431;&#x443;&#x435;&#x442; &#x434;&#x432;&#x443;&#x445;&#xa;&#x43f;&#x435;&#x440;&#x435;&#x43a;&#x43b;&#x44e;&#x447;&#x435;&#x43d;&#x438;&#x439; &#x43a;&#x43e;&#x43d;&#x442;&#x435;&#x43a;&#x441;&#x442;&#x430;" STYLE_REF="pre_last_node" ID="ID_348169715" CREATED="1425023276993" MODIFIED="1425023322263"/>
</node>
<node TEXT="&#x414;&#x43b;&#x44f; &#x44d;&#x442;&#x43e;&#x433;&#x43e;" STYLE_REF="orange_node" ID="ID_1965985989" CREATED="1425023167818" MODIFIED="1425023317100">
<node TEXT="&#x417;&#x430;&#x43f;&#x438;&#x441;&#x44b;&#x432;&#x430;&#x439;&#x442;&#x435; &#x438;&#x437; &#x43d;&#x435;&#x441;&#x43a;&#x43e;&#x43b;&#x44c;&#x43a;&#x438;&#x445; &#x431;&#x443;&#x444;&#x435;&#x440;&#x43e;&#x432;" STYLE_REF="yellow_node" ID="ID_1922691979" CREATED="1425023296066" MODIFIED="1425024047515">
<node TEXT="UNIX" ID="ID_950546198" CREATED="1425023833452" MODIFIED="1425023835830">
<node TEXT="#include &lt;sys/uio.h&gt;&#xa;ssize_t writev(int fd, const struct iovec *iov, int cnt);&#xa;sszie_t readv(int fd, const struct iovec *iov, int cnt);" STYLE_REF="pre_last_node" ID="ID_599273058" CREATED="1425023836642" MODIFIED="1425024055873">
<node TEXT="iov" ID="ID_85389726" CREATED="1425023941138" MODIFIED="1425023944073">
<node TEXT="&#x423;&#x43a;&#x430;&#x437;&#x430;&#x442;&#x435;&#x43b;&#x44c; &#x43d;&#x430; &#x43c;&#x438;&#x441;&#x438;&#x432; &#x441;&#x442;&#x440;&#x443;&#x43a;&#x442;&#x443;&#x440; iovec, &#x432; &#x43a;&#x43e;&#x442;&#x43e;&#x440;&#x44b;&#x445; &#x445;&#x440;&#x430;&#x43d;&#x44f;&#x442;&#x441;&#x44f;&#xa;&#x443;&#x43a;&#x430;&#x437;&#x430;&#x442;&#x435;&#x43b;&#x438; &#x43d;&#x430; &#x431;&#x443;&#x444;&#x435;&#x440;&#x44b; &#x434;&#x430;&#x43d;&#x43d;&#x44b;&#x445; &#x438; &#x440;&#x430;&#x437;&#x43c;&#x435;&#x440;&#x44b; &#x44d;&#x442;&#x438;&#x445; &#x431;&#x443;&#x444;&#x435;&#x440;&#x43e;&#x432;" ID="ID_524372225" CREATED="1425023945730" MODIFIED="1425023982734"/>
<node TEXT="struct iovec {&#xa;    char *iov_base; // &#x430;&#x434;&#x440;&#x435;&#x441; &#x43d;&#x430;&#x447;&#x430;&#x43b;&#x430; &#x431;&#x443;&#x444;&#xa;    size_t iov_len; // &#x434;&#x43b;&#x438;&#x43d;&#x430; &#x431;&#x443;&#x444;&#xa;};" ID="ID_31841723" CREATED="1425023986938" MODIFIED="1425024034191"/>
</node>
</node>
</node>
<node TEXT="WIN" ID="ID_1903088452" CREATED="1425023207530" MODIFIED="1425024066526">
<node TEXT="WSAsend" ID="ID_1973313858" CREATED="1425024067563" MODIFIED="1425024068631"/>
</node>
</node>
<node TEXT="&#x412;&#x43e;&#x437;&#x43c;&#x43e;&#x436;&#x43d;&#x43e; &#x43e;&#x442;&#x43a;&#x43b;&#x44e;&#x447;&#x438;&#x442;&#x44c; &#x430;&#x43b;&#x433;&#x43e;&#x440;&#x438;&#x442;&#x43c; &#x41d;&#x435;&#x439;&#x433;&#x43b;&#x430;" STYLE_REF="yellow_node" ID="ID_299151444" CREATED="1425023692810" MODIFIED="1425024047517">
<node TEXT="const int on = 1;&#xa;setsockopt(s, IPPROTO_TCP, TCP_NODELAY, &amp;on, sizeof(on));" STYLE_REF="pre_last_node" ID="ID_1582693708" CREATED="1425023707594" MODIFIED="1425024055880"/>
</node>
</node>
</node>
<node TEXT="API &#x441;&#x43e;&#x43a;&#x435;&#x442;&#x43e;&#x432;" STYLE_REF="second_node" POSITION="right" ID="ID_324989766" CREATED="1425025607260" MODIFIED="1425025652583">
<edge COLOR="#7b117b"/>
<font SIZE="11" BOLD="true"/>
<node TEXT="&#x41a;&#x43b;&#x438;&#x435;&#x43d;&#x442;" STYLE_REF="orange_node" ID="ID_934324053" CREATED="1425025615483" MODIFIED="1425025676756">
<node TEXT="" ID="ID_1752329418" CREATED="1425025657651" MODIFIED="1425025668694">
<hook URI="client-api.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="socket" STYLE_REF="pre_last_node" ID="ID_65886068" CREATED="1425025762651" MODIFIED="1425026234135">
<font BOLD="true"/>
<node TEXT="#include &lt;sys/socket.h&gt;&#xa;SOCKET socket(int domain, int type, int protocol);" ID="ID_766935423" CREATED="1425025770155" MODIFIED="1425025839536">
<node TEXT="int domain" STYLE_REF="yellow_node" ID="ID_172999689" CREATED="1425025846515" MODIFIED="1425026039857">
<node TEXT="AF_INET" STYLE_REF="pre_last_node" ID="ID_1442493102" CREATED="1425025860387" MODIFIED="1425026219825">
<node TEXT="&#x414;&#x43b;&#x44f; &#x438;&#x43d;&#x442;&#x435;&#x440;&#x43d;&#x435;&#x442;" ID="ID_1702796273" CREATED="1425025906019" MODIFIED="1425025916911"/>
</node>
<node TEXT="AF_LOCAL" STYLE_REF="pre_last_node" ID="ID_1159525484" CREATED="1425025884611" MODIFIED="1425026219828">
<hook NAME="FirstGroupNode"/>
</node>
<node TEXT="AF_UNIX" STYLE_REF="pre_last_node" ID="ID_1358400989" CREATED="1425025895803" MODIFIED="1425026219829">
<hook NAME="FirstGroupNode"/>
</node>
</node>
<node TEXT="int type" STYLE_REF="yellow_node" ID="ID_986035055" CREATED="1425025969483" MODIFIED="1425026039856">
<node TEXT="SOCK_STREAM" STYLE_REF="pre_last_node" ID="ID_1728310950" CREATED="1425025992979" MODIFIED="1425026219829">
<node TEXT="TCP" ID="ID_1281026468" CREATED="1425026016163" MODIFIED="1425026018103"/>
</node>
<node TEXT="SOCK_DGRAM" STYLE_REF="pre_last_node" ID="ID_856064161" CREATED="1425026000907" MODIFIED="1425026219830">
<node TEXT="UDP" ID="ID_1369933571" CREATED="1425026019659" MODIFIED="1425026021607"/>
</node>
<node TEXT="SOCK_RAW" STYLE_REF="pre_last_node" ID="ID_1161911939" CREATED="1425026007075" MODIFIED="1425026219830">
<node TEXT="&#x41d;&#x430; &#x443;&#x440;&#x43e;&#x432;&#x43d;&#x435; IP" ID="ID_1648637406" CREATED="1425026024771" MODIFIED="1425026030607"/>
</node>
</node>
<node TEXT="int protocol" STYLE_REF="yellow_node" ID="ID_627187628" CREATED="1425026055257" MODIFIED="1425026163657">
<node TEXT="&#x417;&#x430;&#x434;&#x430;&#x435;&#x442;&#x441;&#x44f; &#x43d;&#x435;&#x44f;&#x432;&#x43d;&#x43e; &#x442;&#x438;&#x43f;&#x43e;&#x43c; &#x441;&#x43e;&#x43a;&#x435;&#x442;" STYLE_REF="pre_last_node" ID="ID_1372202797" CREATED="1425026088635" MODIFIED="1425026219831"/>
<node TEXT="0" OBJECT="java.lang.Long|0" STYLE_REF="pre_last_node" ID="ID_343259321" CREATED="1425026062947" MODIFIED="1425026219831">
<node TEXT="&#x414;&#x43b;&#x44f; TCP" ID="ID_856673485" CREATED="1425026082067" MODIFIED="1425026086607"/>
</node>
</node>
</node>
</node>
<node TEXT="connect" STYLE_REF="pre_last_node" ID="ID_1030523831" CREATED="1425026225595" MODIFIED="1425026238232">
<font BOLD="true"/>
<node TEXT="#include &lt;winsock2.h&gt; // windows&#xa;int connect(socket s, const struct sockaddr *peer, int peer_len);" ID="ID_1801919005" CREATED="1425026253251" MODIFIED="1425026306391">
<node TEXT="const struct sockaddr" STYLE_REF="yellow_node" ID="ID_1094355011" CREATED="1425026335963" MODIFIED="1425027627408">
<node TEXT="&#x421;&#x442;&#x440;&#x443;&#x43a;&#x442;&#x443;&#x440;&#x430; &#x432; &#x43a;&#x43e;&#x442;&#x43e;&#x440;&#x43e;&#x439; &#x430;&#x434;&#x440;&#x435;&#x441; &#x438; &#x434;&#x43e;&#x43f;&#xa;&#x438;&#x43d;&#x444;&#x430; &#x43e;&#x431; &#x443;&#x434;&#x430;&#x43b;&#x435;&#x43d;&#x43d;&#x43e;&#x43c; &#x445;&#x43e;&#x441;&#x442;&#x435;" ID="ID_44768762" CREATED="1425026350020" MODIFIED="1425026450017"/>
</node>
<node TEXT="return" STYLE_REF="yellow_node" ID="ID_36770640" CREATED="1425027168044" MODIFIED="1425027627409">
<node TEXT="0 ok" ID="ID_1858052145" CREATED="1425027177444" MODIFIED="1425027185903"/>
<node TEXT="-1 &#x43e;&#x448;&#x438;&#x431;&#x43a;&#x430;" ID="ID_578957019" CREATED="1425027186733" MODIFIED="1425027191039"/>
</node>
</node>
</node>
<node TEXT="recv&#xa;send" STYLE_REF="pre_last_node" ID="ID_160626715" CREATED="1425026911571" MODIFIED="1425027110831">
<font BOLD="true"/>
<node TEXT="int recv(socket s, void *buf, size_t len, int flags);&#xa;int send(socket s, const void *buf, size_t len, int flags);" ID="ID_1044585952" CREATED="1425026927099" MODIFIED="1425026977392">
<node TEXT="int flags" STYLE_REF="yellow_node" ID="ID_41223621" CREATED="1425026985276" MODIFIED="1425027041264">
<node TEXT="MSG_OBB" STYLE_REF="pre_last_node" ID="ID_438959558" CREATED="1425026989947" MODIFIED="1425027105305">
<node TEXT="&#x421;&#x440;&#x43e;&#x447;&#x43d;&#x44b;&#x435; &#x434;&#x430;&#x43d;&#x43d;&#x44b;&#x435;" ID="ID_689693861" CREATED="1425027045060" MODIFIED="1425027050352"/>
</node>
<node TEXT="MSG_PEEK" STYLE_REF="pre_last_node" ID="ID_427878749" CREATED="1425026999932" MODIFIED="1425027105306">
<node TEXT="&#x41f;&#x440;&#x43e;&#x441;&#x43c;&#x43e;&#x442;&#x440; &#x43f;&#x43e;&#x441;&#x442;&#x443;&#x43f;&#x438;&#x432;&#x448;&#x44b;&#x445; &#x434;&#x430;&#x43d;&#x43d;&#x44b;&#x445; &#x431;&#x435;&#x437;&#xa;&#x438;&#x445; &#x443;&#x434;&#x430;&#x43b;&#x435;&#x43d;&#x438;&#x44f; &#x438;&#x437; &#x43f;&#x440;&#x438;&#x435;&#x43c;&#x43d;&#x43e;&#x433;&#x43e; &#x431;&#x443;&#x444;&#x435;&#x440;&#x430;" ID="ID_1991958697" CREATED="1425027059987" MODIFIED="1425027080879"/>
</node>
<node TEXT="MSG_DONTROUTE" STYLE_REF="pre_last_node" ID="ID_915016944" CREATED="1425027011179" MODIFIED="1425027105307">
<node TEXT="&#x41d;&#x435; &#x438;&#x441;&#x43f;&#x43e;&#x43b;&#x44c;&#x437;&#x43e;&#x432;&#x430;&#x442;&#x44c; &#x43e;&#x431;&#x44b;&#x447;&#x43d;&#x44b;&#x439; &#x430;&#x43b;&#x433;. &#x43c;&#x430;&#x440;&#x448;&#x440;&#x443;&#x442;&#x438;&#x437;&#x430;&#x446;&#x438;&#x438;" ID="ID_1402469271" CREATED="1425027083739" MODIFIED="1425027097681"/>
</node>
</node>
<node TEXT="return" STYLE_REF="yellow_node" ID="ID_874070317" CREATED="1425027121467" MODIFIED="1425027160154">
<node TEXT="&#x427;&#x438;&#x441;&#x43b;&#x43e; &#x43f;&#x440;&#x438;&#x43d;&#x44f;&#x442;&#x44b;&#x445; / &#x43f;&#x435;&#x440;&#x435;&#x434;&#x430;&#x43d;&#x44b;&#x445; &#x431;&#x430;&#x439;&#x442;" ID="ID_1669408144" CREATED="1425027125283" MODIFIED="1425027146111"/>
<node TEXT="-1 &#x435;&#x441;&#x43b;&#x438; &#x43e;&#x448;&#x438;&#x431;&#x43a;&#x430;" ID="ID_1858444408" CREATED="1425027147123" MODIFIED="1425027155441"/>
</node>
</node>
</node>
<node TEXT="recvfrom&#xa;sendto" STYLE_REF="pre_last_node" ID="ID_1978705163" CREATED="1425027114222" MODIFIED="1425027220207">
<font BOLD="true"/>
<node TEXT="int recvfrom(socket s, void *buf, size_t len, int flags, struct sockaddr *from, int fromlen);&#xa;int sendto(socket s, void *buf, size_t len, int flags, const struct sockaddr *to, int tolen);" ID="ID_1459257498" CREATED="1425027221883" MODIFIED="1425027308608">
<node TEXT="struct sockaddr *from" STYLE_REF="yellow_node" ID="ID_1332941804" CREATED="1425027319900" MODIFIED="1425027388736">
<node TEXT="&#x410;&#x434;&#x440;&#x435;&#x441; &#x438;&#x441;&#x442;&#x43e;&#x447;&#x43d;&#x438;&#x43a;&#x430; &#x434;&#x430;&#x442;&#x430;&#x433;&#x440;&#x430;&#x43c;&#x44b;" ID="ID_903656006" CREATED="1425027343044" MODIFIED="1425027378440"/>
</node>
<node TEXT="const struct sockaddr *to" STYLE_REF="yellow_node" ID="ID_992124850" CREATED="1425027358899" MODIFIED="1425027388736">
<node TEXT="&#x410;&#x434;&#x440;&#x435;&#x441; &#x43d;&#x430;&#x437;&#x43d;&#x430;&#x447;&#x435;&#x43d;&#x438;&#x44f; &#x434;&#x430;&#x442;&#x430;&#x433;&#x440;&#x430;&#x43c;&#x44b;" ID="ID_678388003" CREATED="1425027361051" MODIFIED="1425027380368"/>
</node>
</node>
</node>
</node>
</node>
<node TEXT="&#x421;&#x435;&#x440;&#x432;&#x435;&#x440;" STYLE_REF="orange_node" ID="ID_311567816" CREATED="1425025622347" MODIFIED="1425025676753">
<node TEXT="" ID="ID_1070272655" CREATED="1425025718035" MODIFIED="1425025724529">
<hook URI="server-api.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="socket" STYLE_REF="pre_last_node" ID="ID_702570126" CREATED="1425027397436" MODIFIED="1425027502127">
<font BOLD="true"/>
</node>
<node TEXT="bind" STYLE_REF="pre_last_node" ID="ID_568359395" CREATED="1425027403403" MODIFIED="1425027502129">
<font BOLD="true"/>
<node TEXT="int bind(socket s, const struct sockaddr *name, int namelen);" ID="ID_1135855514" CREATED="1425027406747" MODIFIED="1425027428688">
<node TEXT="&#x41f;&#x440;&#x438;&#x432;&#x44f;&#x437;&#x438;&#x432;&#x430;&#x435;&#x442; &#x430;&#x434;&#x440;&#x435;&#x441; &#x438;&#x43d;&#x442;&#x435;&#x440;&#x444;&#x435;&#x439;&#x441;&#x430; &#x438; &#x43d;&#x43e;&#x43c;&#x435;&#x440;&#xa;&#x43f;&#x43e;&#x440;&#x442;&#x430; &#x43a; &#x43f;&#x440;&#x43e;&#x441;&#x43b;&#x443;&#x448;&#x438;&#x432;&#x430;&#x44e;&#x449;&#x435;&#x43c;&#x443; &#x441;&#x43e;&#x43a;&#x435;&#x442;&#x443;" STYLE_REF="yellow_node" ID="ID_980896401" CREATED="1425027430012" MODIFIED="1425027597488"/>
<node TEXT="struct sockaddr *name" STYLE_REF="yellow_node" ID="ID_1042136423" CREATED="1425027523716" MODIFIED="1425027593441">
<node TEXT="&#x410;&#x434;&#x440;&#x435;&#x441; &#x438; &#x43f;&#x43e;&#x440;&#x442; &#x43a;&#x43e;&#x442;&#x43e;&#x440;&#x44b;&#x435; &#x43d;&#x443;&#x436;&#x43d;&#x43e; &#x441;&#x43b;&#x443;&#x448;&#x430;&#x442;&#x44c;" ID="ID_1641326375" CREATED="1425027531387" MODIFIED="1425027542208"/>
<node TEXT="INADDR_ANY - &#x431;&#x443;&#x434;&#x435;&#x442; &#x43f;&#x440;&#x438;&#x43d;&#x44f;&#x442;&#x43e; &#x441;&#x43e;&#x435;&#x434;&#x438;&#x43d;&#x435;&#x43d;&#x438;&#x435; &#x43f;&#x43e; &#x43b;&#x44e;&#x431;&#x43e;&#x43c;&#x443; &#x438;&#x43d;&#x442;&#x435;&#x440;&#x444;&#x435;&#x439;&#x441;&#x443;" ID="ID_310518418" CREATED="1425027543027" MODIFIED="1425027583960"/>
</node>
<node TEXT="return" STYLE_REF="yellow_node" ID="ID_1095059364" CREATED="1425027168044" MODIFIED="1425027634304">
<node TEXT="0 ok" ID="ID_82942756" CREATED="1425027177444" MODIFIED="1425027185903"/>
<node TEXT="-1 &#x43e;&#x448;&#x438;&#x431;&#x43a;&#x430;" ID="ID_286733009" CREATED="1425027186733" MODIFIED="1425027191039"/>
</node>
</node>
</node>
<node TEXT="listen" STYLE_REF="pre_last_node" ID="ID_304600540" CREATED="1425027460891" MODIFIED="1425027502130">
<font BOLD="true"/>
<node TEXT="int listen(socket s, int backlog);" ID="ID_327369766" CREATED="1425027637284" MODIFIED="1425027660080">
<node TEXT="&#x41f;&#x43e;&#x43c;&#x435;&#x447;&#x430;&#x435;&#x442; &#x441;&#x43e;&#x43a;&#x435;&#x442; &#x43a;&#x430;&#x43a; &#x43f;&#x440;&#x43e;&#x441;&#x43b;&#x443;&#x448;&#x438;&#x432;&#x430;&#x44e;&#x449;&#x438;&#x439;" STYLE_REF="yellow_node" ID="ID_1320853579" CREATED="1425027660988" MODIFIED="1425027731173"/>
<node TEXT="int backlog" STYLE_REF="yellow_node" ID="ID_927295327" CREATED="1425027673732" MODIFIED="1425027731173">
<node TEXT="&#x41c;&#x430;&#x43a;&#x441;&#x438;&#x43c;&#x430;&#x43b;&#x44c;&#x43d;&#x43e;&#x435; &#x447;&#x438;&#x441;&#x43b;&#x43e; &#x43e;&#x436;&#x438;&#x434;&#x430;&#x44e;&#x449;&#x438;&#x445; &#x43d;&#x43e; &#x435;&#x449;&#x435; &#x43d;&#x435; &#x43f;&#x440;&#x438;&#x43d;&#x44f;&#x442;&#x44b;&#x445; &#x441;&#x43e;&#x435;&#x434;&#x438;&#x43d;&#x435;&#x43d;&#x438;&#x439;" ID="ID_219790971" CREATED="1425027688812" MODIFIED="1425027712057"/>
<node TEXT="Default == 5" ID="ID_692648453" CREATED="1425027716347" MODIFIED="1425027723672"/>
</node>
<node TEXT="return" STYLE_REF="yellow_node" ID="ID_744988306" CREATED="1425027168044" MODIFIED="1425027634304">
<node TEXT="0 ok" ID="ID_344013391" CREATED="1425027177444" MODIFIED="1425027185903"/>
<node TEXT="-1 &#x43e;&#x448;&#x438;&#x431;&#x43a;&#x430;" ID="ID_1463089507" CREATED="1425027186733" MODIFIED="1425027191039"/>
</node>
</node>
</node>
<node TEXT="accept" STYLE_REF="pre_last_node" ID="ID_1221853738" CREATED="1425027467196" MODIFIED="1425027502131">
<font BOLD="true"/>
<node TEXT="int accept(socket s, struct sockaddr *addr, int *addrlen);" ID="ID_768156728" CREATED="1425027743515" MODIFIED="1425027765177">
<node TEXT="struct sockaddr *addr" STYLE_REF="yellow_node" ID="ID_10835012" CREATED="1425027807508" MODIFIED="1425027872812">
<node TEXT="&#x412;&#x43e;&#x437;&#x432;&#x440;&#x430;&#x449;&#x430;&#x435;&#x442; &#x430;&#x434;&#x440;&#x435;&#x441;&#x443;&#x434;&#x430;&#x43b;&#x435;&#x43d;&#x43d;&#x43e;&#x433;&#x43e; &#x43f;&#x440;&#x438;&#x43b;&#x43e;&#x436;&#x435;&#x43d;&#x438;&#x44f; &#x432; &#x44d;&#x442;&#x43e;&#x439; &#x441;&#x442;&#x440;&#x443;&#x43a;&#x442;&#x443;&#x440;&#x435;" ID="ID_594316745" CREATED="1425027833908" MODIFIED="1425027845792"/>
<node TEXT="&#x412; &#x44d;&#x442;&#x43e;&#x43c; &#x447;&#x430;&#x441;&#x442;&#x43e; &#x43d;&#x435;&#x442; &#x43d;&#x435;&#x43e;&#x431;&#x445;&#x43e;&#x434;&#x438;&#x43c;&#x43e;&#x441;&#x442;&#x438; &#x43f;&#x43e;&#x44d;&#x442;&#x43e;&#x43c;&#x443; &#x43f;&#x435;&#x440;&#x435;&#x434;&#x430;&#x435;&#x43c; null, null" ID="ID_592141777" CREATED="1425027846636" MODIFIED="1425027867704"/>
</node>
<node TEXT="return" STYLE_REF="yellow_node" ID="ID_785603081" CREATED="1425027773132" MODIFIED="1425027872815">
<node TEXT="&#x414;&#x435;&#x441;&#x43a;&#x440;&#x438;&#x43f;&#x442;&#x43e;&#x440; &#x43d;&#x43e;&#x432;&#x43e;&#x433;&#x43e; &#x441;&#x43e;&#x43a;&#x435;&#x442;&#x430; &#x43f;&#x43e; &#x43a;&#x43e;&#x442;&#x43e;&#x440;&#x43e;&#x43c;&#x443;&#xa;&#x431;&#x443;&#x434;&#x435;&#x442; &#x43f;&#x440;&#x43e;&#x438;&#x441;&#x445;&#x43e;&#x434;&#x438;&#x442;&#x44c; &#x43e;&#x431;&#x43c;&#x435;&#x43d; &#x434;&#x430;&#x43d;&#x43d;&#x44b;&#x43c;&#x438;" ID="ID_256244053" CREATED="1425027778684" MODIFIED="1425027796809"/>
<node TEXT="-1 &#x43e;&#x448;&#x438;&#x431;&#x43a;&#x430;" ID="ID_1703544424" CREATED="1425027800004" MODIFIED="1425027803827"/>
</node>
</node>
</node>
<node TEXT="recv&#xa;send" STYLE_REF="pre_last_node" ID="ID_107877882" CREATED="1425027470612" MODIFIED="1425027502133">
<font BOLD="true"/>
</node>
<node TEXT="close&#xa;closesocket" STYLE_REF="pre_last_node" ID="ID_1617903243" CREATED="1425027485052" MODIFIED="1425027502134">
<font BOLD="true"/>
</node>
</node>
</node>
</node>
<node TEXT="TCP &#x43d;&#x435; &#x432;&#x44b;&#x43f;&#x43e;&#x43b;&#x43d;&#x44f;&#x435;&#x442; &#x43e;&#x43f;&#x440;&#x43e;&#x441; &#x441;&#x43e;&#x435;&#x434;&#x438;&#x43d;&#x435;&#x43d;&#x438;&#x44f;" STYLE_REF="second_node" POSITION="left" ID="ID_162218713" CREATED="1425027935468" MODIFIED="1425030606172">
<edge COLOR="#0f7e7e"/>
<font SIZE="11" BOLD="true"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      TCP &#1085;&#1077; &#1091;&#1074;&#1077;&#1076;&#1086;&#1084;&#1083;&#1103;&#1077;&#1090; &#1086; &#1087;&#1086;&#1090;&#1077;&#1088;&#1077; &#1089;&#1074;&#1103;&#1079;&#1080; &#1085;&#1077;&#1084;&#1077;&#1076;&#1083;&#1077;&#1085;&#1085;&#1086;
    </p>
  </body>
</html>

</richcontent>
<hook URI="../../Images/interview.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x41f;&#x43e;&#x447;&#x435;&#x43c;&#x443;" STYLE_REF="orange_node" ID="ID_1898008481" CREATED="1425028049212" MODIFIED="1425029240249">
<node TEXT="&#x411;&#x43e;&#x43b;&#x44c;&#x448;&#x438;&#x43d;&#x441;&#x442;&#x432;&#x443; &#x43f;&#x440;&#x438;&#x43b;&#x43e;&#x436;&#x435;&#x43d;&#x438;&#x439; &#x44d;&#x442;&#x43e; &#x43d;&#x435; &#x43d;&#x443;&#x436;&#x43d;&#x43e;" STYLE_REF="pre_last_node" ID="ID_766601321" CREATED="1425028055716" MODIFIED="1425028155451"/>
<node TEXT="&#x42d;&#x442;&#x43e; &#x44d;&#x43a;&#x43e;&#x43d;&#x43e;&#x43c;&#x438;&#x442; &#x441;&#x435;&#x442;&#x435;&#x432;&#x44b;&#x435; &#x440;&#x435;&#x441;&#x443;&#x440;&#x441;&#x44b;" STYLE_REF="pre_last_node" ID="ID_139723943" CREATED="1425028072092" MODIFIED="1425029240249"/>
<node TEXT="&#x41f;&#x440;&#x438;&#x43b;&#x43e;&#x436;&#x435;&#x43d;&#x438;&#x44f; &#x43a;&#x43e;&#x442;&#x43e;&#x440;&#x44b;&#x43c; &#x44d;&#x442;&#x43e; &#x43d;&#x443;&#x436;&#x43d;&#x43e; &#x43c;&#x43e;&#x433;&#x443;&#x442;&#xa;&#x440;&#x435;&#x430;&#x43b;&#x438;&#x437;&#x43e;&#x432;&#x430;&#x442;&#x44c; &#x43d;&#x430; &#x43f;&#x440;&#x438;&#x43a;&#x43b;&#x430;&#x434;&#x43d;&#x43e;&#x43c; &#x443;&#x440;&#x43e;&#x432;&#x43d;&#x435;" STYLE_REF="pre_last_node" ID="ID_1239129338" CREATED="1425028091244" MODIFIED="1425028155456"/>
</node>
<node TEXT="&#x41f;&#x440;&#x438;&#x447;&#x438;&#x43d;&#x44b; &#x440;&#x430;&#x437;&#x440;&#x44b;&#x432;&#x430; &#x441;&#x432;&#x44f;&#x437;&#x438;" STYLE_REF="orange_node" ID="ID_429010232" CREATED="1425028129236" MODIFIED="1425029236202">
<hook URI="../../Images/disconnection-2.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x421;&#x431;&#x43e;&#x439; &#x432; &#x441;&#x435;&#x442;&#x438;" STYLE_REF="yellow_node" ID="ID_1415340783" CREATED="1425028504180" MODIFIED="1425028539218">
<node TEXT="&#x41a;&#x43e;&#x43d;&#x435;&#x447;&#x43d;&#x44b;&#x435; &#x442;&#x43e;&#x447;&#x43a;&#x438;" STYLE_REF="pre_last_node" ID="ID_819264875" CREATED="1425028817901" MODIFIED="1425028999662"/>
<node TEXT="&#x41f;&#x440;&#x43e;&#x43c;&#x435;&#x436;&#x443;&#x442;&#x43e;&#x447;&#x43d;&#x44b;&#x435; &#x442;&#x43e;&#x447;&#x43a;&#x438;" STYLE_REF="pre_last_node" ID="ID_1978883343" CREATED="1425028829676" MODIFIED="1425028999660">
<node TEXT="&#x41c;&#x430;&#x440;&#x448;&#x440;&#x443;&#x442;&#x438;&#x437;&#x430;&#x442;&#x43e;&#x440;" ID="ID_1330206914" CREATED="1425028841829" MODIFIED="1425028858864">
<node TEXT="&#x41f;&#x43e;&#x441;&#x44b;&#x43b;&#x430;&#x435;&#x442; ICMP &#x447;&#x442;&#x43e; &#x43f;&#x43e;&#x43b;&#x443;&#x447;&#x430;&#x442;&#x435;&#x43b;&#x44c; &#x43d;&#x435; &#x434;&#x43e;&#x441;&#x442;&#x443;&#x43f;&#x435;&#x43d;" STYLE_REF="yellow_node" ID="ID_236712527" CREATED="1425028859596" MODIFIED="1425029006795">
<node TEXT="&#x412;&#x43e;&#x437;&#x432;&#x440;&#x430;&#x442; &#x43e;&#x448;&#x438;&#x431;&#x43e;&#x43a;" ID="ID_522731206" CREATED="1425028900677" MODIFIED="1425028979809">
<node TEXT="ENETUNREACH" STYLE_REF="pre_last_node" ID="ID_911149000" CREATED="1425028907477" MODIFIED="1425029021216">
<font BOLD="true"/>
</node>
<node TEXT="EHOSTUNREACH" STYLE_REF="pre_last_node" ID="ID_868033767" CREATED="1425028918092" MODIFIED="1425029021218">
<font BOLD="true"/>
</node>
</node>
</node>
<node TEXT="&#x41d;&#x435; &#x43f;&#x43e;&#x441;&#x44b;&#x43b;&#x430;&#x435;&#x442; ICMP" STYLE_REF="yellow_node" ID="ID_836587404" CREATED="1425028875076" MODIFIED="1425029006794">
<node TEXT="&#x421;&#x440;&#x430;&#x431;&#x430;&#x442;&#x44b;&#x432;&#x430;&#x435;&#x442; &#x43f;&#x43e;&#x432;&#x442;&#x43e;&#x440; &#x43d;&#x430; &#x43e;&#x434;&#x43f;&#x440;&#x430;&#x432;&#x43a;&#x443;" ID="ID_540538193" CREATED="1425028934556" MODIFIED="1425028952080">
<node TEXT="&#x412; BSD == 12 &#x43f;&#x43e;&#x43f;&#x44b;&#x442;&#x43e;&#x43a;" ID="ID_666941779" CREATED="1425028953084" MODIFIED="1425028968755">
<node TEXT="&#x412;&#x43e;&#x437;&#x432;&#x440;&#x430;&#x442; &#x43e;&#x448;&#x438;&#x431;&#x43e;&#x43a;" ID="ID_266073720" CREATED="1425028969612" MODIFIED="1425028975545">
<node TEXT="ETIMEDOUT" STYLE_REF="pre_last_node" ID="ID_1759115935" CREATED="1425028982132" MODIFIED="1425029021220">
<font BOLD="true"/>
</node>
<node TEXT="EPIPE" STYLE_REF="pre_last_node" ID="ID_1192710428" CREATED="1425028988756" MODIFIED="1425029021221">
<font BOLD="true"/>
</node>
</node>
</node>
</node>
</node>
</node>
</node>
</node>
<node TEXT="&#x41a;&#x440;&#x430;&#x445; &#x445;&#x43e;&#x441;&#x442;&#x430;" STYLE_REF="yellow_node" ID="ID_1095681438" CREATED="1425028512268" MODIFIED="1425028539218"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1053;&#1072; &#1076;&#1088;&#1091;&#1075;&#1086;&#1084; &#1082;&#1086;&#1085;&#1094;&#1077;
    </p>
  </body>
</html>

</richcontent>
<node TEXT="TCP &#x43d;&#x435; &#x43c;&#x43e;&#x436;&#x435;&#x442; &#x43e;&#x442;&#x43e;&#x441;&#x43b;&#x430;&#x442;&#x44c; RST" STYLE_REF="pre_last_node" ID="ID_138518400" CREATED="1425028545556" MODIFIED="1425028745929"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      RST - &#1089;&#1073;&#1088;&#1086;&#1089;
    </p>
  </body>
</html>

</richcontent>
<node TEXT="&#x423;&#x434;&#x430;&#x43b;&#x435;&#x43d;&#x43d;&#x44b;&#x439; &#x445;&#x43e;&#x441;&#x442;&#x44c; &#x43d;&#x435; &#x43e;&#x442;&#x432;&#x435;&#x447;&#x430;&#x435;&#x442;" STYLE_REF="yellow_node" ID="ID_1437507781" CREATED="1425028560396" MODIFIED="1425028813937">
<node TEXT="ETIMEDOUT" STYLE_REF="pre_last_node" ID="ID_703216228" CREATED="1425028627453" MODIFIED="1425029050280"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1054;&#1096;&#1080;&#1073;&#1082;&#1072;
    </p>
  </body>
</html>

</richcontent>
<font BOLD="true"/>
</node>
</node>
<node TEXT="&#x425;&#x43e;&#x441;&#x442; &#x43f;&#x435;&#x440;&#x435;&#x437;&#x430;&#x433;&#x440;&#x443;&#x437;&#x438;&#x43b;&#x441;&#x44f; &#x438; &#x43f;&#x43e;&#x442;&#x435;&#x440;&#x44f;&#x43b; &#x441;&#x43e;&#x435;&#x434;&#x438;&#x43d;&#x435;&#x43d;&#x438;&#x435;" STYLE_REF="yellow_node" ID="ID_1583573485" CREATED="1425028581508" MODIFIED="1425028813938">
<node TEXT="&#x41e;&#x442;&#x43e;&#x448;&#x43b;&#x435;&#x442; RST &#x43d;&#x430; &#x43f;&#x440;&#x438;&#x445;&#x43e;&#x434;&#x44f;&#x449;&#x438;&#x435; &#x43f;&#x430;&#x43a;&#x435;&#x442;&#x44b;" ID="ID_1297514216" CREATED="1425028613476" MODIFIED="1425028624625"/>
</node>
</node>
</node>
<node TEXT="&#x41e;&#x442;&#x43a;&#x430;&#x437; &#x43f;&#x440;&#x438;&#x43b;&#x43e;&#x436;&#x435;&#x43d;&#x438;&#x44f;" STYLE_REF="yellow_node" ID="ID_41609771" CREATED="1425028526236" MODIFIED="1425028539217">
<node TEXT="TCP &#x441;&#x435;&#x440;&#x432;&#x435;&#x440;&#x430; &#x43e;&#x442;&#x441;&#x44b;&#x43b;&#x430;&#x435;&#x442; FIN" STYLE_REF="pre_last_node" ID="ID_1298485440" CREATED="1425028639748" MODIFIED="1425028726789">
<node TEXT="&#x415;&#x441;&#x43b;&#x438; &#x43f;&#x440;&#x43e;&#x434;&#x43e;&#x43b;&#x436;&#x438;&#x442;&#x44c; &#x43f;&#x43e;&#x441;&#x44b;&#x43b;&#x430;&#x442;&#x44c; &#x441;&#x435;&#x440;&#x432;&#x435;&#x440; &#x43e;&#x442;&#x432;&#x435;&#x442;&#x438;&#x442; RST" STYLE_REF="yellow_node" ID="ID_1563096677" CREATED="1425028666940" MODIFIED="1425028813938">
<node TEXT="ECONNRESET" STYLE_REF="pre_last_node" ID="ID_692317967" CREATED="1425028695884" MODIFIED="1425029050280"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1054;&#1096;&#1080;&#1073;&#1082;&#1072; &#1085;&#1072; &#1082;&#1083;&#1080;&#1077;&#1085;&#1090;&#1077;
    </p>
  </body>
</html>

</richcontent>
<font BOLD="true"/>
</node>
</node>
</node>
<node TEXT="" ID="ID_1952619741" CREATED="1425029799661" MODIFIED="1425029807178">
<hook URI="server-crush.jpg" SIZE="0.81411123" NAME="ExternalObject"/>
</node>
</node>
</node>
<node TEXT="&#x420;&#x435;&#x448;&#x435;&#x43d;&#x438;&#x44f;" STYLE_REF="orange_node" ID="ID_977754253" CREATED="1425028024492" MODIFIED="1425030143484" VGAP="16">
<node TEXT="&#x41a;&#x43e;&#x43d;&#x442;&#x440;&#x43e;&#x43b;&#x43b;&#x435;&#x440;&#x44b;" STYLE_REF="yellow_node" ID="ID_231971902" CREATED="1425028159252" MODIFIED="1425030143482">
<font BOLD="true"/>
<hook URI="../../Images/controller.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x41d;&#x443;&#x436;&#x43d;&#x43e; &#x430;&#x43a;&#x442;&#x438;&#x432;&#x438;&#x440;&#x43e;&#x432;&#x430;&#x442;&#x44c;" STYLE_REF="pre_last_node" ID="ID_1674835817" CREATED="1425028173749" MODIFIED="1425029444729">
<node TEXT="setsockopt(socket s, , SO_KEEPALIVE, 0/1);" ID="ID_1640478499" CREATED="1425029527740" MODIFIED="1425029643026">
<node TEXT="0 &#x43e;&#x442;&#x43a;&#x43b;&#x44e;&#x447;&#x438;&#x442;" ID="ID_875854969" CREATED="1425029644166" MODIFIED="1425029650209"/>
<node TEXT="1 &#x430;&#x43a;&#x442;&#x438;&#x432;&#x438;&#x440;&#x43e;&#x432;&#x430;&#x442;&#x44c;" ID="ID_712238387" CREATED="1425029651437" MODIFIED="1425029656442"/>
</node>
</node>
<node TEXT="&#x41e;&#x431;&#x43d;&#x430;&#x440;&#x443;&#x436;&#x438;&#x432;&#x430;&#x435;&#x442; &#x434;&#x430;&#x432;&#x43d;&#x43e; &#x437;&#x430;&#x432;&#x438;&#x441;&#x448;&#x438;&#x435; &#x441;&#x43e;&#x435;&#x434;&#x438;&#x43d;&#x435;&#x43d;&#x438;&#x44f;" STYLE_REF="pre_last_node" ID="ID_767157247" CREATED="1425028180836" MODIFIED="1425029444729"/>
<node TEXT="&#x411;&#x43e;&#x43b;&#x44c;&#x448;&#x43e;&#x439; &#x432;&#x440;&#x435;&#x43c;&#x435;&#x43d;&#x43d;&#x43e;&#x439; &#x438;&#x43d;&#x442;&#x435;&#x440;&#x432;&#x430;&#x43b; &#x434;&#x43e; &#x441;&#x440;&#x430;&#x431;&#x430;&#x442;&#x44b;&#x432;&#x430;&#x43d;&#x438;&#x44f;" STYLE_REF="pre_last_node" ID="ID_1113799510" CREATED="1425028198060" MODIFIED="1425029444728">
<node TEXT="~2 &#x447;&#x430;&#x441;&#x430;" ID="ID_1848646952" CREATED="1425028216988" MODIFIED="1425028226776"/>
</node>
<node TEXT="&#x41a;&#x43e;&#x43d;&#x442;&#x440;&#x43e;&#x43b;&#x43b;&#x435;&#x440;&#x44b; &#x443;&#x441;&#x442;&#x430;&#x43d;&#x430;&#x432;&#x43b;&#x438;&#x432;&#x430;&#x44e;&#x442;&#x441;&#x44f; &#x442;&#x43e;&#x43b;&#x44c;&#x43a;&#x43e;&#xa;&#x434;&#x43b;&#x44f; &#x432;&#x441;&#x435;&#x445; &#x441;&#x440;&#x430;&#x437;&#x443; &#x441;&#x43e;&#x435;&#x434;&#x438;&#x43d;&#x435;&#x43d;&#x438;&#x439;" STYLE_REF="pre_last_node" ID="ID_603357904" CREATED="1425028228564" MODIFIED="1425029519458"/>
</node>
<node TEXT="&#x41f;&#x443;&#x43b;&#x44c;&#x441;&#x430;&#x446;&#x438;&#x44f;" STYLE_REF="yellow_node" ID="ID_661177175" CREATED="1425028251284" MODIFIED="1425030092069">
<font BOLD="true"/>
<hook URI="../../Images/pulse.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x41f;&#x440;&#x438;&#x43b;&#x43e;&#x436;&#x435;&#x43d;&#x438;&#x44f; &#x43f;&#x435;&#x440;&#x435;&#x434;&#x430;&#x442; &#x434;&#x430;&#x43d;&#x43d;&#x44b;&#x435; &#x432; &#x432;&#x438;&#x434;&#x435; &#x43f;&#x43e;&#x442;&#x43e;&#x43a;&#x430; &#x431;&#x430;&#x439;&#x442;&#x43e;&#x432;" STYLE_REF="pre_last_node" ID="ID_1977924324" CREATED="1425029313476" MODIFIED="1425029444726">
<node TEXT="&#x414;&#x43b;&#x44f; &quot;&#x43f;&#x443;&#x43b;&#x44c;&#x441;&#x43e;&#x432;&quot; &#x43e;&#x442;&#x434;&#x435;&#x43b;&#x44c;&#x43d;&#x43e;&#x435; &#x441;&#x43e;&#x435;&#x434;&#x438;&#x43d;&#x435;&#x43d;&#x438;&#x435;" ID="ID_1715243536" CREATED="1425029334412" MODIFIED="1425029347747"/>
</node>
<node TEXT="&#x41f;&#x440;&#x438;&#x43b;&#x43e;&#x436;&#x435;&#x43d;&#x438;&#x44f; &#x43f;&#x435;&#x440;&#x435;&#x434;&#x430;&#x44e;&#x442; &#x441;&#x43e;&#x43e;&#x431;&#x449;&#x435;&#x43d;&#x438;&#x44f; &#x440;&#x430;&#x437;&#x43d;&#x44b;&#x445; &#xa;&#x442;&#x438;&#x43f;&#x43e;&#x432;, &#x43a;&#x430;&#x436;&#x434;&#x43e;&#x435; &#x438;&#x43c;&#x435;&#x435;&#x442; &#x437;&#x430;&#x433;&#x43e;&#x43b;&#x43e;&#x432;&#x43e;&#x43a;" STYLE_REF="pre_last_node" ID="ID_19255618" CREATED="1425029349845" MODIFIED="1425029463561">
<node TEXT="&#x41d;&#x43e;&#x432;&#x43e;&#x435; &#x441;&#x43e;&#x43e;&#x431;&#x449;&#x435;&#x43d;&#x438;&#x435; &quot;&#x43f;&#x443;&#x43b;&#x44c;&#x441;&quot;, &#x43f;&#x43e;&#x43b;&#x443;&#x447;&#x430;&#x442;&#x435;&#x43b;&#x44c;&#xa;&#x43e;&#x442;&#x43f;&#x440;&#x430;&#x432;&#x43b;&#x44f;&#x435;&#x442; &#x435;&#x433;&#x43e; &#x43d;&#x430;&#x437;&#x430;&#x434; &#x43e;&#x442;&#x43f;&#x440;&#x430;&#x432;&#x438;&#x442;&#x435;&#x43b;&#x44e;" ID="ID_1023743875" CREATED="1425029386308" MODIFIED="1425029439213"/>
</node>
</node>
</node>
</node>
</node>
</map>
