<map version="freeplane 1.3.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="&#x414;&#x456;&#x442;&#x438; &#x437; &#x43d;&#x435;&#x431;&#x435;&#x441;" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1427365944997" BACKGROUND_COLOR="#ffff00" VGAP="8">
<font SIZE="20" BOLD="true"/>
<edge STYLE="sharp_bezier" COLOR="#003333" WIDTH="4"/>
<hook NAME="MapStyle">

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
<stylenode TEXT="second_node" BACKGROUND_COLOR="#00ff66" STYLE="bubble">
<font SIZE="14"/>
</stylenode>
<stylenode TEXT="pre_last_node" BACKGROUND_COLOR="#99ff99" STYLE="bubble">
<edge STYLE="bezier" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="yellow_node" BACKGROUND_COLOR="#ffff33" STYLE="bubble"/>
<stylenode TEXT="orange_node" BACKGROUND_COLOR="#ff9900">
<font BOLD="true"/>
</stylenode>
<stylenode TEXT="last_node">
<edge STYLE="bezier" WIDTH="thin"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook URI="../../Images/child-from-heaven-large.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x423;&#x441;&#x442;&#x430;&#x43d;&#x43e;&#x432;&#x43b;&#x435;&#x43d;&#x43d;&#x44f; &#x441;&#x43f;&#x456;&#x432;&#x43f;&#x440;&#x430;&#x446;&#x456;" STYLE_REF="second_node" POSITION="right" ID="ID_229510684" CREATED="1418574676168" MODIFIED="1427365585437" BACKGROUND_COLOR="#00ff66">
<edge COLOR="#660000"/>
<hook URI="../../Images/partnership.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x414;&#x430;&#x432;&#x430;&#x439; &#x437;&#x440;&#x43e;&#x431;&#x438;&#x43c;&#x43e; &#x442;&#x43e;&#x442;&#x43e; &#x456; &#x442;&#x43e;&#x442;&#x43e;" STYLE_REF="third_node" ID="ID_1990012006" CREATED="1418577622187" MODIFIED="1418628427532" BACKGROUND_COLOR="#99ff99" STYLE="bubble">
<edge STYLE="bezier" WIDTH="thin"/>
<node TEXT="&#x414;&#x43e; 9 &#x440;&#x43e;&#x43a;&#x456;&#x432; &#x446;&#x435; &#x441;&#x443;&#x43f;&#x435;&#x440; &#x444;&#x43e;&#x440;&#x43c;&#x443;&#x43b;&#x430;" STYLE_REF="last_node" ID="ID_863659549" CREATED="1418579702241" MODIFIED="1418627679984"/>
</node>
<node TEXT="&#x41f;&#x440;&#x43e;&#x445;&#x430;&#x43d;&#x43d;&#x44f; &#x437;&#x430;&#x43c;&#x456;&#x441;&#x442;&#x44c; &#x43f;&#x440;&#x438;&#x43a;&#x430;&#x437;&#x456;&#x432; &#x456; &#x432;&#x438;&#x43c;&#x43e;&#x433;" STYLE_REF="pre_last_node" ID="ID_1491552184" CREATED="1418577697791" MODIFIED="1418628464836"/>
<node TEXT="&#x417;&#x440;&#x43e;&#x431;&#x438; ... &#x431;&#x443;&#x434;&#x44c;-&#x43b;&#x430;&#x441;&#x43a;&#x430; &#x442;&#x43e;&#x442;&#x43e;" STYLE_REF="pre_last_node" ID="ID_1318700487" CREATED="1418577713678" MODIFIED="1418628464882"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1047;&#1072;&#1084;&#1110;&#1089;&#1090;&#1100;: &#1058;&#1080; &#1084;&#1086;&#1078;&#1077;&#1096; ...
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&#x412;&#x456;&#x434;&#x43c;&#x43e;&#x432;&#x430; &#x432;&#x456;&#x434; &#x43b;&#x435;&#x43a;&#x446;&#x456;&#x439;!&#xa;&#x41c;&#x43e;&#x436;&#x43d;&#x430; &#x442;&#x456;&#x43b;&#x44c;&#x43a;&#x438; &#x43a;&#x43e;&#x43b;&#x438; &#x43f;&#x440;&#x43e;&#x441;&#x44f;&#x442;&#x44c;" STYLE_REF="pre_last_node" ID="ID_215703319" CREATED="1418587088328" MODIFIED="1418628464909"/>
<node TEXT="&#x412;&#x456;&#x434;&#x43c;&#x43e;&#x432;&#x430; &#x432;&#x456;&#x434; &#x440;&#x438;&#x442;&#x43e;&#x440;&#x438;&#x447;&#x43d;&#x438;&#x445; &#x437;&#x430;&#x43f;&#x438;&#x442;&#x430;&#x43d;&#x44c;" STYLE_REF="pre_last_node" ID="ID_1224148319" CREATED="1418587130751" MODIFIED="1418628464936"/>
<node TEXT="&#x413;&#x43e;&#x432;&#x43e;&#x440;&#x438;&#x442;&#x44c; &#x43f;&#x440;&#x44f;&#x43c;&#x43e;" STYLE_REF="pre_last_node" ID="ID_1913424566" CREATED="1418587119334" MODIFIED="1418628464948">
<node TEXT="&#x42f; &#x445;&#x43e;&#x447;&#x443; &#x449;&#x43e;&#x431; &#x442;&#x438; &#x43f;&#x435;&#x440;&#x435;&#x441;&#x442;&#x430;&#x432; &#x43a;&#x438;&#x434;&#x430;&#x442;&#x44c; &#x456;&#x433;&#x440;&#x430;&#x448;&#x43a;&#x438;,&#xa;&#x456; &#x441;&#x43f;&#x443;&#x441;&#x442;&#x438;&#x432;&#x441;&#x44f; &#x432;&#x43d;&#x438;&#x437; &#x441;&#x43d;&#x456;&#x434;&#x430;&#x442;&#x44c; &#x456;&#x437; &#x441;&#x456;&#x43c;&apos;&#x454;&#x44e;" STYLE_REF="last_node" ID="ID_208886462" CREATED="1418587364763" MODIFIED="1418627679992"/>
</node>
<node TEXT="&#x412;&#x456;&#x434;&#x43c;&#x43e;&#x432;&#x430; &#x432;&#x456;&#x434; &#x43f;&#x43e;&#x44f;&#x441;&#x43d;&#x435;&#x43d;&#x44c;!" STYLE_REF="pre_last_node" ID="ID_97571235" CREATED="1418587299975" MODIFIED="1418628464959"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1052;&#1086;&#1078;&#1085;&#1072; &#1090;&#1110;&#1083;&#1100;&#1082;&#1080; &#1087;&#1110;&#1089;&#1083;&#1103; &#1079;&#1088;&#1086;&#1073;&#1083;&#1077;&#1085;&#1086;&#1075;&#1086;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&#x412;&#x456;&#x434;&#x43c;&#x43e;&#x432;&#x430; &#x432;&#x456;&#x434; &#x43b;&#x435;&#x43a;&#x446;&#x456;&#x439;!" STYLE_REF="pre_last_node" ID="ID_584956115" CREATED="1418587507960" MODIFIED="1418628464969"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1052;&#1086;&#1078;&#1085;&#1072; &#1090;&#1110;&#1083;&#1100;&#1082;&#1080; &#1082;&#1086;&#1083;&#1080; &#1087;&#1088;&#1086;&#1089;&#1103;&#1090;&#1100;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&#x412;&#x456;&#x434;&#x43c;&#x43e;&#x432;&#x430; &#x432;&#x456;&#x434; &#x43c;&#x430;&#x43d;&#x456;&#x43f;&#x443;&#x43b;&#x44e;&#x432;&#x430;&#x43d;&#x43d;&#x44f; &#x43f;&#x43e;&#x447;&#x443;&#x442;&#x442;&#x44f;&#x43c;&#x438; &#x434;&#x438;&#x442;&#x438;&#x43d;&#x438;!" STYLE_REF="pre_last_node" ID="ID_1279052936" CREATED="1418587573222" MODIFIED="1418628464979"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1053;&#1077; &#1076;&#1110;&#1083;&#1080;&#1090;&#1080;&#1089;&#1103; &#1079; &#1076;&#1080;&#1090;&#1080;&#1085;&#1086;&#1102; &#1085;&#1077;&#1075;&#1072;&#1090;&#1080;&#1074;&#1085;&#1080;&#1084;&#1080; &#1077;&#1084;&#1086;&#1094;&#1110;&#1103;&#1084;&#1080;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="&#x41f;&#x43e;&#x434;&#x43e;&#x43b;&#x430;&#x43d;&#x43d;&#x44f; &#x43e;&#x43f;&#x43e;&#x440;&#x443; &#x437;&#x430; &#x442;&#x438;&#x43f;&#x430;&#x43c;&#x438;" STYLE_REF="second_node" POSITION="right" ID="ID_1861996079" CREATED="1418576981076" MODIFIED="1427365419871">
<edge COLOR="#000066"/>
<hook URI="../../Images/resistance.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x421;&#x435;&#x43d;&#x441;&#x435;&#x442;&#x438;&#x432;&#x43d;&#x438;&#x439; &#x442;&#x438;&#x43f;" STYLE_REF="orange_node" ID="ID_1794932839" CREATED="1418588136437" MODIFIED="1427365239988"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1042;&#1086;&#1076;&#1072;
    </p>
  </body>
</html>
</richcontent>
<hook URI="../../Images/water.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x41e;&#x441;&#x43e;&#x431;&#x43b;&#x438;&#x432;&#x43e;&#x441;&#x442;&#x456;" STYLE_REF="third_node" ID="ID_1611786872" CREATED="1418588584073" MODIFIED="1418642559319" BACKGROUND_COLOR="#ffff33" STYLE="bubble">
<icon BUILTIN="list"/>
<node TEXT="&#x414;&#x456;&#x442;&#x438; &#x43f;&#x43e;&#x441;&#x442;&#x456;&#x439;&#x43d;&#x43e; &#x436;&#x430;&#x43b;&#x456;&#x44e;&#x442;&#x44c;&#x441;&#x44f;" STYLE_REF="pre_last_node" ID="ID_1046230849" CREATED="1418629129984" MODIFIED="1418629152313"/>
<node TEXT="&#x413;&#x43e;&#x441;&#x442;&#x440;&#x43e; &#x441;&#x43f;&#x440;&#x438;&#x439;&#x43c;&#x430;&#x44e;&#x442;&#x44c; &#x43d;&#x435;&#x432;&#x434;&#x430;&#x447;&#x456;" STYLE_REF="pre_last_node" ID="ID_914688574" CREATED="1418629139323" MODIFIED="1418629152304"/>
</node>
<node TEXT="&#x414;&#x43e;&#x43f;&#x43e;&#x43c;&#x43e;&#x433;&#x430;" STYLE_REF="yellow_node" ID="ID_1930972323" CREATED="1418588592868" MODIFIED="1418628841680">
<icon BUILTIN="button_ok"/>
<node TEXT="&#x421;&#x43b;&#x443;&#x445;&#x430;&#x43d;&#x43d;&#x44f; + &#x440;&#x43e;&#x437;&#x443;&#x43c;&#x456;&#x43d;&#x43d;&#x44f;" STYLE_REF="pre_last_node" ID="ID_1435964187" CREATED="1418588610216" MODIFIED="1418629164351"/>
<node TEXT="&#x421;&#x442;&#x440;&#x430;&#x436;&#x434;&#x430;&#x44e;&#x442;&#x44c; &#x43d;&#x435; &#x442;&#x456;&#x43b;&#x44c;&#x43a;&#x438; &#x432;&#x43e;&#x43d;&#x438;" STYLE_REF="pre_last_node" ID="ID_1090146184" CREATED="1418588625164" MODIFIED="1427365966260"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1044;&#1072;&#1090;&#1080; &#1111;&#1084; &#1094;&#1077; &#1079;&#1088;&#1086;&#1079;&#1091;&#1084;&#1110;&#1090;&#1080;
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="&#x41d;&#x435;&#x433;&#x430;&#x442;&#x438;&#x432;&#x43d;&#x456; &#x43f;&#x43e;&#x447;&#x443;&#x442;&#x442;&#x44f; &#x446;&#x435; &#x43d;&#x43e;&#x440;&#x43c;&#x430;&#x43b;&#x44c;&#x43d;&#x43e;" STYLE_REF="pre_last_node" ID="ID_716328629" CREATED="1418629178256" MODIFIED="1418629191100"/>
</node>
<node TEXT="&#x41f;&#x43e;&#x43c;&#x438;&#x43b;&#x43a;&#x438; &#x431;&#x430;&#x442;&#x44c;&#x43a;&#x456;&#x432;" STYLE_REF="yellow_node" ID="ID_434114746" CREATED="1418588600227" MODIFIED="1418628841690">
<icon BUILTIN="button_cancel"/>
<node TEXT="&#x421;&#x43f;&#x440;&#x43e;&#x431;&#x443;&#x432;&#x430;&#x442;&#x438; &#x440;&#x43e;&#x437;&#x432;&#x435;&#x441;&#x435;&#x43b;&#x438;&#x442;&#x44c;" STYLE_REF="pre_last_node" ID="ID_404211534" CREATED="1418588737783" MODIFIED="1418628896444"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1076;&#1080;&#1090;&#1080;&#1085;&#1091; &#1082;&#1086;&#1083;&#1080; &#1074;&#1086;&#1085;&#1072; &#1078;&#1072;&#1083;&#1091;&#1108;&#1090;&#1100;&#1089;&#1103;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&#x41f;&#x43e;&#x43a;&#x430;&#x437;&#x430;&#x442;&#x438; &#x449;&#x43e; &#x446;&#x435; &#x434;&#x440;&#x456;&#x431;&#x43d;&#x438;&#x446;&#x456;" STYLE_REF="pre_last_node" ID="ID_1643759904" CREATED="1418588749646" MODIFIED="1418629243857">
<node TEXT="&#x41f;&#x456;&#x434; &#x447;&#x430;&#x441; &#x457;&#x457; &#x436;&#x430;&#x43b;&#x43e;&#x431; &#x43f;&#x43e;&#x43a;&#x430;&#x437;&#x430;&#x442;&#x438; &#x449;&#x43e; &#x446;&#x435; &#x434;&#x440;&#x456;&#x431;&#x43d;&#x438;&#x446;&#x456;&#xa; &#x456; &#x43f;&#x43e; &#x446;&#x44c;&#x43e;&#x43c;&#x443; &#x43f;&#x43e;&#x432;&#x43e;&#x434;&#x443; &#x43d;&#x435; &#x432;&#x430;&#x440;&#x442;&#x43e; &#x442;&#x430;&#x43a; &#x437;&#x430;&#x441;&#x43c;&#x443;&#x447;&#x443;&#x432;&#x430;&#x442;&#x438;&#x441;&#x44c;." ID="ID_639042199" CREATED="1418629224181" MODIFIED="1418629273538"/>
<node TEXT="&#x41f;&#x456;&#x441;&#x43b;&#x44f; &#x442;&#x43e;&#x433;&#x43e; &#x44f;&#x43a; &#x434;&#x438;&#x442;&#x438;&#x43d;&#x430; &#x437;&#x430;&#x441;&#x43f;&#x43e;&#x43a;&#x43e;&#x457;&#x43b;&#x430;&#x441;&#x44c; &#x437;&#x433;&#x430;&#x434;&#x443;&#x432;&#x430;&#x442;&#x44c; &#x457;&#x457; &#x433;&#x43e;&#x440;&#x435;&#x441;&#x442;&#x456;&#xa;&#x456; &#x43f;&#x43e;&#x43a;&#x430;&#x437;&#x443;&#x432;&#x430;&#x442;&#x438; &#x449;&#x43e; &#x442;&#x43e; &#x431;&#x443;&#x43b;&#x438; &#x434;&#x440;&#x456;&#x431;&#x43d;&#x438;&#x446;&#x456;" ID="ID_1553281188" CREATED="1418629230135" MODIFIED="1418629241618"/>
</node>
<node TEXT="&#x414;&#x443;&#x43c;&#x430;&#x442;&#x438; &#x449;&#x43e; &#x437; &#x434;&#x438;&#x442;&#x438;&#x43d;&#x43e;&#x44e; &#x449;&#x43e;&#x441;&#x44c; &#x43d;&#x435; &#x442;&#x430;&#x43a;" STYLE_REF="pre_last_node" ID="ID_1706709583" CREATED="1418588884319" MODIFIED="1418629212917"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1047; &#1085;&#1077;&#1102; &#1074;&#1089;&#1077; &#1090;&#1072;&#1082;! &#1055;&#1088;&#1086;&#1089;&#1090;&#1086; &#1074; &#1085;&#1077;&#1111; &#1090;&#1072;&#1082;&#1072; &#1087;&#1088;&#1080;&#1088;&#1086;&#1076;&#1072;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="&#x410;&#x43a;&#x442;&#x438;&#x432;&#x43d;&#x438;&#x439; &#x442;&#x438;&#x43f;" STYLE_REF="orange_node" ID="ID_1838875768" CREATED="1418588169675" MODIFIED="1427365301099" VGAP="13"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1042;&#1086;&#1075;&#1086;&#1085;&#1100;
    </p>
  </body>
</html>
</richcontent>
<hook URI="../../Images/fire.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x41e;&#x441;&#x43e;&#x431;&#x43b;&#x438;&#x432;&#x43e;&#x441;&#x442;&#x456;" STYLE_REF="yellow_node" ID="ID_1442023384" CREATED="1418627251964" MODIFIED="1418628841694">
<icon BUILTIN="list"/>
<node TEXT="&#x414;&#x456;&#x442;&#x438; - &#x43b;&#x456;&#x434;&#x435;&#x440;&#x438;" STYLE_REF="pre_last_node" ID="ID_1201068894" CREATED="1418627307483" MODIFIED="1418628483685"/>
<node TEXT="&#x412; &#x43d;&#x438;&#x445; &#x434;&#x443;&#x436;&#x435; &#x431;&#x430;&#x433;&#x430;&#x442;&#x43e; &#x435;&#x43d;&#x435;&#x440;&#x433;&#x456;&#x457;" STYLE_REF="pre_last_node" ID="ID_722539413" CREATED="1418627318905" MODIFIED="1418628483656"/>
<node TEXT="&#x417;&#x430;&#x432;&#x436;&#x434;&#x438; &#x445;&#x43e;&#x447;&#x435; &#x43d;&#x430;&#x43b;&#x435;&#x436;&#x430;&#x442;&#x44c; &#x43a;&#x43e;&#x43c;&#x430;&#x43d;&#x434;&#x456; &#x43f;&#x435;&#x440;&#x435;&#x43c;&#x43e;&#x436;&#x446;&#x456;&#x432;" STYLE_REF="pre_last_node" ID="ID_304635107" CREATED="1418628620585" MODIFIED="1418628637504"/>
</node>
<node TEXT="&#x414;&#x43e;&#x43f;&#x43e;&#x43c;&#x43e;&#x433;&#x430;" STYLE_REF="yellow_node" ID="ID_828276622" CREATED="1418627261366" MODIFIED="1418628841699">
<icon BUILTIN="button_ok"/>
<node TEXT="&#x41f;&#x43b;&#x430;&#x43d; &#x434;&#x456;&#x457;" STYLE_REF="pre_last_node" ID="ID_710284886" CREATED="1418627327887" MODIFIED="1418628483668"/>
<node TEXT="&#x41f;&#x440;&#x430;&#x432;&#x438;&#x43b;&#x430; &#x433;&#x440;&#x438;" STYLE_REF="pre_last_node" ID="ID_454422626" CREATED="1418627335167" MODIFIED="1418628483677"/>
<node TEXT="&#x411;&#x430;&#x442;&#x44c;&#x43a;&#x438; - &#x433;&#x43e;&#x43b;&#x43e;&#x432;&#x43d;&#x456;" STYLE_REF="pre_last_node" ID="ID_544937626" CREATED="1418627339252" MODIFIED="1418628483692"/>
<node TEXT="&#x414;&#x430;&#x442;&#x44c; &#x439;&#x43e;&#x43c;&#x443; &#x43c;&#x43e;&#x436;&#x43b;&#x438;&#x432;&#x456;&#x441;&#x442;&#x44c; &#x43a;&#x435;&#x440;&#x443;&#x432;&#x430;&#x442;&#x44c;" STYLE_REF="pre_last_node" ID="ID_1538923698" CREATED="1418627348565" MODIFIED="1418628483700">
<node TEXT="&#x406; &#x43f;&#x43e;&#x43a;&#x43b;&#x430;&#x441;&#x442;&#x438; &#x43d;&#x430; &#x439;&#x43e;&#x433;&#x43e; &#x44f;&#x43a;&#x443;&#x441;&#x44c; &#x432;&#x456;&#x434;&#x43f;&#x43e;&#x432;&#x456;&#x434;&#x430;&#x43b;&#x44c;&#x43d;&#x456;&#x441;&#x442;&#x44c;" STYLE_REF="last_node" ID="ID_771877177" CREATED="1418627829135" MODIFIED="1418627845132"/>
</node>
<node TEXT="&#x429;&#x43e;&#x431; &#x43f;&#x43e;&#x441;&#x442;&#x456;&#x439;&#x43d;&#x43e; &#x431;&#x443;&#x432; &#x437;&#x430;&#x434;&#x456;&#x44f;&#x43d;&#x438;&#x439; &#x432; &#x44f;&#x43a;&#x43e;&#x43c;&#x443;&#x441;&#x44c; &#x434;&#x456;&#x43b;&#x456;" STYLE_REF="pre_last_node" ID="ID_1285125454" CREATED="1418627359964" MODIFIED="1418628483707"/>
<node TEXT="&#x41f;&#x440;&#x438;&#x437;&#x43d;&#x430;&#x432;&#x430;&#x442;&#x44c; &#x457;&#x445;&#x43d;&#x456; &#x443;&#x441;&#x43f;&#x456;&#x445;&#x456;, &#x43f;&#x440;&#x43e;&#x449;&#x430;&#x442;&#x44c; &#x43f;&#x43e;&#x43c;&#x438;&#x43b;&#x43a;&#x438;" STYLE_REF="pre_last_node" ID="ID_1160714984" CREATED="1418627390869" MODIFIED="1418628483716"/>
<node TEXT="&#x424;&#x43e;&#x440;&#x43c;&#x443;&#x43b;&#x430;: &#x42f; &#x445;&#x43e;&#x447;&#x443; &#x449;&#x43e;&#x431; &#x442;&#x438; &#x437;&#x440;&#x43e;&#x431;&#x438;&#x432; &#x442;&#x43e;&#x442;&#x43e;!" STYLE_REF="pre_last_node" ID="ID_1600392607" CREATED="1418628962476" MODIFIED="1418629095171">
<font BOLD="true"/>
<node TEXT="&#x422;&#x430;&#x43a;&#x456; &#x441;&#x43b;&#x43e;&#x432;&#x430; &#x437;&#x432;&#x43e;&#x434;&#x44f;&#x442;&#x44c; &#x441;&#x443;&#x43f;&#x440;&#x43e;&#x442;&#x438;&#x432; &#x434;&#x43e; &#x43c;&#x456;&#x43d;&#x456;&#x43c;&#x443;&#x43c;&#x443;.&#xa;&#x422;&#x43e;&#x43c;&#x443;&#x449;&#x43e; &#x43d;&#x430;&#x43f;&#x43e;&#x43c;&#x438;&#x43d;&#x430;&#x44e;&#x442;&#x44c; &#x439;&#x43e;&#x43c;&#x443; &#x445;&#x442;&#x43e; &#x433;&#x43e;&#x43b;&#x43e;&#x432;&#x43d;&#x438;&#x439;" STYLE_REF="last_node" ID="ID_1951333899" CREATED="1418628983707" MODIFIED="1418629036904"/>
</node>
</node>
<node TEXT="&#x41f;&#x43e;&#x43c;&#x438;&#x43b;&#x43a;&#x438;" STYLE_REF="yellow_node" ID="ID_1214521132" CREATED="1418627266107" MODIFIED="1418628841704">
<icon BUILTIN="button_cancel"/>
<node TEXT="&#x414;&#x43e;&#x432;&#x433;&#x456; &#x431;&#x435;&#x441;&#x456;&#x434;&#x438; &#x437; &#x43d;&#x438;&#x43c;&#x438;" STYLE_REF="pre_last_node" ID="ID_282070970" CREATED="1418627411522" MODIFIED="1418628483721"/>
<node TEXT="&#x41f;&#x438;&#x442;&#x430;&#x442;&#x44c; &#x443; &#x43d;&#x438;&#x445; &#x43f;&#x440;&#x44f;&#x43c;&#x43e; &#x449;&#x43e; &#x432;&#x43e;&#x43d;&#x438; &#x445;&#x43e;&#x447;&#x443;&#x442;&#x44c;" STYLE_REF="pre_last_node" ID="ID_366017349" CREATED="1418627423660" MODIFIED="1418628483728">
<node TEXT="&#x41f;&#x440;&#x438; &#x446;&#x44c;&#x43e;&#x43c;&#x443; &#x431;&#x430;&#x442;&#x44c;&#x43a;&#x438; &#x432;&#x442;&#x440;&#x430;&#x447;&#x430;&#x44e;&#x442;&#x44c; &#x430;&#x432;&#x442;&#x43e;&#x440;&#x438;&#x442;&#x435;&#x442;" STYLE_REF="last_node" ID="ID_643548068" CREATED="1418627458263" MODIFIED="1418629068366"/>
</node>
<node TEXT="&#x41f;&#x43e;&#x43f;&#x440;&#x430;&#x432;&#x43b;&#x44f;&#x442;&#x44c; &#x447;&#x438; &#x43a;&#x440;&#x438;&#x442;&#x438;&#x43a;&#x443;&#x432;&#x430;&#x442;&#x44c; &#x457;&#x445; &#x43d;&#x430; &#x43b;&#x44e;&#x434;&#x44f;&#x445;" STYLE_REF="pre_last_node" ID="ID_1778212636" CREATED="1418627472421" MODIFIED="1418628483735">
<node TEXT="&#x41a;&#x440;&#x430;&#x449;&#x435; &#x43f;&#x440;&#x438;&#x434;&#x443;&#x43c;&#x430;&#x442;&#x44c; &#x441;&#x438;&#x441;&#x442;&#x435;&#x43c;&#x443; &#x441;&#x438;&#x433;&#x43d;&#x430;&#x43b;&#x456;&#x432; &#x456; &#x437;&#x43d;&#x430;&#x43a;&#x456;&#x432;.&#xa;&#x429;&#x43e;&#x431; &#x43d;&#x430; &#x43b;&#x44e;&#x434;&#x44f;&#x445; &#x43c;&#x43e;&#x436;&#x43d;&#x430; &#x431;&#x443;&#x43b;&#x43e; &#x43f;&#x43e;&#x43f;&#x440;&#x430;&#x432;&#x438;&#x442;&#x44c; &#x434;&#x438;&#x442;&#x438;&#x43d;&#x443;.&#xa;&#x412;&#x43e;&#x43d;&#x430; &#x431;&#x443;&#x434;&#x435; &#x434;&#x443;&#x436;&#x435; &#x432;&#x434;&#x44f;&#x447;&#x43d;&#x430; &#x437;&#x430; &#x446;&#x435;." STYLE_REF="last_node" ID="ID_1886935376" CREATED="1418627491904" MODIFIED="1418627535316"/>
</node>
</node>
</node>
<node TEXT="&#x420;&#x435;&#x430;&#x43a;&#x442;&#x438;&#x432;&#x43d;&#x438;&#x439; &#x442;&#x438;&#x43f;" STYLE_REF="orange_node" ID="ID_1065528884" CREATED="1418624142059" MODIFIED="1427365340445" VGAP="13"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1055;&#1086;&#1074;&#1110;&#1090;&#1088;&#1103;
    </p>
  </body>
</html>
</richcontent>
<hook URI="../../Images/wind.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x41e;&#x441;&#x43e;&#x431;&#x43b;&#x438;&#x432;&#x43e;&#x441;&#x442;&#x456;" STYLE_REF="yellow_node" ID="ID_891340654" CREATED="1418626267771" MODIFIED="1418628841708">
<icon BUILTIN="list"/>
<edge WIDTH="thin"/>
<node TEXT="&#x41b;&#x44e;&#x431;&#x43b;&#x44f;&#x442;&#x44c; &#x437;&#x43c;&#x456;&#x43d;&#x438;" STYLE_REF="pre_last_node" ID="ID_1055466406" CREATED="1418626317473" MODIFIED="1418628509551"/>
<node TEXT="&#x41f;&#x440;&#x43e;&#x442;&#x438;&#x432;&#x43b;&#x44f;&#x442;&#x44c;&#x441;&#x44f; &#x43d;&#x435;&#x43e;&#x431;&#x445;&#x456;&#x434;&#x43d;&#x43e;&#x441;&#x442;&#x456; &#x437;&#x43e;&#x441;&#x435;&#x440;&#x435;&#x434;&#x438;&#x442;&#x438;&#x441;&#x44c;" STYLE_REF="pre_last_node" ID="ID_446055662" CREATED="1418626395612" MODIFIED="1418628509544"/>
<node TEXT="&#x41b;&#x44e;&#x431;&#x43b;&#x44f;&#x442;&#x44c; &#x445;&#x430;&#x43e;&#x441;" STYLE_REF="pre_last_node" ID="ID_1713965813" CREATED="1418626416931" MODIFIED="1418628509538"/>
<node TEXT="&#x420;&#x443;&#x442;&#x438;&#x43d;&#x430; &#x457;&#x445; &#x432;&#x431;&#x438;&#x432;&#x430;&#x454;" STYLE_REF="pre_last_node" ID="ID_1932210023" CREATED="1418626425872" MODIFIED="1418628509533"/>
</node>
<node TEXT="&#x414;&#x43e;&#x43f;&#x43e;&#x43c;&#x43e;&#x433;&#x430;" STYLE_REF="yellow_node" ID="ID_693620138" CREATED="1418626297780" MODIFIED="1418628841713">
<icon BUILTIN="button_ok"/>
<edge WIDTH="1"/>
<node TEXT="&#x412;&#x456;&#x434;&#x432;&#x43e;&#x43b;&#x456;&#x43a;&#x430;&#x442;&#x44c; &#x456; &#x43d;&#x430;&#x43f;&#x440;&#x430;&#x432;&#x43b;&#x44f;&#x442;&#x44c;" STYLE_REF="pre_last_node" ID="ID_1997184973" CREATED="1418626458623" MODIFIED="1418628509514"/>
<node TEXT="&#x41f;&#x43e;&#x434;&#x430;&#x440;&#x438;&#x442;&#x44c; &#x43f;&#x456;&#x441;&#x43d;&#x44e;.&#xa;&#x41f;&#x456;&#x441;&#x43d;&#x44f; &#x43f;&#x456;&#x434; &#x447;&#x430;&#x441; &#x440;&#x443;&#x442;&#x438;&#x43d;&#x438;" STYLE_REF="pre_last_node" ID="ID_725392633" CREATED="1418626482505" MODIFIED="1418628509495"/>
<node TEXT="&#x414;&#x443;&#x436;&#x435; &#x43d;&#x435; &#x43d;&#x430;&#x433;&#x440;&#x443;&#x436;&#x430;&#x442;&#x44c; &#x434;&#x456;&#x442;&#x435;&#x439; &#x431;&#x438;&#x442;&#x43e;&#x43c; &#x434;&#x43e; &#x441;&#x442;&#x430;&#x442;&#x435;&#x432;&#x43e;&#x433;&#x43e; &#x434;&#x43e;&#x437;&#x440;&#x456;&#x432;&#x430;&#x43d;&#x43d;&#x44f;" STYLE_REF="pre_last_node" ID="ID_744009018" CREATED="1418626500525" MODIFIED="1418628509486"/>
<node TEXT="&#x427;&#x438;&#x442;&#x430;&#x442;&#x44c; &#x43f;&#x435;&#x440;&#x435;&#x434; &#x441;&#x43d;&#x43e;&#x43c; &#x43a;&#x430;&#x437;&#x43a;&#x438;, &#x43b;&#x435;&#x433;&#x435;&#x43d;&#x434;&#x438;" STYLE_REF="pre_last_node" ID="ID_1244604088" CREATED="1418626521696" MODIFIED="1418628509477"/>
<node TEXT="&#x41f;&#x440;&#x438;&#x431;&#x438;&#x440;&#x430;&#x442;&#x44c; &#x432; &#x439;&#x43e;&#x433;&#x43e; &#x43a;&#x456;&#x43c;&#x43d;&#x430;&#x442;&#x456; &#x440;&#x430;&#x437;&#x43e;&#x43c;" STYLE_REF="pre_last_node" ID="ID_582136953" CREATED="1418626539553" MODIFIED="1418628509472"/>
<node TEXT="&#x424;&#x43e;&#x440;&#x43c;&#x443;&#x43b;&#x430;: &#x430; &#x442;&#x435;&#x43f;&#x435;&#x440; &#x434;&#x430;&#x432;&#x430;&#x439; &#x437;&#x440;&#x43e;&#x431;&#x438;&#x43c; &#x442;&#x43e;&#x442;&#x43e;" STYLE_REF="pre_last_node" ID="ID_695424528" CREATED="1418626553063" MODIFIED="1418628509465">
<font BOLD="true"/>
</node>
</node>
<node TEXT="&#x41f;&#x43e;&#x43c;&#x438;&#x43b;&#x43a;&#x438;" STYLE_REF="yellow_node" ID="ID_736333652" CREATED="1418626305930" MODIFIED="1418628841717">
<icon BUILTIN="button_cancel"/>
<edge WIDTH="1"/>
<node TEXT="&#x41d;&#x430;&#x432;&#x430;&#x43b;&#x44e;&#x432;&#x430;&#x442;&#x44c; &#x43d;&#x430; &#x457;&#x445; &#x431;&#x430;&#x433;&#x430;&#x442;&#x43e; &#x440;&#x443;&#x442;&#x438;&#x43d;&#x438;" STYLE_REF="pre_last_node" ID="ID_32134245" CREATED="1418626626154" MODIFIED="1418628509453"/>
<node TEXT="&#x41f;&#x438;&#x442;&#x430;&#x442;&#x44c; &#x443; &#x43d;&#x438;&#x445; &#x449;&#x43e; &#x432;&#x43e;&#x43d;&#x438; &#x445;&#x43e;&#x447;&#x443;&#x442;&#x44c;" STYLE_REF="pre_last_node" ID="ID_1553372801" CREATED="1418626644458" MODIFIED="1418628509447">
<node TEXT="&#x41a;&#x440;&#x430;&#x449;&#x435; &#x43f;&#x440;&#x43e;&#x43f;&#x43e;&#x43d;&#x443;&#x432;&#x430;&#x442;&#x44c; &#x457;&#x43c; &#x440;&#x456;&#x437;&#x43d;&#x443; &#x434;&#x456;&#x44f;&#x43b;&#x44c;&#x43d;&#x456;&#x441;&#x442;&#x44c;.&#xa;&#x410; &#x432;&#x43e;&#x43d;&#x438; &#x441;&#x432;&#x43e;&#x457;&#x43c;&#x438; &#x432;&#x456;&#x434;&#x433;&#x443;&#x43a;&#x430;&#x43c;&#x438; &#x434;&#x430;&#x434;&#x443;&#x442;&#x44c; &#x442;&#x43e;&#x431;&#x456; &#x43f;&#x456;&#x434;&#x43a;&#x430;&#x437;&#x43a;&#x443;" STYLE_REF="last_node" ID="ID_376696611" CREATED="1418626690664" MODIFIED="1418626748904"/>
</node>
</node>
</node>
<node TEXT="&#x427;&#x443;&#x442;&#x43b;&#x438;&#x432;&#x438;&#x439; &#x442;&#x438;&#x43f;" ID="ID_1808726194" CREATED="1418624162937" MODIFIED="1427365263645" BACKGROUND_COLOR="#ff9900" VGAP="13">
<font BOLD="true"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1047;&#1077;&#1084;&#1083;&#1103;
    </p>
  </body>
</html>
</richcontent>
<hook URI="../../Images/earth-1.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x41e;&#x441;&#x43e;&#x431;&#x43b;&#x438;&#x432;&#x43e;&#x441;&#x442;&#x456;" STYLE_REF="yellow_node" ID="ID_1010523889" CREATED="1418624407980" MODIFIED="1418628841721">
<icon BUILTIN="list"/>
<node TEXT="&#x41f;&#x440;&#x43e;&#x442;&#x438;&#x432;&#x438;&#x442;&#x44c;&#x441;&#x44f; &#x431;&#x443;&#x434;&#x44c;-&#x44f;&#x43a;&#x438;&#x43c; &#x437;&#x43c;&#x456;&#x43d;&#x430;&#x43c;" STYLE_REF="pre_last_node" ID="ID_1816790493" CREATED="1418624419656" MODIFIED="1418628509440"/>
<node TEXT="&#x425;&#x43e;&#x447;&#x435; &#x437;&#x43d;&#x430;&#x442;&#x438; &#x447;&#x43e;&#x433;&#x43e; &#x447;&#x435;&#x43a;&#x430;&#x442;&#x438; &#x432;&#x456;&#x434; &#x43c;&#x430;&#x439;&#x431;&#x443;&#x442;&#x43d;&#x44c;&#x43e;&#x433;&#x43e;" STYLE_REF="pre_last_node" ID="ID_102358473" CREATED="1418624436222" MODIFIED="1418628509433"/>
<node TEXT="&#x412;&#x43e;&#x43d;&#x438; &#x434;&#x43e;&#x431;&#x440;&#x43e;&#x434;&#x443;&#x448;&#x43d;&#x456; &#x456; &#x432;&#x434;&#x443;&#x43c;&#x447;&#x438;&#x432;&#x456;" STYLE_REF="pre_last_node" ID="ID_1331590100" CREATED="1418624453635" MODIFIED="1418628509425"/>
<node TEXT="&#x412;&#x43e;&#x43d;&#x438; &#x43f;&#x440;&#x438;&#x439;&#x43c;&#x430;&#x44e;&#x442;&#x44c; &#x443;&#x447;&#x430;&#x441;&#x442;&#x44c; &#x432; &#x456;&#x433;&#x440;&#x430;&#x445; &#x43d;&#x430;&#x431;&#x43b;&#x44e;&#x434;&#x430;&#x44e;&#x447;&#x438;" STYLE_REF="pre_last_node" ID="ID_1923990387" CREATED="1418624472250" MODIFIED="1418628509418"/>
</node>
<node TEXT="&#x414;&#x43e;&#x43f;&#x43e;&#x43c;&#x43e;&#x433;&#x430;" STYLE_REF="yellow_node" ID="ID_846252817" CREATED="1418624524152" MODIFIED="1418628841726">
<icon BUILTIN="button_ok"/>
<node TEXT="&#x41f;&#x440;&#x430;&#x43a;&#x442;&#x438;&#x447;&#x43d;&#x456; &#x440;&#x438;&#x442;&#x443;&#x430;&#x43b;&#x438;" STYLE_REF="pre_last_node" ID="ID_170581893" CREATED="1418624569788" MODIFIED="1418628509406">
<node TEXT="&#x417;&#x430; &#x441;&#x442;&#x43e;&#x43b;&#x43e;&#x43c; &#x443; &#x43d;&#x44c;&#x43e;&#x433;&#x43e; &#x43e;&#x434;&#x43d;&#x435; &#x43c;&#x456;&#x441;&#x446;&#x435;" STYLE_REF="last_node" ID="ID_1163543929" CREATED="1418625508609" MODIFIED="1418625588601"/>
<node TEXT="&#x414;&#x438;&#x442;&#x438;&#x43d;&#x443; &#x431;&#x443;&#x434;&#x44f;&#x442;&#x44c; &#x434;&#x43e; &#x448;&#x43a;&#x43e;&#x43b;&#x438; &#x432; &#x43e;&#x434;&#x438;&#x43d; &#x456; &#x442;&#x43e;&#x439; &#x441;&#x430;&#x43c;&#x438;&#x439; &#x447;&#x430;&#x441;" STYLE_REF="last_node" ID="ID_1589842219" CREATED="1418625517824" MODIFIED="1418625588598"/>
<node TEXT="&#x41d;&#x435;&#x445;&#x430;&#x439; &#x434;&#x438;&#x442;&#x438;&#x43d;&#x430; &#x433;&#x43e;&#x442;&#x443;&#x454; &#x43e;&#x434;&#x44f;&#x433; &#x434;&#x43e; &#x448;&#x43a;&#x43e;&#x43b;&#x438; &#x43d;&#x430;&#x43f;&#x435;&#x440;&#x435;&#x434;&#x43e;&#x434;&#x43d;&#x456;" STYLE_REF="last_node" ID="ID_1462063578" CREATED="1418625547445" MODIFIED="1418625588596"/>
<node TEXT="&#x414;&#x438;&#x442;&#x438;&#x43d;&#x443; &#x437;&#x430;&#x431;&#x438;&#x440;&#x430;&#x44e;&#x442;&#x44c; &#x456;&#x437; &#x448;&#x43a;&#x43e;&#x43b;&#x438; &#x432; &#x43e;&#x434;&#x438;&#x43d; &#x456; &#x442;&#x43e;&#x439; &#x441;&#x430;&#x43c;&#x438;&#x439; &#x447;&#x430;&#x441;" STYLE_REF="last_node" ID="ID_1872758261" CREATED="1418625565450" MODIFIED="1418625588588"/>
</node>
<node TEXT="&#x420;&#x438;&#x442;&#x443;&#x430;&#x43b;&#x438; &#x43b;&#x44e;&#x431;&#x43e;&#x432;&#x456;" STYLE_REF="pre_last_node" ID="ID_1021929466" CREATED="1418624592606" MODIFIED="1418628509396">
<node TEXT="&#x423; &#x43d;&#x435;&#x434;&#x456;&#x43b;&#x44e; &#x442;&#x430;&#x442;&#x43e; &#x433;&#x43e;&#x442;&#x43e;&#x432;&#x438;&#x442;&#x44c; &#x441;&#x43d;&#x456;&#x434;&#x430;&#x43d;&#x43e;&#x43a;" STYLE_REF="last_node" ID="ID_1118961425" CREATED="1418625394627" MODIFIED="1418625480794"/>
<node TEXT="&#x41f;&#x435;&#x440;&#x435;&#x434; &#x441;&#x43d;&#x43e;&#x43c; &#x445;&#x442;&#x43e;&#x441;&#x44c; &#x456;&#x437; &#x431;&#x430;&#x442;&#x44c;&#x43a;&#x456;&#x432; &#x447;&#x438;&#x442;&#x430;&#x454; &#x43a;&#x430;&#x437;&#x43a;&#x443;" STYLE_REF="last_node" ID="ID_1880384621" CREATED="1418625408778" MODIFIED="1418625480799"/>
<node TEXT="&#x412;&#x435;&#x441;&#x43d;&#x43e;&#x44e; &#x432;&#x441;&#x44f; &#x441;&#x456;&#x43c;&apos;&#x44f; &#x439;&#x434;&#x435; &#x437;&#x431;&#x438;&#x440;&#x430;&#x442;&#x438; &#x43f;&#x456;&#x434;&#x441;&#x43d;&#x456;&#x436;&#x43d;&#x438;&#x43a;&#x438;" STYLE_REF="last_node" ID="ID_854734690" CREATED="1418625441049" MODIFIED="1418625480802"/>
</node>
<node TEXT="&#x424;&#x43e;&#x440;&#x43c;&#x443;&#x43b;&#x430;: &#x417;&#x430;&#x440;&#x430;&#x437; &#x43f;&#x440;&#x438;&#x439;&#x448;&#x43e;&#x432; &#x447;&#x430;&#x441; &#x437;&#x440;&#x43e;&#x431;&#x438;&#x442;&#x44c; &#x442;&#x43e;&#x442;&#x43e;" STYLE_REF="pre_last_node" ID="ID_759753425" CREATED="1418624604240" MODIFIED="1418628509387">
<font BOLD="true"/>
</node>
<node TEXT="&#x417;&#x430;&#x431;&#x435;&#x437;&#x43f;&#x435;&#x447;&#x438;&#x442;&#x44c; &#x457;&#x43c; &#x440;&#x435;&#x436;&#x438;&#x43c;, &#x43f;&#x43e;&#x432;&#x442;&#x43e;&#x440;&#x44e;&#x432;&#x430;&#x43d;&#x456;&#x441;&#x442;&#x44c; &#x456; &#x440;&#x438;&#x442;&#x43c;" STYLE_REF="pre_last_node" ID="ID_328168878" CREATED="1418624629352" MODIFIED="1418628509379"/>
<node TEXT="&#x422;&#x43e;&#x431;&#x456; &#x43f;&#x43e;&#x440;&#x430; &#x43f;&#x440;&#x438;&#x454;&#x434;&#x43d;&#x430;&#x442;&#x438;&#x441;&#x44c;!" STYLE_REF="pre_last_node" ID="ID_1968476673" CREATED="1418625158963" MODIFIED="1418628509371"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1047;&#1072;&#1084;&#1110;&#1089;&#1090;&#1100;: &#1095;&#1080; &#1085;&#1077;&#1093;&#1086;&#1095;&#1077;&#1096; &#1090;&#1080; &#1087;&#1088;&#1080;&#1108;&#1076;&#1085;&#1072;&#1090;&#1080;&#1089;&#1100;?
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="&#x41f;&#x43e;&#x43c;&#x438;&#x43b;&#x43a;&#x438;" STYLE_REF="yellow_node" ID="ID_879319550" CREATED="1418624771703" MODIFIED="1418628841731">
<icon BUILTIN="button_cancel"/>
<node TEXT="&#x41d;&#x435; &#x441;&#x442;&#x430;&#x432;&#x438;&#x442;&#x44c; &#x437;&#x430;&#x434;&#x430;&#x447; &#x456; &#x43d;&#x435; &#x443;&#x447;&#x430;&#x441;&#x442;&#x432;&#x443;&#x432;&#x430;&#x442;&#x44c; &#x432; &#x457;&#x445;&#x43d;&#x43e;&#x43c;&#x443; &#x436;&#x438;&#x442;&#x442;&#x456;" STYLE_REF="pre_last_node" ID="ID_511442332" CREATED="1418624782245" MODIFIED="1418628509363">
<node TEXT="&#x411;&#x435;&#x437; &#x434;&#x43e;&#x441;&#x442;&#x430;&#x442;&#x43d;&#x44c;&#x43e;&#x457; &#x43f;&#x456;&#x434;&#x442;&#x440;&#x438;&#x43c;&#x43a;&#x438; &#x432;&#x456;&#x43d; &#x43c;&#x43e;&#x436;&#x435; &#x432;&#x437;&#x430;&#x433;&#x430;&#x43b;&#x456;&#xa;&#x43d;&#x435; &#x43f;&#x440;&#x438;&#x434;&#x431;&#x430;&#x442;&#x438; &#x436;&#x43e;&#x434;&#x43d;&#x438;&#x445; &#x456;&#x43d;&#x442;&#x435;&#x440;&#x435;&#x441;&#x456;&#x432;" STYLE_REF="last_node" ID="ID_1160833994" CREATED="1418624973658" MODIFIED="1418625470638"/>
</node>
<node TEXT="&#x41f;&#x435;&#x440;&#x435;&#x440;&#x438;&#x432;&#x430;&#x442;&#x44c; &#x457;&#x445; &#x43f;&#x456;&#x434; &#x447;&#x430;&#x441; &#x440;&#x43e;&#x431;&#x43e;&#x442;&#x438;" STYLE_REF="pre_last_node" ID="ID_712164852" CREATED="1418624804894" MODIFIED="1418628509357"/>
<node TEXT="&#x41f;&#x456;&#x434;&#x433;&#x430;&#x43d;&#x44f;&#x442;&#x44c; &#x457;&#x445; &#x449;&#x43e;&#x441;&#x44c; &#x437;&#x440;&#x43e;&#x431;&#x438;&#x442;&#x438; &#x447;&#x438; &#x43f;&#x440;&#x438;&#x439;&#x43d;&#x44f;&#x442;&#x438; &#x443;&#x447;&#x430;&#x441;&#x442;&#x44c; &#x443; &#x447;&#x43e;&#x43c;&#x443;&#x441;&#x44c;" STYLE_REF="pre_last_node" ID="ID_724374496" CREATED="1418624832374" MODIFIED="1418628509344"/>
</node>
</node>
</node>
<node TEXT="&#x423;&#x43d;&#x456;&#x432;&#x435;&#x440;&#x441;&#x430;&#x43b;&#x44c;&#x43d;&#x435; &#x43f;&#x43e;&#x434;&#x43e;&#x43b;&#x430;&#x43d;&#x43d;&#x44f; &#x43e;&#x43f;&#x43e;&#x440;&#x443;" STYLE_REF="second_node" POSITION="left" ID="ID_222777888" CREATED="1418638779364" MODIFIED="1427365462443" VGAP="13">
<hook URI="../../Images/resistance-1.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x41f;&#x440;&#x438;&#x447;&#x438;&#x43d;&#x438; &#x43e;&#x43f;&#x43e;&#x440;&#x443;" STYLE_REF="orange_node" ID="ID_229636681" CREATED="1418638823778" MODIFIED="1427365984888">
<icon BUILTIN="help"/>
<node TEXT="&#x412; &#x434;&#x438;&#x442;&#x438;&#x43d;&#x438; &#x454; &#x43d;&#x435;&#x437;&#x430;&#x434;&#x43e;&#x432;&#x43e;&#x43b;&#x435;&#x43d;&#x430; &#x43f;&#x43e;&#x442;&#x440;&#x435;&#x431;&#x430;/&#x431;&#x430;&#x436;&#x430;&#x43d;&#x43d;&#x44f;" STYLE_REF="pre_last_node" ID="ID_1912718805" CREATED="1418638914046" MODIFIED="1418638947335"/>
<node TEXT="&#x414;&#x438;&#x442;&#x438;&#x43d;&#x456; &#x437;&#x434;&#x430;&#x454;&#x442;&#x44c;&#x441;&#x44f;, &#x449;&#x43e; &#x457;&#x457; &#x43d;&#x435;&#x447;&#x443;&#x44e;&#x442;&#x44c;,&#xa; &#x456; &#x43d;&#x435;&#x440;&#x43e;&#x437;&#x443;&#x43c;&#x443;&#x44e;&#x442;&#x44c; &#x457;&#x457; &#x43f;&#x43e;&#x442;&#x440;&#x435;&#x431;&#x438;" STYLE_REF="pre_last_node" ID="ID_1457271238" CREATED="1418638949837" MODIFIED="1418638989891"/>
<node TEXT="&#x417;&#x430;&#x437;&#x432;&#x438;&#x447;&#x430;&#x439; &#x43a;&#x43e;&#x43b;&#x438; &#x434;&#x456;&#x442;&#x438; &#x43d;&#x435; &#x445;&#x43e;&#x447;&#x443;&#x442;&#x44c; &#x441;&#x43f;&#x456;&#x432;&#x43f;&#x440;&#x430;&#x446;&#x44e;&#x432;&#x430;&#x442;&#x438;&#xa;&#x457;&#x43c; &#x43f;&#x43e;&#x442;&#x440;&#x456;&#x431;&#x43d;&#x43e; &#x43d;&#x435; &#x442;&#x435; &#x449;&#x43e; &#x432;&#x43e;&#x43d;&#x438; &#x43f;&#x440;&#x43e;&#x441;&#x44f;&#x442;&#x44c;,&#xa;&#x430; &#x449;&#x43e;&#x441;&#x44c; &#x431;&#x456;&#x43b;&#x44c;&#x448; &#x433;&#x43b;&#x438;&#x431;&#x43e;&#x43a;&#x435;: &#x43b;&#x44e;&#x431;&#x43e;&#x432;, &#x443;&#x432;&#x430;&#x433;&#x430;, &#x43e;&#x431;&#x456;&#x439;&#x43c;&#x438;" STYLE_REF="pre_last_node" ID="ID_844319043" CREATED="1418641447631" MODIFIED="1427365984885"/>
<node TEXT="&#x41f;&#x456;&#x434; &#x43e;&#x43f;&#x43e;&#x440;&#x43e;&#x43c; &#x434;&#x438;&#x442;&#x438;&#x43d;&#x438; &#x437;&#x430;&#x43b;&#x44f;&#x433;&#x430;&#x454; &#x441;&#x43f;&#x43e;&#x447;&#x430;&#x442;&#x43a;&#x443;:" STYLE_REF="pre_last_node" ID="ID_1688577883" CREATED="1418641804257" MODIFIED="1427365067472">
<font BOLD="false"/>
<node TEXT="&#x433;&#x43d;&#x456;&#x432;" STYLE_REF="pre_last_node" ID="ID_1747612947" CREATED="1427365041052" MODIFIED="1427365079432">
<font BOLD="true"/>
</node>
<node TEXT="&#x441;&#x443;&#x43c;" STYLE_REF="pre_last_node" ID="ID_864567939" CREATED="1427365054260" MODIFIED="1427365083632">
<font BOLD="true"/>
</node>
<node TEXT="&#x441;&#x442;&#x440;&#x430;&#x445;" STYLE_REF="pre_last_node" ID="ID_803493103" CREATED="1427365060617" MODIFIED="1427365079437">
<font BOLD="true"/>
</node>
</node>
</node>
<node TEXT="&#x41f;&#x43e;&#x434;&#x43e;&#x43b;&#x430;&#x43d;&#x43d;&#x44f; &#x43e;&#x43f;&#x43e;&#x440;&#x443;" STYLE_REF="orange_node" ID="ID_211819258" CREATED="1418638875125" MODIFIED="1418640122665" VGAP="42">
<icon BUILTIN="wizard"/>
<node TEXT="&#x410;&#x432;&#x442;&#x43e;&#x440;&#x438;&#x442;&#x430;&#x440;&#x43d;&#x435; &#x432;&#x438;&#x445;&#x43e;&#x432;&#x430;&#x43d;&#x43d;&#x44f;" STYLE_REF="yellow_node" ID="ID_778651169" CREATED="1418639200866" MODIFIED="1427365758420" VGAP="13">
<hook URI="../../Images/authoritarianism-small.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x41e;&#x437;&#x43d;&#x430;&#x43a;&#x438;" ID="ID_1288092750" CREATED="1418639255988" MODIFIED="1418640279074" BACKGROUND_COLOR="#99ff66">
<icon BUILTIN="list"/>
<node TEXT="&#x41f;&#x43e;&#x434;&#x430;&#x432;&#x43b;&#x435;&#x43d;&#x43d;&#x44f; &#x43e;&#x43f;&#x43e;&#x440;&#x443; &#x434;&#x438;&#x442;&#x438;&#x43d;&#x438;,&#xa;&#x440;&#x456;&#x437;&#x43d;&#x438;&#x43c;&#x438; &#x43c;&#x435;&#x442;&#x43e;&#x434;&#x430;&#x43c;&#x438; &#x43d;&#x430;&#x432;&#x456;&#x442;&#x44c; &#x431;&#x438;&#x442;&#x442;&#x44f;&#x43c;" STYLE_REF="pre_last_node" ID="ID_761291512" CREATED="1418639557601" MODIFIED="1418639638731"/>
<node TEXT="&#x414;&#x438;&#x442;&#x438;&#x43d;&#x430; &#x43d;&#x435; &#x43c;&#x430;&#x454; &#x43f;&#x440;&#x430;&#x432;&#x430; &#x43d;&#x430; &#x43e;&#x43f;&#x456;&#x440;" STYLE_REF="pre_last_node" ID="ID_925721827" CREATED="1418639615824" MODIFIED="1418639633033"/>
<node TEXT="&#x41f;&#x43e;&#x436;&#x430;&#x43b;&#x456;&#x442;&#x44c; &#x440;&#x43e;&#x437;&#x433;&#x443; - &#x437;&#x456;&#x43f;&#x441;&#x443;&#x432;&#x430;&#x442;&#x44c; &#x434;&#x438;&#x442;&#x438;&#x43d;&#x443;" STYLE_REF="pre_last_node" ID="ID_988318014" CREATED="1418642010939" MODIFIED="1418642039865"/>
</node>
<node TEXT="&#x41d;&#x430;&#x441;&#x43b;&#x456;&#x434;&#x43a;&#x438;" ID="ID_931063249" CREATED="1418639259926" MODIFIED="1418640279072" BACKGROUND_COLOR="#99ff66">
<icon BUILTIN="messagebox_warning"/>
<node TEXT="&#x414;&#x438;&#x442;&#x438;&#x43d;&#x430; &#x432;&#x438;&#x440;&#x43e;&#x441;&#x442;&#x435; &#x43d;&#x435;&#x437;&#x434;&#x430;&#x442;&#x43d;&#x43e;&#x44e; &#x441;&#x43b;&#x443;&#x445;&#x430;&#x442;&#x438;&#xa;&#x456;&#x43d;&#x448;&#x438;&#x445; &#x43b;&#x44e;&#x434;&#x435;&#x439;, &#x431;&#x43e; &#x457;&#x457; &#x43d;&#x435;&#x441;&#x43b;&#x443;&#x445;&#x430;&#x43b;&#x438;" STYLE_REF="pre_last_node" ID="ID_420638424" CREATED="1418639264201" MODIFIED="1418639302852"/>
<node TEXT="&#x411;&#x443;&#x43d;&#x442; &#x432; &#x43f;&#x456;&#x434;&#x43b;&#x438;&#x442;&#x43a;&#x43e;&#x432;&#x438;&#x439; &#x43f;&#x435;&#x440;&#x456;&#x43e;&#x434;, &#x43d;&#x435;&#x43c;&#x438;&#x43d;&#x443;&#x447;&#x438;&#x439;" STYLE_REF="pre_last_node" ID="ID_1894516660" CREATED="1418639315274" MODIFIED="1418639384891"/>
<node TEXT="&#x422;&#x443;&#x43f;&#x430; &#x441;&#x43b;&#x443;&#x445;&#x43d;&#x44f;&#x43d;&#x456;&#x441;&#x442;&#x44c; &#x43b;&#x430;&#x43c;&#x430;&#x454; &#x432;&#x43e;&#x43b;&#x44e; &#x434;&#x438;&#x442;&#x438;&#x43d;&#x438;" STYLE_REF="pre_last_node" ID="ID_1678451052" CREATED="1418639337380" MODIFIED="1418639384884"/>
<node TEXT="&#x414;&#x456;&#x442;&#x438; &#x432;&#x438;&#x440;&#x43e;&#x441;&#x442;&#x430;&#x44e;&#x442;&#x44c; &#x43d;&#x435; &#x43a;&#x43e;&#x43c;&#x43f;&#x435;&#x442;&#x435;&#x43d;&#x442;&#x43d;&#x456; &#x432; &#x446;&#x44c;&#x43e;&#x43c;&#x443; &#x441;&#x432;&#x456;&#x442;&#x456;" STYLE_REF="pre_last_node" ID="ID_295465255" CREATED="1418639353482" MODIFIED="1418639384875"/>
</node>
</node>
<node TEXT="&#x41f;&#x43e;&#x437;&#x438;&#x442;&#x438;&#x432;&#x43d;&#x435; &#x432;&#x438;&#x445;&#x43e;&#x432;&#x430;&#x43d;&#x43d;&#x44f;" STYLE_REF="yellow_node" ID="ID_1908717874" CREATED="1418639192377" MODIFIED="1427365691778" VGAP="18">
<hook URI="../../Images/smile-small.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x42f;&#x43a; &#x434;&#x456;&#x44f;&#x442;&#x438;?" ID="ID_1503850723" CREATED="1418639741192" MODIFIED="1418642996422" BACKGROUND_COLOR="#99ff66" VGAP="20">
<icon BUILTIN="checked"/>
<node TEXT="&#x41f;&#x43e;&#x442;&#x440;&#x456;&#x431;&#x43d;&#x43e; &#x432;&#x438;&#x437;&#x43d;&#x430;&#x447;&#x438;&#x442;&#x44c; &#x43d;&#x435;&#x437;&#x430;&#x434;&#x43e;&#x432;&#x43e;&#x43b;&#x435;&#x43d;&#x443;&#xa;&#x43f;&#x43e;&#x442;&#x440;&#x435;&#x431;&#x443;/&#x431;&#x430;&#x436;&#x430;&#x43d;&#x43d;&#x44f; &#x434;&#x438;&#x442;&#x438;&#x43d;&#x438;. &#x406; &#x432;&#x438;&#x441;&#x43a;&#x430;&#x437;&#x430;&#x442;&#x44c; &#x457;&#x457; &#x432;&#x441;&#x43b;&#x443;&#x445;" STYLE_REF="pre_last_node" ID="ID_939651812" CREATED="1418641237093" MODIFIED="1418641414186"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1055;&#1110;&#1089;&#1083;&#1103; &#1094;&#1100;&#1086;&#1075;&#1086; &#1091;&#1087;&#1080;&#1088;&#1072;&#1085;&#1085;&#1103; &#1085;&#1077; &#1088;&#1110;&#1076;&#1082;&#1086; &#1091;&#1093;&#1086;&#1076;&#1080;&#1090;&#1100;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&#x421;&#x43f;&#x43e;&#x447;&#x430;&#x442;&#x43a;&#x443; &#x432;&#x438;&#x441;&#x43b;&#x443;&#x445;&#x43e;&#x432;&#x443;&#x454;&#x43c;&#x43e; &#x443;&#x43f;&#x438;&#x440;&#x430;&#x44e;&#x449;&#x443;&#x441;&#x44f; &#x434;&#x438;&#x442;&#x438;&#x43d;&#x443;&#xa;&#x43f;&#x43e;&#x442;&#x456;&#x43c; &#x432;&#x438;&#x440;&#x456;&#x448;&#x443;&#x454;&#x43c;&#x43e; &#x44f;&#x43a; &#x43a;&#x440;&#x430;&#x449;&#x435; &#x43f;&#x43e;&#x441;&#x442;&#x443;&#x43f;&#x438;&#x442;&#x44c;" STYLE_REF="pre_last_node" ID="ID_43510393" CREATED="1418641034156" MODIFIED="1418641341292">
<node TEXT="&#x41f;&#x43e;&#x445;&#x43e;&#x436;&#x435; &#x43d;&#x430; &#x441;&#x443;&#x434;. &#x41f;&#x456;&#x441;&#x43b;&#x44f; &#x442;&#x43e;&#x433;&#x43e; &#x44f;&#x43a; &#x441;&#x443;&#x434; &#x43f;&#x440;&#x438;&#x439;&#x43d;&#x44f;&#x432; &#x440;&#x456;&#x448;&#x435;&#x43d;&#x43d;&#x44f;,&#xa;&#x434;&#x456;&#x43b;&#x43e; &#x437;&#x430;&#x43b;&#x438;&#x448;&#x430;&#x454;&#x442;&#x44c;&#x441;&#x44f; &#x437;&#x430;&#x43a;&#x440;&#x438;&#x442;&#x438;&#x43c;, &#x434;&#x43e; &#x442;&#x438;&#x445; &#x43f;&#x456;&#x440; &#x43f;&#x43e;&#x43a;&#x438; &#x43d;&#x435; &#x437;&apos;&#x44f;&#x432;&#x43b;&#x44f;&#x44e;&#x442;&#x44c;&#x441;&#x44f;&#xa;&#x43d;&#x43e;&#x432;&#x456; &#x434;&#x43e;&#x43a;&#x430;&#x437;&#x438; &#x456; &#x441;&#x432;&#x456;&#x434;&#x447;&#x435;&#x43d;&#x43d;&#x44f;. &#x41f;&#x43e;&#x442;&#x456;&#x43c; &#x43c;&#x43e;&#x436;&#x43b;&#x438;&#x432;&#x438;&#x439; &#x43f;&#x435;&#x440;&#x435;&#x433;&#x43b;&#x44f;&#x434; &#x441;&#x43f;&#x440;&#x430;&#x432;&#x438;" STYLE_REF="last_node" ID="ID_41204532" CREATED="1418642079491" MODIFIED="1418642215147"/>
</node>
<node TEXT="&#x41b;&#x438;&#x448;&#x435; &#x437; 12 &#x434;&#x43e; 14 &#x440;&#x43e;&#x43a;&#x456;&#x432; &#x43f;&#x43e;&#x447;&#x438;&#x43d;&#x430;&#x442;&#x438;&#xa;&#x43f;&#x438;&#x442;&#x430;&#x442;&#x438; &#x443; &#x434;&#x456;&#x442;&#x435;&#x439; &#x447;&#x43e;&#x433;&#x43e; &#x432;&#x43e;&#x43d;&#x438; &#x445;&#x43e;&#x447;&#x443;&#x442;&#x44c;" STYLE_REF="pre_last_node" ID="ID_609126200" CREATED="1418639749582" MODIFIED="1418639877788"/>
<node TEXT="&#x417; 14 &#x440;&#x43e;&#x43a;&#x456;&#x432; &#x43f;&#x43e;&#x447;&#x438;&#x43d;&#x430;&#x442;&#x438; &#x43f;&#x438;&#x442;&#x430;&#x442;&#x438;&#xa;&#x443; &#x434;&#x456;&#x442;&#x435;&#x439; &#x449;&#x43e; &#x432;&#x43e;&#x43d;&#x438; &#x434;&#x443;&#x43c;&#x430;&#x44e;&#x442;&#x44c;" STYLE_REF="pre_last_node" ID="ID_51391606" CREATED="1418639844745" MODIFIED="1418641341321"/>
<node TEXT="&#x410;&#x43b;&#x433;&#x43e;&#x440;&#x438;&#x442;&#x43c;" STYLE_REF="pre_last_node" ID="ID_1850133848" CREATED="1418641607308" MODIFIED="1418642992063">
<node TEXT="1. &#x412;&#x438;&#x441;&#x43b;&#x443;&#x445;&#x43e;&#x432;&#x443;&#x454;&#x43c;&#x43e; &#x443;&#x43f;&#x438;&#x440;&#x430;&#x44e;&#x449;&#x443;&#x441;&#x44f; &#x434;&#x438;&#x442;&#x438;&#x43d;&#x443;" ID="ID_395516724" CREATED="1418642736730" MODIFIED="1418642756445"/>
<node TEXT="2. &#x412;&#x438;&#x441;&#x43b;&#x43e;&#x432;&#x43b;&#x44e;&#x454;&#x43c;&#x43e; &#x441;&#x432;&#x43e;&#x454; &#x440;&#x43e;&#x437;&#x443;&#x43c;&#x456;&#x43d;&#x43d;&#x44f; &#x457;&#x457; &#x43f;&#x43e;&#x447;&#x443;&#x442;&#x442;&#x456;&#x432;" ID="ID_137926019" CREATED="1418642758333" MODIFIED="1418642822438">
<node TEXT="&#x414;&#x43b;&#x430; &#x430;&#x43a;&#x442;&#x438;&#x432;&#x43d;&#x438;&#x445; &#x434;&#x456;&#x442;&#x435;&#x439; &#x437;&#x43e;&#x441;&#x435;&#x440;&#x435;&#x434;&#x436;&#x443;&#x454;&#x43c;&#x43e;&#x441;&#x44f;&#xa;&#x43d;&#x435; &#x43d;&#x430; &#x43f;&#x43e;&#x447;&#x443;&#x442;&#x442;&#x44f;&#x445; &#x430; &#x43d;&#x430;&#x434;&#x456;&#x44f;&#x445;: &#x42f; &#x440;&#x43e;&#x437;&#x443;&#x43c;&#x456;&#x44e; &#x449;&#x43e; &#x442;&#x438; &#x43a;&#x438;&#x43d;&#x443;&#x432;&#xa;&#x432;&#x441;&#x456; &#x441;&#x432;&#x43e;&#x457; &#x441;&#x43f;&#x440;&#x430;&#x432;&#x438;, &#x449;&#x43e;&#x431; &#x43f;&#x440;&#x438;&#x439;&#x442;&#x438; &#x441;&#x44e;&#x434;&#x438; &#x456; &#x432;&#x437;&#x44f;&#x442;&#x44c; &#x43f;&#x456;&#x440;&#x43e;&#x436;&#x435;&#x43d;&#x435;,&#xa;&#x430;&#x43b;&#x435; &#x44f; &#x445;&#x43e;&#x447;&#x443; &#x449;&#x43e;&#x431; &#x442;&#x438; &#x441;&#x43f;&#x435;&#x440;&#x448;&#x443; &#x43f;&#x43e;&#x43e;&#x431;&#x456;&#x434;&#x430;&#x432;" ID="ID_1519217147" CREATED="1418642877396" MODIFIED="1418642985200"/>
<node TEXT="&#x414;&#x43b;&#x44f; &#x437;&#x435;&#x43c;&#x43b;&#x44f;&#x43d;&#x438;&#x445; &#x434;&#x456;&#x442;&#x435;&#x439; &#x43c;&#x43e;&#x436;&#x43d;&#x430; &#x434;&#x43e;&#x431;&#x430;&#x432;&#x438;&#x442;&#x44c; &#x43f;&#x430;&#x440;&#x443; &#x441;&#x43b;&#x456;&#x432;&#xa;&#x43f;&#x440;&#x43e; &#x440;&#x438;&#x442;&#x43c;: ..., &#x430;&#x43b;&#x435; &#x437;&#x430;&#x440;&#x430;&#x437; &#x43f;&#x43e;&#x440;&#x430; &#x433;&#x43e;&#x442;&#x443;&#x432;&#x430;&#x442;&#x44c;&#x441;&#x44f; &#x434;&#x43e; &#x43e;&#x431;&#x456;&#x434;&#x443;,&#xa;&#x430; &#x43f;&#x456;&#x441;&#x43b;&#x44f; &#x43e;&#x431;&#x456;&#x434;&#x443; &#x43f;&#x440;&#x438;&#x439;&#x434;&#x435; &#x43f;&#x43e;&#x440;&#x430; &#x434;&#x435;&#x441;&#x435;&#x440;&#x442;&#x443;" ID="ID_1194207726" CREATED="1418643064515" MODIFIED="1418643123874"/>
</node>
<node TEXT="3. &#x41f;&#x440;&#x438;&#x439;&#x43c;&#x430;&#x454;&#x43c;&#x43e; &#x440;&#x456;&#x448;&#x435;&#x43d;&#x43d;&#x44f;" ID="ID_1711845823" CREATED="1418642824291" MODIFIED="1418643014520"/>
</node>
</node>
<node TEXT="&#x41f;&#x440;&#x438;&#x43d;&#x446;&#x438;&#x43f;&#x438;" ID="ID_63950908" CREATED="1418639763225" MODIFIED="1418640321403" BACKGROUND_COLOR="#99ff66">
<icon BUILTIN="list"/>
<node TEXT="&#x412;&#x438;&#x431;&#x456;&#x440; &#x434;&#x43e; 9 &#x440;&#x43e;&#x43a;&#x456;&#x432; &#x434;&#x438;&#x442;&#x438;&#x43d;&#x456; &#x43d;&#x435; &#x43f;&#x43e;&#x442;&#x440;&#x456;&#x431;&#x435;&#x43d;" STYLE_REF="pre_last_node" ID="ID_1490320929" CREATED="1418639767466" MODIFIED="1418639877834"/>
<node TEXT="&#x414;&#x456;&#x442;&#x438; &#x43d;&#x435; &#x433;&#x43e;&#x442;&#x43e;&#x432;&#x456; &#x434;&#x43e; &#x441;&#x430;&#x43c;&#x43e;&#x441;&#x442;&#x456;&#x439;&#x43d;&#x43e;&#x441;&#x442;&#x456;&#xa; &#x457;&#x43c; &#x43f;&#x43e;&#x442;&#x440;&#x456;&#x431;&#x435;&#x43d; &#x43a;&#x435;&#x440;&#x456;&#x432;&#x43d;&#x438;&#x43a;" STYLE_REF="pre_last_node" ID="ID_1343615801" CREATED="1418639785933" MODIFIED="1418639877848"/>
<node TEXT="&#x412;&#x438;&#x434;&#x456;&#x43b;&#x438;&#x442;&#x44c; &#x447;&#x430;&#x441; &#x434;&#x43b;&#x44f; &#x432;&#x438;&#x441;&#x43b;&#x443;&#x445;&#x430;&#x43d;&#x43d;&#x44f; &#x434;&#x438;&#x442;&#x438;&#x43d;&#x438; &#x43d;&#x430;&#x431;&#x430;&#x433;&#x430;&#x442;&#x43e;&#xa;&#x432;&#x430;&#x436;&#x43d;&#x456;&#x448;&#x435;, &#x43d;&#x456;&#x436; &#x43f;&#x440;&#x438;&#x432;&#x435;&#x437;&#x442;&#x438; &#x457;&#x457; &#x432;&#x447;&#x430;&#x441;&#x43d;&#x43e; &#x43d;&#x430; &#x44f;&#x43a;&#x456;&#x439;&#x441;&#x44c; &#x433;&#x443;&#x440;&#x442;&#x43e;&#x43a;" STYLE_REF="pre_last_node" ID="ID_753985873" CREATED="1418641618157" MODIFIED="1418641722488"/>
<node TEXT="&#x414;&#x43e;&#x437;&#x432;&#x43e;&#x43b;&#x438;&#x442;&#x44c; &#x434;&#x438;&#x442;&#x438;&#x43d;&#x456; &#x437;&#x430;&#x431;&#x443;&#x442;&#x44c; &#x445;&#x442;&#x43e; &#x433;&#x43e;&#x43b;&#x43e;&#x432;&#x43d;&#x438;&#x439; -&#xa;- &#x437;&#x456;&#x43f;&#x441;&#x443;&#x432;&#x430;&#x442;&#x44c; &#x434;&#x438;&#x442;&#x438;&#x43d;&#x443;" STYLE_REF="pre_last_node" ID="ID_369416494" CREATED="1418641979693" MODIFIED="1418642002111"/>
</node>
<node TEXT="&#x41d;&#x430;&#x441;&#x43b;&#x456;&#x434;&#x43a;&#x438;" ID="ID_1933175970" CREATED="1418642255692" MODIFIED="1418642494369" BACKGROUND_COLOR="#99ff66">
<icon BUILTIN="bookmark"/>
<node TEXT="&#x41a;&#x43e;&#x43b;&#x438; &#x434;&#x456;&#x442;&#x44f;&#x43c; &#x434;&#x43e;&#x437;&#x432;&#x43e;&#x43b;&#x435;&#x43d;&#x43e; &#x43e;&#x43f;&#x438;&#x440;&#x430;&#x442;&#x44c;&#x441;&#x44f; &#x432;&#x43e;&#x43b;&#x456; &#x431;&#x430;&#x442;&#x44c;&#x43a;&#x456;&#x432;,&#xa;&#x432;&#x43e;&#x43d;&#x430; &#x432;&#x447;&#x438;&#x442;&#x44c;&#x441;&#x44f; &#x432;&#x456;&#x434;&#x43a;&#x43b;&#x430;&#x434;&#x443;&#x432;&#x430;&#x442;&#x44c; &#x437;&#x430;&#x434;&#x43e;&#x432;&#x43e;&#x43b;&#x435;&#x43d;&#x43d;&#x44f; &#x431;&#x430;&#x436;&#x430;&#x43d;&#x44c;&#xa;&#x43d;&#x430; &#x43f;&#x43e;&#x442;&#x456;&#x43c;." STYLE_REF="pre_last_node" ID="ID_372808539" CREATED="1418642260051" MODIFIED="1418642323737"/>
<node TEXT="&#x414;&#x456;&#x442;&#x438; &#x432;&#x447;&#x430;&#x442;&#x44c;&#x441;&#x44f; &#x43f;&#x440;&#x438;&#x439;&#x43c;&#x430;&#x442;&#x438; &#x440;&#x435;&#x447;&#x456; &#x44f;&#x43a; &#x432;&#x43e;&#x43d;&#x438; &#x454;&#x441;&#x442;&#x44c;" STYLE_REF="pre_last_node" ID="ID_229610615" CREATED="1418642386192" MODIFIED="1418642482100"/>
<node TEXT="&#x414;&#x456;&#x442;&#x438; &#x432;&#x447;&#x430;&#x442;&#x44c;&#x441;&#x44f; &#x443;&#x441;&#x432;&#x456;&#x434;&#x43e;&#x43c;&#x43b;&#x44e;&#x432;&#x430;&#x442;&#x44c; &#x457;&#x445;&#x43d;&#x456; &#x441;&#x43f;&#x440;&#x430;&#x432;&#x436;&#x43d;&#x456; &#x43f;&#x43e;&#x442;&#x440;&#x435;&#x431;&#x438;" STYLE_REF="pre_last_node" ID="ID_462081845" CREATED="1418642400018" MODIFIED="1418642482090"/>
</node>
</node>
<node TEXT="&#x41f;&#x43e;&#x441;&#x442;&#x443;&#x43f;&#x43b;&#x438;&#x432;&#x435; &#x432;&#x438;&#x445;&#x43e;&#x432;&#x430;&#x43d;&#x43d;&#x44f;" STYLE_REF="yellow_node" ID="ID_732656659" CREATED="1418639208090" MODIFIED="1418640120086" VGAP="20" HGAP="30" VSHIFT="-60">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="80" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_1908717874" MIDDLE_LABEL="&#x41d;&#x435; &#x434;&#x43e;&#x440;&#x456;&#x432;&#x43d;&#x44e;&#x454;" STARTINCLINATION="144;-72;" ENDINCLINATION="-133;76;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
<node TEXT="&#x41e;&#x437;&#x43d;&#x430;&#x43a;&#x438;" ID="ID_827581800" CREATED="1418639235904" MODIFIED="1418640279055" BACKGROUND_COLOR="#99ff66">
<icon BUILTIN="list"/>
<node TEXT="&#x41f;&#x43e;&#x442;&#x430;&#x43a;&#x430;&#x43d;&#x43d;&#x44f; &#x43f;&#x440;&#x438;&#x445;&#x43e;&#x442;&#x44f;&#x43c; &#x434;&#x438;&#x442;&#x438;&#x43d;&#x438;,&#xa;&#x430;&#x431;&#x438; &#x443;&#x43d;&#x438;&#x43a;&#x43d;&#x443;&#x442;&#x438; &#x441;&#x43a;&#x430;&#x43d;&#x434;&#x430;&#x43b;&#x443;" STYLE_REF="pre_last_node" ID="ID_1124008260" CREATED="1418639421394" MODIFIED="1418639489064"/>
</node>
<node TEXT="&#x41d;&#x430;&#x441;&#x43b;&#x456;&#x434;&#x43a;&#x438;" ID="ID_1713566995" CREATED="1418639245493" MODIFIED="1418640279063" BACKGROUND_COLOR="#99ff66">
<icon BUILTIN="messagebox_warning"/>
<node TEXT="&#x414;&#x438;&#x442;&#x438;&#x43d;&#x430; &#x443;&#x441;&#x432;&#x456;&#x434;&#x43e;&#x431;&#x43b;&#x44e;&#x454;, &#x449;&#x43e; &#x432;&#x43e;&#x43d;&#x430; &#x43c;&#x43e;&#x436;&#x435;&#xa;&#x434;&#x43e;&#x431;&#x438;&#x442;&#x438;&#x441;&#x44c; &#x441;&#x432;&#x43e;&#x433;&#x43e; &#x437; &#x434;&#x43e;&#x43f;&#x43e;&#x43c;&#x43e;&#x433;&#x43e;&#x44e; &#x441;&#x43a;&#x430;&#x43d;&#x434;&#x430;&#x43b;&#x456;&#x432;" STYLE_REF="pre_last_node" ID="ID_1533413412" CREATED="1418639398420" MODIFIED="1418639489043"/>
</node>
</node>
</node>
</node>
</node>
</map>
