<map version="freeplane 1.3.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="&#x41c;&#x456;&#x439; &#x437;&#x434;&#x43e;&#x440;&#x43e;&#x432;&#x438;&#x439; &#x441;&#x43f;&#x43e;&#x441;&#x456;&#x431; &#x436;&#x438;&#x442;&#x442;&#x44f;" STYLE_REF="main_node" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1430100222625">
<hook NAME="AutomaticEdgeColor" COUNTER="4"/>
<hook NAME="MapStyle" zoom="1.003">
    <properties show_icon_for_attributes="true" show_note_icons="true"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
<stylenode TEXT="main_node" BACKGROUND_COLOR="#ccccff">
<font SIZE="18" BOLD="true"/>
<edge STYLE="sharp_bezier" WIDTH="4"/>
</stylenode>
<stylenode TEXT="second_node" BACKGROUND_COLOR="#00cccc" STYLE="bubble">
<font SIZE="14"/>
</stylenode>
<stylenode TEXT="third_node" BACKGROUND_COLOR="#ffbdf4" STYLE="bubble">
<font SIZE="11" BOLD="true"/>
</stylenode>
<stylenode TEXT="orange_node" BACKGROUND_COLOR="#ff9900" STYLE="bubble">
<font BOLD="true"/>
</stylenode>
<stylenode TEXT="gray_node" BACKGROUND_COLOR="#9ec19e">
<font BOLD="true"/>
</stylenode>
<stylenode TEXT="yellow_node" BACKGROUND_COLOR="#ffff66" STYLE="bubble">
<edge WIDTH="1"/>
</stylenode>
<stylenode TEXT="example_node" BACKGROUND_COLOR="#afcdfa">
<edge STYLE="bezier" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="pre_last_node" BACKGROUND_COLOR="#99ff99" STYLE="bubble">
<edge STYLE="bezier" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="last_node" BACKGROUND_COLOR="#ffffff">
<edge STYLE="bezier" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="warning_node" BACKGROUND_COLOR="#fda6a6">
<icon BUILTIN="messagebox_warning"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<node TEXT="&#x41d;&#x435;&#x434;&#x456;&#x44f;&#x43d;&#x43d;&#x44f;" STYLE_REF="second_node" POSITION="right" ID="ID_1139101200" CREATED="1430100271970" MODIFIED="1430109057197">
<edge COLOR="#ff0000"/>
<hook URI="../../Images/meditation.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x41d;&#x435; &#x432;&#x436;&#x438;&#x432;&#x430;&#x44e;" STYLE_REF="yellow_node" ID="ID_352832374" CREATED="1430100311101" MODIFIED="1430109385068">
<hook URI="../../Images/no.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x410;&#x43b;&#x43a;&#x43e;&#x433;&#x43e;&#x43b;&#x44c;" STYLE_REF="pre_last_node" ID="ID_160547302" CREATED="1430100316027" MODIFIED="1430100520736"/>
<node TEXT="&#x41c;&apos;&#x44f;&#x441;&#x430;" STYLE_REF="pre_last_node" ID="ID_382256629" CREATED="1430101535716" MODIFIED="1430101606838"/>
<node TEXT="&#x420;&#x438;&#x431;&#x438;" STYLE_REF="pre_last_node" ID="ID_1257319654" CREATED="1430101539146" MODIFIED="1430101587744"/>
<node TEXT="&#x41c;&#x43e;&#x440;&#x43e;&#x437;&#x438;&#x432;&#x43e;" STYLE_REF="pre_last_node" ID="ID_1042912482" CREATED="1430100320956" MODIFIED="1430100520737"/>
<node TEXT="&#x428;&#x43e;&#x43a;&#x43e;&#x43b;&#x430;&#x434;" STYLE_REF="pre_last_node" ID="ID_154320922" CREATED="1430100459242" MODIFIED="1430100520737"/>
<node TEXT="&#x426;&#x443;&#x43a;&#x435;&#x440;&#x43a;&#x438;" STYLE_REF="pre_last_node" ID="ID_500108288" CREATED="1430100469994" MODIFIED="1430100520737"/>
<node TEXT="&#x414;&#x440;&#x456;&#x436;&#x434;&#x436;&#x43e;&#x432;&#x43e;&#x433;&#x43e; &#x445;&#x43b;&#x456;&#x431;&#x443;" STYLE_REF="pre_last_node" ID="ID_896121813" CREATED="1430101489491" MODIFIED="1430101531815"/>
<node TEXT="&#x412;&#x441;&#x456;  &#x43a;&#x443;&#x43f;&#x43b;&#x44f;&#x43d;&#x456; &#x43f;&#x440;&#x43e;&#x434;&#x443;&#x43a;&#x442;&#x438; &#x437; &#x446;&#x443;&#x43a;&#x440;&#x43e;&#x43c;: &#x43f;&#x435;&#x447;&#x438;&#x432;&#x43e;, &#x43a;&#x43e;&#x437;&#x456;&#x43d;&#x430;&#x43a;&#x456;, &#x43d;&#x43e;&#x442;&#x435;&#x43b;&#x43b;&#x438;, ..." STYLE_REF="pre_last_node" ID="ID_31887389" CREATED="1430100324355" MODIFIED="1430101873231">
<icon BUILTIN="messagebox_warning"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1044;&#1086; 31.12.2015
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="&#x414;&#x456;&#x44f;&#x43d;&#x43d;&#x44f;" STYLE_REF="second_node" POSITION="left" ID="ID_770366589" CREATED="1430100448507" MODIFIED="1430108878132" VGAP="25">
<edge COLOR="#0000ff"/>
<hook URI="../../Images/workout.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x421;&#x43e;&#x43d;" STYLE_REF="yellow_node" ID="ID_466949967" CREATED="1430101425683" MODIFIED="1430108803355">
<hook URI="../../Images/sleep.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x41b;&#x456;&#x436;&#x43a;&#x43e;" STYLE_REF="gray_node" ID="ID_168272532" CREATED="1430102176907" MODIFIED="1430102257073" VSHIFT="-10">
<font BOLD="false"/>
<node TEXT="&#x420;&#x456;&#x432;&#x43d;&#x435;, &#x442;&#x432;&#x435;&#x440;&#x434;&#x435;" STYLE_REF="pre_last_node" ID="ID_141044588" CREATED="1430102184867" MODIFIED="1430102245175">
<icon BUILTIN="closed"/>
</node>
<node TEXT="&#x41c;&#x430;&#x43b;&#x430; &#x43f;&#x43e;&#x434;&#x443;&#x448;&#x43a;&#x430;" STYLE_REF="pre_last_node" ID="ID_268694923" CREATED="1430102190442" MODIFIED="1430102241624"/>
</node>
<node TEXT="&#x420;&#x430;&#x43d;&#x43e; &#x43b;&#x44f;&#x433;&#x430;&#x44e; &#x441;&#x43f;&#x430;&#x442;&#x438;" STYLE_REF="pre_last_node" ID="ID_296749909" CREATED="1430100681347" MODIFIED="1430100998866"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1076;&#1086; 23:00
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&#x420;&#x430;&#x43d;&#x43e; &#x432;&#x441;&#x442;&#x430;&#x44e; &#x437; &#x43a;&#x440;&#x43e;&#x432;&#x430;&#x442;&#x456;" STYLE_REF="pre_last_node" ID="ID_1359107151" CREATED="1430100721427" MODIFIED="1430100998866"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1076;&#1086; 07:30
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="&#x412;&#x440;&#x430;&#x43d;&#x446;&#x456;" STYLE_REF="yellow_node" ID="ID_1096698947" CREATED="1430100757762" MODIFIED="1430109114654">
<hook URI="../../Images/morning.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x420;&#x43e;&#x431;&#x43b;&#x44e; &#x43f;&#x440;&#x43e;&#x433;&#x443;&#x43b;&#x44f;&#x43d;&#x43a;&#x443;" STYLE_REF="pre_last_node" ID="ID_777773424" CREATED="1430100768739" MODIFIED="1430100998866"/>
<node TEXT="&#x420;&#x43e;&#x431;&#x43b;&#x44e; &#x434;&#x438;&#x445;&#x430;&#x43b;&#x44c;&#x43d;&#x443; &#x433;&#x456;&#x43c;&#x43d;&#x430;&#x441;&#x442;&#x438;&#x43a;&#x443;" STYLE_REF="pre_last_node" ID="ID_1719623793" CREATED="1430100783419" MODIFIED="1430100998865"/>
<node TEXT="&#x411;&#x456;&#x433;&#x430;&#x44e;" STYLE_REF="pre_last_node" ID="ID_758204152" CREATED="1430100823258" MODIFIED="1430100998865"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      2 &#1088;&#1072;&#1079;&#1080; &#1085;&#1072; &#1090;&#1080;&#1078;&#1076;&#1077;&#1085;&#1100;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&#x420;&#x43e;&#x431;&#x43b;&#x44e; &#x441;&#x438;&#x43b;&#x43e;&#x432;&#x435; &#x442;&#x440;&#x435;&#x43d;&#x443;&#x432;&#x430;&#x43d;&#x43d;&#x44f;" STYLE_REF="pre_last_node" ID="ID_455859399" CREATED="1430100961580" MODIFIED="1430101864998">
<icon BUILTIN="messagebox_warning"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      2 &#1088;&#1072;&#1079;&#1080; &#1085;&#1072; &#1090;&#1080;&#1078;&#1076;&#1077;&#1085;&#1100;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="&#x412;&#x432;&#x435;&#x447;&#x435;&#x440;&#x456;" STYLE_REF="yellow_node" ID="ID_1701888330" CREATED="1430101242107" MODIFIED="1430109148964">
<hook URI="../../Images/evening.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x420;&#x43e;&#x431;&#x43b;&#x44e; &#x440;&#x43e;&#x437;&#x43c;&#x438;&#x43d;&#x43a;&#x443; (&#x440;&#x43e;&#x437;&#x442;&#x44f;&#x436;&#x43a;&#x443;)" STYLE_REF="pre_last_node" ID="ID_779114902" CREATED="1430101245043" MODIFIED="1430101861566">
<icon BUILTIN="messagebox_warning"/>
</node>
</node>
<node TEXT="&#x407;&#x436;&#x430;" STYLE_REF="yellow_node" ID="ID_741379165" CREATED="1430100848283" MODIFIED="1490525365075">
<hook URI="../../Images/food.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x407;&#x43c; &#x43d;&#x430;&#x442;&#x43e;&#x449;&#x430;&#x43a;" STYLE_REF="gray_node" ID="ID_1233898778" CREATED="1430101982771" MODIFIED="1490525365074">
<font BOLD="false"/>
<node TEXT="&#x417;&#x435;&#x43b;&#x435;&#x43d;&#x438;&#x439; &#x43a;&#x43e;&#x43a;&#x442;&#x435;&#x43b;&#x44c;" STYLE_REF="pre_last_node" ID="ID_636840716" CREATED="1430100928171" MODIFIED="1430102011537"/>
<node TEXT="&#x424;&#x440;&#x443;&#x43a;&#x442;&#x438;" STYLE_REF="pre_last_node" ID="ID_1230299844" CREATED="1430101998650" MODIFIED="1430102016024"/>
</node>
<node TEXT="&#x411;&#x43b;&#x430;&#x433;&#x43e;&#x441;&#x43b;&#x43e;&#x432;&#x43b;&#x44f;&#x44e; &#x457;&#x436;&#x443; &#x456; &#x43d;&#x430;&#x43b;&#x430;&#x448;&#x442;&#x43e;&#x432;&#x443;&#x44e;&#x441;&#x44c; &#x43d;&#x430; &#x457;&#x457; &#x43f;&#x440;&#x438;&#x439;&#x43e;&#x43c;" STYLE_REF="pre_last_node" ID="ID_1342040699" CREATED="1430100852775" MODIFIED="1430100998865"/>
<node TEXT="&#x421;&#x43f;&#x43e;&#x43a;&#x456;&#x439;&#x43d;&#x43e; &#x457;&#x43c; &#x456; &#x440;&#x435;&#x442;&#x435;&#x43b;&#x44c;&#x43d;&#x43e; &#x43f;&#x435;&#x440;&#x435;&#x436;&#x43e;&#x432;&#x443;&#x44e; &#x457;&#x436;&#x443;" STYLE_REF="pre_last_node" ID="ID_32542280" CREATED="1430100883595" MODIFIED="1430101858046">
<icon BUILTIN="messagebox_warning"/>
</node>
<node TEXT="&#x407;&#x43c; &#x432; &#x43c;&#x456;&#x440;&#x443; (&#x43d;&#x435; &#x43f;&#x435;&#x440;&#x435;&#x457;&#x434;&#x430;&#x44e;)" STYLE_REF="pre_last_node" ID="ID_501961557" CREATED="1430100604332" MODIFIED="1430109584012">
<icon BUILTIN="messagebox_warning"/>
</node>
</node>
<node TEXT="&#x412;&#x43e;&#x434;&#x430;" STYLE_REF="yellow_node" ID="ID_488018303" CREATED="1430101732459" MODIFIED="1430109174721">
<hook URI="../../Images/water.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x412;&#x434;&#x43e;&#x441;&#x442;&#x430;&#x43b;&#x44c; &#x43f;&#x44c;&#x44e; &#x432;&#x43e;&#x434;&#x438;" STYLE_REF="pre_last_node" ID="ID_254361729" CREATED="1430101736139" MODIFIED="1430102133520">
<icon BUILTIN="messagebox_warning"/>
<node TEXT="&#x412;&#x440;&#x430;&#x43d;&#x446;&#x456; 2 &#x447;&#x430;&#x448;&#x43a;&#x438;" ID="ID_1942797243" CREATED="1430101744684" MODIFIED="1430101758927"/>
<node TEXT="&#x41f;&#x435;&#x440;&#x435;&#x434; &#x43a;&#x43e;&#x436;&#x43d;&#x438;&#x43c; &#x43f;&#x440;&#x438;&#x439;&#x43e;&#x43c;&#x43e;&#x43c; &#x457;&#x436;&#x456; &#x43f;&#x43e; 1 &#x447;&#x430;&#x448;&#x446;&#x456;" ID="ID_1574396465" CREATED="1430101759851" MODIFIED="1430101772087"/>
<node TEXT="&#x412;&#x43f;&#x440;&#x43e;&#x434;&#x43e;&#x432;&#x436; &#x434;&#x43d;&#x44f; &#x432;&#x43e;&#x434;&#x430; &#x441;&#x442;&#x43e;&#x457;&#x442;&#x44c; &#x431;&#x456;&#x43b;&#x44f; &#x43a;&#x43e;&#x43c;&#x43f;&#x430;" ID="ID_1625162622" CREATED="1430101806994" MODIFIED="1430101822296"/>
</node>
</node>
<node TEXT="&#x413;&#x456;&#x433;&#x456;&#x454;&#x43d;&#x430;" STYLE_REF="yellow_node" ID="ID_633071096" CREATED="1430101032788" MODIFIED="1430109443757" VGAP="15">
<hook URI="../../Images/hygiene.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x41c;&#x438;&#x44e; &#x440;&#x443;&#x43a;&#x438;" STYLE_REF="pre_last_node" ID="ID_703563601" CREATED="1430101060716" MODIFIED="1430109443757">
<icon BUILTIN="messagebox_warning"/>
<node TEXT="&#x41f;&#x435;&#x440;&#x435;&#x434; &#x457;&#x436;&#x435;&#x44e;" ID="ID_826132122" CREATED="1430101064674" MODIFIED="1430101067870"/>
<node TEXT="&#x41f;&#x456;&#x441;&#x43b;&#x44f; &#x432;&#x443;&#x43b;&#x438;&#x446;&#x456;" ID="ID_1803190255" CREATED="1430101068962" MODIFIED="1430101073103"/>
</node>
<node TEXT="&#x41f;&#x440;&#x438;&#x439;&#x43c;&#x430;&#x44e; &#x43a;&#x43e;&#x43d;&#x442;&#x440;&#x430;&#x441;&#x442;&#x43d;&#x438;&#x439; &#x434;&#x443;&#x448;" STYLE_REF="pre_last_node" ID="ID_1624595651" CREATED="1430101077843" MODIFIED="1430101176264">
<node TEXT="&#x41f;&#x435;&#x440;&#x435;&#x434; &#x441;&#x43d;&#x43e;&#x43c;" ID="ID_509515447" CREATED="1430101090108" MODIFIED="1430101105824"/>
<node TEXT="&#x41f;&#x456;&#x441;&#x43b;&#x44f;" ID="ID_1905815472" CREATED="1430101107107" MODIFIED="1430101119023">
<node TEXT="&#x421;&#x438;&#x43b;&#x43e;&#x432;&#x438;&#x445; &#x432;&#x43f;&#x440;&#x430;&#x432;" ID="ID_606754433" CREATED="1430101119779" MODIFIED="1430101123511"/>
<node TEXT="&#x411;&#x456;&#x433;&#x443;" ID="ID_535496942" CREATED="1430101124426" MODIFIED="1430101125984"/>
</node>
</node>
<node TEXT="&#x427;&#x438;&#x449;&#x443; &#x437;&#x443;&#x431;&#x438;" STYLE_REF="pre_last_node" ID="ID_303540184" CREATED="1430101129243" MODIFIED="1430101176263">
<node TEXT="&#x41f;&#x435;&#x440;&#x435;&#x434; &#x441;&#x43d;&#x43e;&#x43c;" ID="ID_715344059" CREATED="1430101160452" MODIFIED="1430101161231"/>
</node>
<node TEXT="&#x425;&#x43e;&#x436;&#x443; &#x432; &#x431;&#x430;&#x43d;&#x44e;" STYLE_REF="pre_last_node" ID="ID_1123572351" CREATED="1430109408128" MODIFIED="1430109464125">
<icon BUILTIN="messagebox_warning"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      1 &#1088;&#1072;&#1079; &#1085;&#1072; &#1084;&#1110;&#1089;&#1103;&#1094;&#1100;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
</node>
</map>
