<map version="freeplane 1.3.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="State machines" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1419002610268" VGAP="23" BACKGROUND_COLOR="#cc99ff">
<font SIZE="18" BOLD="true"/>
<hook NAME="MapStyle" zoom="1.001">

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
<stylenode TEXT="second_node" BACKGROUND_COLOR="#00ff66" STYLE="bubble">
<font SIZE="14"/>
</stylenode>
<stylenode TEXT="pre_last_node" BACKGROUND_COLOR="#99ff99" STYLE="bubble">
<edge STYLE="bezier" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="yellow_node" BACKGROUND_COLOR="#ffff33" STYLE="bubble"/>
<stylenode TEXT="orange_node" BACKGROUND_COLOR="#ff9900">
<font BOLD="true"/>
</stylenode>
<stylenode TEXT="last_node">
<edge STYLE="bezier" WIDTH="thin"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="4"/>
<edge STYLE="sharp_bezier" WIDTH="4"/>
<hook URI="../../Images/fsm-large.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x412;&#x438;&#x434;&#x438;" STYLE_REF="second_node" POSITION="right" ID="ID_971538099" CREATED="1418999804429" MODIFIED="1419000933802" VGAP="15">
<edge STYLE="sharp_bezier" COLOR="#660000" WIDTH="4"/>
<node TEXT="Finale state machines" STYLE_REF="orange_node" ID="ID_1889035960" CREATED="1419000566819" MODIFIED="1419000933800" VGAP="24"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      FSM
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Moore machine" STYLE_REF="yellow_node" ID="ID_734222161" CREATED="1419000614260" MODIFIED="1419000832295">
<hook URI="moore.png" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x420;&#x435;&#x437;&#x443;&#x43b;&#x44c;&#x442;&#x430;&#x442; - &#x441;&#x442;&#x432;&#x43e;&#x440;&#x44e;&#x454;&#x442;&#x44c;&#x441;&#x44f; &#x44f;&#x43a;&#xa;&#x43f;&#x440;&#x43e;&#x434;&#x443;&#x43a;&#x442; &#x441;&#x442;&#x430;&#x43d;&#x443;" STYLE_REF="pre_last_node" ID="ID_557984628" CREATED="1419000640196" MODIFIED="1419000671825"/>
<node TEXT="&#x421;&#x442;&#x430;&#x43d; &#x432;&#x438;&#x437;&#x43d;&#x430;&#x447;&#x430;&#x454; &#x449;&#x43e; &#x440;&#x43e;&#x431;&#x438;&#x442;&#x438;" STYLE_REF="pre_last_node" ID="ID_222235768" CREATED="1419000658457" MODIFIED="1419000671818"/>
</node>
<node TEXT="Mearly machine" STYLE_REF="yellow_node" ID="ID_921174055" CREATED="1419000623837" MODIFIED="1419000835418">
<hook URI="mearly.png" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x420;&#x435;&#x437;&#x443;&#x43b;&#x44c;&#x442;&#x430;&#x442; - &#x441;&#x442;&#x432;&#x43e;&#x440; &#x44f;&#x43a; &#x43f;&#x440;&#x43e;&#x434;&#x443;&#x43a;&#x442;&#xa;&#x43f;&#x435;&#x440;&#x435;&#x445;&#x43e;&#x434;&#x443; &#x43c;&#x456;&#x436; &#x441;&#x442;&#x430;&#x43d;&#x430;&#x43c;&#x438;" STYLE_REF="pre_last_node" ID="ID_575964340" CREATED="1419000774078" MODIFIED="1419000815963"/>
<node TEXT="&#x414;&#x456;&#x457; &#x437;&#x43e;&#x441;&#x435;&#x440;&#x435;&#x434;&#x436;&#x435;&#x43d;&#x456; &#x432; &#x43f;&#x440;&#x43e;&#x446;&#x435;&#x441;&#x456;&#xa;&#x43f;&#x435;&#x440;&#x435;&#x445;&#x43e;&#x434;&#x443; &#x43c;&#x456;&#x436; &#x441;&#x442;&#x430;&#x43d;&#x430;&#x43c;&#x438;" STYLE_REF="pre_last_node" ID="ID_484369059" CREATED="1419000794496" MODIFIED="1419000815951"/>
</node>
</node>
<node TEXT="Fuzzy state machines" STYLE_REF="orange_node" ID="ID_1856204181" CREATED="1419000588220" MODIFIED="1419000611699"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      FuSM
    </p>
  </body>
</html>
</richcontent>
<node TEXT="&#x417;&#x430;&#x433;&#x430;&#x43b;&#x44c;&#x43d;&#x430; &#x43c;&#x430;&#x448;&#x438;&#x43d;&#x430; &#x441;&#x442;&#x430;&#x43d;&#x456;&#x432;" STYLE_REF="pre_last_node" ID="ID_1232854383" CREATED="1419000844356" MODIFIED="1419000926740"/>
<node TEXT="Transitions &#x43f;&#x440;&#x435;&#x434;&#x441;&#x442;&#x430;&#x432;&#x43b;&#x435;&#x43d;&#x456; &#x43d;&#x435;&#x447;&#x456;&#x442;&#x43a;&#x438;&#x43c;&#x438; &#x43f;&#x440;&#x430;&#x432;&#x438;&#x43b;&#x430;&#x43c;&#x438;" STYLE_REF="pre_last_node" ID="ID_1399494301" CREATED="1419000856625" MODIFIED="1419000926736"/>
<node TEXT="Transitions &#x43e;&#x442;&#x440;&#x438;&#x43c;&#x443;&#x44e;&#x442;&#x44c; &#x441;&#x442;&#x435;&#x43f;&#x456;&#x43d;&#x44c; &#x43f;&#x440;&#x438;&#x43d;&#x430;&#x43b;&#x435;&#x436;&#x43d;&#x43e;&#x441;&#x442;&#x456;" STYLE_REF="pre_last_node" ID="ID_1179929590" CREATED="1419000878150" MODIFIED="1419000926732"/>
<node TEXT="FuSM &#x43c;&#x43e;&#x436;&#x435; &#x43f;&#x435;&#x440;&#x435;&#x431;&#x443;&#x432;&#x430;&#x442;&#x438; &#x431;&#x456;&#x43b;&#x44c;&#x448;&#x435; &#x43d;&#x456;&#x436; &#x432; &#x43e;&#x434;&#x43d;&#x43e;&#x43c;&#x443; &#x441;&#x442;&#x430;&#x43d;&#x456; &#x43e;&#x434;&#x43d;&#x43e;&#x447;&#x430;&#x441;&#x43d;&#x43e;" STYLE_REF="pre_last_node" ID="ID_196720279" CREATED="1419000899277" MODIFIED="1419000926725"/>
</node>
</node>
<node TEXT="FSM" STYLE_REF="second_node" POSITION="left" ID="ID_1067035322" CREATED="1418999965188" MODIFIED="1425651855394" VGAP="0" HGAP="84" VSHIFT="-63">
<edge COLOR="#000099"/>
<node TEXT="&#x41a;&#x43e;&#x43c;&#x43f;&#x43e;&#x43d;&#x435;&#x43d;&#x442;&#x438;" STYLE_REF="yellow_node" ID="ID_1776947606" CREATED="1419000974116" MODIFIED="1425651855392" HGAP="57" VSHIFT="-10">
<node TEXT="States" STYLE_REF="pre_last_node" ID="ID_635811678" CREATED="1419000997696" MODIFIED="1419001181761">
<font BOLD="true"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1053;&#1072;&#1073;&#1110;&#1088; &#1089;&#1090;&#1072;&#1085;&#1110;&#1074; &#1103;&#1082;&#1110; &#1074;&#1080;&#1079;&#1085;&#1072;&#1095;&#1072;&#1102;&#1090;&#1100; &#1087;&#1086;&#1074;&#1077;&#1076;&#1110;&#1085;&#1082;&#1091;
    </p>
    <p>
      &#1110; &#1084;&#1086;&#1078;&#1091;&#1090;&#1100; &#1074;&#1080;&#1082;&#1086;&#1085;&#1091;&#1074;&#1072;&#1090;&#1080; &#1076;&#1110;&#1111;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="State transitions" STYLE_REF="pre_last_node" ID="ID_155835196" CREATED="1419001044143" MODIFIED="1419001181758">
<font BOLD="true"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1071;&#1082;&#1110; &#1108; &#1087;&#1077;&#1088;&#1077;&#1093;&#1086;&#1076;&#1072;&#1084;&#1080; &#1074;&#1110;&#1076; &#1086;&#1076;&#1085;&#1086;&#1075;&#1086; &#1089;&#1090;&#1072;&#1085;&#1091; &#1076;&#1086; &#1110;&#1085;&#1096;&#1086;&#1075;&#1086;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Rules" STYLE_REF="pre_last_node" ID="ID_1718714511" CREATED="1419001080544" MODIFIED="1419001181755"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1055;&#1088;&#1072;&#1074;&#1080;&#1083;&#1072; &#1095;&#1080; &#1091;&#1084;&#1086;&#1074;&#1080; &#1103;&#1082;&#1110; &#1087;&#1086;&#1074;&#1080;&#1085;&#1085;&#1110; &#1074;&#1080;&#1082;&#1086;&#1085;&#1091;&#1074;&#1072;&#1090;&#1080;&#1089;&#1100;,
    </p>
    <p>
      &#1097;&#1086;&#1073; &#1079;&#1072;&#1087;&#1091;&#1089;&#1090;&#1080;&#1090;&#1080; &#1087;&#1077;&#1088;&#1077;&#1093;&#1110;&#1076; &#1084;&#1110;&#1078; &#1089;&#1090;&#1072;&#1085;&#1072;&#1084;&#1080;
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true"/>
</node>
<node TEXT="Input events" STYLE_REF="pre_last_node" ID="ID_1442291300" CREATED="1419001118377" MODIFIED="1419001181750"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1047;&#1086;&#1074;&#1085;&#1110;&#1096;&#1085;&#1100;&#1086; &#1095;&#1080; &#1074;&#1085;&#1091;&#1090;&#1088;&#1110;&#1096;&#1085;&#1100;&#1086; &#1089;&#1090;&#1074;&#1086;&#1088;&#1077;&#1085;&#1110;, &#1103;&#1082;&#1110; &#1084;&#1086;&#1078;&#1091;&#1090;&#1100;
    </p>
    <p>
      &#1110;&#1085;&#1110;&#1094;&#1110;&#1102;&#1074;&#1072;&#1090;&#1080; &#1087;&#1088;&#1072;&#1074;&#1080;&#1083;&#1072; &#1110; &#1087;&#1088;&#1080;&#1079;&#1074;&#1077;&#1089;&#1090;&#1080; &#1076;&#1086; &#1087;&#1077;&#1088;&#1077;&#1093;&#1086;&#1076;&#1091;
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true"/>
</node>
</node>
<node TEXT="&#x41f;&#x440;&#x438;&#x43a;&#x43b;&#x430;&#x434;&#x438;" STYLE_REF="yellow_node" ID="ID_1788353714" CREATED="1419000981654" MODIFIED="1419002584467" VGAP="23" HGAP="59" VSHIFT="23">
<node TEXT="&#x421;&#x445;&#x435;&#x43c;&#x430;" STYLE_REF="pre_last_node" ID="ID_703439105" CREATED="1419002205810" MODIFIED="1419002413754">
<hook URI="main.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
<node TEXT="Rocket from Quake" STYLE_REF="pre_last_node" ID="ID_274256955" CREATED="1419001346619" MODIFIED="1419001362334">
<hook URI="rocket.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
<node TEXT="Shambler monster from Quake" STYLE_REF="pre_last_node" ID="ID_838447843" CREATED="1419001283357" MODIFIED="1419002862344">
<hook URI="shambler-monster.png" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Sub-FSM Melee attack - &#x44f;&#x43a;&#x449;&#x43e; &#x440;&#x430;&#x43d;&#x434;&#x43e;&#x43c;&#x43d;&#x43e; &#x43e;&#x431;&#x438;&#x440;&#x430;&#x454;&#x442;&#x44c;&#x441;&#x44f;&#xa;&#x432;&#x438;&#x434; &#x430;&#x442;&#x430;&#x43a;&#x456; &#x442;&#x43e; &#x442;&#x430;&#x43a;&#x430; FSM &#x43d;&#x435; &#x434;&#x435;&#x442;&#x435;&#x440;&#x43c;&#x456;&#x43d;&#x43e;&#x432;&#x430;&#x43d;&#x430;" ID="ID_1159624670" CREATED="1419002863682" MODIFIED="1419002872571"/>
<node TEXT="&#x414;&#x456;&#x457; &#x441;&#x442;&#x435;&#x439;&#x442;&#x430; Attack, &#x43c;&#x43e;&#x436;&#x443;&#x442;&#x44c; &#x440;&#x43e;&#x437;&#x433;&#x43b;&#x44f;&#x434;&#x443;&#x432;&#x430;&#x442;&#x438;&#x441;&#x44c;,&#xa;&#x44f;&#x43a; &#x43f;&#x456;&#x434;&#x441;&#x442;&#x435;&#x439;&#x442;&#x438; - &#x442;&#x43e;&#x43c;&#x443;&#x449;&#x43e; &#x43c;&#x43e;&#x43d;&#x441;&#x442;&#x435;&#x440; &#x43c;&#x43e;&#x436;&#x435;&#xa;&#x432;&#x438;&#x43a;&#x43e;&#x43d;&#x443;&#x432;&#x430;&#x442;&#x438; &#x43b;&#x438;&#x448;&#x435; &#x43e;&#x434;&#x43d;&#x443; &#x437; &#x43d;&#x438;&#x445; &#x43e;&#x434;&#x43d;&#x43e;&#x447;&#x430;&#x441;&#x43d;&#x43e;" ID="ID_505580379" CREATED="1419002873919" MODIFIED="1419002940428"/>
</node>
<node TEXT="Dog monster in Quake" STYLE_REF="pre_last_node" ID="ID_1244380618" CREATED="1419001253423" MODIFIED="1419001281208">
<hook URI="dog-monster.png" SIZE="0.99667776" NAME="ExternalObject"/>
</node>
</node>
</node>
<node TEXT="&#x422;&#x435;&#x440;&#x43c;&#x456;&#x43d;&#x438;" STYLE_REF="second_node" POSITION="right" ID="ID_240140070" CREATED="1418999980253" MODIFIED="1419002525557" HGAP="110" VSHIFT="10">
<edge COLOR="#999900"/>
<hook URI="../../Images/dictionary.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x421;&#x442;&#x430;&#x43d;" STYLE_REF="pre_last_node" ID="ID_1909893356" CREATED="1419000425895" MODIFIED="1419000525910"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1054;&#1073;&#1089;&#1090;&#1072;&#1074;&#1080;&#1085;&#1080; &#1088;&#1077;&#1095;&#1077;&#1081;, &#1085;&#1072;&#1073;&#1110;&#1088; &#1076;&#1110;&#1081; &#1103;&#1082;&#1110; &#1074;&#1080;&#1082;&#1086;&#1085;&#1091;&#1102;&#1090;&#1100;&#1089;&#1103;
    </p>
    <p>
      &#1074; &#1082;&#1086;&#1078;&#1085;&#1086;&#1084;&#1091; &#1082;&#1086;&#1085;&#1082;&#1088;&#1077;&#1090;&#1085;&#1086;&#1084;&#1091; &#1074;&#1080;&#1087;&#1072;&#1076;&#1082;&#1091;
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true"/>
</node>
<node TEXT="&#x414;&#x456;&#x457;" STYLE_REF="pre_last_node" ID="ID_1022359738" CREATED="1419000431007" MODIFIED="1419000527638"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1040;&#1090;&#1088;&#1080;&#1073;&#1091;&#1090;&#1080; &#1089;&#1090;&#1072;&#1085;&#1091;, &#1090;&#1074;&#1086;&#1088;&#1077;&#1085;&#1085;&#1103; &#1095;&#1086;&#1075;&#1086;&#1089;&#1100;
    </p>
    <p>
      &#1076;&#1083;&#1103; &#1076;&#1086;&#1089;&#1103;&#1075;&#1085;&#1077;&#1085;&#1085;&#1103; &#1082;&#1086;&#1085;&#1082;&#1088;&#1077;&#1090;&#1085;&#1086;&#1075;&#1086; &#1088;&#1077;&#1079;&#1091;&#1083;&#1100;&#1090;&#1072;&#1090;&#1091;
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true"/>
</node>
</node>
<node TEXT="&#x41f;&#x435;&#x440;&#x435;&#x432;&#x430;&#x433;&#x438;" STYLE_REF="second_node" POSITION="right" ID="ID_827532386" CREATED="1418999954995" MODIFIED="1419002529740" HGAP="110" VSHIFT="-18">
<icon BUILTIN="addition"/>
<edge COLOR="#339900"/>
<node TEXT="&#x41d;&#x438;&#x437;&#x44c;&#x43a;&#x438;&#x439; &#x43f;&#x43e;&#x440;&#x456;&#x433; &#x432;&#x445;&#x43e;&#x434;&#x436;&#x435;&#x43d;&#x43d;&#x44f;" STYLE_REF="pre_last_node" ID="ID_951126511" CREATED="1419000028207" MODIFIED="1419000225985"/>
<node TEXT="&#x41f;&#x435;&#x440;&#x435;&#x434;&#x431;&#x430;&#x447;&#x443;&#x432;&#x430;&#x43d;&#x456;&#x441;&#x442;&#x44c;" STYLE_REF="pre_last_node" ID="ID_531355232" CREATED="1419000042630" MODIFIED="1419000206958">
<hook NAME="FirstGroupNode"/>
<node TEXT="&#x412;&#x445;&#x456;&#x434;&#x43d;&#x456; &#x434;&#x430;&#x43d;&#x456;" ID="ID_1261309651" CREATED="1419000053710" MODIFIED="1419000075085">
<hook NAME="FirstGroupNode"/>
</node>
<node TEXT="Current state" ID="ID_92210228" CREATED="1419000060813" MODIFIED="1419000075080">
<hook NAME="FirstGroupNode"/>
</node>
</node>
<node TEXT="Transition" ID="ID_1215598881" CREATED="1419000206952" MODIFIED="1419000220061">
<hook NAME="SummaryNode"/>
</node>
<node TEXT="&#x428;&#x432;&#x438;&#x434;&#x43a;&#x43e; &#x440;&#x43e;&#x437;&#x440;&#x43e;&#x431;&#x43b;&#x44f;&#x454;&#x442;&#x44c;&#x441;&#x44f; &#x456; &#x440;&#x435;&#x430;&#x43b;&#x456;&#x437;&#x443;&#x454;&#x442;&#x44c;&#x441;&#x44f;" STYLE_REF="pre_last_node" ID="ID_906019384" CREATED="1419000094825" MODIFIED="1419000172044"/>
<node TEXT="&#x421;&#x442;&#x430;&#x440;&#x430; &#x43f;&#x435;&#x440;&#x435;&#x432;&#x456;&#x440;&#x435;&#x43d;&#x430; &#x442;&#x435;&#x445;&#x43d;&#x43e;&#x43b;&#x43e;&#x433;&#x456;&#x44f;" STYLE_REF="pre_last_node" ID="ID_1430771341" CREATED="1419000114393" MODIFIED="1419000172041"/>
<node TEXT="&#x41c;&#x430;&#x43b;&#x43e; &#x436;&#x440;&#x435; &#x43f;&#x440;&#x43e;&#x446;&#x435;&#x441;&#x43e;&#x440;&#x43d;&#x456;&#x439; &#x447;&#x430;&#x441;" STYLE_REF="pre_last_node" ID="ID_23685322" CREATED="1419000125512" MODIFIED="1419000172038"/>
<node TEXT="&#x414;&#x43e;&#x441;&#x438;&#x442;&#x44c; &#x433;&#x43d;&#x443;&#x447;&#x43a;&#x430; &#x442;&#x435;&#x445;&#x43d;&#x43e;&#x43b;&#x43e;&#x433;&#x456;&#x44f;" STYLE_REF="pre_last_node" ID="ID_363680579" CREATED="1419000139971" MODIFIED="1419000172035"/>
<node TEXT="&#x41b;&#x435;&#x433;&#x43a;&#x43e; &#x440;&#x435;&#x430;&#x43b;&#x456;&#x437;&#x443;&#x432;&#x430;&#x442;&#x438; &#x430;&#x431;&#x441;&#x442;&#x440;&#x430;&#x43a;&#x442;&#x43d;&#x435; &#x43f;&#x440;&#x435;&#x434;&#x441;&#x442;&#x430;&#x432;&#x43b;&#x435;&#x43d;&#x43d;&#x44f;" STYLE_REF="pre_last_node" ID="ID_385628693" CREATED="1419000153366" MODIFIED="1419000172026"/>
</node>
<node TEXT="&#x41d;&#x435;&#x434;&#x43e;&#x43b;&#x456;&#x43a;&#x438;" STYLE_REF="second_node" POSITION="right" ID="ID_1734881801" CREATED="1418999997954" MODIFIED="1419002532386" HGAP="77" VSHIFT="-50">
<icon BUILTIN="subtraction"/>
<edge COLOR="#ff0000"/>
<node TEXT="&#x41f;&#x435;&#x440;&#x435;&#x434;&#x431;&#x430;&#x447;&#x443;&#x432;&#x430;&#x43d;&#x430; &#x43f;&#x440;&#x438;&#x440;&#x43e;&#x434;&#x430; FSM &#x43d;&#x435; &#x431;&#x430;&#x436;&#x430;&#x43d;&#x430; &#x434;&#x43b;&#x44f; &#x456;&#x433;&#x43e;&#x440;" STYLE_REF="pre_last_node" ID="ID_39706614" CREATED="1419000233412" MODIFIED="1419000365139"/>
<node TEXT="&#x412;&#x435;&#x43b;&#x438;&#x43a;&#x456; &#x441;&#x438;&#x441;&#x442;&#x435;&#x43c;&#x438; &#x43c;&#x43e;&#x436;&#x443;&#x442;&#x44c; &#x431;&#x443;&#x442;&#x438; &#x434;&#x43e;&#x441;&#x438;&#x442;&#x44c; &#x441;&#x43a;&#x43b;&#x430;&#x434;&#x43d;&#x456;&#xa;transitions &#x43c;&#x43e;&#x436;&#x443;&#x442;&#x44c; &#x434;&#x430;&#x432;&#x430;&#x442;&#x438; &#x441;&#x43f;&#x430;&#x43a;&#x435;&#x442;&#x442;&#x456;-&#x444;&#x430;&#x43a;&#x442;&#x43e;&#x440;" STYLE_REF="pre_last_node" ID="ID_1375728920" CREATED="1419000261776" MODIFIED="1419000365126"/>
<node TEXT="&#x41d;&#x435; &#x43f;&#x456;&#x434;&#x445;&#x43e;&#x434;&#x438;&#x442;&#x44c; &#x434;&#x43b;&#x44f; &#x432;&#x441;&#x456;&#x445; &#x43e;&#x431;&#x43b;&#x430;&#x441;&#x442;&#x435;&#x439;, &#x43b;&#x438;&#x448;&#x435; &#x43a;&#x43e;&#x43b;&#x438; &#x43f;&#x43e;&#x432;&#x435;&#x434;&#x456;&#x43d;&#x43a;&#x430;&#xa;&#x440;&#x43e;&#x437;&#x43a;&#x43b;&#x430;&#x434;&#x430;&#x454;&#x442;&#x44c;&#x441;&#x44f; &#x43d;&#x430; &#x441;&#x442;&#x430;&#x43d;&#x438;, &#x456;&#x437; &#x432;&#x438;&#x437;&#x43d;&#x430;&#x447; &#x443;&#x43c;&#x43e;&#x432;&#x430;&#x43c;&#x438; &#x434;&#x43b;&#x44f; &#x43f;&#x435;&#x440;&#x435;&#x445;&#x43e;&#x434;&#x456;&#x432;" STYLE_REF="pre_last_node" ID="ID_344540329" CREATED="1419000297727" MODIFIED="1419000365117"/>
<node TEXT="&#x423;&#x43c;&#x43e;&#x432;&#x438; &#x43f;&#x435;&#x440;&#x435;&#x445;&#x43e;&#x434;&#x456;&#x432; &#x43c;&#x456;&#x436; &#x441;&#x442;&#x430;&#x43d;&#x430;&#x43c;&#x438; &#x432;&#x438;&#x437;&#x43d;&#x430;&#x447;&#x435;&#x43d;&#x456; &#x456; &#x444;&#x456;&#x43a;&#x441;&#x43e;&#x432;&#x430;&#x43d;&#x456; &#x434;&#x43b;&#x44f; FSM" STYLE_REF="pre_last_node" ID="ID_1133575564" CREATED="1419000337790" MODIFIED="1419000365111"/>
</node>
</node>
</map>
