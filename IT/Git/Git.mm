<map version="freeplane 1.3.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Git" STYLE_REF="main_node" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1429602317295">
<hook NAME="AutomaticEdgeColor" COUNTER="11"/>
<hook NAME="MapStyle" zoom="1.003">
    <properties show_icon_for_attributes="true" show_note_icons="true"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
<stylenode TEXT="main_node" BACKGROUND_COLOR="#ccccff">
<font SIZE="18" BOLD="true"/>
<edge STYLE="sharp_bezier" WIDTH="4"/>
</stylenode>
<stylenode TEXT="second_node" BACKGROUND_COLOR="#00cccc" STYLE="bubble">
<font SIZE="14"/>
</stylenode>
<stylenode TEXT="third_node" BACKGROUND_COLOR="#ffbdf4" STYLE="bubble">
<font SIZE="11" BOLD="true"/>
</stylenode>
<stylenode TEXT="orange_node" BACKGROUND_COLOR="#ff9900" STYLE="bubble">
<font BOLD="true"/>
</stylenode>
<stylenode TEXT="gray_node" BACKGROUND_COLOR="#9ec19e">
<font BOLD="true"/>
</stylenode>
<stylenode TEXT="yellow_node" BACKGROUND_COLOR="#ffff66" STYLE="bubble">
<edge WIDTH="1"/>
</stylenode>
<stylenode TEXT="example_node" BACKGROUND_COLOR="#afcdfa">
<edge STYLE="bezier" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="pre_last_node" BACKGROUND_COLOR="#99ff99" STYLE="bubble">
<edge STYLE="bezier" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="last_node" BACKGROUND_COLOR="#ffffff">
<edge STYLE="bezier" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="warning_node" BACKGROUND_COLOR="#fda6a6">
<icon BUILTIN="messagebox_warning"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook URI="../../git-logo-large.png" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Basics" STYLE_REF="second_node" POSITION="left" ID="ID_700289052" CREATED="1429087048429" MODIFIED="1430110124590">
<edge COLOR="#00007c"/>
<hook URI="../../Images/basics.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="About Version Control" STYLE_REF="orange_node" ID="ID_1564036574" CREATED="1429858780848" MODIFIED="1429859420091">
<node TEXT="Local Version Control Systems" STYLE_REF="gray_node" ID="ID_339833959" CREATED="1429858790104" MODIFIED="1429859428757">
<node TEXT="Many people&#x2019;s version-control method of choice is to copy files into another directory (perhaps a time-stamped directory, if they&#x2019;re clever). This approach is very common because it is so simple, but it is also incredibly error prone. It is easy to forget which directory you&#x2019;re in and accidentally write to the wrong file or copy over files you don&#x2019;t mean to" STYLE_REF="pre_last_node" ID="ID_1575021205" CREATED="1429858893391" MODIFIED="1429859448321"/>
<node TEXT="To deal with this issue, programmers long ago developed local VCSs that had a simple database that kept all the changes to files under revision control" STYLE_REF="pre_last_node" ID="ID_967684340" CREATED="1429858899735" MODIFIED="1429859448321"/>
<node TEXT="" STYLE_REF="pre_last_node" ID="ID_1161908182" CREATED="1429858914888" MODIFIED="1429859448321">
<hook URI="vc1.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
</node>
<node TEXT="Centralized Version Control Systems" STYLE_REF="gray_node" ID="ID_739342741" CREATED="1429858912104" MODIFIED="1429859460195">
<node TEXT="These systems, such as CVS, Subversion, and Perforce, have a single server that contains all the versioned files, and a number of clients that check out files from that central place. For many years, this has been the standard for version control" STYLE_REF="pre_last_node" ID="ID_586489058" CREATED="1429859062864" MODIFIED="1429859448336"/>
<node TEXT="Advantages" STYLE_REF="yellow_node" ID="ID_596614555" CREATED="1429859127607" MODIFIED="1429859453055">
<node TEXT="Everyone knows to a certain degree what everyone else on the project is doing" STYLE_REF="pre_last_node" ID="ID_1784096971" CREATED="1429859154592" MODIFIED="1429859448336"/>
<node TEXT="Administrators have fine-grained control over who can do what; and it&#x2019;s far easier to administer a CVCS than it is to deal with local databases on every client" STYLE_REF="pre_last_node" ID="ID_63380468" CREATED="1429859176808" MODIFIED="1429859448336"/>
</node>
<node TEXT="Downsides" STYLE_REF="yellow_node" ID="ID_596193061" CREATED="1429859186160" MODIFIED="1429859453055">
<node TEXT="The most obvious is the single point of failure that the centralized server represents. If that server goes down for an hour, then during that hour nobody can collaborate at all or save versioned changes to anything they&#x2019;re working on. If the hard disk the central database is on becomes corrupted, and proper backups haven&#x2019;t been kept, you lose absolutely everything&#x2014;the entire history of the project except whatever single snapshots people happen to have on their local machines" STYLE_REF="pre_last_node" ID="ID_1212344801" CREATED="1429859267977" MODIFIED="1429859448336"/>
</node>
<node TEXT="" STYLE_REF="pre_last_node" ID="ID_215382828" CREATED="1429859383023" MODIFIED="1429859460195">
<hook URI="vc2.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
</node>
<node TEXT="Distributed Version Control Systems" STYLE_REF="gray_node" ID="ID_883158641" CREATED="1429859295281" MODIFIED="1429859428757">
<node TEXT="clients don&#x2019;t just check out the latest snapshot of the files: they fully mirror the repository. Thus if any server dies, and these systems were collaborating via it, any of the client repositories can be copied back up to the server to restore it" STYLE_REF="pre_last_node" ID="ID_1957150569" CREATED="1429859346751" MODIFIED="1429859448321"/>
<node TEXT="" STYLE_REF="pre_last_node" ID="ID_1449053432" CREATED="1429859400927" MODIFIED="1429859448336">
<hook URI="vc3.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
</node>
</node>
<node TEXT="Getting a Git Repository" STYLE_REF="orange_node" ID="ID_532693122" CREATED="1428908030602" MODIFIED="1429087083684">
<node TEXT="Initializing a Repository" STYLE_REF="gray_node" ID="ID_1220062915" CREATED="1428908076273" MODIFIED="1429087097028">
<node TEXT="Go to the project&#x2019;s directory and type:" STYLE_REF="pre_last_node" ID="ID_1426075411" CREATED="1428908155762" MODIFIED="1428908523636"/>
<node TEXT="$ git init" STYLE_REF="example_node" ID="ID_828353922" CREATED="1428908189594" MODIFIED="1428908478807">
<node TEXT="This creates a new subdirectory named .git that&#xa;contains all of your necessary repository files" STYLE_REF="pre_last_node" ID="ID_571485496" CREATED="1428908225138" MODIFIED="1428908492654"/>
</node>
<node TEXT="$ git add *.c&#xa;$ git add README&#xa;$ git commit -m &apos;initial project version&apos;" STYLE_REF="example_node" ID="ID_1438668830" CREATED="1428908473081" MODIFIED="1428908478806">
<node TEXT="To start version-controlling existing files, just do initial commit" STYLE_REF="pre_last_node" ID="ID_868033777" CREATED="1428908438627" MODIFIED="1428908492653"/>
</node>
</node>
<node TEXT="Cloning an Existing Repository" STYLE_REF="gray_node" ID="ID_1505187428" CREATED="1428908414139" MODIFIED="1429087097027">
<node TEXT="git clone [url]" STYLE_REF="pre_last_node" ID="ID_1285830229" CREATED="1428908660442" MODIFIED="1428908674940"/>
<node TEXT="$ git clone git://github.com/schacon/grit.git" STYLE_REF="example_node" ID="ID_1753444302" CREATED="1428908723714" MODIFIED="1428908804964"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Clones to current directory
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="$ git clone git://github.com/schacon/grit.git mygrit" STYLE_REF="example_node" ID="ID_1833490439" CREATED="1428908767010" MODIFIED="1428908804965"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Clones to mygrit directory
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Git receives a copy of nearly all data that the server has" STYLE_REF="pre_last_node" ID="ID_1868084946" CREATED="1428908798384" MODIFIED="1428908842662">
<node TEXT="if your server disk gets corrupted, you can use any of the clones on any client to set the server back to the state it was in when it was cloned" ID="ID_1698207266" CREATED="1428908837156" MODIFIED="1428908838539"/>
</node>
</node>
</node>
<node TEXT="Recording Changes to the Repository" STYLE_REF="orange_node" ID="ID_1179484438" CREATED="1428908932025" MODIFIED="1429087083684">
<node TEXT="File status lifecycle" STYLE_REF="gray_node" ID="ID_692797415" CREATED="1428909009322" MODIFIED="1429087169442"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      each file in your working directory
    </p>
    <p>
      can be in one of two states
    </p>
  </body>
</html>
</richcontent>
<hook URI="./file-status-lifecycle.png" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Tracked" STYLE_REF="orange_node" ID="ID_581499485" CREATED="1428909025995" MODIFIED="1429087182738"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      are files that were in the last snapshot
    </p>
  </body>
</html>
</richcontent>
<font BOLD="false"/>
<node TEXT="Unmodified" STYLE_REF="yellow_node" ID="ID_497085331" CREATED="1428909058625" MODIFIED="1428909287029">
<node TEXT="When you first clone a repository, all of your files will be tracked and unmodified because you just checked them out and haven&#x2019;t edited anything" STYLE_REF="pre_last_node" ID="ID_1589846724" CREATED="1428909146999" MODIFIED="1428909291437"/>
</node>
<node TEXT="Modified" STYLE_REF="yellow_node" ID="ID_445187699" CREATED="1428909067520" MODIFIED="1428909287029">
<node TEXT="As you edit files, Git sees them as modified" STYLE_REF="pre_last_node" ID="ID_956350313" CREATED="1428909222697" MODIFIED="1428909291438"/>
</node>
<node TEXT="Staged" STYLE_REF="yellow_node" ID="ID_611030277" CREATED="1428909078135" MODIFIED="1428909287028"/>
</node>
<node TEXT="Untracked" STYLE_REF="orange_node" ID="ID_1773209875" CREATED="1428909105632" MODIFIED="1429087182738"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      are everything else
    </p>
  </body>
</html>
</richcontent>
<font BOLD="false"/>
</node>
</node>
<node TEXT="Checking the Status of Your Files" STYLE_REF="gray_node" ID="ID_1368464305" CREATED="1428909351468" MODIFIED="1429087097029">
<node TEXT="$ git status" STYLE_REF="example_node" ID="ID_1619211528" CREATED="1428909373434" MODIFIED="1428909539934"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      On branch master
    </p>
    <p>
      Untracked files:
    </p>
    <p>
      &#160;&#160;(use &quot;git add &lt;file&gt;...&quot; to include in what will be committed)
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;README
    </p>
    <p>
      nothing added to commit but untracked files present (use &quot;git add&quot; to track)
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Tracking New Files" STYLE_REF="gray_node" ID="ID_1874517475" CREATED="1428909548464" MODIFIED="1429087097029">
<node TEXT="$ git add README" STYLE_REF="example_node" ID="ID_1767986731" CREATED="1428909576289" MODIFIED="1428909743829"/>
<node TEXT="Recursively add the entire folder" STYLE_REF="yellow_node" ID="ID_643904047" CREATED="1425647931814" MODIFIED="1425647974746">
<node TEXT="$ git add -A" STYLE_REF="example_node" ID="ID_450463654" CREATED="1427445921722" MODIFIED="1428909758403"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Stages All
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="$ git add ." STYLE_REF="example_node" ID="ID_159686965" CREATED="1427445943618" MODIFIED="1428909758404"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Stages new and modified, without deleted
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="$ git add -u" STYLE_REF="example_node" ID="ID_302498100" CREATED="1427445935250" MODIFIED="1428909758404"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Stages modified and deleted, without new
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="Staging Modified Files" STYLE_REF="gray_node" ID="ID_905222366" CREATED="1428909970481" MODIFIED="1429087097029">
<node TEXT="Let&#x2019;s change a file that was already tracked benchmarks.rb" STYLE_REF="pre_last_node" ID="ID_1712604395" CREATED="1428909988466" MODIFIED="1428910254868"/>
<node TEXT="$ git status" STYLE_REF="example_node" ID="ID_1955547591" CREATED="1428910005536" MODIFIED="1428910218494"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      On branch master
    </p>
    <p>
      Changes to be committed:
    </p>
    <p>
      &#160;&#160;(use &quot;git reset HEAD &lt;file&gt;...&quot; to unstage)
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;new file:&#160;&#160;&#160;README
    </p>
    <p>
      
    </p>
    <p>
      Changes not staged for commit:
    </p>
    <p>
      &#160;&#160;(use &quot;git add &lt;file&gt;...&quot; to update what will be committed)
    </p>
    <p>
      &#160;&#160;(use &quot;git checkout -- &lt;file&gt;...&quot; to discard changes in working directory)
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;modified:&#160;&#160;&#160;benchmarks.rb
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="The file that is tracked has been modified in the&#xa;working directory but not yet staged" STYLE_REF="pre_last_node" ID="ID_1334507521" CREATED="1428910036263" MODIFIED="1428910271528"/>
<node TEXT="$ git add benchmarks.rb" STYLE_REF="example_node" ID="ID_1909005537" CREATED="1428910061084" MODIFIED="1428910218500"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      $ git status
    </p>
    <p>
      On branch master
    </p>
    <p>
      Changes to be committed:
    </p>
    <p>
      &#160;&#160;(use &quot;git reset HEAD &lt;file&gt;...&quot; to unstage)
    </p>
    <p>
      
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;new file:&#160;&#160;&#160;README
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;modified:&#160;&#160;&#160;benchmarks.rb
    </p>
  </body>
</html>
</richcontent>
<node TEXT="To stage it" ID="ID_363713239" CREATED="1428910067129" MODIFIED="1428910070211"/>
</node>
<node TEXT="$ vim benchmarks.rb&#xa;$ git status&#xa;On branch master&#xa;Changes to be committed:&#xa;  (use &quot;git reset HEAD &lt;file&gt;...&quot; to unstage)&#xa;&#xa;        new file:   README&#xa;        modified:   benchmarks.rb&#xa;&#xa;Changes not staged for commit:&#xa;  (use &quot;git add &lt;file&gt;...&quot; to update what will be committed)&#xa;  (use &quot;git checkout -- &lt;file&gt;...&quot; to discard changes in working directory)&#xa;&#xa;        modified:   benchmarks.rb" STYLE_REF="example_node" ID="ID_709682050" CREATED="1428910117369" MODIFIED="1428910218500">
<node TEXT="Change it again" ID="ID_1171393189" CREATED="1428910291054" MODIFIED="1428910292037"/>
</node>
<node TEXT=" Now benchmarks.rb is listed as both staged and unstaged" STYLE_REF="pre_last_node" ID="ID_1602189247" CREATED="1428910173152" MODIFIED="1428910304014"/>
<node TEXT="If you commit now, the version of benchmarks.rb as it was when you last ran the git add command is how it will go into the commit" STYLE_REF="pre_last_node" ID="ID_1414620967" CREATED="1428910193028" MODIFIED="1428910304013"/>
</node>
<node TEXT="Ignoring Files" STYLE_REF="gray_node" ID="ID_1264079072" CREATED="1428910336097" MODIFIED="1429087097029">
<node TEXT="Add such files to .gitignore file" STYLE_REF="pre_last_node" ID="ID_412660059" CREATED="1428910366344" MODIFIED="1428910397460"/>
<node TEXT="$ cat .gitignore&#xa;*.[oa]&#xa;*~" STYLE_REF="example_node" ID="ID_598107291" CREATED="1428910382859" MODIFIED="1428910581092">
<node TEXT="Ignore any files ending in .o or .a" STYLE_REF="pre_last_node" ID="ID_547230413" CREATED="1428910413580" MODIFIED="1428910597958"/>
<node TEXT="Ignore any files end tilde (~)" STYLE_REF="pre_last_node" ID="ID_256452193" CREATED="1428910455025" MODIFIED="1428910597958"/>
</node>
<node TEXT="Patterns" STYLE_REF="yellow_node" ID="ID_1266874734" CREATED="1428910541822" MODIFIED="1428910584821">
<node TEXT="Blank lines or lines starting with # are ignored" STYLE_REF="pre_last_node" ID="ID_732901905" CREATED="1428910550693" MODIFIED="1428910597958"/>
<node TEXT="Standard glob patterns work" STYLE_REF="pre_last_node" ID="ID_1570716633" CREATED="1428910550693" MODIFIED="1428910597957"/>
<node TEXT="You can end patterns with a forward slash (/) to specify a directory" STYLE_REF="pre_last_node" ID="ID_1370288024" CREATED="1428910550694" MODIFIED="1428910597957"/>
<node TEXT="You can negate a pattern by starting it with an exclamation point (!)" STYLE_REF="pre_last_node" ID="ID_1288240793" CREATED="1428910550694" MODIFIED="1428910597958"/>
</node>
<node TEXT="# a comment - this is ignored&#xa;# no .a files&#xa;*.a&#xa;# but do track lib.a, even though you&apos;re ignoring .a files above&#xa;!lib.a&#xa;# only ignore the root TODO file, not subdir/TODO&#xa;/TODO&#xa;# ignore all files in the build/ directory&#xa;build/&#xa;# ignore doc/notes.txt, but not doc/server/arch.txt&#xa;doc/*.txt&#xa;# ignore all .txt files in the doc/ directory&#xa;doc/**/*.txt" STYLE_REF="example_node" ID="ID_1061044781" CREATED="1428910572154" MODIFIED="1428996102910"/>
</node>
<node TEXT="Viewing Your Staged and Unstaged Changes" STYLE_REF="gray_node" ID="ID_1852367511" CREATED="1428910679027" MODIFIED="1429087102323">
<node TEXT="$ git diff" STYLE_REF="example_node" ID="ID_1793110348" CREATED="1428911464446" MODIFIED="1428995609317"><richcontent TYPE="DETAILS" HIDDEN="true">

<html>
  <head>
    
  </head>
  <body>
    <p>
      diff --git a/benchmarks.rb b/benchmarks.rb
    </p>
    <p>
      index 3cb747f..da65585 100644
    </p>
    <p>
      --- a/benchmarks.rb
    </p>
    <p>
      +++ b/benchmarks.rb
    </p>
    <p>
      @@ -36,6 +36,10 @@ def main
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;@commit.parents[0].parents[0].parents[0]
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;end
    </p>
    <p>
      
    </p>
    <p>
      +&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;run_code(x, 'commits 1') do
    </p>
    <p>
      +&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;git.commits.size
    </p>
    <p>
      +&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;end
    </p>
    <p>
      +
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;run_code(x, 'commits 2') do
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;log = git.commits('master', 15)
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;log.size
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Shows you the exact lines added and removed" STYLE_REF="pre_last_node" ID="ID_262478916" CREATED="1428911476340" MODIFIED="1428995594678"/>
<node TEXT="To see what you&#x2019;ve changed but not yet staged" STYLE_REF="pre_last_node" ID="ID_1028585528" CREATED="1428912124454" MODIFIED="1428995594677"/>
</node>
<node TEXT="$ git diff --staged&#xa;$ git diff --cached // the same" STYLE_REF="example_node" ID="ID_1175481190" CREATED="1428995375769" MODIFIED="1429087102322"><richcontent TYPE="DETAILS" HIDDEN="true">

<html>
  <head>
    
  </head>
  <body>
    <p>
      diff --git a/README b/README
    </p>
    <p>
      new file mode 100644
    </p>
    <p>
      index 0000000..03902a1
    </p>
    <p>
      --- /dev/null
    </p>
    <p>
      +++ b/README2
    </p>
    <p>
      @@ -0,0 +1,5 @@
    </p>
    <p>
      +grit
    </p>
    <p>
      + by Tom Preston-Werner, Chris Wanstrath
    </p>
    <p>
      + http://github.com/mojombo/grit
    </p>
    <p>
      +
    </p>
    <p>
      +Grit is a Ruby library for extracting information from a Git repository
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Compares your staged changes to your last commit" STYLE_REF="pre_last_node" ID="ID_704769081" CREATED="1428995367337" MODIFIED="1428995594678"/>
</node>
</node>
<node TEXT="Committing Your Changes" STYLE_REF="gray_node" ID="ID_1373745441" CREATED="1428995573967" MODIFIED="1435305601303">
<node TEXT="$ git commit" STYLE_REF="example_node" ID="ID_602518527" CREATED="1428995698776" MODIFIED="1428995889046">
<node TEXT="Anything that is still unstaged won&#x2019;t go into this commit" STYLE_REF="pre_last_node" ID="ID_1109877095" CREATED="1428995717737" MODIFIED="1428996088639"/>
<node TEXT="Git will lanch editor for typing the comment" STYLE_REF="pre_last_node" ID="ID_1598471870" CREATED="1428995857201" MODIFIED="1428996088639"/>
</node>
<node TEXT="$ git commit -m &quot;Commit message&quot;" STYLE_REF="example_node" ID="ID_1846133975" CREATED="1428995828873" MODIFIED="1435305601302"/>
<node TEXT="$ git commit -a" STYLE_REF="example_node" ID="ID_59556887" CREATED="1428996026537" MODIFIED="1428996093125">
<node TEXT="Git automatically stage every file that is already tracked&#xa;before doing the commit, letting you skip the git add part" STYLE_REF="pre_last_node" ID="ID_1536727143" CREATED="1428996051128" MODIFIED="1428996088638"/>
</node>
</node>
<node TEXT="Removing Files" STYLE_REF="gray_node" ID="ID_668911543" CREATED="1428996081185" MODIFIED="1429936687171">
<icon BUILTIN="button_cancel"/>
<node TEXT="$ git rm file.txt" STYLE_REF="example_node" ID="ID_1281735385" CREATED="1428996203641" MODIFIED="1428996572927">
<node TEXT="The next time you commit, the file will be gone and no longer tracked" STYLE_REF="pre_last_node" ID="ID_1942039500" CREATED="1428996276304" MODIFIED="1428996581518"/>
<node TEXT="File will be deleted from file system too" STYLE_REF="pre_last_node" ID="ID_144872533" CREATED="1428996349601" MODIFIED="1428996581518"/>
</node>
<node TEXT="$ git rm -f file.txt" STYLE_REF="example_node" ID="ID_468019303" CREATED="1428996512353" MODIFIED="1429936687171">
<node TEXT="If you modified the file and added it to the&#xa;index already, you must force the removal" STYLE_REF="pre_last_node" ID="ID_450898490" CREATED="1428996530505" MODIFIED="1428996581519"/>
<node TEXT="This is a safety feature to prevent accidental removal of data that hasn&#x2019;t&#xa;yet been recorded in a snapshot and that can&#x2019;t be recovered from Git" STYLE_REF="pre_last_node" ID="ID_1112239080" CREATED="1428996553689" MODIFIED="1428996611629"/>
</node>
<node TEXT="$ git rm --cached readme.txt" STYLE_REF="example_node" ID="ID_824618449" CREATED="1428996338633" MODIFIED="1428996572927">
<node TEXT="To keep the file on your hard drive but not have Git track it anymore" STYLE_REF="pre_last_node" ID="ID_1924203768" CREATED="1428996391120" MODIFIED="1428996581518"/>
</node>
<node TEXT="$ git rm log/\*.log" STYLE_REF="example_node" ID="ID_318552841" CREATED="1428996447721" MODIFIED="1428996572926">
<node TEXT="On Windows with the system console, the backslash must be omitted" STYLE_REF="pre_last_node" ID="ID_1946248017" CREATED="1428996471473" MODIFIED="1428996581518"/>
<node TEXT="Removes all files that have the .log extension in the log/ directory" STYLE_REF="pre_last_node" ID="ID_374030972" CREATED="1428996482200" MODIFIED="1428996581519"/>
</node>
<node TEXT="$ git rm -r directoryName" STYLE_REF="example_node" ID="ID_1243898525" CREATED="1429936721314" MODIFIED="1429936948724">
<node TEXT="To remove directory recursively" STYLE_REF="pre_last_node" ID="ID_1366327414" CREATED="1429936737769" MODIFIED="1429936750823"/>
</node>
</node>
<node TEXT="Moving Files" STYLE_REF="gray_node" ID="ID_1114875051" CREATED="1428996700952" MODIFIED="1430103148544">
<node TEXT="$ git mv file_from file_to" STYLE_REF="example_node" ID="ID_1155540513" CREATED="1428996707201" MODIFIED="1428996823912">
<node TEXT="To rename a file" STYLE_REF="pre_last_node" ID="ID_1325204823" CREATED="1428996718544" MODIFIED="1428996831845"/>
</node>
<node TEXT="$ git mv README README.txt" STYLE_REF="example_node" ID="ID_1411460568" CREATED="1428996802328" MODIFIED="1428996823911">
<hook NAME="FirstGroupNode"/>
</node>
<node TEXT="$ mv README README.txt&#xa;$ git rm README&#xa;$ git add README.txt" STYLE_REF="example_node" ID="ID_856223688" CREATED="1428996792080" MODIFIED="1430103148544"/>
<node TEXT="The same" STYLE_REF="pre_last_node" ID="ID_120830222" CREATED="1428996813749" MODIFIED="1428996831846">
<hook NAME="SummaryNode"/>
</node>
<node TEXT="$ git mv hello.html lib" STYLE_REF="example_node" ID="ID_66986412" CREATED="1430102573354" MODIFIED="1430102614576"><richcontent TYPE="DETAILS" HIDDEN="true">

<html>
  <head>
    
  </head>
  <body>
    <p>
      # On branch master
    </p>
    <p>
      # Changes to be committed:
    </p>
    <p>
      #&#160;&#160;&#160;(use &quot;git reset HEAD &lt;file&gt;...&quot; to unstage)
    </p>
    <p>
      #
    </p>
    <p>
      # renamed:&#160;&#160;&#160;&#160;hello.html -&gt; lib/hello.html
    </p>
    <p>
      #
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="Viewing the Commit History" STYLE_REF="orange_node" ID="ID_1783939424" CREATED="1429020366561" MODIFIED="1429087083683">
<node TEXT="Viewing the Commit History" STYLE_REF="gray_node" ID="ID_325878616" CREATED="1429073935218" MODIFIED="1429087112587">
<node TEXT="git log" STYLE_REF="example_node" ID="ID_942156821" CREATED="1429073926626" MODIFIED="1429081018356"><richcontent TYPE="DETAILS" HIDDEN="true">

<html>
  <head>
    
  </head>
  <body>
    <p>
      commit ca3601a9df4a299b1918900581048684e30cac78
    </p>
    <p>
      Author: Aleksandr Albul &lt;abdula.ua@gmail.com&gt;
    </p>
    <p>
      Date:&#160;&#160;&#160;Tue Apr 14 17:31:04 2015 +0300
    </p>
    <p>
      
    </p>
    <p>
      &#160;&#160;&#160;&#160;MenuPanelShareOptions almost done
    </p>
    <p>
      
    </p>
    <p>
      commit 0c3d2100a3101eb1c0b41fd4b8f884e7a0af2e79
    </p>
    <p>
      Merge: f377bfc 0befb6c
    </p>
    <p>
      Author: Mateo Kozomara &lt;mateo.kozomara@gmail.com&gt;
    </p>
    <p>
      Date:&#160;&#160;&#160;Tue Apr 14 16:04:33 2015 +0200
    </p>
    <p>
      
    </p>
    <p>
      &#160;&#160;&#160;&#160;Merge branch 'master' of https://github.com/salbeik/21_AuthorTool
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Shows the commits made in that repository in reverse chronological order" STYLE_REF="pre_last_node" ID="ID_213212921" CREATED="1429080868608" MODIFIED="1429081017292"/>
</node>
<node TEXT="-p" STYLE_REF="yellow_node" ID="ID_634903371" CREATED="1429082206712" MODIFIED="1429082412783">
<node TEXT="Shows the diff introduced in each commit" STYLE_REF="pre_last_node" ID="ID_111294148" CREATED="1429080902504" MODIFIED="1429082240091"/>
<node TEXT="$ git log -p -2" STYLE_REF="example_node" ID="ID_763118387" CREATED="1429080890928" MODIFIED="1429080984222">
<node TEXT="-2 limits the output" STYLE_REF="pre_last_node" ID="ID_1835138634" CREATED="1429081003319" MODIFIED="1429081017293"/>
</node>
</node>
<node TEXT="--word-diff" STYLE_REF="yellow_node" ID="ID_99849678" CREATED="1429082226799" MODIFIED="1429082412782">
<node TEXT="Shwos word diff instead of normal line by line diff" STYLE_REF="pre_last_node" ID="ID_1786714124" CREATED="1429081146279" MODIFIED="1429082254396"/>
<node TEXT="$ git log -U1 --word-diff" STYLE_REF="example_node" ID="ID_1141993143" CREATED="1429081035751" MODIFIED="1429081490925"/>
</node>
<node TEXT="--stat" STYLE_REF="yellow_node" ID="ID_521453852" CREATED="1429082262864" MODIFIED="1429082412782">
<node TEXT="Shows list of modified files, how many files were changed,&#xa;and how many lines in those files were added and removed" STYLE_REF="pre_last_node" ID="ID_707303844" CREATED="1429081276951" MODIFIED="1429081495460"/>
<node TEXT="$ git log --stat" STYLE_REF="example_node" ID="ID_661628558" CREATED="1429081263919" MODIFIED="1429082270355"><richcontent TYPE="DETAILS" HIDDEN="true">

<html>
  <head>
    
  </head>
  <body>
    <p>
      commit ca3601a9df4a299b1918900581048684e30cac78
    </p>
    <p>
      Author: Aleksandr Albul &lt;abdula.ua@gmail.com&gt;
    </p>
    <p>
      Date:&#160;&#160;&#160;Tue Apr 14 17:31:04 2015 +0300
    </p>
    <p>
      
    </p>
    <p>
      &#160;&#160;&#160;&#160;MenuPanelShareOptions almost done
    </p>
    <p>
      
    </p>
    <p>
      &#160;.../view/components/list/CustomList.as&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;|&#160;&#160;&#160;6 +-
    </p>
    <p>
      &#160;.../dashboard/tools_menu/MenuPanelShareOptions.as&#160;&#160;| 104 +++++++++++------
    </p>
    <p>
      &#160;.../view/dashboard/tools_menu/ToolsMenuUI.as&#160;&#160;&#160;&#160;&#160;&#160;&#160;|&#160;&#160;&#160;4 +-
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="--shortstat" STYLE_REF="yellow_node" ID="ID_1002937129" CREATED="1429082283976" MODIFIED="1429082412782">
<node TEXT="Display only the changed/insertions/deletions line from the --stat command" STYLE_REF="pre_last_node" ID="ID_1758604552" CREATED="1429082292520" MODIFIED="1429082421902"/>
</node>
<node TEXT="--name-only" STYLE_REF="yellow_node" ID="ID_524628103" CREATED="1429082303391" MODIFIED="1429082412782">
<node TEXT="Show the list of files modified after the commit information" STYLE_REF="pre_last_node" ID="ID_473560788" CREATED="1429082309895" MODIFIED="1429082421902"/>
</node>
<node TEXT="--name-status" STYLE_REF="yellow_node" ID="ID_1015584467" CREATED="1429082319168" MODIFIED="1429082412782">
<node TEXT="Show the list of files affected with added/modified/deleted information as well" STYLE_REF="pre_last_node" ID="ID_1326018175" CREATED="1429082330847" MODIFIED="1429082421902"/>
</node>
<node TEXT="--abbrev-commit" STYLE_REF="yellow_node" ID="ID_477747155" CREATED="1429082343543" MODIFIED="1429082412782">
<node TEXT="Show only the first few characters of the SHA-1 checksum instead of all 40" STYLE_REF="pre_last_node" ID="ID_1721116327" CREATED="1429082352775" MODIFIED="1429082421902"/>
</node>
<node TEXT="--relative-date" STYLE_REF="yellow_node" ID="ID_16098828" CREATED="1429082366503" MODIFIED="1429082412781">
<node TEXT="Display the date in a relative format (for example,&#xa;&#x201c;2 weeks ago&#x201d;) instead of using the full date format" STYLE_REF="pre_last_node" ID="ID_1092816656" CREATED="1429082378911" MODIFIED="1429082421901"/>
</node>
<node TEXT="--oneline" STYLE_REF="yellow_node" ID="ID_1552819952" CREATED="1429082396015" MODIFIED="1429082412781">
<node TEXT="Prints each commit on a single line" STYLE_REF="pre_last_node" ID="ID_199457691" CREATED="1429081606455" MODIFIED="1429081828213"/>
<node TEXT="$ git log --oneline" STYLE_REF="example_node" ID="ID_108244577" CREATED="1429081544879" MODIFIED="1429081824061"><richcontent TYPE="DETAILS" HIDDEN="true">

<html>
  <head>
    
  </head>
  <body>
    <p>
      ca3601a MenuPanelShareOptions almost done
    </p>
    <p>
      0c3d210 Merge branch 'master' of https://github.com/salbeik/21_AuthorTool
    </p>
    <p>
      f377bfc modifications for pageThumbRenderer and image and video elements
    </p>
    <p>
      0befb6c Merge branch 'master' of https://github.com/salbeik/21_AuthorTool
    </p>
    <p>
      b1f036a Multiselection share done
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="--pretty" STYLE_REF="yellow_node" ID="ID_749364673" CREATED="1429081767975" MODIFIED="1429082412781">
<node TEXT="Show commits in an alternate format" STYLE_REF="pre_last_node" ID="ID_1188498582" CREATED="1429082139727" MODIFIED="1429082421900"/>
<node TEXT="git log --pretty=short" STYLE_REF="example_node" ID="ID_1035110400" CREATED="1429081617135" MODIFIED="1429081824061"/>
<node TEXT="git log --pretty=full" STYLE_REF="example_node" ID="ID_128284376" CREATED="1429081617135" MODIFIED="1429081824061"/>
<node TEXT="git log --pretty=fuller" STYLE_REF="example_node" ID="ID_286231484" CREATED="1429081617135" MODIFIED="1429081824062"/>
<node TEXT="$ git log --pretty=format:&quot;%h - %an, %ar : %s&quot;" STYLE_REF="example_node" ID="ID_1049186735" CREATED="1429081811840" MODIFIED="1429081995659"><richcontent TYPE="DETAILS" HIDDEN="true">

<html>
  <head>
    
  </head>
  <body>
    <p>
      ca82a6d - Scott Chacon, 11 months ago : changed the version number
    </p>
    <p>
      085bb3b - Scott Chacon, 11 months ago : removed unnecessary test code
    </p>
    <p>
      a11bef0 - Scott Chacon, 11 months ago : first commit
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Specifies your own log output format" STYLE_REF="pre_last_node" ID="ID_1659910330" CREATED="1429081851335" MODIFIED="1429081990181"/>
<node TEXT="%H" STYLE_REF="pre_last_node" ID="ID_123763174" CREATED="1429081881770" MODIFIED="1429081990182"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Commit hash
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="%h" STYLE_REF="pre_last_node" ID="ID_625453449" CREATED="1429081881770" MODIFIED="1429081990182"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Abbreviated commit hash
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="%T" STYLE_REF="pre_last_node" ID="ID_161703793" CREATED="1429081881771" MODIFIED="1429081990182"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Tree hash
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="%t" STYLE_REF="pre_last_node" ID="ID_112643609" CREATED="1429081881771" MODIFIED="1429081990182"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Abbreviated tree hash
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="%P" STYLE_REF="pre_last_node" ID="ID_411587065" CREATED="1429081881771" MODIFIED="1429081990183"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Parent hashes
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="%p" STYLE_REF="pre_last_node" ID="ID_76344647" CREATED="1429081881771" MODIFIED="1429081990183"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Abbreviated parent hashes
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="%an" STYLE_REF="pre_last_node" ID="ID_1134136559" CREATED="1429081881772" MODIFIED="1429081990184"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Author name
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="%ae" STYLE_REF="pre_last_node" ID="ID_1223472682" CREATED="1429081881772" MODIFIED="1429081995659"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Author e-mail
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="%ad" STYLE_REF="pre_last_node" ID="ID_1920220933" CREATED="1429081881772" MODIFIED="1429081990184"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Author date (format respects the --date= option)
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="%ar" STYLE_REF="pre_last_node" ID="ID_1996328959" CREATED="1429081881773" MODIFIED="1429081990184"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Author date, relative
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="%cn" STYLE_REF="pre_last_node" ID="ID_1527554095" CREATED="1429081881773" MODIFIED="1429081990185"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Committer name
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="%ce" STYLE_REF="pre_last_node" ID="ID_1886568876" CREATED="1429081881773" MODIFIED="1429081990185"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Committer email
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="%cd" STYLE_REF="pre_last_node" ID="ID_1303198061" CREATED="1429081881773" MODIFIED="1429081990185"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Committer date
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="%cr" STYLE_REF="pre_last_node" ID="ID_1482915542" CREATED="1429081881774" MODIFIED="1429081990186"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Committer date, relative
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="%s" STYLE_REF="pre_last_node" ID="ID_699145458" CREATED="1429081881774" MODIFIED="1429081990181"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Subject
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="--graph" STYLE_REF="yellow_node" ID="ID_1534683351" CREATED="1429082079863" MODIFIED="1429082412780">
<node TEXT="Display an ASCII graph of the branch and merge history beside the log output" STYLE_REF="pre_last_node" ID="ID_317086404" CREATED="1429082086599" MODIFIED="1429082430628"/>
<node TEXT="$ git log --graph" STYLE_REF="example_node" ID="ID_645192308" CREATED="1429082149799" MODIFIED="1429082434228"/>
</node>
</node>
<node TEXT="Limiting Log Output according to Date/Time" STYLE_REF="gray_node" ID="ID_340710650" CREATED="1429082693911" MODIFIED="1429087112588">
<node TEXT="$ git log --after=&quot;2014-04-29 00:00:00&quot; --before=&quot;2014-04-29 23:59:59&quot; \&#xa;  --pretty=fuller" STYLE_REF="example_node" ID="ID_361773401" CREATED="1429082697159" MODIFIED="1429083839062">
<node TEXT="Shows commits in this range of time" STYLE_REF="pre_last_node" ID="ID_142442283" CREATED="1429082975536" MODIFIED="1429083844021"/>
</node>
<node TEXT="$ git log --pretty=&quot;%h - %s&quot; --author=gitster \&#xa;       --after=&quot;2008-10-01T00:00:00-0400&quot;         \&#xa;      --before=&quot;2008-10-31T23:59:59-0400&quot; --no-merges -- t/" STYLE_REF="example_node" ID="ID_1802790069" CREATED="1429083144216" MODIFIED="1429083839060">
<node TEXT="If you want to see which commits modifying test files in the Git source code history were committed by Junio Hamano with CommitDate being in the month of October 2008 (relative to the timezone of New York) and were not merges" STYLE_REF="pre_last_node" ID="ID_1798980471" CREATED="1429083169263" MODIFIED="1429083844020"/>
</node>
</node>
<node TEXT="Using a GUI to Visualize History" STYLE_REF="gray_node" ID="ID_355955457" CREATED="1429082587679" MODIFIED="1429087112589">
<node TEXT="$ gitk" STYLE_REF="example_node" ID="ID_1880301292" CREATED="1429082591095" MODIFIED="1429082601924"/>
</node>
</node>
<node TEXT="Undoing Things" STYLE_REF="orange_node" ID="ID_674230878" CREATED="1429084759079" MODIFIED="1429087083683">
<node TEXT="Changing Your Last Commit" STYLE_REF="gray_node" ID="ID_143469892" CREATED="1429084782255" MODIFIED="1429087123722">
<node TEXT="$ git commit --amend" STYLE_REF="example_node" ID="ID_433975986" CREATED="1429085255607" MODIFIED="1429086095645">
<node TEXT="The second commit replaces the results of the first" STYLE_REF="pre_last_node" ID="ID_1366403775" CREATED="1429085862687" MODIFIED="1429086100163"/>
<node TEXT="$ git commit -m &apos;initial commit&apos;&#xa;$ git add forgotten_file&#xa;$ git commit --amend" STYLE_REF="example_node" ID="ID_1309593398" CREATED="1429085877951" MODIFIED="1429087123722"/>
</node>
<node TEXT="$ git revert HEAD" STYLE_REF="example_node" ID="ID_36433959" CREATED="1429936818818" MODIFIED="1429936971836">
<node TEXT="To create second commit that undoing previous commit" STYLE_REF="pre_last_node" ID="ID_752270288" CREATED="1429936834145" MODIFIED="1429936975430"/>
<node TEXT="$ git commit -m &quot;Oops, we didn&apos;t want this commit&quot;&#xa;$ git revert HEAD&#xa;$ git log" STYLE_REF="example_node" ID="ID_126998589" CREATED="1429936884059" MODIFIED="1429937243314"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      [master 45fa96b] Revert &quot;Oops, we didn't want this commit&quot;
    </p>
    <p>
      &#160;1 files changed, 1 insertions(+), 1 deletions(-)
    </p>
    <p>
      
    </p>
    <p>
      * 45fa96b 2011-03-09 | Revert &quot;Oops, we didn't want this commit&quot; (HEAD, master) [Marina Pushkova]
    </p>
    <p>
      * 846b90c 2011-03-09 | Oops, we didn't want this commit [Marina Pushkova]
    </p>
    <p>
      * fa3c141 2011-03-09 | Added HTML header (HEAD, v1, master) [Marina Pushkova]
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="$ git reset --hard v1" STYLE_REF="example_node" ID="ID_319566547" CREATED="1429937218035" MODIFIED="1429937398226">
<node TEXT="To delete all commits after v1 tag. Such commits won&apos;t be visible in history.&#xa;Don&apos;t use when branch is shared in remotes" STYLE_REF="pre_last_node" ID="ID_1121670766" CREATED="1429937273978" MODIFIED="1429937456184"/>
<node TEXT="$ git log" STYLE_REF="example_node" ID="ID_572865521" CREATED="1429937377218" MODIFIED="1429937398226"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      * fa3c141 2011-03-09 | Added HTML header (HEAD, v1, master) [Marina Pushkova]
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="git reset --hard &lt;hash&gt;" STYLE_REF="example_node" ID="ID_373310115" CREATED="1430104803378" MODIFIED="1430104815886">
<node TEXT="To revert all changes and commits, and return to certain commit in history" STYLE_REF="pre_last_node" ID="ID_1803373959" CREATED="1430104804905" MODIFIED="1430104878855"/>
</node>
</node>
<node TEXT="Unstaging a Staged File" STYLE_REF="gray_node" ID="ID_350918060" CREATED="1429086278326" MODIFIED="1429087117692">
<node TEXT="$ git reset HEAD benchmarks.rb" STYLE_REF="example_node" ID="ID_1638041427" CREATED="1429086296126" MODIFIED="1429936401866">
<node TEXT="To unstage the benchmarks.rb file" STYLE_REF="pre_last_node" ID="ID_1016921751" CREATED="1429086304470" MODIFIED="1429936401866"/>
</node>
</node>
<node TEXT="Unmodifying a Modified File" STYLE_REF="gray_node" ID="ID_261020740" CREATED="1429086541030" MODIFIED="1429087117692">
<node TEXT="$ git checkout -- benchmarks.rb" STYLE_REF="example_node" ID="ID_1800542055" CREATED="1429086553494" MODIFIED="1429087121354">
<node TEXT="To revert the changes in this file" STYLE_REF="pre_last_node" ID="ID_999999172" CREATED="1429086580750" MODIFIED="1429087121354"/>
<node TEXT="git checkout benchmarks.rb" STYLE_REF="example_node" ID="ID_1370447221" CREATED="1429936294899" MODIFIED="1429936311324"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      equivalent
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="$ git checkout 911e8c9" STYLE_REF="example_node" ID="ID_550363737" CREATED="1429935387051" MODIFIED="1429935528751">
<node TEXT="To revert all changes that were made after commit with hash=911e8c9" STYLE_REF="pre_last_node" ID="ID_1286240135" CREATED="1429935430499" MODIFIED="1429935524391"/>
<node TEXT="$ git checkout &lt;hash&gt;" STYLE_REF="pre_last_node" ID="ID_1820783443" CREATED="1429935349819" MODIFIED="1429935516189"/>
</node>
<node TEXT="$ git checkout v1" STYLE_REF="example_node" ID="ID_291695098" CREATED="1429935837195" MODIFIED="1429935858923">
<node TEXT="To revert all changes that were made after v1 tag" STYLE_REF="pre_last_node" ID="ID_1492350583" CREATED="1429935646027" MODIFIED="1429935865798"/>
</node>
<node TEXT="git checkout v1^" STYLE_REF="example_node" ID="ID_865572157" CREATED="1429935632355" MODIFIED="1429935858923">
<node TEXT="git checkout v1~1" STYLE_REF="example_node" ID="ID_921399829" CREATED="1429935640827" MODIFIED="1429935888423"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      equivalent
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="To revert all changes that were made after parent commit of v1 tag" STYLE_REF="pre_last_node" ID="ID_535913623" CREATED="1429935646027" MODIFIED="1429935865798"/>
</node>
</node>
</node>
<node TEXT="Working with Remotes" STYLE_REF="orange_node" ID="ID_1013769426" CREATED="1428567293973" MODIFIED="1429087083683">
<node TEXT="Showing Your Remotes" STYLE_REF="gray_node" ID="ID_1855099976" CREATED="1428567571503" MODIFIED="1429087112589">
<node TEXT="$ git remote" STYLE_REF="example_node" ID="ID_1617167320" CREATED="1428567586303" MODIFIED="1428568645674"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      origin
    </p>
  </body>
</html>
</richcontent>
<node TEXT="origin &#x2014; that is the default name Git&#xa;gives to the server you cloned from" STYLE_REF="pre_last_node" ID="ID_32245443" CREATED="1428567599935" MODIFIED="1428568665001"/>
</node>
<node TEXT="$ git remote -v" STYLE_REF="example_node" ID="ID_1240122566" CREATED="1428567640503" MODIFIED="1428568645674"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      origin&#160;&#160;git://github.com/schacon/ticgit.git (fetch)
    </p>
    <p>
      origin&#160;&#160;git://github.com/schacon/ticgit.git (push)
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Shows the URL that Git has stored" STYLE_REF="pre_last_node" ID="ID_1663111327" CREATED="1428567680494" MODIFIED="1428568665003"/>
</node>
<node TEXT="You can have more than one remote" STYLE_REF="yellow_node" ID="ID_919856670" CREATED="1428567745814" MODIFIED="1428568706683">
<node TEXT="git remote -v" STYLE_REF="example_node" ID="ID_858898852" CREATED="1428568564134" MODIFIED="1428568709953"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      bakkdoor&#160;&#160;git://github.com/bakkdoor/grit.git
    </p>
    <p>
      cho45&#160;&#160;&#160;&#160;&#160;git://github.com/cho45/grit.git
    </p>
    <p>
      defunkt&#160;&#160;&#160;git://github.com/defunkt/grit.git
    </p>
    <p>
      koke&#160;&#160;&#160;&#160;&#160;&#160;git://github.com/koke/grit.git
    </p>
    <p>
      origin&#160;&#160;&#160;&#160;git@github.com:mojombo/grit.git
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="You can pull contributions from any of these" STYLE_REF="pre_last_node" ID="ID_12564775" CREATED="1428568594662" MODIFIED="1428568718032"/>
<node TEXT="But, you can push only to the origin remote" STYLE_REF="pre_last_node" ID="ID_917478224" CREATED="1428568612078" MODIFIED="1428568665003"/>
</node>
</node>
<node TEXT="Adding Remote Repositories" STYLE_REF="gray_node" ID="ID_82657135" CREATED="1428568728103" MODIFIED="1429087112589">
<node TEXT="$ git remote add [shortname] [url]" STYLE_REF="pre_last_node" ID="ID_735286552" CREATED="1428568788918" MODIFIED="1428569088808"/>
<node TEXT="$ git remote" STYLE_REF="example_node" ID="ID_1473429196" CREATED="1428569055822" MODIFIED="1428569085306"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      origin
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="$ git remote add pb git://github.com/paulboone/ticgit.git&#xa;$ git remote -v" STYLE_REF="example_node" ID="ID_410385279" CREATED="1428569070268" MODIFIED="1428569085307"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      origin&#160;&#160;git://github.com/schacon/ticgit.git
    </p>
    <p>
      pb&#160;&#160;git://github.com/paulboone/ticgit.git
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="$ git remote set-url origin git://new.url.here" STYLE_REF="example_node" ID="ID_312870795" CREATED="1425647770030" MODIFIED="1429087260034">
<node TEXT="Change the URL for a remote Git repository" STYLE_REF="pre_last_node" ID="ID_1328139182" CREATED="1425647754414" MODIFIED="1429087264306"/>
</node>
</node>
<node TEXT="Fetching and Pulling from Your Remotes" STYLE_REF="gray_node" ID="ID_562542479" CREATED="1428569112885" MODIFIED="1429087112589">
<node TEXT="git fetch [remote-name]" STYLE_REF="pre_last_node" ID="ID_1746432176" CREATED="1428569258339" MODIFIED="1428907956500"/>
<node TEXT="$ git fetch pb" STYLE_REF="example_node" ID="ID_1663991078" CREATED="1428569048141" MODIFIED="1428569085308"/>
<node TEXT="$ git pull" STYLE_REF="example_node" ID="ID_23864217" CREATED="1428906647570" MODIFIED="1428906780350"/>
<node TEXT="Difference between fetch and pull" STYLE_REF="yellow_node" ID="ID_1409344056" CREATED="1428906708963" MODIFIED="1428906771351">
<hook URI="./fetch-pull.png" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Fetch command pulls the data to your local repository - it doesn&#x2019;t automatically merge it with any of your work or modify what you&#x2019;re currently working on" STYLE_REF="pre_last_node" ID="ID_740551611" CREATED="1428569356078" MODIFIED="1428906775029"/>
<node TEXT="Running git pull generally fetches data from the server you originally cloned from and automatically tries to merge it into the code you&#x2019;re currently working on." STYLE_REF="pre_last_node" ID="ID_19128614" CREATED="1428569404355" MODIFIED="1428906775031"/>
</node>
</node>
<node TEXT="Pushing to Your Remotes" STYLE_REF="gray_node" ID="ID_997319401" CREATED="1428906816704" MODIFIED="1430106012407">
<node TEXT="git push [remote-name] [branch-name]" STYLE_REF="pre_last_node" ID="ID_910671335" CREATED="1428906854059" MODIFIED="1428907963325"/>
<node TEXT="$ git push origin master" STYLE_REF="example_node" ID="ID_1028864790" CREATED="1428906861354" MODIFIED="1428907743877"/>
<node TEXT="If you and someone else clone at the same time and they push upstream and then you push upstream, your push will rightly be rejected. You&#x2019;ll have to pull down their work first and incorporate it into yours before you&#x2019;ll be allowed to push" STYLE_REF="pre_last_node" ID="ID_842900281" CREATED="1428906931610" MODIFIED="1430106012406"/>
</node>
<node TEXT="Inspecting a Remote" STYLE_REF="gray_node" ID="ID_619418866" CREATED="1428906940267" MODIFIED="1429087112590">
<node TEXT="git remote show [remote-name]" STYLE_REF="pre_last_node" ID="ID_678948216" CREATED="1428906963874" MODIFIED="1428907970453"/>
<node TEXT="$ git remote show origin" STYLE_REF="example_node" ID="ID_828744668" CREATED="1428906973043" MODIFIED="1430106022733"><richcontent TYPE="DETAILS" HIDDEN="true">

<html>
  <head>
    
  </head>
  <body>
    <p>
      * remote origin
    </p>
    <p>
      &#160;&#160;Fetch URL: https://github.com/salbeik/21_AuthorTool.git
    </p>
    <p>
      &#160;&#160;Push&#160;&#160;URL: https://github.com/salbeik/21_AuthorTool.git
    </p>
    <p>
      &#160;&#160;HEAD branch: master
    </p>
    <p>
      &#160;&#160;Remote branches:
    </p>
    <p>
      &#160;&#160;&#160;&#160;connectingPresentationView&#160;&#160;&#160;tracked
    </p>
    <p>
      &#160;&#160;&#160;&#160;create_youtube_wall&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;tracked
    </p>
    <p>
      &#160;&#160;&#160;&#160;groups_bugs&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;tracked
    </p>
    <p>
      &#160;&#160;&#160;&#160;image_wall_new&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;tracked
    </p>
    <p>
      &#160;&#160;&#160;&#160;implementingPageThumbWorkers tracked
    </p>
    <p>
      &#160;&#160;&#160;&#160;interactive_tools&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;tracked
    </p>
    <p>
      &#160;&#160;&#160;&#160;local_images_panel&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;tracked
    </p>
    <p>
      &#160;&#160;&#160;&#160;login_view_redesign&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;tracked
    </p>
    <p>
      &#160;&#160;&#160;&#160;master&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;tracked
    </p>
    <p>
      &#160;&#160;&#160;&#160;new_drag_image_logic&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;tracked
    </p>
    <p>
      &#160;&#160;&#160;&#160;share&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;tracked
    </p>
    <p>
      &#160;&#160;&#160;&#160;video_editor&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;tracked
    </p>
    <p>
      &#160;&#160;&#160;&#160;video_element_refactoring&#160;&#160;&#160;&#160;tracked
    </p>
    <p>
      &#160;&#160;Local branches configured for 'git pull':
    </p>
    <p>
      &#160;&#160;&#160;&#160;groups_bugs&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;merges with remote groups_bugs
    </p>
    <p>
      &#160;&#160;&#160;&#160;login_view_redesign merges with remote login_view_redesign
    </p>
    <p>
      &#160;&#160;&#160;&#160;master&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;merges with remote master
    </p>
    <p>
      &#160;&#160;&#160;&#160;master_new&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;merges with remote master
    </p>
    <p>
      &#160;&#160;Local refs configured for 'git push':
    </p>
    <p>
      &#160;&#160;&#160;&#160;groups_bugs&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;pushes to groups_bugs&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;(up to date)
    </p>
    <p>
      &#160;&#160;&#160;&#160;login_view_redesign pushes to login_view_redesign (local out of date)
    </p>
    <p>
      &#160;&#160;&#160;&#160;master&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;pushes to master&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;(local out of date)
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Removing and Renaming Remotes" STYLE_REF="gray_node" ID="ID_1760506783" CREATED="1428907352026" MODIFIED="1429087112590">
<node TEXT="$ git remote rename pb paul" STYLE_REF="example_node" ID="ID_1187603169" CREATED="1428907694127" MODIFIED="1428907743880"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      $ git remote
    </p>
    <p>
      origin
    </p>
    <p>
      paul
    </p>
  </body>
</html>
</richcontent>
<node TEXT="To rename a reference" STYLE_REF="pre_last_node" ID="ID_508618032" CREATED="1428907801333" MODIFIED="1428907809038"/>
</node>
<node TEXT="$ git remote rm paul" STYLE_REF="example_node" ID="ID_1966197573" CREATED="1428907659128" MODIFIED="1428907743880"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      $ git remote
    </p>
    <p>
      origin
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="Tagging" STYLE_REF="orange_node" ID="ID_762918364" CREATED="1429088094485" MODIFIED="1429088102330">
<node TEXT="Listing Tags" STYLE_REF="gray_node" ID="ID_596135945" CREATED="1429088163693" MODIFIED="1429088247514">
<node TEXT="$ git tag" STYLE_REF="example_node" ID="ID_532112763" CREATED="1429088171277" MODIFIED="1433259193480"><richcontent TYPE="DETAILS" HIDDEN="true">

<html>
  <head>
    
  </head>
  <body>
    <p>
      v0.1
    </p>
    <p>
      v1.3
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="$ git tag -l &apos;v1.4.2.*&apos;" STYLE_REF="example_node" ID="ID_723897778" CREATED="1429088218278" MODIFIED="1429088251650"><richcontent TYPE="DETAILS" HIDDEN="true">

<html>
  <head>
    
  </head>
  <body>
    <p>
      v1.4.2.1
    </p>
    <p>
      v1.4.2.2
    </p>
    <p>
      v1.4.2.3
    </p>
    <p>
      v1.4.2.4
    </p>
  </body>
</html>
</richcontent>
<node TEXT="To search for tags with a particular pattern" STYLE_REF="pre_last_node" ID="ID_1873083289" CREATED="1429088239725" MODIFIED="1429088254474"/>
</node>
</node>
<node TEXT="Types of tags" STYLE_REF="gray_node" ID="ID_1936210307" CREATED="1429088324949" MODIFIED="1429088609890">
<node TEXT="Lightweight" STYLE_REF="yellow_node" ID="ID_109056891" CREATED="1429088331709" MODIFIED="1429088614090">
<font BOLD="false"/>
<node TEXT="It&#x2019;s just a pointer to a specific commit" STYLE_REF="pre_last_node" ID="ID_832100730" CREATED="1429088362469" MODIFIED="1429088411284"/>
</node>
<node TEXT="Annotated" STYLE_REF="yellow_node" ID="ID_1077986099" CREATED="1429088336989" MODIFIED="1429088614091">
<font BOLD="false"/>
<node TEXT="Are stored as full objects in the Git database. They&#x2019;re checksummed; contain the tagger name, e-mail, and date; have a tagging message" STYLE_REF="pre_last_node" ID="ID_1172451710" CREATED="1429088386493" MODIFIED="1429088411282"/>
</node>
</node>
<node TEXT="Annotated Tags" STYLE_REF="gray_node" ID="ID_835474681" CREATED="1429088315869" MODIFIED="1429088597161">
<node TEXT="$ git tag -a v1.4 -m &apos;my version 1.4&apos;" STYLE_REF="example_node" ID="ID_1558929479" CREATED="1429088438605" MODIFIED="1429088568866">
<node TEXT="To create new tag use -a" STYLE_REF="pre_last_node" ID="ID_718005683" CREATED="1429088556542" MODIFIED="1429088573122"/>
<node TEXT="-m specifies a tagging message" STYLE_REF="pre_last_node" ID="ID_1915014965" CREATED="1429088450077" MODIFIED="1429088573122"/>
</node>
<node TEXT="$ git show v1.4" STYLE_REF="example_node" ID="ID_1474833312" CREATED="1429088624165" MODIFIED="1429088670746"><richcontent TYPE="DETAILS" HIDDEN="true">

<html>
  <head>
    
  </head>
  <body>
    <p>
      tag v1.4
    </p>
    <p>
      Tagger: Scott Chacon &lt;schacon@gee-mail.com&gt;
    </p>
    <p>
      Date:&#160;&#160;&#160;Mon Feb 9 14:45:11 2009 -0800
    </p>
    <p>
      
    </p>
    <p>
      my version 1.4
    </p>
    <p>
      
    </p>
    <p>
      commit 15027957951b64cf874c3557a0f3547bd83b3ff6
    </p>
    <p>
      Merge: 4a447f7... a6b4c97...
    </p>
    <p>
      Author: Scott Chacon &lt;schacon@gee-mail.com&gt;
    </p>
    <p>
      Date:&#160;&#160;&#160;Sun Feb 8 19:02:46 2009 -0800
    </p>
    <p>
      
    </p>
    <p>
      &#160;&#160;&#160;&#160;Merge branch 'experiment'
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Shows the tagger information" STYLE_REF="pre_last_node" ID="ID_413981981" CREATED="1429088653085" MODIFIED="1429088673874"/>
</node>
</node>
<node TEXT="Signed Tags" STYLE_REF="gray_node" ID="ID_71212248" CREATED="1429089594188" MODIFIED="1429100990149">
<node TEXT="$ git tag -s v1.5 -m &apos;my signed 1.5 tag&apos;" STYLE_REF="example_node" ID="ID_1340552798" CREATED="1429089599549" MODIFIED="1429100990149">
<node TEXT="Sign your tags with GPG, assuming you have a private key" STYLE_REF="pre_last_node" ID="ID_940683525" CREATED="1429089623805" MODIFIED="1429089642953"/>
</node>
</node>
<node TEXT="Lightweight Tags" STYLE_REF="gray_node" ID="ID_1832053175" CREATED="1429090264141" MODIFIED="1429090305482">
<node TEXT="$ git tag v1.4-lw" STYLE_REF="example_node" ID="ID_1463598368" CREATED="1429090270430" MODIFIED="1429090308922"/>
<node TEXT="$ git show v1.4-lw" STYLE_REF="example_node" ID="ID_1559337900" CREATED="1429090291100" MODIFIED="1433259232608"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      commit 15027957951b64cf874c3557a0f3547bd83b3ff6
    </p>
    <p>
      Merge: 4a447f7... a6b4c97...
    </p>
    <p>
      Author: Scott Chacon &lt;schacon@gee-mail.com&gt;
    </p>
    <p>
      Date:&#160;&#160;&#160;Sun Feb 8 19:02:46 2009 -0800
    </p>
    <p>
      
    </p>
    <p>
      &#160;&#160;&#160;&#160;Merge branch 'experiment'
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Verifying Tags" STYLE_REF="gray_node" ID="ID_1284360430" CREATED="1429090680973" MODIFIED="1429937492881">
<node TEXT="$ git tag -v v1.4.2.1" STYLE_REF="example_node" ID="ID_473716889" CREATED="1429090697253" MODIFIED="1433259244613">
<node TEXT="Uses GPG to verify the signature" STYLE_REF="pre_last_node" ID="ID_1262040411" CREATED="1429090722365" MODIFIED="1429091545633"/>
<node TEXT="You need the signer&#x2019;s public key in your keyring for this to work properly" STYLE_REF="pre_last_node" ID="ID_524122818" CREATED="1429090734630" MODIFIED="1433259244611"/>
</node>
</node>
<node TEXT="Tagging Later" STYLE_REF="gray_node" ID="ID_1946322880" CREATED="1429090967676" MODIFIED="1429100994045">
<node TEXT="$ git tag -a v1.2 -m &apos;version 1.2&apos; 9fceb02" STYLE_REF="example_node" ID="ID_853546601" CREATED="1429091070268" MODIFIED="1429100994045">
<node TEXT="Suppose you forgot to tag the project at v1.2,&#xa;which was at the &quot;updated rakefile&quot; commit" STYLE_REF="pre_last_node" ID="ID_1225726061" CREATED="1429091090380" MODIFIED="1429091545634"/>
<node TEXT="To tag that commit, you specify the commit checksum&#xa;(or part of it) at the end of the command" STYLE_REF="pre_last_node" ID="ID_1512585639" CREATED="1429091101484" MODIFIED="1429091545635"/>
</node>
</node>
<node TEXT="Sharing Tags" STYLE_REF="gray_node" ID="ID_746830269" CREATED="1429090976084" MODIFIED="1429090982362">
<node TEXT="By default, the git push command doesn&#x2019;t transfer tags to remote servers" STYLE_REF="pre_last_node" ID="ID_1981827691" CREATED="1429091339125" MODIFIED="1429091545637"/>
<node TEXT="git push origin [tagname]" STYLE_REF="pre_last_node" ID="ID_906167349" CREATED="1429091350517" MODIFIED="1429091545637"/>
<node TEXT="$ git push origin v1.5" STYLE_REF="example_node" ID="ID_979713300" CREATED="1429091360117" MODIFIED="1429091667320"><richcontent TYPE="DETAILS" HIDDEN="true">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Counting objects: 50, done.
    </p>
    <p>
      Compressing objects: 100% (38/38), done.
    </p>
    <p>
      Writing objects: 100% (44/44), 4.56 KiB, done.
    </p>
    <p>
      Total 44 (delta 18), reused 8 (delta 1)
    </p>
    <p>
      To git@github.com:schacon/simplegit.git
    </p>
    <p>
      * [new tag]&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;v1.5 -&gt; v1.5
    </p>
  </body>
</html>
</richcontent>
<node TEXT="To explicitly push tag" STYLE_REF="pre_last_node" ID="ID_477164527" CREATED="1429091380909" MODIFIED="1429091545637"/>
</node>
<node TEXT="$ git push origin --tags" STYLE_REF="example_node" ID="ID_1261311029" CREATED="1429091495709" MODIFIED="1429091647992"><richcontent TYPE="DETAILS" HIDDEN="true">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Counting objects: 50, done.
    </p>
    <p>
      Compressing objects: 100% (38/38), done.
    </p>
    <p>
      Writing objects: 100% (44/44), 4.56 KiB, done.
    </p>
    <p>
      Total 44 (delta 18), reused 8 (delta 1)
    </p>
    <p>
      To git@github.com:schacon/simplegit.git
    </p>
    <p>
      &#160;* [new tag]&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;v0.1 -&gt; v0.1
    </p>
    <p>
      &#160;* [new tag]&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;v1.2 -&gt; v1.2
    </p>
    <p>
      &#160;* [new tag]&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;v1.4 -&gt; v1.4
    </p>
    <p>
      &#160;* [new tag]&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;v1.4-lw -&gt; v1.4-lw
    </p>
    <p>
      &#160;* [new tag]&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;v1.5 -&gt; v1.5
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Will transfer all of your tags to the remote server that are not already there" STYLE_REF="pre_last_node" ID="ID_1322153702" CREATED="1429091514237" MODIFIED="1429091545637"/>
</node>
</node>
<node TEXT="Deleting Tags" STYLE_REF="gray_node" ID="ID_932706902" CREATED="1429937499482" MODIFIED="1429937508925">
<node TEXT="$ git tag -d oops" STYLE_REF="example_node" ID="ID_827255536" CREATED="1429937514954" MODIFIED="1429937553429">
<node TEXT="To delete tag in local branch" STYLE_REF="pre_last_node" ID="ID_619856676" CREATED="1429937523874" MODIFIED="1429937635168"/>
</node>
</node>
</node>
</node>
<node TEXT="Customizing" STYLE_REF="second_node" POSITION="right" ID="ID_177938804" CREATED="1429087000789" MODIFIED="1430110167556" HGAP="70" VSHIFT="-169">
<edge COLOR="#7c0000"/>
<hook URI="../../Images/customizing.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Aliases" STYLE_REF="orange_node" ID="ID_121237184" CREATED="1429087216813" MODIFIED="1429087559883">
<node TEXT="$ git config --global alias.co checkout&#xa;$ git config --global alias.br branch&#xa;$ git config --global alias.ci commit&#xa;$ git config --global alias.st status" STYLE_REF="example_node" ID="ID_1102060803" CREATED="1429087302982" MODIFIED="1429087570708">
<node TEXT="$ git ci" STYLE_REF="example_node" ID="ID_1111560281" CREATED="1429087343702" MODIFIED="1429087570710">
<hook NAME="FirstGroupNode"/>
</node>
<node TEXT="$ git commit" STYLE_REF="example_node" ID="ID_1555065626" CREATED="1429087350405" MODIFIED="1429087570710"/>
<node TEXT="equivalent" STYLE_REF="pre_last_node" ID="ID_1613945312" CREATED="1429087365946" MODIFIED="1429087574900">
<hook NAME="SummaryNode"/>
</node>
</node>
<node TEXT="$ git config --global alias.unstage &apos;reset HEAD --&apos;" STYLE_REF="example_node" ID="ID_1347638581" CREATED="1429087310342" MODIFIED="1429087570707">
<node TEXT="$ git unstage fileA" STYLE_REF="example_node" ID="ID_1164089184" CREATED="1429087318053" MODIFIED="1429087570709">
<hook NAME="FirstGroupNode"/>
</node>
<node TEXT="$ git reset HEAD fileA" STYLE_REF="example_node" ID="ID_1737372497" CREATED="1429087318053" MODIFIED="1429087570709"/>
<node TEXT="equivalent" STYLE_REF="pre_last_node" ID="ID_1924191756" CREATED="1429087321554" MODIFIED="1429087574899">
<hook NAME="SummaryNode"/>
</node>
</node>
<node TEXT="$ git config --global alias.last &apos;log -1 HEAD&apos;" STYLE_REF="example_node" ID="ID_1843422242" CREATED="1429087561495" MODIFIED="1429087570710"/>
<node TEXT="$ git config --global alias.visual &apos;!gitk&apos;" STYLE_REF="example_node" ID="ID_945977063" CREATED="1429087591013" MODIFIED="1429087630347">
<node TEXT="To run an external command, start the command with a ! character" STYLE_REF="pre_last_node" ID="ID_1538029124" CREATED="1429087607526" MODIFIED="1429087633802"/>
</node>
</node>
<node TEXT="Configuration" STYLE_REF="orange_node" ID="ID_1795850675" CREATED="1429601784586" MODIFIED="1429601790360">
<node TEXT="Config files" STYLE_REF="gray_node" ID="ID_601959025" CREATED="1429601959261" MODIFIED="1429601970632">
<node TEXT="/etc/gitconfig" STYLE_REF="yellow_node" ID="ID_544378802" CREATED="1429601858251" MODIFIED="1429602051641">
<node TEXT="Contains values for every user on the system and all of their repositories" STYLE_REF="pre_last_node" ID="ID_1536328292" CREATED="1429601888610" MODIFIED="1429602061722"/>
<node TEXT="If you pass the option --system to git config, it reads and writes from this file specifically" STYLE_REF="pre_last_node" ID="ID_174387139" CREATED="1429601898811" MODIFIED="1429602061721"/>
</node>
<node TEXT="~/.gitconfig" STYLE_REF="yellow_node" ID="ID_220145600" CREATED="1429601913555" MODIFIED="1429602054024">
<node TEXT="Is specific to each user" STYLE_REF="pre_last_node" ID="ID_461887699" CREATED="1429601923739" MODIFIED="1429602061721"/>
<node TEXT="You can make Git read and write to this file by passing the --global option" STYLE_REF="pre_last_node" ID="ID_127866350" CREATED="1429601947340" MODIFIED="1429602061721"/>
</node>
<node TEXT=".git/config" STYLE_REF="yellow_node" ID="ID_1722590211" CREATED="1429601957018" MODIFIED="1429602051640">
<node TEXT="These values are specific to that single repository" STYLE_REF="pre_last_node" ID="ID_1265305252" CREATED="1429601986387" MODIFIED="1429602061720"/>
</node>
<node TEXT="Each level overwrites values in the previous level" STYLE_REF="pre_last_node" ID="ID_1518701440" CREATED="1429602018588" MODIFIED="1429602061723"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Values in .git/config trump those in /etc/gitconfig
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Basic Client Configuration" STYLE_REF="gray_node" ID="ID_1929028703" CREATED="1429602069283" MODIFIED="1429771216976">
<node TEXT="core.editor" STYLE_REF="yellow_node" ID="ID_457975978" CREATED="1429602229947" MODIFIED="1429602289416">
<node TEXT="$ git config --global core.editor emacs" STYLE_REF="example_node" ID="ID_928668310" CREATED="1429602235619" MODIFIED="1429602285088"/>
<node TEXT="By default, Git uses whatever you&#x2019;ve set as your default text editor or else falls back to the Vi editor to create and edit your commit and tag messages" STYLE_REF="pre_last_node" ID="ID_1335246881" CREATED="1429602259419" MODIFIED="1429628697529"/>
</node>
<node TEXT="commit.template" STYLE_REF="yellow_node" ID="ID_895697118" CREATED="1429602269579" MODIFIED="1429602289417">
<node TEXT="If you set this to the path of a file on your system, Git will use that file as the default message when you commit." STYLE_REF="pre_last_node" ID="ID_1472749138" CREATED="1429623397779" MODIFIED="1429628697529"/>
<node TEXT="$ git config --global commit.template $HOME/.gitmessage.txt" STYLE_REF="example_node" ID="ID_897411206" CREATED="1429623434275" MODIFIED="1429628692743"/>
</node>
<node TEXT="core.pager" STYLE_REF="yellow_node" ID="ID_285414510" CREATED="1429623679483" MODIFIED="1429628682456">
<node TEXT="The core.pager setting determines what pager is used when Git pages output such as log and diff. You can set it to more or to your favorite pager (by default, it&#x2019;s less), or you can turn it off by setting it to a blank string" ID="ID_75708632" CREATED="1429624456923" MODIFIED="1429624457303"/>
<node TEXT="$ git config --global core.pager &apos;&apos;" ID="ID_847281107" CREATED="1429624463011" MODIFIED="1429624463399"/>
</node>
<node TEXT="user.signingkey" STYLE_REF="yellow_node" ID="ID_878635676" CREATED="1429624470110" MODIFIED="1429771216975">
<node TEXT="To set your key ID" STYLE_REF="pre_last_node" ID="ID_269734823" CREATED="1429624544867" MODIFIED="1429628697528"/>
<node TEXT="$ git config --global user.signingkey &lt;gpg-key-id&gt;" STYLE_REF="example_node" ID="ID_1087702756" CREATED="1429624535706" MODIFIED="1429628692743"/>
<node TEXT="Now, you can sign tags without having to specify your key every time with the git tag command" STYLE_REF="pre_last_node" ID="ID_1308138279" CREATED="1429624556765" MODIFIED="1429628697527">
<node TEXT="$ git tag -s &lt;tag-name&gt;" STYLE_REF="example_node" ID="ID_1862226755" CREATED="1429624562067" MODIFIED="1429628692744"/>
</node>
</node>
<node TEXT="core.excludesfile" STYLE_REF="yellow_node" ID="ID_492506141" CREATED="1429624571006" MODIFIED="1429628682455"/>
<node TEXT="help.autocorrect" STYLE_REF="yellow_node" ID="ID_1388786898" CREATED="1429770049075" MODIFIED="1429770101776">
<node TEXT="$ git com&#xa;git: &apos;com&apos; is not a git-command. See &apos;git --help&apos;.&#xa;&#xa;Did you mean this?&#xa;     commit" STYLE_REF="example_node" ID="ID_1988607463" CREATED="1429770095772" MODIFIED="1429770115270"/>
<node TEXT="If you set help.autocorrect to 1, Git will automatically run the&#xa;command if it has only one match under this scenario" STYLE_REF="pre_last_node" ID="ID_239648520" CREATED="1429770065283" MODIFIED="1429770124658"/>
</node>
</node>
<node TEXT="Colors in Git" STYLE_REF="gray_node" ID="ID_607066050" CREATED="1429602114931" MODIFIED="1429602150209">
<node TEXT="color.ui" STYLE_REF="yellow_node" ID="ID_1908952337" CREATED="1429770178011" MODIFIED="1429770559220">
<node TEXT="To turn on all the default terminal coloring, set color.ui to true" STYLE_REF="pre_last_node" ID="ID_460130667" CREATED="1429770200004" MODIFIED="1429770574168"/>
<node TEXT="$ git config --global color.ui true" STYLE_REF="example_node" ID="ID_874021488" CREATED="1429770184162" MODIFIED="1429770579214"/>
</node>
<node TEXT="color.*" STYLE_REF="yellow_node" ID="ID_1347574157" CREATED="1429770348387" MODIFIED="1429770559220">
<node TEXT="If you want to be more specific about which commands are colored and how, Git provides verb-specific coloring settings. Each of these can be set to true, false, or always:" STYLE_REF="pre_last_node" ID="ID_710846021" CREATED="1429770354340" MODIFIED="1429770574168"/>
<node TEXT="color.branch" STYLE_REF="pre_last_node" ID="ID_1464829518" CREATED="1429770362539" MODIFIED="1429770574168"/>
<node TEXT="color.diff" STYLE_REF="pre_last_node" ID="ID_1964208114" CREATED="1429770362539" MODIFIED="1429770574168"/>
<node TEXT="color.interactive" STYLE_REF="pre_last_node" ID="ID_764932631" CREATED="1429770362539" MODIFIED="1429770574168"/>
<node TEXT="color.status" STYLE_REF="pre_last_node" ID="ID_1381505685" CREATED="1429770362539" MODIFIED="1429770574168"/>
</node>
<node TEXT="To override each color" STYLE_REF="yellow_node" ID="ID_116119424" CREATED="1429770446052" MODIFIED="1429770559220">
<node TEXT="$ git config --global color.diff.meta &quot;blue black bold&quot;" STYLE_REF="example_node" ID="ID_575667361" CREATED="1429770453268" MODIFIED="1429770579214"/>
<node TEXT="Colors" STYLE_REF="pre_last_node" ID="ID_1532124700" CREATED="1429770461899" MODIFIED="1429770574168">
<node TEXT="normal" ID="ID_1791865507" CREATED="1429770473468" MODIFIED="1429770514488"/>
<node ID="ID_705397669" CREATED="1429770478709" MODIFIED="1429770478709"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      black
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_1849926640" CREATED="1429770483014" MODIFIED="1429770483014"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      red
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_1148292351" CREATED="1429770487735" MODIFIED="1429770487735"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      white
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_318805412" CREATED="1429770493039" MODIFIED="1429770493039"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      cyan
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_1472942134" CREATED="1429770497643" MODIFIED="1429770497643"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      magenta
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_513568791" CREATED="1429770501309" MODIFIED="1429770501309"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      blue
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_713404582" CREATED="1429770507267" MODIFIED="1429770507267"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      yellow
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_225520518" CREATED="1429770511583" MODIFIED="1429770511583"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      green
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Styles" STYLE_REF="pre_last_node" ID="ID_505145411" CREATED="1429770525978" MODIFIED="1429770574168">
<node TEXT="bold" ID="ID_1682291441" CREATED="1429770528363" MODIFIED="1429770533669"/>
<node TEXT="dim" ID="ID_382322767" CREATED="1429770534307" MODIFIED="1429770534307"/>
<node TEXT="ul" ID="ID_253219489" CREATED="1429770539531" MODIFIED="1429770539531"/>
<node TEXT="blink" ID="ID_1603972554" CREATED="1429770546925" MODIFIED="1429770546925"/>
<node TEXT="reverse" ID="ID_825800207" CREATED="1429770552307" MODIFIED="1429770552307"/>
</node>
</node>
</node>
<node TEXT="External Merge and Diff Tools" STYLE_REF="gray_node" ID="ID_968916056" CREATED="1429602125395" MODIFIED="1429602150209">
<node TEXT="P4Merge" STYLE_REF="yellow_node" ID="ID_1930368458" CREATED="1429771232514" MODIFIED="1429771251152"/>
</node>
<node TEXT="Formatting and Whitespace" STYLE_REF="gray_node" ID="ID_1091432151" CREATED="1429602134988" MODIFIED="1429602150209">
<node TEXT="Windows uses both a carriage-return character and a linefeed character for newlines in its files, whereas Mac and Linux systems use only the linefeed character" STYLE_REF="pre_last_node" ID="ID_684788715" CREATED="1429771897131" MODIFIED="1429772340555"/>
<node TEXT="core.autocrlf" STYLE_REF="yellow_node" ID="ID_1938113997" CREATED="1429771793979" MODIFIED="1429772302975">
<node TEXT="Git can handle this by auto-converting CRLF line endings into LF when you commit, and vice versa when it checks out code onto your filesystem. If you&#x2019;re on a Windows machine, set it to true &#x2014; this converts LF endings into CRLF when you check out code" STYLE_REF="pre_last_node" ID="ID_1754998312" CREATED="1429771794451" MODIFIED="1429772340555">
<node TEXT="$ git config --global core.autocrlf true" STYLE_REF="example_node" ID="ID_1991280024" CREATED="1429771959515" MODIFIED="1429772618071"/>
</node>
<node TEXT="If you&#x2019;re on a Linux or Mac system that uses LF line endings, then you don&#x2019;t want Git to automatically convert them when you check out files; however, if a file with CRLF endings accidentally gets introduced, then you may want Git to fix it. You can tell Git to convert CRLF to LF on commit but not the other way around by setting core.autocrlf to input" STYLE_REF="pre_last_node" ID="ID_1614327367" CREATED="1429772011474" MODIFIED="1429772340555">
<node TEXT="$ git config --global core.autocrlf input" STYLE_REF="example_node" ID="ID_1958753964" CREATED="1429771998554" MODIFIED="1429772618071"/>
</node>
<node TEXT="Turn off LF to CRLF replacement" STYLE_REF="pre_last_node" ID="ID_849425310" CREATED="1427445780465" MODIFIED="1429772642519">
<node TEXT="git config --global core.autocrlf false" STYLE_REF="example_node" ID="ID_916477140" CREATED="1427445788361" MODIFIED="1429772618070"/>
</node>
</node>
<node TEXT="core.whitespace" STYLE_REF="yellow_node" ID="ID_799298001" CREATED="1429771999003" MODIFIED="1429772302975">
<node TEXT="enabled by default" STYLE_REF="third_node" ID="ID_1822344417" CREATED="1429772134451" MODIFIED="1429772315830">
<font SIZE="10" BOLD="false"/>
<node TEXT="trailing-space" STYLE_REF="gray_node" ID="ID_797361296" CREATED="1429772141299" MODIFIED="1429772330878">
<font BOLD="false"/>
<node TEXT="Looks for spaces at the end of a line" STYLE_REF="pre_last_node" ID="ID_58217534" CREATED="1429772149211" MODIFIED="1429772340555"/>
</node>
<node TEXT="space-before-tab" STYLE_REF="gray_node" ID="ID_1078270777" CREATED="1429772155990" MODIFIED="1429772330878">
<font BOLD="false"/>
<node TEXT="Looks for spaces before tabs at the beginning of a line" STYLE_REF="pre_last_node" ID="ID_1186561619" CREATED="1429772162650" MODIFIED="1429772340555"/>
</node>
</node>
<node TEXT="disabled by default" STYLE_REF="third_node" ID="ID_1562610283" CREATED="1429772189250" MODIFIED="1429772315830">
<font SIZE="10" BOLD="false"/>
<node TEXT="indent-with-non-tab" STYLE_REF="gray_node" ID="ID_834233733" CREATED="1429772196250" MODIFIED="1429772330878">
<font BOLD="false"/>
<node TEXT="Looks for lines that begin with eight or more spaces instead of tabs" STYLE_REF="pre_last_node" ID="ID_845558013" CREATED="1429772244522" MODIFIED="1429772358179"/>
</node>
<node TEXT="cr-at-eol" STYLE_REF="gray_node" ID="ID_319059128" CREATED="1429772255347" MODIFIED="1429772360093">
<font BOLD="false"/>
<node TEXT="Tells Git that carriage returns at the end of lines are OK" STYLE_REF="pre_last_node" ID="ID_756193794" CREATED="1429772295923" MODIFIED="1429772365390"/>
</node>
</node>
<node TEXT="Git try to automatically fix the issue before applying the patch" STYLE_REF="pre_last_node" ID="ID_1220703354" CREATED="1429772473443" MODIFIED="1429772642519">
<node TEXT="$ git apply --whitespace=fix &lt;patch&gt;" STYLE_REF="example_node" ID="ID_141627680" CREATED="1429772491724" MODIFIED="1429773070247"/>
</node>
<node TEXT="If you want all but cr-at-eol to be set" STYLE_REF="pre_last_node" ID="ID_1872670873" CREATED="1429772513707" MODIFIED="1429772642520">
<node TEXT="$ git config --global core.whitespace \&#xa;trailing-space,space-before-tab,indent-with-non-tab" STYLE_REF="example_node" ID="ID_1085217887" CREATED="1429772513707" MODIFIED="1429773070247"/>
</node>
</node>
</node>
<node TEXT="Server Configuration" STYLE_REF="gray_node" ID="ID_22762006" CREATED="1429602141331" MODIFIED="1429602150208">
<node TEXT="receive.fsckObjects" STYLE_REF="yellow_node" ID="ID_495329264" CREATED="1429772600156" MODIFIED="1429773051780">
<node TEXT="If you want Git to check object consistency on every push" STYLE_REF="pre_last_node" ID="ID_1926982344" CREATED="1429772767058" MODIFIED="1429773061350"/>
<node TEXT="$ git config --system receive.fsckObjects true" STYLE_REF="example_node" ID="ID_1388366439" CREATED="1429772782513" MODIFIED="1429773070247"/>
</node>
<node TEXT="receive.denyNonFastForwards" STYLE_REF="yellow_node" ID="ID_841062874" CREATED="1429772869235" MODIFIED="1429773051780">
<node TEXT="To disable the ability to force-update remote&#xa;branches to non-fast-forward references" STYLE_REF="pre_last_node" ID="ID_680924187" CREATED="1429772930819" MODIFIED="1429773061350"/>
<node TEXT="$ git config --system receive.denyNonFastForwards true" STYLE_REF="example_node" ID="ID_1057181023" CREATED="1429772879938" MODIFIED="1429773070247"/>
</node>
<node TEXT="receive.denyDeletes" STYLE_REF="yellow_node" ID="ID_696159242" CREATED="1429772874066" MODIFIED="1429773051780">
<node TEXT="This denies branch and tag deletion over a push across the board &#x2014; no user can do it. To remove remote branches, you must remove the ref files from the server manually" STYLE_REF="pre_last_node" ID="ID_1691518708" CREATED="1429773043876" MODIFIED="1429773061350"/>
<node TEXT="$ git config --system receive.denyDeletes true" STYLE_REF="example_node" ID="ID_1140116148" CREATED="1429773024914" MODIFIED="1429773070247"/>
</node>
</node>
</node>
</node>
<node TEXT="Branching" STYLE_REF="second_node" POSITION="left" ID="ID_109921986" CREATED="1429100968569" MODIFIED="1430110023469">
<edge COLOR="#007c00"/>
<hook URI="../../Images/branch.png" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="What a Branch Is" STYLE_REF="orange_node" ID="ID_637948168" CREATED="1429100982584" MODIFIED="1431335336156">
<node TEXT="Git doesn&#x2019;t store data as a series of changesets or deltas, but instead as a series of snapshots. When you commit in Git, Git stores a commit object that contains a pointer to the snapshot of the content you staged, the author and message metadata, and zero or more pointers to the commit or commits that were the direct parents of this commit: zero parents for the first commit, one parent for a normal commit, and multiple parents for a commit that results from a merge of two or more branches" STYLE_REF="pre_last_node" ID="ID_664323535" CREATED="1429101066753" MODIFIED="1429101529616"/>
<node TEXT="To visualize this, let&#x2019;s assume that you have a directory&#xa;containing three files, and you stage them all and commit" STYLE_REF="yellow_node" ID="ID_1331118875" CREATED="1429103005432" MODIFIED="1431335336153">
<node TEXT="$ git add README test.rb LICENSE&#xa;$ git commit -m &apos;initial commit of my project&apos;" STYLE_REF="example_node" ID="ID_1140490564" CREATED="1429103077840" MODIFIED="1429103188845"/>
<node TEXT="Your Git repository now contains five objects: one tree that lists the contents of the directory and specifies which file names are stored as which blobs, and one commit with the pointer to that root tree and all the commit metadata" STYLE_REF="pre_last_node" ID="ID_1336434655" CREATED="1429103065056" MODIFIED="1429103196037"/>
<node TEXT="Running git commit checksums all project directories and stores them as tree objects in the Git repository. Git then creates a commit object that has the metadata and a pointer to the root project tree object so it can re-create that snapshot when needed." STYLE_REF="pre_last_node" ID="ID_1435413009" CREATED="1429103052448" MODIFIED="1429103196039"/>
<node TEXT="" STYLE_REF="pre_last_node" ID="ID_629228595" CREATED="1429103156040" MODIFIED="1429103196040">
<hook URI="./branch-1.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
</node>
<node TEXT="If you make some changes and commit again, the next commit&#xa;stores a pointer to the commit that came immediately before it" STYLE_REF="yellow_node" ID="ID_1630010783" CREATED="1429103298656" MODIFIED="1429103323821">
<node TEXT="" STYLE_REF="pre_last_node" ID="ID_1169956322" CREATED="1429103310303" MODIFIED="1429103328102">
<hook URI="./branch.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
</node>
<node TEXT="A branch in Git is simply a lightweight movable pointer to one of these commits" STYLE_REF="pre_last_node" ID="ID_945933955" CREATED="1429102918128" MODIFIED="1429103213822"/>
<node TEXT="The default branch name in Git is master" STYLE_REF="pre_last_node" ID="ID_1342973770" CREATED="1429102969648" MODIFIED="1429103213821"/>
</node>
<node TEXT="Branching and Merging" STYLE_REF="orange_node" ID="ID_1855983751" CREATED="1429107604271" MODIFIED="1429109914075">
<node TEXT="Basic Branching" STYLE_REF="gray_node" ID="ID_1231108127" CREATED="1429257049111" MODIFIED="1429257253668">
<node TEXT="To create a new branch" STYLE_REF="yellow_node" ID="ID_454548115" CREATED="1429101460273" MODIFIED="1429101500806">
<node TEXT="$ git branch testing" STYLE_REF="example_node" ID="ID_753308689" CREATED="1429101469304" MODIFIED="1429101508150">
<node TEXT="This creates a new pointer at the same commit you&#x2019;re currently on" STYLE_REF="pre_last_node" ID="ID_860894872" CREATED="1429102874145" MODIFIED="1429102883909"/>
</node>
<node TEXT="How does Git know what branch you&#x2019;re currently on?&#xa;It keeps a special pointer called HEAD" STYLE_REF="pre_last_node" ID="ID_1458826165" CREATED="1429101335344" MODIFIED="1429101529615"/>
<node TEXT="The git branch command only created a new branch&#xa;&#x2014; it didn&#x2019;t switch to that branch" STYLE_REF="pre_last_node" ID="ID_344891017" CREATED="1429101375345" MODIFIED="1429101529613"/>
<node TEXT="" STYLE_REF="pre_last_node" ID="ID_1655156068" CREATED="1429101476592" MODIFIED="1429101584310">
<hook URI="./branch-2.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
</node>
<node TEXT="To switch to an existing branch" STYLE_REF="yellow_node" ID="ID_851421865" CREATED="1429101420200" MODIFIED="1429256794451">
<node TEXT="$ git checkout testing" STYLE_REF="example_node" ID="ID_1065680776" CREATED="1429101425776" MODIFIED="1429101508152"/>
<node TEXT="This moves HEAD to point to the testing branch" STYLE_REF="pre_last_node" ID="ID_296554719" CREATED="1429101428320" MODIFIED="1429101584309"/>
<node TEXT="" STYLE_REF="pre_last_node" ID="ID_249069238" CREATED="1429101569617" MODIFIED="1429101584310">
<hook URI="./branch-3.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
<node TEXT="$ git checkout -b iss53" STYLE_REF="example_node" ID="ID_29537090" CREATED="1429110278174" MODIFIED="1429256770999">
<node TEXT="To create a branch and switch to it at the same time" STYLE_REF="pre_last_node" ID="ID_12486876" CREATED="1429110288174" MODIFIED="1429256776566"/>
<node TEXT="Equialent to" STYLE_REF="pre_last_node" ID="ID_472810845" CREATED="1429110306614" MODIFIED="1429256776565">
<node TEXT="$ git branch iss53&#xa;$ git checkout iss53" STYLE_REF="example_node" ID="ID_917844089" CREATED="1429110302094" MODIFIED="1429256783069"/>
</node>
</node>
</node>
<node TEXT="Let&#x2019;s do another commit" STYLE_REF="yellow_node" ID="ID_1519892166" CREATED="1429101653560" MODIFIED="1429101728118">
<node TEXT="$ vim test.rb&#xa;$ git commit -a -m &apos;made a change&apos;" STYLE_REF="example_node" ID="ID_33125993" CREATED="1429101655997" MODIFIED="1429101733349"/>
<node TEXT="The branch that HEAD points to moves forward with each commit" STYLE_REF="pre_last_node" ID="ID_301639896" CREATED="1429101700529" MODIFIED="1429101738774"/>
<node TEXT="" STYLE_REF="pre_last_node" ID="ID_363311059" CREATED="1429101713592" MODIFIED="1429101738774">
<hook URI="./branch-4.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
</node>
<node TEXT="Let&apos;s back to the master branch" STYLE_REF="yellow_node" ID="ID_6598163" CREATED="1429101756928" MODIFIED="1429101838518">
<node TEXT="$ git checkout master" STYLE_REF="example_node" ID="ID_1786270022" CREATED="1429101766129" MODIFIED="1429101833494"/>
<node TEXT="It moved the HEAD pointer back to point to the master branch" STYLE_REF="pre_last_node" ID="ID_985103394" CREATED="1429101788745" MODIFIED="1429102810152"/>
<node TEXT="It reverted the files in your working directory&#xa;back to the snapshot that master points to" STYLE_REF="pre_last_node" ID="ID_754490972" CREATED="1429102525248" MODIFIED="1429102810149"/>
<node TEXT="" STYLE_REF="pre_last_node" ID="ID_803146342" CREATED="1429101790177" MODIFIED="1429102810151">
<hook URI="./branch-5.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
</node>
<node TEXT="Let&#x2019;s make a few changes and commit again" STYLE_REF="yellow_node" ID="ID_776323402" CREATED="1429102624104" MODIFIED="1429257253667">
<node TEXT="$ vim test.rb&#xa;$ git commit -a -m &apos;made other changes&apos;" STYLE_REF="example_node" ID="ID_418693442" CREATED="1429102630976" MODIFIED="1429102800061"/>
<node TEXT="Now your project history has diverged" STYLE_REF="pre_last_node" ID="ID_1688464558" CREATED="1429102648936" MODIFIED="1429102797366"/>
<node TEXT="Both of those changes are isolated in separate branches" STYLE_REF="pre_last_node" ID="ID_577146602" CREATED="1429102787376" MODIFIED="1429102797365"/>
<node TEXT="" STYLE_REF="pre_last_node" ID="ID_1911718144" CREATED="1429102678576" MODIFIED="1429102797366">
<hook URI="./branch-6.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
</node>
<node TEXT="Let&apos;s merge two branches" STYLE_REF="yellow_node" ID="ID_1859980187" CREATED="1429256808905" MODIFIED="1429256967878">
<node TEXT="$ git merge testing" STYLE_REF="example_node" ID="ID_1132251554" CREATED="1429256824794" MODIFIED="1429256974767">
<node TEXT="Will merge master with testing. Master will contain&#xa;all changes made in testing branch, after merging" STYLE_REF="pre_last_node" ID="ID_100602740" CREATED="1429256837513" MODIFIED="1429257163876"/>
</node>
</node>
<node TEXT="Let&apos;s delete branch" STYLE_REF="yellow_node" ID="ID_1804902259" CREATED="1429256907569" MODIFIED="1431335649263">
<node TEXT="$ git branch -d testing" STYLE_REF="example_node" ID="ID_1465834260" CREATED="1429256917607" MODIFIED="1431335649262"/>
<node TEXT="The master branch for now points at the same place" STYLE_REF="pre_last_node" ID="ID_668922288" CREATED="1429256943457" MODIFIED="1429256979141"/>
</node>
</node>
<node TEXT="Basic Merging" STYLE_REF="gray_node" ID="ID_1411497260" CREATED="1429257385840" MODIFIED="1429257629108">
<node TEXT="" STYLE_REF="pre_last_node" ID="ID_1617607001" CREATED="1429257515289" MODIFIED="1429258314097">
<hook URI="b-2.png" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="$ git checkout master&#xa;$ git merge hotfix" STYLE_REF="example_node" ID="ID_1018691553" CREATED="1429257581408" MODIFIED="1429258298578">
<node TEXT="" STYLE_REF="pre_last_node" ID="ID_1960306411" CREATED="1429257541801" MODIFIED="1429258314096">
<hook URI="b-3.png" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="$ git branch -d hotfix" STYLE_REF="example_node" ID="ID_1981485678" CREATED="1429257605864" MODIFIED="1429258298581">
<node TEXT="" STYLE_REF="pre_last_node" ID="ID_1695644998" CREATED="1429257559647" MODIFIED="1429258314095">
<hook URI="b-4.png" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="$ git checkout iss53" STYLE_REF="example_node" ID="ID_1375065874" CREATED="1429257618046" MODIFIED="1429258326819"/>
</node>
</node>
</node>
</node>
</node>
<node TEXT="" STYLE_REF="pre_last_node" ID="ID_264085044" CREATED="1429257551119" MODIFIED="1429258314098">
<hook URI="b-5.png" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="$ git checkout master&#xa;$ git merge iss53" STYLE_REF="example_node" ID="ID_1923866802" CREATED="1429257699415" MODIFIED="1429258298582"/>
<node TEXT="Your development history has diverged from some older point. Because the commit on the branch you&#x2019;re on isn&#x2019;t a direct ancestor of the branch you&#x2019;re merging in." STYLE_REF="pre_last_node" ID="ID_729910428" CREATED="1429257733253" MODIFIED="1429258314082"/>
<node TEXT="Git does a simple three-way merge, using the two snapshots pointed to by the branch tips and the common ancestor of the two" STYLE_REF="pre_last_node" ID="ID_964928296" CREATED="1429257810935" MODIFIED="1429258314087"/>
<node TEXT="Instead of just moving the branch pointer forward, Git creates a new snapshot that results from this three-way merge and automatically creates a new commit that points to it" STYLE_REF="pre_last_node" ID="ID_552914752" CREATED="1429257859702" MODIFIED="1429258314091">
<node TEXT="" STYLE_REF="pre_last_node" ID="ID_1950240173" CREATED="1429257927112" MODIFIED="1429258314094">
<hook URI="b-6.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
</node>
</node>
</node>
<node TEXT="Merge Conflicts" STYLE_REF="gray_node" ID="ID_695170134" CREATED="1429258281629" MODIFIED="1431335299957">
<node TEXT="If you changed the same part of the same file differently in the two branches you&#x2019;re merging together, Git won&#x2019;t be able to merge them cleanly. You&#x2019;ll get a merge conflict" STYLE_REF="yellow_node" ID="ID_320655231" CREATED="1429258354031" MODIFIED="1429258940584">
<node TEXT="$ git merge iss53" STYLE_REF="example_node" ID="ID_175127142" CREATED="1429258385214" MODIFIED="1429258950716"><richcontent TYPE="DETAILS" HIDDEN="true">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Auto-merging index.html
    </p>
    <p>
      CONFLICT (content): Merge conflict in index.html
    </p>
    <p>
      Automatic merge failed; fix conflicts and then commit the result
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="It has paused the process while you resolve the conflict.If you want to see which files are unmerged at any point after a merge conflict" STYLE_REF="yellow_node" ID="ID_1609597162" CREATED="1429258414270" MODIFIED="1429258940578">
<node TEXT="$ git status" STYLE_REF="example_node" ID="ID_403240688" CREATED="1429258435990" MODIFIED="1429258950714"><richcontent TYPE="DETAILS" HIDDEN="true">

<html>
  <head>
    
  </head>
  <body>
    <p>
      On branch master
    </p>
    <p>
      You have unmerged paths.
    </p>
    <p>
      &#160;&#160;(fix conflicts and run &quot;git commit&quot;)
    </p>
    <p>
      
    </p>
    <p>
      Unmerged paths:
    </p>
    <p>
      &#160;&#160;(use &quot;git add &lt;file&gt;...&quot; to mark resolution)
    </p>
    <p>
      
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;both modified:&#160;&#160;&#160;&#160;&#160;&#160;index.html
    </p>
    <p>
      
    </p>
    <p>
      no changes added to commit (use &quot;git add&quot; and/or &quot;git commit -a&quot;)
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Anything that has merge conflicts and hasn&#x2019;t&#xa;been resolved is listed as unmerged" STYLE_REF="pre_last_node" ID="ID_1610862192" CREATED="1429258473671" MODIFIED="1429258960233"/>
</node>
<node TEXT="Git adds standard conflict-resolution markers to the files that have conflicts" STYLE_REF="yellow_node" ID="ID_683347209" CREATED="1429258505869" MODIFIED="1431335299954">
<node TEXT="&lt;&lt;&lt;&lt;&lt;&lt;&lt; HEAD&#xa;&lt;div id=&quot;footer&quot;&gt;contact : email.support@github.com&lt;/div&gt;&#xa;=======&#xa;&lt;div id=&quot;footer&quot;&gt;&#xa;  please contact us at support@github.com&#xa;&lt;/div&gt;&#xa;&gt;&gt;&gt;&gt;&gt;&gt;&gt; iss53" STYLE_REF="example_node" ID="ID_1676363995" CREATED="1429258552278" MODIFIED="1429258950716">
<node TEXT="This means the version in HEAD is the top part of that block (everything above the =======), while the version in your iss53 branch looks like everything in the bottom part" STYLE_REF="pre_last_node" ID="ID_155226782" CREATED="1429258619110" MODIFIED="1429258960236"/>
<node TEXT="In order to resolve the conflict, you have to either choose&#xa;one side or the other or merge the contents yourself" STYLE_REF="pre_last_node" ID="ID_1638132086" CREATED="1429258651598" MODIFIED="1429258960238"/>
<node TEXT="You might resolve this conflict by replacing the entire block with this" STYLE_REF="pre_last_node" ID="ID_610733547" CREATED="1429258671436" MODIFIED="1429258960240"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &lt;div id=&quot;footer&quot;&gt;
    </p>
    <p>
      please contact us at email.support@github.com
    </p>
    <p>
      &lt;/div&gt;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="Graphical tool for resolving" STYLE_REF="pre_last_node" ID="ID_1435848674" CREATED="1429258744574" MODIFIED="1429258960244">
<node TEXT="$ git mergetool" STYLE_REF="example_node" ID="ID_1065620900" CREATED="1429258760038" MODIFIED="1429258950718"/>
</node>
<node TEXT="After you exit the merge tool, Git asks you if the merge was successful. If you tell the script that it was, it stages the file to mark it as resolved for you" STYLE_REF="pre_last_node" ID="ID_59080222" CREATED="1429258884382" MODIFIED="1429258960240"/>
<node TEXT="If you&#x2019;re happy with that, and you verify that everything that had conflicts has been staged, you can type git commit to finalize the merge commi" STYLE_REF="pre_last_node" ID="ID_1808940722" CREATED="1429258916325" MODIFIED="1429258960242"/>
</node>
</node>
<node TEXT="Branch Management" STYLE_REF="orange_node" ID="ID_1823947636" CREATED="1429259043444" MODIFIED="1429259365641">
<node TEXT="$ git branch" STYLE_REF="example_node" ID="ID_1854825548" CREATED="1429259082589" MODIFIED="1429259359634"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#160;&#160;iss53
    </p>
    <p>
      * master
    </p>
    <p>
      &#160;&#160;testing
    </p>
  </body>
</html>
</richcontent>
<node TEXT="If you run it with no arguments, you get a simple listing of your current branches" STYLE_REF="pre_last_node" ID="ID_527491473" CREATED="1429259102560" MODIFIED="1429259374495"/>
</node>
<node TEXT="$ git branch -a" STYLE_REF="example_node" ID="ID_424821257" CREATED="1430106169729" MODIFIED="1430106214631"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      * master
    </p>
    <p>
      &#160;&#160;remotes/origin/HEAD -&gt; origin/master
    </p>
    <p>
      &#160;&#160;remotes/origin/style
    </p>
    <p>
      &#160;&#160;remotes/origin/master
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Local and remote branches" STYLE_REF="pre_last_node" ID="ID_82650107" CREATED="1430106193121" MODIFIED="1430106217741"/>
</node>
<node TEXT="$ git branch -v" STYLE_REF="example_node" ID="ID_80662917" CREATED="1429259145045" MODIFIED="1429259359632"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#160;iss53&#160;&#160;&#160;93b412c fix javascript issue
    </p>
    <p>
      * master&#160;&#160;7a98805 Merge branch 'iss53'
    </p>
    <p>
      &#160;&#160;testing 782fd34 add scott to the author list in the readmes
    </p>
  </body>
</html>
</richcontent>
<node TEXT="To see the last commit on each branch" STYLE_REF="pre_last_node" ID="ID_834432926" CREATED="1429259153414" MODIFIED="1429259374495"/>
</node>
<node TEXT="$ git branch --merged" STYLE_REF="example_node" ID="ID_1362451062" CREATED="1429259203891" MODIFIED="1429259359635"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#160;&#160;iss53
    </p>
    <p>
      * master
    </p>
  </body>
</html>
</richcontent>
<node TEXT="To see which branches are already merged into the branch you&#x2019;re on" STYLE_REF="pre_last_node" ID="ID_973729685" CREATED="1429259223126" MODIFIED="1429259374493"/>
<node TEXT="Branches on this list without the * in front&#xa;of them are generally fine to delete" STYLE_REF="pre_last_node" ID="ID_178890360" CREATED="1429259329406" MODIFIED="1429259374490"/>
</node>
<node TEXT="$ git branch --no-merged" STYLE_REF="example_node" ID="ID_1229705730" CREATED="1429259261121" MODIFIED="1429259359634"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#160;&#160;testing
    </p>
  </body>
</html>
</richcontent>
<node TEXT="To see all the branches that contain work you haven&#x2019;t yet merged in" STYLE_REF="pre_last_node" ID="ID_1005570530" CREATED="1429259275316" MODIFIED="1429259374490"/>
<node TEXT="Trying to delete it with git branch -d will fail" STYLE_REF="pre_last_node" ID="ID_1733847438" CREATED="1429259299837" MODIFIED="1429259374489"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      error: The branch 'testing' is not fully merged.
    </p>
    <p>
      If you are sure you want to delete it, run 'git branch -D testing'
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="Branching Workflows" STYLE_REF="orange_node" ID="ID_1661869540" CREATED="1429265921364" MODIFIED="1429266359687">
<node TEXT="Long-Running Branches" STYLE_REF="gray_node" ID="ID_1880992301" CREATED="1429266058027" MODIFIED="1429266377904">
<node TEXT="Master branch" STYLE_REF="pre_last_node" ID="ID_1342213528" CREATED="1429266121820" MODIFIED="1429266387649"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      having only code that is entirely stable &#8212; possibly
    </p>
    <p>
      only code that has been or will be released
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Develop branch" STYLE_REF="pre_last_node" ID="ID_1797859794" CREATED="1429266153988" MODIFIED="1429266387648"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      For using to test stability &#8212; it isn&#8217;t necessarily always stable, but whenever it gets to a stable state, it can be merged into master
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="" STYLE_REF="pre_last_node" ID="ID_1251164657" CREATED="1429266211819" MODIFIED="1429266387650">
<hook URI="lb1.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
<node TEXT="" STYLE_REF="pre_last_node" ID="ID_1268771391" CREATED="1429266345772" MODIFIED="1429266387650">
<hook URI="lb2.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
<node TEXT="The idea is that your branches are at various levels of stability; when they reach a more stable level, they&#x2019;re merged into the branch above them" STYLE_REF="pre_last_node" ID="ID_381480621" CREATED="1429266408651" MODIFIED="1429266699657"/>
</node>
<node TEXT="Topic Branches" STYLE_REF="gray_node" ID="ID_1378981810" CREATED="1429266372147" MODIFIED="1429270324401">
<node TEXT="Consider an example of doing some work (on master), branching off for an issue (iss91), working on it for a bit, branching off the second branch to try another way of handling the same thing (iss91v2), going back to your master branch and working there for a while, and then branching off there to do some work that you&#x2019;re not sure is a good idea (dumbidea branch)" STYLE_REF="pre_last_node" ID="ID_1671786601" CREATED="1429266665353" MODIFIED="1429266699652"/>
<node TEXT="" STYLE_REF="pre_last_node" ID="ID_1334208306" CREATED="1429266671483" MODIFIED="1429266699651">
<hook URI="lb3.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
<node TEXT="Now, let&#x2019;s say you decide you like the second solution to your issue best (iss91v2); and you showed the dumbidea branch to your coworkers, and it turns out to be genius. You can throw away the original iss91 branch (losing commits C5 and C6) and merge in the other two" STYLE_REF="pre_last_node" ID="ID_721689524" CREATED="1429266686464" MODIFIED="1429270324400"/>
<node TEXT="" STYLE_REF="pre_last_node" ID="ID_375370439" CREATED="1429266688976" MODIFIED="1429266699648">
<hook URI="lb4.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
</node>
</node>
<node TEXT="Remote Branches" STYLE_REF="orange_node" ID="ID_215869569" CREATED="1429267030141" MODIFIED="1429267034486">
<node TEXT="Basic" STYLE_REF="gray_node" ID="ID_674902675" CREATED="1430104713970" MODIFIED="1430104727863">
<node TEXT="They&#x2019;re local branches that you can&#x2019;t move; they&#x2019;re moved automatically whenever you do any network communication. Remote branches act as bookmarks to remind you where the branches on your remote repositories were the last time you connected to them" STYLE_REF="pre_last_node" ID="ID_21294467" CREATED="1429283802448" MODIFIED="1429514924819"/>
<node TEXT="(remote)/(branch)" STYLE_REF="pre_last_node" ID="ID_1260872581" CREATED="1429514393329" MODIFIED="1429514924815"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      They take the form
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="A Git clone gives you your own master branch and&#xa;origin/master pointing to origin&#x2019;s master branch" STYLE_REF="pre_last_node" ID="ID_946514803" CREATED="1429514456102" MODIFIED="1429514924810">
<hook URI="rb1.png" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Working locally and having someone push to your remote&#xa;server makes each history move forward differently" STYLE_REF="pre_last_node" ID="ID_1989771977" CREATED="1429514515344" MODIFIED="1429514924806">
<hook URI="rb2.png" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT=" The git fetch command updates your remote references" STYLE_REF="pre_last_node" ID="ID_1308618241" CREATED="1429514642274" MODIFIED="1429514924805">
<hook URI="rb3.png" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Adding another server as a remote" STYLE_REF="pre_last_node" ID="ID_1422185744" CREATED="1429514714597" MODIFIED="1429514924802">
<hook URI="rb4.png" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Now, you can run git fetch teamone to fetch everything the remote teamone server has that you don&#x2019;t have yet. Because that server has a subset of the data your origin server has right now, Git fetches no data but sets a remote branch called teamone/master to point to the commit that teamone has as its master branch" STYLE_REF="pre_last_node" ID="ID_595264638" CREATED="1429514816534" MODIFIED="1429514924797">
<hook URI="rb6.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
</node>
</node>
</node>
</node>
</node>
<node TEXT="Pushing" STYLE_REF="gray_node" ID="ID_1303629123" CREATED="1429514951414" MODIFIED="1430106137653">
<node TEXT="$ git push origin serverfix" STYLE_REF="example_node" ID="ID_1841435689" CREATED="1429515008454" MODIFIED="1429515367108"><richcontent TYPE="DETAILS" HIDDEN="true">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Counting objects: 20, done.
    </p>
    <p>
      Compressing objects: 100% (14/14), done.
    </p>
    <p>
      Writing objects: 100% (15/15), 1.74 KiB, done.
    </p>
    <p>
      Total 15 (delta 5), reused 0 (delta 0)
    </p>
    <p>
      To git@github.com:schacon/simplegit.git
    </p>
    <p>
      &#160;* [new branch]&#160;&#160;&#160;&#160;&#160;&#160;serverfix -&gt; serverfix
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="git push origin serverfix:awesomebranch" STYLE_REF="example_node" ID="ID_112936021" CREATED="1429515076212" MODIFIED="1429515367107">
<node TEXT="To push your local serverfix branch to the&#xa;awesomebranch branch on the remote project" STYLE_REF="pre_last_node" ID="ID_636850142" CREATED="1429515086965" MODIFIED="1429515372325"/>
</node>
<node TEXT="$ git fetch origin" STYLE_REF="example_node" ID="ID_1946979705" CREATED="1429515207992" MODIFIED="1430106137653"><richcontent TYPE="DETAILS" HIDDEN="true">

<html>
  <head>
    
  </head>
  <body>
    <p>
      remote: Counting objects: 20, done.
    </p>
    <p>
      remote: Compressing objects: 100% (14/14), done.
    </p>
    <p>
      remote: Total 15 (delta 5), reused 0 (delta 0)
    </p>
    <p>
      Unpacking objects: 100% (15/15), done.
    </p>
    <p>
      From git@github.com:schacon/simplegit
    </p>
    <p>
      &#160;* [new branch]&#160;&#160;&#160;&#160;&#160;&#160;serverfix&#160;&#160;&#160;&#160;-&gt; origin/serverfix
    </p>
  </body>
</html>
</richcontent>
<node TEXT="The next time one of your collaborators fetches from the server, they will get a reference to where the server&#x2019;s version of serverfix is under the remote branch origin/serverfix" STYLE_REF="pre_last_node" ID="ID_269655729" CREATED="1429515200035" MODIFIED="1429515372320"/>
<node TEXT="When you do a fetch that brings down new remote branches, you don&#x2019;t automatically have local, editable copies of them. In other words, in this case, you don&#x2019;t have a new serverfix branch &#x2014; you only have an origin/serverfix pointer that you can&#x2019;t modify" STYLE_REF="pre_last_node" ID="ID_779046188" CREATED="1429515236592" MODIFIED="1429515372317"/>
</node>
<node TEXT="$ git merge origin/serverfix" STYLE_REF="example_node" ID="ID_1792190129" CREATED="1429515292604" MODIFIED="1429515367106">
<node TEXT="To merge this work into your current working branch" STYLE_REF="pre_last_node" ID="ID_849682274" CREATED="1429515305655" MODIFIED="1429515372319"/>
</node>
<node TEXT="$ git checkout -b serverfix origin/serverfix" STYLE_REF="example_node" ID="ID_631809557" CREATED="1429515318395" MODIFIED="1429515367105"><richcontent TYPE="DETAILS" HIDDEN="true">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Branch serverfix set up to track remote branch serverfix from origin.
    </p>
    <p>
      Switched to a new branch 'serverfix'
    </p>
  </body>
</html>
</richcontent>
<node TEXT="To create your own serverfix branch that you can work on,&#xa;you can base it off your remote branch" STYLE_REF="pre_last_node" ID="ID_1782163236" CREATED="1429515345109" MODIFIED="1429515372314"/>
</node>
</node>
<node TEXT="Tracking Branches" STYLE_REF="gray_node" ID="ID_451883979" CREATED="1429515375070" MODIFIED="1429515651899">
<node TEXT="Tracking branches are local branches that have&#xa;a direct relationship to a remote branch" STYLE_REF="pre_last_node" ID="ID_1636852699" CREATED="1429515415840" MODIFIED="1429515663029"/>
<node TEXT="If you&#x2019;re on a tracking branch and type git push, Git&#xa;automatically knows which server and branch to push to" STYLE_REF="pre_last_node" ID="ID_1705500334" CREATED="1429515454654" MODIFIED="1429515663033"/>
<node TEXT="When you clone a repository, it generally automatically&#xa;creates a master branch that tracks origin/master" STYLE_REF="pre_last_node" ID="ID_1283190929" CREATED="1429515517156" MODIFIED="1429515663026"/>
<node TEXT="$ git checkout --track origin/serverfix" STYLE_REF="example_node" ID="ID_558339086" CREATED="1429515527142" MODIFIED="1431334936587"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Branch serverfix set up to track remote branch serverfix from origin.
    </p>
    <p>
      Switched to a new branch 'serverfix'
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="$ git checkout -b sf origin/serverfix" STYLE_REF="example_node" ID="ID_576989397" CREATED="1429515594493" MODIFIED="1429515656130"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Branch sf set up to track remote branch serverfix from origin.
    </p>
    <p>
      Switched to a new branch 'sf'
    </p>
  </body>
</html>
</richcontent>
<node TEXT="To set up a local branch with a different name than the remote branch" STYLE_REF="pre_last_node" ID="ID_1943699047" CREATED="1429515635542" MODIFIED="1429515663038"/>
<node TEXT="Now, your local branch sf will automatically push to and pull from origin/serverfix" STYLE_REF="pre_last_node" ID="ID_1399685580" CREATED="1429515616654" MODIFIED="1429515663038"/>
</node>
</node>
<node TEXT="Deleting Remote Branches" STYLE_REF="gray_node" ID="ID_845183470" CREATED="1429515645749" MODIFIED="1429515651897">
<node TEXT="git push [remotename] :[branch]" STYLE_REF="example_node" ID="ID_460816257" CREATED="1429515745933" MODIFIED="1429515783034"/>
<node TEXT="$ git push origin :serverfix" STYLE_REF="example_node" ID="ID_24578745" CREATED="1429515756453" MODIFIED="1429515783033">
<node TEXT="To delete your serverfix branch from the server" STYLE_REF="pre_last_node" ID="ID_1179914479" CREATED="1429515774046" MODIFIED="1429515818683"/>
<node TEXT="Take nothing on my side and make it be [remotebranch]" STYLE_REF="pre_last_node" ID="ID_1119883958" CREATED="1429515813518" MODIFIED="1429515818682"/>
</node>
</node>
</node>
<node TEXT="Rebasing" STYLE_REF="orange_node" ID="ID_1117054734" CREATED="1429515840269" MODIFIED="1429515846338">
<node TEXT="The Basic Rebase" STYLE_REF="gray_node" ID="ID_862309847" CREATED="1429515872982" MODIFIED="1429516344273">
<node TEXT="Initial" STYLE_REF="pre_last_node" ID="ID_482656909" CREATED="1429516188254" MODIFIED="1429516640553">
<hook URI="rebase.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
<node TEXT="After merge" STYLE_REF="pre_last_node" ID="ID_607599687" CREATED="1429516002794" MODIFIED="1429516640547">
<hook URI="rebase1.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
<node TEXT="With the rebase command, you can take all the changes that were committed on one branch and replay them on another one." STYLE_REF="pre_last_node" ID="ID_1201102365" CREATED="1429515978428" MODIFIED="1429516640541"/>
<node TEXT="$ git checkout experiment&#xa;$ git rebase master" STYLE_REF="example_node" ID="ID_1127716727" CREATED="1429516000292" MODIFIED="1429516338745">
<node TEXT="It works by going to the common ancestor of the two branches, getting the diff introduced by each commit of the branch you&#x2019;re on, saving those diffs to temporary files, resetting the current branch to the same commit as the branch you are rebasing onto, and finally applying each change in turn" STYLE_REF="pre_last_node" ID="ID_1469353118" CREATED="1429516125734" MODIFIED="1429516640537"/>
<node TEXT="" STYLE_REF="pre_last_node" ID="ID_1998434045" CREATED="1429516165622" MODIFIED="1429516640540">
<hook URI="rebase2.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
</node>
<node TEXT="At this point, you can go back to the master branch and do a fast-forward merge" STYLE_REF="pre_last_node" ID="ID_962249048" CREATED="1429516264509" MODIFIED="1429516640533">
<node TEXT="" STYLE_REF="pre_last_node" ID="ID_1860228042" CREATED="1429516327829" MODIFIED="1429516640536">
<hook URI="rebase3.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
</node>
<node TEXT="There is no difference in the end product of the integration, but rebasing makes for a cleaner history. If you examine the log of a rebased branch, it looks like a linear history: it appears that all the work happened in series, even when it originally happened in parallel" STYLE_REF="pre_last_node" ID="ID_1088950066" CREATED="1429516315876" MODIFIED="1429516640530"/>
</node>
<node TEXT="More Interesting Rebases" STYLE_REF="gray_node" ID="ID_798465968" CREATED="1429516438951" MODIFIED="1429517843088">
<node TEXT="Initial" STYLE_REF="pre_last_node" ID="ID_1663139229" CREATED="1429516567054" MODIFIED="1429517109653">
<hook URI="rebase4.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
<node TEXT="$ git rebase --onto master server client" STYLE_REF="example_node" ID="ID_1115909442" CREATED="1429516570557" MODIFIED="1429516693786">
<node TEXT="You can take the changes on client that aren&#x2019;t on server" STYLE_REF="pre_last_node" ID="ID_795529902" CREATED="1429516764199" MODIFIED="1429517109651"/>
<node TEXT="Check out the client branch, figure out the patches from the common ancestor of the client and server branches, and then replay them onto master" STYLE_REF="pre_last_node" ID="ID_1777296898" CREATED="1429516680924" MODIFIED="1429517109650"/>
<node TEXT="" STYLE_REF="pre_last_node" ID="ID_622844622" CREATED="1429516789406" MODIFIED="1429517109650">
<hook URI="rebase5.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
</node>
<node TEXT="$ git checkout master&#xa;$ git merge client" STYLE_REF="example_node" ID="ID_1735411235" CREATED="1429516567225" MODIFIED="1429517096056">
<node TEXT="Fast-forwarding your master branch to include the client branch changes" STYLE_REF="pre_last_node" ID="ID_1442460339" CREATED="1429516833885" MODIFIED="1429517109648"/>
<node TEXT="" STYLE_REF="pre_last_node" ID="ID_1720525927" CREATED="1429516862892" MODIFIED="1429517109649">
<hook URI="rebase6.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
</node>
<node TEXT="$ git rebase master server" STYLE_REF="example_node" ID="ID_1081258171" CREATED="1429516920092" MODIFIED="1429517096052">
<node TEXT="This replays your server work on top of your master work" STYLE_REF="pre_last_node" ID="ID_1452979598" CREATED="1429516950773" MODIFIED="1429517109658"/>
<node TEXT="" STYLE_REF="pre_last_node" ID="ID_1859119262" CREATED="1429516967557" MODIFIED="1429517109659">
<hook URI="rebase7.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
</node>
<node TEXT="$ git checkout master&#xa;$ git merge server" STYLE_REF="example_node" ID="ID_1541934649" CREATED="1429516982245" MODIFIED="1429517096049">
<node TEXT="fast-forward the base branch" STYLE_REF="pre_last_node" ID="ID_1944246182" CREATED="1429517018532" MODIFIED="1429517109660"/>
<node TEXT="" STYLE_REF="pre_last_node" ID="ID_896629875" CREATED="1429517020559" MODIFIED="1429517109660">
<hook URI="rebase7.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
</node>
<node TEXT="$ git branch -d client&#xa;$ git branch -d server" STYLE_REF="example_node" ID="ID_1699373184" CREATED="1429517030940" MODIFIED="1429517096054">
<node TEXT="" STYLE_REF="pre_last_node" ID="ID_1891193937" CREATED="1429517080398" MODIFIED="1429517109661">
<hook URI="rebase8.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
</node>
</node>
<node TEXT="One more" STYLE_REF="gray_node" ID="ID_611610228" CREATED="1429517862046" MODIFIED="1429520180963">
<node TEXT="git rebase --abort" STYLE_REF="example_node" ID="ID_1918069275" CREATED="1429517869391" MODIFIED="1429520186200"/>
<node TEXT="git rebase --continue" STYLE_REF="example_node" ID="ID_562078921" CREATED="1429517880740" MODIFIED="1429520186200"/>
<node TEXT="git rebase --skip" STYLE_REF="example_node" ID="ID_320573701" CREATED="1429517886451" MODIFIED="1429520186198"/>
</node>
</node>
</node>
<node TEXT="Distributed Git" STYLE_REF="second_node" POSITION="right" ID="ID_1966758784" CREATED="1429521606083" MODIFIED="1430110213812" HGAP="86" VSHIFT="-63">
<edge COLOR="#7c007c"/>
<hook URI="../../Images/network-1.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Distributed Workflows" STYLE_REF="orange_node" ID="ID_684645188" CREATED="1429521635127" MODIFIED="1429551402167">
<node TEXT="Centralized Workflow" STYLE_REF="gray_node" ID="ID_262510052" CREATED="1429521644596" MODIFIED="1429551411269">
<node TEXT="In centralized systems, there is generally a single collaboration model&#x2014;the centralized workflow. One central hub, or repository, can accept code, and everyone synchronizes their work to it. A number of developers are nodes &#x2014; consumers of that hub &#x2014; and synchronize to that one place" STYLE_REF="pre_last_node" ID="ID_1566284417" CREATED="1429521735747" MODIFIED="1429551419734"/>
<node TEXT="" STYLE_REF="pre_last_node" ID="ID_11550131" CREATED="1429521834788" MODIFIED="1429551419733">
<hook URI="dist1.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
</node>
<node TEXT="Integration-Manager Workflow" STYLE_REF="gray_node" ID="ID_261648949" CREATED="1429521859971" MODIFIED="1429551411269">
<node TEXT="Because Git allows you to have multiple remote repositories, it&#x2019;s possible to have a workflow where each developer has write access to their own public repository and read access to everyone else&#x2019;s. This scenario often includes a canonical repository that represents the &quot;official&quot; project. To contribute to that project, you create your own public clone of the project and push your changes to it. Then, you can send a request to the maintainer of the main project to pull in your changes. They can add your repository as a remote, test your changes locally, merge them into their branch, and push back to their repository" STYLE_REF="pre_last_node" ID="ID_1138052375" CREATED="1429599053827" MODIFIED="1429599112876"/>
<node TEXT="Workflow" STYLE_REF="yellow_node" ID="ID_1566222378" CREATED="1429599069420" MODIFIED="1429599662017">
<node STYLE_REF="pre_last_node" ID="ID_825710443" CREATED="1429599076783" MODIFIED="1429599112873"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      The project maintainer pushes to their public repository
    </p>
  </body>
</html>
</richcontent>
</node>
<node STYLE_REF="pre_last_node" ID="ID_707790882" CREATED="1429599082350" MODIFIED="1429599112874"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      A contributor clones that repository and makes changes
    </p>
  </body>
</html>
</richcontent>
</node>
<node STYLE_REF="pre_last_node" ID="ID_860449255" CREATED="1429599087502" MODIFIED="1429599112874"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      The contributor pushes to their own public copy
    </p>
  </body>
</html>
</richcontent>
</node>
<node STYLE_REF="pre_last_node" ID="ID_490599616" CREATED="1429599092837" MODIFIED="1429599112875"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      The contributor sends the maintainer an e-mail asking them to pull changes
    </p>
  </body>
</html>
</richcontent>
</node>
<node STYLE_REF="pre_last_node" ID="ID_1211121492" CREATED="1429599098405" MODIFIED="1429599112875"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      The maintainer adds the contributor&#8217;s repo as a remote and merges locally
    </p>
  </body>
</html>
</richcontent>
</node>
<node STYLE_REF="pre_last_node" ID="ID_1385776343" CREATED="1429599103245" MODIFIED="1429599112875"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      The maintainer pushes merged changes to the main repository
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="" STYLE_REF="pre_last_node" ID="ID_1364301486" CREATED="1429599213651" MODIFIED="1429599681273">
<hook URI="dist2.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
</node>
<node TEXT="Dictator and Lieutenants Workflow" STYLE_REF="gray_node" ID="ID_228901375" CREATED="1429599192212" MODIFIED="1429599671216">
<node TEXT="This is a variant of a multiple-repository workflow. It&#x2019;s generally used by huge projects with hundreds of collaborators; one famous example is the Linux kernel. Various integration managers are in charge of certain parts of the repository; they&#x2019;re called lieutenants. All the lieutenants have one integration manager known as the benevolent dictator. The benevolent dictator&#x2019;s repository serves as the reference repository from which all the collaborators need to pull" STYLE_REF="pre_last_node" ID="ID_124882283" CREATED="1429599599756" MODIFIED="1429599681272"/>
<node TEXT="Workflow" STYLE_REF="yellow_node" ID="ID_1509290422" CREATED="1429599609995" MODIFIED="1429599668049">
<node TEXT="Regular developers work on their topic branch and rebase their work on top of master. The master branch is that of the dictator" STYLE_REF="pre_last_node" ID="ID_1234108385" CREATED="1429599614581" MODIFIED="1429599681265"/>
<node TEXT="Lieutenants merge the developers&#x2019; topic branches into their master branch" STYLE_REF="pre_last_node" ID="ID_1445585164" CREATED="1429599625875" MODIFIED="1429599681268"/>
<node TEXT="The dictator merges the lieutenants&#x2019; master branches&#xa;into the dictator&#x2019;s master branch" STYLE_REF="pre_last_node" ID="ID_442863041" CREATED="1429599633460" MODIFIED="1429599681269"/>
<node TEXT="The dictator pushes their master to the reference&#xa;repository so the other developers can rebase on it" STYLE_REF="pre_last_node" ID="ID_1964761437" CREATED="1429599644069" MODIFIED="1429599681271"/>
</node>
<node TEXT="" STYLE_REF="pre_last_node" ID="ID_1914948427" CREATED="1429599221836" MODIFIED="1429599681273">
<hook URI="dist3.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
</node>
</node>
</node>
<node TEXT="Tools" STYLE_REF="second_node" POSITION="right" ID="ID_102731544" CREATED="1429599919652" MODIFIED="1430109977363" HGAP="12" VSHIFT="-7">
<edge COLOR="#007c7c"/>
<hook URI="../../Images/tools.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Interactive Staging" STYLE_REF="orange_node" ID="ID_642236140" CREATED="1429599938588" MODIFIED="1429600293192">
<node TEXT="$ git add -i" STYLE_REF="example_node" ID="ID_693722491" CREATED="1429599955164" MODIFIED="1429602351263"><richcontent TYPE="DETAILS" HIDDEN="true">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;staged&#160;&#160;&#160;&#160;&#160;unstaged path
    </p>
    <p>
      &#160;&#160;1:&#160;&#160;&#160;&#160;unchanged&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;+0/-1 TODO
    </p>
    <p>
      &#160;&#160;2:&#160;&#160;&#160;&#160;unchanged&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;+1/-1 index.html
    </p>
    <p>
      &#160;&#160;3:&#160;&#160;&#160;&#160;unchanged&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;+5/-1 lib/simplegit.rb
    </p>
    <p>
      
    </p>
    <p>
      *** Commands ***
    </p>
    <p>
      &#160;&#160;1: status&#160;&#160;&#160;&#160;&#160;2: update&#160;&#160;&#160;&#160;&#160;&#160;3: revert&#160;&#160;&#160;&#160;&#160;4: add untracked
    </p>
    <p>
      &#160;&#160;5: patch&#160;&#160;&#160;&#160;&#160;&#160;6: diff&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;7: quit&#160;&#160;&#160;&#160;&#160;&#160;&#160;8: help
    </p>
    <p>
      What now&gt;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Type 2 or u" STYLE_REF="pre_last_node" ID="ID_1867923937" CREATED="1429600055059" MODIFIED="1429602399879">
<node TEXT="What now&gt; 2&#xa;           staged     unstaged path&#xa;  1:    unchanged        +0/-1 TODO&#xa;  2:    unchanged        +1/-1 index.html&#xa;  3:    unchanged        +5/-1 lib/simplegit.rb&#xa;Update&gt;&gt;" STYLE_REF="example_node" ID="ID_138672202" CREATED="1429600071916" MODIFIED="1429600317320"/>
</node>
<node TEXT="To stage the TODO and index.html files, type 1 and 2" STYLE_REF="pre_last_node" ID="ID_530975759" CREATED="1429600086747" MODIFIED="1429600459521">
<node TEXT="Update&gt;&gt; 1,2&#xa;           staged     unstaged path&#xa;* 1:    unchanged        +0/-1 TODO&#xa;* 2:    unchanged        +1/-1 index.html&#xa;  3:    unchanged        +5/-1 lib/simplegit.rb&#xa;Update&gt;&gt;" STYLE_REF="example_node" ID="ID_820192427" CREATED="1429600092940" MODIFIED="1429600317319">
<node TEXT="The * next to each file means the file is selected to be staged" STYLE_REF="pre_last_node" ID="ID_1163834358" CREATED="1429600131036" MODIFIED="1429600459523"/>
<node TEXT="If you press Enter after typing nothing at the Update&gt;&gt; prompt, Git takes anything selected and stages it for you" STYLE_REF="pre_last_node" ID="ID_1795847547" CREATED="1429600151972" MODIFIED="1429600459521"/>
<node TEXT="Update&gt;&gt;&#xa;updated 2 paths&#xa;&#xa;*** Commands ***&#xa;  1: status     2: update      3: revert     4: add untracked&#xa;  5: patch      6: diff        7: quit       8: help&#xa;What now&gt; 1&#xa;           staged     unstaged path&#xa;  1:        +0/-1      nothing TODO&#xa;  2:        +1/-1      nothing index.html&#xa;  3:    unchanged        +5/-1 lib/simplegit.rb" STYLE_REF="example_node" ID="ID_1127242730" CREATED="1429600196485" MODIFIED="1429600317318"/>
</node>
</node>
<node TEXT="If you want to unstage the TODO file at this point, you use the 3 or r" STYLE_REF="pre_last_node" ID="ID_1680686014" CREATED="1429600222843" MODIFIED="1429600459526">
<node TEXT="*** Commands ***&#xa;  1: status     2: update      3: revert     4: add untracked&#xa;  5: patch      6: diff        7: quit       8: help&#xa;What now&gt; 3&#xa;           staged     unstaged path&#xa;  1:        +0/-1      nothing TODO&#xa;  2:        +1/-1      nothing index.html&#xa;  3:    unchanged        +5/-1 lib/simplegit.rb&#xa;Revert&gt;&gt; 1&#xa;           staged     unstaged path&#xa;* 1:        +0/-1      nothing TODO&#xa;  2:        +1/-1      nothing index.html&#xa;  3:    unchanged        +5/-1 lib/simplegit.rb&#xa;Revert&gt;&gt; [enter]&#xa;reverted one path" STYLE_REF="example_node" ID="ID_407985195" CREATED="1429600233987" MODIFIED="1429600317316"/>
</node>
<node TEXT="To see the diff of what you&#x2019;ve staged, you can use the 6 or d (for diff) command" STYLE_REF="pre_last_node" ID="ID_1018151640" CREATED="1429600276411" MODIFIED="1429600459527">
<node TEXT="*** Commands ***&#xa;  1: status     2: update      3: revert     4: add untracked&#xa;  5: patch      6: diff        7: quit       8: help&#xa;What now&gt; 6&#xa;           staged     unstaged path&#xa;  1:        +1/-1      nothing index.html&#xa;Review diff&gt;&gt; 1&#xa;diff --git a/index.html b/index.html&#xa;index 4d07108..4335f49 100644&#xa;--- a/index.html&#xa;+++ b/index.html&#xa;@@ -16,7 +16,7 @@ Date Finder&#xa;&#xa; &lt;p id=&quot;out&quot;&gt;...&lt;/p&gt;&#xa;&#xa;-&lt;div id=&quot;footer&quot;&gt;contact : support@github.com&lt;/div&gt;&#xa;+&lt;div id=&quot;footer&quot;&gt;contact : email.support@github.com&lt;/div&gt;&#xa;&#xa; &lt;script type=&quot;text/javascript&quot;&gt;" STYLE_REF="example_node" ID="ID_555596680" CREATED="1429600283362" MODIFIED="1429600317313"/>
</node>
</node>
<node TEXT="Stashing" STYLE_REF="orange_node" ID="ID_925369621" CREATED="1429600443762" MODIFIED="1429600449656">
<node TEXT="Stashing takes the dirty state of your working directory &#x2014; that is, your modified tracked files and staged changes &#x2014; and saves it on a stack of unfinished changes that you can reapply at any time" STYLE_REF="pre_last_node" ID="ID_1111161032" CREATED="1429600531139" MODIFIED="1429601192056"/>
<node TEXT="Stashing Your Work" STYLE_REF="gray_node" ID="ID_659045574" CREATED="1429600538363" MODIFIED="1429601208001">
<node TEXT="$ git status" STYLE_REF="example_node" ID="ID_556379101" CREATED="1429600595027" MODIFIED="1429601181218"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      # On branch master
    </p>
    <p>
      # Changes to be committed:
    </p>
    <p>
      #&#160;&#160;&#160;(use &quot;git reset HEAD &lt;file&gt;...&quot; to unstage)
    </p>
    <p>
      #
    </p>
    <p>
      #&#160;&#160;&#160;&#160;&#160;&#160;modified:&#160;&#160;&#160;index.html
    </p>
    <p>
      #
    </p>
    <p>
      # Changes not staged for commit:
    </p>
    <p>
      #&#160;&#160;&#160;(use &quot;git add &lt;file&gt;...&quot; to update what will be committed)
    </p>
    <p>
      #
    </p>
    <p>
      #&#160;&#160;&#160;&#160;&#160;&#160;modified:&#160;&#160;&#160;lib/simplegit.rb
    </p>
    <p>
      #
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Now you want to switch branches, but you don&#x2019;t want to commit what you&#x2019;ve been working on yet; so you&#x2019;ll stash the changes" STYLE_REF="pre_last_node" ID="ID_1977541239" CREATED="1429600632403" MODIFIED="1429601192058"/>
<node TEXT="$ git stash" STYLE_REF="example_node" ID="ID_1889132251" CREATED="1429600647891" MODIFIED="1429601181218"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Saved working directory and index state \
    </p>
    <p>
      &#160;&#160;&quot;WIP on master: 049d078 added the index file&quot;
    </p>
    <p>
      HEAD is now at 049d078 added the index file
    </p>
    <p>
      (To restore them type &quot;git stash apply&quot;)
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="$ git status" STYLE_REF="example_node" ID="ID_1767123134" CREATED="1429600666107" MODIFIED="1429601181217"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      # On branch master
    </p>
    <p>
      nothing to commit, working directory clean
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Now your working directory is clean" STYLE_REF="pre_last_node" ID="ID_45734980" CREATED="1429600671539" MODIFIED="1429601192059"/>
</node>
<node TEXT="$ git stash list" STYLE_REF="example_node" ID="ID_1935765304" CREATED="1429600721484" MODIFIED="1429601181217"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      stash@{0}: WIP on master: 049d078 added the index file
    </p>
    <p>
      stash@{1}: WIP on master: c264051 Revert &quot;added file_size&quot;
    </p>
    <p>
      stash@{2}: WIP on master: 21d80a5 added number to log
    </p>
  </body>
</html>
</richcontent>
<node TEXT="To see which stashes you&#x2019;ve stored" STYLE_REF="pre_last_node" ID="ID_1912351243" CREATED="1429600738452" MODIFIED="1429601192059"/>
</node>
<node TEXT="$ git stash apply stash@{2}" STYLE_REF="example_node" ID="ID_1853336203" CREATED="1429600818419" MODIFIED="1429601181217">
<node TEXT="If you want to apply one of the older stashes, you can specify it by naming it" STYLE_REF="pre_last_node" ID="ID_161560007" CREATED="1429600836388" MODIFIED="1429601192060"/>
</node>
<node TEXT="$ git stash apply" STYLE_REF="example_node" ID="ID_1530292316" CREATED="1429600809084" MODIFIED="1429601181217"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      # On branch master
    </p>
    <p>
      # Changes not staged for commit:
    </p>
    <p>
      #&#160;&#160;&#160;(use &quot;git add &lt;file&gt;...&quot; to update what will be committed)
    </p>
    <p>
      #
    </p>
    <p>
      #&#160;&#160;&#160;&#160;&#160;&#160;modified:&#160;&#160;&#160;index.html
    </p>
    <p>
      #&#160;&#160;&#160;&#160;&#160;&#160;modified:&#160;&#160;&#160;lib/simplegit.rb
    </p>
  </body>
</html>
</richcontent>
<node TEXT="If you don&#x2019;t specify a stash, Git assumes&#xa;the most recent stash and tries to apply it" STYLE_REF="pre_last_node" ID="ID_930726758" CREATED="1429600851996" MODIFIED="1429601192060"/>
</node>
<node TEXT="$ git stash apply --index" STYLE_REF="example_node" ID="ID_1787724246" CREATED="1429601059314" MODIFIED="1429601181216"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      # On branch master
    </p>
    <p>
      # Changes to be committed:
    </p>
    <p>
      #&#160;&#160;&#160;(use &quot;git reset HEAD &lt;file&gt;...&quot; to unstage)
    </p>
    <p>
      #
    </p>
    <p>
      #&#160;&#160;&#160;&#160;&#160;&#160;modified:&#160;&#160;&#160;index.html
    </p>
    <p>
      #
    </p>
    <p>
      # Changes not staged for commit:
    </p>
    <p>
      #&#160;&#160;&#160;(use &quot;git add &lt;file&gt;...&quot; to update what will be committed)
    </p>
    <p>
      #
    </p>
    <p>
      #&#160;&#160;&#160;&#160;&#160;&#160;modified:&#160;&#160;&#160;lib/simplegit.rb
    </p>
  </body>
</html>
</richcontent>
<node TEXT="To restage files that was staged before" STYLE_REF="pre_last_node" ID="ID_1171369235" CREATED="1429601080306" MODIFIED="1429601192061"/>
</node>
<node TEXT="$ git stash drop stash@{0}" STYLE_REF="example_node" ID="ID_1739934974" CREATED="1429601130090" MODIFIED="1429601537503"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Dropped stash@{0} (364e91f3f268f0900bc3ee613f9f733e82aaed43)
    </p>
  </body>
</html>
</richcontent>
<node TEXT="The apply option only tries to apply the stashed work &#x2014; you&#xa;continue to have it on your stack. To remove it use stash drop" STYLE_REF="pre_last_node" ID="ID_1856580139" CREATED="1429601155138" MODIFIED="1429601537502"/>
</node>
</node>
<node TEXT="Un-applying a Stash" STYLE_REF="gray_node" ID="ID_1591950229" CREATED="1429601223675" MODIFIED="1429601368056">
<node TEXT="$ git stash show -p stash@{0} | git apply -R" STYLE_REF="example_node" ID="ID_204875459" CREATED="1429601265058" MODIFIED="1429601520593">
<node TEXT="If you want to apply stashed changes, do some work, but then&#xa;un-apply those changes that originally came from the stash" STYLE_REF="pre_last_node" ID="ID_628948404" CREATED="1429601344052" MODIFIED="1429601525999"/>
</node>
<node TEXT="$ git stash show -p | git apply -R" STYLE_REF="example_node" ID="ID_594478985" CREATED="1429601273099" MODIFIED="1429601533990">
<node TEXT="Again, if you don&#x2019;t specify a stash, Git assumes the most recent stash" STYLE_REF="pre_last_node" ID="ID_497372184" CREATED="1429601284884" MODIFIED="1429601533990"/>
</node>
</node>
<node TEXT="Creating a Branch from a Stash" STYLE_REF="gray_node" ID="ID_548023871" CREATED="1429601321419" MODIFIED="1429601368056">
<node TEXT="$ git stash branch testchanges" STYLE_REF="example_node" ID="ID_359648592" CREATED="1429601472035" MODIFIED="1429601520592"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Switched to a new branch &quot;testchanges&quot;
    </p>
    <p>
      # On branch testchanges
    </p>
    <p>
      # Changes to be committed:
    </p>
    <p>
      #&#160;&#160;&#160;(use &quot;git reset HEAD &lt;file&gt;...&quot; to unstage)
    </p>
    <p>
      #
    </p>
    <p>
      #&#160;&#160;&#160;&#160;&#160;&#160;modified:&#160;&#160;&#160;index.html
    </p>
    <p>
      #
    </p>
    <p>
      # Changes not staged for commit:
    </p>
    <p>
      #&#160;&#160;&#160;(use &quot;git add &lt;file&gt;...&quot; to update what will be committed)
    </p>
    <p>
      #
    </p>
    <p>
      #&#160;&#160;&#160;&#160;&#160;&#160;modified:&#160;&#160;&#160;lib/simplegit.rb
    </p>
    <p>
      #
    </p>
    <p>
      Dropped refs/stash@{0} (f0dfc4d5dc332d1cee34a634182e168c4efc3359)
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Creates a new branch for you, checks out the commit you were on when you stashed your work, reapplies your work there, and then drops the stash if it applies successfully" STYLE_REF="pre_last_node" ID="ID_1262867016" CREATED="1429601510058" MODIFIED="1429601526001"/>
</node>
</node>
</node>
<node TEXT="Debugging with Git" STYLE_REF="orange_node" ID="ID_1282736994" CREATED="1429857664601" MODIFIED="1429857860643">
<node TEXT="File Annotation" STYLE_REF="gray_node" ID="ID_217934049" CREATED="1429857672208" MODIFIED="1429857865964">
<node TEXT="$ git blame -L 12,22 simplegit.rb" STYLE_REF="example_node" ID="ID_1531561366" CREATED="1429857706080" MODIFIED="1429857870671"><richcontent TYPE="DETAILS" HIDDEN="true">

<html>
  <head>
    
  </head>
  <body>
    <p>
      ^4832fe2 (Scott Chacon&#160;&#160;2008-03-15 10:31:28 -0700 12)&#160;&#160;def show(tree = 'master')
    </p>
    <p>
      ^4832fe2 (Scott Chacon&#160;&#160;2008-03-15 10:31:28 -0700 13)&#160;&#160;&#160;command(&quot;git show #{tree}&quot;)
    </p>
    <p>
      ^4832fe2 (Scott Chacon&#160;&#160;2008-03-15 10:31:28 -0700 14)&#160;&#160;end
    </p>
    <p>
      ^4832fe2 (Scott Chacon&#160;&#160;2008-03-15 10:31:28 -0700 15)
    </p>
    <p>
      9f6560e4 (Scott Chacon&#160;&#160;2008-03-17 21:52:20 -0700 16)&#160;&#160;def log(tree = 'master')
    </p>
    <p>
      79eaf55d (Scott Chacon&#160;&#160;2008-04-06 10:15:08 -0700 17)&#160;&#160;&#160;command(&quot;git log #{tree}&quot;)
    </p>
    <p>
      9f6560e4 (Scott Chacon&#160;&#160;2008-03-17 21:52:20 -0700 18)&#160;&#160;end
    </p>
    <p>
      9f6560e4 (Scott Chacon&#160;&#160;2008-03-17 21:52:20 -0700 19)
    </p>
    <p>
      42cf2861 (Magnus Chacon 2008-04-13 10:45:01 -0700 20)&#160;&#160;def blame(path)
    </p>
    <p>
      42cf2861 (Magnus Chacon 2008-04-13 10:45:01 -0700 21)&#160;&#160;&#160;command(&quot;git blame #{path}&quot;)
    </p>
    <p>
      42cf2861 (Magnus Chacon 2008-04-13 10:45:01 -0700 22)&#160;&#160;end
    </p>
  </body>
</html>
</richcontent>
<node TEXT="if you see that a method in your code is buggy, you can annotate the file with git blame to see when each line of the method was last edited and by whom" STYLE_REF="pre_last_node" ID="ID_1344404926" CREATED="1429857696960" MODIFIED="1429857877779"/>
</node>
<node TEXT="$ git blame -C -L 141,153 GITPackUpload.m" STYLE_REF="example_node" ID="ID_1655804819" CREATED="1429857779872" MODIFIED="1429857870671"><richcontent TYPE="DETAILS" HIDDEN="true">

<html>
  <head>
    
  </head>
  <body>
    <p>
      f344f58d GITServerHandler.m (Scott 2009-01-04 141)
    </p>
    <p>
      f344f58d GITServerHandler.m (Scott 2009-01-04 142) - (void) gatherObjectShasFromC
    </p>
    <p>
      f344f58d GITServerHandler.m (Scott 2009-01-04 143) {
    </p>
    <p>
      70befddd GITServerHandler.m (Scott 2009-03-22 144)&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160; //NSLog(@&quot;GATHER COMMI
    </p>
    <p>
      ad11ac80 GITPackUpload.m&#160;&#160;&#160;&#160;(Scott 2009-03-24 145)
    </p>
    <p>
      ad11ac80 GITPackUpload.m&#160;&#160;&#160;&#160;(Scott 2009-03-24 146)&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;NSString *parentSha;
    </p>
    <p>
      ad11ac80 GITPackUpload.m&#160;&#160;&#160;&#160;(Scott 2009-03-24 147)&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;GITCommit *commit = [g
    </p>
    <p>
      ad11ac80 GITPackUpload.m&#160;&#160;&#160;&#160;(Scott 2009-03-24 148)
    </p>
    <p>
      ad11ac80 GITPackUpload.m&#160;&#160;&#160;&#160;(Scott 2009-03-24 149)&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160; //NSLog(@&quot;GATHER COMMI
    </p>
    <p>
      ad11ac80 GITPackUpload.m&#160;&#160;&#160;&#160;(Scott 2009-03-24 150)
    </p>
    <p>
      56ef2caf GITServerHandler.m (Scott 2009-01-05 151)&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;if(commit) {
    </p>
    <p>
      56ef2caf GITServerHandler.m (Scott 2009-01-05 152)&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160; [refDict setOb
    </p>
    <p>
      56ef2caf GITServerHandler.m (Scott 2009-01-05 153)
    </p>
  </body>
</html>
</richcontent>
<node TEXT="By blaming GITPackUpload.m with the -C option, I could see where sections of the code originally came from" STYLE_REF="pre_last_node" ID="ID_407564521" CREATED="1429857773976" MODIFIED="1429857877779"/>
</node>
</node>
<node TEXT="Binary Search" STYLE_REF="gray_node" ID="ID_586412342" CREATED="1429857838278" MODIFIED="1429857865964">
<node TEXT="The bisect command does a binary search through your commit history to help you identify as quickly as possible which commit introduced an issue." STYLE_REF="pre_last_node" ID="ID_1892055522" CREATED="1429857844431" MODIFIED="1429857877779"/>
<node TEXT="Let&#x2019;s say you just pushed out a release of your code to a production environment, you&#x2019;re getting bug reports about something that wasn&#x2019;t happening in your development environment, and you can&#x2019;t imagine why the code is doing that. You go back to your code, and it turns out you can reproduce the issue, but you can&#x2019;t figure out what is going wrong. You can bisect the code to find out. First you run git bisect start to get things going, and then you use git bisect bad to tell the system that the current commit you&#x2019;re on is broken. Then, you must tell bisect when the last known good state was, using git bisect good [good_commit]" STYLE_REF="pre_last_node" ID="ID_864249784" CREATED="1429858157936" MODIFIED="1429858368159">
<node TEXT="$ git bisect start&#xa;$ git bisect bad&#xa;$ git bisect good v1.0" STYLE_REF="example_node" ID="ID_754734077" CREATED="1429858165969" MODIFIED="1429858375249"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Bisecting: 6 revisions left to test after this
    </p>
    <p>
      [ecb6e1bc347ccecc5f9350d878ce677feb13d3b2] error handling on repo
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Git figured out that about 12 commits came between the commit you marked as the last good commit (v1.0) and the current bad version, and it checked out the middle one for you. At this point, you can run your test to see if the issue exists as of this commit. If it does, then it was introduced sometime before this middle commit; if it doesn&#x2019;t, then the problem was introduced sometime after the middle commit. It turns out there is no issue here, and you tell Git that by typing git bisect good and continue your journey" STYLE_REF="pre_last_node" ID="ID_658401461" CREATED="1429858233112" MODIFIED="1429858368159">
<node TEXT="$ git bisect good" STYLE_REF="example_node" ID="ID_1191771361" CREATED="1429858238648" MODIFIED="1429858375249"/>
</node>
<node TEXT="Now you&#x2019;re on another commit, halfway between the one you just tested and your bad commit. You run your test again and find that this commit is broken, so you tell Git that with git bisect bad:" STYLE_REF="pre_last_node" ID="ID_45320544" CREATED="1429858253634" MODIFIED="1429858368159">
<node TEXT="$ git bisect bad" STYLE_REF="example_node" ID="ID_160133508" CREATED="1429858259184" MODIFIED="1429858375249"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Bisecting: 1 revisions left to test after this
    </p>
    <p>
      [f71ce38690acf49c1f3c9bea38e09d82a5ce6014] drop exceptions table
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="When you&#x2019;re finished, you should run git bisect reset to reset your HEAD to where you were before you started, or you&#x2019;ll end up in a weird state:" STYLE_REF="pre_last_node" ID="ID_1793632331" CREATED="1429858298263" MODIFIED="1429858368159">
<node TEXT="$ git bisect reset" STYLE_REF="example_node" ID="ID_269771850" CREATED="1429858303272" MODIFIED="1429858375249"/>
</node>
</node>
</node>
<node TEXT="Working with Git objects" STYLE_REF="orange_node" ID="ID_1746769666" CREATED="1430103235786" MODIFIED="1430103659655">
<node TEXT="git cat-file -t &lt;hash&gt;" STYLE_REF="pre_last_node" ID="ID_850269688" CREATED="1430103242787" MODIFIED="1430103645815">
<node TEXT="$ git cat-file -t 8029c07" STYLE_REF="example_node" ID="ID_864227697" CREATED="1430103271074" MODIFIED="1430106852087"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      commit
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="To view the type of object" STYLE_REF="pre_last_node" ID="ID_1685934963" CREATED="1430103292257" MODIFIED="1430106858654"/>
</node>
<node TEXT="git cat-file -p &lt;hash&gt;" STYLE_REF="pre_last_node" ID="ID_691570067" CREATED="1430103264474" MODIFIED="1430103645816">
<node TEXT="$ git cat-file -p 8029c07" STYLE_REF="example_node" ID="ID_1423847272" CREATED="1430103281722" MODIFIED="1430106852087"><richcontent TYPE="DETAILS" HIDDEN="true">

<html>
  <head>
    
  </head>
  <body>
    <p>
      tree 096b74c56bfc6b40e754fc0725b8c70b2038b91e
    </p>
    <p>
      parent 567948ac55daa723807c0c16e34c76797efbcbed
    </p>
    <p>
      author Marina Pushkova &lt;marina (at) githowto.com&gt; 1299684476 -0500
    </p>
    <p>
      committer Marina Pushkova &lt;marina (at) githowto.com&gt; 1299684476 -0500
    </p>
    <p>
      
    </p>
    <p>
      Added index.html.
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="To view the dump of object" STYLE_REF="pre_last_node" ID="ID_1642616639" CREATED="1430103302649" MODIFIED="1430106858654"/>
</node>
<node TEXT="git cat-file -p &lt;treehash&gt;" STYLE_REF="pre_last_node" ID="ID_1690663555" CREATED="1430103503578" MODIFIED="1430103645816">
<node TEXT="$ git cat-file -p 096b74c" STYLE_REF="example_node" ID="ID_1593796892" CREATED="1430103525003" MODIFIED="1430106852088"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      100644 blob 28e0e9d6ea7e25f35ec64a43f569b550e8386f90 index.html
    </p>
    <p>
      040000 tree e46f374f5b36c6f02fb3e9e922b79044f754d795 lib
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="To view the tree" STYLE_REF="pre_last_node" ID="ID_1031855519" CREATED="1430103515273" MODIFIED="1430106858654"/>
</node>
<node TEXT="git cat-file -p &lt;libhash&gt;" STYLE_REF="pre_last_node" ID="ID_1573744396" CREATED="1430103558754" MODIFIED="1430103645816">
<node TEXT="$ git cat-file -p e46f374" STYLE_REF="example_node" ID="ID_388518369" CREATED="1430103565018" MODIFIED="1430106852088"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      100644 blob c45f26b6fdc7db6ba779fc4c385d9d24fc12cf72 hello.html
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="git cat-file -p &lt;filehash&gt;" STYLE_REF="pre_last_node" ID="ID_478023583" CREATED="1430103604507" MODIFIED="1430103645816">
<node TEXT="$ git cat-file -p c45f26b" STYLE_REF="example_node" ID="ID_1757513145" CREATED="1430103610122" MODIFIED="1430106852088"><richcontent TYPE="DETAILS" HIDDEN="true">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &lt;!-- Author: Marina Pushkova (marina@githowto.com) --&gt;
    </p>
    <p>
      &lt;html&gt;
    </p>
    <p>
      &#160;&#160;&lt;head&gt;
    </p>
    <p>
      &#160;&#160;&lt;/head&gt;
    </p>
    <p>
      &#160;&#160;&lt;body&gt;
    </p>
    <p>
      &#160;&#160;&#160;&#160;&lt;h1&gt;Hello, World!&lt;/h1&gt;
    </p>
    <p>
      &#160;&#160;&lt;/body&gt;
    </p>
    <p>
      &lt;/html&gt;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="To view certain file" STYLE_REF="pre_last_node" ID="ID_821972233" CREATED="1430103625228" MODIFIED="1430106858654"/>
</node>
</node>
<node TEXT="cherry-pick" STYLE_REF="orange_node" ID="ID_1798136153" CREATED="1431337697361" MODIFIED="1431337744655">
<node TEXT="git cherry-pick d42c389f" STYLE_REF="example_node" ID="ID_1209938478" CREATED="1431337704005" MODIFIED="1431337749868">
<node TEXT="To merge certain commit into current branch" STYLE_REF="pre_last_node" ID="ID_531740560" CREATED="1431337706197" MODIFIED="1431337755613"/>
</node>
</node>
</node>
</node>
</map>
