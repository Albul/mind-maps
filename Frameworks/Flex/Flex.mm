<map version="freeplane 1.3.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Flex" STYLE_REF="main_node" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1427276115413" VGAP="20">
<hook NAME="AutomaticEdgeColor" COUNTER="5"/>
<hook NAME="MapStyle" zoom="0.829">
    <properties show_icon_for_attributes="true" show_note_icons="true"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
<stylenode TEXT="main_node" BACKGROUND_COLOR="#ccccff">
<font SIZE="18" BOLD="true"/>
<edge STYLE="sharp_bezier" WIDTH="4"/>
</stylenode>
<stylenode TEXT="second_node" BACKGROUND_COLOR="#00cccc" STYLE="bubble">
<font SIZE="14"/>
</stylenode>
<stylenode TEXT="third_node" BACKGROUND_COLOR="#ffbdf4" STYLE="bubble">
<font SIZE="11" BOLD="true"/>
</stylenode>
<stylenode TEXT="orange_node" BACKGROUND_COLOR="#ff9900" STYLE="bubble">
<font BOLD="true"/>
</stylenode>
<stylenode TEXT="gray_node" BACKGROUND_COLOR="#9ec19e">
<font BOLD="true"/>
</stylenode>
<stylenode TEXT="yellow_node" BACKGROUND_COLOR="#ffff66" STYLE="bubble">
<edge WIDTH="1"/>
</stylenode>
<stylenode TEXT="example_node" BACKGROUND_COLOR="#afcdfa">
<edge STYLE="bezier" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="pre_last_node" BACKGROUND_COLOR="#99ff99" STYLE="bubble">
<edge STYLE="bezier" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="last_node" BACKGROUND_COLOR="#ffffff">
<edge STYLE="bezier" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="warning_node" BACKGROUND_COLOR="#fda6a6">
<icon BUILTIN="messagebox_warning"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook URI="../../Images/flex-large.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Life Cycle" STYLE_REF="second_node" POSITION="right" ID="ID_799587188" CREATED="1426235632976" MODIFIED="1426246850943" HGAP="40" VSHIFT="10">
<edge COLOR="#891010"/>
<hook URI="../../Images/life-cycle-2.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Application" STYLE_REF="orange_node" ID="ID_932315528" CREATED="1426246841983" MODIFIED="1427383151701" HGAP="30" VSHIFT="110">
<hook URI="flex-life-cycle.jpg" SIZE="0.76045626" NAME="ExternalObject"/>
<node TEXT="preinitialize" STYLE_REF="gray_node" ID="ID_533787553" CREATED="1426246854126" MODIFIED="1426352924807">
<node TEXT="mx.events.FlexEvent.PREINITIALIZE" STYLE_REF="pre_last_node" ID="ID_170063681" CREATED="1426352535723" MODIFIED="1426352631781">
<font BOLD="true"/>
</node>
<node TEXT="This event is dispatched at the beginning of the component initialization sequence. The component is in a very raw state when this event is dispatched" STYLE_REF="pre_last_node" ID="ID_97068846" CREATED="1426352568888" MODIFIED="1426352629634"/>
<node TEXT="Many components, such as Button control, creates internal child components to implement functionality. For example, the Button control creates an internal UITextField component to represent its label text. When Flex dispatches the preinitialize event, the children, including all the internal children, of a&#xa;component have not yet been created" STYLE_REF="pre_last_node" ID="ID_1132791251" CREATED="1426352584019" MODIFIED="1426352629636"/>
</node>
<node TEXT="initialize" STYLE_REF="gray_node" ID="ID_283622949" CREATED="1426352687093" MODIFIED="1426352924807">
<node TEXT="mx.events.FlexEvent.INITIALIZE" STYLE_REF="pre_last_node" ID="ID_307097608" CREATED="1426352698611" MODIFIED="1426352936285">
<font BOLD="true"/>
</node>
<node TEXT="This event is dispatched after preinitialize phase. Flex framework initializes the internal structure of this component during this phase. This event automatically fires when the component is added to a parent" STYLE_REF="pre_last_node" ID="ID_667173620" CREATED="1426352741897" MODIFIED="1426352931616"/>
</node>
<node TEXT="creationComplete" STYLE_REF="gray_node" ID="ID_820483439" CREATED="1426352755438" MODIFIED="1426352924807">
<node TEXT="mx.events.FlexEvent.CREATION_COMPLETE" STYLE_REF="pre_last_node" ID="ID_802553991" CREATED="1426352764748" MODIFIED="1426352936286">
<font BOLD="true"/>
</node>
<node TEXT="This event is dispatched when the component has finished its&#xa;construction, property processing, measuring, layout, and drawing" STYLE_REF="pre_last_node" ID="ID_1140713322" CREATED="1426352786459" MODIFIED="1426352931618"/>
<node TEXT="At this point, depending on its visible property, the&#xa;component is not visible even though it has been drawn" STYLE_REF="pre_last_node" ID="ID_1737054618" CREATED="1426352802511" MODIFIED="1426352931620"/>
</node>
<node TEXT="applicationComplete" STYLE_REF="gray_node" ID="ID_1711718587" CREATED="1426352845597" MODIFIED="1426352924806">
<node TEXT="mx.events.FlexEvent.APPLICATION_COMPLETE" STYLE_REF="pre_last_node" ID="ID_1653275087" CREATED="1426352854611" MODIFIED="1426352936287">
<font BOLD="true"/>
</node>
<node TEXT="Dispatched after the Application has been initialized,&#xa;processed by the LayoutManager, and attached to the display list" STYLE_REF="pre_last_node" ID="ID_694535158" CREATED="1426352869230" MODIFIED="1426352931624"/>
<node TEXT="This is the last event of the application creation life cycle&#xa;and signifies that application has been loaded completely" STYLE_REF="pre_last_node" ID="ID_1080841462" CREATED="1426352905741" MODIFIED="1426352931627"/>
</node>
</node>
<node TEXT="UIComponent" STYLE_REF="orange_node" ID="ID_1619702593" CREATED="1427383050944" MODIFIED="1427383157182">
<node TEXT="initialize" STYLE_REF="gray_node" ID="ID_608085384" CREATED="1427383059440" MODIFIED="1427383265334">
<node TEXT="Is dispatched when the component has finished its construction but before all of the component&#x2019;s immediate children have been laid out" STYLE_REF="pre_last_node" ID="ID_426246909" CREATED="1427383072849" MODIFIED="1427383273853"/>
</node>
<node TEXT="creationComplete" STYLE_REF="gray_node" ID="ID_834008199" CREATED="1427383055782" MODIFIED="1427383265334">
<node TEXT="Is dispatched and indicates that the component&#xa;has been created, laid out, and is visible" STYLE_REF="pre_last_node" ID="ID_1156031165" CREATED="1427383182266" MODIFIED="1427383273851"/>
</node>
<node TEXT="show" STYLE_REF="gray_node" ID="ID_683328265" CREATED="1427383241881" MODIFIED="1427383265333">
<node TEXT="Dispatches every time the component goes from invisible to visible" STYLE_REF="pre_last_node" ID="ID_1230673015" CREATED="1427383251888" MODIFIED="1427383273855"/>
</node>
</node>
</node>
<node TEXT="Skin" STYLE_REF="second_node" POSITION="left" ID="ID_253718579" CREATED="1426487125477" MODIFIED="1436778817006" HGAP="29" VSHIFT="9">
<edge STYLE="sharp_bezier" COLOR="#0000ff"/>
<hook URI="../../Images/skin.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Skinning in Flex, is a process of customizing&#xa;look and feel of a UI Component completely" STYLE_REF="pre_last_node" ID="ID_1193365514" CREATED="1426487198323" MODIFIED="1427209796755">
<icon BUILTIN="idea"/>
</node>
<node TEXT="Features" STYLE_REF="orange_node" ID="ID_1058892671" CREATED="1426487239594" MODIFIED="1436778817006">
<node TEXT="A Skin can define text, image, filters , transitions and states of a compoent" STYLE_REF="pre_last_node" ID="ID_1270369668" CREATED="1426487245697" MODIFIED="1426488599552"/>
<node TEXT="A Skin can be created as a seperate mxml or ActionScript component" STYLE_REF="pre_last_node" ID="ID_1836891493" CREATED="1426487245703" MODIFIED="1428335143253"/>
<node TEXT="Using skin, we can control all visual aspects of a UI component" STYLE_REF="pre_last_node" ID="ID_1396231725" CREATED="1426487245704" MODIFIED="1428335143827"/>
<node TEXT="The process of defining skin is same for all the UI component" STYLE_REF="pre_last_node" ID="ID_1934408394" CREATED="1426487245796" MODIFIED="1426488599549"/>
</node>
<node TEXT="To create" STYLE_REF="yellow_node" ID="ID_861306950" CREATED="1426488429271" MODIFIED="1426488625829">
<node TEXT="&lt;?xml version=&quot;1.0&quot; encoding=&quot;utf-8&quot;?&gt;&#xa;&lt;s:Skin xmlns:fx=&quot;http://ns.adobe.com/mxml/2009&quot;&#xa;xmlns:s=&quot;library://ns.adobe.com/flex/spark&quot;&#xa;xmlns:mx=&quot;library://ns.adobe.com/flex/mx&quot;&gt;&#xa;&lt;!-- host component --&gt;&#xa;&lt;fx:Metadata&gt;&#xa;    [HostComponent(&quot;spark.components.BorderContainer&quot;)]&#xa;    //[HostComponent(&quot;components.EmployeePortalPanel&quot;)]&#xa;    // hostComponent.propertyofEmployeePortalPanel can access&#xa;&lt;/fx:Metadata&gt;&#xa;&lt;!-- states--&gt;&#xa;&lt;s:states&gt;&#xa;    &lt;s:State name=&quot;disabled&quot; /&gt;&#xa;    &lt;s:State name=&quot;disabled&quot; /&gt;&#xa;    &lt;s:State name=&quot;normal&quot; /&gt;&#xa;&lt;/s:states&gt;&#xa;&lt;!-- fill --&gt;&#xa;&lt;s:Rect id=&quot;backgroundRect&quot; left=&quot;0&quot; right=&quot;0&quot; height=&quot;100%&quot; top=&quot;0&quot;&gt;&#xa;    &lt;s:fill&gt;&#xa;        &lt;s:LinearGradient rotation=&quot;90&quot;&gt;&#xa;            &lt;s:GradientEntry color=&quot;0x111111&quot; ratio=&quot;0.2&quot;/&gt;&#xa;            &lt;s:GradientEntry color=&quot;0x888888&quot; ratio=&quot;1&quot;/&gt;&#xa;        &lt;/s:LinearGradient&gt;&#xa;    &lt;/s:fill&gt;&#xa;&lt;/s:Rect&gt;&#xa;&lt;!-- must specify this for the host component --&gt;&#xa;&lt;s:Group id=&quot;contentGroup&quot; left=&quot;0&quot; right=&quot;0&quot; top=&quot;0&quot; bottom=&quot;0&quot; /&gt;&#xa;&lt;/s:Skin&gt;" STYLE_REF="example_node" ID="ID_1234670161" CREATED="1426488434015" MODIFIED="1428335715884"/>
<node TEXT="&lt;?xml version=&quot;1.0&quot; encoding=&quot;utf-8&quot;?&gt;&#xa;&lt;s:SparkSkin xmlns:fx=&quot;http://ns.adobe.com/mxml/2009&quot;&#xa;    xmlns:s=&quot;library://ns.adobe.com/flex/spark&quot;&gt;&#xa;    &lt;s:states&gt;&#xa;        &lt;s:State name=&quot;normal&quot; /&gt;&#xa;        &lt;s:State name=&quot;disabled&quot; /&gt;&#xa;    &lt;/s:states&gt;&#xa;     &lt;fx:Metadata&gt;&#xa;        [HostComponent(&quot;spark.components.Application&quot;)]&#xa;    &lt;/fx:Metadata&gt;&#xa;&#xa;    &lt;s:Rect horizontalCenter=&quot;0&quot;&#xa;         width=&quot;750&quot; height=&quot;100%&quot;&gt;&#xa;        &lt;s:fill&gt;&#xa;            &lt;s:SolidColor color=&quot;#CCCCCC&quot; /&gt;&#xa;        &lt;/s:fill&gt;&#xa;        &lt;s:stroke&gt;&#xa;            &lt;s:SolidColorStroke color=&quot;#AAAAAA&quot; /&gt;&#xa;        &lt;/s:stroke&gt;&#xa;    &lt;/s:Rect&gt;  &#xa;    &lt;s:Group id=&quot;contentGroup&quot;&#xa;         top=&quot;20&quot;&#xa;         horizontalCenter=&quot;0&quot;&gt;&#xa;       &#xa;    &lt;/s:Group&gt;&#xa;&lt;/s:SparkSkin&gt;" STYLE_REF="example_node" ID="ID_1417997751" CREATED="1427376423815" MODIFIED="1427376470451"/>
</node>
<node TEXT="Apply skin" STYLE_REF="orange_node" ID="ID_1915603962" CREATED="1426487289216" MODIFIED="1426516733342">
<node TEXT="in MXML (statically)" STYLE_REF="pre_last_node" ID="ID_797537950" CREATED="1426487305732" MODIFIED="1428334864787">
<node TEXT="&lt;s:BorderContainer width=&quot;560&quot; height=&quot;500&quot; id=&quot;mainContainer&quot;&#xa;styleName=&quot;container&quot;&gt;&#xa;&lt;s:VGroup width=&quot;100%&quot; height=&quot;100%&quot; gap=&quot;50&quot;&#xa;horizontalAlign=&quot;center&quot; verticalAlign=&quot;middle&quot;&#xa;skinClass=&quot;skins.GradientBackgroundSkin&quot;&gt;" STYLE_REF="example_node" ID="ID_284907575" CREATED="1426487326181" MODIFIED="1428334672606"/>
</node>
<node TEXT="in CSS (statically)" STYLE_REF="pre_last_node" ID="ID_1663529111" CREATED="1428334590729" MODIFIED="1428334869219">
<node TEXT="s|BorderContainer {&#xa;    skinClass: ClassReference(&quot;skins.skins.GradientBackgroundSkin&quot;);&#xa;}" STYLE_REF="example_node" ID="ID_1840422763" CREATED="1428334600334" MODIFIED="1428334704712"/>
</node>
<node TEXT="in ActionScript (dynamically)" STYLE_REF="pre_last_node" ID="ID_1936803052" CREATED="1426487314142" MODIFIED="1426488633739">
<node TEXT="protected function gradientBackground_clickHandler(event:MouseEvent):void {&#xa;    mainContainer.setStyle(&quot;skinClass&quot;, GradientBackgroundSkin );&#xa;}" STYLE_REF="example_node" ID="ID_276499697" CREATED="1426487336546" MODIFIED="1426488641627"/>
</node>
</node>
<node TEXT="Skin parts" STYLE_REF="orange_node" ID="ID_304835574" CREATED="1428335827725" MODIFIED="1428335862474">
<node TEXT="For Panel" STYLE_REF="yellow_node" ID="ID_1901903619" CREATED="1428335832399" MODIFIED="1428335869776">
<node TEXT="contentGroup" STYLE_REF="pre_last_node" ID="ID_141142718" CREATED="1428335837472" MODIFIED="1428335876191"/>
<node TEXT="controlBarGroup" STYLE_REF="pre_last_node" ID="ID_258420667" CREATED="1428335842567" MODIFIED="1428335876192"/>
<node TEXT="titleDisplay" STYLE_REF="pre_last_node" ID="ID_484615596" CREATED="1428335848325" MODIFIED="1428335876192"/>
</node>
</node>
<node TEXT="Skin states" STYLE_REF="orange_node" ID="ID_1531379243" CREATED="1428336226688" MODIFIED="1428336237743">
<node TEXT="&lt;?xml version=&quot;1.0&quot; encoding=&quot;utf-8&quot;?&gt;&#xa;&lt;s:SparkSkin xmlns:fx=&quot;http://ns.adobe.com/mxml/2009&quot;&#xa;    xmlns:s=&quot;library://ns.adobe.com/flex/spark&quot;&#xa;    xmlns:mx=&quot;library://ns.adobe.com/flex/mx&quot;&#xa;    minWidth=&quot;65&quot; minHeight=&quot;22&quot;&gt;&#xa;    &lt;s:states&gt;&#xa;        &lt;s:State name=&quot;up&quot;/&gt;&#xa;        &lt;s:State name=&quot;over&quot;/&gt;&#xa;        &lt;s:State name=&quot;down&quot;/&gt;&#xa;        &lt;s:State name=&quot;disabled&quot;/&gt;&#xa;    &lt;/s:states&gt;&#xa;&#xa;    &lt;s:transitions&gt;&#xa;        &lt;s:Transition fromState=&quot;up&quot; toState=&quot;over&quot;&#xa;                             autoReverse=&quot;true&quot;&gt;&#xa;            &lt;s:Resize target=&quot;{buttonColor}&quot;&#xa;                            widthBy=&quot;25&quot;/&gt;&#xa;        &lt;/s:Transition&gt;&#xa;    &lt;/s:transitions&gt;&#xa;    &lt;fx:Metadata&gt;&#xa;        [HostComponent(&quot;spark.components.Button&quot;)]&#xa;    &lt;/fx:Metadata&gt;&#xa;    &lt;s:Rect id=&quot;buttonColor&quot;&#xa;                top=&quot;0&quot; bottom=&quot;0&quot; left=&quot;0&quot; right=&quot;0&quot;&#xa;                topRightRadiusX=&quot;3&quot;&#xa;                width.over=&quot;90&quot; width.down=&quot;90&quot;&gt;&#xa;        &lt;s:fill&gt;&#xa;            &lt;s:SolidColor color.up=&quot;0x0D86B8&quot; color.over=&quot;0x64BC48&quot;&#xa;                                color.down=&quot;0x555555&quot;/&gt;&#xa;        &lt;/s:fill&gt;&#xa;    &lt;/s:Rect&gt;&#xa;&#xa;    &lt;s:Label id=&quot;labelDisplay&quot;&#xa;        text=&quot;Send&quot;&#xa;        textAlign=&quot;center&quot;&#xa;        verticalAlign=&quot;middle&quot;&#xa;        color=&quot;#FFFFFF&quot;&#xa;        horizontalCenter=&quot;0&quot; verticalCenter=&quot;1&quot;&#xa;        left=&quot;10&quot; right=&quot;10&quot;  top=&quot;2&quot; bottom=&quot;2&quot;&gt;&#xa;    &lt;/s:Label&gt;&#xa;&lt;/s:SparkSkin&gt;" STYLE_REF="example_node" ID="ID_206286619" CREATED="1428336413263" MODIFIED="1428336947118"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      skins.ButtonWithStatesSkin
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="Data Binding" STYLE_REF="second_node" POSITION="right" ID="ID_1096300757" CREATED="1426509283027" MODIFIED="1427276169314">
<edge COLOR="#01a001"/>
<hook URI="../../Images/data-binding.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Data Binding is a process in which data of one object is tied to another object" STYLE_REF="pre_last_node" ID="ID_488544434" CREATED="1426509322489" MODIFIED="1426509333721">
<icon BUILTIN="idea"/>
</node>
<node TEXT="It requires" STYLE_REF="orange_node" ID="ID_1710079176" CREATED="1426509335056" MODIFIED="1436778594336">
<node TEXT="source property" STYLE_REF="pre_last_node" ID="ID_285823542" CREATED="1426509354066" MODIFIED="1426516640304"/>
<node TEXT="destination property" STYLE_REF="pre_last_node" ID="ID_976896717" CREATED="1426509360574" MODIFIED="1426516640302"/>
<node TEXT="triggering event" STYLE_REF="pre_last_node" ID="ID_896002604" CREATED="1426509370809" MODIFIED="1436778594336"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      indicates when to copy the data from source to
    </p>
    <p>
      destination
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Ways for Data Binding" STYLE_REF="orange_node" ID="ID_500952771" CREATED="1426516530638" MODIFIED="1431093481842">
<node TEXT="Curly brace syntax in MXML Script ({})" STYLE_REF="pre_last_node" ID="ID_160023839" CREATED="1426516539547" MODIFIED="1427276195820">
<node TEXT="&lt;s:TextInput id=&quot;txtInput1&quot;/&gt;&#xa;&lt;s:TextInput id=&quot;txtInput2&quot; text = &quot;{txtInput1.text}&quot;/&gt;" STYLE_REF="example_node" ID="ID_1475926836" CREATED="1426516565512" MODIFIED="1427276195820"/>
<node TEXT="&lt;s:TextInput id=&quot;firstName&quot;/&gt;&#xa;&lt;s:TextInput id=&quot;lastName&quot;/&gt;&#xa;&lt;s:TextInput id=&quot;emailInput&quot; text=&quot;{firstName}.{lastName}@gmail.com&quot;" STYLE_REF="example_node" ID="ID_799129543" CREATED="1427360280922" MODIFIED="1427360497832"/>
</node>
<node TEXT="&lt;fx:binding&gt; tag in MXML" STYLE_REF="pre_last_node" ID="ID_1451411077" CREATED="1426516575550" MODIFIED="1431093481841">
<node TEXT="&lt;fx:Binding source=&quot;txtInput1.text&quot; destination=&quot;txtInput2.text&quot; /&gt;&#xa;&lt;s:TextInput id=&quot;txtInput1&quot;/&gt;&#xa;&lt;s:TextInput id=&quot;txtInput2&quot;/&gt;" STYLE_REF="example_node" ID="ID_701299842" CREATED="1426516584641" MODIFIED="1426516634973"/>
</node>
<node TEXT="BindingUtils in ActionScript" STYLE_REF="pre_last_node" ID="ID_1541818233" CREATED="1426516594656" MODIFIED="1426516629818">
<node TEXT="&lt;fx:Script&gt;&#xa;&lt;![CDATA[&#xa;import mx.binding.utils.BindingUtils;&#xa;import mx.events.FlexEvent;&#xa;protected function txtInput2_preinitializeHandler(event:FlexEvent):void {&#xa;    BindingUtils.bindProperty(txtInput2,&quot;text&quot;,txtInput1, &quot;text&quot;);&#xa;}&#xa;]]&gt;&#xa;&lt;/fx:Script&gt;&#xa;&lt;s:TextInput id=&quot;txtInput1&quot;/&gt;&#xa;&lt;s:TextInput id=&quot;txtInput2&quot;&#xa;preinitialize=&quot;txtInput2_preinitializeHandler(event)&quot;/&gt;" STYLE_REF="example_node" ID="ID_310564693" CREATED="1426516604615" MODIFIED="1426516634977"/>
</node>
</node>
<node TEXT="Arrays with Data Binding" STYLE_REF="orange_node" ID="ID_1993455547" CREATED="1427387683851" MODIFIED="1427388095629">
<node TEXT="If you bind Array or Object instances to a UI control and then change one of the values in the instance, the UI control does not update the value in the display. The data binding is only triggered when the application starts or when the instance itself is updated" STYLE_REF="pre_last_node" ID="ID_1454905066" CREATED="1427387973921" MODIFIED="1427388108909"/>
<node TEXT="ArrayCollection" STYLE_REF="yellow_node" ID="ID_1738135016" CREATED="1427387707465" MODIFIED="1431093484406">
<hook NAME="FirstGroupNode"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Introduced in Flex 3
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true"/>
<node TEXT="It is a wrapper class for the Array class" STYLE_REF="pre_last_node" ID="ID_1113051125" CREATED="1427387931401" MODIFIED="1427388118132"/>
<node TEXT="It ss the recommended class for using as the data provider" STYLE_REF="pre_last_node" ID="ID_1964369115" CREATED="1427388036634" MODIFIED="1431093484405"/>
<node TEXT="It is constantly  monitoring all of its individual elements" STYLE_REF="pre_last_node" ID="ID_854344703" CREATED="1427388068073" MODIFIED="1427388118133"/>
</node>
<node TEXT="ArrayList" STYLE_REF="yellow_node" ID="ID_97029049" CREATED="1427387713411" MODIFIED="1427388103420"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Introduced in Flex 4
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true"/>
<node TEXT="ArrayList class has less overhead because it does&#xa;not contain all the sorting or filtering functionality" STYLE_REF="pre_last_node" ID="ID_965640452" CREATED="1427387798857" MODIFIED="1427388118133"/>
</node>
<node TEXT="Both the classes can be used in data bindings" ID="ID_515490535" CREATED="1427387734468" MODIFIED="1427387761467">
<hook NAME="SummaryNode"/>
</node>
<node TEXT="Declaration" STYLE_REF="yellow_node" ID="ID_339376638" CREATED="1428049998359" MODIFIED="1428050125446">
<node TEXT="&lt;fx:Declarations&gt;&#xa;    &lt;s:ArrayList id=&quot;employeeList&quot;&gt;&#xa;        &lt;fx:String&gt;Albul Oleksandr&lt;/fx:String&gt;&#xa;        &lt;fx:String&gt;Albul Nadia&lt;/fx:String&gt;&#xa;        &lt;fx:String&gt;Igor Kuzmenko&lt;/fx:String&gt;&#xa;    &lt;/s:ArrayList&gt;&#xa;&lt;/fx:Declarations&gt;" STYLE_REF="example_node" ID="ID_108338249" CREATED="1428050007526" MODIFIED="1428050129094"/>
</node>
</node>
<node TEXT="Two-way Binding" STYLE_REF="orange_node" ID="ID_1868221972" CREATED="1428046735911" MODIFIED="1428049991438"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Available &gt;= Flex 4
    </p>
  </body>
</html>
</richcontent>
<node TEXT="[Bindable]&#xa;private var employee:ArrayCollection = new ArrayCollection();&#xa;&#xa;[Bindable]&#xa;private var selectedEmployee:Employee;" STYLE_REF="example_node" ID="ID_258046863" CREATED="1428046779624" MODIFIED="1428047102689"/>
<node TEXT="private function itemClickHandler(event:ChartItemEvent):void {&#xa;    selectedEmployee = event.hitData.item as Employee;&#xa;}" STYLE_REF="example_node" ID="ID_383014545" CREATED="1428046951584" MODIFIED="1428049991437"/>
<node TEXT="&lt;mx:ColumnChart id=&quot;employeeChart&quot;&#xa;    dataProvider=&quot;{employees}&quot;&#xa;    itemClick=&quot;itemClickHandler(event)&quot;&gt;&#xa;&lt;/mx:ColumnChart&gt;" STYLE_REF="example_node" ID="ID_370107974" CREATED="1428046869543" MODIFIED="1428047102700"/>
<node TEXT="&lt;s:TextInput id=&quot;salaryInput&quot;&#xa;    text=&quot;@{selectedEmplyee.salary}&quot;/&gt;" STYLE_REF="example_node" ID="ID_1017328769" CREATED="1428047026752" MODIFIED="1428047259941"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      @ - two-way
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Binding on getter and setter" STYLE_REF="orange_node" ID="ID_1886212996" CREATED="1429195399484" MODIFIED="1430390281101">
<node TEXT="private var _name:String;&#xa;&#xa;[Bindable(&quot;nameChanged&quot;)]&#xa;public function get name():String {&#xa;    return _name;&#xa;}&#xa;&#xa;public function set name(value:String) {&#xa;    if (_name == value)&#xa;        return;&#xa;    _name = value;&#xa;    dispatchEvent(new Event(&quot;nameChanged&quot;));&#xa;}" STYLE_REF="example_node" ID="ID_1633833496" CREATED="1429195412522" MODIFIED="1434638772476"/>
</node>
<node TEXT="Function bindings" STYLE_REF="orange_node" ID="ID_1656116723" CREATED="1435150318291" MODIFIED="1435150432448">
<node TEXT="[Bindable]&#xa;private var currentUser:String = &quot;Bill&quot;;&#xa;&#xa;private function isUserAllowed(user:String):Boolean {&#xa;     if (user == &quot;Bill&quot;) {&#xa;         return true;&#xa;     }&#xa;     return false;&#xa;}&#xa;&lt;mx:Button label=&quot;My Button&quot; enabled=&quot;{isUserAllowed(currentUser)}&quot; /&gt;" STYLE_REF="example_node" ID="ID_1436302067" CREATED="1435150331771" MODIFIED="1435150512188"/>
<node TEXT="private var _currentUser:String = &quot;Bill&quot;;&#xa;&#xa;public function set currentUser(value:String):void {&#xa;     if (_currentUser != value) {&#xa;         _currentUser = value;&#xa;         dispatchEvent(new Event(&quot;userChanged&quot;));&#xa;     }&#xa;}&#xa;&#xa;[Bindable(event=&quot;userChanged&quot;)]&#xa;private function isUserEnabled():Boolean {&#xa;     if (_currentUser == &quot;Bill&quot;) {&#xa;         return true;&#xa;     }&#xa;&#xa;     return false;&#xa;}&#xa;&lt;mx:Button label=&quot;My Button&quot; enabled=&quot;{isUserEnabled()}&quot; /&gt;" STYLE_REF="example_node" ID="ID_1957028610" CREATED="1435150435011" MODIFIED="1435150512184"/>
</node>
</node>
<node TEXT="Basic Controls" STYLE_REF="second_node" POSITION="left" ID="ID_1363024649" CREATED="1426516710761" MODIFIED="1427276115413">
<edge COLOR="#ff00ff"/>
<hook URI="../../Images/basic-controls.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Elements of UI" STYLE_REF="orange_node" ID="ID_98235381" CREATED="1426516758804" MODIFIED="1426517360790">
<node TEXT="UI elements" STYLE_REF="yellow_node" ID="ID_221891329" CREATED="1426516765144" MODIFIED="1426517366635">
<node TEXT="These are the core visual elements the user eventually sees and interacts with" STYLE_REF="pre_last_node" ID="ID_272554235" CREATED="1426516855274" MODIFIED="1426517371375"/>
</node>
<node TEXT="Layouts" STYLE_REF="yellow_node" ID="ID_977226020" CREATED="1426516779320" MODIFIED="1426517366625">
<node TEXT="They define how UI elements should be organized on&#xa;the screen and provide a final look and feel to the GUI" STYLE_REF="pre_last_node" ID="ID_868923657" CREATED="1426516872421" MODIFIED="1426517371379"/>
</node>
<node TEXT="Behavior" STYLE_REF="yellow_node" ID="ID_208098614" CREATED="1426516786784" MODIFIED="1426517366636">
<node TEXT="These are events which occur when the user interacts with UI elements" STYLE_REF="pre_last_node" ID="ID_18036123" CREATED="1426516889642" MODIFIED="1426517371389"/>
</node>
</node>
<node TEXT="Component hierarchy" STYLE_REF="orange_node" ID="ID_611047383" CREATED="1426516987219" MODIFIED="1426517376550">
<node TEXT="" ID="ID_526781452" CREATED="1426516991189" MODIFIED="1426516995945">
<hook URI="flex-ui-hierarchy.jpg" SIZE="0.729927" NAME="ExternalObject"/>
</node>
<node TEXT="The UIComponent class is the base class for all visual&#xa;components, both interactive and noninteractive" STYLE_REF="pre_last_node" ID="ID_643948124" CREATED="1426517172487" MODIFIED="1426517379699"/>
</node>
<node TEXT="Flex components" STYLE_REF="orange_node" ID="ID_1222353200" CREATED="1427372439494" MODIFIED="1428209428100">
<node TEXT="MX" STYLE_REF="yellow_node" ID="ID_1548625172" CREATED="1427372445272" MODIFIED="1427372599440"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      aka Halo
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true"/>
<node TEXT="Included in Flex 3" STYLE_REF="pre_last_node" ID="ID_368851452" CREATED="1427372475070" MODIFIED="1427372607851"/>
<node TEXT="Each component contains behavior, layout, styles and skin" STYLE_REF="pre_last_node" ID="ID_996533348" CREATED="1427372492471" MODIFIED="1427372607852"/>
</node>
<node TEXT="Spark" STYLE_REF="yellow_node" ID="ID_1347933581" CREATED="1427372449030" MODIFIED="1428209428100">
<font BOLD="true"/>
<node TEXT="New components introduced in Flex 4" STYLE_REF="pre_last_node" ID="ID_743681836" CREATED="1427372528598" MODIFIED="1427372607852"/>
<node TEXT="Separates behavior, layout, styles and skin into different classes" STYLE_REF="pre_last_node" ID="ID_1276768368" CREATED="1427372565079" MODIFIED="1427372607852"/>
</node>
<node TEXT="Spark + MX" STYLE_REF="yellow_node" ID="ID_1491888734" CREATED="1427372632845" MODIFIED="1427375229897">
<font BOLD="true"/>
<node TEXT="Can be used together" STYLE_REF="pre_last_node" ID="ID_1852733631" CREATED="1427372642215" MODIFIED="1427375235861"/>
<node TEXT="Use spark when possible" STYLE_REF="pre_last_node" ID="ID_1618120621" CREATED="1427372664398" MODIFIED="1427375235859"/>
</node>
</node>
<node TEXT="Layouts" STYLE_REF="orange_node" ID="ID_1978466886" CREATED="1427377706097" MODIFIED="1427377780252">
<node TEXT="BasicLayout" STYLE_REF="gray_node" ID="ID_1421543011" CREATED="1427377732574" MODIFIED="1428210302691">
<hook NAME="FirstGroupNode"/>
<node TEXT="x and y must be defined" STYLE_REF="pre_last_node" ID="ID_152297283" CREATED="1427377968912" MODIFIED="1428210310588"/>
<node TEXT="Constrain-based layout" STYLE_REF="yellow_node" ID="ID_1661618127" CREATED="1428210000253" MODIFIED="1428210319573">
<node TEXT="Specify the constraints by spcifying&#xa;the following properties" STYLE_REF="pre_last_node" ID="ID_201696383" CREATED="1428210031677" MODIFIED="1428210327636">
<node TEXT="baseline" ID="ID_1138009426" CREATED="1428210064773" MODIFIED="1428210069936"/>
<node TEXT="top" ID="ID_1156125461" CREATED="1428210071573" MODIFIED="1428210073328"/>
<node TEXT="bottom" ID="ID_826996364" CREATED="1428210074302" MODIFIED="1428210076671"/>
<node TEXT="left" ID="ID_1446798290" CREATED="1428210077765" MODIFIED="1428210079664"/>
<node TEXT="right" ID="ID_268466560" CREATED="1428210080653" MODIFIED="1428210082816"/>
<node TEXT="horizontalCenter" ID="ID_723077956" CREATED="1428210083750" MODIFIED="1428210090039"/>
<node TEXT="verticalCenter" ID="ID_1366424033" CREATED="1428210091036" MODIFIED="1428210094840"/>
</node>
<node TEXT="Use" STYLE_REF="pre_last_node" ID="ID_757322340" CREATED="1428210199456" MODIFIED="1428210327638">
<node TEXT="&lt;s:Panel left=&quot;50&quot; right=&quot;50&quot; top=&quot;50&quot; bottom=&quot;50&quot;&gt;&#xa;&lt;/s:Panel" STYLE_REF="example_node" ID="ID_1846478677" CREATED="1428210203452" MODIFIED="1428210516474"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Resized panel
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&lt;s:Panel verticalCenter=&quot;0&quot; horizontalCenter=&quot;0&quot;&gt;&#xa;&lt;/s:Panel" STYLE_REF="example_node" ID="ID_1337522475" CREATED="1428210203452" MODIFIED="1428210505594"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      In center
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
</node>
<node TEXT="HorizontalLayout" STYLE_REF="gray_node" ID="ID_1520267220" CREATED="1427377736624" MODIFIED="1428210297734">
<hook NAME="FirstGroupNode"/>
</node>
<node TEXT="TileLayout" STYLE_REF="gray_node" ID="ID_1555350704" CREATED="1427377749816" MODIFIED="1428210297734"/>
<node TEXT="VerticalLayout" STYLE_REF="gray_node" ID="ID_672884019" CREATED="1427377757048" MODIFIED="1428210297733"/>
<node TEXT="x and y are ignored" ID="ID_1883677993" CREATED="1427377938930" MODIFIED="1427377946739">
<hook NAME="SummaryNode"/>
</node>
<node TEXT="Use" STYLE_REF="yellow_node" ID="ID_1888391832" CREATED="1427377793531" MODIFIED="1428212143809">
<node TEXT="&lt;s:SomeComponent&gt;&#xa;    &lt;s:layout&gt;&#xa;        &lt;VerticalLayout/&gt;&#xa;    &lt;/s:layout&gt;&#xa;&lt;/s:SomeComponent&gt;" STYLE_REF="example_node" ID="ID_493027848" CREATED="1427377800009" MODIFIED="1428212143808"/>
</node>
</node>
<node TEXT="Controls" STYLE_REF="orange_node" ID="ID_1814158686" CREATED="1426517389252" MODIFIED="1428385636017">
<node TEXT="Form Controls" STYLE_REF="yellow_node" ID="ID_1171731776" CREATED="1426517402836" MODIFIED="1426517763537"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Allows users to input data and provides them
    </p>
    <p>
      &#160;interaction capability with the application
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true"/>
<node TEXT="Button" STYLE_REF="pre_last_node" ID="ID_27255386" CREATED="1426517435175" MODIFIED="1426517716577"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      The Button component is a commonly used rectangular button
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="ToggleButton" STYLE_REF="pre_last_node" ID="ID_435338989" CREATED="1426517435178" MODIFIED="1426531287286"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      The ToggleButton component defines a toggle button
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="CheckBox" STYLE_REF="pre_last_node" ID="ID_164086826" CREATED="1426517435183" MODIFIED="1426531288998"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      The CheckBox component consists of an optional label
    </p>
    <p>
      and a small box that can contain a check mark or not
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="ColorPicker" STYLE_REF="pre_last_node" ID="ID_1611237348" CREATED="1426517435186" MODIFIED="1426531291156"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      The ColorPicker control provides a way for a user to
    </p>
    <p>
      choose a color from a swatch list
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="ComboBox" STYLE_REF="pre_last_node" ID="ID_1919195754" CREATED="1426517515218" MODIFIED="1426517716583"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      The ComboBox control is a child class of the DropDownListBase control
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="DateChooser" STYLE_REF="pre_last_node" ID="ID_1553837293" CREATED="1426517566425" MODIFIED="1426517716584"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      The DateChooser control displays the name of a month, the year, and a grid of the days of the month, with columns labeled for the day of the week
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="RadioButton" STYLE_REF="pre_last_node" ID="ID_677994554" CREATED="1426517594581" MODIFIED="1426517716585"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      The RadioButton component allows the user make a single
    </p>
    <p>
      choice within a set of mutually exclusive choices
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="DropDownList" STYLE_REF="pre_last_node" ID="ID_1851323797" CREATED="1426517662844" MODIFIED="1426517716588"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      The DropDownList control contains a drop-down list from
    </p>
    <p>
      which the user can select a single value
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="NumericStepper" STYLE_REF="pre_last_node" ID="ID_1263148059" CREATED="1426517683451" MODIFIED="1426517716589"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      The NumericStepper control lets you select a number from an ordered set
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Text Controls" STYLE_REF="yellow_node" ID="ID_376987794" CREATED="1428303096187" MODIFIED="1428303145973">
<font BOLD="true"/>
<node TEXT="Label" STYLE_REF="pre_last_node" ID="ID_815108073" CREATED="1428303119171" MODIFIED="1428303309600"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Lightweight
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="RichEditableText" STYLE_REF="pre_last_node" ID="ID_377469256" CREATED="1428303122532" MODIFIED="1428303150290">
<node TEXT="&lt;s:Scroller width=&quot;100%&quot; height=&quot;100%&quot;&gt;&#xa;    &lt;s:RichEditableText editable=&quot;false&#xa;                                paragraphSpaceBefore=&quot;10&quot;&gt;&#xa;        &lt;s:p fontWeight=&quot;bold&quot;&gt;&#xa;            Title&#xa;        &lt;/s:p&gt;&#xa;        &lt;s:p&gt;&#xa;            other text&#xa;        &lt;/s:p&gt;&#xa;    &lt;/s:RichEditableText&gt;&#xa;&lt;/s:Scroller&gt;" STYLE_REF="example_node" ID="ID_821289289" CREATED="1428303735155" MODIFIED="1428319482569"/>
<node TEXT="Equivalent to &lt;mx:TextArea&gt;" STYLE_REF="example_node" ID="ID_381776592" CREATED="1428303670658" MODIFIED="1428303829418"/>
</node>
<node TEXT="RichText" STYLE_REF="pre_last_node" ID="ID_716196460" CREATED="1428303129155" MODIFIED="1428303150289">
<node TEXT="&lt;s:RichText width=&quot;220&quot;&gt;&#xa;   Some text&#xa;&lt;/s:RichText&gt;" STYLE_REF="example_node" ID="ID_166939171" CREATED="1428303605875" MODIFIED="1428303829417"/>
<node TEXT="Equivalent to &lt;mx:Text&gt;" STYLE_REF="example_node" ID="ID_1174391586" CREATED="1428303648817" MODIFIED="1428303829416"/>
</node>
<node TEXT="TextArea" STYLE_REF="pre_last_node" ID="ID_1385154918" CREATED="1426517617969" MODIFIED="1426517716586"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      TextArea is a text-entry control that lets users enter
    </p>
    <p>
      and edit multiple lines of formatted text
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="TextInput" STYLE_REF="pre_last_node" ID="ID_1195675173" CREATED="1426517639572" MODIFIED="1426517716587"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      TextInput is a text-entry control that lets users enter and
    </p>
    <p>
      edit a single line of uniformly-formatted text
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Containers" STYLE_REF="yellow_node" ID="ID_1413802262" CREATED="1427378208194" MODIFIED="1428385636017">
<font BOLD="true"/>
<node TEXT="non-Skinnable" STYLE_REF="gray_node" ID="ID_246827808" CREATED="1427378213640" MODIFIED="1427378398124">
<node TEXT="Group" STYLE_REF="pre_last_node" ID="ID_670534975" CREATED="1427378237569" MODIFIED="1427378415029"/>
<node TEXT="DataGroup" STYLE_REF="pre_last_node" ID="ID_1898903890" CREATED="1427378240296" MODIFIED="1427378415031"/>
</node>
<node TEXT="Skinnable" STYLE_REF="gray_node" ID="ID_42447369" CREATED="1427378287834" MODIFIED="1427378398123">
<node TEXT="SkinnableContainer" STYLE_REF="pre_last_node" ID="ID_491143310" CREATED="1426517944089" MODIFIED="1427378415029"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      The SkinnableContainer class is the base class
    </p>
    <p>
      &#160;for skinnable containers that provide visual content
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="SkinnableDataContainer" STYLE_REF="pre_last_node" ID="ID_4038841" CREATED="1427378308288" MODIFIED="1427378415030"/>
<node TEXT="Panel" STYLE_REF="pre_last_node" ID="ID_1804356655" CREATED="1426517907007" MODIFIED="1427378415030"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      The Panel class is a container that includes a title bar, a caption,
    </p>
    <p>
      a border, and a content area for its children
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="TitleWindow" STYLE_REF="pre_last_node" ID="ID_1883611897" CREATED="1426517983155" MODIFIED="1427378415030"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      The TitleWindow extends Panel to include a close button and move area
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Application" STYLE_REF="pre_last_node" ID="ID_1279964038" CREATED="1427378347904" MODIFIED="1427378415031"/>
<node TEXT="BorderContainer" STYLE_REF="pre_last_node" ID="ID_1224566140" CREATED="1426517807352" MODIFIED="1427378415031"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      The BorderContainer class provides a set of CSS styles that control the appearance of the border and background fill of the container
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="NavigatorContent" STYLE_REF="pre_last_node" ID="ID_423355528" CREATED="1427378375496" MODIFIED="1427378415028"/>
</node>
</node>
<node TEXT="Containers" STYLE_REF="yellow_node" ID="ID_1154486002" CREATED="1428207040630" MODIFIED="1428208214929">
<font BOLD="true"/>
<node TEXT="Layout contaniners" STYLE_REF="gray_node" ID="ID_1559332162" CREATED="1428207046447" MODIFIED="1428207233531"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Control sizing and positioning of children.
    </p>
    <p>
      Children can be controls or containers
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Form" STYLE_REF="pre_last_node" ID="ID_1249539459" CREATED="1426517833935" MODIFIED="1428331822999"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      The Form container provides control over the layout of a form, mark form fields as required or optional, handle error messages, and bind form data to the Flex data model to perform data checking and validation
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Form layouts" STYLE_REF="yellow_node" ID="ID_1115293622" CREATED="1428331207429" MODIFIED="1428331536945">
<node TEXT="Horizontal" STYLE_REF="pre_last_node" ID="ID_1209124700" CREATED="1428331211953" MODIFIED="1428331546374"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Default
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Stacked" STYLE_REF="pre_last_node" ID="ID_1777745696" CREATED="1428331216777" MODIFIED="1428331533184"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Uses two rows for each FormItem
    </p>
  </body>
</html>
</richcontent>
<node TEXT="&lt;fx:Style&gt;&#xa;    s|Form {&#xa;         skinClass: ClassReference(&quot;spark.skins.spark.StackedFormSkin&quot;);&#xa;    }&#xa;    s|FormHeading {&#xa;        skinClass: ClassReference(&quot;spark.skins.spark.StackedFormHeadingSkin&quot;);&#xa;    }&#xa;    s|FormItem {&#xa;        skinClass: ClassReference(&quot;spark.skins.spark.StackedFormItemSkin&quot;);&#xa;    }&#xa;&lt;/fx:Style&gt;" STYLE_REF="example_node" ID="ID_1465419561" CREATED="1428331301243" MODIFIED="1428331499039"/>
</node>
</node>
<node TEXT="&lt;s:FromItem label=&quot;Office phone&quot;&#xa;                    sequenceLabel=&quot;1)&quot;&#xa;                    prompt=&quot;Select employee&quot;&gt;&#xa;&lt;/s:FormItem&gt;&#xa;&lt;s:FromItem label=&quot;Mobile phone&quot;&#xa;                    sequenceLabel=&quot;2)&quot;&#xa;                    required=&quot;true&quot;&#xa;                    requiredIndicatorSource=&quot;assets/required.png&quot;&#xa;                    errorIndicatorSource=&quot;assets/invalid.png&quot;&gt;&#xa;&lt;/s:FormItem&gt;" STYLE_REF="example_node" ID="ID_1159960486" CREATED="1428331570004" MODIFIED="1428331822998"/>
</node>
<node TEXT="VGroup" STYLE_REF="pre_last_node" ID="ID_1409270739" CREATED="1426517873705" MODIFIED="1426518004997"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      The VGroup container is a Group container that uses the VerticalLayout class
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="HGroup" STYLE_REF="pre_last_node" ID="ID_1732588836" CREATED="1426517891345" MODIFIED="1426518004998"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      The HGroup container is a Group container that uses the HorizontalLayout class
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="TabBar" STYLE_REF="pre_last_node" ID="ID_668187018" CREATED="1426517968444" MODIFIED="1426518005001"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      The TabBar displays a set of identical tabs
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Navigator containers" STYLE_REF="gray_node" ID="ID_1986373686" CREATED="1428207091064" MODIFIED="1428207233532"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Control navigation among child containers.
    </p>
    <p>
      Children can only be containers
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Accordion" STYLE_REF="pre_last_node" ID="ID_1213148867" CREATED="1428207298620" MODIFIED="1428207398725">
<node TEXT="&lt;mx:Accordion resizeToContent=&quot;true&quot;&gt;&#xa;    &lt;s:Label text=&quot;Some text&quot;/&gt;&#xa;&lt;/mx:Accordion&gt;" STYLE_REF="example_node" ID="ID_1896664808" CREATED="1428207349052" MODIFIED="1428207645311"/>
</node>
<node TEXT="TabNavigator" STYLE_REF="pre_last_node" ID="ID_943916724" CREATED="1428207403214" MODIFIED="1428207626323">
<node TEXT="&lt;mx:TabNavigator resizeToContent=&quot;true&quot;&gt;&#xa;    &lt;s:Label text=&quot;Some text&quot;/&gt;&#xa;&lt;/mx:TabNavigator&gt;" STYLE_REF="example_node" ID="ID_971364640" CREATED="1428207455532" MODIFIED="1428207645310"/>
</node>
<node TEXT="ViewStack" STYLE_REF="pre_last_node" ID="ID_970293189" CREATED="1428207502693" MODIFIED="1428207626323">
<node TEXT="&lt;s:ButtonBar dataProvider=&quot;{empDepartments}&quot;/&gt;" STYLE_REF="example_node" ID="ID_325686695" CREATED="1428207610757" MODIFIED="1428207645309"/>
<node TEXT="&lt;s:TabBar dataProvider=&quot;{empDepartments}&quot;/&gt;" STYLE_REF="example_node" ID="ID_1539327415" CREATED="1428207616767" MODIFIED="1428207645308"/>
<node TEXT="&lt;mx:ViewStack id=&quot;empDepartments&quot;&#xa;                        resizeToContent=&quot;true&quot;&gt;&#xa;    &lt;s:Label text=&quot;Some text&quot;/&gt;&#xa;&lt;/mx:ViewStack&gt;" STYLE_REF="example_node" ID="ID_620193537" CREATED="1428207520054" MODIFIED="1428207645311"/>
</node>
</node>
</node>
<node TEXT="Complex Controls" STYLE_REF="yellow_node" ID="ID_1620141259" CREATED="1428052196902" MODIFIED="1428208222112">
<font BOLD="true"/>
<node TEXT="DataGrid" STYLE_REF="gray_node" ID="ID_315143802" CREATED="1428052212046" MODIFIED="1428208222111">
<node TEXT="Control features" STYLE_REF="yellow_node" ID="ID_1681193547" CREATED="1428052261133" MODIFIED="1428052382332">
<node TEXT="Interactive column width resizing" STYLE_REF="pre_last_node" ID="ID_1256696731" CREATED="1428052266902" MODIFIED="1428052376141"/>
<node TEXT="Control of column visibility" STYLE_REF="pre_last_node" ID="ID_1166233344" CREATED="1428052279593" MODIFIED="1428052376142"/>
<node TEXT="Cell and row selection" STYLE_REF="pre_last_node" ID="ID_1023652519" CREATED="1428052292134" MODIFIED="1428052376142"/>
<node TEXT="Single and multiple item selection modes" STYLE_REF="pre_last_node" ID="ID_636015628" CREATED="1428052306694" MODIFIED="1428052376143"/>
<node TEXT="Customizable column headers" STYLE_REF="pre_last_node" ID="ID_1268478719" CREATED="1428052319377" MODIFIED="1428052376143"/>
<node TEXT="Cell editing" STYLE_REF="pre_last_node" ID="ID_801620574" CREATED="1428052327694" MODIFIED="1428052376143"/>
<node TEXT="Column sorting" STYLE_REF="pre_last_node" ID="ID_1573777674" CREATED="1428052331677" MODIFIED="1428052376144"/>
<node TEXT="Custom item rederers and editors" STYLE_REF="pre_last_node" ID="ID_144811620" CREATED="1428052338295" MODIFIED="1428052376144"/>
<node TEXT="Smooth scrolling through large amounts of data" STYLE_REF="pre_last_node" ID="ID_1381623029" CREATED="1428052360313" MODIFIED="1428052376140"/>
</node>
<node TEXT="Use" STYLE_REF="yellow_node" ID="ID_548308090" CREATED="1428052399342" MODIFIED="1428052404406">
<node TEXT="private function employeeName(item:Object, column:GridColumn):String {&#xa;   return item.firstName + &quot; &quot; + item.lastName;&#xa;}&#xa;&#xa;private function formatDate(item:Object, column:GridColumn):String {&#xa;    return employyDateFormatter.format(item[column.dataField]);&#xa;}" STYLE_REF="example_node" ID="ID_1276425270" CREATED="1428052859752" MODIFIED="1428122849040"/>
<node TEXT="&lt;s:DataGrid dataProvider={employeeList}&quot;&#xa;                    alternatingRowColors=&quot;[#FFFFFF, #CCCCCC]&quot;&gt;&#xa;    &lt;s:typicalItem firstName=&quot;Christopher&quot;&#xa;                        lastName=&quot;Whinchester&quot;&#xa;                        hireDate=&quot;12/31/2011&quot;/&gt;&#xa;    &lt;/s:typicalItem&gt;&lt;!-- for fit columns--&gt;&#xa;    &lt;s:columns&gt;&#xa;        &lt;s:ArrayList&gt;&#xa;            &lt;s:GridColumn labelFunction=&quot;employeeName&quot;&#xa;                                headerText=&quot;Name&quot;/&gt;&#xa;            &lt;s:GridColumn dataField=&quot;hireDate&quot;&#xa;                                headerText=&quot;Hire Date&quot;/&gt;&#xa;        &lt;/s:ArrayList&gt;&#xa;    &lt;/s:columns&gt;&#xa;&lt;/s:DataGrid&gt;" STYLE_REF="example_node" ID="ID_309152435" CREATED="1428052525167" MODIFIED="1428122654520"/>
</node>
</node>
</node>
<node TEXT="Other" STYLE_REF="yellow_node" ID="ID_1022286621" CREATED="1427379194056" MODIFIED="1428052232593">
<font BOLD="true"/>
<node TEXT="Scroller" STYLE_REF="pre_last_node" ID="ID_1278959040" CREATED="1427379196432" MODIFIED="1428052232593">
<node TEXT="&lt;s:Scroller width=&quot;250&quot; height=&quot;200&quot;&gt;&#xa;    &lt;s:Group ...&gt;&#xa;    &lt;/s:Group&gt;&#xa;&lt;/s:Scroller&gt;" STYLE_REF="example_node" ID="ID_153476103" CREATED="1427379205577" MODIFIED="1428052194691"/>
<node TEXT="&lt;s:Group id=&quot;someGroup&quot;&gt;&#xa;&lt;/s:Group&gt;&#xa;&lt;s:Scroller viewport=&quot;{someGroup}&quot;/&gt;" STYLE_REF="example_node" ID="ID_1960867297" CREATED="1427379501303" MODIFIED="1427379580428"/>
</node>
</node>
</node>
</node>
<node TEXT="Visual Effects" STYLE_REF="second_node" POSITION="right" ID="ID_535693986" CREATED="1426518185454" MODIFIED="1427276180050">
<edge COLOR="#106c6c"/>
<hook URI="../../Images/effect.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="We can add behaviour to flex application using concept of Effects. For example, when a text box get focus, we can make its text become bolder and make its size slight bigger" STYLE_REF="pre_last_node" ID="ID_1650553112" CREATED="1426518205331" MODIFIED="1426518996764">
<icon BUILTIN="idea"/>
</node>
<node TEXT="Define effects in the Declarations tag" STYLE_REF="pre_last_node" ID="ID_893862721" CREATED="1428210990853" MODIFIED="1428211027459"/>
<node TEXT="public class Effect&#xa;extends EventDispatcher&#xa;implements IEffect" STYLE_REF="example_node" ID="ID_49311993" CREATED="1426518261508" MODIFIED="1426531277327"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Class declaration
    </p>
  </body>
</html>
</richcontent>
<node TEXT="The Effect class is an abstract base class that defines the basic functionality of all Flex effects. This class defines the base factory class for all effects" STYLE_REF="pre_last_node" ID="ID_1071042768" CREATED="1426518321466" MODIFIED="1426518996775"/>
</node>
<node TEXT="Types" STYLE_REF="orange_node" ID="ID_1803763515" CREATED="1428210766685" MODIFIED="1428211361155">
<node TEXT="mx.effects" STYLE_REF="pre_last_node" ID="ID_530953607" CREATED="1428210715453" MODIFIED="1428211366578"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Work only with UIComponent subclasses
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="spark.effects" STYLE_REF="pre_last_node" ID="ID_1289672895" CREATED="1428210723564" MODIFIED="1428211366579"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Work on any object
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Basic Effects" STYLE_REF="orange_node" ID="ID_185798061" CREATED="1426518346707" MODIFIED="1428210630511">
<node TEXT="Fade" STYLE_REF="pre_last_node" ID="ID_1058968591" CREATED="1426518353322" MODIFIED="1426518458121"/>
<node TEXT="WipeLeft" STYLE_REF="pre_last_node" ID="ID_1109340583" CREATED="1426518364414" MODIFIED="1426518458125"/>
<node TEXT="WipeRight" STYLE_REF="pre_last_node" ID="ID_1539607769" CREATED="1426518374289" MODIFIED="1426518458126"/>
<node TEXT="Move3D" STYLE_REF="pre_last_node" ID="ID_355102767" CREATED="1426518385051" MODIFIED="1426518458128"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      The Move3D class moves a target object in the x, y, and z dimensions
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Scale3D" STYLE_REF="pre_last_node" ID="ID_267081367" CREATED="1426518391849" MODIFIED="1428210630511"/>
<node TEXT="Rotate3D" STYLE_REF="pre_last_node" ID="ID_855810572" CREATED="1426518399088" MODIFIED="1426518458131"/>
<node TEXT="Animate" STYLE_REF="pre_last_node" ID="ID_1183406234" CREATED="1426518413634" MODIFIED="1426518458132"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      This Animate effect animates an arbitrary set of properties between values. Specify the properties and values to animate by setting the motionPaths property
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Composite effects" STYLE_REF="orange_node" ID="ID_636839582" CREATED="1428211483669" MODIFIED="1428211575787">
<node TEXT="Parallel" STYLE_REF="pre_last_node" ID="ID_1145955022" CREATED="1428211491371" MODIFIED="1428211700060"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      All effects run simultaneously
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Sequence" STYLE_REF="pre_last_node" ID="ID_477714606" CREATED="1428211501283" MODIFIED="1428211700059"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      All effects run sequentially
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Use" STYLE_REF="orange_node" ID="ID_233719322" CREATED="1428210799556" MODIFIED="1428211036731">
<node TEXT="&lt;s:Animate id=&quot;shake&quot;&#xa;                target=&quot;{button}&quot;&#xa;                duration=&quot;1000&quot;&gt;&#xa;    &lt;s:SimpleMotionPath property=&quot;x&quot;&#xa;                    valueTo=&quot;100&quot;/&gt;&#xa;&#xa;    &lt;s:SimpleMotionPath property=&quot;y&quot;&#xa;                    valueFrom=&quot;0&quot; valueTo=&quot;100&quot;/&gt;&#xa;&#xa;    &lt;s:SimpleMotionPath property=&quot;width&quot;&#xa;                   valueBy=&quot;20&quot;/&gt;&#xa;&lt;/s:Animate&gt;" STYLE_REF="example_node" ID="ID_391356400" CREATED="1428210801563" MODIFIED="1428211945744"/>
<node TEXT="&lt;s:Sequence  id=&quot;shake&quot;&#xa;                //target=&quot;{loginPanel}&quot;&#xa;                targets=&quot;{[loginPanel, otherPanel]}&quot;&#xa;                duration=&quot;20&quot;&gt;&#xa;    &lt;s:Move xBy=&quot;20&quot;/&gt;&#xa;    &lt;s:Move xBy=&quot;-20&quot;/&gt;&#xa;    &lt;s:Move xBy=&quot;20&quot;/&gt;&#xa;    &lt;s:Move xBy=&quot;-20&quot;/&gt;&#xa;    &lt;s:Move xBy=&quot;20&quot;/&gt;&#xa;    &lt;s:Move xBy=&quot;-20&quot;/&gt;&#xa;&lt;/s:Sequence&gt;&#xa;&lt;s:Button click=&quot;checkLogin()&quot;/&gt;&#xa;...&#xa;private function checkLogin():void {&#xa;    if (!shake.isPlaying)&#xa;        shake.play()&#xa;}" STYLE_REF="example_node" ID="ID_1814426570" CREATED="1428211065516" MODIFIED="1428211966529"/>
</node>
</node>
<node TEXT="Custom Controls" STYLE_REF="second_node" POSITION="left" ID="ID_1274839994" CREATED="1426518472623" MODIFIED="1427276111721">
<edge COLOR="#afaf1c"/>
<hook URI="../../Images/ui.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="The ways to create" STYLE_REF="orange_node" ID="ID_928284218" CREATED="1426518624227" MODIFIED="1427276396815">
<node TEXT="Using ActionScript" STYLE_REF="yellow_node" ID="ID_1848477857" CREATED="1426518634931" MODIFIED="1436798617415">
<node TEXT="package com.tutorialspoint.client {&#xa;  import spark.components.Button;&#xa;  public class CustomButton extends Button {&#xa;    public function CustomButton() {&#xa;      super();&#xa;      this.setStyle(&quot;color&quot;,&quot;green&quot;);&#xa;      this.label = &quot;Submit&quot;;&#xa;    }&#xa;  }&#xa;}" STYLE_REF="example_node" ID="ID_1378202817" CREATED="1426518713405" MODIFIED="1436798617414"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      CustomButton.as
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Using MXML" STYLE_REF="yellow_node" ID="ID_3863048" CREATED="1426518641124" MODIFIED="1427276403453">
<node TEXT="&lt;?xml version=&quot;1.0&quot; encoding=&quot;utf-8&quot;?&gt;&#xa;&lt;s:Group xmlns:fx=&quot;http://ns.adobe.com/mxml/2009&quot;&#xa;xmlns:s=&quot;library://ns.adobe.com/flex/spark&quot;&#xa;xmlns:mx=&quot;library://ns.adobe.com/flex/mx&quot; width=&quot;400&quot; height=&quot;300&quot;&gt;&#xa;    &lt;s:Form&gt;&#xa;        &lt;s:FormItem label=&quot;UserName:&quot;&gt;&#xa;            &lt;s:TextInput width=&quot;200&quot; /&gt;&#xa;        &lt;/s:FormItem&gt;&#xa;        &lt;s:FormItem label=&quot;Password:&quot;&gt;&#xa;            &lt;s:TextInput width=&quot;200&quot; displayAsPassword=&quot;true&quot; /&gt;&#xa;        &lt;/s:FormItem&gt;&#xa;        &lt;s:FormItem&gt;&#xa;            &lt;s:Button label=&quot;Login&quot; /&gt;&#xa;        &lt;/s:FormItem&gt;&#xa;    &lt;/s:Form&gt;&#xa;&lt;/s:Group&gt;" STYLE_REF="example_node" ID="ID_1833039676" CREATED="1426518818808" MODIFIED="1427382195703"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      CustomLogin.mxml
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&lt;?xml version=&quot;1.0&quot; encoding=&quot;utf-8&quot;?&gt;&#xa;&lt;s:Group xmlns:fx=&quot;http://ns.adobe.com/mxml/2009&quot;&#xa;xmlns:s=&quot;library://ns.adobe.com/flex/spark&quot;&gt;&#xa;    &lt;fx:Script&gt;&#xa;        &lt;![CDATA[&#xa;        [Bindable]&#xa;        public var imageFile:String;&#xa;&#xa;        [Bindable]&#xa;        public var fullName:String;&#xa;        ]]&gt;&#xa;    &lt;/fx:Script&gt;&#xa;    &lt;s:BitmapImage source=&quot;images/{imageFile}&quot; /&gt;&#xa;    &lt;s:Label x=&quot;0&quot; y=&quot;80&quot;  text=&quot;{fullName}&quot;/&gt;&#xa;&lt;/s:Group&gt;" STYLE_REF="example_node" ID="ID_1616629879" CREATED="1427382647546" MODIFIED="1427382728917"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      EmpoyeeDisplay.mxml
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
</node>
<node TEXT="RPC Services" STYLE_REF="second_node" POSITION="right" ID="ID_448475697" CREATED="1426518925582" MODIFIED="1428385463043">
<edge COLOR="#7c0000"/>
<hook URI="../../Images/server.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="RPC services for providing server side data to client side" STYLE_REF="pre_last_node" ID="ID_1362145002" CREATED="1426520535904" MODIFIED="1426520769999">
<icon BUILTIN="idea"/>
</node>
<node TEXT="Types of RPC Services" STYLE_REF="orange_node" ID="ID_1715394239" CREATED="1426519040317" MODIFIED="1426520756623">
<node TEXT="HttpService" STYLE_REF="yellow_node" ID="ID_1890786185" CREATED="1426520555870" MODIFIED="1426520762768">
<font BOLD="true"/>
<node TEXT="&lt;mx:HTTPService&gt; tag is used to represent an HTTPService object in an MXML file. When you make a call to HTTPService object&apos;s send() method, it makes an HTTP request to the specified URL, and an HTTP response is returned.You can also use the HTTP HEAD, OPTIONS, GET, POST, TRACE, and DELETE methods" STYLE_REF="pre_last_node" ID="ID_1543607518" CREATED="1426520682389" MODIFIED="1427610293647">
<icon BUILTIN="idea"/>
</node>
<node TEXT="Use" STYLE_REF="gray_node" ID="ID_157446733" CREATED="1426610496233" MODIFIED="1426610545892">
<node TEXT="Static file" STYLE_REF="yellow_node" ID="ID_1029929079" CREATED="1427610577399" MODIFIED="1427610991015">
<node TEXT="&lt;items&gt;&#xa;&lt;item name=&quot;Book&quot; description=&quot;History of France&quot;&gt;&lt;/item&gt;&#xa;&lt;item name=&quot;Pen&quot; description=&quot;Parker Pen&quot;&gt;&lt;/item&gt;&#xa;&lt;item name=&quot;Pencil&quot; description=&quot;Stationary&quot;&gt;&lt;/item&gt;&#xa;&lt;items&gt;" STYLE_REF="example_node" ID="ID_1338806316" CREATED="1426610500400" MODIFIED="1426610550374"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Xml file in server
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&lt;fx:Declarations&gt;&#xa;    &lt;mx:HTTPService id=&quot;itemRequest&quot;  &#xa;          url=&quot;http://www.tutorialspoint.com/flex/Items.xml&quot;/&gt;&#xa;&lt;/fx:Declarations&gt;" STYLE_REF="example_node" ID="ID_745874382" CREATED="1426610105481" MODIFIED="1427610401054"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Declaration
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="...&#xa;itemRequest.send();&#xa;...&#xa;&lt;mx:DataGrid id=&quot;dgItems&quot; height=&quot;80%&quot; width=&quot;75%&quot;&#xa;dataProvider=&quot;{itemRequest.lastResult.items.item}&quot;&gt;&#xa;    &lt;mx:columns&gt;&#xa;        &lt;mx:DataGridColumn headerText=&quot;Name&quot; dataField=&quot;name&quot;/&gt;&#xa;        &lt;mx:DataGridColumn headerText=&quot;Description&quot; dataField=&quot;description&quot;/&gt;&#xa;    &lt;/mx:columns&gt;&#xa;&lt;/mx:DataGrid&gt;" STYLE_REF="example_node" ID="ID_419809153" CREATED="1426610526215" MODIFIED="1427611028463"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Make a call to itemRequest.send() method and bind values from lastResult object of itemRequest webservice to Flex UI component
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Dinamic file" STYLE_REF="yellow_node" ID="ID_174998446" CREATED="1427610378281" MODIFIED="1427873875688">
<node TEXT="&lt;fx:Declarations&gt;&#xa;    &lt;mx:HTTPService id=&quot;itemRequest&quot;  &#xa;        url=&quot;http://www.tutorialspoint.com/somscript.jsp&quot;&#xa;        result=&quot;itemRequest_resultHandler&quot;&#xa;        fault=&quot;itemRequest_faultHandler&quot;/&gt;&#xa;&lt;/fx:Declarations&gt;" STYLE_REF="example_node" ID="ID_1954026202" CREATED="1427610388601" MODIFIED="1427873875685"/>
<node TEXT="function itemRequest_resultHandler(event:ResultEvent):void {&#xa;    event.result.items.time;&#xa;}" STYLE_REF="example_node" ID="ID_1396627537" CREATED="1427610503337" MODIFIED="1427611005506"/>
</node>
</node>
<node TEXT="Fault" STYLE_REF="gray_node" ID="ID_1454833849" CREATED="1427610734688" MODIFIED="1427610979887">
<node TEXT="FaultEvent" STYLE_REF="yellow_node" ID="ID_1631881585" CREATED="1427610893104" MODIFIED="1427610997295">
<node TEXT="faultDetail" STYLE_REF="pre_last_node" ID="ID_374765895" CREATED="1427610759440" MODIFIED="1427611011711"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Any extra details about the fault
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="faultCode" STYLE_REF="pre_last_node" ID="ID_1955467064" CREATED="1427610772360" MODIFIED="1427611011712"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      A code of the fault
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="faultString" STYLE_REF="pre_last_node" ID="ID_1024795117" CREATED="1427610779056" MODIFIED="1427611011713"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Text description of the fault
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="message" STYLE_REF="pre_last_node" ID="ID_1379987556" CREATED="1427610783681" MODIFIED="1427611011713"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Contains three above properties
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="fault=&quot;itemRequest_faultHandler&quot;" STYLE_REF="pre_last_node" ID="ID_1435784431" CREATED="1427610942433" MODIFIED="1427611011713"/>
</node>
<node TEXT="Send" STYLE_REF="gray_node" ID="ID_1491450295" CREATED="1427873853438" MODIFIED="1427873865477">
<node TEXT="Using parameter binding" STYLE_REF="yellow_node" ID="ID_862151886" CREATED="1427874353127" MODIFIED="1427874362431">
<node TEXT="&lt;s:HTTPService id=&quot;vehicleService&quot;&#xa;    url=&quot;http://someurl.com/remoteData/addVehicleRequest.cfm&quot;&#xa;    result=&quot;vehicleService_resultHandler(event)&quot;&gt;&#xa;    &lt;mx:request&gt;&#xa;        &lt;officePhone&gt;{phone.text}&lt;/officePhone&gt;&#xa;    &lt;/mx:request&gt;&#xa;&lt;/s:HTTPSService&gt;" STYLE_REF="example_node" ID="ID_1056444881" CREATED="1427873887327" MODIFIED="1427874565165"/>
<node TEXT="&lt;s:Button click=&quot;vehicleService.send()&quot;/&gt;" STYLE_REF="example_node" ID="ID_1921136386" CREATED="1427874614935" MODIFIED="1427874625341"/>
</node>
<node TEXT="Using explicit parameter passing" STYLE_REF="yellow_node" ID="ID_1474035841" CREATED="1427874326414" MODIFIED="1427874362429">
<node TEXT="Cannot use automatic Flex data validators" STYLE_REF="pre_last_node" ID="ID_907658297" CREATED="1427874378415" MODIFIED="1427874573750"/>
<node TEXT="&lt;s:HTTPService id=&quot;vehicleService&quot;&#xa;    url=&quot;http://someurl.com/remoteData/addVehicleRequest.cfm&quot;/&gt;" STYLE_REF="example_node" ID="ID_208908728" CREATED="1427874398863" MODIFIED="1427874560893"/>
<node TEXT="&lt;s:Button click=&quot;vehicleService.send(someObj)&quot;/&gt;" STYLE_REF="example_node" ID="ID_1821350269" CREATED="1427874524909" MODIFIED="1427874560897"/>
<node TEXT="&lt;s:Button click=&quot;vehicleService.send({employeeID:4})&quot;/&gt;" STYLE_REF="example_node" ID="ID_1140183104" CREATED="1427874533361" MODIFIED="1427874560898"/>
</node>
</node>
</node>
<node TEXT="WebService" STYLE_REF="yellow_node" ID="ID_447948808" CREATED="1426520560172" MODIFIED="1427951009747">
<font BOLD="true"/>
<node TEXT="The &lt;mx:WebService&gt; tag is used to get access&#xa;to the operations of SOAP-compliant web services" STYLE_REF="pre_last_node" ID="ID_544585698" CREATED="1426520603283" MODIFIED="1427275064592">
<icon BUILTIN="idea"/>
</node>
<node TEXT="SOAP" STYLE_REF="gray_node" ID="ID_194719562" CREATED="1427795914538" MODIFIED="1427796009346">
<node TEXT="Simple Object Access Protocol (SOAP) is an industry-standard specification for web services" STYLE_REF="pre_last_node" ID="ID_1391980633" CREATED="1427787139985" MODIFIED="1427796203402"/>
<node TEXT="The service interface" STYLE_REF="pre_last_node" ID="ID_1301154090" CREATED="1427794820754" MODIFIED="1427796203401"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Is defined in a Web Service Description Language (WSDL) document
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Can be accessed at runtime" ID="ID_1115151616" CREATED="1427795925329" MODIFIED="1427795934119"/>
<node TEXT="Transported over HTTP" ID="ID_280712185" CREATED="1427795935354" MODIFIED="1427795942839"/>
</node>
</node>
<node TEXT="Use" STYLE_REF="gray_node" ID="ID_1178211414" CREATED="1427795994627" MODIFIED="1427951009746">
<node TEXT="&lt;s:WebService id=&quot;employeeService&quot;&#xa;    wsdl=&quot;http://www.adobetes.com/EmployeeData.cfc?wsdl&quot;&#xa;    load=&quot;employeeService.getEmployeeRecord()&quot;&#xa;    result=&quot;resultHandler(event)&quot;/&gt;" STYLE_REF="example_node" ID="ID_609321712" CREATED="1427796011906" MODIFIED="1427796646264">
<node TEXT="wsdl" STYLE_REF="yellow_node" ID="ID_1886845017" CREATED="1427796090866" MODIFIED="1427796212355">
<node TEXT="The address can refer to static or&#xa;dynamically generated documents" STYLE_REF="pre_last_node" ID="ID_564253151" CREATED="1427796103137" MODIFIED="1427796221414"/>
<node TEXT="The WSDL document is loaded automatically upon object creation when service is declared in MXML" STYLE_REF="pre_last_node" ID="ID_93491865" CREATED="1427796142188" MODIFIED="1427796221416"/>
<node TEXT="A relative or absolute address of web service" STYLE_REF="pre_last_node" ID="ID_1560466694" CREATED="1427796174474" MODIFIED="1427796221417"/>
</node>
</node>
<node TEXT="employeeService.methodName();" STYLE_REF="example_node" ID="ID_136069673" CREATED="1427796232331" MODIFIED="1427796313042"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Invoking the methods on a web service
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&lt;s:Label text=&quot;{employeeService.getEmployeeRecord.lastResult}&quot;/&gt;" STYLE_REF="example_node" ID="ID_507896040" CREATED="1427796560826" MODIFIED="1427796600697"/>
<node TEXT="[Bindable]&#xa;private var employees:ArrayCollection;&#xa;private function resultHandler(event:ResultEvent):void {&#xa;    employees = event.result as ArrayCollection;&#xa;}" STYLE_REF="example_node" ID="ID_984479714" CREATED="1427796661626" MODIFIED="1427796721505"/>
<node TEXT="&lt;s:DropDownList id=&quot;dropDown&quot;&#xa;    dataProvider=&quot;{employees}&quot;&#xa;    labelField=&quot;LASTNAME&quot;/&gt;" STYLE_REF="example_node" ID="ID_577120269" CREATED="1427796863908" MODIFIED="1427796981146"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Use UPPERCASE fields
    </p>
  </body>
</html>
</richcontent>
<node TEXT="WebService not case sensitive" STYLE_REF="pre_last_node" ID="ID_1947910865" CREATED="1427796952042" MODIFIED="1427797015302"/>
</node>
</node>
<node TEXT="Fault" STYLE_REF="gray_node" ID="ID_1306856272" CREATED="1427796988811" MODIFIED="1427797018534">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="80" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_1454833849" STARTINCLINATION="636;0;" ENDINCLINATION="690;0;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
</node>
<node TEXT="Multiple methods" STYLE_REF="gray_node" ID="ID_370153772" CREATED="1427797127571" MODIFIED="1427797162546">
<node TEXT="serviceObjId.method1.lastResult&#xa;serviceObjId.method2.lastResult" STYLE_REF="pre_last_node" ID="ID_1242084924" CREATED="1427797133731" MODIFIED="1427797176362"/>
<node TEXT="&lt;s:WebService id=&quot;employeeService&quot;&#xa;    wsdl=&quot;http://www.adobetes.com/EmployeeData.cfc?wsdl&quot;&gt;&#xa;    &lt;mx:operation name=&quot;getEmployees&quot; result=&quot;employeeHandler(event)&quot;/&gt;&#xa;    &lt;mx:operation name=&quot;getDepartments&quot; result=deptHandler(event)&quot;/&gt;&#xa;&lt;/s:WebService&gt;" STYLE_REF="example_node" ID="ID_1124866965" CREATED="1427797192746" MODIFIED="1427797314203"/>
</node>
<node TEXT="Send" STYLE_REF="gray_node" ID="ID_1410517904" CREATED="1427896839549" MODIFIED="1427951233829">
<node TEXT="Using parameter binding" STYLE_REF="yellow_node" ID="ID_1997691821" CREATED="1427874353127" MODIFIED="1427874362431">
<node TEXT="&lt;s:WebService id=&quot;vehicleService&quot;&#xa;    wsdl=&quot;http://www.adobetes.com/vehicleRequestData.cfc?wsdl&quot;&#xa;    result=&quot;vehicleService_resultHander(event)&quot;&gt;&#xa;    &lt;mx:operation name=&quot;addVehicleRequest&quot;&gt;&#xa;        &lt;mx:request&gt;&#xa;            &lt;officePhone&gt;{phone.text}&lt;/officePhone&gt;&#xa;            &lt;mobilePhone&gt;{mobilePhone.text}&lt;/mobilePhone&gt;&#xa;        &lt;/mx:request&gt;&#xa;    &lt;/mx:operation&gt;&#xa;&lt;/s:WebService&gt;" STYLE_REF="example_node" ID="ID_1898605786" CREATED="1427873887327" MODIFIED="1427951844604"/>
<node TEXT="&lt;s:Button click=&quot;addVehicleRequest.send()&quot;/&gt;" STYLE_REF="example_node" ID="ID_943135644" CREATED="1427874614935" MODIFIED="1427951133033"/>
</node>
<node TEXT="Using explicit parameter passing" STYLE_REF="yellow_node" ID="ID_623401455" CREATED="1427874326414" MODIFIED="1427874362429">
<node TEXT="Cannot use automatic Flex data validators" STYLE_REF="pre_last_node" ID="ID_779440839" CREATED="1427874378415" MODIFIED="1427874573750"/>
<node TEXT="&lt;s:Button click=&quot;employeeService.addVehicleRequest(someObj)&quot;/&gt;" STYLE_REF="example_node" ID="ID_1569611975" CREATED="1427874524909" MODIFIED="1427951170439"/>
<node TEXT="&lt;s:Button click=&quot;employeeService.addVehicleRequest({employeeID:4})&quot;/&gt;" STYLE_REF="example_node" ID="ID_1985334106" CREATED="1427874533361" MODIFIED="1427951176571"/>
</node>
</node>
</node>
<node TEXT="RemoteObject" STYLE_REF="yellow_node" ID="ID_45433033" CREATED="1426520564941" MODIFIED="1426520762773">
<font BOLD="true"/>
<node TEXT="The &lt;mx:RemoteObject&gt; tag is used to represent an HTTPService object in an MXML file. This tag gives you access to the methods of Java objects using Action Message Format (AMF) [binary format] encoding (You can call methods directly)" STYLE_REF="pre_last_node" ID="ID_1090960867" CREATED="1426520639350" MODIFIED="1427611427710">
<icon BUILTIN="idea"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      AMF smaller and faster than XML
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Native data types are converted" STYLE_REF="pre_last_node" ID="ID_117276933" CREATED="1427611457048" MODIFIED="1427611475407"/>
<node TEXT="AMF-compatible app servers" STYLE_REF="gray_node" ID="ID_1328865405" CREATED="1427611215472" MODIFIED="1427611276271">
<node TEXT="LiveCycle Data Services" STYLE_REF="pre_last_node" ID="ID_780667598" CREATED="1427611248345" MODIFIED="1427611350361"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      LCDS
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="BlazeDS" STYLE_REF="pre_last_node" ID="ID_1051706863" CREATED="1427611267202" MODIFIED="1427611350361"/>
<node TEXT="ColdFusion MX" STYLE_REF="pre_last_node" ID="ID_1866489948" CREATED="1427611277681" MODIFIED="1427611350361"/>
<node TEXT="PHP" STYLE_REF="yellow_node" ID="ID_1496115672" CREATED="1427611294808" MODIFIED="1427611354423">
<node TEXT="Zend" STYLE_REF="pre_last_node" ID="ID_1315766301" CREATED="1427611299161" MODIFIED="1427611350361"/>
<node TEXT="AMFPHP" STYLE_REF="pre_last_node" ID="ID_1887230348" CREATED="1427611302897" MODIFIED="1427611350362"/>
<node TEXT="SabreAMF" STYLE_REF="pre_last_node" ID="ID_629978361" CREATED="1427611307855" MODIFIED="1427611350362"/>
<node TEXT="WebORB" STYLE_REF="pre_last_node" ID="ID_85015778" CREATED="1427611315160" MODIFIED="1427611350360"/>
</node>
</node>
<node TEXT="Destination" STYLE_REF="gray_node" ID="ID_883507973" CREATED="1427611587993" MODIFIED="1427611874856">
<node TEXT="A service destination is essential an alias mapping" STYLE_REF="pre_last_node" ID="ID_1917895733" CREATED="1427611599352" MODIFIED="1427611976015"/>
<node TEXT="Define in remoting-config.xml on the server" STYLE_REF="pre_last_node" ID="ID_921408155" CREATED="1427611621177" MODIFIED="1427611976016"/>
<node TEXT="&lt;destination id=&quot;javaClassAlias&quot;&gt;&#xa;    &lt;properties&gt;&#xa;        &lt;source&gt;package.ClassName&lt;/source&gt;&#xa;        &lt;scope&gt;application&lt;/scope&gt;&#xa;    &lt;/properties&gt;&#xa;&lt;/destination&gt;" STYLE_REF="example_node" ID="ID_325798009" CREATED="1427651394137" MODIFIED="1427651468124"/>
</node>
<node TEXT="Use" STYLE_REF="gray_node" ID="ID_435754592" CREATED="1427611682913" MODIFIED="1427611874857">
<node TEXT="&lt;s:RemoteObject id=&quot;empoyeeService&quot;&#xa;    destination=&quot;ColdFusion&quot;&#xa;    source=&quot;f45iaw100.remoteData.employeeData&quot;&#xa;    endpoint=&quot;http://adobetest.com/flex2gateway/&quot;&#xa;    result=&quot;empoyeeService_resultHander(event)&quot;/&gt;" STYLE_REF="example_node" ID="ID_701077722" CREATED="1427611685601" MODIFIED="1427643321931">
<node TEXT="destination" STYLE_REF="pre_last_node" ID="ID_1837236121" CREATED="1427611776000" MODIFIED="1427611893079"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      May be specific to the server implementation
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="source" STYLE_REF="pre_last_node" ID="ID_453486162" CREATED="1427611818792" MODIFIED="1427611893080"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Fully qualified package or path to the service
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="endpoint" STYLE_REF="pre_last_node" ID="ID_124532710" CREATED="1427611846481" MODIFIED="1427611893080"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      The address to the remoting gateway
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="&lt;mx:RemoteObject id=&quot;employeeService&quot;&gt;&#xa;    &lt;mx:method name=&quot;getEmployees&quot;&#xa;        result=&quot;allEmployeesHandler(event)&quot;/&gt;&#xa;&lt;/mx:RemoteObject&gt;" STYLE_REF="example_node" ID="ID_1819786131" CREATED="1427651231719" MODIFIED="1427651353870"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Different handlers for different methods
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="public var employees:ArrayCollection;&#xa;&#xa;function empoyeeService_resultHander(event:ResultEvent):void {&#xa;    employees = event.result as ArrayCollection;&#xa;}" STYLE_REF="example_node" ID="ID_1814194018" CREATED="1427643332502" MODIFIED="1427643458404"/>
<node TEXT="empoyeeService.remoteName();" STYLE_REF="pre_last_node" ID="ID_87791454" CREATED="1427611930624" MODIFIED="1427643418462"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Invoke a remote service method to get data from the server
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="serviceObjID.remoteMethod.lastResult" STYLE_REF="pre_last_node" ID="ID_455755520" CREATED="1427643215045" MODIFIED="1427643418461"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Data can be accessed
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Fault" STYLE_REF="gray_node" ID="ID_640787648" CREATED="1427643579566" MODIFIED="1427643600716">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="80" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_1454833849" STARTINCLINATION="979;0;" ENDINCLINATION="979;0;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
</node>
<node TEXT="Send" STYLE_REF="gray_node" ID="ID_566288088" CREATED="1427896839549" MODIFIED="1427951233829">
<node TEXT="&lt;s:RemoteObject id=&quot;vehicleService&quot;&#xa;    destination=&quot;ColdFusion&quot;&#xa;    source=&quot;f45iaw100.remoteData.employeeData&quot;&#xa;    endpoint=&quot;http://adobetest.com/flex2gateway/&quot;&#xa;    result=&quot;vehicleService_resultHander(event)&quot;/&gt;" STYLE_REF="example_node" ID="ID_1227442826" CREATED="1427896845381" MODIFIED="1427951397716"/>
<node TEXT="package valueObjects {&#xa;    [RemoteClass(alias=&quot;f45iaw100.remoteData.VehicleRequest&quot;)]&#xa;    public class VehicleRequest {&#xa;        public var id:String;&#xa;        public var phone:String;&#xa;        public var mobilePhone:String;&#xa;        public var pickupDate:String;&#xa;        public var returnDate:String;&#xa;&#xa;        public function VehicleRequest() {&#xa;        }&#xa;    }&#xa;}" STYLE_REF="example_node" ID="ID_709238679" CREATED="1427950914169" MODIFIED="1427951220487"/>
<node TEXT="Using parameter binding" STYLE_REF="yellow_node" ID="ID_1545299198" CREATED="1427874353127" MODIFIED="1427874362431">
<node TEXT="&lt;s:RemoteObject id=&quot;vehicleService&quot;&gt;&#xa;&lt;mx:method name=&quot;addVehicleRequest&quot;&gt;&#xa;    &lt;mx:argumets&gt;&#xa;        &lt;officePhone&gt;{phone.text}&lt;/officePhone&gt;&#xa;        &lt;mobilePhone&gt;{mobilePhone.text}&lt;/mobilePhone&gt;&#xa;    &lt;/mx:argumets&gt;&#xa;&lt;/mx:method&gt;&#xa;&lt;/s:RemoteObject&gt;" STYLE_REF="example_node" ID="ID_849827349" CREATED="1427873887327" MODIFIED="1427951204517"/>
<node TEXT="&lt;s:Button click=&quot;addVehicleRequest.send()&quot;/&gt;" STYLE_REF="example_node" ID="ID_1676039554" CREATED="1427874614935" MODIFIED="1427951133033"/>
</node>
<node TEXT="Using explicit parameter passing" STYLE_REF="yellow_node" ID="ID_1248502301" CREATED="1427874326414" MODIFIED="1427874362429">
<node TEXT="Cannot use automatic Flex data validators" STYLE_REF="pre_last_node" ID="ID_453145609" CREATED="1427874378415" MODIFIED="1427874573750"/>
<node TEXT="&lt;s:Button click=&quot;employeeService.addVehicleRequest(someObj)&quot;/&gt;" STYLE_REF="example_node" ID="ID_863498111" CREATED="1427874524909" MODIFIED="1427951170439"/>
<node TEXT="&lt;s:Button click=&quot;employeeService.addVehicleRequest({employeeID:4})&quot;/&gt;" STYLE_REF="example_node" ID="ID_1882427702" CREATED="1427874533361" MODIFIED="1427951176571"/>
</node>
</node>
</node>
</node>
</node>
<node TEXT="Internationalization" STYLE_REF="second_node" POSITION="left" ID="ID_1774320645" CREATED="1426610817201" MODIFIED="1427275623627">
<edge COLOR="#00007c"/>
<hook URI="../../Images/internalization.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Techniques" STYLE_REF="orange_node" ID="ID_1513431515" CREATED="1426610830927" MODIFIED="1426611516185">
<node TEXT="Compile Time Internationalization" STYLE_REF="yellow_node" ID="ID_509627723" CREATED="1426610836477" MODIFIED="1426611522942">
<node TEXT="This technique is most prevalent and requires very little overhead at&#xa;runtime; is a very efficient technique for translating both constant and&#xa;parameterized strings;simplest to implement" STYLE_REF="pre_last_node" ID="ID_779626190" CREATED="1426610877230" MODIFIED="1426611463670"/>
<node TEXT="Compile Time internationalization uses standard properties files to store translated strings and parameterized messages, and these properties files are compiled&#xa;directly in the application" STYLE_REF="pre_last_node" ID="ID_1162834257" CREATED="1426610886052" MODIFIED="1426611463667"/>
</node>
<node TEXT="Run Time Internationalization" STYLE_REF="yellow_node" ID="ID_294607824" CREATED="1426610841419" MODIFIED="1426611522948">
<node TEXT="This technique is very flexible but slower than static string internationalization. You need to compile the localization properties files separately, leave them external to application, and load them at run time" STYLE_REF="pre_last_node" ID="ID_723943381" CREATED="1426610913861" MODIFIED="1426611463669"/>
</node>
</node>
<node TEXT="Workflow" STYLE_REF="orange_node" ID="ID_48285876" CREATED="1426610939171" MODIFIED="1428936519650">
<node TEXT="Create folder structure" STYLE_REF="yellow_node" ID="ID_1988114946" CREATED="1426610973177" MODIFIED="1426611522948">
<node TEXT="Create a locale folder under src folder of Flex project.This will be the parent directory for all of the properties files for the locales that the application will support" STYLE_REF="pre_last_node" ID="ID_414190407" CREATED="1426610982132" MODIFIED="1426611463672"/>
<node TEXT="{language}_{country code}" STYLE_REF="pre_last_node" ID="ID_1843379569" CREATED="1426611010940" MODIFIED="1426611528083"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Inside the locale folder, create subfolders, one for each of the application's locales to be supported. The convention for naming a locale shown above
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true"/>
<node TEXT="en_US" ID="ID_821975483" CREATED="1426611043762" MODIFIED="1426611044148"/>
</node>
</node>
<node TEXT="Create properties files" STYLE_REF="yellow_node" ID="ID_1523720949" CREATED="1426611056512" MODIFIED="1426611522949">
<node TEXT="Create properties file containing the messages to be used in the application" STYLE_REF="pre_last_node" ID="ID_1096175682" CREATED="1426611343757" MODIFIED="1426611463674"/>
<node TEXT="enterName=Enter your name&#xa;clickMe=Click Me&#xa;applicationTitle=Application Internationalization Demonstration&#xa;greeting=Hello {0}" STYLE_REF="example_node" ID="ID_144193895" CREATED="1426611351219" MODIFIED="1427274892457"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      We've created helloWorld.properties file
    </p>
    <p>
      under src &gt; locale &gt; en_US folder
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Set file encoding as UTF-8" STYLE_REF="pre_last_node" ID="ID_35946017" CREATED="1426611440804" MODIFIED="1426611467034">
<font BOLD="true"/>
</node>
</node>
<node TEXT="Specify Compiler options" STYLE_REF="yellow_node" ID="ID_254089633" CREATED="1426611510736" MODIFIED="1426611522949">
<node TEXT="-locale en_US de_DE" STYLE_REF="pre_last_node" ID="ID_260409496" CREATED="1426611580842" MODIFIED="1426611639453"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Add the following to the Additional Compiler Arguments settings
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true"/>
</node>
<node TEXT="src\locale\{locale}" STYLE_REF="pre_last_node" ID="ID_1553518754" CREATED="1426611620182" MODIFIED="1426611639450"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Select Flex Build Path, and add the following to the Source Path settings
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true"/>
</node>
</node>
<node TEXT="Bundle the resource" STYLE_REF="yellow_node" ID="ID_1459774979" CREATED="1428935959189" MODIFIED="1428936519650">
<node TEXT="&lt;fx:Metadata&gt;&#xa;      [ResourceBundle(&quot;helloWorld&quot;)]&#xa;&lt;/fx:Metadata&gt;" STYLE_REF="example_node" ID="ID_1677447548" CREATED="1427274664887" MODIFIED="1428936422844"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      in MXML file
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="[ResourceBundle(&quot;helloWorld&quot;)]&#xa;public class CustomSwitch  extends Sprite {" STYLE_REF="example_node" ID="ID_783454588" CREATED="1428936407782" MODIFIED="1428936517570"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      In as file
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Use in application" STYLE_REF="yellow_node" ID="ID_468559788" CREATED="1427274951209" MODIFIED="1427274972215">
<node TEXT="ResourceManager.getInstance().getString(&apos;helloWorld&apos;, &apos;helloWorld.applicationTitle&apos;);" STYLE_REF="example_node" ID="ID_1473411363" CREATED="1427274748250" MODIFIED="1427274943864"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Use it in script
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
</node>
<node TEXT="MXML" STYLE_REF="second_node" POSITION="right" ID="ID_307264054" CREATED="1427287802783" MODIFIED="1428385511782">
<edge COLOR="#007c00"/>
<hook URI="../../Images/mxml.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Syntax" STYLE_REF="third_node" ID="ID_648739043" CREATED="1427287815148" MODIFIED="1427287825097">
<node TEXT="Not permitted Conditions" STYLE_REF="orange_node" ID="ID_405935697" CREATED="1427287840148" MODIFIED="1427288028785">
<node TEXT="&gt;" STYLE_REF="pre_last_node" ID="ID_1319896716" CREATED="1427287847562" MODIFIED="1427288034849"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &amp;gt;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&lt;" STYLE_REF="pre_last_node" ID="ID_358356038" CREATED="1427287849627" MODIFIED="1427288034851"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &amp;lt;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&amp;&amp;" STYLE_REF="pre_last_node" ID="ID_846057011" CREATED="1427287851566" MODIFIED="1430840782074"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &amp;amp;&amp;amp;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
</node>
<node TEXT="Name spaces" STYLE_REF="second_node" POSITION="left" ID="ID_1535997727" CREATED="1427372026343" MODIFIED="1428385610180">
<edge COLOR="#7c007c"/>
<hook URI="../../Images/namespace.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Standart" STYLE_REF="orange_node" ID="ID_1657010551" CREATED="1427372056239" MODIFIED="1427372288090">
<node TEXT="It comes from sdk/framework/flex-config.xml" STYLE_REF="pre_last_node" ID="ID_1161505903" CREATED="1427372316111" MODIFIED="1427372342161"/>
<node TEXT="xmlns:s=&quot;library://ns.adobe.com/flex/spark&quot;" STYLE_REF="example_node" ID="ID_673244325" CREATED="1427372060760" MODIFIED="1427372296655"/>
<node TEXT="xmlns:fx=&quot;http://ns.adobe.com/mxml/2009&quot;" STYLE_REF="example_node" ID="ID_845955299" CREATED="1427372074838" MODIFIED="1427372296659"/>
<node TEXT="xmlns:mx=&quot;library://ns.adobe.com/flex/mx&quot;" STYLE_REF="example_node" ID="ID_531211903" CREATED="1427372095519" MODIFIED="1427372296660"/>
</node>
<node TEXT="Custom" STYLE_REF="orange_node" ID="ID_996324379" CREATED="1427372098160" MODIFIED="1427372288089">
<node TEXT="xmlns:ns1=&quot;com.flexLlib.view.components.*&quot;&#xa;&#xa;&lt;ns1:UsersWall id=&quot;userWall&quot;/&gt;" STYLE_REF="example_node" ID="ID_12017364" CREATED="1427372101078" MODIFIED="1427372296656"/>
<node TEXT="xmlns:scrollbar=&quot;com.flexLlib.view.components.scorllBar.*&quot;  &lt;!-- Here custom controls --&gt;&#xa;&lt;scrollbar:CustomVScrollBar id=&quot;scrollBar&quot;/&gt;" STYLE_REF="example_node" ID="ID_441031885" CREATED="1427372217054" MODIFIED="1427372296651"/>
</node>
</node>
<node TEXT="CSS" STYLE_REF="second_node" POSITION="right" ID="ID_1156178999" CREATED="1427375545590" MODIFIED="1428385563698">
<edge COLOR="#007c7c"/>
<hook URI="../../Images/css.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Declaration" STYLE_REF="orange_node" ID="ID_1854228663" CREATED="1427375555271" MODIFIED="1428330283145">
<node TEXT="&lt;fx:Declarations&gt;&#xa;    &lt;fx:Style&gt;&#xa;        .myFontStyle {&#xa;            fontSize: 15;&#xa;            color: #9933FF;&#xa;        }&#xa;    &lt;/fx:Style&gt;&#xa;&lt;/fx:Declarations&gt;" STYLE_REF="example_node" ID="ID_1726335982" CREATED="1427375569895" MODIFIED="1428330001261"/>
<node TEXT="&lt;fx:Declarations&gt;&#xa;    &lt;fx:Style source=&quot;../css/SimpleTypeSelector.css&quot;/&gt;&#xa;&lt;/fx:Declarations&gt;" STYLE_REF="example_node" ID="ID_255060645" CREATED="1427375587341" MODIFIED="1428330210877">
<node TEXT="@namespace s &apos;library://ns.adobe.com/flex/spark&apos;&#xa;.myFontStyle {&#xa;    fontSize: 15;&#xa;    color: #9933FF;&#xa;}&#xa;.titleHeader {&#xa;    fontSize: 16;&#xa;    color: #FFFFFF;&#xa;}" STYLE_REF="example_node" ID="ID_7343749" CREATED="1427375692687" MODIFIED="1427375843759"/>
</node>
<node TEXT="/* CSS file */&#xa;@namespace s &quot;library://ns.adobe.com/flex/spark&quot;;&#xa;@namespace mx &quot;library://ns.adobe.com/flex/mx&quot;;&#xa;@namespace cx &quot;components.*&quot;;&#xa;&#xa;global {&#xa;    font-family: Verdana;&#xa;    font-size: 11;&#xa;}&#xa;&#xa;s|Panel {&#xa;    backgroundColor: #E8E8E8;&#xa;}&#xa;&#xa;s|Button {&#xa;    color: #0074AA;&#xa;}&#xa;&#xa;s|Button {&#xa;    color:#000000;&#xa;}&#xa;&#xa;s|Button {&#xa;    font-family:&quot;Arial&quot;;&#xa;}&#xa;&#xa;cx|MonthlyEvents {&#xa;    color: #0B85B7;&#xa;}" STYLE_REF="example_node" ID="ID_1030668893" CREATED="1428329804457" MODIFIED="1428330283144"/>
</node>
<node TEXT="Apply" STYLE_REF="orange_node" ID="ID_1750215426" CREATED="1427375619849" MODIFIED="1428331169413">
<node TEXT="&lt;s:Button id=&quot;myButton&quot; styleName=&quot;myFontStyle&quot; label=&quot;Click Me&quot;/&gt;" STYLE_REF="example_node" ID="ID_1427431453" CREATED="1427375624119" MODIFIED="1428331169413"/>
</node>
<node TEXT="Descendent selectors" STYLE_REF="orange_node" ID="ID_85924380" CREATED="1428330322547" MODIFIED="1428330345160">
<node TEXT="s|Panel s|Scroller s|Button {&#xa;    color: #CCCCCC;&#xa;}&#xa;&#xa;s|Panel s|Button {&#xa;    color: #000000;&#xa;    chromeColor: #664232;&#xa;}&#xa;&#xa;s|Panel s|Scroller s|Button {&#xa;    chromeColor: 555555;&#xa;}" STYLE_REF="example_node" ID="ID_1372622919" CREATED="1428330363449" MODIFIED="1428330742647"/>
</node>
<node TEXT="Pseudo selectors" STYLE_REF="orange_node" ID="ID_770274821" CREATED="1428330561266" MODIFIED="1428330758310">
<node TEXT="Selector matches components based on an additional condition" STYLE_REF="pre_last_node" ID="ID_821671674" CREATED="1428330573915" MODIFIED="1428330747569">
<node TEXT="Condition may be dynamic" ID="ID_114926342" CREATED="1428330612066" MODIFIED="1428330618661"/>
</node>
<node TEXT="Used to apply styles to components only when they are in a specified state" STYLE_REF="pre_last_node" ID="ID_807292956" CREATED="1428330642170" MODIFIED="1428330747568"/>
<node TEXT="s|Button:up {&#xa;    color= #00FF00;&#xa;}&#xa;&#xa;s|Button:down {&#xa;    color: #CCCCCC;&#xa;}&#xa;&#xa;s|Panel s|Button:over {&#xa;    color: #000000;&#xa;    chromeColor: #FF0000;&#xa;}&#xa;&#xa;:up {&#xa;    color: #FF9933;&#xa;}" STYLE_REF="example_node" ID="ID_1082129821" CREATED="1428330671786" MODIFIED="1428330847582"/>
</node>
<node TEXT="Id selectors" STYLE_REF="orange_node" ID="ID_690244809" CREATED="1428330891226" MODIFIED="1428331000278">
<node TEXT="Selector matches components that meet an id condition" STYLE_REF="pre_last_node" ID="ID_5239097" CREATED="1428330902817" MODIFIED="1428331011191"/>
<node TEXT="Selector is tied one specific instance of a component" STYLE_REF="pre_last_node" ID="ID_1453181096" CREATED="1428330928955" MODIFIED="1428331011193"/>
<node TEXT="#submitButton {&#xa;    color: #CCCCCC;&#xa;}&#xa;...&#xa;&lt;s:Button id=&quot;submitButton&quot;&gt;" STYLE_REF="example_node" ID="ID_1944960051" CREATED="1428330950128" MODIFIED="1428331005744"/>
</node>
</node>
<node TEXT="Events" STYLE_REF="second_node" POSITION="left" ID="ID_498240460" CREATED="1427864138123" MODIFIED="1428050252661">
<edge COLOR="#7c7c00"/>
<hook URI="../../Images/event.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&lt;fx:Metadata&gt;&#xa;    &lt;!-- [Event(name=&quot;showPreview&quot;, type=&quot;flash.events.Event&quot;)] --&gt;&#xa;    [Event(name=&quot;showPreview&quot;, type=&quot;events.SomeEvent&quot;)]&#xa;&lt;/fx:Metadata&gt;&#xa;&lt;fx:Script&gt;&#xa;    &lt;![CDATA[&#xa;        protected function someHandler(event:MouseEvent):void {&#xa;            var eventObj:SomeEvent = new SomeEvent(&quot;showPreview&quot;,&#xa;                         employeeList.selectedItem, message.text);&#xa;            dispatchEvent(eventObj);&#xa;        }&#xa;    ]]&gt;&#xa;&lt;/fx:Script&gt;" STYLE_REF="example_node" ID="ID_480851174" CREATED="1427864328737" MODIFIED="1428050252660"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Choose.mxml
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&lt;components:Choose&#xa;    showPreview=&quot;showPreviewHandler(event)&quot;/&gt;" STYLE_REF="example_node" ID="ID_881043861" CREATED="1427864458927" MODIFIED="1427871061282"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Main.mxml
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Formatters" STYLE_REF="second_node" POSITION="right" ID="ID_38635926" CREATED="1427952059887" MODIFIED="1428047571257">
<edge COLOR="#ff0000"/>
<hook URI="../../Images/format.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Components used to format data into strings" STYLE_REF="pre_last_node" ID="ID_499425413" CREATED="1427952145400" MODIFIED="1427952411471"/>
<node TEXT="format() methods takse a value and returns a string" STYLE_REF="pre_last_node" ID="ID_878061495" CREATED="1427952157928" MODIFIED="1427952411472"/>
<node TEXT="Formatters" STYLE_REF="gray_node" ID="ID_1646766626" CREATED="1428036877236" MODIFIED="1428036900140">
<node TEXT="spark.formatters.Formatter" STYLE_REF="yellow_node" ID="ID_1140313207" CREATED="1427952194057" MODIFIED="1428036907154"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Use this when possible
    </p>
  </body>
</html>
</richcontent>
<node TEXT="DateTimeFormatter" STYLE_REF="pre_last_node" ID="ID_1201581865" CREATED="1427952229840" MODIFIED="1428036920202">
<hook NAME="FirstGroupNode"/>
</node>
<node TEXT="NumberFormatter" STYLE_REF="pre_last_node" ID="ID_1337715267" CREATED="1427952237728" MODIFIED="1428036920205"/>
<node TEXT="CurrencyFormatter" STYLE_REF="pre_last_node" ID="ID_476469259" CREATED="1427952243295" MODIFIED="1428036920205"/>
<node TEXT="Access the locales supported by the OS" ID="ID_968070552" CREATED="1427952375187" MODIFIED="1427952390267">
<hook NAME="SummaryNode"/>
</node>
</node>
<node TEXT="mx.formatters.Formatter" STYLE_REF="yellow_node" ID="ID_1010216668" CREATED="1427952248878" MODIFIED="1428036907155">
<node TEXT="CurrencyFormatter" STYLE_REF="pre_last_node" ID="ID_525393886" CREATED="1427952257240" MODIFIED="1428036920203"/>
<node TEXT="DateFormatter" STYLE_REF="pre_last_node" ID="ID_1395125375" CREATED="1427952263239" MODIFIED="1428036920203"/>
<node TEXT="NumberFormatter" STYLE_REF="pre_last_node" ID="ID_838023985" CREATED="1427952269625" MODIFIED="1428036920204"/>
<node TEXT="PhoneFormatter" STYLE_REF="pre_last_node" ID="ID_234621181" CREATED="1427952275521" MODIFIED="1428036920204"/>
<node TEXT="ZipCodeFormatter" STYLE_REF="pre_last_node" ID="ID_390548086" CREATED="1427952280793" MODIFIED="1428036920203"/>
</node>
</node>
<node TEXT="Formatter properties" STYLE_REF="gray_node" ID="ID_1299517866" CREATED="1428036776915" MODIFIED="1428385657888">
<node TEXT="CurrencyFormatter" STYLE_REF="yellow_node" ID="ID_352210282" CREATED="1428036952831" MODIFIED="1428385657888">
<node TEXT="currencySymbol" STYLE_REF="pre_last_node" ID="ID_591456700" CREATED="1428036786906" MODIFIED="1428036941618"/>
<node TEXT="decimalSeparator" STYLE_REF="pre_last_node" ID="ID_1845906684" CREATED="1428036809762" MODIFIED="1428036941619"/>
<node TEXT="locale" STYLE_REF="pre_last_node" ID="ID_1874330089" CREATED="1428036826743" MODIFIED="1428036941620"/>
<node TEXT="leadingZero" STYLE_REF="pre_last_node" ID="ID_1299399516" CREATED="1428036831748" MODIFIED="1428036941621"/>
<node TEXT="fractionalDigits" STYLE_REF="pre_last_node" ID="ID_837453575" CREATED="1428036836926" MODIFIED="1428036941621"/>
<node TEXT="useCurrencySymbol" STYLE_REF="pre_last_node" ID="ID_1720176772" CREATED="1428036843755" MODIFIED="1428036941622"/>
</node>
</node>
<node TEXT="Use" STYLE_REF="gray_node" ID="ID_1471104461" CREATED="1427952417549" MODIFIED="1428036900138">
<node TEXT="DataTimeFormatter" STYLE_REF="yellow_node" ID="ID_215099978" CREATED="1428036864316" MODIFIED="1428036968017">
<node TEXT="&lt;fx:Declarations&gt;&#xa;    &lt;s:DataTimeFormatter id=&quot;requestDataFormatter&quot;&#xa;        dataTimePattern=&quot;MM-dd-yy&quot;/&gt;&#xa;&lt;/fx:Declarations&gt;" STYLE_REF="example_node" ID="ID_509027035" CREATED="1428036944978" MODIFIED="1428037242690"/>
<node TEXT="private function dataChangeHandler(event:CalendarLayoutChangeEvent):void {&#xa;    requestDataFormatter.format(event.target.selectedDate.toDateString());&#xa;}" STYLE_REF="example_node" ID="ID_1436739805" CREATED="1428037134057" MODIFIED="1428037242694"/>
</node>
<node TEXT="PhoneFormatter" STYLE_REF="yellow_node" ID="ID_833291695" CREATED="1428037245307" MODIFIED="1428037262177">
<node TEXT="&lt;fx:Declarations&gt;&#xa;    &lt;s:PhoneFormatter id=&quot;phoneFieldFormatter&quot;&#xa;        formatString=&quot;(###) ###-####&quot;/&gt;&#xa;&lt;/fx:Declarations&gt;" STYLE_REF="example_node" ID="ID_323045968" CREATED="1428037263588" MODIFIED="1428037386091"/>
<node TEXT="private function mobilePhone_focusOutHandler(event:FocusEvent):void {&#xa;    mobilePhone.text = phoneFieldFormatter.format(mobilePhone.text);&#xa;    if (phoneFieldFormatter.error) {&#xa;        Alert.show(&quot;Phone format error:  &quot; + phoneFieldFormatter.error);&#xa;    }&#xa;}" STYLE_REF="example_node" ID="ID_780831951" CREATED="1428037323756" MODIFIED="1428037504650"/>
</node>
</node>
</node>
<node TEXT="Validators" STYLE_REF="second_node" POSITION="right" ID="ID_1242028746" CREATED="1428039576099" MODIFIED="1428047620589">
<edge COLOR="#0000ff"/>
<hook URI="../../Images/check.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Validators" STYLE_REF="gray_node" ID="ID_1673343655" CREATED="1428045346521" MODIFIED="1428045551497">
<node TEXT="spark.validator" STYLE_REF="yellow_node" ID="ID_990664519" CREATED="1428045365304" MODIFIED="1428045559680">
<node TEXT="NumberValidator" STYLE_REF="pre_last_node" ID="ID_911306626" CREATED="1428045375430" MODIFIED="1428045569686"/>
<node TEXT="CurrencyValidator" STYLE_REF="pre_last_node" ID="ID_1329137573" CREATED="1428045380399" MODIFIED="1428045569687"/>
</node>
<node TEXT="mx.validator" STYLE_REF="yellow_node" ID="ID_567198078" CREATED="1428045371104" MODIFIED="1428045559679">
<node TEXT="CreditCardValidator" STYLE_REF="pre_last_node" ID="ID_933236535" CREATED="1428045388892" MODIFIED="1428045569687"/>
<node TEXT="DateValidator" STYLE_REF="pre_last_node" ID="ID_466032936" CREATED="1428045395434" MODIFIED="1428045569687"/>
<node TEXT="EmailValidator" STYLE_REF="pre_last_node" ID="ID_904952365" CREATED="1428045399797" MODIFIED="1428045569688"/>
<node TEXT="NumberValidator" STYLE_REF="pre_last_node" ID="ID_962695240" CREATED="1428045405976" MODIFIED="1428045569688"/>
<node TEXT="PhoneNumberValidator" STYLE_REF="pre_last_node" ID="ID_1819032293" CREATED="1428045412488" MODIFIED="1428045569689"/>
<node TEXT="RegExpValidator" STYLE_REF="pre_last_node" ID="ID_1177736989" CREATED="1428045418408" MODIFIED="1428045569689"/>
<node TEXT="SocialSecurityValidator" STYLE_REF="pre_last_node" ID="ID_40038546" CREATED="1428045425698" MODIFIED="1428045569689"/>
<node TEXT="StringValidator" STYLE_REF="pre_last_node" ID="ID_470787392" CREATED="1428045432785" MODIFIED="1428045569690"/>
<node TEXT="ZipCodeValidator" STYLE_REF="pre_last_node" ID="ID_678186548" CREATED="1428045437654" MODIFIED="1428045569690"/>
</node>
</node>
<node TEXT="Creating" STYLE_REF="gray_node" ID="ID_173107315" CREATED="1428045604256" MODIFIED="1428045676261">
<node TEXT="Create on validator for each form control" STYLE_REF="pre_last_node" ID="ID_1135542782" CREATED="1428045608878" MODIFIED="1428045781417"/>
<node TEXT="Must be placed in the Declarations tag block" STYLE_REF="pre_last_node" ID="ID_1216840627" CREATED="1428045637696" MODIFIED="1428045781418"/>
<node TEXT="source - binding to the id of the object to validate" STYLE_REF="pre_last_node" ID="ID_1234002981" CREATED="1428045653697" MODIFIED="1428045781418"/>
<node TEXT="property - the name of the object&apos;s property to validate" STYLE_REF="pre_last_node" ID="ID_1545020916" CREATED="1428045678395" MODIFIED="1428045781418"/>
<node TEXT="required (def true) - a Booleand indicating a required value" STYLE_REF="pre_last_node" ID="ID_613122437" CREATED="1428045710997" MODIFIED="1428045781416"/>
<node TEXT="trigger - bind to the object that will trigger the validation" STYLE_REF="pre_last_node" ID="ID_75604085" CREATED="1428045892586" MODIFIED="1428046484097"/>
</node>
<node TEXT="Use" STYLE_REF="gray_node" ID="ID_1574574270" CREATED="1428045229418" MODIFIED="1428045240960">
<node TEXT="&lt;s:FormItem label=&quot;Mobile Phone:&quot;&#xa;    required=&quot;true&quot;&gt;&#xa;    &lt;s:helpContent&gt;&#xa;        &lt;s:Label text=&quot;ex. 555-555-5555&quot;&#xa;                baseline=&quot;24&quot;/&gt;&#xa;        &lt;s:Button label=&quot;?&quot;&#xa;                width=&quot;30&quot;&#xa;                x=&quot;120&quot;&#xa;                baseline=&quot;24&quot;/&gt;&#xa;    &lt;/s:helpContent&gt;&#xa;    &lt;s:TextInput id=&quot;mobilePhone&quot;/&gt;&#xa;&lt;/s:FromItem&gt;" STYLE_REF="example_node" ID="ID_661634157" CREATED="1428045029400" MODIFIED="1428046309967"/>
<node TEXT="Trigger via mxml" STYLE_REF="yellow_node" ID="ID_245581175" CREATED="1428046087860" MODIFIED="1428046304185">
<node TEXT="&lt;fx:Declarations&gt;&#xa;    &lt;mx:PhoneNumberValidator id=&quot;mobileValidator&quot;&#xa;        source=&quot;{mobilePhone}&quot;&#xa;        property=&quot;text&quot;&#xa;        trigger=&quot;{submitButton}&#xa;        triggerEvent=&quot;click&quot;/&gt;       &#xa;&lt;/fx:Declarations&gt;" STYLE_REF="example_node" ID="ID_1871927338" CREATED="1428045804104" MODIFIED="1428046339346"/>
</node>
<node TEXT="Trigger via actionscript" STYLE_REF="yellow_node" ID="ID_909996579" CREATED="1428046041873" MODIFIED="1428046304186">
<node TEXT="&lt;fx:Declarations&gt;&#xa;    &lt;mx:PhoneNumberValidator id=&quot;mobileValidator&quot;&#xa;        source=&quot;{mobilePhone}&quot;&#xa;        property=&quot;text&quot;&#xa;        triggerEvent=&quot;&quot;/&gt;&#xa;&#xa;    &lt;mx:PhoneNumberValidator id=&quot;officePhoneValidator&quot;&#xa;        source&quot;{phone}&quot;&#xa;        property=&quot;text&quot;&#xa;        triggerEvent=&quot;&quot;/&gt;&#xa;&lt;/fx:Declarations&gt;" STYLE_REF="example_node" ID="ID_722161180" CREATED="1428046146673" MODIFIED="1428046424325"/>
<node TEXT="&lt;s:FormItem&gt;&#xa;    &lt;s:Button id=&quot;submitButton&quot;&#xa;        label=&quot;Submit Request&quot;&#xa;        click=&quot;validatePhones()&quot;/&gt;&#xa;&lt;/s:FormItem&gt;" STYLE_REF="example_node" ID="ID_286378994" CREATED="1428046225313" MODIFIED="1428046339348"/>
<node TEXT="private functions validatePhones():void {&#xa;    // var event:ValidationResultEvent  = mobileValidator.validate();&#xa;    var validationArr:Array = Validator.validateAll([mobileValidator, officePhoneValidator]);&#xa;}" STYLE_REF="example_node" ID="ID_819775843" CREATED="1428046160744" MODIFIED="1428046502940"/>
</node>
</node>
</node>
<node TEXT="Item Renderers" STYLE_REF="second_node" POSITION="left" ID="ID_345434906" CREATED="1428050160494" MODIFIED="1428385421096">
<edge COLOR="#00cc00"/>
<hook URI="item-renderer.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Default" STYLE_REF="orange_node" ID="ID_1204927546" CREATED="1428050178479" MODIFIED="1428050205996">
<node TEXT="DefaultItemRenderer" STYLE_REF="yellow_node" ID="ID_1617685094" CREATED="1428050182309" MODIFIED="1428051108026">
<node TEXT="&lt;fx:Declarations&gt;&#xa;    &lt;s:ArrayList id=&quot;employeeList&quot;&gt;&#xa;        &lt;fx:String&gt;Albul Oleksandr&lt;/fx:String&gt;&#xa;        &lt;fx:String&gt;Albul Nadia&lt;/fx:String&gt;&#xa;        &lt;fx:String&gt;Igor Kuzmenko&lt;/fx:String&gt;&#xa;    &lt;/s:ArrayList&gt;&#xa;&lt;/fx:Declarations&gt;" STYLE_REF="example_node" ID="ID_1546344487" CREATED="1428050007526" MODIFIED="1428051108025"/>
<node TEXT="&lt;s:DataGroup dataProvider=&quot;{employeeList}&quot;&#xa;    itemRenderer=&quot;spark.skins.spark.DefaultItemRenderer&quot;&gt;&#xa;    &lt;s:layout&gt;&#xa;        &lt;s:VerticalLayout paddingLeft=&quot;25/&gt;&#xa;    &lt;/s:layout&gt;&#xa;&lt;/s:DataGroup&gt;" STYLE_REF="example_node" ID="ID_1060043927" CREATED="1428050263886" MODIFIED="1428050606100"/>
</node>
<node TEXT="DefaultComplexItemRenderer" STYLE_REF="yellow_node" ID="ID_1813964854" CREATED="1428050188719" MODIFIED="1428050825790">
<node TEXT="&lt;fx:Declarations&gt;&#xa;    &lt;s:ArrayList id=&quot;employeeList&quot;&gt;          &#xa;        &lt;s:BitmapImage source=&quot;images/ole.jpg&quot;/&gt;&#xa;        &lt;fx:String&gt;Albul Oleksandr&lt;/fx:String&gt;&#xa;        &lt;s:BitmapImage source=&quot;images/nad.jpg&quot;/&gt;&#xa;        &lt;fx:String&gt;Albul Nadia&lt;/fx:String&gt;&#xa;        &lt;s:BitmapImage source=&quot;images/ig.jpg&quot;/&gt;&#xa;        &lt;fx:String&gt;Igor Kuzmenko&lt;/fx:String&gt;&#xa;    &lt;/s:ArrayList&gt;&#xa;&lt;/fx:Declarations&gt;" STYLE_REF="example_node" ID="ID_496044273" CREATED="1428050007526" MODIFIED="1428050806539"/>
<node TEXT="&lt;s:DataGroup dataProvider=&quot;{employeeList}&quot;&#xa;    itemRendererFunction=&quot;rendererFunction&quot;&gt;&#xa;    &lt;s:layout&gt;&#xa;        &lt;s:VerticalLayout paddingLeft=&quot;25/&gt;&#xa;    &lt;/s:layout&gt;&#xa;&lt;/s:DataGroup&gt;" STYLE_REF="example_node" ID="ID_444034305" CREATED="1428050263886" MODIFIED="1428050636747"/>
<node TEXT="private function rendererFunction(item:Object):ClassFactory {&#xa;    if (item is String) {&#xa;        return new ClassFactory(DefaultItemRenderer);&#xa;    } else {&#xa;        return new ClassFactory(DefaultComplexItemRenderer);&#xa;    }&#xa;}" STYLE_REF="example_node" ID="ID_1382131856" CREATED="1428050638911" MODIFIED="1428050754412"/>
</node>
</node>
<node TEXT="Custom for DataGroup" STYLE_REF="orange_node" ID="ID_1085042412" CREATED="1428050262087" MODIFIED="1428123028343">
<node TEXT="&lt;fx:Declarations&gt;&#xa;    &lt;s:ArrayList id=&quot;employeeList&quot;&gt;&#xa;        &lt;fx:Object firstName=&quot;Samuel&quot;&#xa;            lastName=&quot;Ang&quot;          &#xa;            imageFile=&quot;images/sang.jpg&quot;/&gt;&#xa;&#xa;        &lt;fx:Object firstName=&quot;Athena&quot;&#xa;           lastName=&quot;Parker&quot;&#xa;           imageFile=&quot;images/aparker.jpg&quot;/&gt;&#xa;    &lt;/s:ArrayList&gt;&#xa;&lt;/fx:Declarations&gt;" STYLE_REF="example_node" ID="ID_1759099521" CREATED="1428051005333" MODIFIED="1428051786472"/>
<node TEXT="&lt;s:DataGroup dataProvider=&quot;{employeeList}&quot;&#xa;    itemRendererFunction=&quot;rendererFunction&quot;&gt;&#xa;    &lt;s:layout&gt;&#xa;        &lt;s:VerticalLayout paddingLeft=&quot;25/&gt;&#xa;    &lt;/s:layout&gt;&#xa;    &lt;s:itemRenderer&gt;&#xa;        &lt;fx:Component&gt;&#xa;            &lt;s:ItemRenderer&gt;&#xa;                &lt;s:states&gt;&#xa;                    &lt;s:State name=&quot;normal&quot;/&gt;&#xa;                &lt;/s:states&gt;&#xa;                &lt;s:BorderContainer&gt;&#xa;                    &lt;s:layout&gt;&#xa;                        &lt;s:VerticalLayout/&gt;&#xa;                    &lt;/s:layout&gt;&#xa;                    &lt;s:Label text=&quot;{data.firstName} {data.lastName}&quot;/&gt;&#xa;                    &lt;s:BitmapImage source=&quot;{data.imageFile}&quot;/&gt;&#xa;                &lt;/s:BorderContainer&gt;&#xa;            &lt;/s:ItemRenderer&gt;&#xa;        &lt;/fx:Component&gt;&#xa;    &lt;/s:itemRenderer&gt;&#xa;&lt;/s:DataGroup&gt;" STYLE_REF="example_node" ID="ID_1584316335" CREATED="1428051123992" MODIFIED="1428051786469"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Nested itemRenderer
    </p>
  </body>
</html>
</richcontent>
<node TEXT="&lt;s:Label text=&quot;{data.lastName}, {data.firstName}&quot;/&gt;" STYLE_REF="pre_last_node" ID="ID_1754674511" CREATED="1428051290127" MODIFIED="1428051799277"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Reference the data from the dataProvider property of the DataGroup
    </p>
    <p>
      container through data object
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="Custom for DataGrid" STYLE_REF="orange_node" ID="ID_644903759" CREATED="1428123042405" MODIFIED="1428124301192"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &amp; ItemEditor
    </p>
  </body>
</html>
</richcontent>
<node TEXT="&lt;s:GridItemRendrer xmlns:s...&#xa;                            width=&quot;100&quot; height=&quot;100&quot;&gt;&#xa;    &lt;s:VGroup horizontalAlign=&quot;center&quot; verticalAlign=&quot;middle&quot;&#xa;                    width=&quot;100%&quot; height=&quot;100%&quot;&gt;&#xa;        &lt;s:Label text=&quot;{data.firstName} {data.lastName}&quot;&#xa;        &lt;s:BitmapImage source=&quot;images/{data.id}.jpg&quot;/&gt;&#xa;    &lt;/s:VGroup&gt;&#xa;&lt;/s:GridItemRendrer&gt;" STYLE_REF="example_node" ID="ID_56684744" CREATED="1428123538929" MODIFIED="1428123828224"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      components.EmployeeDisplay
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="private function selectionChangeHandler(event:GridSelectionEvent):void {&#xa;    Alert.show(event.currentTarget.selectedItem.firstName);&#xa;}" STYLE_REF="example_node" ID="ID_1982760814" CREATED="1428124414451" MODIFIED="1428124492481"/>
<node TEXT="&lt;s:DataGrid dataProvider={employeeList}&quot;&#xa;                    alternatingRowColors=&quot;[#FFFFFF, #CCCCCC]&quot;&#xa;                    editable=&quot;true&quot;&#xa;                    selectionChange=&quot;selectionChangeHandler(event)&quot;&gt;&#xa;    &lt;s:typicalItem firstName=&quot;Christopher&quot;&#xa;                        lastName=&quot;Whinchester&quot;&#xa;                        hireDate=&quot;12/31/2011&quot;/&#xa;                        evaluation=&quot;Evaluation&quot;&gt;&#xa;    &lt;/s:typicalItem&gt;&lt;!-- for fit columns--&gt;&#xa;    &lt;s:columns&gt;&#xa;        &lt;s:ArrayList&gt;&#xa;            &lt;s:GridColumn itemRenderer=&quot;components.EmployeeDisplay&quot;&#xa;                                headerText=&quot;Name&quot;&#xa;                                editable=&quot;false&quot;/&gt;&#xa;            &lt;s:GridColumn dataField=&quot;hireDate&quot;&#xa;                                headerText=&quot;Hire Date&quot;&#xa;                                editable=&quot;false&quot;/&gt;&#xa;            &lt;s:GridColumn dataField=&quot;evaluation&quot;&#xa;                                headerText=&quot;Evaluation&quot;&gt;&#xa;            &lt;s:itemEditor&gt;&#xa;                &lt;fx:Component&gt;&#xa;                    &lt;s:GridItemEditor&gt;&#xa;                        &lt;s:Label text=&quot;{data.evaluation}&quot;&#xa;                                       fontWeight=&quot;bold&quot;/&gt;&#xa;                        &lt;s:NumericStepper value=&quot;@{value}&quot;&#xa;                                                    maximum=&quot;100&quot;&#xa;                                                    width=&quot;100%&quot;/&gt;   &#xa;                    &lt;/s:GridItemEditor&gt;&#xa;                &lt;/fx:Component&gt;&#xa;            &lt;/s:itemEditor&gt;&#xa;            &lt;/s:GridColumn&gt;&#xa;        &lt;/s:ArrayList&gt;&#xa;    &lt;/s:columns&gt;&#xa;&lt;/s:DataGrid&gt;" STYLE_REF="example_node" ID="ID_598699148" CREATED="1428052525167" MODIFIED="1428124411498"/>
</node>
</node>
<node TEXT="State machine" STYLE_REF="second_node" POSITION="left" ID="ID_1596957325" CREATED="1428208250485" MODIFIED="1428213565325">
<edge COLOR="#ff00ff"/>
<hook URI="../../Images/fsm.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="States" STYLE_REF="orange_node" ID="ID_2167391" CREATED="1428213262077" MODIFIED="1428213398597">
<node TEXT="Defining" STYLE_REF="yellow_node" ID="ID_978433684" CREATED="1428208327125" MODIFIED="1428213530122">
<node TEXT="&lt;s:states&gt;&#xa;    &lt;s:State name=&quot;portalState&quot;/&gt;&#xa;    &lt;s:State name=&quot;loginState&quot;/&gt;&#xa;&lt;/s:states&gt;" STYLE_REF="example_node" ID="ID_1451616140" CREATED="1428208335781" MODIFIED="1428208422916"/>
<node TEXT="The first state defined is the default state for the component" STYLE_REF="pre_last_node" ID="ID_1854949088" CREATED="1428208365817" MODIFIED="1428208429387"/>
<node TEXT="&lt;s:Application currentState=&quot;loginState&quot;&gt;&#xa;&lt;/s:Application&gt;" STYLE_REF="example_node" ID="ID_911332061" CREATED="1428208934053" MODIFIED="1428208966053"/>
</node>
<node TEXT="Use" STYLE_REF="yellow_node" ID="ID_725492318" CREATED="1428208532766" MODIFIED="1428213530123">
<node TEXT="&lt;s:Panel id=&quot;login&quot;&#xa;            includeIn=&quot;loginState, contactState&quot;&gt;&#xa;    &lt;s:Label text=&quot;Some Text&quot;/&gt;&#xa;    ...&#xa;    &lt;s:Button label=&quot;Submit&quot;&#xa;                    click=&quot;currentState=&apos;portalState&apos;&quot;/&gt;&#xa;&lt;/s:Panel&gt;" STYLE_REF="example_node" ID="ID_1939029786" CREATED="1428208535501" MODIFIED="1428209045633"/>
<node TEXT="&lt;s:Panel id=&quot;portal&quot;&#xa;            includeIn=&quot;portalState&quot;&gt;&#xa;    ....&#xa;&lt;/s:Panel&gt;" STYLE_REF="example_node" ID="ID_847030701" CREATED="1428208808022" MODIFIED="1428208852199"/>
<node TEXT="&lt;s:Button label=&quot;Logout&quot;&#xa;                excludeFrom=&quot;loginState, validationState&quot;&#xa;                color.portalState=&quot;#FF0000&quot; color.otherState=&quot;#00FF00&quot;&#xa;                click.portalState=&quot;logout()&quot;/&gt;" STYLE_REF="example_node" ID="ID_1721401034" CREATED="1428208710882" MODIFIED="1428209280881"/>
</node>
</node>
<node TEXT="Transitions" STYLE_REF="orange_node" ID="ID_1075969149" CREATED="1428212071685" MODIFIED="1428212174243">
<node TEXT="Use the (*) to refer to any state" STYLE_REF="pre_last_node" ID="ID_1146244518" CREATED="1428212193509" MODIFIED="1428213484443"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &lt;s:Transition fromState=&quot;*&quot;
    </p>
    <p>
      &#160;&#160;&#160;&#160;toState=&quot;portalState&quot;&gt;
    </p>
    <p>
      &lt;/s:Transition&gt;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Use AddAction/RemoveAction with defined targets to specify when&#xa;to add/remove the component during the transition" STYLE_REF="pre_last_node" ID="ID_1710048201" CREATED="1428213078269" MODIFIED="1428213484441"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &lt;s:AddAction target=&quot;{login}&quot;/&gt;
    </p>
    <p>
      &lt;s:RemoveAction targets=&quot;{[searchPanel, monthlyEventsPanel]}&quot;/&gt;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&lt;s:transitions&gt;&#xa;    &lt;s:Transition fromState=&quot;loginState&quot;&#xa;                        toState=&quot;portalState&quot;&#xa;                        autoReverse=&quot;true&quot;&gt;&#xa;        &lt;s:Sequence&gt;  &#xa;            &lt;s:Parallel&gt;&#xa;                &lt;s:Fade target=&quot;{login}&quot;/&gt;&#xa;                &lt;s:Move target=&quot;{login}&quot;&#xa;                            xTo=&quot;-266&quot;/&gt;&#xa;                &lt;s:Resize target=&quot;{empOfTheMonthPanel}&quot;&#xa;                                widthFrom=&quot;390&quot; widthTo=&quot;250&quot;/&gt;&#xa;                &lt;s:Move target=&quot;{empOfTheMonthPanel}&quot;&#xa;                            xFrom=&quot;298&quot; xTo=&quot;24&quot;/&gt;&#xa;            &lt;/s:Parallel&gt;  &#xa;            &lt;s:RemoveAction target=&quot;{login}&quot;/&gt;                  &#xa;            &lt;s:Parallel targets=&quot;{[searchPanel, cafeteriaSpecialPanel, monthlyEventsPanel]}&quot;&gt;&#xa;                &lt;s:AddAction/&gt;&#xa;                &lt;s:Fade/&gt;&#xa;                &lt;s:Move target=&quot;{searchPanel}&quot;&#xa;                            xFrom=&quot;-166&quot;/&gt;&#xa;                &lt;s:Move target=&quot;{cafeteriaSpecialPanel}&quot;&#xa;                            yFrom=&quot;-329&quot;/&gt;&#xa;                &lt;s:Move target=&quot;{monthlyEventsPanel}&quot;&#xa;                            xFrom=&quot;833&quot;/&gt;&#xa;            &lt;/s:Parallel&gt;&#xa;        &lt;/s:Sequence&gt;&#xa;    &lt;/s:Transition&gt;&#xa;&#xa;    &lt;s:Transition fromState=&quot;portalState&quot;&#xa;                        toState=&quot;loginState&quot;&gt;&#xa;        &lt;s:Sequence&gt;&#xa;            &lt;s:Parallel&gt;&#xa;                &lt;s:Fade targets=&quot;{[search, cafeteriaSpecial, monthlyEvents]}&quot;/&gt;&#xa;                &lt;s:Resize target=&quot;{employeeOfTheMonth}&quot;&#xa;                                widthFrom=&quot;250&quot; widthTo=&quot;390&quot;/&gt;&#xa;                &lt;s:Move target=&quot;{employeeOfTheMonth}&quot;&#xa;                                xFrom=&quot;24&quot; xTo=&quot;434&quot;/&gt;&#xa;            &lt;/s:Parallel&gt;&#xa;            &lt;s:RemoveAction targets=&quot;{[search, cafeteriaSpecial, monthlyEvents]}&quot;/&gt;&#xa;            &lt;s:AddAction target=&quot;{login}&quot;/&gt;&#xa;            &lt;s:Parallel target=&quot;{login}&quot;&gt;&#xa;                &lt;s:Fade/&gt;&#xa;            &lt;/s:Parallel&gt;&#xa;        &lt;/s:Sequence&gt;&#xa;    &lt;/s:Transition&gt;&#xa;&lt;/s:transitions&gt;" STYLE_REF="example_node" ID="ID_1570523801" CREATED="1428212156076" MODIFIED="1428213768544"/>
</node>
</node>
<node TEXT="Graphics" STYLE_REF="second_node" POSITION="right" ID="ID_75992004" CREATED="1428334914969" MODIFIED="1428385369875">
<edge COLOR="#006699"/>
<hook URI="../../Images/graphics.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Use" STYLE_REF="orange_node" ID="ID_435825605" CREATED="1428335044800" MODIFIED="1428335065324" VSHIFT="24">
<node TEXT="&lt;s:Rect left=&quot;0&quot; right=&quot;0&quot; top=&quot;0&quot; bottom=&quot;0&quot;&#xa;            radiusX=&quot;10&quot; radiusY=&quot;10&quot;&gt;&#xa;    &lt;s:fill&gt;&#xa;        &lt;s:SolidColor color=&quot;#CCCCCC&quot;/&gt;&#xa;    &lt;/s:fill&gt;&#xa;&lt;/s:Rect&gt;" STYLE_REF="example_node" ID="ID_1995982780" CREATED="1428334931990" MODIFIED="1428335062303"/>
</node>
</node>
</node>
</map>
