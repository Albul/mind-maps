<map version="freeplane 1.3.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Alchemy" STYLE_REF="main_node" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1425117938353" VGAP="14">
<hook NAME="AutomaticEdgeColor" COUNTER="6"/>
<hook URI="../../Images/alchemy-large.jpg" SIZE="1.0" NAME="ExternalObject"/>
<hook NAME="MapStyle" zoom="1.003">
    <properties show_icon_for_attributes="true" show_note_icons="true"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
<stylenode TEXT="main_node" BACKGROUND_COLOR="#ccccff">
<font SIZE="18" BOLD="true"/>
<edge STYLE="sharp_bezier" WIDTH="4"/>
</stylenode>
<stylenode TEXT="second_node" BACKGROUND_COLOR="#00cccc" STYLE="bubble">
<font SIZE="14"/>
</stylenode>
<stylenode TEXT="third_node" BACKGROUND_COLOR="#ffbdf4" STYLE="bubble">
<font SIZE="11" BOLD="true"/>
</stylenode>
<stylenode TEXT="orange_node" BACKGROUND_COLOR="#ff9900" STYLE="bubble">
<font BOLD="true"/>
</stylenode>
<stylenode TEXT="gray_node" BACKGROUND_COLOR="#9ec19e">
<font BOLD="true"/>
</stylenode>
<stylenode TEXT="yellow_node" BACKGROUND_COLOR="#ffff66" STYLE="bubble">
<edge WIDTH="1"/>
</stylenode>
<stylenode TEXT="example_node" BACKGROUND_COLOR="#afcdfa">
<edge STYLE="bezier" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="pre_last_node" BACKGROUND_COLOR="#99ff99" STYLE="bubble">
<edge STYLE="bezier" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="last_node" BACKGROUND_COLOR="#ffffff">
<edge STYLE="bezier" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="warning_node" BACKGROUND_COLOR="#fda6a6">
<icon BUILTIN="messagebox_warning"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<node TEXT="Compiling" STYLE_REF="second_node" POSITION="right" ID="ID_636583583" CREATED="1424963847194" MODIFIED="1425117938351">
<edge COLOR="#ff0000"/>
<hook URI="../../Images/compiling.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Output formats" STYLE_REF="orange_node" ID="ID_1784947196" CREATED="1424963853586" MODIFIED="1424963886247">
<node TEXT="Projectors" STYLE_REF="yellow_node" ID="ID_1607113501" CREATED="1424963862170" MODIFIED="1424963984373">
<node TEXT="By default FlasCC compiles the program and embeds it within a&#xa;copy of the AVM runtime so that it can run as a native command-line program" ID="ID_1278028809" CREATED="1424963926226" MODIFIED="1424964003645"/>
<node TEXT="~/flascc/sdk/usr/bin/gcc hello.c -o hello" STYLE_REF="pre_last_node" ID="ID_638614667" CREATED="1424963930306" MODIFIED="1424963991784"/>
</node>
<node TEXT="SWFs" STYLE_REF="yellow_node" ID="ID_657684524" CREATED="1424963891674" MODIFIED="1424963984368">
<node TEXT="The executable format suitable for use in the Flash Runtime" ID="ID_145865846" CREATED="1424963967963" MODIFIED="1424963973850"/>
<node TEXT="gcc -emit-swf hello.c -o hello.swf" STYLE_REF="pre_last_node" ID="ID_1850751603" CREATED="1424963950723" MODIFIED="1424963991778"/>
</node>
</node>
<node TEXT="Output byte formats" STYLE_REF="orange_node" ID="ID_1451614619" CREATED="1424964060539" MODIFIED="1424964183617">
<node TEXT="ActionScript bytecode" STYLE_REF="yellow_node" ID="ID_1152333174" CREATED="1424964077547" MODIFIED="1424964189962"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      ABC
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Default" ID="ID_1219156895" CREATED="1424964116500" MODIFIED="1424964118520"/>
<node TEXT=" Un-optimized" ID="ID_291327311" CREATED="1424964111244" MODIFIED="1424965350360">
<font BOLD="true"/>
</node>
<node TEXT="gcc test.c -c -o test.o" STYLE_REF="pre_last_node" ID="ID_1045644811" CREATED="1424964126004" MODIFIED="1424964205321"/>
</node>
<node TEXT="LLVM bytecode" STYLE_REF="yellow_node" ID="ID_1544238395" CREATED="1424964085860" MODIFIED="1424964189966">
<node TEXT="Full-optimized" ID="ID_822142627" CREATED="1424964136116" MODIFIED="1424965347770">
<font BOLD="true"/>
</node>
<node TEXT="For release" ID="ID_1463338872" CREATED="1424964156028" MODIFIED="1424964179377"/>
<node TEXT="gcc test.c -emit-llvm -c -o test.o" STYLE_REF="pre_last_node" ID="ID_1829178376" CREATED="1424964145092" MODIFIED="1424964205325"/>
<node TEXT="gcc test.c -O4 -c -o test.o" STYLE_REF="pre_last_node" ID="ID_266040152" CREATED="1424964153356" MODIFIED="1424964205326"/>
</node>
</node>
<node TEXT="Obfuscation" STYLE_REF="orange_node" ID="ID_682132301" CREATED="1424964923411" MODIFIED="1424965312581">
<node TEXT="-fllvm-opt-opt=-strip" STYLE_REF="yellow_node" ID="ID_1035090416" CREATED="1424965006251" MODIFIED="1424965316174"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Pass the param
    </p>
  </body>
</html>
</richcontent>
<node TEXT="It hides the names of any generated C/C++ functions" STYLE_REF="pre_last_node" ID="ID_1951171331" CREATED="1424965018251" MODIFIED="1424965321595"/>
<node TEXT="Any function names not listed in your exports.txt file will be renamed" STYLE_REF="pre_last_node" ID="ID_1018115930" CREATED="1424965033172" MODIFIED="1424965321599"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      form: __unnamed_N
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="GCC Command Line Options" STYLE_REF="orange_node" ID="ID_168318477" CREATED="1425036635376" MODIFIED="1425036799080">
<node TEXT="-disable-telemetry" STYLE_REF="pre_last_node" ID="ID_950686676" CREATED="1425036645491" MODIFIED="1425036792236">
<font BOLD="true" ITALIC="false"/>
<node TEXT="By default FlasCC generates SWFs that support Adobe Scout&apos;s advanced metrics. This means that stack trace information will be visible to anybody profiling the SWF." ID="ID_516629652" CREATED="1425036662416" MODIFIED="1425036663772"/>
</node>
<node TEXT="-emit-swc=" STYLE_REF="pre_last_node" ID="ID_1312384365" CREATED="1425036645491" MODIFIED="1425036792237">
<font BOLD="true" ITALIC="false"/>
<node TEXT="Emits a SWC that can be linked into a Flash Builder project or distributed for others to link into their own projects. You must specify the namespace that you want to use to replace the default com.adobe.flascc namespace, this lets you link multiple FlasCC-generated SWCs into a single project." ID="ID_942872658" CREATED="1425036671272" MODIFIED="1425036672228"/>
</node>
<node TEXT="-emit-swf" STYLE_REF="pre_last_node" ID="ID_690753355" CREATED="1425036645492" MODIFIED="1425036792237">
<font BOLD="true" ITALIC="false"/>
<node TEXT="Emit a SWF that can be executed by the Flash Runtime" ID="ID_829171255" CREATED="1425036678048" MODIFIED="1425036679372"/>
</node>
<node TEXT="-enable-debugger" STYLE_REF="pre_last_node" ID="ID_718974650" CREATED="1425036645494" MODIFIED="1425036792237">
<font BOLD="true" ITALIC="false"/>
<node TEXT="Generate SWF debug information and include it within the SWF. It isn&apos;t necessary to specify this option if you already compile your code with -g." ID="ID_1064487786" CREATED="1425036689122" MODIFIED="1425036689122"/>
</node>
<node TEXT="-fllvm-llc-opt=" STYLE_REF="pre_last_node" ID="ID_243863456" CREATED="1425036645494" MODIFIED="1425036792237">
<font BOLD="true" ITALIC="false"/>
<node TEXT="Pass an argument through to the final invocation of llc (LLVM-IR-&gt;ABC codegen). For more information on what options llc accepts read the LLVM documentation." ID="ID_1985418697" CREATED="1425036699976" MODIFIED="1425036704332"/>
</node>
<node TEXT="-fllvm-opt-opt=" STYLE_REF="pre_last_node" ID="ID_330758628" CREATED="1425036645495" MODIFIED="1425036792238">
<font BOLD="true" ITALIC="false"/>
<node TEXT="Pass an argument through to the final invocation of opt (LLVM-IR-&gt;LLVM-IR optimization). For more information on what options opt accepts read the LLVM documentation." ID="ID_1152170951" CREATED="1425036712040" MODIFIED="1425036712998"/>
</node>
<node TEXT="-flto-api=" STYLE_REF="pre_last_node" ID="ID_1946020615" CREATED="1425036645496" MODIFIED="1425036792238">
<font BOLD="true" ITALIC="false"/>
<node TEXT="Specifies a file containing the public API LLVM should preserve (implies that the optimizer is free to strip any symbols not mentioned in this file that aren&apos;t referenced by the symbols mentioned in this file)" ID="ID_777615414" CREATED="1425036717656" MODIFIED="1425036719237">
<node TEXT="# built in symbols that must always be preserved&#xa;_start1&#xa;malloc&#xa;free&#xa;memcpy&#xa;memmove&#xa;flascc_uiTickProc&#xa;_sync_synchronize" ID="ID_397903827" CREATED="1425036856376" MODIFIED="1425036858324"/>
</node>
</node>
<node TEXT="-no-swf-preloader" STYLE_REF="pre_last_node" ID="ID_741655018" CREATED="1425036645497" MODIFIED="1425036792238">
<font BOLD="true" ITALIC="false"/>
<node TEXT="By default FlasCC injects a simple preloader into your SWF, which displays a loading bar until the SWF has downloaded. Use this option to disable the preloader." ID="ID_1390756252" CREATED="1425036723664" MODIFIED="1425036725116"/>
</node>
<node TEXT="-swf-ns=" STYLE_REF="pre_last_node" ID="ID_718040031" CREATED="1425036645499" MODIFIED="1425036792238">
<font BOLD="true" ITALIC="false"/>
<node TEXT="Performs the same namespace re-writing as the -emit-swc= option when generating a SWF, by default no namespace rewriting takes place when generating a SWF." ID="ID_491448732" CREATED="1425036729688" MODIFIED="1425036731476"/>
</node>
<node TEXT="-swf-preloader=" STYLE_REF="pre_last_node" ID="ID_1384808135" CREATED="1425036645500" MODIFIED="1425036792238">
<font BOLD="true" ITALIC="false"/>
<node TEXT="Lets you use a custom SWF as the preloader. All of the SWF tags within the specified SWF will be included at the beginning of the generated SWF. To see a sample preloader, review sdk/usr/share/DefaultPreloader.as." ID="ID_508993240" CREATED="1425036736032" MODIFIED="1425036736604"/>
</node>
<node TEXT="-swf-size=WxH" STYLE_REF="pre_last_node" ID="ID_655397114" CREATED="1425036645501" MODIFIED="1425036792239">
<font BOLD="true" ITALIC="false"/>
<node TEXT="Specifies the initial width and height of the generated SWF." ID="ID_1264191472" CREATED="1425036748208" MODIFIED="1425036748868"/>
</node>
<node TEXT="-swf-version=" STYLE_REF="pre_last_node" ID="ID_994063327" CREATED="1425036645501" MODIFIED="1425036792239">
<font BOLD="true" ITALIC="false"/>
<node TEXT="Specifies the SWF version of the generated SWF. Remember that certain APIs will only be available to your code if your SWF version is high enough. By default the SWF version is 18." ID="ID_69286860" CREATED="1425036760592" MODIFIED="1425036761877"/>
</node>
<node TEXT="-symbol-abc=" STYLE_REF="pre_last_node" ID="ID_762540211" CREATED="1425036645502" MODIFIED="1425036792240">
<font BOLD="true" ITALIC="false"/>
<node TEXT="If you have compiled a custom Console.as you must point to the location of the ABC so that it can be used as the root sprite within the generated SWF." ID="ID_1710095181" CREATED="1425036766984" MODIFIED="1425036768029"/>
</node>
<node TEXT="-symbol-class=" STYLE_REF="pre_last_node" ID="ID_1555298650" CREATED="1425036645503" MODIFIED="1425036792240">
<font BOLD="true" ITALIC="false"/>
<node TEXT="If you have changed the namespace or name of the class within your Console.as implementation you must specify it here in the form &quot;:0:com.adobe.flascc::Console&quot; where &quot;com.adobe.flascc&quot; is the package name and &quot;Console&quot; is the class name." ID="ID_1773095306" CREATED="1425036773968" MODIFIED="1425036777069"/>
</node>
</node>
</node>
<node TEXT="Interop" STYLE_REF="second_node" POSITION="left" ID="ID_1939047582" CREATED="1425033740434" MODIFIED="1425035879050">
<edge COLOR="#0000ff"/>
<hook URI="../../Images/interop.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Inline assembly" STYLE_REF="orange_node" ID="ID_448885216" CREATED="1425033764278" MODIFIED="1425034133344"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      The language it supports natively is AS3
    </p>
  </body>
</html>
</richcontent>
<node TEXT="To pass arbitrary strings containing platform specific&#xa;instructions to the assembler to be included in your final executable" STYLE_REF="pre_last_node" ID="ID_130521452" CREATED="1425033838000" MODIFIED="1425034145532"/>
<node TEXT="FlasCC doesn&apos;t attempt to synthesize ActionScript classes from C++ classes" STYLE_REF="pre_last_node" ID="ID_1138750251" CREATED="1425034087167" MODIFIED="1425034848532"/>
<node TEXT="inline_as3" STYLE_REF="gray_node" ID="ID_17052770" CREATED="1425034274431" MODIFIED="1425034387207">
<node TEXT="#include &lt;AS3/AS3.h&gt;&#xa;&#xa;// Use AS3 to sqrt a double!&#xa;double somenumber = 45.0;&#xa;double result = 0.0;&#xa;inline_as3(&#xa;    &quot;%0 = Math.sqrt(%1);\n&quot;&#xa;    : &quot;=r&quot;(result) : &quot;r&quot;(somenumber)&#xa;);&#xa;&#xa;// Back in C we can take the result and print it out:&#xa;printf(&quot;sqrt of %f is %f\n&quot;, somenumber, result);" STYLE_REF="last_node" ID="ID_151091970" CREATED="1425033773663" MODIFIED="1425117300904"/>
<node TEXT="Allows" STYLE_REF="yellow_node" ID="ID_815669897" CREATED="1425033809791" MODIFIED="1425034138727">
<node TEXT="Injects code into the body of a generated function" STYLE_REF="pre_last_node" ID="ID_155805907" CREATED="1425034333064" MODIFIED="1425034370054"/>
<node TEXT="It can only be used for injecting additional&#xa;statements and expressions" STYLE_REF="pre_last_node" ID="ID_1829910851" CREATED="1425034357247" MODIFIED="1425034370047"/>
</node>
<node TEXT="Disadvantages" STYLE_REF="yellow_node" ID="ID_959859839" CREATED="1425034121874" MODIFIED="1425034138730">
<node TEXT="Tedious to write" STYLE_REF="pre_last_node" ID="ID_464339600" CREATED="1425033936165" MODIFIED="1425034145531"/>
<node TEXT="Not type-safe" STYLE_REF="pre_last_node" ID="ID_49946088" CREATED="1425033944648" MODIFIED="1425034145528"/>
</node>
</node>
<node TEXT="package_as3" STYLE_REF="gray_node" ID="ID_1302996690" CREATED="1425034271431" MODIFIED="1425034387205">
<node TEXT="package_as3(&quot;#package public\n var somepublicvar:int = 42;&quot;);&#xa;package_as3(&quot;#package private\n var someprivatevar:int = 42;&quot;);&#xa;package_as3(&quot;#package com.example.yourpackage\n var somespecialvar:int = 42;&quot;);" STYLE_REF="last_node" ID="ID_1187822915" CREATED="1425034545023" MODIFIED="1425117296936"/>
<node TEXT="Allows" STYLE_REF="yellow_node" ID="ID_1095429511" CREATED="1425034394464" MODIFIED="1425034449215">
<node TEXT="Inject global package-level" STYLE_REF="pre_last_node" ID="ID_888086088" CREATED="1425034404136" MODIFIED="1425034559212">
<node TEXT="Variables" ID="ID_971319133" CREATED="1425034425687" MODIFIED="1425034439477"/>
<node TEXT="Classes" ID="ID_418472350" CREATED="1425034427295" MODIFIED="1425034430187"/>
<node TEXT="Interfaces" ID="ID_303570274" CREATED="1425034431591" MODIFIED="1425034434843"/>
</node>
</node>
<node TEXT="By default" STYLE_REF="yellow_node" ID="ID_1464612880" CREATED="1425034477160" MODIFIED="1425034524896">
<node TEXT="It will inject its contents into the private package" STYLE_REF="pre_last_node" ID="ID_1462962745" CREATED="1425034481904" MODIFIED="1425034559214">
<node TEXT="Tthis can be changed using the &quot;#package&quot; directive within the inline asm" ID="ID_923304727" CREATED="1425034517983" MODIFIED="1425034519533"/>
</node>
</node>
</node>
</node>
<node TEXT="Flash++" STYLE_REF="orange_node" ID="ID_1206231189" CREATED="1425034764983" MODIFIED="1425034835800">
<node TEXT="Required" STYLE_REF="yellow_node" ID="ID_610552251" CREATED="1425034949916" MODIFIED="1425035059279">
<node TEXT="#include &lt;Flash++.h&gt;" STYLE_REF="pre_last_node" ID="ID_1243198078" CREATED="1425034969175" MODIFIED="1425035067217"/>
<node TEXT=" -lFlash++ -lAS3++" STYLE_REF="pre_last_node" ID="ID_1818803907" CREATED="1425035020681" MODIFIED="1425035067222"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Pass these compiler arguments to g++
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="AS3 wrapper interface generator" STYLE_REF="yellow_node" ID="ID_212064354" CREATED="1425035172872" MODIFIED="1425035245212"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      as3wig.jar
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Can generate C++ bindings for any AS3 classes" STYLE_REF="pre_last_node" ID="ID_1596666641" CREATED="1425035198272" MODIFIED="1425035258991"/>
<node TEXT="Provide writing in AS3 only with C++ syntax" STYLE_REF="pre_last_node" ID="ID_1166996476" CREATED="1425035207079" MODIFIED="1425035258988"/>
<node TEXT="flash::display::Sprite mySprite = flash::display::Sprite::_new();&#xa;flash::display::Graphics graphics = mySprite-&gt;graphics;&#xa;// draw  simple filled circle&#xa;graphics-&gt;beginFill(0xff00ff, 1.0);&#xa;graphics-&gt;drawCircle(0.0, 0.0, 50.0);&#xa;graphics-&gt;endFill();" STYLE_REF="last_node" ID="ID_701521291" CREATED="1425034883256" MODIFIED="1425117290014"/>
</node>
</node>
</node>
<node TEXT="Concurrency" STYLE_REF="second_node" POSITION="right" ID="ID_646111909" CREATED="1425035491351" MODIFIED="1425036592333">
<edge COLOR="#6d056d"/>
<hook URI="../../Images/concurrency.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Pthreads library" STYLE_REF="orange_node" ID="ID_112740282" CREATED="1425035526047" MODIFIED="1425035824010">
<node TEXT="It reflects to Worker IP in Flash Runtime" STYLE_REF="pre_last_node" ID="ID_473590524" CREATED="1425035531935" MODIFIED="1425036605717"/>
<node TEXT="When pthread_create to create a new C thread a new Flash Worker is setup that shares the FlasCC domainMemory ByteArray" STYLE_REF="pre_last_node" ID="ID_1034763025" CREATED="1425035547560" MODIFIED="1425036605721"/>
<node TEXT="It possible to compile and run existing C/C++ based multi-threaded code within the Flash Player without needing to modify the code" STYLE_REF="pre_last_node" ID="ID_1311468126" CREATED="1425035656912" MODIFIED="1425036605724"/>
</node>
<node TEXT="Primordial Worker" STYLE_REF="orange_node" ID="ID_1265364080" CREATED="1425035747512" MODIFIED="1425035824006">
<node TEXT="The first SWF that the Flash player runs" STYLE_REF="pre_last_node" ID="ID_918176312" CREATED="1425035762512" MODIFIED="1425036605726"/>
<node TEXT="It runs on the UI thread in the runtime" STYLE_REF="pre_last_node" ID="ID_1711010353" CREATED="1425035777743" MODIFIED="1425036605727"/>
<node TEXT="It has access to the full set of APIs that Flash exposes" STYLE_REF="pre_last_node" ID="ID_1841504553" CREATED="1425035786960" MODIFIED="1425036605727"/>
</node>
<node TEXT="APIs unsupported in background Workers" STYLE_REF="orange_node" ID="ID_169771235" CREATED="1425036074176" MODIFIED="1425036613923"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Any attempt to construct an instance of any of these
    </p>
    <p>
      will throw an IllegalOperationError
    </p>
  </body>
</html>
</richcontent>
<node TEXT="flash.desktop.Clipboard calling constructor will throw; calling generalClipboard will return null" ID="ID_57543621" CREATED="1425036449109" MODIFIED="1425036449109"/>
<node TEXT="flash.desktop.NativeDragManager isSupported returns false" ID="ID_734465334" CREATED="1425036449109" MODIFIED="1425036449109"/>
<node TEXT="flash.desktop.Updater isSupported returns false" ID="ID_725883287" CREATED="1425036449110" MODIFIED="1425036449110"/>
<node TEXT="flash.display.NativeMenu isSupported returns false" ID="ID_183330625" CREATED="1425036449110" MODIFIED="1425036449110"/>
<node TEXT="flash.display.NativeWindow isSupported returns false; ctor will throw; supportsMenu property false; supportsNotification property is false; supportsTransparency is false; systemMinSize is null; systemMaxSize is null;" ID="ID_430523285" CREATED="1425036449110" MODIFIED="1425036449110"/>
<node TEXT="flash.external.ExtensionContext createExtensionContext() will throw; getExtensionDirectory will throw;" ID="ID_1010630963" CREATED="1425036449110" MODIFIED="1425036449110"/>
<node TEXT="flash.external.ExternalInterface available returns false; objectID property is null;" ID="ID_1373856440" CREATED="1425036449110" MODIFIED="1425036449110"/>
<node TEXT="flash.html.* HTMLLoader.isSupported returns false" ID="ID_676948933" CREATED="1425036449110" MODIFIED="1425036449110"/>
<node TEXT="flash.media.Camera isSupported returns false; getCamera returns null; names property is null;" ID="ID_1698249362" CREATED="1425036449111" MODIFIED="1425036449111"/>
<node TEXT="flash.media.CameraRoll Mobile only. No changes made here for this version as workers are not supported on mobile." ID="ID_1632292456" CREATED="1425036449111" MODIFIED="1425036613920"/>
<node TEXT="flash.media.CameraUI Mobile only. No changes made here for this version as workers are not supported on mobile." ID="ID_1484217115" CREATED="1425036449111" MODIFIED="1425036449111"/>
<node TEXT="flash.media.StageWebView isSupported returns false; constructor will throw;" ID="ID_124323805" CREATED="1425036449112" MODIFIED="1425036449112"/>
<node TEXT="flash.net.drm.* DRMManager.isSupported returns false; getDRMManager() returns null;" ID="ID_1544664691" CREATED="1425036449112" MODIFIED="1425036449112"/>
<node TEXT="flash.printing.* PrintJob.isSupported returns false; constructor will throw; active is always false; printers is null; supportsPageSetupDialog is false;" ID="ID_1957076257" CREATED="1425036449112" MODIFIED="1425036449112"/>
<node TEXT="flash.security.XMLSignatureValidator isSupported returns false; calling constructor will throw." ID="ID_1693507662" CREATED="1425036449112" MODIFIED="1425036449112"/>
<node TEXT="flash.system.IME isSupported returns false; get conversionMode returns null; set conversionMode is a no op; get enabled returns false; set enabled is a no op; doConversion() is a no op; setCompositionString() is a no op." ID="ID_1207664882" CREATED="1425036449113" MODIFIED="1425036449113"/>
<node TEXT="flash.system.SystemUpdater calling constructor throws" ID="ID_551036580" CREATED="1425036449113" MODIFIED="1425036449113"/>
<node TEXT="flash.text.StageText calling constructor throws" ID="ID_895031404" CREATED="1425036449113" MODIFIED="1425036449113"/>
<node TEXT="flash.ui.ContextMenu isSupported returns false; calling constructor will throw" ID="ID_977531897" CREATED="1425036449114" MODIFIED="1425036449114"/>
<node TEXT="flash.ui.Mouse all methods are no-ops; setting &apos;cursor&apos; property is a no-op" ID="ID_203203381" CREATED="1425036449114" MODIFIED="1425036449114"/>
</node>
<node TEXT="APIs with behavioral changes in background Workers" STYLE_REF="orange_node" ID="ID_83610334" CREATED="1425036400200" MODIFIED="1425036406360">
<node TEXT="flash.accessibility.Accessibility" ID="ID_8076952" CREATED="1425036419304" MODIFIED="1425036419304">
<node TEXT="active &#x2014; always returns false" ID="ID_1652681626" CREATED="1425036419304" MODIFIED="1425036419304"/>
<node TEXT="updateProperties(); &#x2014; no-op" ID="ID_324956121" CREATED="1425036419304" MODIFIED="1425036419304"/>
</node>
<node TEXT="flash.desktop.NativeApplication" ID="ID_1714452364" CREATED="1425036419304" MODIFIED="1425036419304">
<node TEXT="properties" ID="ID_139812970" CREATED="1425036419304" MODIFIED="1425036419304">
<node TEXT="supportsDefaultApplication, supportsDockIcon, supportsMenu, supportsStartAtLogin, supportsSystemTrayIcon &#x2014; all return false" ID="ID_1694456131" CREATED="1425036419305" MODIFIED="1425036419305"/>
<node TEXT="activeWindow &#x2014; returns null" ID="ID_1363196715" CREATED="1425036419305" MODIFIED="1425036419305"/>
<node TEXT="autoExit &#x2014; setter is a no-op" ID="ID_1506242198" CREATED="1425036419305" MODIFIED="1425036419305"/>
<node TEXT="idleThreshold &#x2014; setter is a no-op" ID="ID_1688618812" CREATED="1425036419305" MODIFIED="1425036419305"/>
<node TEXT="openedWindows &#x2014; returns an empty Array []" ID="ID_1362700222" CREATED="1425036419305" MODIFIED="1425036419305"/>
<node TEXT="systemIdleMode &#x2014; setter is no-op" ID="ID_688880529" CREATED="1425036419305" MODIFIED="1425036419305"/>
</node>
<node TEXT="methods" ID="ID_1116499992" CREATED="1425036419305" MODIFIED="1425036419305">
<node TEXT="activate(), clear(), copy(), cut(), paste(), selectAll() &#x2014; no-op" ID="ID_1942819858" CREATED="1425036419305" MODIFIED="1425036419305"/>
<node TEXT="exit() &#x2014; forces this background worker to stop and shutdown" ID="ID_791470438" CREATED="1425036419305" MODIFIED="1425036419305"/>
</node>
<node TEXT="events" ID="ID_232274096" CREATED="1425036419305" MODIFIED="1425036419305">
<node TEXT="no events are supported &#x2014; background workers will not receive any NativeApplication events." ID="ID_863592739" CREATED="1425036419305" MODIFIED="1425036419305"/>
</node>
</node>
<node TEXT="flash.display.Stage" ID="ID_1702404238" CREATED="1425036419305" MODIFIED="1425036419305">
<node TEXT="allowsFullScreen &#x2014; always returns false" ID="ID_449766303" CREATED="1425036419305" MODIFIED="1425036419305"/>
<node TEXT="stage3Ds &#x2014; always returns empty Vector" ID="ID_1660835019" CREATED="1425036419305" MODIFIED="1425036419305"/>
<node TEXT="stageVideos &#x2014; always returns empty Vector" ID="ID_277830054" CREATED="1425036419305" MODIFIED="1425036419305"/>
<node TEXT="supportsOrientationChange &#x2014; always returns false" ID="ID_649369916" CREATED="1425036419305" MODIFIED="1425036419305"/>
<node TEXT="wmodeGPU &#x2014; always returns false" ID="ID_724093324" CREATED="1425036419305" MODIFIED="1425036419305"/>
</node>
<node TEXT="flash.filesystem.File" ID="ID_1097862084" CREATED="1425036419305" MODIFIED="1425036419305">
<node TEXT="browseForDirectory(), browseForOpen(), browseForOpenMultiple(), browseForSave() &#x2014; no-op" ID="ID_372290813" CREATED="1425036419305" MODIFIED="1425036419305"/>
</node>
<node TEXT="flash.net.FileReference" ID="ID_856383774" CREATED="1425036419305" MODIFIED="1425036419305">
<node TEXT="browse(), download(), save() &#x2014; no-op; always returns false" ID="ID_897454115" CREATED="1425036419305" MODIFIED="1425036419305"/>
</node>
<node TEXT="flash.net.FileReferenceList" ID="ID_1646228243" CREATED="1425036419305" MODIFIED="1425036419305">
<node TEXT="browse() &#x2014; no-op; always returns false" ID="ID_260375656" CREATED="1425036419306" MODIFIED="1425036419306"/>
</node>
<node TEXT="flash.system.System" ID="ID_430515998" CREATED="1425036419306" MODIFIED="1425036419306">
<node TEXT="ime &#x2014; always returns null" ID="ID_285512452" CREATED="1425036419306" MODIFIED="1425036419306"/>
<node TEXT="exit() &#x2014; forces this background worker to stop and shutdown" ID="ID_1973892790" CREATED="1425036419306" MODIFIED="1425036419306"/>
<node TEXT="setClipboard() &#x2014; no-op" ID="ID_1793302848" CREATED="1425036419306" MODIFIED="1425036419306"/>
</node>
</node>
</node>
<node TEXT="Debugging With GDB" STYLE_REF="second_node" POSITION="left" ID="ID_978455406" CREATED="1425039883529" MODIFIED="1425117755636">
<edge COLOR="#239797"/>
<hook URI="../../Images/bug-1.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Setting Up GDB" STYLE_REF="orange_node" ID="ID_1807232982" CREATED="1425039901834" MODIFIED="1425040189207">
<node TEXT="Set the FLASCC_GDB_RUNTIME environment variable" STYLE_REF="yellow_node" ID="ID_255648619" CREATED="1426081641275" MODIFIED="1426081660931">
<node TEXT="Setting the path to a standalone Flash Player Debugger" STYLE_REF="pre_last_node" ID="ID_697371480" CREATED="1426081750300" MODIFIED="1426081780251">
<node TEXT="Mac&gt; export FLASCC_GDB_RUNTIME=&quot;/Users/path/to/Flash Player.app&quot;&#xa;Win&gt; export FLASCC_GDB_RUNTIME=&quot;/cygdrive/path/to/FlashPlayerDebugger.exe&quot;" STYLE_REF="example_node" ID="ID_811245304" CREATED="1426081654409" MODIFIED="1426081699679"/>
</node>
<node TEXT="Setting the path to a web browser" STYLE_REF="pre_last_node" ID="ID_536233551" CREATED="1426081762158" MODIFIED="1426081780253">
<node TEXT="Mac&gt; export FLASCC_GDB_RUNTIME=&quot;/Applications/Safari.app&quot;&#xa;Win&gt; export FLASCC_GDB_RUNTIME=&quot;/cygdrive/path/to/iexplore.exe&quot;" STYLE_REF="example_node" ID="ID_1566468294" CREATED="1426081769113" MODIFIED="1426081783226"/>
</node>
</node>
<node TEXT="Set the ScriptStuckTimeout variable in mms.cfg" STYLE_REF="yellow_node" ID="ID_1477521460" CREATED="1426081803227" MODIFIED="1426081807470">
<node TEXT="Location of mms.cfg" STYLE_REF="pre_last_node" ID="ID_1733194564" CREATED="1426081854750" MODIFIED="1426081918521">
<node TEXT="C:\Windows\SysWow64\Macromed\Flash" ID="ID_736234778" CREATED="1426081864034" MODIFIED="1426081879010"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Windows 7 (64-bit)
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="C:\Windows\System32\Macromed\Flash" ID="ID_955065954" CREATED="1426081885504" MODIFIED="1426081907943"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Windows 7 (32-bit)
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="/Library/Application Support/Macromedia" ID="ID_797402394" CREATED="1426081891139" MODIFIED="1426081910511"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Mac OSX
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
<node TEXT="The Flash Player stops running programs if they take too long" STYLE_REF="pre_last_node" ID="ID_837245759" CREATED="1426081944150" MODIFIED="1426081954643"/>
<node TEXT="ScriptStuckTimeout=600" STYLE_REF="example_node" ID="ID_416577066" CREATED="1426081839001" MODIFIED="1426081960289"/>
</node>
</node>
<node TEXT="Compiling a Debug SWF" STYLE_REF="orange_node" ID="ID_1496971089" CREATED="1425039910025" MODIFIED="1425040189207">
<node TEXT="This produces un-optimized code with full debug info&#xa;so that GDB is able to fully inspect your data structures" STYLE_REF="pre_last_node" ID="ID_1730721157" CREATED="1425039966914" MODIFIED="1425040219809"/>
<node TEXT="~/flascc/sdk/usr/bin/gcc -g -O0 hello.c -emit-swf -o hello.swf" STYLE_REF="pre_last_node" ID="ID_858855313" CREATED="1425039920721" MODIFIED="1425040219812"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Mac
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="/cygdrive/c/flascc/sdk/usr/bin/gcc.exe -g -O0 hello.c -emit-swf -o hello.swf" STYLE_REF="pre_last_node" ID="ID_1762917554" CREATED="1425039931789" MODIFIED="1425040219812"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Win
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Running a SWF" STYLE_REF="orange_node" ID="ID_346367335" CREATED="1425039975829" MODIFIED="1425040189206">
<node TEXT="~/flascc/sdk/usr/bin/gdb hello.swf" STYLE_REF="pre_last_node" ID="ID_1957797635" CREATED="1425039991330" MODIFIED="1425040233247"/>
<node TEXT="/cygdrive/c/flascc/sdk/usr/bin/gdb.exe hello.swf" STYLE_REF="pre_last_node" ID="ID_1529869122" CREATED="1425040004413" MODIFIED="1425040233250"/>
<node TEXT="To start the content issue the run command" STYLE_REF="pre_last_node" ID="ID_1485996844" CREATED="1425040059667" MODIFIED="1425040233251"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      (gdb) run
    </p>
    <p>
      Starting program: helloWorld.swf
    </p>
    <p>
      0xdddddddd in ?? ()
    </p>
    <p>
      
    </p>
    <p>
      Breakpoint 1, 0xf0000030 in main (argc=0, argv=0x200ff0) at helloWorld.c:20
    </p>
    <p>
      20&#160;&#160;&#160;&#160;&#160;&#160;int s = 2;
    </p>
    <p>
      (gdb)
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Stepping Through Code" STYLE_REF="yellow_node" ID="ID_1381786533" CREATED="1425039999761" MODIFIED="1425040196686">
<node TEXT="step" STYLE_REF="pre_last_node" ID="ID_1265574259" CREATED="1425040121385" MODIFIED="1425040203614">
<node TEXT="Step over code" ID="ID_1751833954" CREATED="1425040145441" MODIFIED="1425040150015"/>
</node>
<node TEXT="next" STYLE_REF="pre_last_node" ID="ID_1946530549" CREATED="1425040124098" MODIFIED="1425040203615">
<node TEXT="Step into code" ID="ID_1873524063" CREATED="1425040151257" MODIFIED="1425040154606"/>
</node>
<node TEXT="Breakpoint 1, 0xf000007b in main (argc=0, argv=0x200ff0) at debuggingexample.c:28&#xa;28      int s = 2;&#xa;(gdb) step&#xa;29      int t = 1;&#xa;(gdb) next&#xa;32      for (i = 0; i &lt; 8; i++){&#xa;(gdb)" STYLE_REF="example_node" ID="ID_1788334683" CREATED="1425040178938" MODIFIED="1426081706853"/>
</node>
<node TEXT="Inspecting Data" STYLE_REF="yellow_node" ID="ID_716180786" CREATED="1425040461354" MODIFIED="1425040468225">
<node TEXT="info locals" STYLE_REF="pre_last_node" ID="ID_702313542" CREATED="1425040480842" MODIFIED="1425040527936">
<node TEXT="Will show you the values of all the C/C++ local variables" ID="ID_910524340" CREATED="1425040503626" MODIFIED="1425040508118"/>
</node>
<node TEXT="info args" STYLE_REF="pre_last_node" ID="ID_1020300763" CREATED="1425040490010" MODIFIED="1425040527935">
<node TEXT="Will show you the arguments to the current function" ID="ID_1941380201" CREATED="1425040515737" MODIFIED="1425040518247"/>
</node>
<node TEXT="#0  0xf0000086 in main (argc=0, argv=0x200ff0) at debuggingexample.c:33&#xa;33          t *= 2;&#xa;(gdb) info locals&#xa;s = 2&#xa;t = 2&#xa;i = 1&#xa;(gdb) info args&#xa;argc = 0&#xa;argv = 0x200ff0" STYLE_REF="example_node" ID="ID_615416787" CREATED="1425041473250" MODIFIED="1426081715726"/>
</node>
<node TEXT="Expression parser" STYLE_REF="yellow_node" ID="ID_260683660" CREATED="1425116931009" MODIFIED="1425117265326">
<node TEXT="Breakpoint 1, 0xf000007b in main (argc=0, argv=0x200ff0) at debuggingexample.c:28&#xa;28      int s = 2;&#xa;(gdb) step&#xa;29      int t = 1;&#xa;(gdb) call square(4)&#xa;$1 = 16&#xa;(gdb) call square(s)&#xa;$2 = 4" STYLE_REF="example_node" ID="ID_1590578534" CREATED="1425116967697" MODIFIED="1426081722098"/>
</node>
<node TEXT="AS3 Specific Commands" STYLE_REF="yellow_node" ID="ID_37599525" CREATED="1425117242641" MODIFIED="1425117265327">
<node TEXT="as3bt" STYLE_REF="pre_last_node" ID="ID_833540313" CREATED="1425117258539" MODIFIED="1425117431261">
<font BOLD="true"/>
<node TEXT="Shows the backtrace of the entire ActionScript stack. This displays mangled names for all of the currently executing C/C++ code along with the names of any ActionScript functions above them on the callstack" ID="ID_60773604" CREATED="1425117326273" MODIFIED="1425117328286"/>
</node>
<node TEXT="as3locals" STYLE_REF="pre_last_node" ID="ID_1523245349" CREATED="1425117258539" MODIFIED="1425117431264">
<font BOLD="true"/>
<node TEXT="Displays the values of ActionScript local variables. Within the stack frame for a C/C++ function you will see internal mangled variable names along with any AS3 variables defined using the AS3_DeclareVar macro" ID="ID_765052216" CREATED="1425117334306" MODIFIED="1425117356781"/>
</node>
<node TEXT="as3args" STYLE_REF="pre_last_node" ID="ID_1516424509" CREATED="1425117258540" MODIFIED="1425117431265">
<font BOLD="true"/>
<node TEXT="Displays the ActionScript arguments to the function. For a normal C/C++ function this displays nothing, but if you are within a function that was annotated using the AS3 interop function annotation syntax, it lists the arguments specified in the ActionScript signature for the function" ID="ID_482028340" CREATED="1425117340537" MODIFIED="1425117358558"/>
</node>
<node TEXT="set as3namespace" STYLE_REF="pre_last_node" ID="ID_852703111" CREATED="1425117258547" MODIFIED="1425117431266">
<font BOLD="true"/>
<node TEXT="When debugging a SWC (or a SWF compiled with -swf-ns) you must inform GDB what namespace you wish to debug, the default is com.adobe.flascc. This must be done before the run command is used" ID="ID_219364004" CREATED="1425117349201" MODIFIED="1425117354174"/>
</node>
<node TEXT="show as3namespace" STYLE_REF="pre_last_node" ID="ID_1359502350" CREATED="1425117258548" MODIFIED="1426081727298">
<font BOLD="true"/>
<node TEXT="Shows the namespace GDB is using to debug, the default is com.adobe.flasc" ID="ID_1726438855" CREATED="1425117362505" MODIFIED="1426081727297"/>
</node>
<node TEXT="as3sync" STYLE_REF="pre_last_node" ID="ID_180008113" CREATED="1425117258548" MODIFIED="1425117431267">
<font BOLD="true"/>
<node TEXT="Causes GDB to re-read all information about the state of the program without stepping" ID="ID_25948728" CREATED="1425117368761" MODIFIED="1425117413124"/>
</node>
<node TEXT="as3up" STYLE_REF="pre_last_node" ID="ID_1635567379" CREATED="1425117258549" MODIFIED="1425117431268">
<font BOLD="true"/>
<node TEXT="Switches to a higher ActionScript stack frame, which lets you inspect stack frames that might not be part of the compiled C/C++ code; for example the code within Console.as" ID="ID_228856754" CREATED="1425117374170" MODIFIED="1425117410509"/>
</node>
<node TEXT="as3down" STYLE_REF="pre_last_node" ID="ID_1476709468" CREATED="1425117258550" MODIFIED="1425117431269">
<font BOLD="true"/>
<node TEXT="Switches to a lower stack frame, this lets you inspect stack frames that might not be part of the compiled C/C++ code; for example the code within Console.as" ID="ID_1296750456" CREATED="1425117379898" MODIFIED="1425117381157"/>
</node>
<node TEXT="as3finish" STYLE_REF="pre_last_node" ID="ID_1542973170" CREATED="1425117258551" MODIFIED="1425117431270">
<font BOLD="true"/>
<node TEXT="Runs until the current ActionScript function exits" ID="ID_742431410" CREATED="1425117387985" MODIFIED="1425117407558"/>
</node>
<node TEXT="as3step" STYLE_REF="pre_last_node" ID="ID_1360598275" CREATED="1425117258551" MODIFIED="1425117431271">
<font BOLD="true"/>
<node TEXT="Steps over the currently executing instruction, but follows function calls. This might correspond to one C/C++ step or within an AS3 stack frame it might correspond to one ActionScript expression" ID="ID_910093019" CREATED="1425117393489" MODIFIED="1425117403656"/>
</node>
<node TEXT="as3next" STYLE_REF="pre_last_node" ID="ID_1853500038" CREATED="1425117258552" MODIFIED="1425117431272">
<font BOLD="true"/>
<node TEXT="Steps over the currently executing instruction without following function calls. This might correspond to one C/C++ step or within an AS3 stack frame it might correspond with one ActionScript expression" ID="ID_457023591" CREATED="1425117398441" MODIFIED="1425117400325"/>
</node>
</node>
</node>
</node>
<node TEXT="SWIG" STYLE_REF="second_node" POSITION="right" ID="ID_1335618625" CREATED="1425117919610" MODIFIED="1425119534128">
<edge COLOR="#9c9c08"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Tool that connects programs written in C/C++ with
    </p>
    <p>
      a variety of high-level programming languages
    </p>
  </body>
</html>
</richcontent>
<node TEXT="What for" STYLE_REF="orange_node" ID="ID_511014183" CREATED="1425117981209" MODIFIED="1425118249649">
<node TEXT="SWIG can automatically generate wrappers using the interop functionality" STYLE_REF="pre_last_node" ID="ID_1825810994" CREATED="1425117990273" MODIFIED="1425118264747"/>
<node TEXT="This is ideal when you have a large codebase that you want to&#xa;expose to ActionScript without writing lots of interop code by hand" STYLE_REF="pre_last_node" ID="ID_1721579640" CREATED="1425118005721" MODIFIED="1425118264745"/>
</node>
<node TEXT="Limitations" STYLE_REF="orange_node" ID="ID_1503837965" CREATED="1425118058121" MODIFIED="1425118249650">
<node TEXT="Where the source language (C/C++) supports constructs that&#xa;the target language (AS3 for FlasCC) does not support" STYLE_REF="yellow_node" ID="ID_381930152" CREATED="1425118129258" MODIFIED="1425118280984"/>
<node TEXT="Overloaded functions" STYLE_REF="yellow_node" ID="ID_190813634" CREATED="1425118170081" MODIFIED="1425118280988">
<hook NAME="FirstGroupNode"/>
</node>
<node TEXT="Operator overloading" STYLE_REF="yellow_node" ID="ID_1534242171" CREATED="1425118178546" MODIFIED="1425118280989"/>
<node TEXT="SWIG has support for detecting and renaming functions" STYLE_REF="pre_last_node" ID="ID_1575514084" CREATED="1425118213894" MODIFIED="1425118286473">
<hook NAME="SummaryNode"/>
<node TEXT="But some manual work is required to tell SWIG which functions to rename" ID="ID_26147530" CREATED="1425118239457" MODIFIED="1425118242469"/>
</node>
</node>
<node TEXT="Workflow" STYLE_REF="orange_node" ID="ID_298243628" CREATED="1425118300186" MODIFIED="1426081548533">
<node TEXT="Write a SWIG interface file" STYLE_REF="pre_last_node" ID="ID_399748610" CREATED="1425118341287" MODIFIED="1425118387409"/>
<node TEXT="Process the interface file with SWIG to produce&#xa;the ActionScript and C/C++ wrapper implementations" STYLE_REF="pre_last_node" ID="ID_107613366" CREATED="1425118341287" MODIFIED="1425118387413">
<node TEXT="Call" STYLE_REF="yellow_node" ID="ID_632570936" CREATED="1425119344323" MODIFIED="1425119415264">
<node TEXT="~/flascc/sdk/usr/bin/swig -as3 -outdir . -includeall -ignoremissing -o MyAPI_wrapper.c MyAPI.i" ID="ID_1844845165" CREATED="1425119352746" MODIFIED="1425119353910">
<node TEXT="-includeall instructs" STYLE_REF="pre_last_node" ID="ID_487391581" CREATED="1425119383226" MODIFIED="1425119477710">
<font BOLD="true"/>
<node TEXT="To follow all #include statements" ID="ID_29994554" CREATED="1425119397658" MODIFIED="1425119400014"/>
</node>
<node TEXT="-ignoremissing argument" STYLE_REF="pre_last_node" ID="ID_699944432" CREATED="1425119433202" MODIFIED="1425119477711">
<font BOLD="true"/>
<node TEXT="It should not stop when it is unable to locate an include file" ID="ID_1200005262" CREATED="1425119463706" MODIFIED="1425119468263"/>
</node>
</node>
</node>
<node TEXT="Output" STYLE_REF="yellow_node" ID="ID_1825207328" CREATED="1425119356170" MODIFIED="1425119415267">
<node TEXT="ActionScript interface" STYLE_REF="pre_last_node" ID="ID_1167995796" CREATED="1425118543937" MODIFIED="1425119421736">
<node TEXT="Defines the packages, functions, and classes that an ActionScript developer using the final SWC will be expected to use" ID="ID_27465484" CREATED="1425118571202" MODIFIED="1425118572902"/>
</node>
<node TEXT="C/C++ layer" STYLE_REF="pre_last_node" ID="ID_1929274149" CREATED="1425118547786" MODIFIED="1425119421735">
<node TEXT="Contains code that helps to bridge the gap between the ActionScript interface and the actual codebase. Typically this includes functions that perform the actual type conversion between C++ and ActionScript objects" ID="ID_28551880" CREATED="1425118598257" MODIFIED="1425118601887"/>
</node>
</node>
</node>
<node TEXT="Compile the ActionScript wrapper implementation into an ABC file" STYLE_REF="pre_last_node" ID="ID_1929472336" CREATED="1425118341289" MODIFIED="1426081548528"/>
<node TEXT="Compile the C/C++ wrapper implementation&#xa;into an LLVM bitcode or ABC object file" STYLE_REF="pre_last_node" ID="ID_1346748187" CREATED="1425118341294" MODIFIED="1425118387417"/>
<node TEXT="Perform your final link including both of&#xa;the compiled wrapper implementations" STYLE_REF="pre_last_node" ID="ID_1010700690" CREATED="1425118341295" MODIFIED="1425118387419"/>
</node>
<node TEXT="Typemaps" STYLE_REF="orange_node" ID="ID_257845793" CREATED="1425118820515" MODIFIED="1425119491278"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      How to map C/C++ types into the target language
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Default SWIG typemaps" STYLE_REF="yellow_node" ID="ID_603797032" CREATED="1425118886434" MODIFIED="1425119491278"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Can be found in sdk/usr/share/swig/2.0.4/as3
    </p>
  </body>
</html>
</richcontent>
<node ID="ID_1119830971" CREATED="1425118920674" MODIFIED="1425119195647"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <table border="0" style="width: 80%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 0; border-right-width: 0; border-bottom-width: 0; border-left-width: 0">
      <tr>
        <td valign="top" style="width: 33%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            <b>C Type </b>
          </p>
        </td>
        <td valign="top" style="width: 33%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            <b>ActionScript Type</b>
          </p>
        </td>
        <td valign="top" style="width: 33%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            <b>Notes</b>
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 33%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            char
          </p>
        </td>
        <td valign="top" style="width: 33%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            String
          </p>
        </td>
        <td valign="top" style="width: 33%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            AS3 has no char type
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 33%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            int
          </p>
        </td>
        <td valign="top" style="width: 33%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            int
          </p>
        </td>
        <td valign="top" style="width: 33%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 33%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            float
          </p>
        </td>
        <td valign="top" style="width: 33%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Number
          </p>
        </td>
        <td valign="top" style="width: 33%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            AS3 has no float type so all floats are promoted to double
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 33%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            double
          </p>
        </td>
        <td valign="top" style="width: 33%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Number
          </p>
        </td>
        <td valign="top" style="width: 33%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 33%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            char *
          </p>
        </td>
        <td valign="top" style="width: 33%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            String
          </p>
        </td>
        <td valign="top" style="width: 33%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 33%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            unsigned char
          </p>
        </td>
        <td valign="top" style="width: 33%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            String
          </p>
        </td>
        <td valign="top" style="width: 33%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 33%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            bool
          </p>
        </td>
        <td valign="top" style="width: 33%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Boolean
          </p>
        </td>
        <td valign="top" style="width: 33%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 33%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            function pointer
          </p>
        </td>
        <td valign="top" style="width: 33%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Function
          </p>
        </td>
        <td valign="top" style="width: 33%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 33%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            any other type
          </p>
        </td>
        <td valign="top" style="width: 33%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            *
          </p>
        </td>
        <td valign="top" style="width: 33%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            
          </p>
        </td>
      </tr>
    </table>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
</node>
</node>
</map>
