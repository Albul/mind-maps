<map version="freeplane 1.3.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Swiz" STYLE_REF="main_node" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1426093134783">
<hook NAME="AutomaticEdgeColor" COUNTER="11"/>
<hook NAME="MapStyle" zoom="1.103">
    <properties show_icon_for_attributes="true" show_note_icons="true"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
<stylenode TEXT="main_node" BACKGROUND_COLOR="#ccccff">
<font SIZE="18" BOLD="true"/>
<edge STYLE="sharp_bezier" WIDTH="4"/>
</stylenode>
<stylenode TEXT="second_node" BACKGROUND_COLOR="#00cccc" STYLE="bubble">
<font SIZE="14"/>
</stylenode>
<stylenode TEXT="third_node" BACKGROUND_COLOR="#ffbdf4" STYLE="bubble">
<font SIZE="11" BOLD="true"/>
</stylenode>
<stylenode TEXT="orange_node" BACKGROUND_COLOR="#ff9900" STYLE="bubble">
<font BOLD="true"/>
</stylenode>
<stylenode TEXT="gray_node" BACKGROUND_COLOR="#9ec19e">
<font BOLD="true"/>
</stylenode>
<stylenode TEXT="yellow_node" BACKGROUND_COLOR="#ffff66" STYLE="bubble">
<edge WIDTH="1"/>
</stylenode>
<stylenode TEXT="example_node" BACKGROUND_COLOR="#afcdfa">
<edge STYLE="bezier" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="pre_last_node" BACKGROUND_COLOR="#99ff99" STYLE="bubble">
<edge STYLE="bezier" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="last_node" BACKGROUND_COLOR="#ffffff">
<edge STYLE="bezier" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="warning_node" BACKGROUND_COLOR="#fda6a6">
<icon BUILTIN="messagebox_warning"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook URI="../../Images/swiz-large.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Inversion of control" STYLE_REF="second_node" POSITION="right" ID="ID_1763055719" CREATED="1425971840603" MODIFIED="1426090276052" VGAP="23" HGAP="46" VSHIFT="24">
<edge COLOR="#a90d0d"/>
<hook URI="../../Images/inversion.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="It is the BeanProvider class" STYLE_REF="pre_last_node" ID="ID_1090752770" CREATED="1425971877811" MODIFIED="1425971962300">
<icon BUILTIN="idea"/>
</node>
<node TEXT="In the BeanProvider you typically define non-view classes that you want registered with the framework and available as injection sources or targets" STYLE_REF="pre_last_node" ID="ID_1082681204" CREATED="1425971898873" MODIFIED="1425971962299">
<node TEXT="Models" ID="ID_1412617644" CREATED="1425971931642" MODIFIED="1425971934817"/>
<node TEXT="Controllers" ID="ID_567361910" CREATED="1425971939698" MODIFIED="1425971954968"/>
<node TEXT="Delegates" ID="ID_565917504" CREATED="1425971943863" MODIFIED="1425971952143"/>
<node TEXT="Services" ID="ID_1900427570" CREATED="1425971947587" MODIFIED="1425971950350"/>
</node>
<node TEXT="Beans are defined" STYLE_REF="orange_node" ID="ID_986793990" CREATED="1425971992007" MODIFIED="1425972560544">
<node TEXT="Regular in main MXML file" STYLE_REF="pre_last_node" ID="ID_1962039354" CREATED="1425971994901" MODIFIED="1425972125486">
<node TEXT="&lt;?xml version=&quot;1.0&quot; encoding=&quot;utf-8&quot;?&gt;&#xa;&lt;swiz:BeanProvider&#xa;    xmlns:swiz=&quot;http://swiz.swizframework.org&quot;&#xa;    xmlns:model=&quot;com.example.model.*&quot;&#xa;    xmlns:control=&quot;com.example.control.*&quot;&gt;&#xa; &#xa;    &lt;model:ApplicationModel id=&quot;applicationModel&quot; /&gt;&#xa;    &lt;control:UserController id=&quot;userController&quot; /&gt;&#xa; &#xa;&lt;/swiz:BeanProvider&gt;" STYLE_REF="example_node" ID="ID_595492394" CREATED="1425972001506" MODIFIED="1425972128757"/>
</node>
<node TEXT="Separate file Beans.mxml" STYLE_REF="pre_last_node" ID="ID_513633644" CREATED="1425972049971" MODIFIED="1425972125488">
<node TEXT="&lt;swiz:Swiz&gt;&#xa;    &lt;swiz:beanProviders&gt;&#xa;        &lt;swiz:BeanProvider&gt;&#xa;            &lt;swiz:Bean name=&quot;applicationModel&quot;&gt;&#xa;                &lt;model:ApplicationModel /&gt;&#xa;            &lt;/swiz:Bean&gt;&#xa;            &lt;swiz:Bean name=&quot;userController&quot;&gt;&#xa;                &lt;control:UserController /&gt;&#xa;            &lt;/swiz:Bean&gt;&#xa;        &lt;/swiz:BeanProvider&gt;&#xa;    &lt;/swiz:beanProviders&gt;&#xa; &#xa;    &lt;swiz:config&gt;&#xa;        &lt;swiz:SwizConfig&#xa;            eventPackages=&quot;com.foo.events, org.bar.events&quot;&#xa;            viewPackages=&quot;com.foo.views, org.bar.events&quot; /&gt;&#xa;    &lt;/swiz:config&gt;&#xa;&lt;/swiz:Swiz&gt;" STYLE_REF="example_node" ID="ID_1836322898" CREATED="1425972080121" MODIFIED="1425972128761"/>
</node>
</node>
<node TEXT="Prototype Beans" STYLE_REF="orange_node" ID="ID_917475635" CREATED="1425972145386" MODIFIED="1425972560542">
<node TEXT="Features" STYLE_REF="yellow_node" ID="ID_1105670117" CREATED="1425972512621" MODIFIED="1425972536958">
<node TEXT="Constructor injection" STYLE_REF="pre_last_node" ID="ID_718054943" CREATED="1425972268990" MODIFIED="1425972315785">
<node TEXT="You can pass up to 8 arguments to your bean&#x2019;s constructor" ID="ID_302750974" CREATED="1425972274292" MODIFIED="1425972278056"/>
</node>
<node TEXT="Unique copy for each injection target" STYLE_REF="pre_last_node" ID="ID_1234235398" CREATED="1425972285480" MODIFIED="1425972315784"/>
<node TEXT="Deferred instantiation" STYLE_REF="pre_last_node" ID="ID_1273127723" CREATED="1425972298521" MODIFIED="1425972315782">
<node TEXT="Prototype beans will not be created until they are needed for injection" ID="ID_1803969563" CREATED="1425972306532" MODIFIED="1425972310676"/>
</node>
</node>
<node TEXT="&lt;swiz:Prototype id=&quot;editViewPresoModel&quot;&#xa;    type=&quot;{ EditViewPresentationModel }&quot;&#xa;    constructorArguments=&quot;{ someOtherBean }&quot; /&gt;" STYLE_REF="example_node" ID="ID_1988426587" CREATED="1425972326062" MODIFIED="1425972332259"/>
</node>
<node TEXT="Externalizing Configuration Values" STYLE_REF="orange_node" ID="ID_939027077" CREATED="1425972447503" MODIFIED="1425972560544">
<node TEXT="Bean property values can also be defined in external XML files" STYLE_REF="pre_last_node" ID="ID_1226507546" CREATED="1425972629184" MODIFIED="1425972842632"/>
<node TEXT="SwizConfigValueLoader" STYLE_REF="pre_last_node" ID="ID_366459508" CREATED="1425972866180" MODIFIED="1425972911855"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#160;It is a Swiz extension for loading config files
    </p>
  </body>
</html>
</richcontent>
<node TEXT="[Inject]&#xa;public var configLoader : SwizConfigValueLoader;" STYLE_REF="example_node" ID="ID_73467968" CREATED="1425972921855" MODIFIED="1425972981275"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Can be injected just like any other bean
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="[EventHandler( event = &quot;ConfigLoaderEvent.CONFIG_LOAD_COMPLETE&quot;, properties = &quot;configData&quot; )]&#xa;public function onConfigLoadComplete( configData : Object ) : void {&#xa;    // You can now reference the value with syntax such as:&#xa;    // configData.config.value1, configData.config.value2, etc.&#xa;}" STYLE_REF="example_node" ID="ID_1921256225" CREATED="1425972955366" MODIFIED="1425972985856"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Event handler to execute after the external data is loaded
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Loading a config file in MXML file" STYLE_REF="pre_last_node" ID="ID_73054393" CREATED="1425972641159" MODIFIED="1425973003994" VGAP="22">
<node TEXT="&lt;config&gt;&#xa;    &lt;value1&gt;This is value 1.&lt;/value1&gt;&#xa;    &lt;value2&gt;This is value 2.&lt;/value2&gt;&#xa;&lt;/config&gt;" STYLE_REF="example_node" ID="ID_619298196" CREATED="1425972648937" MODIFIED="1425972850982"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Config file
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&lt;externalconfig:SwizConfigValueLoader id=&quot;configLoader&quot; source=&quot;config.xml&quot; /&gt;&#xa;&lt;controller:MyController id=&quot;myController&quot;&#xa;                         value1=&quot;{configLoader.configData.config.value1}&quot;&#xa;                         value2=&quot;{configLoader.configData.config.value2}&quot; /&gt;" STYLE_REF="example_node" ID="ID_1626038777" CREATED="1425972687361" MODIFIED="1425972847604"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Can be loaded and used in a BeanProvider like this
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
</node>
<node TEXT="Dependency Injection" STYLE_REF="second_node" POSITION="left" ID="ID_70025832" CREATED="1425994401113" MODIFIED="1426080504178" HGAP="40" VSHIFT="-10">
<edge COLOR="#010188"/>
<hook URI="../../Images/injection.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Any beans defined in a registered BeanProvider can be injected&#xa;into other registered beans or into views added to the display list" STYLE_REF="pre_last_node" ID="ID_1391302040" CREATED="1425994447170" MODIFIED="1425994460805">
<icon BUILTIN="idea"/>
<node TEXT="" ID="ID_1596209310" CREATED="1426082151642" MODIFIED="1426082155994">
<hook URI="IoC.jpg" SIZE="1.0" NAME="ExternalObject"/>
</node>
</node>
<node TEXT="Injections occur after the CREATION_COMPLETE event" STYLE_REF="pre_last_node" ID="ID_924849578" CREATED="1426057321233" MODIFIED="1426057334957"/>
<node TEXT="Injection scenarios" STYLE_REF="orange_node" ID="ID_694565827" CREATED="1425994484034" MODIFIED="1426058455302">
<node TEXT="Inject Model into View" STYLE_REF="pre_last_node" ID="ID_164224795" CREATED="1425994494397" MODIFIED="1425994503640"/>
<node TEXT="Inject Model into Controller" STYLE_REF="pre_last_node" ID="ID_1638002456" CREATED="1425994494397" MODIFIED="1425994503643"/>
<node TEXT="Inject Delegate into Controller" STYLE_REF="pre_last_node" ID="ID_1758088363" CREATED="1425994494398" MODIFIED="1425994503645"/>
<node TEXT="Inject Service into Delegate" STYLE_REF="pre_last_node" ID="ID_1764892264" CREATED="1425994494398" MODIFIED="1425994503646"/>
</node>
<node TEXT="Inject by" STYLE_REF="orange_node" ID="ID_660904568" CREATED="1426057347222" MODIFIED="1426058455301" VGAP="18">
<node TEXT="Type" STYLE_REF="gray_node" ID="ID_1476321791" CREATED="1426057358888" MODIFIED="1426058312998">
<node TEXT="If you define exactly one bean of a given type you can&#xa;inject it into a matching destination with a plain [Inject] tag" STYLE_REF="pre_last_node" ID="ID_889295211" CREATED="1426057379425" MODIFIED="1426058350674"/>
<node TEXT="[Inject]&#xa;public var model:IUserModel;" STYLE_REF="example_node" ID="ID_1744960088" CREATED="1426057389841" MODIFIED="1426058346315"/>
</node>
<node TEXT="Name" STYLE_REF="gray_node" ID="ID_789355819" CREATED="1426058241910" MODIFIED="1426058251297">
<node TEXT="If you have more than one bean of a given type defined you&#xa;can request that a bean with a specific name be injected" STYLE_REF="pre_last_node" ID="ID_1201285061" CREATED="1426058280087" MODIFIED="1426058350660"/>
<node TEXT="This bean name must match the id attribute of&#xa;the bean&apos;s tag as defined in your BeanProvide" STYLE_REF="pre_last_node" ID="ID_1139250288" CREATED="1426058300741" MODIFIED="1426058350644"/>
<node TEXT="[Inject( source=&quot;userService&quot; )]&#xa;public var service:RemoteObject;" STYLE_REF="example_node" ID="ID_1703340678" CREATED="1426058325592" MODIFIED="1426058346308">
<node TEXT="[Inject( &quot;userService&quot; )]&#xa;public var service:RemoteObject;" STYLE_REF="example_node" ID="ID_69952004" CREATED="1426058369441" MODIFIED="1426058388169"/>
</node>
<node TEXT="// will inject this from BeanProvider&#xa;&lt;mx:RemoteObject id=&quot;userService&quot; /&gt;" STYLE_REF="example_node" ID="ID_334291238" CREATED="1426058337738" MODIFIED="1426058346299"/>
</node>
</node>
<node TEXT="Setter injection" STYLE_REF="orange_node" ID="ID_478053496" CREATED="1426058418156" MODIFIED="1426058455300">
<node TEXT="Swiz can also inject values into single argument methods" STYLE_REF="pre_last_node" ID="ID_1373419687" CREATED="1426058433192" MODIFIED="1426058461605"/>
<node TEXT="[Inject]&#xa;public function setModel( model:UserModel ):void {&#xa;    this.model = model;&#xa;}" STYLE_REF="example_node" ID="ID_599683576" CREATED="1426058440155" MODIFIED="1426058470899"/>
</node>
<node TEXT="Inject bean property" STYLE_REF="orange_node" ID="ID_1636061411" CREATED="1426058484646" MODIFIED="1426058596827">
<node TEXT="Swiz can also inject public bean properties" STYLE_REF="pre_last_node" ID="ID_1967192322" CREATED="1426058506242" MODIFIED="1426058601315">
<node TEXT="[Inject( source=&quot;userModel.currentUser&quot; )]&#xa;public var currentUser : User;" STYLE_REF="example_node" ID="ID_1617681795" CREATED="1426058587705" MODIFIED="1426058593681"/>
</node>
<node TEXT="You can also inject nested properties" STYLE_REF="pre_last_node" ID="ID_1347686504" CREATED="1426058648393" MODIFIED="1426058658107">
<node TEXT="[Inject( source=&quot;userModel.currentUser.firstName&quot; )]&#xa;public var firstName : String;" STYLE_REF="example_node" ID="ID_578655060" CREATED="1426058636933" MODIFIED="1426058704130"/>
</node>
<node TEXT="If the bean property is bindable and the decorated&#xa;property is public, you can have Swiz create a binding" STYLE_REF="pre_last_node" ID="ID_1274782117" CREATED="1426058674810" MODIFIED="1426058700115">
<node TEXT="[Inject( source=&quot;userModel.currentUser&quot;, bind=&quot;true&quot; )]&#xa;public var currentUser : User;" STYLE_REF="example_node" ID="ID_1723064156" CREATED="1426058694154" MODIFIED="1426058704135"/>
</node>
</node>
<node TEXT="Two way bindings" STYLE_REF="orange_node" ID="ID_1506795598" CREATED="1426058856647" MODIFIED="1426058895340">
<node TEXT="To set up a reverse binding, so the bean property&#xa;will receive updates made to the injection target" STYLE_REF="pre_last_node" ID="ID_531233450" CREATED="1426058878170" MODIFIED="1426058902639"/>
<node TEXT="[Bindable]&#xa;[Inject( source=&quot;userModel.currentUser&quot;, twoWay=&quot;true&quot; )]&#xa;public var currentUser:User;" STYLE_REF="example_node" ID="ID_399873939" CREATED="1426058888462" MODIFIED="1426058906785"/>
</node>
<node TEXT="Injection destination" STYLE_REF="orange_node" ID="ID_1319524943" CREATED="1426059091605" MODIFIED="1426059137570">
<node TEXT="This is especially true in MXML views, where decorating&#xa;a child component&apos;s property is not possible" STYLE_REF="pre_last_node" ID="ID_1361866208" CREATED="1426059113700" MODIFIED="1426059159736"/>
<node TEXT="[Inject( source=&quot;userModel.currentMode&quot;,&#xa;destination=&quot;modeViewStack.selectedIndex&quot; )]" STYLE_REF="example_node" ID="ID_598148275" CREATED="1426059130126" MODIFIED="1426059152702"/>
<node TEXT="Take care to ensure that the injection destination will be&#xa;created and available by the time your injections are processed" ID="ID_950007255" CREATED="1426059175516" MODIFIED="1426059291496" BACKGROUND_COLOR="#fda6a6">
<icon BUILTIN="messagebox_warning"/>
<edge STYLE="bezier" WIDTH="thin"/>
</node>
</node>
</node>
<node TEXT=" Bean Life Cycle Management" STYLE_REF="second_node" POSITION="right" ID="ID_533130505" CREATED="1426059836906" MODIFIED="1426081153698" HGAP="68" VSHIFT="70">
<edge COLOR="#006c00"/>
<hook URI="../../Images/life-cycle-2.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Manually Creating and Destroying Beans" STYLE_REF="orange_node" ID="ID_1865645749" CREATED="1426061131743" MODIFIED="1426061137827">
<node TEXT="Create" STYLE_REF="yellow_node" ID="ID_496289664" CREATED="1426061151591" MODIFIED="1426061211339">
<node TEXT="BeanEvent.SET_UP_BEAN" STYLE_REF="pre_last_node" ID="ID_76839726" CREATED="1426061161593" MODIFIED="1426061571717">
<font BOLD="true"/>
</node>
<node TEXT="BeanEvent.ADD_BEAN" STYLE_REF="pre_last_node" ID="ID_1605893786" CREATED="1426061168887" MODIFIED="1426061571716">
<font BOLD="true"/>
<node TEXT="Will also add the bean as a singleton to the BeanFactory cache" ID="ID_740087589" CREATED="1426061202858" MODIFIED="1426061206609"/>
</node>
<node TEXT="[Dispatcher]&#xa;public var dispatcher : IEventDispatcher;&#xa; &#xa;private function createNewBean() : void {&#xa;    userModel : UserModel = new UserModel();&#xa;    dispatcher.dispatchEvent(new BeanEvent(BeanEvent.SET_UP_BEAN, userModel));&#xa;}" STYLE_REF="example_node" ID="ID_150038928" CREATED="1426061177213" MODIFIED="1426061321542">
<node TEXT="Swiz will create a bean for the userModel,&#xa;and process any metadata in it" ID="ID_1740785104" CREATED="1426061308228" MODIFIED="1426061334220"/>
</node>
<node TEXT="setUpBean();" STYLE_REF="pre_last_node" ID="ID_1858605641" CREATED="1426061470220" MODIFIED="1426144687009">
<node TEXT="Method on the BeanFactory, not preferred" ID="ID_947273583" CREATED="1426061514057" MODIFIED="1426144687009"/>
</node>
</node>
<node TEXT="Destroy" STYLE_REF="yellow_node" ID="ID_394669951" CREATED="1426061227328" MODIFIED="1426061234605">
<node TEXT="BeanEvent.TEAR_DOWN_BEAN" STYLE_REF="pre_last_node" ID="ID_1460981601" CREATED="1426061243135" MODIFIED="1426061571715">
<font BOLD="true"/>
<node TEXT="Cleans up the target by removing injections" ID="ID_955282015" CREATED="1426061391117" MODIFIED="1426061394057"/>
</node>
<node TEXT="BeanEvent.REMOVE_BEAN" STYLE_REF="pre_last_node" ID="ID_420035676" CREATED="1426061251358" MODIFIED="1426061571706">
<font BOLD="true"/>
<node TEXT="Cleans up the bean as well as removing it&#xa;from the singleton cache in the BeanFactory" ID="ID_1264721093" CREATED="1426061403878" MODIFIED="1426061411473"/>
</node>
<node TEXT="private function destroyBean() : void {&#xa;    dispatcher.dispatchEvent(new BeanEvent(BeanEvent.TEAR_DOWN_BEAN, userModel));&#xa;}" STYLE_REF="example_node" ID="ID_270739981" CREATED="1426061272442" MODIFIED="1426061353827">
<node TEXT="Swiz will destroy the userModel bean, clean up&#xa;any injected objects, and delete any injection bindings" ID="ID_1232905975" CREATED="1426061288082" MODIFIED="1426061297846"/>
</node>
<node TEXT="tearDownBean();" STYLE_REF="pre_last_node" ID="ID_1765560254" CREATED="1426061481919" MODIFIED="1426061491603">
<node TEXT="Method on the BeanFactory, not preferred" ID="ID_889547645" CREATED="1426061514057" MODIFIED="1426061552285"/>
</node>
</node>
</node>
<node TEXT="[PostConstruct] and [PreDestroy]" STYLE_REF="orange_node" ID="ID_608780300" CREATED="1426066517286" MODIFIED="1426066589197">
<node TEXT="[PostConstruct]" STYLE_REF="yellow_node" ID="ID_1262547961" CREATED="1426066524397" MODIFIED="1426066677027">
<font BOLD="true"/>
<node TEXT="[PostConstruct] methods are invoked after the bean has&#xa;been set up, had dependencies injected, and had mediators created" STYLE_REF="pre_last_node" ID="ID_1514028454" CREATED="1426066547926" MODIFIED="1426066650788"/>
<node TEXT="package org.swizframework.quickswiz.controller {&#xa;    import org.swizframework.quickswiz.service.UserService;&#xa; &#xa;    public class UserController {&#xa;        [Inject]&#xa;        public var userService : UserService;&#xa; &#xa;        [PostConstruct]&#xa;        public function createDefaultUser() : void {&#xa;            userService.loadDefaultUser();&#xa;        }&#xa;    }&#xa;}" STYLE_REF="example_node" ID="ID_1312499919" CREATED="1426066565233" MODIFIED="1426068108059">
<font BOLD="false"/>
</node>
</node>
<node TEXT="[PreDestroy]" STYLE_REF="yellow_node" ID="ID_1495643157" CREATED="1426066661578" MODIFIED="1426066677025">
<font BOLD="true"/>
<node TEXT="[PreDestroy] will be called when a bean is destroyed by Swiz" STYLE_REF="pre_last_node" ID="ID_1826952673" CREATED="1426066701563" MODIFIED="1426066758643">
<node TEXT="This would happen if a UI component is removed&#xa;from the stage, or a module is unloaded" ID="ID_1747950510" CREATED="1426066713879" MODIFIED="1426066723703"/>
</node>
<node TEXT="package org.swizframework.quickswiz.controller {&#xa;    import org.swizframework.quickswiz.service.UserService;&#xa; &#xa;    public class UserController {&#xa;        [Inject]&#xa;        public var userService : UserService;&#xa; &#xa;        [PreDestroy]&#xa;        public function clearPollingTimer() : void {&#xa;            userService.stopPolling();&#xa;        }&#xa;    }&#xa;}" STYLE_REF="example_node" ID="ID_1175726733" CREATED="1426066730329" MODIFIED="1426066755127"/>
</node>
</node>
<node TEXT="Swiz and Flex Life Cycle Steps" STYLE_REF="orange_node" ID="ID_507068586" CREATED="1426067250463" MODIFIED="1426067633583">
<node TEXT="The steps that Flex and Swiz will go through" STYLE_REF="yellow_node" ID="ID_1739419724" CREATED="1426067261463" MODIFIED="1426067636893">
<node TEXT="On application startup" STYLE_REF="pre_last_node" ID="ID_1541213653" CREATED="1426067591124" MODIFIED="1426067645766">
<node ID="ID_656567589" CREATED="1426067262355" MODIFIED="1426067552959"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <table border="1">
      <tr>
        <th unselectable="on" tabindex="0" data-column="0">
          <p>
            Type
          </p>
        </th>
        <th unselectable="on" tabindex="0" data-column="1">
          <p>
            Step
          </p>
        </th>
      </tr>
      <tr>
        <td class="confluenceTd">
          <p>
            Flex
          </p>
        </td>
        <td class="confluenceTd">
          <p>
            Preinitialize event
          </p>
        </td>
      </tr>
      <tr>
        <td class="confluenceTd">
          <p>
            Swiz
          </p>
        </td>
        <td class="confluenceTd">
          <p>
            <code>dispatcher</code>&#160;set
          </p>
        </td>
      </tr>
      <tr>
        <td class="confluenceTd">
          <p>
            Swiz
          </p>
        </td>
        <td class="confluenceTd">
          <p>
            Swiz created event
          </p>
        </td>
      </tr>
      <tr>
        <td class="confluenceTd">
          <p>
            Swiz
          </p>
        </td>
        <td class="confluenceTd">
          <p>
            <code>domain</code>&#160;set
          </p>
        </td>
      </tr>
      <tr>
        <td class="confluenceTd">
          <p>
            Swiz
          </p>
        </td>
        <td class="confluenceTd">
          <p>
            Global dispatcher set
          </p>
        </td>
      </tr>
      <tr>
        <td class="confluenceTd">
          <p>
            Swiz
          </p>
        </td>
        <td class="confluenceTd">
          <p>
            Processors initialized
          </p>
        </td>
      </tr>
      <tr>
        <td class="confluenceTd">
          <p>
            Swiz
          </p>
        </td>
        <td class="confluenceTd">
          <p>
            Bean factory initialized
          </p>
        </td>
      </tr>
      <tr>
        <td class="confluenceTd">
          <p>
            Swiz
          </p>
        </td>
        <td class="confluenceTd">
          <p>
            <code>setUpEvent</code>&#160;and <code>tearDownEvent</code>&#160;values set from <code>SwizConfig</code>
          </p>
        </td>
      </tr>
      <tr>
        <td class="confluenceTd">
          <p>
            Swiz
          </p>
        </td>
        <td class="confluenceTd">
          <p>
            Beans defined in the BeanProvider(s) are processed
          </p>
        </td>
      </tr>
      <tr>
        <td class="confluenceTd">
          <p>
            Swiz
          </p>
        </td>
        <td class="confluenceTd">
          <p>
            (per bean) <code>beanFactory.setUpBean()</code>
          </p>
        </td>
      </tr>
      <tr>
        <td class="confluenceTd">
          <p>
            Swiz
          </p>
        </td>
        <td class="confluenceTd">
          <p>
            (per bean) <code>[Inject]</code>&#160;processed
          </p>
        </td>
      </tr>
      <tr>
        <td class="confluenceTd">
          <p>
            Swiz
          </p>
        </td>
        <td class="confluenceTd">
          <p>
            (per bean) <code>[EventHandler]</code>&#160;processed
          </p>
        </td>
      </tr>
      <tr>
        <td class="confluenceTd">
          <p>
            Swiz
          </p>
        </td>
        <td class="confluenceTd">
          <p>
            (per bean) <code>[Dispatcher]</code>&#160;processed
          </p>
        </td>
      </tr>
      <tr>
        <td class="confluenceTd">
          <p>
            Swiz
          </p>
        </td>
        <td class="confluenceTd">
          <p>
            (per bean) Default custom metadata processed
          </p>
        </td>
      </tr>
      <tr>
        <td class="confluenceTd">
          <p>
            Swiz
          </p>
        </td>
        <td class="confluenceTd">
          <p>
            (per bean) <code>[PostConstruct]</code>&#160;processed
          </p>
        </td>
      </tr>
      <tr>
        <td class="confluenceTd">
          <p>
            Flex
          </p>
        </td>
        <td class="confluenceTd">
          <p>
            Initialize event
          </p>
        </td>
      </tr>
      <tr>
        <td class="confluenceTd">
          <p>
            Flex
          </p>
        </td>
        <td class="confluenceTd">
          <p>
            Creation complete event
          </p>
        </td>
      </tr>
      <tr>
        <td class="confluenceTd">
          <p>
            Flex
          </p>
        </td>
        <td class="confluenceTd">
          <p>
            Added to stage event
          </p>
        </td>
      </tr>
      <tr>
        <td class="confluenceTd">
          <p>
            Flex
          </p>
        </td>
        <td class="confluenceTd">
          <p>
            Display objects in the display list are processed (see table below)
          </p>
        </td>
      </tr>
      <tr>
        <td class="confluenceTd">
          <p>
            Flex
          </p>
        </td>
        <td class="confluenceTd">
          <p>
            Application complete event
          </p>
        </td>
      </tr>
      <tr>
        <td class="confluenceTd">
          <p>
            Flex
          </p>
        </td>
        <td class="confluenceTd">
          <p>
            Update complete event
          </p>
        </td>
      </tr>
    </table>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="When a new display object is set up" STYLE_REF="pre_last_node" ID="ID_72007713" CREATED="1426067604512" MODIFIED="1426067642494">
<node ID="ID_652029738" CREATED="1426067728854" MODIFIED="1426067781999"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <table border="1">
      <tr>
        <th unselectable="on" tabindex="0" data-column="0">
          <p>
            Type
          </p>
        </th>
        <th unselectable="on" tabindex="0" data-column="1">
          <p>
            Step
          </p>
        </th>
      </tr>
      <tr>
        <td class="confluenceTd">
          <p>
            Flex
          </p>
        </td>
        <td class="confluenceTd">
          <p>
            Invalidation
          </p>
        </td>
      </tr>
      <tr>
        <td class="confluenceTd">
          <p>
            Flex
          </p>
        </td>
        <td class="confluenceTd">
          <p>
            Property bindings
          </p>
        </td>
      </tr>
      <tr>
        <td class="confluenceTd">
          <p>
            Flex
          </p>
        </td>
        <td class="confluenceTd">
          <p>
            Preinitialize
          </p>
        </td>
      </tr>
      <tr>
        <td class="confluenceTd">
          <p>
            Flex
          </p>
        </td>
        <td class="confluenceTd">
          <p>
            Create children
          </p>
        </td>
      </tr>
      <tr>
        <td class="confluenceTd">
          <p>
            Flex
          </p>
        </td>
        <td class="confluenceTd">
          <p>
            Initialize event
          </p>
        </td>
      </tr>
      <tr>
        <td class="confluenceTd">
          <p>
            Flex
          </p>
        </td>
        <td class="confluenceTd">
          <p>
            Commit properties
          </p>
        </td>
      </tr>
      <tr>
        <td class="confluenceTd">
          <p>
            Flex
          </p>
        </td>
        <td class="confluenceTd">
          <p>
            Resize
          </p>
        </td>
      </tr>
      <tr>
        <td class="confluenceTd">
          <p>
            Flex
          </p>
        </td>
        <td class="confluenceTd">
          <p>
            Render
          </p>
        </td>
      </tr>
      <tr>
        <td class="confluenceTd">
          <p>
            Flex
          </p>
        </td>
        <td class="confluenceTd">
          <p>
            Measure
          </p>
        </td>
      </tr>
      <tr>
        <td class="confluenceTd">
          <p>
            Flex
          </p>
        </td>
        <td class="confluenceTd">
          <p>
            Set actual size
          </p>
        </td>
      </tr>
      <tr>
        <td class="confluenceTd">
          <p>
            Flex
          </p>
        </td>
        <td class="confluenceTd">
          <p>
            Update display list
          </p>
        </td>
      </tr>
      <tr>
        <td class="confluenceTd">
          <p>
            Flex
          </p>
        </td>
        <td class="confluenceTd">
          <p>
            Creation complete event
          </p>
        </td>
      </tr>
      <tr>
        <td class="confluenceTd">
          <p>
            Flex
          </p>
        </td>
        <td class="confluenceTd">
          <p>
            Added event
          </p>
        </td>
      </tr>
      <tr>
        <td class="confluenceTd">
          <p>
            Flex
          </p>
        </td>
        <td class="confluenceTd">
          <p>
            Added to stage event
          </p>
        </td>
      </tr>
      <tr>
        <td class="confluenceTd">
          <p>
            Swiz
          </p>
        </td>
        <td class="confluenceTd">
          <p>
            <code>beanFactory.setUpBean()</code>
          </p>
        </td>
      </tr>
      <tr>
        <td class="confluenceTd">
          <p>
            Swiz
          </p>
        </td>
        <td class="confluenceTd">
          <p>
            <code>[Inject]</code>&#160;processed
          </p>
        </td>
      </tr>
      <tr>
        <td class="confluenceTd">
          <p>
            Swiz
          </p>
        </td>
        <td class="confluenceTd">
          <p>
            <code>[EventHandler]</code>&#160;processed
          </p>
        </td>
      </tr>
      <tr>
        <td class="confluenceTd">
          <p>
            Swiz
          </p>
        </td>
        <td class="confluenceTd">
          <p>
            <code>[Dispatcher]</code>&#160;processed
          </p>
        </td>
      </tr>
      <tr>
        <td class="confluenceTd">
          <p>
            Swiz
          </p>
        </td>
        <td class="confluenceTd">
          <p>
            Default custom metadata processed
          </p>
        </td>
      </tr>
      <tr>
        <td class="confluenceTd">
          <p>
            Swiz
          </p>
        </td>
        <td class="confluenceTd">
          <p>
            <code>[PostConstruct]</code>&#160;processed
          </p>
        </td>
      </tr>
      <tr>
        <td class="confluenceTd">
          <p>
            Swiz
          </p>
        </td>
        <td class="confluenceTd">
          <p>
            <code>[ViewAdded]</code>&#160;processed
          </p>
        </td>
      </tr>
      <tr>
        <td class="confluenceTd">
          <p>
            Flex
          </p>
        </td>
        <td class="confluenceTd">
          <p>
            Update complete event
          </p>
        </td>
      </tr>
    </table>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="When a display object is torn down" STYLE_REF="pre_last_node" ID="ID_424422737" CREATED="1426067613399" MODIFIED="1426067642496">
<node ID="ID_946499421" CREATED="1426067828337" MODIFIED="1426067882587"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <table border="1">
      <tr>
        <th unselectable="on" tabindex="0" data-column="0">
          <p>
            Type
          </p>
        </th>
        <th unselectable="on" tabindex="0" data-column="1">
          <p>
            Step
          </p>
        </th>
      </tr>
      <tr>
        <td class="confluenceTd">
          <p>
            Flex
          </p>
        </td>
        <td class="confluenceTd">
          <p>
            Removed event
          </p>
        </td>
      </tr>
      <tr>
        <td class="confluenceTd">
          <p>
            Flex
          </p>
        </td>
        <td class="confluenceTd">
          <p>
            Removed from stage event
          </p>
        </td>
      </tr>
      <tr>
        <td class="confluenceTd">
          <p>
            Swiz
          </p>
        </td>
        <td class="confluenceTd">
          <p>
            <code>[PreDestroy]</code>&#160;processed
          </p>
        </td>
      </tr>
      <tr>
        <td class="confluenceTd">
          <p>
            Swiz
          </p>
        </td>
        <td class="confluenceTd">
          <p>
            <code>[Inject]</code>&#160;tear down
          </p>
        </td>
      </tr>
      <tr>
        <td class="confluenceTd">
          <p>
            Swiz
          </p>
        </td>
        <td class="confluenceTd">
          <p>
            <code>[EventHandler]</code>&#160;tear down
          </p>
        </td>
      </tr>
      <tr>
        <td class="confluenceTd">
          <p>
            Swiz
          </p>
        </td>
        <td class="confluenceTd">
          <p>
            <code>[Dispatcher]</code>&#160;tear down
          </p>
        </td>
      </tr>
      <tr>
        <td class="confluenceTd">
          <p>
            Swiz
          </p>
        </td>
        <td class="confluenceTd">
          <p>
            Default custom metadata tear down
          </p>
        </td>
      </tr>
      <tr>
        <td class="confluenceTd">
          <p>
            Swiz
          </p>
        </td>
        <td class="confluenceTd">
          <p>
            <code>[ViewRemoved]</code>&#160;processed
          </p>
        </td>
      </tr>
    </table>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
</node>
</node>
<node TEXT="Configuration" STYLE_REF="second_node" POSITION="left" ID="ID_707729740" CREATED="1426067063117" MODIFIED="1426080657276">
<edge COLOR="#7a0e7a"/>
<hook URI="../../Images/config.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="You configure each Swiz instance using the SwizConfig class" STYLE_REF="pre_last_node" ID="ID_377627289" CREATED="1426091743703" MODIFIED="1426091754416">
<icon BUILTIN="idea"/>
</node>
<node TEXT="The only configuration values that commonly need to be set are" STYLE_REF="yellow_node" ID="ID_1497634391" CREATED="1426091858955" MODIFIED="1426091937062">
<node TEXT="eventPackages" STYLE_REF="gray_node" ID="ID_505171401" CREATED="1426091864757" MODIFIED="1426091934035">
<node TEXT="When you define eventPackages, you must specify each package individually. Children of your specified packages cannot be resolved and must be explicitly set" STYLE_REF="last_node" ID="ID_1143676922" CREATED="1426091831889" MODIFIED="1426091941518"/>
</node>
<node TEXT="viewPackages" STYLE_REF="gray_node" ID="ID_1667396620" CREATED="1426091869736" MODIFIED="1426091934032">
<node TEXT="This limitation does not apply to viewPackages because they are handled differently, but for consistency it may be useful to use the same rules to define both sets of packages" STYLE_REF="last_node" ID="ID_718179699" CREATED="1426091917104" MODIFIED="1426091941520"/>
</node>
</node>
<node TEXT="Options" STYLE_REF="orange_node" ID="ID_721833759" CREATED="1426066822432" MODIFIED="1426067074293">
<node TEXT="setUpEventType" STYLE_REF="yellow_node" ID="ID_8470780" CREATED="1426066832663" MODIFIED="1426066930123">
<node TEXT="addedToStage" STYLE_REF="pre_last_node" ID="ID_1634353614" CREATED="1426066852581" MODIFIED="1426066939201"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Default
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true"/>
<node TEXT="This means that whenever a UI component is added to the stage,&#xa;Swiz will inspect the component and process any metadata it finds" ID="ID_1691326867" CREATED="1426066886937" MODIFIED="1426066893816"/>
<node TEXT="Any dependency injections and event mediators will happen at this time" ID="ID_1553153705" CREATED="1426066975226" MODIFIED="1426066976890"/>
</node>
<node TEXT="creationComplete" STYLE_REF="pre_last_node" ID="ID_950582711" CREATED="1426066908393" MODIFIED="1426066939203">
<font BOLD="true"/>
</node>
</node>
<node TEXT="setUpEventPhase" STYLE_REF="yellow_node" ID="ID_929112610" CREATED="1426067120290" MODIFIED="1426067155661"/>
<node TEXT="setUpEventPriority" STYLE_REF="yellow_node" ID="ID_974535534" CREATED="1426067130419" MODIFIED="1426067155662"/>
<node TEXT="tearDownEventType" STYLE_REF="yellow_node" ID="ID_1275535478" CREATED="1426067100614" MODIFIED="1426067155663">
<node TEXT="removedFromStage" STYLE_REF="pre_last_node" ID="ID_1005233681" CREATED="1426067107567" MODIFIED="1426067162633"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Default
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true"/>
<node TEXT="When a UI component is removed from the stage, Swiz will&#xa;perform clean up activities such as removing event mediators" ID="ID_1242282091" CREATED="1426067181141" MODIFIED="1426067190824"/>
</node>
</node>
<node TEXT="tearDownEventPhase" STYLE_REF="yellow_node" ID="ID_1762902701" CREATED="1426067141326" MODIFIED="1426067155663"/>
<node TEXT="tearDownEventPriority" STYLE_REF="yellow_node" ID="ID_1807890849" CREATED="1426067147532" MODIFIED="1426067155664"/>
<node TEXT="defaultFaultHandler" STYLE_REF="yellow_node" ID="ID_1274904193" CREATED="1426092301927" MODIFIED="1426144441101">
<node TEXT="Useful when you are using Swiz&apos;s support for server communication" STYLE_REF="last_node" ID="ID_412305724" CREATED="1426092315822" MODIFIED="1426144441100"/>
</node>
<node TEXT="Default Dispatcher" STYLE_REF="yellow_node" ID="ID_1717042309" CREATED="1426092019094" MODIFIED="1426144408547">
<node TEXT="global" STYLE_REF="pre_last_node" ID="ID_1471338663" CREATED="1426092134642" MODIFIED="1426092218023"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Default
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true"/>
<node TEXT="In the case where a Swiz instance is the child of another&#xa;Swiz instance, the child will use the parent dispatcher" ID="ID_1933341222" CREATED="1426092099134" MODIFIED="1426092208074">
<node TEXT="This allows for easy communication between Swiz instances,&#xa;since they all share the same event dispatcher" ID="ID_1849069891" CREATED="1426092118868" MODIFIED="1426092227643"/>
</node>
</node>
<node TEXT="local" STYLE_REF="pre_last_node" ID="ID_871459584" CREATED="1426092152428" MODIFIED="1426092218021">
<font BOLD="true"/>
<node TEXT="If you want to force a child Swiz to use it&apos;s own dispatcher" ID="ID_1243975640" CREATED="1426092163897" MODIFIED="1426092167661"/>
</node>
</node>
</node>
<node TEXT="&lt;swiz:Swiz&gt;&#xa;    &lt;swiz:beanProviders&gt;&#xa;        &lt;local:MyBeans /&gt;&#xa;    &lt;/swiz:beanProviders&gt;&#xa; &#xa;    &lt;swiz:loggingTargets&gt;&#xa;        &lt;swiz:SwizTraceTarget id=&quot;myTraceTarget&quot; /&gt;&#xa;    &lt;/swiz:loggingTargets&gt;&#xa; &#xa;    &lt;swiz:config&gt;&#xa;        &lt;swiz:SwizConfig&#xa;            setUpEventType=&quot;{ Event.ADDED_TO_STAGE }&quot;&#xa;            setUpEventPhase=&quot;{ EventPhase.CAPTURING_PHASE }&quot;&#xa;            setUpEventPriority=&quot;50&quot;&#xa;            tearDownEventType=&quot;{ Event.REMOVED_FROM_STAGE }&quot;&#xa;            tearDownEventPhase=&quot;{ EventPhase.CAPTURING_PHASE }&quot;&#xa;            tearDownEventPriority=&quot;50&quot;&#xa;            eventPackages=&quot;com.foo.event.*, org.bar.event.*&quot;&#xa;            viewPackages=&quot;com.foo.view.*, org.bar.view.*&quot;&#xa;            defaultFaultHandler=&quot;handleUnhandledFaults&quot;&#xa;            defaultDispatcher=&quot;global&quot; /&gt;&#xa;    &lt;/swiz:config&gt;&#xa;&lt;/swiz:Swiz&gt;" STYLE_REF="example_node" ID="ID_702692497" CREATED="1426091770366" MODIFIED="1426092255612"/>
</node>
<node TEXT=" Client Persistence" STYLE_REF="second_node" POSITION="right" ID="ID_1598013160" CREATED="1426068300344" MODIFIED="1426082285776" VSHIFT="19">
<edge COLOR="#127a7a"/>
<hook URI="../../Images/persistence.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="SharedObjectBean" STYLE_REF="orange_node" ID="ID_115876750" CREATED="1426068326913" MODIFIED="1426068574940" VSHIFT="-7">
<node TEXT="Saving data in different sessions" STYLE_REF="pre_last_node" ID="ID_1662048374" CREATED="1426068493768" MODIFIED="1426068551133"/>
<node TEXT="1. Declare in BeanProvider" STYLE_REF="pre_last_node" ID="ID_1995932293" CREATED="1426068410075" MODIFIED="1426068568225">
<node TEXT="&lt;swiz:BeanProvider&#xa;    xmlns:swiz=&quot;http://swiz.swizframework.org&quot;&#xa;    xmlns:storage=&quot;org.swizframework.storage.*&quot;&gt;&#xa; &#xa;    &lt;storage:SharedObjectBean id=&quot;soBean&quot; /&gt;&#xa; &#xa;&lt;/swiz:BeanProvider&gt;" STYLE_REF="example_node" ID="ID_1066969390" CREATED="1426068438568" MODIFIED="1426068480340"/>
</node>
<node TEXT="2. Use" STYLE_REF="pre_last_node" ID="ID_1841958345" CREATED="1426068452608" MODIFIED="1426068571004">
<node TEXT="[Inject]&#xa;public var so:ISharedObjectBean;&#xa; &#xa;[Bindable]&#xa;public function get appIndex():int {&#xa;    // the second parameter is the initial value&#xa;    return so.getInt(&quot;appIndex&quot;, 0);&#xa;}&#xa; &#xa;public function set appIndex(index:int):void {&#xa;    so.setInt(&quot;appIndex&quot;, index);&#xa;}" STYLE_REF="example_node" ID="ID_803623295" CREATED="1426068459194" MODIFIED="1426068480342"/>
</node>
</node>
<node TEXT="EncryptedLocalStorageBean" STYLE_REF="orange_node" ID="ID_205210793" CREATED="1426068332837" MODIFIED="1426068379975"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Only for AIR projects
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Can be find at" STYLE_REF="pre_last_node" ID="ID_936306593" CREATED="1426068383324" MODIFIED="1426068644991">
<node TEXT="https://github.com/swiz/swiz-extensions" ID="ID_1514726885" CREATED="1426068637373" MODIFIED="1426068638756"/>
</node>
</node>
</node>
<node TEXT="Service Layer" STYLE_REF="second_node" POSITION="left" ID="ID_207721672" CREATED="1426079159101" MODIFIED="1426080435427">
<edge COLOR="#979702"/>
<hook URI="../../Images/service.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="ServiceHelper" STYLE_REF="orange_node" ID="ID_764983884" CREATED="1426079348833" MODIFIED="1426079570443">
<node TEXT="ServiceHelper is a class that simplifies and improves&#xa;your interactions with service classes that return an AsyncToken" STYLE_REF="pre_last_node" ID="ID_62588138" CREATED="1426079362101" MODIFIED="1426079566065"/>
<node TEXT="[Inject( &quot;userService&quot; )]&#xa;public var ro:RemoteObject;&#xa; &#xa;[Inject]&#xa;public var sh:ServiceHelper;&#xa; &#xa;public function fetchUserRoles(user:User):void {&#xa;    sh.executeServiceCall(ro.fetchUserRoles(user.id), fetchUserRoles_result, fetchUserRoles_fault, [user]);&#xa;}&#xa; &#xa;protected function fetchUserRoles_result(data:Object, user:User):void {&#xa;    user.roles = data.result;&#xa;}&#xa; &#xa;protected function fetchUserRoles_fault(info:Object):void {&#xa;    // handle service fault&#xa;}" STYLE_REF="example_node" ID="ID_887711988" CREATED="1426079548330" MODIFIED="1426144550290"/>
</node>
<node TEXT="URLRequestHelper" STYLE_REF="orange_node" ID="ID_340225454" CREATED="1426079577868" MODIFIED="1426079694026">
<node TEXT="Remote calls that do not return an AsyncToken are generally&#xa;implemented using the URLRequest and URLLoader classes" STYLE_REF="pre_last_node" ID="ID_1421978259" CREATED="1426079596832" MODIFIED="1426079688380"/>
<node TEXT="executeURLRequest() method works much like executeServiceCall()" STYLE_REF="pre_last_node" ID="ID_1710252134" CREATED="1426079638857" MODIFIED="1426079688377"/>
<node TEXT="[Inject]&#xa;public var urh:URLRequestHelper;&#xa; &#xa;public function loadConfig( user:User ):void {&#xa;    urh.executeURLRequest( new URLRequest( &quot;config.xml&quot; ), loadConfig_result, loadConfig_fault, loadConfig_progress, loadConfig_httpStatus, [ user ] );&#xa;}&#xa; &#xa;protected function loadConfig_result( event:Event, user:User ):void {&#xa;    user.config = XML( URLLoader( event.target ).data );&#xa;}&#xa; &#xa;protected function loadConfig_fault( event:Event ):void {&#xa;    // will be called in response to an IOErrorEvent.IO_ERROR or SecurityErrorEvent.SECURITY_ERROR&#xa;}&#xa; &#xa;protected function loadConfig_progress( event:Event ):void {&#xa;    // will be called in response to ProgressEvent.PROGRESS&#xa;}&#xa; &#xa;protected function loadConfig_httpStatus( event:Event ):void {&#xa;    // will be called in response to HTTPStatusEvent.HTTP_STATUS&#xa;}" STYLE_REF="example_node" ID="ID_1577641939" CREATED="1426079648940" MODIFIED="1426079684692"/>
</node>
</node>
<node TEXT="Interfaces" STYLE_REF="second_node" POSITION="right" ID="ID_183018775" CREATED="1426080184283" MODIFIED="1426080211505">
<edge COLOR="#7c0000"/>
<hook URI="../../Images/interface-2.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="ISetUpValidator" STYLE_REF="orange_node" ID="ID_1383472531" CREATED="1426080668012" MODIFIED="1426080739916">
<node TEXT="Implementing this interface will instruct Swiz to invoke&#xa;the allowSetUp() method of the bean before the bean is set up" STYLE_REF="pre_last_node" ID="ID_70232178" CREATED="1426080695018" MODIFIED="1426080745519"/>
<node TEXT="If this method returns true" STYLE_REF="pre_last_node" ID="ID_1809299042" CREATED="1426080712719" MODIFIED="1426080756932">
<node TEXT="Set up will proceed" ID="ID_1609856222" CREATED="1426080724448" MODIFIED="1426080750767"/>
</node>
<node TEXT="If it returns false" STYLE_REF="pre_last_node" ID="ID_42840979" CREATED="1426080731083" MODIFIED="1426080745518">
<node TEXT="Swiz will not set up the bean" ID="ID_958708942" CREATED="1426080735953" MODIFIED="1426080736860"/>
</node>
</node>
<node TEXT="ITearDownValidator" STYLE_REF="orange_node" ID="ID_820857979" CREATED="1426080765663" MODIFIED="1426080827827">
<node TEXT="Implementing this interface will instruct Swiz to invoke the&#xa;allowTearDown() method of the bean before the bean is torn down" STYLE_REF="pre_last_node" ID="ID_1855271164" CREATED="1426080779439" MODIFIED="1426080833760"/>
<node TEXT="If this method returns true" STYLE_REF="pre_last_node" ID="ID_733201139" CREATED="1426080796083" MODIFIED="1426080833764">
<node TEXT="Tear down will proceed" ID="ID_569472549" CREATED="1426080804696" MODIFIED="1426080807711"/>
</node>
<node TEXT="If it returns false" STYLE_REF="pre_last_node" ID="ID_1207604303" CREATED="1426080814312" MODIFIED="1426080833764">
<node TEXT="Swiz will not tear down the bean" ID="ID_305603423" CREATED="1426080822579" MODIFIED="1426080823536"/>
</node>
</node>
<node TEXT="IInitializing" STYLE_REF="orange_node" ID="ID_1317479195" CREATED="1426080836845" MODIFIED="1426080883709">
<node TEXT="Implementing this interface will instruct Swiz to invoke the&#xa;afterPropertiesSet() method of the bean after the bean is set up" STYLE_REF="pre_last_node" ID="ID_870088744" CREATED="1426080899556" MODIFIED="1426081005018"/>
</node>
<node TEXT="IDisposable" STYLE_REF="orange_node" ID="ID_1283281093" CREATED="1426080859007" MODIFIED="1426080883709">
<node TEXT="Implementing this interface will instruct Swiz to invoke the&#xa;destroy() method of the bean when the bean is destroyed" STYLE_REF="pre_last_node" ID="ID_110663608" CREATED="1426080915099" MODIFIED="1426081005017"/>
</node>
<node TEXT="IDispatcherAware" STYLE_REF="orange_node" ID="ID_1636220947" CREATED="1426080864177" MODIFIED="1426080883709">
<node TEXT="Implementing this interface will instruct Swiz to inject an&#xa;IEventDispatcher into the bean&apos;s dispatcher property when it is set up" STYLE_REF="pre_last_node" ID="ID_1504481818" CREATED="1426080944370" MODIFIED="1426081005016"/>
</node>
<node TEXT="IBeanFactoryAware" STYLE_REF="orange_node" ID="ID_1429671406" CREATED="1426080869737" MODIFIED="1426080883708">
<node TEXT="Implementing this interface will instruct Swiz to inject the Swiz&#xa;IBeanFactory into the bean&apos;s beanFactory property when it is set up" STYLE_REF="pre_last_node" ID="ID_275313251" CREATED="1426080970475" MODIFIED="1426081005015"/>
</node>
<node TEXT="ISwizAware" STYLE_REF="orange_node" ID="ID_1831215088" CREATED="1426080876929" MODIFIED="1426080883707">
<node TEXT="Implementing this interface will instruct Swiz to inject&#xa;itself into the bean&apos;s swiz property when it is set up" STYLE_REF="pre_last_node" ID="ID_1159754014" CREATED="1426080987362" MODIFIED="1426081005012"/>
</node>
</node>
<node TEXT="View Mediator and ViewNavigator" STYLE_REF="second_node" POSITION="left" ID="ID_1697095890" CREATED="1426081038558" MODIFIED="1426081449522" VSHIFT="9">
<edge COLOR="#00007c"/>
<hook URI="../../Images/mediator.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="View Mediator" STYLE_REF="orange_node" ID="ID_1338325021" CREATED="1426081063027" MODIFIED="1426081109841">
<node TEXT="Swiz will pass in any instances of the type specified that it&#xa;detects have been added to or removed from stage" STYLE_REF="pre_last_node" ID="ID_1991640216" CREATED="1426081093662" MODIFIED="1426081195683"/>
<node TEXT="[ViewAdded]" STYLE_REF="yellow_node" ID="ID_1788473379" CREATED="1426081070268" MODIFIED="1426081189982">
<node TEXT="[ViewAdded]&#xa;public function userListViewAdded( view : UserListView ) : void {&#xa;    this.view = view;&#xa;}" STYLE_REF="example_node" ID="ID_1299607507" CREATED="1426081171990" MODIFIED="1426081200560"/>
</node>
<node TEXT="[ViewRemoved]" STYLE_REF="yellow_node" ID="ID_1246012058" CREATED="1426081105855" MODIFIED="1426081189981">
<node TEXT="[ViewRemoved]&#xa;public function userListViewRemoved( view : UserListView ) : void {&#xa;    this.view = null;&#xa;}" STYLE_REF="example_node" ID="ID_1878770803" CREATED="1426081181098" MODIFIED="1426081200556"/>
</node>
</node>
<node TEXT="ViewNavigator" STYLE_REF="orange_node" ID="ID_1311796499" CREATED="1426081243394" MODIFIED="1426081249739">
<node TEXT="you can use the [ViewNavigator] tag to get a reference&#xa;to the ViewNavigator instance in non-view classes" STYLE_REF="pre_last_node" ID="ID_1713317903" CREATED="1426081269800" MODIFIED="1426081286789"/>
<node TEXT="[ViewNavigator]&#xa;public var viewNavigator : ViewNavigator;" STYLE_REF="example_node" ID="ID_723027249" CREATED="1426081283023" MODIFIED="1426081289820"/>
</node>
</node>
<node TEXT="Event Handling" STYLE_REF="second_node" POSITION="right" ID="ID_685843477" CREATED="1426081508536" MODIFIED="1426089914656">
<edge COLOR="#007c00"/>
<hook URI="../../Images/event.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Dispatching Events" STYLE_REF="orange_node" ID="ID_1553895014" CREATED="1426088630227" MODIFIED="1426088638775">
<node TEXT="Events dispatched from children of DisplayObject" STYLE_REF="yellow_node" ID="ID_126206519" CREATED="1426087689074" MODIFIED="1426088750049">
<node TEXT="Dispatching events from UI components is done using the&#xa;standard dispatchEvent() method that DisplayObject provides" STYLE_REF="pre_last_node" ID="ID_967729070" CREATED="1426088802744" MODIFIED="1426088853784"/>
<node TEXT="If you dispatch events that bubble, Swiz can invoke&#xa;event handlers to handle these events" STYLE_REF="pre_last_node" ID="ID_683971920" CREATED="1426088823678" MODIFIED="1426088853789"/>
</node>
<node TEXT="Events dispatched from non-UI objects" STYLE_REF="yellow_node" ID="ID_1700930150" CREATED="1426088576101" MODIFIED="1426088750047">
<node TEXT="Use the [Dispatcher] metadata tag" STYLE_REF="pre_last_node" ID="ID_1613017482" CREATED="1426088657605" MODIFIED="1426088756110">
<node TEXT="Once the dispatcher is injected, you can use it to dispatch events&#xa;that can be handled using metadata-based event handlers" ID="ID_794152323" CREATED="1426088740296" MODIFIED="1426088744611"/>
<node TEXT="[Dispatcher]&#xa;public var dispatcher:IEventDispatcher;" STYLE_REF="example_node" ID="ID_962129027" CREATED="1426088676423" MODIFIED="1426088760845"/>
</node>
<node TEXT="To implement the Swiz IDispatcherAware interface" STYLE_REF="pre_last_node" ID="ID_1725667724" CREATED="1426088701510" MODIFIED="1426088756109"/>
</node>
</node>
<node TEXT="Handling Events with [EventHandler]" STYLE_REF="orange_node" ID="ID_1390071819" CREATED="1426088879368" MODIFIED="1426089486403">
<node TEXT="Swiz handles application events using the&#xa;[EventHandler] metadata tag" STYLE_REF="pre_last_node" ID="ID_1738803113" CREATED="1426088935376" MODIFIED="1426089192712"/>
<node TEXT="Methods decorated with [EventHandler] must be public" STYLE_REF="pre_last_node" ID="ID_698825323" CREATED="1426089179475" MODIFIED="1426089192711"/>
<node TEXT="Benefits" STYLE_REF="yellow_node" ID="ID_678171732" CREATED="1426088946952" MODIFIED="1426089195839">
<node TEXT="You no longer need to manually declare event listeners. Swiz will automatically remove the event listener when the bean is destroyed or removed from the display list" STYLE_REF="pre_last_node" ID="ID_756182154" CREATED="1426088983853" MODIFIED="1426089441896"/>
<node TEXT="[EventHandler] methods can be declared without specifying an Event object as a method argument. You can easily invoke these methods yourself, outside the context of responding to an event" STYLE_REF="pre_last_node" ID="ID_1175209355" CREATED="1426089066516" MODIFIED="1426089441895"/>
<node TEXT="A single method can easily respond to multiple event types" STYLE_REF="pre_last_node" ID="ID_1068913563" CREATED="1426089102469" MODIFIED="1426089441894"/>
<node TEXT="Because the methods are annotated with metadata, it is possible&#xa;to introspect a class to determine what events it will respond to" STYLE_REF="pre_last_node" ID="ID_1970309828" CREATED="1426089134572" MODIFIED="1426089441893"/>
</node>
<node TEXT="Event Handling Using Class and Constant Names" STYLE_REF="yellow_node" ID="ID_1645957267" CREATED="1426089333662" MODIFIED="1426089428716">
<node TEXT="[EventHandler( event=&quot;UserEvent.ADD_USER&quot; )]&#xa;public function handleAddUserEvent( event:UserEvent ):void {&#xa;    // do stuff&#xa;}" STYLE_REF="example_node" ID="ID_1817567804" CREATED="1426089344883" MODIFIED="1426089433455">
<node TEXT="Should be added &quot;com.foo.events&quot; to the eventPackages property of SwizConfig" ID="ID_1381320554" CREATED="1426089367086" MODIFIED="1426089447481"/>
</node>
<node TEXT="[EventHandler( event=&quot;com.foo.events.UserEvent.ADD_USER&quot; )]&#xa;public function handleAddUserEvent( event:UserEvent ):void {&#xa;    // do stuff&#xa;}" STYLE_REF="example_node" ID="ID_710910049" CREATED="1426089383485" MODIFIED="1426089433452"/>
</node>
<node TEXT="Event Handling Using Event Type Value" STYLE_REF="yellow_node" ID="ID_47401667" CREATED="1426089396357" MODIFIED="1426089428717"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Not recommended
    </p>
  </body>
</html>
</richcontent>
<node TEXT="[EventHandler( event=&quot;addUser&quot; )]&#xa;public function handleAddUserEvent( event:UserEvent ):void {&#xa;    // do stuff&#xa;}" STYLE_REF="example_node" ID="ID_456912700" CREATED="1426089421339" MODIFIED="1426089433448"/>
</node>
</node>
<node TEXT="[EventHandler] tag options" STYLE_REF="orange_node" ID="ID_1297088422" CREATED="1426089469006" MODIFIED="1426089879686">
<node TEXT="In some cases you don&apos;t need the event&#xa;passed to your method" STYLE_REF="pre_last_node" ID="ID_1713686030" CREATED="1426089545990" MODIFIED="1426145208637">
<node TEXT="[EventHandler(event=&quot;UserEvent.ADD_USER&quot;)]&#xa;public function handleAddUserEvent():void {&#xa;    // do stuff&#xa;}" STYLE_REF="example_node" ID="ID_615843498" CREATED="1426089558109" MODIFIED="1426089849338"/>
</node>
<node TEXT="Properties attribute" STYLE_REF="yellow_node" ID="ID_428788463" CREATED="1426089616704" MODIFIED="1426089858961">
<node TEXT="You can also specify a comma delimited list of property names in the tag to have the matching properties pulled off of the event and passed into your method" STYLE_REF="pre_last_node" ID="ID_886731824" CREATED="1426089650401" MODIFIED="1426089865737"/>
<node TEXT="[EventHandler(event=&quot;UserEvent.ADD_USER&quot;, properties=&quot;user&quot;)]&#xa;public function addUser( user:User ):void {&#xa;    // do stuff&#xa;}" STYLE_REF="example_node" ID="ID_1096258665" CREATED="1426089657417" MODIFIED="1426089849334"/>
</node>
<node TEXT="Other attributes" STYLE_REF="yellow_node" ID="ID_1125615941" CREATED="1426089667889" MODIFIED="1426089879685" HGAP="29" VSHIFT="-48">
<node TEXT="Swiz also supports some related attributes. useCapture, priority, stopPropagation and stopImmediatePropagation are all supported" STYLE_REF="pre_last_node" ID="ID_384593721" CREATED="1426089723347" MODIFIED="1426089865742"/>
<node TEXT="[EventHandler(event=&quot;UserEvent.ADD_USER&quot;, stopPropagation=true)]&#xa;public function addUser( user:User ):void {&#xa;    // do stuff&#xa;}" STYLE_REF="example_node" ID="ID_1389466844" CREATED="1426089931858" MODIFIED="1426089955424"/>
</node>
</node>
<node TEXT="Handling Multiple Events from a Single Method" STYLE_REF="orange_node" ID="ID_1344096294" CREATED="1426089740333" MODIFIED="1426089783872">
<node TEXT="It is possible to handle multiple events using a single method" STYLE_REF="pre_last_node" ID="ID_1603075764" CREATED="1426089838690" MODIFIED="1426089853978">
<node TEXT="[EventHandler( event=&quot;UserEvent.ADD_USER&quot;, properties=&quot;user&quot; )]&#xa;[EventHandler( event=&quot;UserEvent.EDIT_USER&quot;, properties=&quot;user&quot; )]&#xa;[EventHandler( event=&quot;UserEvent.DELETE_USER&quot;, properties=&quot;user&quot; )]&#xa;public function handleUserEvents(user:User):void {&#xa;    // do stuff&#xa;}" STYLE_REF="example_node" ID="ID_968442113" CREATED="1426089757582" MODIFIED="1426089849331"/>
</node>
<node TEXT="You can handle any event type of a given event using a wildcard" STYLE_REF="pre_last_node" ID="ID_1411424533" CREATED="1426089811933" MODIFIED="1426089853976">
<node TEXT="[EventHandler( event=&quot;UserEvent.*&quot;, properties=&quot;user&quot; )]&#xa;public function handleUserEvents( user:User ):void {&#xa;    // do stuff&#xa;}" STYLE_REF="example_node" ID="ID_503067046" CREATED="1426089817873" MODIFIED="1426089849328"/>
</node>
</node>
</node>
<node TEXT="Command pattern" STYLE_REF="second_node" POSITION="left" ID="ID_541607062" CREATED="1426090388299" MODIFIED="1426091439748">
<edge COLOR="#7c007c"/>
<hook URI="../../Images/command.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="1. Extend CommandMap" STYLE_REF="pre_last_node" ID="ID_145811807" CREATED="1426090522394" MODIFIED="1426144938686">
<node TEXT="public class MyCommandMap extends CommandMap {&#xa;    override protected function mapCommands() : void {&#xa;        mapCommand( MyEvent.OPEN, MyCommand, MyEvent, false );&#xa;    }&#xa;}" STYLE_REF="example_node" ID="ID_481149939" CREATED="1426090585465" MODIFIED="1426144938683"/>
</node>
<node TEXT="2. Override the mapCommands() method" STYLE_REF="pre_last_node" ID="ID_1329802896" CREATED="1426090537680" MODIFIED="1426090776121">
<node TEXT="MyEvent" STYLE_REF="example_node" ID="ID_1455856126" CREATED="1426091193214" MODIFIED="1426091273400"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Third argument
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Event class associated with the event type" ID="ID_688992644" CREATED="1426091228394" MODIFIED="1426091232926"/>
<node TEXT="if MyEvent.OPEN and YourEvent.OPEN both evaluate to &quot;open&quot;,&#xa;providing MyEvent as the third argument will instruct Swiz to only&#xa;execute MyCommand in response to MyEvent.OPEN" ID="ID_1403227647" CREATED="1426091258319" MODIFIED="1426091265802"/>
</node>
<node TEXT="oneTime:Boolean = false" STYLE_REF="example_node" ID="ID_1001374453" CREATED="1426091130736" MODIFIED="1426091183639"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Fourth argument
    </p>
  </body>
</html>
</richcontent>
<node TEXT="If true, Swiz will remove the mapping after&#xa;the first time the command is executed" ID="ID_708025030" CREATED="1426091167793" MODIFIED="1426091177509"/>
</node>
</node>
<node TEXT="3. Add your CommandMap to one of your BeanProviders" STYLE_REF="pre_last_node" ID="ID_1765333397" CREATED="1426090621974" MODIFIED="1426090776117">
<node TEXT="&lt;swiz:BeanProvider&gt;&#xa;    &lt;command:MyCommandMap id=&quot;myCommandMap&quot; /&gt;&#xa;&lt;/swiz:BeanProvider&gt;" STYLE_REF="example_node" ID="ID_729706734" CREATED="1426090633588" MODIFIED="1426090891872"/>
</node>
<node TEXT="Commands you register must implement&#xa;one of two interfaces provided by Swiz" STYLE_REF="pre_last_node" ID="ID_692005040" CREATED="1426090707402" MODIFIED="1426090776113">
<node TEXT="public interface ICommand {&#xa;    function execute():void;&#xa;}" STYLE_REF="example_node" ID="ID_1690612299" CREATED="1426090728084" MODIFIED="1426090759317"/>
<node TEXT="public interface IEventAwareCommand extends ICommand {&#xa;    function set event( value:Event ):void;&#xa;}" STYLE_REF="example_node" ID="ID_1989531337" CREATED="1426090733751" MODIFIED="1426090759313"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      If your Command needs access to the event that triggered it
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="Presentation Model" STYLE_REF="second_node" POSITION="right" ID="ID_1242409533" CREATED="1426092656875" MODIFIED="1426093230290" VSHIFT="22">
<edge COLOR="#007c7c"/>
<hook URI="../../Images/best-practice.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="The presentation model (PM) approach is the&#xa;recommended view layer architecture for Swiz" STYLE_REF="pre_last_node" ID="ID_1621638895" CREATED="1426092680720" MODIFIED="1426092889889"/>
<node TEXT="The goal is to remove any logic from the view&#xa;and let the PM handle the view logic" STYLE_REF="pre_last_node" ID="ID_1820318893" CREATED="1426092707961" MODIFIED="1426092889900"/>
<node TEXT="The PM has to be declared in the BeanProvider and the recommended way is to declare it as a Prototype so the PM will only be created (instantiated) when the corresponding view is added to the stage" STYLE_REF="pre_last_node" ID="ID_1976065652" CREATED="1426092731312" MODIFIED="1426092889902">
<node TEXT="&lt;?xml version=&quot;1.0&quot; encoding=&quot;utf-8&quot;?&gt;&#xa;&lt;swiz:BeanProvider xmlns:swiz=&quot;http://swiz.swizframework.org&quot; xmlns:fx=&quot;http://ns.adobe.com/mxml/2009&quot;&gt;&#xa;    &lt;fx:Script&gt;&#xa;        &lt;![CDATA[&#xa;            import example.model.presentation.MyPresentationModel;&#xa;        ]]&gt;&#xa;    &lt;/fx:Script&gt;&#xa;    &lt;swiz:Prototype type=&quot;{MyPresentationModel}&quot; /&gt;&#xa; &#xa;&lt;/swiz:BeanProvider&gt;" STYLE_REF="example_node" ID="ID_727132077" CREATED="1426092738974" MODIFIED="1426092895144"/>
</node>
<node TEXT="The view gets the PM injected and binds data from&#xa;it and delegates all interactions (events) to the PM" STYLE_REF="pre_last_node" ID="ID_676780005" CREATED="1426092759644" MODIFIED="1426092863699">
<node TEXT="&lt;?xml version=&quot;1.0&quot; encoding=&quot;utf-8&quot;?&gt;&#xa;&lt;s:SkinnableContainer xmlns:fx=&quot;http://ns.adobe.com/mxml/2009&quot; xmlns:s=&quot;library://ns.adobe.com/flex/spark&quot; xmlns:mx=&quot;library://ns.adobe.com/flex/mx&quot; currentState=&quot;{model.currentState}&quot;&gt;&#xa;    &lt;fx:Script&gt;&#xa;        &lt;![CDATA[&#xa;            import example.model.presentation.MyPresentationModel;&#xa; &#xa;            [Bindable]&#xa;            [Inject]&#xa;            public var model:MyPresentationModel;&#xa; &#xa;            [PreDestroy]&#xa;            public function cleanUp():void {&#xa;               model.destroy();&#xa;            }&#xa;        ]]&gt;&#xa;    &lt;/fx:Script&gt;&#xa; &#xa;    &lt;s:layout&gt;&#xa;        &lt;s:VerticalLayout /&gt;&#xa;    &lt;/s:layout&gt;&#xa; &#xa;    &lt;s:states&gt;&#xa;        &lt;s:State name=&quot;default&quot; /&gt;&#xa;        &lt;s:State name=&quot;detail&quot; /&gt;&#xa;    &lt;/s:states&gt;&#xa; &#xa;    &lt;s:List labelField=&quot;{model.listLabelField}&quot; dataProvider=&quot;{model.dataProvider}&quot; change=&quot;model.changeListIndex(event.newIndex)&quot; /&gt;&#xa;    &lt;s:Label includeIn=&quot;detail&quot; text=&quot;{model.selectedItem.title}&quot; /&gt;&#xa;    &lt;s:Button includeIn=&quot;detail&quot; label=&quot;Edit&quot; click=&quot;model.edit()&quot; /&gt;&#xa;&lt;/s:SkinnableContainer&gt;" STYLE_REF="example_node" ID="ID_475648839" CREATED="1426092768197" MODIFIED="1426092954017"/>
</node>
<node TEXT="The PM is a independent unit and only gets&#xa;data injected from the application layer" STYLE_REF="pre_last_node" ID="ID_1477834027" CREATED="1426092796970" MODIFIED="1426092880705">
<node TEXT="package example.model.presentation {&#xa; &#xa;    import flash.events.IEventDispatcher;&#xa;    import org.swizframework.events.BeanEvent;&#xa; &#xa;    public class MyPresentationModel {&#xa;        [Dispatcher]&#xa;        public var dispatcher:IEventDispatcher;&#xa; &#xa;        [Bindable]&#xa;        public var currentState:String;&#xa; &#xa;        [Bindable]&#xa;        [Inject(&quot;appModel.list&quot;)]&#xa;        public var dataProvider:IList;&#xa; &#xa;        [Bindable]&#xa;        public var selectedItem:Object;&#xa; &#xa;        public function MyPresentationModel() {&#xa;        }&#xa; &#xa;        public function changeListIndex(index:int):void {&#xa;            if(index != -1){&#xa;                currentState = &quot;edit&quot;;&#xa;                selectedItem = dataProvider.getItemAt(index);&#xa;            } else {&#xa;                currentState = &quot;default&quot;;&#xa;                selectedItem = null;&#xa;            }&#xa;        }&#xa; &#xa;        public function edit():void {&#xa;            dispatcher.dispatchEvent(new MyEvent(MyEvent.EDIT, selectedItem));&#xa;        }&#xa; &#xa;        public function destroy():void {&#xa;            dispatcher.dispatchEvent( new BeanEvent(BeanEvent.TEAR_DOWN_BEAN, this) );&#xa;        }&#xa; &#xa;    }&#xa;}" STYLE_REF="example_node" ID="ID_1962471938" CREATED="1426092806525" MODIFIED="1426092980835"/>
</node>
</node>
</node>
</map>
