<map version="freeplane 1.3.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Design patterns" STYLE_REF="main_node" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1425913919027">
<hook NAME="AutomaticEdgeColor" COUNTER="2"/>
<hook NAME="MapStyle" zoom="1.003">
    <properties show_icon_for_attributes="true" show_note_icons="true"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
<stylenode TEXT="main_node" BACKGROUND_COLOR="#ccccff">
<font SIZE="18" BOLD="true"/>
<edge STYLE="sharp_bezier" WIDTH="4"/>
</stylenode>
<stylenode TEXT="second_node" BACKGROUND_COLOR="#00cccc" STYLE="bubble">
<font SIZE="14"/>
</stylenode>
<stylenode TEXT="third_node" BACKGROUND_COLOR="#ffbdf4" STYLE="bubble">
<font SIZE="11" BOLD="true"/>
</stylenode>
<stylenode TEXT="orange_node" BACKGROUND_COLOR="#ff9900" STYLE="bubble">
<font BOLD="true"/>
</stylenode>
<stylenode TEXT="gray_node" BACKGROUND_COLOR="#9ec19e">
<font BOLD="true"/>
</stylenode>
<stylenode TEXT="yellow_node" BACKGROUND_COLOR="#ffff66" STYLE="bubble">
<edge WIDTH="1"/>
</stylenode>
<stylenode TEXT="example_node" BACKGROUND_COLOR="#afcdfa">
<edge STYLE="bezier" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="pre_last_node" BACKGROUND_COLOR="#99ff99" STYLE="bubble">
<edge STYLE="bezier" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="last_node" BACKGROUND_COLOR="#ffffff">
<edge STYLE="bezier" WIDTH="thin"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<node TEXT="Other patterns" STYLE_REF="second_node" POSITION="right" ID="ID_1445943299" CREATED="1425920773391" MODIFIED="1425920796186">
<edge COLOR="#010189"/>
<node TEXT="Dependency injection" STYLE_REF="second_node" ID="ID_1765363629" CREATED="1425914076095" MODIFIED="1427364031535" BACKGROUND_COLOR="#ffbdf4">
<font SIZE="11" BOLD="true"/>
<hook URI="../../../../../D:/Documents/MindMaps/Images/injection.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Dependency Injection is a practice where objects are designed in a&#xa;manner where they receive instances of the objects from other pieces&#xa;of code, instead of constructing them internally" STYLE_REF="pre_last_node" ID="ID_1384712708" CREATED="1425914473053" MODIFIED="1425920505752">
<icon BUILTIN="idea"/>
</node>
<node TEXT="Advantages" STYLE_REF="orange_node" ID="ID_962540642" CREATED="1425915129644" MODIFIED="1425920467921">
<icon BUILTIN="addition"/>
<node TEXT="DI doesn&apos;t require any change in code behavior&#xa;it can be applied to legacy code as a refactoring" STYLE_REF="pre_last_node" ID="ID_1634716683" CREATED="1425915176857" MODIFIED="1425920079185">
<node ID="ID_801636521" CREATED="1425919984016" MODIFIED="1425919984016"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#160;The result is clients that are more independent and that are easier to unit test in isolation using stubs or mock objects that simulate other objects not under test
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="DI allows a client to remove all knowledge of a&#xa;concrete implementation that it needs to use" STYLE_REF="pre_last_node" ID="ID_1328502001" CREATED="1425915255058" MODIFIED="1425920079191">
<node TEXT="This helps isolate the client from the impact of design changes&#xa;and defects. It promotes reusability, testability and maintainability" ID="ID_1488094436" CREATED="1425919959135" MODIFIED="1425920059819"/>
</node>
<node TEXT="DI can be used to externalize a system&apos;s configuration&#xa;details into configuration files allowing the system to be&#xa;reconfigured without recompilation" STYLE_REF="pre_last_node" ID="ID_202082849" CREATED="1425915315613" MODIFIED="1425920079197"/>
<node TEXT="DI allows concurrent or independent development" STYLE_REF="pre_last_node" ID="ID_1583669779" CREATED="1425915368476" MODIFIED="1425920079198">
<node TEXT="Two developers can independently develop classes that use each other, while only needing to know the interface the classes will communicate through" ID="ID_800826075" CREATED="1425919918331" MODIFIED="1425920064905"/>
</node>
<node TEXT="DI decreases coupling between a class and its dependency" STYLE_REF="pre_last_node" ID="ID_544698717" CREATED="1425915387673" MODIFIED="1425920079185"/>
</node>
<node TEXT="Disadvantages" STYLE_REF="orange_node" ID="ID_1614558123" CREATED="1425915402850" MODIFIED="1425920476373">
<icon BUILTIN="subtraction"/>
<node TEXT="DI can make code difficult to trace (read) because&#xa;it separates behavior from construction" STYLE_REF="pre_last_node" ID="ID_1915868535" CREATED="1425915432878" MODIFIED="1425920132059"/>
<node TEXT="DI typically requires more lines of code to accomplish&#xa;the same behavior legacy code would" STYLE_REF="pre_last_node" ID="ID_756930905" CREATED="1425915445241" MODIFIED="1425920476370"/>
<node TEXT="DI diminishes encapsulation by requiring users of a system &#xa;to know how it works and not merely what it does" STYLE_REF="pre_last_node" ID="ID_761651129" CREATED="1425915468063" MODIFIED="1425920132057"/>
</node>
<node TEXT="Elements" STYLE_REF="orange_node" ID="ID_731283535" CREATED="1425919274751" MODIFIED="1425919488930">
<node TEXT="Service object" STYLE_REF="gray_node" ID="ID_477744919" CREATED="1425919278531" MODIFIED="1425919496127"/>
<node TEXT="Client object" STYLE_REF="gray_node" ID="ID_1667742399" CREATED="1425919292331" MODIFIED="1425919496128">
<node TEXT="It depends from the service" STYLE_REF="pre_last_node" ID="ID_563629286" CREATED="1425919394408" MODIFIED="1425920554583"/>
<node TEXT="Forms" STYLE_REF="yellow_node" ID="ID_1869679039" CREATED="1425914675259" MODIFIED="1425919889200">
<font BOLD="true"/>
<node TEXT="Constructor-based" STYLE_REF="gray_node" ID="ID_513822557" CREATED="1425914695129" MODIFIED="1425920260420">
<node TEXT="It&#x2019;s the technique of passing objects dependencies to its constructor" STYLE_REF="pre_last_node" ID="ID_2908578" CREATED="1425915027780" MODIFIED="1425917882709"/>
<node TEXT="// Constructor&#xa;Client(Service service) {&#xa;    // Save the reference to the passed-in service inside this client&#xa;    this.service = service;&#xa;}" STYLE_REF="example_node" ID="ID_1957971648" CREATED="1425917502187" MODIFIED="1425917510971"/>
<node TEXT="When use" STYLE_REF="yellow_node" ID="ID_81351033" CREATED="1425918414814" MODIFIED="1425920275447">
<node TEXT="Preferred when all dependencies can be constructed first because it can be used to ensure the client object is always in a valid state, as opposed to having some of its dependency references be null (not be set). However, on its own, it lacks the flexibility to have its dependencies changed later" ID="ID_626483794" CREATED="1425918453395" MODIFIED="1425918456281"/>
</node>
</node>
<node TEXT="Interface-based" STYLE_REF="gray_node" ID="ID_857935221" CREATED="1425914683271" MODIFIED="1425920260423">
<node TEXT="The dependency provides an injector method that will inject the dependency&#xa;into any client passed to it. Clients must implement an interface that exposes&#xa;a setter method that accepts the dependency" STYLE_REF="pre_last_node" ID="ID_401925519" CREATED="1425917819560" MODIFIED="1425920294558"/>
<node TEXT="// Service setter interface&#xa;public interface ServiceSetter {&#xa;    public void setService(Service service);&#xa;}&#xa; &#xa;// Client class&#xa;public class Client implements ServiceSetter {&#xa;    // Internal reference to the service used by this client.&#xa;    private Service service;&#xa; &#xa;    // Set the service that this client is to use.&#xa;    @Override&#xa;    public void setService(Service service) {&#xa;        this.service = service;&#xa;    }&#xa;}" STYLE_REF="example_node" ID="ID_506482771" CREATED="1425917655944" MODIFIED="1425917677191"/>
<node TEXT="When use" STYLE_REF="yellow_node" ID="ID_682842598" CREATED="1425919774925" MODIFIED="1425920275446">
<node TEXT="The advantage of interface injection is that dependencies can be completely ignorant of their clients yet can still receive a reference to a new client and, using it, send a reference-to-self back to the client. In this way, the dependencies become injectors" ID="ID_494387824" CREATED="1425919778144" MODIFIED="1425920325057"/>
<node TEXT="An assembler is still needed to introduce the client and its dependencies. The assembler would take a reference to the client, cast it to the setter interface that sets that dependency, and pass it to that dependency object which would turn around and pass a reference-to-self back to the client" ID="ID_690396074" CREATED="1425919830523" MODIFIED="1425919833383"/>
</node>
</node>
<node TEXT="Setter-based" STYLE_REF="gray_node" ID="ID_1320137036" CREATED="1425914678550" MODIFIED="1425920260424">
<node TEXT="The client exposes a setter method that the injector uses to inject the dependency" STYLE_REF="pre_last_node" ID="ID_796714233" CREATED="1425917404388" MODIFIED="1425917949811"/>
<node TEXT="// Setter method&#xa;public void setService(Service service) {&#xa;    // Save the reference to the passed-in service inside this client&#xa;    this.service = service;&#xa;}" STYLE_REF="example_node" ID="ID_853072843" CREATED="1425917551017" MODIFIED="1425917556442"/>
<node TEXT="When use" STYLE_REF="yellow_node" ID="ID_1483621982" CREATED="1425917415228" MODIFIED="1425920275444">
<node TEXT="Suppose you have a class with ten methods that have no dependencies, but you&#x2019;re adding a new method that does have a dependency on IDAO. You could change the constructor to use Constructor Injection, but this may force you to changes to all constructor calls all over the place. Alternatively, you could just add a new constructor that takes the dependency, but then how does a developer easily know when to use one constructor over the other. In such case, a better technique is setter Injection" ID="ID_1880636561" CREATED="1425917268036" MODIFIED="1425917343491"/>
<node TEXT="Requires the client provide a setter method for each dependency. This gives the freedom to manipulate the state of the dependency references at any time. This offers flexibility, but if there is more than one dependency to be injected, it is difficult for the client to ensure that all dependencies are injected before the client could be provided for use" ID="ID_1118276750" CREATED="1425918967039" MODIFIED="1425918970252"/>
</node>
</node>
</node>
</node>
<node TEXT="Interface" STYLE_REF="gray_node" ID="ID_1208968199" CREATED="1425919306311" MODIFIED="1425919496128">
<node TEXT="The client uses it to communicate with the service" STYLE_REF="pre_last_node" ID="ID_1703534297" CREATED="1425919412056" MODIFIED="1425919541459"/>
</node>
<node TEXT="Injector object" STYLE_REF="gray_node" ID="ID_953100236" CREATED="1425919336485" MODIFIED="1425919496123">
<node TEXT="Which is responsible for injecting the service into the client" STYLE_REF="pre_last_node" ID="ID_625942037" CREATED="1425919421250" MODIFIED="1425919552065"/>
<node TEXT="Known as" STYLE_REF="pre_last_node" ID="ID_1077934239" CREATED="1425919455525" MODIFIED="1425919504535">
<node TEXT="Assembler" ID="ID_1676580916" CREATED="1425919461291" MODIFIED="1425919465237"/>
<node TEXT="Provider" ID="ID_1589520150" CREATED="1425919470439" MODIFIED="1425919483284"/>
<node TEXT="Container" ID="ID_1418851904" CREATED="1425919476237" MODIFIED="1425919479510"/>
</node>
<node TEXT="Forms" STYLE_REF="yellow_node" ID="ID_929748821" CREATED="1425919626248" MODIFIED="1425919727530">
<font BOLD="true"/>
<node TEXT="public class Injector {&#xa;    public static void main(String[] args) {&#xa;        // Build the dependencies first&#xa;        Service service = new ServiceExample();&#xa; &#xa;        // Inject the service, constructor style&#xa;        Client client = new Client(service);&#xa; &#xa;        // Use the objects&#xa;        System.out.println(client.greet());&#xa;    }&#xa;}" STYLE_REF="example_node" ID="ID_949313113" CREATED="1425918503680" MODIFIED="1425918532268"/>
</node>
</node>
</node>
</node>
</node>
</node>
</map>
