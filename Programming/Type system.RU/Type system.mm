<map version="freeplane 1.3.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="&#x42f;&#x437;&#x44b;&#x43a;&#x43e;&#x432;&#x430;&#x44f; &#x442;&#x438;&#x43f;&#x438;&#x437;&#x430;&#x446;&#x438;&#x44f;" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1418667045144" BACKGROUND_COLOR="#ffff00">
<font SIZE="18" BOLD="true"/>
<hook NAME="AutomaticEdgeColor" COUNTER="4"/>
<hook NAME="MapStyle" zoom="1.003">

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
<stylenode TEXT="second_node" BACKGROUND_COLOR="#00ff66" STYLE="bubble">
<font SIZE="14"/>
</stylenode>
<stylenode TEXT="pre_last_node" BACKGROUND_COLOR="#99ff99" STYLE="bubble">
<edge STYLE="bezier" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="yellow_node" BACKGROUND_COLOR="#ffff33" STYLE="bubble"/>
<stylenode TEXT="orange_node" BACKGROUND_COLOR="#ff9900">
<font BOLD="true"/>
</stylenode>
<stylenode TEXT="last_node">
<edge STYLE="bezier" WIDTH="thin"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<edge STYLE="sharp_bezier" COLOR="#660000" WIDTH="4"/>
<hook URI="main.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x422;&#x438;&#x43f;&#x438;&#x437;&#x438;&#x440;&#x43e;&#x432;&#x430;&#x43d;&#x43d;&#x44b;&#x435;" STYLE_REF="second_node" POSITION="right" ID="ID_737747471" CREATED="1418651854693" MODIFIED="1418663649152" VGAP="15">
<edge COLOR="#660000"/>
<node TEXT="Static / dynamic" STYLE_REF="orange_node" ID="ID_1906062532" CREATED="1418651926173" MODIFIED="1418662570829" VGAP="26">
<font SIZE="12" BOLD="false"/>
<node TEXT="&#x421;&#x442;&#x430;&#x442;&#x438;&#x447;&#x435;&#x441;&#x43a;&#x430;&#x44f; &#x442;&#x438;&#x43f;&#x438;&#x437;&#x430;&#x446;&#x438;&#x44f;" STYLE_REF="yellow_node" ID="ID_1910923056" CREATED="1418660097633" MODIFIED="1418994060685"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1055;&#1077;&#1088;&#1077;&#1084;&#1077;&#1085;&#1085;&#1072;&#1103; &#1089;&#1074;&#1103;&#1079;&#1099;&#1074;&#1072;&#1077;&#1090;&#1089;&#1103; &#1089; &#1090;&#1080;&#1087;&#1086;&#1084; &#1074; &#1084;&#1086;&#1084;&#1077;&#1085;&#1090;
    </p>
    <p>
      &#1086;&#1073;&#1098;&#1103;&#1074;&#1083;&#1077;&#1085;&#1080;&#1103; &#1080; &#1090;&#1080;&#1087; &#1085;&#1077; &#1084;&#1086;&#1078;&#1077;&#1090; &#1073;&#1099;&#1090;&#1100; &#1080;&#1079;&#1084;&#1077;&#1085;&#1105;&#1085; &#1087;&#1086;&#1079;&#1078;&#1077;
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true"/>
<hook NAME="FirstGroupNode"/>
<hook URI="../../Images/static.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x41f;&#x440;&#x43e;&#x432;&#x435;&#x440;&#x43a;&#x430; &#x442;&#x438;&#x43f;&#x43e;&#x432; &#x43e;&#x434;&#x438;&#x43d; &#x440;&#x430;&#x437;&#xa;&#x43f;&#x440;&#x438; &#x43a;&#x43e;&#x43c;&#x43f;&#x438;&#x43b;&#x44f;&#x446;&#x438;&#x438;" STYLE_REF="pre_last_node" ID="ID_276737335" CREATED="1418660260541" MODIFIED="1418660397400">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="80" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_610429019" STARTINCLINATION="83;0;" ENDINCLINATION="83;0;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
</node>
<node TEXT="&#x411;&#x43e;&#x43b;&#x44c;&#x448;&#x430;&#x44f; &#x441;&#x43a;&#x43e;&#x440;&#x43e;&#x441;&#x442;&#x44c; &#x432;&#x44b;&#x43f;&#x43e;&#x43b;&#x43d;&#x435;&#x43d;&#x438;&#x438; &#x43a;&#x43e;&#x434;&#x430;" STYLE_REF="pre_last_node" ID="ID_610429019" CREATED="1418660301386" MODIFIED="1418660373953"/>
<node TEXT="&#x41e;&#x431;&#x43d;&#x430;&#x440;&#x443;&#x436;&#x435;&#x43d;&#x438;&#x435; &#x43e;&#x448;&#x44b;&#x431;&#x43e;&#x43a; &#x43d;&#x430; &#x44d;&#x442;&#x430;&#x43f;&#x435; &#x43a;&#x43e;&#x43c;&#x43f;&#x438;&#x43b;&#x44f;&#x446;&#x438;&#x438;" STYLE_REF="pre_last_node" ID="ID_597094658" CREATED="1418660325665" MODIFIED="1418660403092">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="80" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_1946921183" STARTINCLINATION="54;0;" ENDINCLINATION="54;0;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
</node>
<node TEXT="&#x423;&#x441;&#x43a;&#x43e;&#x440;&#x435;&#x43d;&#x438;&#x435; &#x440;&#x430;&#x437;&#x440;&#x430;&#x431;&#x43e;&#x442;&#x43a;&#x438; &#x441; &#x445;&#x43e;&#x440;&#x43e;&#x448;&#x435;&#x439; IDE" STYLE_REF="pre_last_node" ID="ID_1946921183" CREATED="1418660351325" MODIFIED="1418661090521"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1069;&#1092;&#1077;&#1082;&#1090;&#1080;&#1074;&#1085;&#1086;&#1077; &#1072;&#1074;&#1090;&#1086; &#1076;&#1086;&#1087;&#1086;&#1083;&#1085;&#1077;&#1085;&#1080;&#1077;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&#x41c;&#x43d;&#x43e;&#x433;&#x43e; &#x431;&#x443;&#x43a;&#x430;&#x444;" STYLE_REF="pre_last_node" ID="ID_1369603468" CREATED="1418661142972" MODIFIED="1418661191131"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1050;&#1072;&#1078;&#1076;&#1099;&#1081; &#1088;&#1072;&#1079; &#1085;&#1091;&#1078;&#1085;&#1086; &#1074;&#1074;&#1086;&#1076;&#1080;&#1090;&#1100; &#1090;&#1080;&#1087; &#1087;&#1077;&#1088;&#1077;&#1084;&#1077;&#1085;&#1085;&#1086;&#1081;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="&#x421;++, &#x421;#, Pascal, Java" ID="ID_1850578487" CREATED="1418661357962" MODIFIED="1418661545868">
<hook NAME="SummaryNode"/>
<font BOLD="true"/>
</node>
<node TEXT="&#x414;&#x438;&#x43d;&#x430;&#x43c;&#x438;&#x447;&#x435;&#x441;&#x43a;&#x430;&#x44f; &#x442;&#x438;&#x43f;&#x438;&#x437;&#x430;&#x446;&#x438;&#x44f;" STYLE_REF="yellow_node" ID="ID_1241404009" CREATED="1418660112925" MODIFIED="1418994068123"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1055;&#1077;&#1088;&#1077;&#1084;&#1077;&#1085;&#1085;&#1072;&#1103; &#1089;&#1074;&#1103;&#1079;&#1099;&#1074;&#1072;&#1077;&#1090;&#1089;&#1103; &#1089; &#1090;&#1080;&#1087;&#1086;&#1084;
    </p>
    <p>
      &#1074; &#1084;&#1086;&#1084;&#1077;&#1085;&#1090; &#1087;&#1088;&#1080;&#1089;&#1074;&#1072;&#1080;&#1074;&#1072;&#1085;&#1080;&#1103; &#1079;&#1085;&#1072;&#1095;&#1077;&#1085;&#1080;&#1103;
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true"/>
<hook NAME="FirstGroupNode"/>
<hook URI="../../Images/dynamic.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x41f;&#x440;&#x43e;&#x441;&#x442;&#x43e;&#x442;&#x430; &#x441;&#x43e;&#x437;&#x434;&#x430;&#x43d;&#x438;&#x439; &#x443;&#x43d;&#x438;&#x432;&#x435;&#x440;&#x441;&#x430;&#x43b;&#x44c;&#x43d;&#x44b;&#x445; &#x43a;&#x43e;&#x43b;&#x435;&#x43a;&#x446;&#x438;&#x439;" STYLE_REF="pre_last_node" ID="ID_1056111512" CREATED="1418660411055" MODIFIED="1418660480563"/>
<node TEXT="&#x41e;&#x431;&#x43e;&#x431;&#x449;&#x435;&#x43d;&#x43d;&#x44b;&#x435; &#x430;&#x43b;&#x433;&#x43e;&#x440;&#x438;&#x442;&#x43c;&#x44b;" STYLE_REF="pre_last_node" ID="ID_1399767547" CREATED="1418660442188" MODIFIED="1418660480577"/>
<node TEXT="&#x41b;&#x435;&#x433;&#x43a;&#x43e;&#x441;&#x442;&#x44c; &#x432; &#x43e;&#x441;&#x432;&#x43e;&#x435;&#x43d;&#x438;&#x438;" STYLE_REF="pre_last_node" ID="ID_636742923" CREATED="1418660460339" MODIFIED="1418660480582"/>
<node TEXT="&#x41f;&#x43e;&#x434;&#x434;&#x435;&#x440;&#x436;&#x43a;&#x430; &#x432; &#x43e;&#x441;&#x43d;&#x43e;&#x432;&#x43d;&#x43e;&#x43c; &#x438;&#x43d;&#x442;&#x435;&#x440;&#x43f;&#x440;&#x435;&#x442;&#x438;&#x440;&#x43e;&#x432;&#x430;&#x43d;&#x43d;&#x44b;&#x43c;&#x438; &#x44f;&#x437;&#x44b;&#x43a;&#x430;&#x43c;&#x438;" STYLE_REF="pre_last_node" ID="ID_1404797216" CREATED="1418660648518" MODIFIED="1418661028570"/>
<node TEXT="&#x41f;&#x440;&#x43e;&#x431;&#x43b;&#x435;&#x43c;&#x430; &#x441;&#x440;&#x430;&#x432;&#x43d;&#x435;&#x43d;&#x438;&#x44f; &#x432;&#x435;&#x43b;&#x438;&#x447;&#x438;&#x43d;.&#xa;&#x421;&#x443;&#x449;&#x435;&#x441;&#x442;&#x432;&#x443;&#x435;&#x442; == &#x438; ===" STYLE_REF="pre_last_node" ID="ID_184574810" CREATED="1418660965714" MODIFIED="1418661004068"/>
<node TEXT="&#x422;&#x440;&#x443;&#x434;&#x43d;&#x43e; &#x443;&#x43b;&#x43e;&#x432;&#x438;&#x43c;&#x44b;&#x435; &#x43e;&#x448;&#x438;&#x431;&#x43a;&#x438;" STYLE_REF="pre_last_node" ID="ID_1066061178" CREATED="1418661260960" MODIFIED="1418661292305"/>
</node>
<node TEXT="Smalltalk, Python, Objective-C, Ruby, PHP, JavaScript" ID="ID_357429500" CREATED="1418661305203" MODIFIED="1418661548204">
<hook NAME="SummaryNode"/>
<font BOLD="true"/>
</node>
</node>
<node TEXT="Strong / weak" STYLE_REF="orange_node" ID="ID_525820181" CREATED="1418661560412" MODIFIED="1418662379466" VGAP="63">
<font SIZE="12" BOLD="false"/>
<node TEXT="&#x421;&#x438;&#x43b;&#x44c;&#x43d;&#x430;&#x44f; / &#x441;&#x442;&#x440;&#x43e;&#x433;&#x430;&#x44f; &#x442;&#x438;&#x43f;&#x438;&#x437;&#x430;&#x446;&#x438;&#x44f;" STYLE_REF="yellow_node" ID="ID_239144647" CREATED="1418661777826" MODIFIED="1418994110284">
<font BOLD="true"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1048;&#1089;&#1082;&#1083;&#1102;&#1095;&#1072;&#1077;&#1090; &#1072;&#1074;&#1090;&#1086; &#1087;&#1088;&#1077;&#1086;&#1073;&#1088;&#1072;&#1079;&#1086;&#1074;&#1072;&#1085;&#1080;&#1103; &#1090;&#1080;&#1087;&#1086;&#1074;.
    </p>
    <p>
      &#1042;&#1089;&#1077; &#1087;&#1088;&#1077;&#1086;&#1073;&#1088;&#1072;&#1079;&#1086;&#1074;&#1072;&#1085;&#1080;&#1103; &#1085;&#1091;&#1078;&#1085;&#1086; &#1076;&#1077;&#1083;&#1072;&#1090;&#1100; &#1103;&#1074;&#1085;&#1086;
    </p>
  </body>
</html>
</richcontent>
<hook NAME="FirstGroupNode"/>
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="3" TRANSPARENCY="140" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_870969643" MIDDLE_LABEL="&#x427;&#x438;&#x441;&#x43b;&#x43e;&#x432;&#x43e;&#x439; &#x43b;&#x438;&#x442;&#x435;&#x440;&#x430;&#x43b; &#x431;&#x435;&#x437; &#x442;&#x438;&#x43f;&#x430; &#x432; Haskell.&#xa;Pi + 1" STARTINCLINATION="479;305;" ENDINCLINATION="426;184;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="4" TRANSPARENCY="200" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_870969643" MIDDLE_LABEL="&#x41e;&#x433;&#x440;&#x430;&#x43d;&#x438;&#x447;&#x435;&#x43d;&#x43e;&#x435; &#x43d;&#x435;&#x44f;&#x432;&#x43d;&#x43e;&#x435; &#x43f;&#x440;&#x435;&#x43e;&#x431;&#x440;&#x430;&#x437;&#x43e;&#x432;&#x430;&#x43d;&#x438;&#x435;&#xa;int -&gt; float, float &#x43d;&#x435; -&gt; int" STARTINCLINATION="618;85;" ENDINCLINATION="618;85;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
<hook URI="../../Images/strong.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x41d;&#x430;&#x434;&#x435;&#x436;&#x43d;&#x430;&#x44f;. &#x418;&#x441;&#x43a;&#x43b;&#x44e;&#x447;&#x430;&#x435;&#x442; &#x43e;&#x448;&#x438;&#x431;&#x43a;&#x438;" STYLE_REF="pre_last_node" ID="ID_1469049783" CREATED="1418661979879" MODIFIED="1418662083005"/>
<node TEXT="&#x421;&#x43a;&#x43e;&#x440;&#x43e;&#x441;&#x442;&#x44c; &#x432;&#x44b;&#x43f;&#x43e;&#x43b;&#x43d;&#x435;&#x43d;&#x438;&#x44f; &#x431;&#x43e;&#x43b;&#x44c;&#x448;&#x435;" STYLE_REF="pre_last_node" ID="ID_1241045851" CREATED="1418662000108" MODIFIED="1418662082997"/>
<node TEXT="&#x41f;&#x43e;&#x43d;&#x438;&#x43c;&#x430;&#x43d;&#x438;&#x435; &#x440;&#x430;&#x431;&#x43e;&#x442;&#x44b; &#x43f;&#x440;&#x43e;&#x433;&#x440;&#x430;&#x43c;&#x43c;&#x44b;" STYLE_REF="pre_last_node" ID="ID_897370791" CREATED="1418662018706" MODIFIED="1418662082990"/>
<node TEXT="&#x41e;&#x43f;&#x440;&#x435;&#x434;&#x435;&#x43b;&#x435;&#x43d;&#x43d;&#x43e;&#x441;&#x442;&#x44c;. &#x41d;&#x443;&#x436;&#x43d;&#x43e;&#x435; &#x43f;&#x43e;&#x432;&#x435;&#x434;&#x435;&#x43d;&#x438;&#x435;" STYLE_REF="pre_last_node" ID="ID_827269357" CREATED="1418662031457" MODIFIED="1418662082984"/>
<node TEXT="&#x41d;&#x438;&#x43a;&#x430;&#x43a;&#x438;&#x445; &#x430;&#x432;&#x442;&#x43e;&#x43f;&#x440;&#x435;&#x43e;&#x431;&#x440;&#x430;&#x437;&#x43e;&#x432;&#x430;&#x43d;&#x438;&#x439;" STYLE_REF="pre_last_node" ID="ID_608196007" CREATED="1418662048268" MODIFIED="1418662082971"/>
</node>
<node TEXT="Java, Python, Lisp" ID="ID_1748982029" CREATED="1418662315748" MODIFIED="1418662349906">
<hook NAME="SummaryNode"/>
<font BOLD="true"/>
</node>
<node TEXT="&#x421;&#x43b;&#x430;&#x431;&#x430;&#x44f; / &#x43d;&#x435; &#x441;&#x442;&#x440;&#x43e;&#x433;&#x430;&#x44f; &#x442;&#x438;&#x43f;&#x438;&#x437;&#x430;&#x446;&#x438;&#x44f;" STYLE_REF="yellow_node" ID="ID_870969643" CREATED="1418661803470" MODIFIED="1418994116660">
<font BOLD="true"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1044;&#1086;&#1087;&#1091;&#1089;&#1082;&#1072;&#1077;&#1090; &#1072;&#1074;&#1090;&#1086; &#1087;&#1088;&#1077;&#1086;&#1073;&#1088;&#1072;&#1079;&#1086;&#1074;&#1072;&#1085;&#1080;&#1103; &#1090;&#1080;&#1087;&#1086;&#1074;.
    </p>
    <p>
      &#1042;&#1086;&#1079;&#1084;&#1086;&#1078;&#1085;&#1099; &#1085;&#1077;&#1103;&#1074;&#1085;&#1099; &#1087;&#1088;&#1077;&#1086;&#1073;&#1088;&#1072;&#1079;&#1086;&#1074;&#1072;&#1085;&#1080;&#1103;
    </p>
  </body>
</html>
</richcontent>
<hook NAME="FirstGroupNode"/>
<hook URI="../../Images/weak.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x421;&#x43c;&#x435;&#x448;&#x438;&#x432;&#x430;&#x43d;&#x438;&#x435; &#x440;&#x430;&#x437;&#x43d;&#x44b;&#x445; &#x442;&#x438;&#x43f;&#x43e;&#x432;&#xa;&#x432; &#x43e;&#x434;&#x43d;&#x43e;&#x43c; &#x432;&#x44b;&#x440;&#x430;&#x436;&#x435;&#x43d;&#x438;&#x438;" STYLE_REF="pre_last_node" ID="ID_747723794" CREATED="1418661847064" MODIFIED="1418661955891"/>
<node TEXT="&#x410;&#x432;&#x442;&#x43e; &#x43f;&#x440;&#x435;&#x43e;&#x431;&#x440;&#x430;&#x437;&#x43e;&#x432;&#x430;&#x43d;&#x438;&#x435; &#x442;&#x438;&#x43f;&#x43e;&#x432;" STYLE_REF="pre_last_node" ID="ID_156964308" CREATED="1418661933489" MODIFIED="1418661955883"/>
<node TEXT="&#x41a;&#x440;&#x430;&#x442;&#x43a;&#x43e;&#x441;&#x442;&#x44c; &#x437;&#x430;&#x43f;&#x438;&#x441;&#x438;" STYLE_REF="pre_last_node" ID="ID_161312180" CREATED="1418661943687" MODIFIED="1418661955871"/>
</node>
<node TEXT="C, JS, PHP" ID="ID_1790767681" CREATED="1418662337809" MODIFIED="1418662349894">
<hook NAME="SummaryNode"/>
<font BOLD="true"/>
</node>
</node>
<node TEXT="Explicit / implicit" STYLE_REF="orange_node" ID="ID_1197723029" CREATED="1418663013182" MODIFIED="1418663649147" VGAP="22" HGAP="50" VSHIFT="80">
<font SIZE="12" BOLD="false"/>
<node TEXT="&#x42f;&#x432;&#x43d;&#x430;&#x44f;" STYLE_REF="yellow_node" ID="ID_1566873898" CREATED="1418663188297" MODIFIED="1418994180802">
<font BOLD="true"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1058;&#1080;&#1087;&#1099; &#1087;&#1077;&#1088;&#1077;&#1084;&#1077;&#1085;&#1085;&#1099;&#1093;
    </p>
    <p>
      &#1091;&#1082;&#1072;&#1079;&#1099;&#1074;&#1072;&#1102;&#1090;&#1089;&#1103; &#1103;&#1074;&#1085;&#1086;
    </p>
  </body>
</html>
</richcontent>
<hook NAME="FirstGroupNode"/>
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="3" TRANSPARENCY="180" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_409402756" MIDDLE_LABEL="&#x41d;&#x435; &#x44f;&#x432;&#x43d;&#x430;&#x44f; &#x442;&#x438;&#x43f;&#x438;&#x437;&#x430;&#x446;&#x438;&#x44f; &#x43f;&#x43e; &#x432;&#x44b;&#x431;&#x43e;&#x440;&#x443;&#xa;C++11:&#xa;int a, auto b = a + 3" STARTINCLINATION="642;50;" ENDINCLINATION="619;-65;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
<hook URI="explict.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x41d;&#x443;&#x436;&#x43d;&#x43e; &#x443;&#x43a;&#x430;&#x437;&#x44b;&#x432;&#x430;&#x442;&#x44c; &#x442;&#x438;&#x43f;&#x44b; &#x432;&#x441;&#x435;&#x445; var &amp; func" STYLE_REF="pre_last_node" ID="ID_445715535" CREATED="1418663217229" MODIFIED="1418663311900"/>
<node TEXT="&#x41b;&#x44e;&#x431;&#x430;&#x44f; &#x444;&#x443;&#x43d;&#x43a;&#x446;&#x438;&#x44f; &#x438;&#x43c;&#x435;&#x435;&#x442; &#x441;&#x438;&#x433;&#x43d;&#x430;&#x442;&#x443;&#x440;&#x443;" STYLE_REF="pre_last_node" ID="ID_1316106678" CREATED="1418663252657" MODIFIED="1418663311896"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      int add(int x, int y)
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&#x41d;&#x435; &#x43d;&#x443;&#x436;&#x43d;&#x43e; &#x437;&#x430;&#x43f;&#x43e;&#x43c;&#x438;&#x43d;&#x430;&#x442;&#x44c; &#x442;&#x438;&#x43f;&#x44b; &#x43f;&#x435;&#x440;&#x435;&#x43c;&#x435;&#x43d;&#x43d;&#x44b;&#x445;" STYLE_REF="pre_last_node" ID="ID_948960418" CREATED="1418663294397" MODIFIED="1418663311887"/>
</node>
<node TEXT="C++, D, C#" ID="ID_332487814" CREATED="1418663600276" MODIFIED="1418663622623">
<hook NAME="SummaryNode"/>
<font BOLD="true"/>
</node>
<node TEXT="&#x41d;&#x435; &#x44f;&#x432;&#x43d;&#x430;&#x44f;" STYLE_REF="yellow_node" ID="ID_409402756" CREATED="1418663201998" MODIFIED="1418994170922">
<font BOLD="true"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1058;&#1080;&#1087;&#1099; &#1085;&#1077; &#1091;&#1082;&#1072;&#1079;&#1099;&#1074;&#1072;&#1102;&#1090;&#1089;&#1103;,
    </p>
    <p>
      &#1072; &#1074;&#1099;&#1095;&#1080;&#1089;&#1083;&#1103;&#1102;&#1090;&#1089;&#1103; &#1082;&#1086;&#1084;&#1087;&#1080;&#1083;&#1103;&#1090;&#1086;&#1088;&#1086;&#1084;
    </p>
    <p>
      &#1080;&#1083;&#1080; &#1080;&#1085;&#1090;&#1077;&#1088;&#1087;&#1088;&#1077;&#1090;&#1072;&#1090;&#1086;&#1088;&#1086;&#1084;
    </p>
  </body>
</html>
</richcontent>
<hook NAME="FirstGroupNode"/>
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="3" TRANSPARENCY="140" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_1566873898" MIDDLE_LABEL="&#x42f;&#x432;&#x43d;&#x430;&#x44f; &#x442;&#x438;&#x43f;&#x438;&#x437;&#x430;&#x446;&#x438;&#x44f; &#x43f;&#x43e; &#x432;&#x44b;&#x431;&#x43e;&#x440;&#x443;&#xa;&#x412;&#x43e;&#x437;&#x43c;&#x43e;&#x436;&#x43d;&#x43e;&#x441;&#x442;&#x44c; &#x443;&#x43a;&#x430;&#x437;&#x44b;&#x432;&#x430;&#x442;&#x44c; &#x442;&#x438;&#x43f; &#x437;&#x43d;&#x430;&#x447;&#x435;&#x43d;&#x438;&#x44f;&#xa;Haskell" STARTINCLINATION="691;331;" ENDINCLINATION="746;274;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
<hook URI="../../Images/implicit.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x41d;&#x435; &#x43d;&#x443;&#x436;&#x43d;&#x43e; &#x443;&#x43a;&#x430;&#x437;&#x44b;&#x432;&#x430;&#x442;&#x44c; &#x442;&#x438;&#x43f;&#x44b;" STYLE_REF="pre_last_node" ID="ID_1596854254" CREATED="1418663322407" MODIFIED="1418663365412"/>
<node TEXT="&#x421;&#x43e;&#x43a;&#x440;&#x430;&#x449;&#x435;&#x43d;&#x438;&#x435; &#x437;&#x430;&#x43f;&#x438;&#x441;&#x438;" STYLE_REF="pre_last_node" ID="ID_211138869" CREATED="1418663329888" MODIFIED="1418663365405"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      add(x, y)
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&#x423;&#x441;&#x442;&#x43e;&#x439;&#x447;&#x438;&#x432;&#x43e;&#x441;&#x442;&#x44c; &#x43a; &#x438;&#x437;&#x43c;&#x435;&#x43d;&#x435;&#x43d;&#x438;&#x44f;&#x43c;" STYLE_REF="pre_last_node" ID="ID_1182994466" CREATED="1418663349214" MODIFIED="1418663365396"/>
<node TEXT="&#x41c;&#x43e;&#x436;&#x435;&#x442; &#x431;&#x44b;&#x442;&#x44c; &#x441;&#x442;&#x430;&#x442;&#x438;&#x447;&#x435;&#x441;&#x43a;&#x43e;&#x439;" STYLE_REF="pre_last_node" ID="ID_1185754453" CREATED="1418663498581" MODIFIED="1418663508675"/>
</node>
<node TEXT="PHP, Lua, JS" ID="ID_730841253" CREATED="1418663625748" MODIFIED="1418663635447">
<hook NAME="SummaryNode"/>
<font BOLD="true"/>
</node>
</node>
<node TEXT="&#x41a;&#x432;&#x430;&#x434;&#x440;&#x430;&#x43d;&#x442; &#x442;&#x438;&#x43f;&#x438;&#x437;&#x430;&#x446;&#x438;&#x438;" STYLE_REF="orange_node" ID="ID_1119534115" CREATED="1418994204796" MODIFIED="1418994340686">
<hook URI="typing.png" SIZE="1.0" NAME="ExternalObject"/>
<font SIZE="12" BOLD="false"/>
</node>
</node>
<node TEXT="&#x41d;&#x435; &#x442;&#x438;&#x43f;&#x438;&#x437;&#x438;&#x440;&#x43e;&#x432;&#x430;&#x43d;&#x43d;&#x44b;&#x435;" STYLE_REF="second_node" POSITION="left" ID="ID_831722426" CREATED="1418651869979" MODIFIED="1418994082459" VGAP="19">
<edge COLOR="#660000"/>
<hook NAME="FirstGroupNode"/>
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="120" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_239144647" MIDDLE_LABEL="&#x421;&#x438;&#x43b;&#x44c;&#x43d;&#x430;&#x44f; &#x431;&#x435;&#x437;&#x442;&#x438;&#x43f;&#x43e;&#x432;&#x430;&#x44f; &#x442;&#x438;&#x43f;&#x438;&#x437;&#x430;&#x446;&#x438;&#x44f; Assembler 64 bit &#x43d;&#x435; -&gt; 16 bit" STARTINCLINATION="305;-206;" ENDINCLINATION="-299;-265;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
<node TEXT="&#x41f;&#x440;&#x435;&#x438;&#x43c;&#x443;&#x449;&#x435;&#x441;&#x442;&#x432;&#x430;" STYLE_REF="yellow_node" ID="ID_1443322184" CREATED="1418664119537" MODIFIED="1418664354568">
<icon BUILTIN="button_ok"/>
<font BOLD="true"/>
<node TEXT="&#x41f;&#x440;&#x43e;&#x437;&#x440;&#x430;&#x447;&#x43d;&#x43e;&#x441;&#x442;&#x44c; &#x438;&#x43d;&#x441;&#x442;&#x440;&#x443;&#x43a;&#x446;&#x438;&#x439;" STYLE_REF="pre_last_node" ID="ID_1009966774" CREATED="1418664156004" MODIFIED="1418664351262"/>
<node TEXT="&#x42d;&#x444;&#x444;&#x435;&#x43a;&#x442;&#x438;&#x432;&#x43d;&#x44b;&#x439; &#x43a;&#x43e;&#x434;" STYLE_REF="pre_last_node" ID="ID_79699943" CREATED="1418664295236" MODIFIED="1418664351258"/>
<node TEXT="&#x41d;&#x438;&#x437;&#x43a;&#x438;&#x439; &#x443;&#x440;&#x43e;&#x432;&#x435;&#x43d;&#x44c;" STYLE_REF="pre_last_node" ID="ID_1277370184" CREATED="1418664314004" MODIFIED="1418664351248"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1042;&#1086;&#1079;&#1084;&#1086;&#1078;&#1085;&#1099; &#1083;&#1102;&#1073;&#1099;&#1077; &#1086;&#1087;&#1077;&#1088;&#1072;&#1094;&#1080;&#1080;
    </p>
    <p>
      &#1085;&#1072;&#1076; &#1083;&#1102;&#1073;&#1099;&#1084;&#1080; &#1076;&#1072;&#1085;&#1085;&#1099;&#1084;&#1080;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="&#x41d;&#x435;&#x434;&#x43e;&#x441;&#x442;&#x430;&#x442;&#x43a;&#x438;" STYLE_REF="yellow_node" ID="ID_940900215" CREATED="1418664132762" MODIFIED="1418664265832">
<icon BUILTIN="button_cancel"/>
<font BOLD="true"/>
<node TEXT="&#x41d;&#x438;&#x437;&#x43a;&#x438;&#x439; &#x443;&#x440;&#x43e;&#x432;&#x435;&#x43d;&#x44c; &#x430;&#x431;&#x441;&#x442;&#x440;&#x430;&#x43a;&#x446;&#x438;&#x438;" STYLE_REF="pre_last_node" ID="ID_446072011" CREATED="1418664174364" MODIFIED="1418664261326"/>
<node TEXT="&#x41e;&#x442;&#x441;&#x443;&#x442;&#x441;&#x442;&#x432;&#x438;&#x435; &#x43f;&#x440;&#x43e;&#x432;&#x435;&#x440;&#x43e;&#x43a;" STYLE_REF="pre_last_node" ID="ID_1448592710" CREATED="1418664182994" MODIFIED="1418664261322"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1058;&#1088;&#1091;&#1076;&#1085;&#1086; &#1074;&#1099;&#1083;&#1086;&#1074;&#1080;&#1090;&#1100; &#1086;&#1096;&#1080;&#1073;&#1082;&#1080;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&#x421;&#x43b;&#x43e;&#x436;&#x43d;&#x43e; &#x43f;&#x440;&#x435;&#x434;&#x441;&#x442;&#x430;&#x432;&#x438;&#x442;&#x44c; &#x441;&#x43b;&#x43e;&#x436;&#x43d;&#x44b;&#x435; &#x434;&#x430;&#x43d;&#x43d;&#x44b;&#x435;" STYLE_REF="pre_last_node" ID="ID_596352094" CREATED="1418664228376" MODIFIED="1418664261314"/>
</node>
</node>
<node TEXT="Assembler, Forth" POSITION="left" ID="ID_1051689216" CREATED="1418664492697" MODIFIED="1418664530500">
<edge COLOR="#990000"/>
<hook NAME="SummaryNode"/>
<font BOLD="true"/>
</node>
</node>
</map>
