<map version="freeplane 1.3.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Java" STYLE_REF="main_node" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1436188158696" VGAP="10">
<hook NAME="AutomaticEdgeColor" COUNTER="11"/>
<hook NAME="MapStyle" zoom="0.912">
    <properties show_icon_for_attributes="true" show_note_icons="true"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
<stylenode TEXT="second_node" BACKGROUND_COLOR="#00cccc" STYLE="bubble">
<font SIZE="14"/>
</stylenode>
<stylenode TEXT="pre_last_node" BACKGROUND_COLOR="#99ff99" STYLE="bubble">
<edge STYLE="bezier" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="yellow_node" BACKGROUND_COLOR="#ffff66" STYLE="bubble">
<edge WIDTH="1"/>
</stylenode>
<stylenode TEXT="orange_node" BACKGROUND_COLOR="#ff9900" STYLE="bubble">
<font BOLD="true"/>
</stylenode>
<stylenode TEXT="last_node" BACKGROUND_COLOR="#ffffff">
<edge STYLE="bezier" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="main_node" BACKGROUND_COLOR="#ccccff">
<font SIZE="18" BOLD="true"/>
<edge STYLE="sharp_bezier" WIDTH="4"/>
</stylenode>
<stylenode TEXT="gray_node" BACKGROUND_COLOR="#9ec19e">
<font BOLD="true"/>
</stylenode>
<stylenode TEXT="example_node" BACKGROUND_COLOR="#afcdfa">
<edge STYLE="bezier" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="main_definition" BACKGROUND_COLOR="#ffccff">
<edge STYLE="bezier" WIDTH="thin"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook URI="../../Images/java-large.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Solutions" STYLE_REF="second_node" POSITION="right" ID="ID_1311526426" CREATED="1425648265302" MODIFIED="1425649700404" HGAP="30" VSHIFT="-10">
<edge STYLE="sharp_bezier" COLOR="#a82222" WIDTH="4"/>
<hook URI="../../Images/solution.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Measure execution time" STYLE_REF="orange_node" ID="ID_1429213229" CREATED="1425648489798" MODIFIED="1425648570858">
<edge WIDTH="2"/>
<node TEXT="long startTime = System.nanoTime();&#xa;myCall();&#xa;long stopTime = System.nanoTime();&#xa;System.out.println(stopTime - startTime);" STYLE_REF="example_node" ID="ID_1787852202" CREATED="1425648512990" MODIFIED="1425648537730"/>
<node TEXT="Instant start = Instant.now();&#xa;Thread.sleep(63553);&#xa;Instant end = Instant.now();&#xa;System.out.println(Duration.between(start, end));" STYLE_REF="example_node" ID="ID_288867856" CREATED="1425648522248" MODIFIED="1425648537724"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &gt;= Java 8
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Difference between == and equals" STYLE_REF="orange_node" ID="ID_79515189" CREATED="1425648659150" MODIFIED="1425648667747">
<node TEXT="==" STYLE_REF="yellow_node" ID="ID_1788065675" CREATED="1425648676926" MODIFIED="1425648714646">
<node TEXT="Tests for reference equality" STYLE_REF="pre_last_node" ID="ID_480416057" CREATED="1425648693094" MODIFIED="1425648721724"/>
</node>
<node TEXT="equals()" STYLE_REF="yellow_node" ID="ID_351792798" CREATED="1425648681783" MODIFIED="1425648714644">
<node TEXT="Tests for value equality" STYLE_REF="pre_last_node" ID="ID_1150482827" CREATED="1425648705510" MODIFIED="1425648721725"/>
</node>
<node TEXT="Examples" STYLE_REF="yellow_node" ID="ID_450757170" CREATED="1425648772814" MODIFIED="1425648785835">
<node TEXT="// These two have the same value&#xa;new String(&quot;test&quot;).equals(&quot;test&quot;) // --&gt; true&#xa;// ... but they are not the same object&#xa;new String(&quot;test&quot;) == &quot;test&quot; // --&gt; false&#xa;&#xa;String fooString1 = new String(&quot;foo&quot;);&#xa;String fooString2 = new String(&quot;foo&quot;);&#xa;// Evaluates to false&#xa;fooString1 == fooString2;&#xa;&#xa;// Evaluates to true&#xa;fooString1.equals(fooString2);&#xa;&#xa;// Evaluates to true, because Java uses the same object&#xa;&quot;bar&quot; == &quot;bar&quot;;" STYLE_REF="example_node" ID="ID_1433729792" CREATED="1425648732054" MODIFIED="1425648798892"/>
<node TEXT="But beware of nulls!&#xa;String nullString1 = null;&#xa;String nullString2 = null;&#xa;&#xa;// Evaluates to true&#xa;nullString1 == nullString2;&#xa;&#xa;// Throws an Exception&#xa;nullString1.equals(nullString2);" STYLE_REF="example_node" ID="ID_1096109277" CREATED="1425648770087" MODIFIED="1425648781095"/>
</node>
</node>
</node>
<node TEXT="Data types" STYLE_REF="second_node" POSITION="left" ID="ID_1692507035" CREATED="1436166922025" MODIFIED="1436179544574" VGAP="18">
<edge COLOR="#006666"/>
<hook URI="../../Images/type-2.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="A variable&apos;s data type determines the values it may contain, plus the operations that may be performed on it" STYLE_REF="main_definition" ID="ID_810883159" CREATED="1436172110624" MODIFIED="1436180220042"/>
<node TEXT="Primitive Data Types" STYLE_REF="orange_node" ID="ID_1315500763" CREATED="1436171483393" MODIFIED="1436171897183">
<hook URI="../../Images/primitive.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="byte" STYLE_REF="gray_node" ID="ID_609864763" CREATED="1436172124427" MODIFIED="1436172630709">
<node TEXT="The byte data type is an 8-bit signed two&apos;s complement integer. It has a minimum value of -128 and a maximum value of 127 (inclusive). The byte data type can be useful for saving memory in large arrays, where the memory savings actually matters. They can also be used in place of int where their limits help to clarify your code; the fact that a variable&apos;s range is limited can serve as a form of documentation." STYLE_REF="pre_last_node" ID="ID_1545308957" CREATED="1436172164568" MODIFIED="1436172250206"/>
<node TEXT="0" OBJECT="java.lang.Long|0" STYLE_REF="pre_last_node" ID="ID_420739446" CREATED="1425209098726" MODIFIED="1436172250205"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Default value
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="short" STYLE_REF="gray_node" ID="ID_1608196205" CREATED="1436172124427" MODIFIED="1436172630710">
<node TEXT="The short data type is a 16-bit signed two&apos;s complement integer. It has a minimum value of -32,768 and a maximum value of 32,767 (inclusive). As with byte, the same guidelines apply: you can use a short to save memory in large arrays, in situations where the memory savings actually matters." STYLE_REF="pre_last_node" ID="ID_713973653" CREATED="1436172243097" MODIFIED="1436172647057"/>
<node TEXT="0" OBJECT="java.lang.Long|0" STYLE_REF="pre_last_node" ID="ID_1965982654" CREATED="1425209098726" MODIFIED="1436172250205"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Default value
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="int" STYLE_REF="gray_node" ID="ID_1777871555" CREATED="1436172124430" MODIFIED="1436188465488">
<node TEXT="By default, the int data type is a 32-bit signed two&apos;s complement integer, which has a minimum value of -2^31 and a maximum value of 2^31-1. In Java SE 8 and later, you can use the int data type to represent an unsigned 32-bit integer, which has a minimum value of 0 and a maximum value of 2^32-1. Use the Integer class to use int data type as an unsigned integer. Static methods like compareUnsigned, divideUnsigned etc have been added to the Integer class to support the arithmetic operations for unsigned integers." STYLE_REF="pre_last_node" ID="ID_727813225" CREATED="1436172280976" MODIFIED="1436188465487"/>
<node TEXT="0" OBJECT="java.lang.Long|0" STYLE_REF="pre_last_node" ID="ID_659707469" CREATED="1425209098726" MODIFIED="1436172250205"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Default value
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="long" STYLE_REF="gray_node" ID="ID_772562294" CREATED="1436172124431" MODIFIED="1436172630710">
<node TEXT="The long data type is a 64-bit two&apos;s complement integer. The signed long has a minimum value of -2^63 and a maximum value of 2^63-1. In Java SE 8 and later, you can use the long data type to represent an unsigned 64-bit long, which has a minimum value of 0 and a maximum value of 2^64-1. Use this data type when you need a range of values wider than those provided by int. The Long class also contains methods like compareUnsigned, divideUnsigned etc to support arithmetic operations for unsigned long." STYLE_REF="pre_last_node" ID="ID_862777975" CREATED="1436172378016" MODIFIED="1436172647053"/>
<node TEXT="0L" STYLE_REF="pre_last_node" ID="ID_18542883" CREATED="1425209098726" MODIFIED="1436172388949"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Default value
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="float" STYLE_REF="gray_node" ID="ID_599054791" CREATED="1436172124432" MODIFIED="1436172630711">
<node TEXT="The float data type is a single-precision 32-bit IEEE 754 floating point. As with the recommendations for byte and short, use a float (instead of double) if you need to save memory in large arrays of floating point numbers. This data type should never be used for precise values, such as currency. For that, you will need to use the java.math.BigDecimal class instead. Numbers and Strings covers BigDecimal and other useful classes provided by the Java platform." STYLE_REF="pre_last_node" ID="ID_259043873" CREATED="1436172439696" MODIFIED="1436172647051"/>
<node TEXT="0.0f" STYLE_REF="pre_last_node" ID="ID_71298439" CREATED="1425209098726" MODIFIED="1436172517252"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Default value
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="double" STYLE_REF="gray_node" ID="ID_1914501220" CREATED="1436172124433" MODIFIED="1436172630711">
<node TEXT="The double data type is a double-precision 64-bit IEEE 754 floating point. For decimal values, this data type is generally the default choice. As mentioned above, this data type should never be used for precise values, such as currency." STYLE_REF="pre_last_node" ID="ID_1476273558" CREATED="1436172524183" MODIFIED="1436172647050"/>
<node TEXT="0.0d" STYLE_REF="pre_last_node" ID="ID_99854950" CREATED="1425209098726" MODIFIED="1436172558456"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Default value
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="boolean" STYLE_REF="gray_node" ID="ID_752287139" CREATED="1436172124434" MODIFIED="1436172630711">
<node TEXT="The boolean data type has only two possible values: true and false. Use this data type for simple flags that track true/false conditions. This data type represents one bit of information, but its &quot;size&quot; isn&apos;t something that&apos;s precisely defined." STYLE_REF="pre_last_node" ID="ID_16783110" CREATED="1436172564015" MODIFIED="1436172647046"/>
<node TEXT="false" STYLE_REF="pre_last_node" ID="ID_1897662190" CREATED="1425209098726" MODIFIED="1436172589835"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Default value
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="char" STYLE_REF="gray_node" ID="ID_267967975" CREATED="1436172124435" MODIFIED="1436172630708">
<node TEXT="The char data type is a single 16-bit Unicode character. It has a minimum value of &apos;\u0000&apos; (or 0) and a maximum value of &apos;\uffff&apos; (or 65,535 inclusive)" STYLE_REF="pre_last_node" ID="ID_1526010274" CREATED="1436172599016" MODIFIED="1436172647048"/>
<node TEXT="&apos;\u0000&apos;" STYLE_REF="pre_last_node" ID="ID_88516475" CREATED="1425209098726" MODIFIED="1436172622372"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Default value
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="Complex Data Types" STYLE_REF="orange_node" ID="ID_812811218" CREATED="1436179008830" MODIFIED="1436179490411">
<hook URI="../../Images/complex.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="String" STYLE_REF="gray_node" ID="ID_1734388815" CREATED="1436179014775" MODIFIED="1436179500972">
<node TEXT="String objects are immutable, which means that once created, their values cannot be changed" STYLE_REF="pre_last_node" ID="ID_330136765" CREATED="1436179053729" MODIFIED="1436179509083"/>
<node TEXT="null" STYLE_REF="pre_last_node" ID="ID_1449267270" CREATED="1425209098726" MODIFIED="1436179030171"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Default value
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Object" STYLE_REF="gray_node" ID="ID_1224321542" CREATED="1436179031982" MODIFIED="1436179500973">
<node TEXT="null" STYLE_REF="pre_last_node" ID="ID_950024064" CREATED="1425209098726" MODIFIED="1436179030171"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Default value
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Object" STYLE_REF="gray_node" ID="ID_1850832141" CREATED="1436429497288" MODIFIED="1436430738523">
<node TEXT=" Every class is a descendant, direct or indirect, of the Object class" STYLE_REF="main_definition" ID="ID_516716976" CREATED="1436429514273" MODIFIED="1436430750019"/>
<node TEXT="Methods" STYLE_REF="pre_last_node" ID="ID_333656005" CREATED="1436429538823" MODIFIED="1436430870059">
<node TEXT="Concurrency" STYLE_REF="pre_last_node" ID="ID_292990575" CREATED="1436429543808" MODIFIED="1436430896170">
<node TEXT="public final void notify()" STYLE_REF="yellow_node" ID="ID_1053461402" CREATED="1436429550874" MODIFIED="1436430861126"/>
<node TEXT="public final void notifyAll()" STYLE_REF="yellow_node" ID="ID_438816713" CREATED="1436429550874" MODIFIED="1436430861126"/>
<node TEXT="public final void wait()" STYLE_REF="yellow_node" ID="ID_1320447730" CREATED="1436429550875" MODIFIED="1436430861125"/>
<node TEXT="public final void wait(long timeout)" STYLE_REF="yellow_node" ID="ID_803874721" CREATED="1436429550875" MODIFIED="1436430861125"/>
<node TEXT="public final void wait(long timeout, int nanos)" STYLE_REF="yellow_node" ID="ID_1766108684" CREATED="1436429550875" MODIFIED="1436430861125"/>
</node>
<node TEXT="protected Object clone() throws CloneNotSupportedException" STYLE_REF="yellow_node" ID="ID_929574426" CREATED="1436429562995" MODIFIED="1436430861124">
<node TEXT="If a class, or one of its superclasses, implements the Cloneable interface, you can use the clone() method to create a copy from an existing object" STYLE_REF="pre_last_node" ID="ID_721307964" CREATED="1436429646135" MODIFIED="1436430793407"/>
<node TEXT="aCloneableObject.clone();" STYLE_REF="example_node" ID="ID_1328736680" CREATED="1436429652095" MODIFIED="1436430808381"/>
<node TEXT="Object&apos;s implementation of this method checks to see whether the object on which clone() was invoked implements the Cloneable interface. If the object does not, the method throws a CloneNotSupportedException exception" STYLE_REF="pre_last_node" ID="ID_552694517" CREATED="1436429670463" MODIFIED="1436430793404"/>
<node TEXT="If an object contains a reference to an external object, say ObjExternal, you may need to override clone() to get correct behavior. Otherwise, a change in ObjExternal made by one object will be visible in its clone also. This means that the original object and its clone are not independent&#x2014;to decouple them, you must override clone() so that it clones the object and ObjExternal" STYLE_REF="pre_last_node" ID="ID_748963314" CREATED="1436429823855" MODIFIED="1436430793402"/>
</node>
<node TEXT="public boolean equals(Object obj)" STYLE_REF="yellow_node" ID="ID_1201203130" CREATED="1436429843527" MODIFIED="1436430861125">
<node TEXT="Indicates whether some other object is &quot;equal to&quot; this one." STYLE_REF="pre_last_node" ID="ID_1982867783" CREATED="1436429853875" MODIFIED="1436430793400"/>
<node TEXT="The equals() method provided in the Object class uses the identity operator (==) to determine whether two objects are equal." STYLE_REF="pre_last_node" ID="ID_1857623662" CREATED="1436429905545" MODIFIED="1436430793397"/>
<node TEXT="For primitive data types, this gives the correct result." STYLE_REF="pre_last_node" ID="ID_135010519" CREATED="1436429929855" MODIFIED="1436430793395"/>
<node TEXT="test whether two objects are equal in the sense of equivalency, you must override the equals() method" STYLE_REF="pre_last_node" ID="ID_1173803746" CREATED="1436430042954" MODIFIED="1436430793394"/>
<node TEXT="Example" STYLE_REF="pre_last_node" ID="ID_1300799236" CREATED="1436429949776" MODIFIED="1436430793395">
<node TEXT="public class Book {&#xa;    ...&#xa;    public boolean equals(Object obj) {&#xa;        if (obj instanceof Book)&#xa;            return ISBN.equals((Book)obj.getISBN());&#xa;        else&#xa;            return false;&#xa;    }&#xa;}" STYLE_REF="example_node" ID="ID_1061321792" CREATED="1436429952719" MODIFIED="1436430808384"/>
</node>
<node TEXT="If you override equals(), you must override hashCode() as well." STYLE_REF="pre_last_node" ID="ID_1843473503" CREATED="1436430016816" MODIFIED="1436430793393"/>
</node>
<node TEXT="protected void finalize() throws Throwable" STYLE_REF="yellow_node" ID="ID_1733862676" CREATED="1436430060655" MODIFIED="1436430861125">
<node TEXT="Called by the garbage collector on an object when garbage collection determines that there are no more references to the object" STYLE_REF="pre_last_node" ID="ID_1467951114" CREATED="1436430082647" MODIFIED="1436430793389"/>
<node TEXT="Object&apos;s implementation of finalize() does nothing&#x2014;you can override finalize() to do cleanup, such as freeing resources" STYLE_REF="pre_last_node" ID="ID_51747510" CREATED="1436430181215" MODIFIED="1436430793387"/>
<node TEXT="The finalize() method may be called automatically by the system, but when it is called, or even if it is called, is uncertain." STYLE_REF="pre_last_node" ID="ID_843422673" CREATED="1436430230160" MODIFIED="1436430793385"/>
<node TEXT="Therefore, you should not rely on this method to do your cleanup for you. For example, if you don&apos;t close file descriptors in your code after performing I/O and you expect finalize() to close them for you, you may run out of file descriptors" STYLE_REF="pre_last_node" ID="ID_523238654" CREATED="1436430239256" MODIFIED="1436430793384"/>
</node>
<node TEXT="public final Class getClass()" STYLE_REF="yellow_node" ID="ID_421553687" CREATED="1436430253584" MODIFIED="1436430861125">
<node TEXT="The getClass() method returns a Class object, which has methods you can use to get information about the class" STYLE_REF="pre_last_node" ID="ID_521745518" CREATED="1436430268925" MODIFIED="1436430793383">
<node TEXT="getSimpleName()" STYLE_REF="example_node" ID="ID_551644861" CREATED="1436430331617" MODIFIED="1436430808385"/>
<node TEXT="getSuperclass()" STYLE_REF="example_node" ID="ID_215991077" CREATED="1436430337912" MODIFIED="1436430808385"/>
<node TEXT="getInterfaces()" STYLE_REF="example_node" ID="ID_1779366525" CREATED="1436430343226" MODIFIED="1436430808385"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Interfaces it implements
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="You cannot override getClass" STYLE_REF="pre_last_node" ID="ID_254041443" CREATED="1436430298050" MODIFIED="1436430793383"/>
<node TEXT="The Class class" STYLE_REF="pre_last_node" ID="ID_1327714948" CREATED="1436430398968" MODIFIED="1436430812141">
<node TEXT="isAnnotation()" STYLE_REF="example_node" ID="ID_1397433860" CREATED="1436430406461" MODIFIED="1436430808385"/>
<node TEXT="isInterface()" STYLE_REF="example_node" ID="ID_216149448" CREATED="1436430411602" MODIFIED="1436430808386"/>
<node TEXT="isEnum()" STYLE_REF="example_node" ID="ID_1251995741" CREATED="1436430416084" MODIFIED="1436430808386"/>
<node TEXT="getFields()" STYLE_REF="example_node" ID="ID_159766746" CREATED="1436430421002" MODIFIED="1436430808386"/>
<node TEXT="getMethods()" STYLE_REF="example_node" ID="ID_1096075249" CREATED="1436430427025" MODIFIED="1436430808386"/>
</node>
</node>
<node TEXT="public int hashCode()" STYLE_REF="yellow_node" ID="ID_1353495362" CREATED="1436430436064" MODIFIED="1436430861125">
<node TEXT="Returns a hash code value for the object, which is the object&apos;s memory address in hexadecimal" STYLE_REF="pre_last_node" ID="ID_702915928" CREATED="1436430518640" MODIFIED="1436430793381"/>
<node TEXT="By definition, if two objects are equal, their hash code must also be equal. If you override the equals() method, you change the way two objects are equated and Object&apos;s implementation of hashCode() is no longer valid. Therefore, if you override the equals() method, you must also override the hashCode() method as well." STYLE_REF="pre_last_node" ID="ID_412832422" CREATED="1436430575361" MODIFIED="1436430793380"/>
</node>
<node TEXT="public String toString()" STYLE_REF="yellow_node" ID="ID_619370342" CREATED="1436430575855" MODIFIED="1436430861125">
<node TEXT="Returns a string representation of the object." STYLE_REF="pre_last_node" ID="ID_170170308" CREATED="1436430590579" MODIFIED="1436430793379"/>
</node>
</node>
</node>
<node TEXT="Array" STYLE_REF="gray_node" ID="ID_967483591" CREATED="1436180683980" MODIFIED="1436248040044">
<node TEXT="An array is a container object that holds a fixed number of values of a single type" STYLE_REF="main_definition" ID="ID_588360543" CREATED="1436279644796" MODIFIED="1436279649690"/>
<node TEXT="After creation, its length is fixed" STYLE_REF="main_definition" ID="ID_1360170994" CREATED="1436279684273" MODIFIED="1436430480829"/>
<node TEXT="null" STYLE_REF="pre_last_node" ID="ID_214311944" CREATED="1425209098726" MODIFIED="1436179030171"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Default value
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Declaring" STYLE_REF="yellow_node" ID="ID_1159200139" CREATED="1436279800346" MODIFIED="1436280164744">
<node TEXT="byte[] anArrayOfBytes;&#xa;short[] anArrayOfShorts;&#xa;long[] anArrayOfLongs;&#xa;float[] anArrayOfFloats;&#xa;double[] anArrayOfDoubles;&#xa;boolean[] anArrayOfBooleans;&#xa;char[] anArrayOfChars;&#xa;String[] anArrayOfStrings;" STYLE_REF="example_node" ID="ID_1909678043" CREATED="1436279803210" MODIFIED="1436287870093"/>
<node TEXT="You can also place the brackets after the array&apos;s name. This form is discouraged" STYLE_REF="pre_last_node" ID="ID_867777227" CREATED="1436279810178" MODIFIED="1436287859446"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      float anArrayOfFloats[];
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Creating, Initializing" STYLE_REF="yellow_node" ID="ID_1285706121" CREATED="1436279939370" MODIFIED="1436280164744">
<node TEXT="anArray = new int[10];&#xa;anArray[0] = 100;&#xa;anArray[1] = 200;&#xa;anArray[2] = 300;" STYLE_REF="example_node" ID="ID_1614799231" CREATED="1436279899282" MODIFIED="1436287870085"/>
<node TEXT="int[] anArray = {&#xa;    100, 200, 300,&#xa;    400, 500, 600,&#xa;    700, 800, 900, 1000&#xa;};" STYLE_REF="example_node" ID="ID_324264610" CREATED="1436279887962" MODIFIED="1436287870087"/>
<node TEXT="String[] days;&#xa;days = new String[]{&quot;monday&quot;, &quot;tuesday&quot;, &quot;wednesday&quot;, &quot;thursday&quot;,&#xa;&quot;friday&quot;, &quot;saturday&quot;, &quot;sunday&quot;};" STYLE_REF="example_node" ID="ID_253231474" CREATED="1436362778749" MODIFIED="1436362900044"/>
</node>
<node TEXT="Multidimensional array" STYLE_REF="yellow_node" ID="ID_978956033" CREATED="1436280002700" MODIFIED="1436280164743">
<node TEXT="String[][] names;" STYLE_REF="example_node" ID="ID_498375983" CREATED="1436280025922" MODIFIED="1436287870088"/>
<node TEXT="String[][] names = {&#xa;      {&quot;Mr. &quot;, &quot;Mrs. &quot;, &quot;Ms. &quot;},&#xa;      {&quot;Smith&quot;, &quot;Jones&quot;}&#xa;};" STYLE_REF="example_node" ID="ID_1522646791" CREATED="1436280037730" MODIFIED="1436287870088">
<node TEXT="// Mr. Smith&#xa;System.out.println(names[0][0] + names[1][0]);&#xa;// Ms. Jones&#xa;System.out.println(names[0][2] + names[1][1]);" STYLE_REF="example_node" ID="ID_1035223563" CREATED="1436280054186" MODIFIED="1436287870089"/>
</node>
</node>
<node TEXT="Copying Arrays" STYLE_REF="yellow_node" ID="ID_1673745519" CREATED="1436280075002" MODIFIED="1436280164743">
<node TEXT="public static void arraycopy(Object src, int srcPos,&#xa;                             Object dest, int destPos, int length)" STYLE_REF="example_node" ID="ID_1674263937" CREATED="1436280099393" MODIFIED="1436287870090"/>
<node TEXT="char[] copyFrom = { &apos;d&apos;, &apos;e&apos;, &apos;c&apos;, &apos;a&apos;, &apos;f&apos;, &apos;f&apos;, &apos;e&apos;, &apos;i&apos;, &apos;n&apos;, &apos;a&apos;, &apos;t&apos;, &apos;e&apos;, &apos;d&apos; };&#xa;char[] copyTo = new char[7];&#xa;&#xa;System.arraycopy(copyFrom, 2, copyTo, 0, 7);&#xa;System.out.println(new String(copyTo));" STYLE_REF="example_node" ID="ID_321121402" CREATED="1436280141242" MODIFIED="1436287870091"/>
</node>
<node TEXT="Array Manipulations" STYLE_REF="yellow_node" ID="ID_127767814" CREATED="1436287595896" MODIFIED="1436287803565">
<node TEXT="copyOfRange" STYLE_REF="gray_node" ID="ID_419675174" CREATED="1436287604311" MODIFIED="1436287807508">
<font BOLD="false"/>
<node TEXT=" char[] copyFrom = {&apos;d&apos;, &apos;e&apos;, &apos;c&apos;, &apos;a&apos;, &apos;f&apos;, &apos;f&apos;, &apos;e&apos;,&#xa;            &apos;i&apos;, &apos;n&apos;, &apos;a&apos;, &apos;t&apos;, &apos;e&apos;, &apos;d&apos;};&#xa;           &#xa;        char[] copyTo = java.util.Arrays.copyOfRange(copyFrom, 2, 9);" STYLE_REF="example_node" ID="ID_95727689" CREATED="1436287815584" MODIFIED="1436287870092"/>
<node TEXT="Second parameter is initial index of the range to be copied, inclusively" STYLE_REF="pre_last_node" ID="ID_941904704" CREATED="1436287650640" MODIFIED="1436287859445"/>
<node TEXT="Third parameter is the final index of the range to be copied, exclusively" STYLE_REF="pre_last_node" ID="ID_828000445" CREATED="1436287659488" MODIFIED="1436287859444"/>
</node>
<node TEXT="binarySearch" STYLE_REF="gray_node" ID="ID_1379822101" CREATED="1436287682856" MODIFIED="1436287807508">
<font BOLD="false"/>
<node TEXT="Searching an array for a specific value to get the index at which it is placed" STYLE_REF="pre_last_node" ID="ID_1469782244" CREATED="1436287689329" MODIFIED="1436287859445"/>
</node>
<node TEXT="equals" STYLE_REF="gray_node" ID="ID_292166833" CREATED="1436287703039" MODIFIED="1436287807509">
<font BOLD="false"/>
<node TEXT="Comparing two arrays to determine if they are equal or not" STYLE_REF="pre_last_node" ID="ID_353210808" CREATED="1436287710727" MODIFIED="1436287859445"/>
</node>
<node TEXT="fill" STYLE_REF="gray_node" ID="ID_1908439993" CREATED="1436287720079" MODIFIED="1436287807509">
<font BOLD="false"/>
<node TEXT="Filling an array to place a specific value at each index" STYLE_REF="pre_last_node" ID="ID_122817307" CREATED="1436287726936" MODIFIED="1436287859445"/>
</node>
<node TEXT="sort" STYLE_REF="gray_node" ID="ID_11389821" CREATED="1436287750609" MODIFIED="1436287807509">
<font BOLD="false"/>
<node TEXT="Sorting an array into ascending order" STYLE_REF="pre_last_node" ID="ID_1804381167" CREATED="1436287759984" MODIFIED="1436287859445"/>
</node>
<node TEXT="parallelSort" STYLE_REF="gray_node" ID="ID_351849998" CREATED="1436287769192" MODIFIED="1436287807509">
<font BOLD="false"/>
<node TEXT="Java &gt;= 8" STYLE_REF="pre_last_node" ID="ID_920658147" CREATED="1436287788959" MODIFIED="1436287859445"/>
</node>
</node>
</node>
</node>
</node>
<node TEXT="Variables" STYLE_REF="second_node" POSITION="left" ID="ID_1766740287" CREATED="1436171125034" MODIFIED="1436430916127">
<edge COLOR="#666600"/>
<hook URI="../../Images/variable.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Naming" STYLE_REF="orange_node" ID="ID_1324479215" CREATED="1436171130107" MODIFIED="1436179583444">
<node TEXT="Variable names are case-sensitive" STYLE_REF="pre_last_node" ID="ID_980246598" CREATED="1436171191018" MODIFIED="1436171912934"/>
<node TEXT=" A variable&apos;s name can be any legal identifier &#x2014; an unlimited-length sequence of Unicode letters and digits, beginning with" STYLE_REF="pre_last_node" ID="ID_921400466" CREATED="1436171336921" MODIFIED="1436171912938"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      The convention, however, is to always begin your variable names with a letter, not &quot;$&quot; or &quot;_&quot;.
    </p>
  </body>
</html>
</richcontent>
<node TEXT="a letter" ID="ID_447050107" CREATED="1436171346908" MODIFIED="1436171346908"/>
<node TEXT="the dollar sign &quot;$&quot;" ID="ID_12916296" CREATED="1436171340114" MODIFIED="1436171357318"/>
<node TEXT="the underscore character &quot;_&quot;" ID="ID_633848721" CREATED="1436171365003" MODIFIED="1436171386365"/>
</node>
<node TEXT="The name you choose must not be a keyword or reserved word" STYLE_REF="pre_last_node" ID="ID_564258194" CREATED="1436171244043" MODIFIED="1436180245098"/>
<node TEXT="If the name you choose consists of only one word, spell that word in all lowercase letters. If it consists of more than one word, capitalize the first letter of each subsequent word" STYLE_REF="pre_last_node" ID="ID_613580797" CREATED="1436171266809" MODIFIED="1436171912943"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      The names gearRatio and currentGear are prime examples of this convention
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="If your variable stores a constant value, the convention changes slightly, capitalizing every letter and separating subsequent words with the underscore character. By convention, the underscore character is never used elsewhere." STYLE_REF="pre_last_node" ID="ID_1533893271" CREATED="1436171422771" MODIFIED="1436171912947"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      static final int NUM_GEARS = 6
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Variable types" STYLE_REF="orange_node" ID="ID_1974302729" CREATED="1436170834747" MODIFIED="1436179583444">
<node TEXT="Instance Variables (Non-Static Fields)" STYLE_REF="yellow_node" ID="ID_1046630559" CREATED="1436170848322" MODIFIED="1436171931329">
<node TEXT="Technically speaking, objects store their individual states in &quot;non-static fields&quot;, that is, fields declared without the static keyword. Non-static fields are also known as instance variables because their values are unique to each instance of a class (to each object, in other words); the currentSpeed of one bicycle is independent from the currentSpeed of another." STYLE_REF="pre_last_node" ID="ID_342192078" CREATED="1436170886930" MODIFIED="1436171937542"/>
<node TEXT="Fields that are declared but not initialized will be set to a reasonable default by the compiler" STYLE_REF="pre_last_node" ID="ID_1151952766" CREATED="1436179091910" MODIFIED="1436179182150"/>
</node>
<node TEXT="Class Variables (Static Fields)" STYLE_REF="yellow_node" ID="ID_397773757" CREATED="1436170897281" MODIFIED="1436171931326">
<node TEXT="A class variable is any field declared with the static modifier; this tells the compiler that there is exactly one copy of this variable in existence, regardless of how many times the class has been instantiated. A field defining the number of gears for a particular kind of bicycle could be marked as static since conceptually the same number of gears will apply to all instances. The code static int numGears = 6; would create such a static field. Additionally, the keyword final could be added to indicate that the number of gears will never change." STYLE_REF="pre_last_node" ID="ID_6027676" CREATED="1436170910067" MODIFIED="1436171937547"/>
</node>
<node TEXT="Local Variables" STYLE_REF="yellow_node" ID="ID_945634810" CREATED="1436170957682" MODIFIED="1436171931328">
<node TEXT="Similar to how an object stores its state in fields, a method will often store its temporary state in local variables. The syntax for declaring a local variable is similar to declaring a field (for example, int count = 0;). There is no special keyword designating a variable as local; that determination comes entirely from the location in which the variable is declared &#x2014; which is between the opening and closing braces of a method. As such, local variables are only visible to the methods in which they are declared; they are not accessible from the rest of the class." STYLE_REF="pre_last_node" ID="ID_13797122" CREATED="1436170962305" MODIFIED="1436171937549"/>
<node TEXT="Local variables are slightly different; the compiler never assigns a default value to an uninitialized local variable. Accessing an uninitialized local variable will result in a compile-time error." STYLE_REF="pre_last_node" ID="ID_1593153324" CREATED="1436179113959" MODIFIED="1436179182149"/>
</node>
<node TEXT="Parameters" STYLE_REF="yellow_node" ID="ID_229646974" CREATED="1436171008266" MODIFIED="1436171931328">
<node TEXT="Recall that the signature for the main method is public static void main(String[] args). Here, the args variable is the parameter to this method. The important thing to remember is that parameters are always classified as &quot;variables&quot; not &quot;fields&quot;. This applies to other parameter-accepting constructs as well (such as constructors and exception handlers)." STYLE_REF="pre_last_node" ID="ID_1100268587" CREATED="1436171012666" MODIFIED="1436171937545"/>
</node>
</node>
<node TEXT="Literals" STYLE_REF="orange_node" ID="ID_1561460653" CREATED="1436179193846" MODIFIED="1436179583443">
<node TEXT="A literal is the source code representation of a fixed value; literals are represented directly in your code without requiring computation" STYLE_REF="example_node" ID="ID_1741466494" CREATED="1436179211732" MODIFIED="1436180172543" BACKGROUND_COLOR="#ffccff"/>
<node TEXT="Integer Literals" STYLE_REF="gray_node" ID="ID_584641534" CREATED="1436179218698" MODIFIED="1436179590939">
<node TEXT="By types" STYLE_REF="yellow_node" ID="ID_460857256" CREATED="1436179354054" MODIFIED="1436179594917">
<node TEXT="An integer literal is of type long if it ends with the letter L or l" STYLE_REF="pre_last_node" ID="ID_26851862" CREATED="1436179271562" MODIFIED="1436179601628"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      L prefered
    </p>
  </body>
</html>
</richcontent>
</node>
<node STYLE_REF="pre_last_node" ID="ID_727729680" CREATED="1436179278889" MODIFIED="1436179601628"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      otherwise it is of type int
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="By systems" STYLE_REF="yellow_node" ID="ID_877294533" CREATED="1436179362239" MODIFIED="1436179594915">
<node TEXT="Decimal" STYLE_REF="pre_last_node" ID="ID_1158188682" CREATED="1436179370992" MODIFIED="1436179601628"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      int decVal = 26;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Hexadecimal" STYLE_REF="pre_last_node" ID="ID_1384970422" CREATED="1436179382006" MODIFIED="1436179601627"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      int hexVal = 0x1a;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Binary &gt;= JDK7" STYLE_REF="pre_last_node" ID="ID_1586732616" CREATED="1436179395198" MODIFIED="1436179601627"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      int binVal = 0b11010;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="Floating-Point Literals" STYLE_REF="gray_node" ID="ID_39242673" CREATED="1436179424905" MODIFIED="1436179590940">
<node TEXT="By types" STYLE_REF="yellow_node" ID="ID_1524361540" CREATED="1436179855942" MODIFIED="1436179880106">
<node TEXT="A floating-point literal is of type float if it ends with the letter F or f" STYLE_REF="pre_last_node" ID="ID_9539298" CREATED="1436179726335" MODIFIED="1436179875916"/>
<node TEXT="otherwise its type is double and it can optionally end with the letter D or d" STYLE_REF="pre_last_node" ID="ID_650804505" CREATED="1436179734168" MODIFIED="1436179875916"/>
</node>
<node TEXT="Notations" STYLE_REF="yellow_node" ID="ID_128384991" CREATED="1436179755606" MODIFIED="1436179880108">
<node TEXT="Scientific" STYLE_REF="pre_last_node" ID="ID_772547039" CREATED="1436179764078" MODIFIED="1436179875917"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      double d2 = 1.234e2;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Float literal" STYLE_REF="pre_last_node" ID="ID_1674926577" CREATED="1436179812149" MODIFIED="1436179875917"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      float f1&#160;&#160;= 123.4f;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Double literal" STYLE_REF="pre_last_node" ID="ID_1365879871" CREATED="1436179823982" MODIFIED="1436179875917"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      double d1 = 123.4;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="Character and String Literals" STYLE_REF="gray_node" ID="ID_1404612094" CREATED="1436179661847" MODIFIED="1436179700978">
<node TEXT=" &apos;single quotes&apos; for char literals" STYLE_REF="pre_last_node" ID="ID_862029463" CREATED="1436179676887" MODIFIED="1436179875918"/>
<node TEXT="&quot;double quotes&quot; for String literals" STYLE_REF="pre_last_node" ID="ID_310097545" CREATED="1436179681661" MODIFIED="1436179875918"/>
<node TEXT="If your editor and file system allow it, you can use such characters directly in your code. If not, you can use a &quot;Unicode escape&quot; such as &apos;\u0108&apos; (capital C with circumflex), or &quot;S\u00ED Se\u00F1or&quot; (S&#xed; Se&#xf1;or in Spanish)" STYLE_REF="pre_last_node" ID="ID_1965987241" CREATED="1436180103838" MODIFIED="1436180635948"/>
</node>
<node TEXT="Null literal" STYLE_REF="gray_node" ID="ID_104315505" CREATED="1436179974079" MODIFIED="1436180624809">
<node TEXT="null literal that can be used as a value for any reference type" STYLE_REF="pre_last_node" ID="ID_1599300285" CREATED="1436179992469" MODIFIED="1436180641882"/>
<node TEXT="null may be assigned to any variable, except variables of primitive types" STYLE_REF="pre_last_node" ID="ID_1894086477" CREATED="1436180006534" MODIFIED="1436180635946"/>
</node>
<node TEXT="Using Underscore Characters in Numeric Literals" STYLE_REF="gray_node" ID="ID_603710170" CREATED="1436180345068" MODIFIED="1436180624810">
<node TEXT="JDK &gt;= 7" STYLE_REF="pre_last_node" ID="ID_638184026" CREATED="1436180361623" MODIFIED="1436180635946"/>
<node TEXT="You can use an underscore character to separate digits in groups of digits in numeric literals" STYLE_REF="pre_last_node" ID="ID_1163772729" CREATED="1436180408757" MODIFIED="1436180635947"/>
<node TEXT="Examples" STYLE_REF="pre_last_node" ID="ID_964653460" CREATED="1436180450973" MODIFIED="1436180635949">
<node TEXT="long creditCardNumber = 1234_5678_9012_3456L;&#xa;long socialSecurityNumber = 999_99_9999L;&#xa;float pi =  3.14_15F;&#xa;long hexBytes = 0xFF_EC_DE_5E;&#xa;long hexWords = 0xCAFE_BABE;&#xa;long maxLong = 0x7fff_ffff_ffff_ffffL;&#xa;byte nybbles = 0b0010_0101;&#xa;long bytes = 0b11010010_01101001_10010100_10010010;" STYLE_REF="example_node" ID="ID_189126880" CREATED="1436180455788" MODIFIED="1436180645121"/>
</node>
<node TEXT="Forbidden place underscores in the following places" STYLE_REF="pre_last_node" ID="ID_1153533264" CREATED="1436180480606" MODIFIED="1436180635949">
<node TEXT="At the beginning or end of a number" ID="ID_440690429" CREATED="1436180497814" MODIFIED="1436180581226"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      int x2 = 52_;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Adjacent to a decimal point in a floating point literal" ID="ID_964580380" CREATED="1436180497815" MODIFIED="1436180552706"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      float pi1 = 3_.1415F;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Prior to an F or L suffix" ID="ID_1701092861" CREATED="1436180497817" MODIFIED="1436180599658"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      long socialSecurityNumber1 = 999_99_9999_L;
    </p>
    <p>
      int x4 = 0_x52;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="In positions where a string of digits is expected" ID="ID_442123813" CREATED="1436180497820" MODIFIED="1436180517858"/>
</node>
</node>
</node>
</node>
<node TEXT="Casting" STYLE_REF="second_node" POSITION="left" ID="ID_704383485" CREATED="1425648319143" MODIFIED="1425649711979">
<edge COLOR="#0000ff"/>
<hook URI="../../Images/casting.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Long to" STYLE_REF="orange_node" ID="ID_895445607" CREATED="1425648325886" MODIFIED="1436166896741">
<edge WIDTH="1"/>
<node TEXT="int" STYLE_REF="yellow_node" ID="ID_766110648" CREATED="1425648332574" MODIFIED="1436166896740">
<edge WIDTH="thin"/>
<node TEXT="public static int longToInt(long l) {&#xa;if (l &lt; Integer.MIN_VALUE || l &gt; Integer.MAX_VALUE) {&#xa;    throw new IllegalArgumentException(l + &quot; cannot be cast to int without changing its value.&quot;);&#xa;        }&#xa;    return (int) l;&#xa;}" STYLE_REF="example_node" ID="ID_1016675043" CREATED="1425648322414" MODIFIED="1425648471924"/>
</node>
</node>
<node TEXT="Float to" STYLE_REF="orange_node" ID="ID_911960859" CREATED="1425648409134" MODIFIED="1425648636755">
<edge WIDTH="2"/>
<node TEXT="String" STYLE_REF="yellow_node" ID="ID_1497022953" CREATED="1425648412614" MODIFIED="1425648630490">
<edge WIDTH="thin"/>
<node TEXT="String str = String.format(&quot;%.02f&quot;, 3.1428733);&#xa;System.out.println(&quot;formatted float up to 2 decimals &quot; + str);" STYLE_REF="example_node" ID="ID_1512365965" CREATED="1425648423990" MODIFIED="1425648436091"/>
<node TEXT="StringBuilder sb = new StringBuilder();&#xa;Formatter formatter = new Formatter(sb);&#xa;formatter.format(&quot;%.4f&quot;, 3.1428733);     // 4 decimal places&#xa;System.out.println(&quot;PI of float up to 4 decimals : &quot; + formatter.toString());&#xa;formatter.close();" STYLE_REF="example_node" ID="ID_633130914" CREATED="1425648444422" MODIFIED="1425648465675"/>
<node TEXT="DecimalFormat df = new DecimalFormat(&quot;#.##&quot;);&#xa;String formatted = df.format(2.456345);&#xa;System.out.println(formatted);  //prints 2.46" STYLE_REF="example_node" ID="ID_657257774" CREATED="1425648451654" MODIFIED="1425648465680"/>
<node TEXT="double price = 20.25;&#xa;System.out.printf(&quot;Value of double up-to 2 decimals : %.2f&quot;, price);" STYLE_REF="example_node" ID="ID_1533037789" CREATED="1425648459518" MODIFIED="1425648465682"/>
</node>
</node>
</node>
<node TEXT="Performance" STYLE_REF="second_node" POSITION="right" ID="ID_1960433498" CREATED="1432093336134" MODIFIED="1436188334753">
<edge COLOR="#006600"/>
<hook URI="../../Images/performance.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Casting" STYLE_REF="yellow_node" ID="ID_1780786884" CREATED="1432093340878" MODIFIED="1436192237107">
<node TEXT="for (int i = 0; i &lt; 100000; i++) {&#xa;            TrainingModel trn = (TrainingModel) retrieveEntity(TrainingModel.NAME);&#xa;        }" STYLE_REF="example_node" ID="ID_1479565066" CREATED="1432093364198" MODIFIED="1436192259901">
<node TEXT="33 mili" ID="ID_462550880" CREATED="1432093400230" MODIFIED="1432093430083"/>
</node>
<node TEXT="for (int i = 0; i &lt; 100000; i++) {&#xa;            ITrainingModel trn = (ITrainingModel) retrieveEntity(ITrainingModel.NAME);&#xa;        }" STYLE_REF="example_node" ID="ID_693381254" CREATED="1432093387527" MODIFIED="1436192259901">
<node TEXT="21 mili" ID="ID_1226013302" CREATED="1432093431375" MODIFIED="1432093465923"/>
</node>
</node>
<node TEXT="Access for static field vs instance field" STYLE_REF="yellow_node" ID="ID_853820002" CREATED="1432453790709" MODIFIED="1436192237106">
<node TEXT="long start = System.currentTimeMillis();&#xa;        String str;&#xa;        for (int i = 0; i &lt; 10000000; i++) {&#xa;            str = CHECKED_ITEM_VAL;&#xa;        }&#xa;        Log.d(&quot;TIME&quot;, String.valueOf(System.currentTimeMillis() - start));" STYLE_REF="example_node" ID="ID_1104629829" CREATED="1432453802225" MODIFIED="1436192259899"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      public static final String CHECKED_ITEM_VAL = &quot;checkedItemVal&quot;;
    </p>
    <p>
      // Static field inherited from interface
    </p>
  </body>
</html>
</richcontent>
<node TEXT="4348 mili" ID="ID_1076132030" CREATED="1432453890777" MODIFIED="1432453912245"/>
</node>
<node TEXT="long start = System.currentTimeMillis();&#xa;        String str;&#xa;        for (int i = 0; i &lt; 10000000; i++) {&#xa;            str = IEntity.CHECKED_ITEM_VAL;&#xa;        }&#xa;        Log.d(&quot;TIME&quot;, String.valueOf(System.currentTimeMillis() - start));" STYLE_REF="example_node" ID="ID_1388360031" CREATED="1432453886882" MODIFIED="1436192259897">
<node TEXT="4321 mili" ID="ID_7011472" CREATED="1432453899441" MODIFIED="1432453916781"/>
</node>
<node TEXT="public static final String CHECKED_ITEM_VAL = &quot;checkedItemVal&quot;;" STYLE_REF="example_node" ID="ID_717656648" CREATED="1432454039352" MODIFIED="1436192259896"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Static field declared in current class
    </p>
  </body>
</html>
</richcontent>
<node TEXT="4312 mili" ID="ID_678120094" CREATED="1432454039630" MODIFIED="1432454042765"/>
</node>
<node TEXT="public String checkedItemVal = &quot;checkedItemVal&quot;;" STYLE_REF="example_node" ID="ID_372882500" CREATED="1432454160049" MODIFIED="1436192259896">
<node TEXT="5099" OBJECT="java.lang.Long|5099" ID="ID_488121968" CREATED="1432454162040" MODIFIED="1432454162909"/>
</node>
<node TEXT="public final String checkedItemVal = &quot;checkedItemVal&quot;;" STYLE_REF="example_node" ID="ID_1539866063" CREATED="1432454269911" MODIFIED="1436192259896">
<node TEXT="4338 mili" ID="ID_1819471117" CREATED="1432454271031" MODIFIED="1432454277044"/>
</node>
</node>
<node TEXT="Access direct vs getter" STYLE_REF="yellow_node" ID="ID_486689348" CREATED="1432454571744" MODIFIED="1436192237106">
<node TEXT="        long start = System.currentTimeMillis();&#xa;        String str;&#xa;        for (int i = 0; i &lt; 1000000; i++) {&#xa;            mFacade.hashCode();&#xa;        }&#xa;        Log.d(&quot;TIME&quot;, String.valueOf(System.currentTimeMillis() - start));" STYLE_REF="example_node" ID="ID_518827701" CREATED="1432454575367" MODIFIED="1436192259894">
<node TEXT="public Facade mFacade = Facade.getInstance();" STYLE_REF="example_node" ID="ID_643150625" CREATED="1432455248110" MODIFIED="1436192259895">
<node TEXT="4087 mili" ID="ID_1736269897" CREATED="1432454604592" MODIFIED="1432454606549"/>
</node>
<node TEXT="public static final Facade mFacade = Facade.getInstance();" STYLE_REF="example_node" ID="ID_222075508" CREATED="1432455238639" MODIFIED="1436192259895">
<node TEXT="3909 mili" ID="ID_1959638852" CREATED="1432455204512" MODIFIED="1432455439837"/>
</node>
</node>
<node TEXT=" long start = System.currentTimeMillis();&#xa;        String str;&#xa;        for (int i = 0; i &lt; 1000000; i++) {&#xa;            Facade.getInstance().hashCode();&#xa;        }&#xa;        Log.d(&quot;TIME&quot;, String.valueOf(System.currentTimeMillis() - start));" STYLE_REF="example_node" ID="ID_961090966" CREATED="1432454689423" MODIFIED="1436192259890">
<node TEXT="public static Facade getInstance() {&#xa;        if (instance == null) {&#xa;            instance = new Facade();&#xa;        }&#xa;        return instance;&#xa;    }" STYLE_REF="example_node" ID="ID_892136163" CREATED="1432454897464" MODIFIED="1436192259892">
<node TEXT="9233 mili" ID="ID_1897439460" CREATED="1432454689887" MODIFIED="1432454739980"/>
</node>
<node TEXT="public static Facade getInstance() {&#xa;        return instance;&#xa;    }" STYLE_REF="example_node" ID="ID_168573954" CREATED="1432454902599" MODIFIED="1436192259891">
<node TEXT="8107" OBJECT="java.lang.Long|8107" ID="ID_382174946" CREATED="1432454889104" MODIFIED="1432454890444"/>
</node>
</node>
</node>
<node TEXT="Traversing array with 1000000 items" STYLE_REF="yellow_node" ID="ID_1854729126" CREATED="1432356524210" MODIFIED="1432356698618">
<node TEXT="for" STYLE_REF="pre_last_node" ID="ID_144761094" CREATED="1432356573378" MODIFIED="1432356709288">
<node TEXT="520 ms" ID="ID_1710516428" CREATED="1432356583243" MODIFIED="1432356599390"/>
</node>
<node TEXT="for each..in" STYLE_REF="pre_last_node" ID="ID_987452115" CREATED="1432356601234" MODIFIED="1432356709289">
<node TEXT="1753 ms" ID="ID_809861313" CREATED="1432356612554" MODIFIED="1432356613628"/>
</node>
<node TEXT="while" STYLE_REF="pre_last_node" ID="ID_1270782707" CREATED="1432356616690" MODIFIED="1432356709290">
<node TEXT="2775 ms" ID="ID_890293159" CREATED="1432356633657" MODIFIED="1432356635419"/>
</node>
<node TEXT="util loop" STYLE_REF="pre_last_node" ID="ID_1937136973" CREATED="1432356642098" MODIFIED="1435323198869"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      myVector.forEach(arrayConverter);
    </p>
    <p>
      
    </p>
    <p>
      function arrayConverter(element:*, index:int, array:Array):void{
    </p>
    <p>
      &#160;&#160;&#160;&#160;myArray[myArray.length] = element;
    </p>
    <p>
      }
    </p>
  </body>
</html>
</richcontent>
<node TEXT="4059 ms" ID="ID_1728795272" CREATED="1432356649515" MODIFIED="1432356650870"/>
</node>
</node>
</node>
<node TEXT="Collections" STYLE_REF="second_node" POSITION="left" ID="ID_1436515414" CREATED="1435118642152" MODIFIED="1435150638260">
<edge COLOR="#660066"/>
<hook URI="../../Images/collection-2.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="ArrayList" STYLE_REF="gray_node" ID="ID_1011080670" CREATED="1435118711471" MODIFIED="1435321270928">
<node TEXT="C&#x43f;&#x438;&#x441;&#x43e;&#x43a;, &#x440;&#x435;&#x430;&#x43b;&#x438;&#x437;&#x43e;&#x432;&#x430;&#x43d;&#x43d;&#x44b;&#x439; &#x43d;&#x430; &#x43e;&#x441;&#x43d;&#x43e;&#x432;&#x435; &#x43c;&#x430;&#x441;&#x441;&#x438;&#x432;&#x430;" STYLE_REF="pre_last_node" ID="ID_1054906678" CREATED="1435118753940" MODIFIED="1435119379334"/>
<node TEXT="&#x41f;&#x440;&#x435;&#x438;&#x43c;&#x443;&#x449;&#x435;&#x441;&#x442;&#x432;&#x430;" STYLE_REF="yellow_node" ID="ID_825223588" CREATED="1435118866776" MODIFIED="1435120920187">
<icon BUILTIN="addition"/>
<node TEXT="&#x414;&#x43e;&#x441;&#x442;&#x443;&#x43f; &#x43a; &#x43f;&#x440;&#x43e;&#x438;&#x437;&#x432;&#x43e;&#x43b;&#x44c;&#x43d;&#x43e;&#x43c;&#x443; &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442;&#x443; &#x43f;&#x43e; &#x438;&#x43d;&#x434;&#x435;&#x43a;&#x441;&#x443; &#x437;&#x430; &#x43f;&#x43e;&#x441;&#x442;&#x43e;&#x44f;&#x43d;&#x43d;&#x43e;&#x435; &#x432;&#x440;&#x435;&#x43c;&#x44f;" STYLE_REF="pre_last_node" ID="ID_1209608509" CREATED="1435118874063" MODIFIED="1435119379347"/>
<node TEXT="&#x41c;&#x438;&#x43d;&#x438;&#x43c;&#x443;&#x43c; &#x43d;&#x430;&#x43a;&#x43b;&#x430;&#x434;&#x43d;&#x44b;&#x445; &#x440;&#x430;&#x441;&#x445;&#x43e;&#x434;&#x43e;&#x432; &#x43f;&#x440;&#x438; &#x445;&#x440;&#x430;&#x43d;&#x435;&#x43d;&#x438;&#x438;" STYLE_REF="pre_last_node" ID="ID_1959059284" CREATED="1435118893200" MODIFIED="1435119379349"/>
<node TEXT="&#x412;&#x441;&#x442;&#x430;&#x432;&#x43a;&#x430; &#x432; &#x43a;&#x43e;&#x43d;&#x435;&#x446; &#x441;&#x43f;&#x438;&#x441;&#x43a;&#x430; &#x432; &#x441;&#x440;&#x435;&#x434;&#x43d;&#x435;&#x43c; &#x43f;&#x440;&#x43e;&#x438;&#x437;&#x432;&#x43e;&#x434;&#x438;&#x442;&#x441;&#x44f; &#x442;&#x430;&#x43a; &#x436;&#x435; &#x437;&#x430; &#x43f;&#x43e;&#x441;&#x442;&#x43e;&#x44f;&#x43d;&#x43d;&#x43e;&#x435; &#x432;&#x440;&#x435;&#x43c;&#x44f;" STYLE_REF="pre_last_node" ID="ID_1594256442" CREATED="1435118913287" MODIFIED="1435119379345"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1052;&#1072;&#1089;&#1089;&#1080;&#1074; &#1080;&#1084;&#1077;&#1077;&#1090; &#1086;&#1087;&#1088;&#1077;&#1076;&#1077;&#1083;&#1077;&#1085;&#1085;&#1099;&#1081; &#1085;&#1072;&#1095;&#1072;&#1083;&#1100;&#1085;&#1099;&#1081; &#1088;&#1072;&#1079;&#1084;&#1077;&#1088; n (&#1087;&#1072;&#1088;&#1072;&#1084;&#1077;&#1090;&#1088; capacity), &#1087;&#1086; &#1091;&#1084;&#1086;&#1083;&#1095;&#1072;&#1085;&#1080;&#1102; n = 10, &#1087;&#1088;&#1080; &#1079;&#1072;&#1087;&#1080;&#1089;&#1080; n+1 &#1101;&#1083;&#1077;&#1084;&#1077;&#1085;&#1090;&#1072;, &#1073;&#1091;&#1076;&#1077;&#1090; &#1089;&#1086;&#1079;&#1076;&#1072;&#1085; &#1085;&#1086;&#1074;&#1099;&#1081; &#1084;&#1072;&#1089;&#1089;&#1080;&#1074; &#1088;&#1072;&#1079;&#1084;&#1077;&#1088;&#1086;&#1084; (n * 3) / 2 + 1, &#1074; &#1085;&#1077;&#1075;&#1086; &#1073;&#1091;&#1076;&#1091;&#1090; &#1087;&#1086;&#1084;&#1077;&#1097;&#1077;&#1085;&#1099; &#1074;&#1089;&#1077; &#1101;&#1083;&#1077;&#1084;&#1077;&#1085;&#1090;&#1099; &#1080;&#1079; &#1089;&#1090;&#1072;&#1088;&#1086;&#1075;&#1086; &#1084;&#1072;&#1089;&#1089;&#1080;&#1074;&#1072; + &#1085;&#1086;&#1074;&#1099;&#1081;, &#1076;&#1086;&#1073;&#1072;&#1074;&#1083;&#1103;&#1077;&#1084;&#1099;&#1081; &#1101;&#1083;&#1077;&#1084;&#1077;&#1085;&#1090;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&#x423;&#x434;&#x430;&#x43b;&#x435;&#x43d;&#x438;&#x435; &#x43f;&#x43e;&#x441;&#x43b;&#x435;&#x434;&#x43d;&#x435;&#x433;&#x43e; &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442;&#x430; &#x43f;&#x440;&#x43e;&#x438;&#x441;&#x445;&#x43e;&#x434;&#x438;&#x442; &#x437;&#x430; &#x43a;&#x43e;&#x43d;&#x441;&#x442;&#x430;&#x43d;&#x442;&#x43d;&#x43e;&#x435; &#x432;&#x440;&#x435;&#x43c;&#x44f;" STYLE_REF="pre_last_node" ID="ID_1296402952" CREATED="1435118999854" MODIFIED="1435119379344"/>
</node>
<node TEXT="&#x41d;&#x435;&#x434;&#x43e;&#x441;&#x442;&#x430;&#x442;&#x43a;&#x438;" STYLE_REF="yellow_node" ID="ID_1917585706" CREATED="1435119013288" MODIFIED="1435119039498">
<icon BUILTIN="subtraction"/>
<node TEXT="&#x41f;&#x440;&#x438; &#x432;&#x441;&#x442;&#x430;&#x432;&#x43a;&#x435;/&#x443;&#x434;&#x430;&#x43b;&#x435;&#x43d;&#x438;&#x438; &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442;&#x430; &#x432; &#x441;&#x435;&#x440;&#x435;&#x434;&#x438;&#x43d;&#x435; &#x441;&#x43f;&#x438;&#x441;&#x43a;&#x430; &#x2014; &#x44d;&#x442;&#x43e; &#x432;&#x437;&#x44b;&#x432;&#x430;&#x435;&#x442; &#x43f;&#x435;&#x440;&#x435;&#x437;&#x430;&#x43f;&#x438;&#x441;&#x44c; &#x432;&#x441;&#x435;&#x445; &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442;&#x43e;&#x432; &#x440;&#x430;&#x437;&#x43c;&#x435;&#x449;&#x435;&#x43d;&#x43d;&#x44b;&#x445; &#xab;&#x43f;&#x440;&#x430;&#x432;&#x435;&#x435;&#xbb; &#x432; &#x441;&#x43f;&#x438;&#x441;&#x43a;&#x435; &#x43d;&#x430; &#x43e;&#x434;&#x43d;&#x443; &#x43f;&#x43e;&#x437;&#x438;&#x446;&#x438;&#x44e; &#x432;&#x43b;&#x435;&#x432;&#x43e;" STYLE_REF="pre_last_node" ID="ID_554949861" CREATED="1435119058719" MODIFIED="1435119379338"/>
<node TEXT="&#x41f;&#x440;&#x438; &#x443;&#x434;&#x430;&#x43b;&#x435;&#x43d;&#x438;&#x438; &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442;&#x43e;&#x432; &#x440;&#x430;&#x437;&#x43c;&#x435;&#x440; &#x43c;&#x430;&#x441;&#x441;&#x438;&#x432;&#x430; &#x43d;&#x435; &#x443;&#x43c;&#x435;&#x43d;&#x44c;&#x448;&#x430;&#x435;&#x442;&#x441;&#x44f;, &#x434;&#x43e; &#x44f;&#x432;&#x43d;&#x43e;&#x433;&#x43e; &#x432;&#x44b;&#x437;&#x43e;&#x432;&#x430; &#x43c;&#x435;&#x442;&#x43e;&#x434;&#x430; trimToSize()" STYLE_REF="pre_last_node" ID="ID_1145642698" CREATED="1435119086350" MODIFIED="1435119379335"/>
</node>
<node TEXT="&#x41e;&#x441;&#x43e;&#x431;&#x435;&#x43d;&#x43d;&#x43e;&#x441;&#x442;&#x438;" STYLE_REF="yellow_node" ID="ID_1483005554" CREATED="1435119412628" MODIFIED="1435122245207">
<node TEXT="&#x42d;&#x444;&#x444;&#x435;&#x43a;&#x442;&#x438;&#x432;&#x43d;&#x44b;&#x439; &#x430;&#x43b;&#x433;&#x43e;&#x440;&#x438;&#x442;&#x43c; &#x443;&#x434;&#x430;&#x43b;&#x435;&#x43d;&#x438;&#x44f;" STYLE_REF="pre_last_node" ID="ID_1382546860" CREATED="1435119420726" MODIFIED="1435120935612"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1044;&#1086;&#1087;&#1091;&#1089;&#1090;&#1080;&#1084; &#1085;&#1091;&#1078;&#1085;&#1086; &#1091;&#1076;&#1072;&#1083;&#1080;&#1090;&#1100; n &#1101;&#1083;&#1077;&#1084;&#1077;&#1085;&#1090;&#1086;&#1074; &#1089; &#1087;&#1086;&#1079;&#1080;&#1094;&#1080;&#1080; m &#1074; &#1089;&#1087;&#1080;&#1089;&#1082;&#1077;. &#1042;&#1084;&#1077;&#1089;&#1090;&#1086; &#1074;&#1099;&#1087;&#1086;&#1083;&#1085;&#1077;&#1085;&#1080;&#1103; &#1091;&#1076;&#1072;&#1083;&#1077;&#1085;&#1080;&#1103; &#1086;&#1076;&#1085;&#1086;&#1075;&#1086; &#1101;&#1083;&#1077;&#1084;&#1077;&#1085;&#1090;&#1072; n &#1088;&#1072;&#1079; (&#1082;&#1072;&#1078;&#1076;&#1099;&#1081; &#1088;&#1072;&#1079; &#1089;&#1084;&#1077;&#1097;&#1072;&#1103; &#1085;&#1072; 1 &#1087;&#1086;&#1079;&#1080;&#1094;&#1080;&#1102; &#1101;&#1083;&#1077;&#1084;&#1077;&#1085;&#1090;&#1099;, &#1089;&#1090;&#1086;&#1103;&#1097;&#1080;&#1077; &#171;&#1087;&#1088;&#1072;&#1074;&#1077;&#1077;&#187; &#1074; &#1089;&#1087;&#1080;&#1089;&#1082;&#1077;), &#1085;&#1091;&#1078;&#1085;&#1086; &#1074;&#1099;&#1087;&#1086;&#1083;&#1085;&#1080;&#1090;&#1100; &#1089;&#1084;&#1077;&#1097;&#1077;&#1085;&#1080;&#1077; &#1074;&#1089;&#1077;&#1093; &#1101;&#1083;&#1077;&#1084;&#1077;&#1085;&#1090;&#1086;&#1074;, &#1089;&#1090;&#1086;&#1103;&#1097;&#1080;&#1093; &#171;&#1087;&#1088;&#1072;&#1074;&#1077;&#1077;&#187; n+m &#1087;&#1086;&#1079;&#1080;&#1094;&#1080;&#1080; &#1085;&#1072; n &#1101;&#1083;&#1077;&#1084;&#1077;&#1085;&#1090;&#1086;&#1074; &#1083;&#1077;&#1074;&#1077;&#1077; &#1082; &#1085;&#1072;&#1095;&#1072;&#1083;&#1091; &#1089;&#1087;&#1080;&#1089;&#1082;&#1072;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&#x412; Java &#x43c;&#x430;&#x441;&#x441;&#x438;&#x432;&#x44b; &#x438;&#x43c;&#x435;&#x44e;&#x442; &#x444;&#x438;&#x43a;&#x441;&#x438;&#x440;&#x43e;&#x432;&#x430;&#x43d;&#x43d;&#x443;&#x44e; &#x434;&#x43b;&#x438;&#x43d;&#x443;, &#x438; &#x43f;&#x43e;&#x441;&#x43b;&#x435; &#x442;&#x43e;&#x433;&#x43e; &#x43a;&#x430;&#x43a; &#x43c;&#x430;&#x441;&#x441;&#x438;&#x432; &#x441;&#x43e;&#x437;&#x434;&#x430;&#x43d;, &#x43e;&#x43d; &#x43d;&#x435; &#x43c;&#x43e;&#x436;&#x435;&#x442; &#x440;&#x430;&#x441;&#x442;&#x438; &#x438;&#x43b;&#x438; &#x443;&#x43c;&#x435;&#x43d;&#x44c;&#x448;&#x430;&#x442;&#x44c;&#x441;&#x44f;. ArrayList &#x43c;&#x43e;&#x436;&#x435;&#x442; &#x43c;&#x435;&#x43d;&#x44f;&#x442;&#x44c; &#x441;&#x432;&#x43e;&#x439; &#x440;&#x430;&#x437;&#x43c;&#x435;&#x440; &#x432;&#x43e; &#x432;&#x440;&#x435;&#x43c;&#x44f; &#x438;&#x441;&#x43f;&#x43e;&#x43b;&#x43d;&#x435;&#x43d;&#x438;&#x44f; &#x43f;&#x440;&#x43e;&#x433;&#x440;&#x430;&#x43c;&#x43c;&#x44b;, &#x43f;&#x440;&#x438; &#x44d;&#x442;&#x43e;&#x43c; &#x43d;&#x435; &#x43e;&#x431;&#x44f;&#x437;&#x430;&#x442;&#x435;&#x43b;&#x44c;&#x43d;&#x43e; &#x443;&#x43a;&#x430;&#x437;&#x44b;&#x432;&#x430;&#x442;&#x44c; &#x440;&#x430;&#x437;&#x43c;&#x435;&#x440;&#x43d;&#x43e;&#x441;&#x442;&#x44c; &#x43f;&#x440;&#x438; &#x441;&#x43e;&#x437;&#x434;&#x430;&#x43d;&#x438;&#x438; &#x43e;&#x431;&#x44a;&#x435;&#x43a;&#x442;&#x430;." STYLE_REF="pre_last_node" ID="ID_877330479" CREATED="1435121827238" MODIFIED="1435121843362"/>
</node>
<node TEXT="&#x418;&#x441;&#x43f;&#x43e;&#x43b;&#x44c;&#x437;&#x43e;&#x432;&#x430;&#x43d;&#x438;&#x435;" STYLE_REF="yellow_node" ID="ID_852051633" CREATED="1435122020683" MODIFIED="1435122172937">
<node TEXT="&#x421;&#x43e;&#x437;&#x434;&#x430;&#x43d;&#x438;&#x435; &#x43e;&#x431;&#x44a;&#x435;&#x43a;&#x442;&#x430;" STYLE_REF="gray_node" ID="ID_1214168638" CREATED="1435122033124" MODIFIED="1435150977026">
<font BOLD="false"/>
<node TEXT="ArrayList&lt;String&gt; list = new ArrayList&lt;String&gt;();" STYLE_REF="example_node" ID="ID_1435763007" CREATED="1435122038644" MODIFIED="1435122210595">
<hook URI="array-list-new.png" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="elementData" STYLE_REF="pre_last_node" ID="ID_1491874119" CREATED="1435122056756" MODIFIED="1435122200529">
<node TEXT="&#x41c;&#x430;&#x441;&#x441;&#x438;&#x432; &#x43e;&#x43f;&#x440;&#x435;&#x434;&#x435;&#x43b;&#x435;&#x43d;&#x43d;&#x43e;&#x433;&#x43e; &#x442;&#x438;&#x43f;&#x430; (&#x443;&#x43a;&#x430;&#x437;&#x430;&#x43d;&#x43d;&#x43e;&#x433;&#x43e; &#x432; generic), &#x432; &#x43d;&#x430;&#x448;&#x435;&#x43c; &#x441;&#x43b;&#x443;&#x447;&#x430;&#x435; String[]. &#x415;&#x441;&#x43b;&#x438; &#x432;&#x44b;&#x437;&#x44b;&#x432;&#x430;&#x435;&#x442;&#x441;&#x44f; &#x43a;&#x43e;&#x43d;&#x441;&#x442;&#x440;&#x443;&#x43a;&#x442;&#x43e;&#x440; &#x431;&#x435;&#x437; &#x43f;&#x430;&#x440;&#x430;&#x43c;&#x435;&#x442;&#x440;&#x43e;&#x432;, &#x442;&#x43e; &#x43f;&#x43e; &#x443;&#x43c;&#x43e;&#x43b;&#x447;&#x430;&#x43d;&#x438;&#x44e; &#x431;&#x443;&#x434;&#x435;&#x442; &#x441;&#x43e;&#x437;&#x434;&#x430;&#x43d; &#x43c;&#x430;&#x441;&#x441;&#x438;&#x432; &#x438;&#x437; 10-&#x442;&#x438; &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442;&#x43e;&#x432; &#x442;&#x438;&#x43f;&#x430; Object (&#x441; &#x43f;&#x440;&#x438;&#x432;&#x435;&#x434;&#x435;&#x43d;&#x438;&#x435;&#x43c; &#x43a; &#x442;&#x438;&#x43f;&#x443;, &#x440;&#x430;&#x437;&#x443;&#x43c;&#x435;&#x435;&#x442;&#x441;&#x44f;)" ID="ID_917580439" CREATED="1435122080191" MODIFIED="1435122104690"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      elementData = (E[]) new Object[10];
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="size" STYLE_REF="pre_last_node" ID="ID_474913386" CREATED="1435122062356" MODIFIED="1435122200530"/>
</node>
</node>
<node TEXT="&#x414;&#x43e;&#x431;&#x430;&#x432;&#x43b;&#x435;&#x43d;&#x438;&#x435; &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442;&#x43e;&#x432;" STYLE_REF="gray_node" ID="ID_170611578" CREATED="1435122277883" MODIFIED="1435150977026">
<font BOLD="false"/>
<node TEXT="list.add(&quot;0&quot;);" STYLE_REF="example_node" ID="ID_1065735205" CREATED="1435122288701" MODIFIED="1435122373385">
<hook URI="array-list-add1.png" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="ensureCapacity(size + 1);" STYLE_REF="pre_last_node" ID="ID_604423669" CREATED="1435122309715" MODIFIED="1435122378416"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1055;&#1088;&#1086;&#1074;&#1077;&#1088;&#1103;&#1077;&#1090;&#1089;&#1103;, &#1076;&#1086;&#1089;&#1090;&#1072;&#1090;&#1086;&#1095;&#1085;&#1086; &#1083;&#1080; &#1084;&#1077;&#1089;&#1090;&#1072; &#1074; &#1084;&#1072;&#1089;&#1089;&#1080;&#1074;&#1077; &#1076;&#1083;&#1103; &#1074;&#1089;&#1090;&#1072;&#1074;&#1082;&#1080; &#1085;&#1086;&#1074;&#1086;&#1075;&#1086; &#1101;&#1083;&#1077;&#1084;&#1077;&#1085;&#1090;&#1072;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="elementData[size++] = element;" STYLE_REF="pre_last_node" ID="ID_465219961" CREATED="1435122300196" MODIFIED="1435122382555"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1044;&#1086;&#1073;&#1072;&#1074;&#1083;&#1103;&#1077;&#1090;&#1089;&#1103; &#1101;&#1083;&#1077;&#1084;&#1077;&#1085;&#1090; &#1074; &#1082;&#1086;&#1085;&#1077;&#1094; (&#1089;&#1086;&#1075;&#1083;&#1072;&#1089;&#1085;&#1086; &#1079;&#1085;&#1072;&#1095;&#1077;&#1085;&#1080;&#1102; size) &#1084;&#1072;&#1089;&#1089;&#1080;&#1074;&#1072;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="..." STYLE_REF="example_node" ID="ID_680316026" CREATED="1435150664922" MODIFIED="1435150724591"/>
<node TEXT="list.add(&quot;10&quot;);" STYLE_REF="example_node" ID="ID_1716971157" CREATED="1435150675986" MODIFIED="1435150724592">
<hook URI="array-list-add2.png" SIZE="0.85714287" NAME="ExternalObject"/>
<node TEXT="&#x41f;&#x440;&#x438; &#x434;&#x43e;&#x431;&#x430;&#x432;&#x43b;&#x435;&#x43d;&#x438;&#x438; 11-&#x433;&#x43e; &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442;&#x430;, &#x43f;&#x440;&#x43e;&#x432;&#x435;&#x440;&#x43a;&#x430; &#x43f;&#x43e;&#x43a;&#x430;&#x437;&#x44b;&#x432;&#x430;&#x435;&#x442; &#x447;&#x442;&#x43e; &#x43c;&#x435;&#x441;&#x442;&#x430; &#x432; &#x43c;&#x430;&#x441;&#x441;&#x438;&#x432;&#x435; &#x43d;&#x435;&#x442;. &#x421;&#x43e;&#x43e;&#x442;&#x432;&#x435;&#x442;&#x441;&#x442;&#x432;&#x435;&#x43d;&#x43d;&#x43e; &#x441;&#x43e;&#x437;&#x434;&#x430;&#x435;&#x442;&#x441;&#x44f; &#x43d;&#x43e;&#x432;&#x44b;&#x439; &#x43c;&#x430;&#x441;&#x441;&#x438;&#x432; &#x438; &#x432;&#x44b;&#x437;&#x44b;&#x432;&#x430;&#x435;&#x442;&#x441;&#x44f; System.arraycopy()" STYLE_REF="pre_last_node" ID="ID_1012391956" CREATED="1435150733443" MODIFIED="1435150739384"/>
</node>
</node>
<node TEXT="&#x414;&#x43e;&#x431;&#x430;&#x432;&#x43b;&#x435;&#x43d;&#x438;&#x435; &#x432; &#xab;&#x441;&#x435;&#x440;&#x435;&#x434;&#x438;&#x43d;&#x443;&#xbb; &#x441;&#x43f;&#x438;&#x441;&#x43a;&#x430;" STYLE_REF="gray_node" ID="ID_1447626898" CREATED="1435150766498" MODIFIED="1435150977026">
<font BOLD="false"/>
<node TEXT="ensureCapacity(size + 1);" STYLE_REF="pre_last_node" ID="ID_1658909018" CREATED="1435122309715" MODIFIED="1435122378416"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1055;&#1088;&#1086;&#1074;&#1077;&#1088;&#1103;&#1077;&#1090;&#1089;&#1103;, &#1076;&#1086;&#1089;&#1090;&#1072;&#1090;&#1086;&#1095;&#1085;&#1086; &#1083;&#1080; &#1084;&#1077;&#1089;&#1090;&#1072; &#1074; &#1084;&#1072;&#1089;&#1089;&#1080;&#1074;&#1077; &#1076;&#1083;&#1103; &#1074;&#1089;&#1090;&#1072;&#1074;&#1082;&#1080; &#1085;&#1086;&#1074;&#1086;&#1075;&#1086; &#1101;&#1083;&#1077;&#1084;&#1077;&#1085;&#1090;&#1072;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="System.arraycopy(elementData, index, elementData, index + 1, size - index);" STYLE_REF="pre_last_node" ID="ID_1521000810" CREATED="1435150796826" MODIFIED="1435150963280"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1055;&#1086;&#1076;&#1075;&#1086;&#1090;&#1072;&#1074;&#1083;&#1080;&#1074;&#1072;&#1077;&#1090;&#1089;&#1103; &#1084;&#1077;&#1089;&#1090;&#1086; &#1076;&#1083;&#1103; &#1085;&#1086;&#1074;&#1086;&#1075;&#1086; &#1101;&#1083;&#1077;&#1084;&#1077;&#1085;&#1090;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="elementData[index] = element;&#xa;size++;" STYLE_REF="pre_last_node" ID="ID_1817994989" CREATED="1435150838890" MODIFIED="1435150963279"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1055;&#1077;&#1088;&#1077;&#1079;&#1072;&#1087;&#1080;&#1089;&#1099;&#1074;&#1072;&#1077;&#1090;&#1089;&#1103; &#1079;&#1085;&#1072;&#1095;&#1077;&#1085;&#1080;&#1077; &#1091; &#1101;&#1083;&#1077;&#1084;&#1077;&#1085;&#1090;&#1072; &#1089; &#1091;&#1082;&#1072;&#1079;&#1072;&#1085;&#1085;&#1099;&#1084; &#1080;&#1085;&#1076;&#1077;&#1082;&#1089;&#1086;&#1084;.
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="&#x414;&#x43e;&#x431;&#x430;&#x432;&#x438;&#x442;&#x44c; &#x434;&#x440;&#x443;&#x433;&#x443;&#x44e; &#x43a;&#x43e;&#x43b;&#x43b;&#x435;&#x43a;&#x446;&#x438;&#x44e; &#x432; &#x441;&#x435;&#x440;&#x435;&#x434;&#x438;&#x43d;&#x443;" STYLE_REF="gray_node" ID="ID_281737678" CREATED="1435150892194" MODIFIED="1435150977026">
<font BOLD="false"/>
<node TEXT="addAll(index, Collection)" STYLE_REF="pre_last_node" ID="ID_36366684" CREATED="1435150914930" MODIFIED="1435150963278"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      3 &#1074;&#1099;&#1079;&#1086;&#1074;&#1072; System.arraycopy(). &#1041;&#1091;&#1076;&#1077;&#1090; &#1075;&#1086;&#1088;&#1072;&#1079;&#1076;&#1086; &#1073;&#1099;&#1089;&#1090;&#1088;&#1077;&#1077; &#1087;&#1086;&#1101;&#1083;&#1077;&#1084;&#1077;&#1085;&#1090;&#1085;&#1086;&#1075;&#1086; &#1076;&#1086;&#1073;&#1072;&#1074;&#1083;&#1077;&#1085;&#1080;&#1103;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="&#x423;&#x434;&#x430;&#x43b;&#x435;&#x43d;&#x438;&#x435; &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442;&#x43e;&#x432;" STYLE_REF="gray_node" ID="ID_982271007" CREATED="1435150955418" MODIFIED="1435150977025">
<font BOLD="false"/>
<node TEXT="remove(index)" STYLE_REF="example_node" ID="ID_896763097" CREATED="1435150992578" MODIFIED="1435151127543"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1087;&#1086; &#1080;&#1085;&#1076;&#1077;&#1082;&#1089;&#1091;
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true"/>
<node TEXT="int numMoved = size - index - 1;" STYLE_REF="example_node" ID="ID_940945682" CREATED="1435151046611" MODIFIED="1435151124791"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1054;&#1087;&#1088;&#1077;&#1076;&#1077;&#1083;&#1103;&#1077;&#1090;&#1089;&#1103; &#1082;&#1072;&#1082;&#1086;&#1077; &#1082;&#1086;&#1083;&#1080;&#1095;&#1077;&#1089;&#1090;&#1074;&#1086; &#1101;&#1083;&#1077;&#1084;&#1077;&#1085;&#1090;&#1086;&#1074; &#1085;&#1072;&#1076;&#1086; &#1089;&#1082;&#1086;&#1087;&#1080;&#1088;&#1086;&#1074;&#1072;&#1090;&#1100;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="System.arraycopy(elementData, index + 1, elementData, index, numMoved);" STYLE_REF="example_node" ID="ID_118355843" CREATED="1435151064434" MODIFIED="1435151124790"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1050;&#1086;&#1087;&#1080;&#1088;&#1091;&#1102;&#1090;&#1089;&#1103; &#1101;&#1083;&#1077;&#1084;&#1077;&#1085;&#1090;&#1099;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="remove(value)" STYLE_REF="example_node" ID="ID_1937493705" CREATED="1435150997849" MODIFIED="1435151127542"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1087;&#1086; &#1079;&#1085;&#1072;&#1095;&#1077;&#1085;&#1080;&#1102;
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true"/>
<node TEXT="&#x412; &#x446;&#x438;&#x43a;&#x43b;&#x435; &#x43f;&#x440;&#x43e;&#x441;&#x43c;&#x430;&#x442;&#x440;&#x438;&#x432;&#x430;&#x44e;&#x442;&#x441;&#x44f; &#x432;&#x441;&#x435; &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442;&#x44b; &#x441;&#x43f;&#x438;&#x441;&#x43a;&#x430;, &#x434;&#x43e; &#x442;&#x435;&#x445; &#x43f;&#x43e;&#x440; &#x43f;&#x43e;&#x43a;&#x430; &#x43d;&#x435; &#x431;&#x443;&#x434;&#x435;&#x442; &#x43d;&#x430;&#x439;&#x434;&#x435;&#x43d;&#x43e; &#x441;&#x43e;&#x43e;&#x442;&#x432;&#x435;&#x442;&#x441;&#x442;&#x432;&#x438;&#x435;. &#x423;&#x434;&#x430;&#x43b;&#x435;&#x43d; &#x431;&#x443;&#x434;&#x435;&#x442; &#x43b;&#x438;&#x448;&#x44c; &#x43f;&#x435;&#x440;&#x432;&#x44b;&#x439; &#x43d;&#x430;&#x439;&#x434;&#x435;&#x43d;&#x43d;&#x44b;&#x439; &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442;" STYLE_REF="pre_last_node" ID="ID_1524471016" CREATED="1435151022634" MODIFIED="1435151120224"/>
</node>
</node>
</node>
<node TEXT="&#x418;&#x435;&#x440;&#x430;&#x440;&#x445;&#x438;&#x44f;" STYLE_REF="yellow_node" ID="ID_741047146" CREATED="1435151225089" MODIFIED="1435151256078">
<node TEXT="" ID="ID_872301991" CREATED="1435151233634" MODIFIED="1435151240391">
<hook URI="array-list-hierarchy.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
</node>
</node>
<node TEXT="LinkedList" STYLE_REF="gray_node" ID="ID_697869785" CREATED="1435118718006" MODIFIED="1435119021415">
<node TEXT="K&#x43b;&#x430;&#x441;&#x441;&#x438;&#x447;&#x435;&#x441;&#x43a;&#x438;&#x439; &#x441;&#x432;&#x44f;&#x437;&#x43d;&#x44b;&#x439; &#x441;&#x43f;&#x438;&#x441;&#x43e;&#x43a;, &#x43e;&#x441;&#x43d;&#x43e;&#x432;&#x430;&#x43d;&#x43d;&#x44b;&#x439; &#x43d;&#x430; &#x43e;&#x431;&#x44a;&#x435;&#x43a;&#x442;&#x430;&#x445; &#x441; &#x441;&#x441;&#x44b;&#x43b;&#x43a;&#x430;&#x43c;&#x438; &#x43c;&#x435;&#x436;&#x434;&#x443; &#x43d;&#x438;&#x43c;&#x438;. K&#x430;&#x436;&#x434;&#x44b;&#x439; &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442; &#x441;&#x442;&#x440;&#x443;&#x43a;&#x442;&#x443;&#x440;&#x44b; &#x441;&#x43e;&#x434;&#x435;&#x440;&#x436;&#x438;&#x442; &#x443;&#x43a;&#x430;&#x437;&#x430;&#x442;&#x435;&#x43b;&#x438; &#x43d;&#x430; &#x43f;&#x440;&#x435;&#x434;&#x44b;&#x434;&#x443;&#x449;&#x438;&#x439; &#x438; &#x441;&#x43b;&#x435;&#x434;&#x443;&#x44e;&#x449;&#x438;&#x439; &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442;&#x44b;" STYLE_REF="pre_last_node" ID="ID_1834909296" CREATED="1435118766502" MODIFIED="1435227247311"/>
<node TEXT="&#x41f;&#x440;&#x435;&#x434;&#x43f;&#x43e;&#x447;&#x442;&#x438;&#x442;&#x435;&#x43b;&#x44c;&#x43d;&#x43e; &#x43f;&#x440;&#x438;&#x43c;&#x435;&#x43d;&#x44f;&#x442;&#x44c;, &#x43a;&#x43e;&#x433;&#x434;&#x430; &#x43f;&#x440;&#x43e;&#x438;&#x441;&#x445;&#x43e;&#x434;&#x438;&#x442; &#x430;&#x43a;&#x442;&#x438;&#x432;&#x43d;&#x430;&#x44f; &#x440;&#x430;&#x431;&#x43e;&#x442;&#x430; (&#x432;&#x441;&#x442;&#x430;&#x432;&#x43a;&#x430;/&#x443;&#x434;&#x430;&#x43b;&#x435;&#x43d;&#x438;&#x435;) &#x441; &#x441;&#x435;&#x440;&#x435;&#x434;&#x438;&#x43d;&#x43e;&#x439; &#x441;&#x43f;&#x438;&#x441;&#x43a;&#x430; &#x438;&#x43b;&#x438; &#x432; &#x441;&#x43b;&#x443;&#x447;&#x430;&#x44f;&#x445;, &#x43a;&#x43e;&#x433;&#x434;&#x430; &#x43d;&#x435;&#x43e;&#x431;&#x445;&#x43e;&#x434;&#x438;&#x43c;&#x43e; &#x433;&#x430;&#x440;&#x430;&#x43d;&#x442;&#x438;&#x440;&#x43e;&#x432;&#x430;&#x43d;&#x43d;&#x43e;&#x435; &#x432;&#x440;&#x435;&#x43c;&#x44f; &#x434;&#x43e;&#x431;&#x430;&#x432;&#x43b;&#x435;&#x43d;&#x438;&#x44f; &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442;&#x430; &#x432; &#x441;&#x43f;&#x438;&#x441;&#x43e;&#x43a;" STYLE_REF="pre_last_node" ID="ID_1378219422" CREATED="1435119252134" MODIFIED="1435119379359"/>
<node TEXT="&#x41f;&#x440;&#x435;&#x438;&#x43c;&#x443;&#x449;&#x435;&#x441;&#x442;&#x432;&#x430;" STYLE_REF="yellow_node" ID="ID_799184988" CREATED="1435119103023" MODIFIED="1435120914089">
<icon BUILTIN="addition"/>
<node TEXT="&#x417;&#x430; &#x43f;&#x43e;&#x441;&#x442;&#x43e;&#x44f;&#x43d;&#x43d;&#x43e;&#x435; &#x432;&#x440;&#x435;&#x43c;&#x44f; &#x43c;&#x43e;&#x436;&#x435;&#x442; &#x432;&#x44b;&#x43f;&#x43e;&#x43b;&#x43d;&#x44f;&#x442;&#x44c; &#x432;&#x441;&#x442;&#x430;&#x432;&#x43a;&#x443;/&#x443;&#x434;&#x430;&#x43b;&#x435;&#x43d;&#x438;&#x435; &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442;&#x43e;&#x432; &#x432; &#x441;&#x43f;&#x438;&#x441;&#x43a;&#x435; O(n)" STYLE_REF="pre_last_node" ID="ID_896828076" CREATED="1435119127991" MODIFIED="1435296372853"/>
<node TEXT="&#x41d;&#x430; &#x432;&#x441;&#x442;&#x430;&#x432;&#x43a;&#x443; &#x438; &#x443;&#x434;&#x430;&#x43b;&#x435;&#x43d;&#x438;&#x435; &#x438;&#x437; &#x441;&#x435;&#x440;&#x435;&#x434;&#x438;&#x43d;&#x44b; &#x441;&#x43f;&#x438;&#x441;&#x43a;&#x430;, &#x43f;&#x43e;&#x43b;&#x443;&#x447;&#x435;&#x43d;&#x438;&#x435; &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442;&#x430; &#x43f;&#x43e; &#x438;&#x43d;&#x434;&#x435;&#x43a;&#x441;&#x443; &#x438;&#x43b;&#x438; &#x437;&#x43d;&#x430;&#x447;&#x435;&#x43d;&#x438;&#x44e; &#x43f;&#x43e;&#x442;&#x440;&#x435;&#x431;&#x443;&#x435;&#x442;&#x441;&#x44f; &#x43b;&#x438;&#x43d;&#x435;&#x439;&#x43d;&#x43e;&#x435; &#x432;&#x440;&#x435;&#x43c;&#x44f; O(n). &#x41e;&#x434;&#x43d;&#x430;&#x43a;&#x43e;, &#x43d;&#x430; &#x434;&#x43e;&#x431;&#x430;&#x432;&#x43b;&#x435;&#x43d;&#x438;&#x435; &#x438; &#x443;&#x434;&#x430;&#x43b;&#x435;&#x43d;&#x438;&#x435; &#x438;&#x437; &#x441;&#x435;&#x440;&#x435;&#x434;&#x438;&#x43d;&#x44b; &#x441;&#x43f;&#x438;&#x441;&#x43a;&#x430;, &#x438;&#x441;&#x43f;&#x43e;&#x43b;&#x44c;&#x437;&#x443;&#x44f; ListIterator.add() &#x438; ListIterator.remove(), &#x43f;&#x43e;&#x442;&#x440;&#x435;&#x431;&#x443;&#x435;&#x442;&#x441;&#x44f; O(1)" STYLE_REF="pre_last_node" ID="ID_1398548112" CREATED="1435296570958" MODIFIED="1435296578532"/>
<node TEXT="&#x414;&#x43e;&#x441;&#x442;&#x443;&#x43f; &#x43a; &#x43f;&#x435;&#x440;&#x432;&#x43e;&#x43c;&#x443; &#x438; &#x43f;&#x43e;&#x441;&#x43b;&#x435;&#x434;&#x43d;&#x435;&#x43c;&#x443; &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442;&#x443; &#x441;&#x43f;&#x438;&#x441;&#x43a;&#x430; &#x432;&#x441;&#x435;&#x433;&#x434;&#x430; &#x43e;&#x441;&#x443;&#x449;&#x435;&#x441;&#x442;&#x432;&#x43b;&#x44f;&#x435;&#x442;&#x441;&#x44f; &#x437;&#x430; &#x43a;&#x43e;&#x43d;&#x441;&#x442;&#x430;&#x43d;&#x442;&#x43d;&#x43e;&#x435; &#x432;&#x440;&#x435;&#x43c;&#x44f; O(1)" STYLE_REF="pre_last_node" ID="ID_11796584" CREATED="1435119158951" MODIFIED="1435296363436"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1057;&#1089;&#1099;&#1083;&#1082;&#1080; &#1087;&#1086;&#1089;&#1090;&#1086;&#1103;&#1085;&#1085;&#1086; &#1093;&#1088;&#1072;&#1085;&#1103;&#1090;&#1089;&#1103; &#1085;&#1072; &#1087;&#1077;&#1088;&#1074;&#1099;&#1081; &#1080; &#1087;&#1086;&#1089;&#1083;&#1077;&#1076;&#1085;&#1080;&#1081;, &#1090;&#1072;&#1082; &#1095;&#1090;&#1086; &#1076;&#1086;&#1073;&#1072;&#1074;&#1083;&#1077;&#1085;&#1080;&#1077; &#1101;&#1083;&#1077;&#1084;&#1077;&#1085;&#1090;&#1072; &#1074; &#1082;&#1086;&#1085;&#1077;&#1094; &#1089;&#1087;&#1080;&#1089;&#1082;&#1072; &#1074;&#1086;&#1074;&#1089;&#1077; &#1085;&#1077; &#1079;&#1085;&#1072;&#1095;&#1080;&#1090;, &#1095;&#1090;&#1086; &#1087;&#1088;&#1080;&#1076;&#1077;&#1090;&#1089;&#1103; &#1087;&#1077;&#1088;&#1077;&#1073;&#1080;&#1088;&#1072;&#1090;&#1100; &#1074;&#1077;&#1089;&#1100; &#1089;&#1087;&#1080;&#1089;&#1086;&#1082; &#1074; &#1087;&#1086;&#1080;&#1089;&#1082;&#1072;&#1093; &#1087;&#1086;&#1089;&#1083;&#1077;&#1076;&#1085;&#1077;&#1075;&#1086; &#1101;&#1083;&#1077;&#1084;&#1077;&#1085;&#1090;&#1072;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="&#x41d;&#x435;&#x434;&#x43e;&#x441;&#x442;&#x430;&#x442;&#x43a;&#x438;" STYLE_REF="yellow_node" ID="ID_1234066866" CREATED="1435119107230" MODIFIED="1435119227338">
<icon BUILTIN="subtraction"/>
<node TEXT="&#x414;&#x43e;&#x441;&#x442;&#x443;&#x43f; &#x43a; &#x43f;&#x440;&#x43e;&#x438;&#x437;&#x432;&#x43e;&#x43b;&#x44c;&#x43d;&#x43e;&#x43c;&#x443; &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442;&#x443; &#x43e;&#x441;&#x443;&#x449;&#x435;&#x441;&#x442;&#x432;&#x43b;&#x44f;&#x435;&#x442;&#x441;&#x44f; &#x437;&#x430; &#x43b;&#x438;&#x43d;&#x435;&#x439;&#x43d;&#x43e;&#x435; &#x432;&#x440;&#x435;&#x43c;&#x44f;" STYLE_REF="pre_last_node" ID="ID_999129991" CREATED="1435119146662" MODIFIED="1435119379373"/>
<node TEXT="&#x41f;&#x440;&#x43e;&#x438;&#x433;&#x440;&#x44b;&#x432;&#x430;&#x435;&#x442; ArrayList &#x438; &#x43f;&#x43e; &#x43f;&#x43e;&#x442;&#x440;&#x435;&#x431;&#x43b;&#x44f;&#x435;&#x43c;&#x43e;&#x439; &#x43f;&#x430;&#x43c;&#x44f;&#x442;&#x438; &#x438; &#x43f;&#x43e; &#x441;&#x43a;&#x43e;&#x440;&#x43e;&#x441;&#x442;&#x438; &#x432;&#x44b;&#x43f;&#x43e;&#x43b;&#x43d;&#x435;&#x43d;&#x438;&#x44f; &#x43e;&#x43f;&#x435;&#x440;&#x430;&#x446;&#x438;&#x439;" STYLE_REF="pre_last_node" ID="ID_1811937364" CREATED="1435119212414" MODIFIED="1435119379376"/>
</node>
<node TEXT="&#x418;&#x441;&#x43f;&#x43e;&#x43b;&#x44c;&#x437;&#x43e;&#x432;&#x430;&#x43d;&#x438;&#x435;" STYLE_REF="yellow_node" ID="ID_763547789" CREATED="1435227262793" MODIFIED="1435295454438">
<node TEXT="&#x421;&#x43e;&#x437;&#x434;&#x430;&#x43d;&#x438;&#x435; &#x43e;&#x431;&#x44a;&#x435;&#x43a;&#x442;&#x430;" STYLE_REF="gray_node" ID="ID_1248290157" CREATED="1435227294537" MODIFIED="1435295465125">
<font BOLD="false"/>
<node TEXT="List&lt;String&gt; list = new LinkedList&lt;String&gt;();" STYLE_REF="pre_last_node" ID="ID_1200634231" CREATED="1435227283096" MODIFIED="1435295502037">
<hook URI="linkedlist-1.png" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="header" STYLE_REF="yellow_node" ID="ID_1919577981" CREATED="1435227311328" MODIFIED="1435295508862">
<node TEXT="&#x43f;&#x441;&#x435;&#x432;&#x434;&#x43e;-&#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442; &#x441;&#x43f;&#x438;&#x441;&#x43a;&#x430;. &#x415;&#x433;&#x43e; &#x437;&#x43d;&#x430;&#x447;&#x435;&#x43d;&#x438;&#x435; &#x432;&#x441;&#x435;&#x433;&#x434;&#x430; &#x440;&#x430;&#x432;&#x43d;&#x43e; null, a &#x441;&#x432;&#x43e;&#x439;&#x441;&#x442;&#x432;&#x430; next &#x438; prev &#x432;&#x441;&#x435;&#x433;&#x434;&#x430; &#x443;&#x43a;&#x430;&#x437;&#x44b;&#x432;&#x430;&#x44e;&#x442; &#x43d;&#x430; &#x43f;&#x435;&#x440;&#x432;&#x44b;&#x439; &#x438; &#x43f;&#x43e;&#x441;&#x43b;&#x435;&#x434;&#x43d;&#x438;&#x439; &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442; &#x441;&#x43f;&#x438;&#x441;&#x43a;&#x430; &#x441;&#x43e;&#x43e;&#x442;&#x432;&#x435;&#x442;&#x441;&#x442;&#x432;&#x435;&#x43d;&#x43d;&#x43e;" STYLE_REF="pre_last_node" ID="ID_1758889880" CREATED="1435294850049" MODIFIED="1435295513302"/>
</node>
<node TEXT="size" STYLE_REF="yellow_node" ID="ID_1278723146" CREATED="1435227316912" MODIFIED="1435295508862">
<node TEXT="&#x422;&#x430;&#x43a; &#x43a;&#x430;&#x43a; &#x43d;&#x430; &#x434;&#x430;&#x43d;&#x43d;&#x44b;&#x439; &#x43c;&#x43e;&#x43c;&#x435;&#x43d;&#x442; &#x441;&#x43f;&#x438;&#x441;&#x43e;&#x43a; &#x435;&#x449;&#x435; &#x43f;&#x443;&#x441;&#x442;, &#x441;&#x432;&#x43e;&#x439;&#x441;&#x442;&#x432;&#x430; next &#x438; prev &#x443;&#x43a;&#x430;&#x437;&#x44b;&#x432;&#x430;&#x44e;&#x442; &#x441;&#x430;&#x43c;&#x438; &#x43d;&#x430; &#x441;&#x435;&#x431;&#x44f; (&#x442;.&#x435;. &#x43d;&#x430; &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442; header). &#x420;&#x430;&#x437;&#x43c;&#x435;&#x440; &#x441;&#x43f;&#x438;&#x441;&#x43a;&#x430; size &#x440;&#x430;&#x432;&#x435;&#x43d; 0" STYLE_REF="pre_last_node" ID="ID_1229554045" CREATED="1435294887090" MODIFIED="1435295513303"/>
</node>
</node>
</node>
<node TEXT="&#x414;&#x43e;&#x431;&#x430;&#x432;&#x43b;&#x435;&#x43d;&#x438;&#x435; &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442;&#x43e;&#x432;" STYLE_REF="gray_node" ID="ID_190483912" CREATED="1435294942336" MODIFIED="1435295465125"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1042;&#1099;&#1087;&#1086;&#1083;&#1085;&#1103;&#1077;&#1090;&#1089;&#1103; &#1079;&#1072; &#1074;&#1088;&#1077;&#1084;&#1103; O(1)
    </p>
  </body>
</html>
</richcontent>
<font BOLD="false"/>
<node TEXT="addFirst(value)" STYLE_REF="example_node" ID="ID_773294022" CREATED="1435294988529" MODIFIED="1435296031037">
<node TEXT="&#x414;&#x43e;&#x431;&#x430;&#x432;&#x43b;&#x435;&#x43d;&#x438;&#x435; &#x432; &#x43d;&#x430;&#x447;&#x430;&#x43b;&#x43e;" STYLE_REF="pre_last_node" ID="ID_1617552428" CREATED="1435294990920" MODIFIED="1435295494493"/>
</node>
<node TEXT="add(value), addLast(value)" STYLE_REF="example_node" ID="ID_1807178891" CREATED="1435294966737" MODIFIED="1435296031037">
<node TEXT="&#x414;&#x43e;&#x431;&#x430;&#x432;&#x43b;&#x435;&#x43d;&#x438;&#x435; &#x432; &#x43a;&#x43e;&#x43d;&#x435;&#x446; &#x441;&#x43f;&#x438;&#x441;&#x43a;&#x430;" STYLE_REF="pre_last_node" ID="ID_1896924200" CREATED="1435295027304" MODIFIED="1435295488086"/>
</node>
<node TEXT="list.add(&quot;0&quot;);" STYLE_REF="yellow_node" ID="ID_160051563" CREATED="1435295372218" MODIFIED="1435295476398">
<node TEXT="&#x421;&#x43e;&#x437;&#x434;&#x430;&#x435;&#x442;&#x441;&#x44f; &#x43d;&#x43e;&#x432;&#x44b;&#x439; &#x43d;&#x43e;&#x432;&#x44b;&#x439; &#x44d;&#x43a;&#x437;&#x435;&#x43c;&#x43f;&#x43b;&#x44f;&#x440; &#x43a;&#x43b;&#x430;&#x441;&#x441;&#x430; Entry" STYLE_REF="pre_last_node" ID="ID_1521186880" CREATED="1435295125048" MODIFIED="1435295488088"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Entry newEntry = new Entry(&quot;0&quot;, header, header.prev);
    </p>
  </body>
</html>
</richcontent>
<hook URI="linkedlist-2.png" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="private static class Entry&lt;E&gt; {&#xa;    E element;&#xa;    Entry&lt;E&gt; next;&#xa;    Entry&lt;E&gt; prev;&#xa;   &#xa;    Entry(E element, Entry&lt;E&gt; next, Entry&lt;E&gt; prev) {&#xa;        this.element = element;&#xa;        this.next = next;&#xa;        this.prev = prev;&#xa;    }&#xa;}" STYLE_REF="example_node" ID="ID_781146797" CREATED="1435295109281" MODIFIED="1435295248382"/>
</node>
<node TEXT="&#x41f;&#x435;&#x440;&#x435;&#x43e;&#x43f;&#x440;&#x435;&#x434;&#x435;&#x43b;&#x44f;&#x44e;&#x442;&#x441;&#x44f; &#x443;&#x43a;&#x430;&#x437;&#x430;&#x442;&#x435;&#x43b;&#x438; &#x43d;&#x430; &#x43f;&#x440;&#x435;&#x434;&#x44b;&#x434;&#x443;&#x449;&#x438;&#x439; &#x438; &#x441;&#x43b;&#x435;&#x434;&#x443;&#x44e;&#x449;&#x438;&#x439; &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442;" STYLE_REF="pre_last_node" ID="ID_1100087988" CREATED="1435295197640" MODIFIED="1435295488088"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      newEntry.prev.next = newEntry;
    </p>
    <p>
      newEntry.next.prev = newEntry;
    </p>
    <p>
      size++;
    </p>
  </body>
</html>
</richcontent>
<hook URI="linkedlist-3.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
</node>
<node TEXT="list.add(&quot;1&quot;);" STYLE_REF="yellow_node" ID="ID_1021022830" CREATED="1435295316264" MODIFIED="1435295476398">
<node TEXT="Entry newEntry = new Entry(&quot;1&quot;, header, header.prev);" STYLE_REF="pre_last_node" ID="ID_686891976" CREATED="1435295330863" MODIFIED="1435295488088">
<hook URI="linkedlist-4.png" SIZE="0.82304525" NAME="ExternalObject"/>
</node>
<node TEXT="" STYLE_REF="pre_last_node" ID="ID_893011807" CREATED="1435295424401" MODIFIED="1435295488089">
<hook URI="linkedlist-5.png" SIZE="0.82304525" NAME="ExternalObject"/>
</node>
</node>
</node>
<node TEXT="&#x414;&#x43e;&#x431;&#x430;&#x432;&#x43b;&#x435;&#x43d;&#x438;&#x435; &#x432; &#xab;&#x441;&#x435;&#x440;&#x435;&#x434;&#x438;&#x43d;&#x443;&#xbb;" STYLE_REF="gray_node" ID="ID_249966477" CREATED="1435295576912" MODIFIED="1435295823396">
<font BOLD="false"/>
<node TEXT="add(index, value)" STYLE_REF="example_node" ID="ID_1960122561" CREATED="1435295660081" MODIFIED="1435296031036">
<node TEXT="&#x41d;&#x430;&#x43f;&#x440;&#x430;&#x432;&#x43b;&#x435;&#x43d;&#x438;&#x435; &#x43e;&#x431;&#x445;&#x43e;&#x434;&#x430; &#x43e;&#x43f;&#x440;&#x435;&#x434;&#x435;&#x43b;&#x44f;&#x435;&#x442;&#x441;&#x44f; &#x443;&#x441;&#x43b;&#x43e;&#x432;&#x438;&#x435;&#x43c; (index &lt; (size &gt;&gt; 1))" STYLE_REF="pre_last_node" ID="ID_824958624" CREATED="1435295711288" MODIFIED="1435295837478"/>
<node TEXT="&#x41f;&#x43e; &#x444;&#x430;&#x43a;&#x442;&#x443; &#x43f;&#x43e;&#x43b;&#x443;&#x447;&#x430;&#x435;&#x442;&#x441;&#x44f; &#x447;&#x442;&#x43e; &#x434;&#x43b;&#x44f; &#x43d;&#x430;&#x445;&#x43e;&#x436;&#x434;&#x435;&#x43d;&#x438;&#x44f; &#x43d;&#x443;&#x436;&#x43d;&#x43e;&#x433;&#x43e; &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442;&#x430; &#x43f;&#x435;&#x440;&#x435;&#x431;&#x438;&#x440;&#x430;&#x435;&#x442;&#x441;&#x44f; &#x43d;&#x435; &#x431;&#x43e;&#x43b;&#x44c;&#x448;&#x435; &#x43f;&#x43e;&#x43b;&#x43e;&#x432;&#x438;&#x43d;&#x44b; &#x441;&#x43f;&#x438;&#x441;&#x43a;&#x430;" STYLE_REF="pre_last_node" ID="ID_1969628517" CREATED="1435295720079" MODIFIED="1435295837480"/>
</node>
<node TEXT="list.add(1, &quot;100&quot;);" STYLE_REF="yellow_node" ID="ID_790876727" CREATED="1435295752472" MODIFIED="1435295828989">
<node TEXT="Entry newEntry = new Entry(&quot;100&quot;, entry, entry.prev);" STYLE_REF="pre_last_node" ID="ID_317802777" CREATED="1435295763327" MODIFIED="1435295926533">
<hook URI="linkedlist-6.png" SIZE="0.75" NAME="ExternalObject"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      // entry &#1091;&#1082;&#1072;&#1079;&#1099;&#1074;&#1072;&#1077;&#1090; &#1085;&#1072; &#1101;&#1083;&#1077;&#1084;&#1077;&#1085;&#1090; &#1089; &#1080;&#1085;&#1076;&#1077;&#1082;&#1089;&#1086;&#1084; 1, entry.prev &#1085;&#1072; &#1101;&#1083;&#1077;&#1084;&#1077;&#1085;&#1090; &#1089; &#1080;&#1085;&#1076;&#1077;&#1082;&#1089;&#1086;&#1084; 0
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="" STYLE_REF="pre_last_node" ID="ID_684795079" CREATED="1435295799802" MODIFIED="1435295837481">
<hook URI="linkedlist-7.png" SIZE="0.75" NAME="ExternalObject"/>
</node>
</node>
</node>
<node TEXT="&#x423;&#x434;&#x430;&#x43b;&#x435;&#x43d;&#x438;&#x435;" STYLE_REF="gray_node" ID="ID_1870641076" CREATED="1435295809976" MODIFIED="1435295823396">
<font BOLD="false"/>
<node TEXT="removeFirst(), removeLast()" STYLE_REF="example_node" ID="ID_1121245484" CREATED="1435295967272" MODIFIED="1435296023430"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1079;&#1072; &#1074;&#1088;&#1077;&#1084;&#1103; O(1)
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="remove(index), remove(value)" STYLE_REF="example_node" ID="ID_1754844956" CREATED="1435295986736" MODIFIED="1435296023430"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      O(n)
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="list.remove(&quot;100&quot;);" STYLE_REF="yellow_node" ID="ID_757468380" CREATED="1435296015889" MODIFIED="1435296219174"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1059;&#1076;&#1072;&#1083;&#1077;&#1085; &#1073;&#1091;&#1076;&#1077;&#1090; &#1083;&#1080;&#1096;&#1100; &#1087;&#1077;&#1088;&#1074;&#1099;&#1081; &#1085;&#1072;&#1081;&#1076;&#1077;&#1085;&#1085;&#1099;&#1081; &#1101;&#1083;&#1077;&#1084;&#1077;&#1085;&#1090;
    </p>
  </body>
</html>
</richcontent>
<node TEXT="&#x41f;&#x43e;&#x438;&#x441;&#x43a; &#x43f;&#x435;&#x440;&#x432;&#x43e;&#x433;&#x43e; &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442;&#x430; &#x441; &#x441;&#x43e;&#x43e;&#x442;&#x432;&#x435;&#x442;&#x441;&#x442;&#x432;&#x443;&#x44e;&#x449;&#x438;&#x43c; &#x437;&#x43d;&#x430;&#x447;&#x435;&#x43d;&#x438;&#x435;&#x43c;" STYLE_REF="pre_last_node" ID="ID_1060276892" CREATED="1435296046737" MODIFIED="1435296175501">
<hook URI="linkedlist-8.png" SIZE="0.75" NAME="ExternalObject"/>
</node>
<node TEXT="&#x41f;&#x435;&#x440;&#x435;&#x43e;&#x43f;&#x440;&#x435;&#x434;&#x435;&#x43b;&#x44f;&#x44e;&#x442;&#x441;&#x44f; &#x443;&#x43a;&#x430;&#x437;&#x430;&#x442;&#x435;&#x43b;&#x438; &#x43d;&#x430; &#x43f;&#x440;&#x435;&#x434;&#x44b;&#x434;&#x443;&#x449;&#x438;&#x439; &#x438; &#x441;&#x43b;&#x435;&#x434;&#x443;&#x44e;&#x449;&#x438;&#x439; &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442;" STYLE_REF="pre_last_node" ID="ID_724210352" CREATED="1435296083455" MODIFIED="1435296175503"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      // &#1047;&#1085;&#1072;&#1095;&#1077;&#1085;&#1080;&#1077; &#1091;&#1076;&#1072;&#1083;&#1103;&#1077;&#1084;&#1086;&#1075;&#1086; &#1101;&#1083;&#1077;&#1084;&#1077;&#1085;&#1090;&#1072; &#1089;&#1086;&#1093;&#1088;&#1072;&#1085;&#1103;&#1077;&#1090;&#1089;&#1103;
    </p>
    <p>
      // &#1076;&#1083;&#1103; &#1090;&#1086;&#1075;&#1086; &#1095;&#1090;&#1086;&#1073;&#1099; &#1074; &#1082;&#1086;&#1085;&#1094;&#1077; &#1084;&#1077;&#1090;&#1086;&#1076;&#1072; &#1074;&#1077;&#1088;&#1085;&#1091;&#1090;&#1100; &#1077;&#1075;&#1086;
    </p>
    <p>
      E result = e.element;
    </p>
    <p>
      e.prev.next = e.next;
    </p>
    <p>
      e.next.prev = e.prev;
    </p>
  </body>
</html>
</richcontent>
<hook URI="linkedlist-9.png" SIZE="0.75" NAME="ExternalObject"/>
</node>
<node TEXT="&#x423;&#x434;&#x430;&#x43b;&#x435;&#x43d;&#x438;&#x435; &#x443;&#x43a;&#x430;&#x437;&#x430;&#x442;&#x435;&#x43b;&#x435;&#x439; &#x43d;&#x430; &#x434;&#x440;&#x443;&#x433;&#x438;&#x435; &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442;&#x44b; &#x438; &#x43f;&#x440;&#x435;&#x434;&#x430;&#x43d;&#x438;&#x435; &#x437;&#x430;&#x431;&#x432;&#x435;&#x43d;&#x438;&#x44e; &#x441;&#x430;&#x43c;&#x43e;&#x433;&#x43e; &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442;&#x430;" STYLE_REF="pre_last_node" ID="ID_1706040214" CREATED="1435296119391" MODIFIED="1435296175504">
<hook URI="linkedlist-10.png" SIZE="0.75" NAME="ExternalObject"/>
</node>
</node>
</node>
<node TEXT="&#x418;&#x442;&#x435;&#x440;&#x430;&#x442;&#x43e;&#x440;" STYLE_REF="gray_node" ID="ID_694168252" CREATED="1435296241248" MODIFIED="1435304259766">
<font BOLD="false"/>
<node TEXT="ListIterator&lt;String&gt; itr = list.listIterator();" STYLE_REF="example_node" ID="ID_1431542737" CREATED="1435296247200" MODIFIED="1435296612133"/>
<node TEXT="ListIterator&lt;String&gt; itr = list.descendingIterator()" STYLE_REF="example_node" ID="ID_1139345771" CREATED="1435296276192" MODIFIED="1435296612136"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1054;&#1073;&#1093;&#1086;&#1076; &#1089; &#1082;&#1086;&#1085;&#1094;&#1072; &#1089;&#1087;&#1080;&#1089;&#1082;&#1072;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="while (itr.hasNext())&#xa;    System.out.println(itr.next());" STYLE_REF="example_node" ID="ID_1922370213" CREATED="1435296316751" MODIFIED="1435296612138"/>
<node TEXT="ConcurrentModificationException, &#x435;&#x441;&#x43b;&#x438; &#x43f;&#x43e;&#x441;&#x43b;&#x435; &#x441;&#x43e;&#x437;&#x434;&#x430;&#x43d;&#x438;&#x44f; &#x438;&#x442;&#x435;&#x440;&#x430;&#x442;&#x43e;&#x440;&#x430;, &#x441;&#x43f;&#x438;&#x441;&#x43e;&#x43a; &#x431;&#x44b;&#x43b; &#x438;&#x437;&#x43c;&#x435;&#x43d;&#x435;&#x43d; &#x43d;&#x435; &#x447;&#x435;&#x440;&#x435;&#x437; &#x441;&#x43e;&#x431;&#x441;&#x442;&#x432;&#x435;&#x43d;&#x43d;&#x44b;&#x435; &#x43c;&#x435;&#x442;&#x43e;&#x434;&#x44b; &#x438;&#x442;&#x435;&#x440;&#x430;&#x442;&#x43e;&#x440;&#x430;" STYLE_REF="pre_last_node" ID="ID_329390223" CREATED="1435296597032" MODIFIED="1435296615604"/>
</node>
</node>
<node TEXT="&#x418;&#x435;&#x440;&#x430;&#x440;&#x445;&#x438;&#x44f;" STYLE_REF="yellow_node" ID="ID_1842854702" CREATED="1435151225089" MODIFIED="1435151256078">
<node TEXT="" ID="ID_1043251475" CREATED="1435151233634" MODIFIED="1435160487525">
<hook URI="linkedlist-hierarchy.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
</node>
</node>
<node TEXT="HashMap" STYLE_REF="gray_node" ID="ID_269083326" CREATED="1435119478838" MODIFIED="1435120895043">
<node TEXT="HashMap &#x441;&#x43e;&#x441;&#x442;&#x43e;&#x438;&#x442; &#x438;&#x437; &#xab;&#x43a;&#x43e;&#x440;&#x437;&#x438;&#x43d;&#xbb; (bucket`&#x43e;&#x432;). &#x421; &#x442;&#x435;&#x445;&#x43d;&#x438;&#x447;&#x435;&#x441;&#x43a;&#x43e;&#x439; &#x442;&#x43e;&#x447;&#x43a;&#x438; &#x437;&#x440;&#x435;&#x43d;&#x438;&#x44f; &#xab;&#x43a;&#x43e;&#x440;&#x437;&#x438;&#x43d;&#x44b;&#xbb; &#x2014; &#x44d;&#x442;&#x43e; &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442;&#x44b; &#x43c;&#x430;&#x441;&#x441;&#x438;&#x432;&#x430;, &#x43a;&#x43e;&#x442;&#x43e;&#x440;&#x44b;&#x435; &#x445;&#x440;&#x430;&#x43d;&#x44f;&#x442; &#x441;&#x441;&#x44b;&#x43b;&#x43a;&#x438; &#x43d;&#x430; &#x441;&#x43f;&#x438;&#x441;&#x43a;&#x438; &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442;&#x43e;&#x432;. &#x41f;&#x440;&#x438; &#x434;&#x43e;&#x431;&#x430;&#x432;&#x43b;&#x435;&#x43d;&#x438;&#x438; &#x43d;&#x43e;&#x432;&#x43e;&#x439; &#x43f;&#x430;&#x440;&#x44b; &#x43a;&#x43b;&#x44e;&#x447;-&#x437;&#x43d;&#x430;&#x447;&#x435;&#x43d;&#x438;&#x435;, &#x432;&#x44b;&#x447;&#x438;&#x441;&#x43b;&#x44f;&#x435;&#x442; &#x445;&#x435;&#x448;-&#x43a;&#x43e;&#x434; &#x43a;&#x43b;&#x44e;&#x447;&#x430;, &#x43d;&#x430; &#x43e;&#x441;&#x43d;&#x43e;&#x432;&#x430;&#x43d;&#x438;&#x438; &#x43a;&#x43e;&#x442;&#x43e;&#x440;&#x43e;&#x433;&#x43e; &#x432;&#x44b;&#x447;&#x438;&#x441;&#x43b;&#x44f;&#x435;&#x442;&#x441;&#x44f; &#x43d;&#x43e;&#x43c;&#x435;&#x440; &#x43a;&#x43e;&#x440;&#x437;&#x438;&#x43d;&#x44b; (&#x43d;&#x43e;&#x43c;&#x435;&#x440; &#x44f;&#x447;&#x435;&#x439;&#x43a;&#x438; &#x43c;&#x430;&#x441;&#x441;&#x438;&#x432;&#x430;), &#x432; &#x43a;&#x43e;&#x442;&#x43e;&#x440;&#x443;&#x44e; &#x43f;&#x43e;&#x43f;&#x430;&#x434;&#x435;&#x442; &#x43d;&#x43e;&#x432;&#x44b;&#x439; &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442;. &#x415;&#x441;&#x43b;&#x438; &#x43a;&#x43e;&#x440;&#x437;&#x438;&#x43d;&#x430; &#x43f;&#x443;&#x441;&#x442;&#x430;&#x44f;, &#x442;&#x43e; &#x432; &#x43d;&#x435;&#x435; &#x441;&#x43e;&#x445;&#x440;&#x430;&#x43d;&#x44f;&#x435;&#x442;&#x441;&#x44f; &#x441;&#x441;&#x44b;&#x43b;&#x43a;&#x430; &#x43d;&#x430; &#x432;&#x43d;&#x43e;&#x432;&#x44c; &#x434;&#x43e;&#x431;&#x430;&#x432;&#x43b;&#x44f;&#x435;&#x43c;&#x44b;&#x439; &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442;, &#x435;&#x441;&#x43b;&#x438; &#x436;&#x435; &#x442;&#x430;&#x43c; &#x443;&#x436;&#x435; &#x435;&#x441;&#x442;&#x44c; &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442;, &#x442;&#x43e; &#x43f;&#x440;&#x43e;&#x438;&#x441;&#x445;&#x43e;&#x434;&#x438;&#x442; &#x43f;&#x43e;&#x441;&#x43b;&#x435;&#x434;&#x43e;&#x432;&#x430;&#x442;&#x435;&#x43b;&#x44c;&#x43d;&#x44b;&#x439; &#x43f;&#x435;&#x440;&#x435;&#x445;&#x43e;&#x434; &#x43f;&#x43e; &#x441;&#x441;&#x44b;&#x43b;&#x43a;&#x430;&#x43c; &#x43c;&#x435;&#x436;&#x434;&#x443; &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442;&#x430;&#x43c;&#x438; &#x432; &#x446;&#x435;&#x43f;&#x43e;&#x447;&#x43a;&#x435;, &#x432; &#x43f;&#x43e;&#x438;&#x441;&#x43a;&#x430;&#x445; &#x43f;&#x43e;&#x441;&#x43b;&#x435;&#x434;&#x43d;&#x435;&#x433;&#x43e; &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442;&#x430;, &#x43e;&#x442; &#x43a;&#x43e;&#x442;&#x43e;&#x440;&#x43e;&#x433;&#x43e; &#x438; &#x441;&#x442;&#x430;&#x432;&#x438;&#x442;&#x441;&#x44f; &#x441;&#x441;&#x44b;&#x43b;&#x43a;&#x430; &#x43d;&#x430; &#x432;&#x43d;&#x43e;&#x432;&#x44c; &#x434;&#x43e;&#x431;&#x430;&#x432;&#x43b;&#x435;&#x43d;&#x43d;&#x44b;&#x439; &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442;. &#x415;&#x441;&#x43b;&#x438; &#x432; &#x441;&#x43f;&#x438;&#x441;&#x43a;&#x435; &#x431;&#x44b;&#x43b; &#x43d;&#x430;&#x439;&#x434;&#x435;&#x43d; &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442; &#x441; &#x442;&#x430;&#x43a;&#x438;&#x43c; &#x436;&#x435; &#x43a;&#x43b;&#x44e;&#x447;&#x43e;&#x43c;, &#x442;&#x43e; &#x43e;&#x43d; &#x437;&#x430;&#x43c;&#x435;&#x43d;&#x44f;&#x435;&#x442;&#x441;&#x44f;" STYLE_REF="main_definition" ID="ID_477871311" CREATED="1435119563358" MODIFIED="1436346351675"/>
<node TEXT="&#x41f;&#x440;&#x435;&#x438;&#x43c;&#x443;&#x449;&#x435;&#x441;&#x442;&#x432;&#x430;" STYLE_REF="yellow_node" ID="ID_611255817" CREATED="1435119568285" MODIFIED="1436346390186">
<icon BUILTIN="addition"/>
<node TEXT="&#x414;&#x43e;&#x431;&#x430;&#x432;&#x43b;&#x435;&#x43d;&#x438;&#x435;, &#x43f;&#x43e;&#x438;&#x441;&#x43a; &#x438; &#x443;&#x434;&#x430;&#x43b;&#x435;&#x43d;&#x438;&#x435; &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442;&#x43e;&#x432; &#x432;&#x44b;&#x43f;&#x43e;&#x43b;&#x43d;&#x44f;&#x435;&#x442;&#x441;&#x44f; &#x437;&#x430; &#x43a;&#x43e;&#x43d;&#x441;&#x442;&#x430;&#x43d;&#x442;&#x43d;&#x43e;&#x435; &#x432;&#x440;&#x435;&#x43c;&#x44f;" STYLE_REF="pre_last_node" ID="ID_1594573233" CREATED="1435119576671" MODIFIED="1436346390186"/>
</node>
<node TEXT="&#x41d;&#x435;&#x434;&#x43e;&#x441;&#x442;&#x430;&#x442;&#x43a;&#x438;" STYLE_REF="yellow_node" ID="ID_1305556668" CREATED="1435119620502" MODIFIED="1435120899441">
<icon BUILTIN="addition"/>
<node TEXT="&#x425;&#x435;&#x448;-&#x444;&#x443;&#x43d;&#x43a;&#x446;&#x438;&#x439; &#x434;&#x43e;&#x43b;&#x436;&#x43d;&#x430; &#x440;&#x430;&#x432;&#x43d;&#x43e;&#x43c;&#x435;&#x440;&#x43d;&#x43e; &#x440;&#x430;&#x441;&#x43f;&#x440;&#x435;&#x434;&#x435;&#x43b;&#x44f;&#x442;&#x44c; &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442;&#x44b; &#x43f;&#x43e; &#x43a;&#x43e;&#x440;&#x437;&#x438;&#x43d;&#x430;&#x43c;, &#x432; &#x44d;&#x442;&#x43e;&#x43c; &#x441;&#x43b;&#x443;&#x447;&#x430;&#x435; &#x432;&#x440;&#x435;&#x43c;&#x435;&#x43d;&#x43d;&#x430;&#x44f; &#x441;&#x43b;&#x43e;&#x436;&#x43d;&#x43e;&#x441;&#x442;&#x44c; &#x434;&#x43b;&#x44f; &#x44d;&#x442;&#x438;&#x445; 3 &#x43e;&#x43f;&#x435;&#x440;&#x430;&#x446;&#x438;&#x439; &#x431;&#x443;&#x434;&#x435;&#x442; &#x43d;&#x435; &#x43d;&#x438;&#x436;&#x435; lg N, &#x430; &#x432; &#x441;&#x440;&#x435;&#x434;&#x43d;&#x435;&#x43c; &#x441;&#x43b;&#x443;&#x447;&#x430;&#x435; &#x43a;&#x430;&#x43a; &#x440;&#x430;&#x437; &#x43a;&#x43e;&#x43d;&#x441;&#x442;&#x430;&#x43d;&#x442;&#x43d;&#x43e;&#x435; &#x432;&#x440;&#x435;&#x43c;&#x44f;" STYLE_REF="pre_last_node" ID="ID_138062962" CREATED="1435119628728" MODIFIED="1435120887098"/>
</node>
<node TEXT="&#x41e;&#x441;&#x43e;&#x431;&#x435;&#x43d;&#x43d;&#x43e;&#x441;&#x442;&#x438;" STYLE_REF="yellow_node" ID="ID_1338382890" CREATED="1435119654302" MODIFIED="1436346527469">
<node TEXT="&#x41d;&#x430;&#x447;&#x430;&#x43b;&#x44c;&#x43d;&#x43e;&#x435; &#x43a;&#x43e;&#x43b;&#x438;&#x447;&#x435;&#x441;&#x442;&#x432;&#x43e; &#x43a;&#x43e;&#x440;&#x437;&#x438;&#x43d; = 16.&#xa;&#x41c;&#x43e;&#x436;&#x43d;&#x43e; &#x438;&#x441;&#x43f;&#x43e;&#x43b;&#x44c;&#x437;&#x443;&#x44f; &#x43a;&#x43e;&#x43d;&#x441;&#x442;&#x440;&#x443;&#x43a;&#x442;&#x43e;&#x440;&#x44b; &#x441; &#x43f;&#x430;&#x440;&#x430;&#x43c;&#x435;&#x442;&#x440;&#x430;&#x43c;&#x438;: &#x447;&#x435;&#x440;&#x435;&#x437; &#x43f;&#x430;&#x440;&#x430;&#x43c;&#x435;&#x442;&#x440; capacity &#x437;&#x430;&#x434;&#x430;&#x432;&#x430;&#x442;&#x44c; &#x441;&#x432;&#x43e;&#x435; &#x43d;&#x430;&#x447;&#x430;&#x43b;&#x44c;&#x43d;&#x43e;&#x435; &#x43a;&#x43e;&#x43b;&#x438;&#x447;&#x435;&#x441;&#x442;&#x432;&#x43e; &#x43a;&#x43e;&#x440;&#x437;&#x438;&#x43d;" STYLE_REF="pre_last_node" ID="ID_803225310" CREATED="1435119661247" MODIFIED="1435120887101"/>
<node TEXT="&#x415;&#x441;&#x43b;&#x438; &#x432;&#x44b; &#x432;&#x43e;&#x437;&#x44c;&#x43c;&#x435;&#x442;&#x435; &#x445;&#x435;&#x448;-&#x444;&#x443;&#x43d;&#x43a;&#x446;&#x438;&#x44e;, &#x43a;&#x43e;&#x442;&#x43e;&#x440;&#x430;&#x44f; &#x43f;&#x43e;&#x441;&#x442;&#x43e;&#x44f;&#x43d;&#x43d;&#x43e; &#x431;&#x443;&#x434;&#x435;&#x442; &#x432;&#x43e;&#x437;&#x432;&#x440;&#x430;&#x449;&#x430;&#x442;&#x44c; &#x43e;&#x434;&#x43d;&#x43e; &#x438; &#x442;&#x43e; &#x436;&#x435; &#x437;&#x43d;&#x430;&#x447;&#x435;&#x43d;&#x438;&#x435;, &#x442;&#x43e; HashMap &#x43f;&#x440;&#x435;&#x432;&#x440;&#x430;&#x442;&#x438;&#x442;&#x441;&#x44f; &#x432; &#x441;&#x432;&#x44f;&#x437;&#x43d;&#x44b;&#x439; &#x441;&#x43f;&#x438;&#x441;&#x43e;&#x43a;, &#x441; &#x43e;&#x442;&#x432;&#x440;&#x430;&#x442;&#x43d;&#x43e;&#x439; &#x43f;&#x440;&#x43e;&#x438;&#x437;&#x432;&#x43e;&#x434;&#x438;&#x442;&#x435;&#x43b;&#x44c;&#x43d;&#x43e;&#x441;&#x442;&#x44c;&#x44e;" STYLE_REF="pre_last_node" ID="ID_453238317" CREATED="1435119790741" MODIFIED="1435120887112"/>
<node TEXT="&#x415;&#x441;&#x43b;&#x438; &#x432;&#x44b; &#x431;&#x443;&#x434;&#x435;&#x442;&#x435; &#x438;&#x441;&#x43f;&#x43e;&#x43b;&#x44c;&#x437;&#x43e;&#x432;&#x430;&#x442;&#x44c; &#x445;&#x435;&#x448;-&#x444;&#x443;&#x43d;&#x43a;&#x446;&#x438;&#x44e; &#x441; &#x440;&#x430;&#x432;&#x43d;&#x43e;&#x43c;&#x435;&#x440;&#x43d;&#x44b;&#x43c; &#x440;&#x430;&#x441;&#x43f;&#x440;&#x435;&#x434;&#x435;&#x43b;&#x435;&#x43d;&#x438;&#x435;&#x43c;, &#x432; &#x43f;&#x440;&#x435;&#x434;&#x435;&#x43b;&#x44c;&#x43d;&#x43e;&#x43c; &#x441;&#x43b;&#x443;&#x447;&#x430;&#x435; &#x433;&#x430;&#x440;&#x430;&#x43d;&#x442;&#x438;&#x440;&#x43e;&#x432;&#x430;&#x442;&#x44c;&#x441;&#x44f; &#x431;&#x443;&#x434;&#x435;&#x442; &#x442;&#x43e;&#x43b;&#x44c;&#x43a;&#x43e; &#x432;&#x440;&#x435;&#x43c;&#x435;&#x43d;&#x43d;&#x430;&#x44f; &#x441;&#x43b;&#x43e;&#x436;&#x43d;&#x43e;&#x441;&#x442;&#x44c; lg N" STYLE_REF="pre_last_node" ID="ID_433375026" CREATED="1435119810846" MODIFIED="1436346527469"/>
<node TEXT="hashCode &#x43f;&#x43e;&#x437;&#x432;&#x43e;&#x43b;&#x44f;&#x435;&#x442; &#x43e;&#x43f;&#x440;&#x435;&#x434;&#x435;&#x43b;&#x438;&#x442;&#x44c; &#x43a;&#x43e;&#x440;&#x437;&#x438;&#x43d;&#x443; &#x434;&#x43b;&#x44f; &#x43f;&#x43e;&#x438;&#x441;&#x43a;&#x430; &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442;&#x430;, &#x430; equals &#x438;&#x441;&#x43f;&#x43e;&#x43b;&#x44c;&#x437;&#x443;&#x435;&#x442;&#x441;&#x44f; &#x434;&#x43b;&#x44f; &#x441;&#x440;&#x430;&#x432;&#x43d;&#x435;&#x43d;&#x438;&#x44f; &#x43a;&#x43b;&#x44e;&#x447;&#x435;&#x439; &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442;&#x43e;&#x432; &#x432; &#x441;&#x43f;&#x438;&#x441;&#x43a;&#x435; &#x432;&#x43d;&#x443;&#x442;&#x440;&#x438; &#x43a;&#x43e;&#x440;&#x437;&#x438;&#x43d;&#x44b; &#x438; &#x438;&#x441;&#x43a;&#x43e;&#x43c;&#x43e;&#x433;&#x43e; &#x43a;&#x43b;&#x44e;&#x447;&#x430;" STYLE_REF="pre_last_node" ID="ID_1642576334" CREATED="1435119835104" MODIFIED="1435120887131"/>
<node TEXT="&#x41a;&#x430;&#x43a; &#x438; &#x43a;&#x43e;&#x433;&#x434;&#x430; &#x43f;&#x440;&#x43e;&#x438;&#x441;&#x445;&#x43e;&#x434;&#x438;&#x442; &#x443;&#x432;&#x435;&#x43b;&#x438;&#x447;&#x435;&#x43d;&#x438;&#x435; &#x43a;&#x43e;&#x43b;&#x438;&#x447;&#x435;&#x441;&#x442;&#x432;&#x430; &#x43a;&#x43e;&#x440;&#x437;&#x438;&#x43d; &#x432; HashMap?&#xa;HashMap &#x435;&#x441;&#x442;&#x44c; &#x43f;&#x430;&#x440;&#x430;&#x43c;&#x435;&#x442;&#x440; loadFactor, &#x43d;&#x430; &#x43e;&#x441;&#x43d;&#x43e;&#x432;&#x430;&#x43d;&#x438;&#x438; &#x43a;&#x43e;&#x442;&#x43e;&#x440;&#x43e;&#x433;&#x43e;, &#x432;&#x44b;&#x447;&#x438;&#x441;&#x43b;&#x44f;&#x435;&#x442;&#x441;&#x44f; &#x43f;&#x440;&#x435;&#x434;&#x435;&#x43b;&#x44c;&#x43d;&#x43e;&#x435; &#x43a;&#x43e;&#x43b;&#x438;&#x447;&#x435;&#x441;&#x442;&#x432;&#x43e; &#x437;&#x430;&#x43d;&#x44f;&#x442;&#x44b;&#x445; &#x43a;&#x43e;&#x440;&#x437;&#x438;&#x43d; (capacity*loadFactor). &#x41f;&#x43e; &#x443;&#x43c;&#x43e;&#x43b;&#x447;&#x430;&#x43d;&#x438;&#x44e; loadFactor = 0,75. &#x41f;&#x43e; &#x434;&#x43e;&#x441;&#x442;&#x438;&#x436;&#x435;&#x43d;&#x438;&#x44e; &#x43f;&#x440;&#x435;&#x434;&#x435;&#x43b;&#x44c;&#x43d;&#x43e;&#x433;&#x43e; &#x437;&#x43d;&#x430;&#x447;&#x435;&#x43d;&#x438;&#x44f;, &#x447;&#x438;&#x441;&#x43b;&#x43e; &#x43a;&#x43e;&#x440;&#x437;&#x438;&#x43d; &#x443;&#x432;&#x435;&#x43b;&#x438;&#x447;&#x438;&#x432;&#x430;&#x435;&#x442;&#x441;&#x44f; &#x432; 2 &#x440;&#x430;&#x437;&#x430;. &#x414;&#x43b;&#x44f; &#x432;&#x441;&#x435;&#x445; &#x445;&#x440;&#x430;&#x43d;&#x438;&#x43c;&#x44b;&#x445; &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442;&#x43e;&#x432; &#x432;&#x44b;&#x447;&#x438;&#x441;&#x43b;&#x44f;&#x435;&#x442;&#x441;&#x44f; &#x43d;&#x43e;&#x432;&#x43e;&#x435; &#xab;&#x43c;&#x435;&#x441;&#x442;&#x43e;&#x43f;&#x43e;&#x43b;&#x43e;&#x436;&#x435;&#x43d;&#x438;&#x435;&#xbb; &#x441; &#x443;&#x447;&#x435;&#x442;&#x43e;&#x43c; &#x43d;&#x43e;&#x432;&#x43e;&#x433;&#x43e; &#x447;&#x438;&#x441;&#x43b;&#x430; &#x43a;&#x43e;&#x440;&#x437;&#x438;&#x43d;" STYLE_REF="pre_last_node" ID="ID_898606074" CREATED="1435120609453" MODIFIED="1435120887136"/>
</node>
<node TEXT="&#x418;&#x441;&#x43f;&#x43e;&#x43b;&#x44c;&#x437;&#x43e;&#x432;&#x430;&#x43d;&#x438;&#x435;" STYLE_REF="yellow_node" ID="ID_1674195667" CREATED="1435305956022" MODIFIED="1435314448037">
<node TEXT="&#x421;&#x43e;&#x437;&#x434;&#x430;&#x43d;&#x438;&#x435;" STYLE_REF="gray_node" ID="ID_653958104" CREATED="1435305961383" MODIFIED="1435307064482">
<font BOLD="false"/>
<node TEXT="Map&lt;String, String&gt; hashmap = new HashMap&lt;String, String&gt;();" STYLE_REF="pre_last_node" ID="ID_1084725505" CREATED="1435305974233" MODIFIED="1435307087982">
<hook URI="hashmap-1.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
</node>
<node TEXT="&#x414;&#x43e;&#x431;&#x430;&#x432;&#x43b;&#x435;&#x43d;&#x438;&#x435; &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442;&#x43e;&#x432;" STYLE_REF="gray_node" ID="ID_1781583577" CREATED="1435306254335" MODIFIED="1435307064483">
<font BOLD="false"/>
<node TEXT="hashmap.put(&quot;0&quot;, &quot;zero&quot;);" STYLE_REF="example_node" ID="ID_966616816" CREATED="1435306260455" MODIFIED="1435307073444">
<node TEXT="&#x421;&#x43d;&#x430;&#x447;&#x430;&#x43b;&#x430; &#x43a;&#x43b;&#x44e;&#x447; &#x43f;&#x440;&#x43e;&#x432;&#x435;&#x440;&#x44f;&#x435;&#x442;&#x441;&#x44f; &#x43d;&#x430; &#x440;&#x430;&#x432;&#x435;&#x43d;&#x441;&#x442;&#x432;&#x43e; null. &#x415;&#x441;&#x43b;&#x438; &#x44d;&#x442;&#x43e; &#x43f;&#x440;&#x43e;&#x432;&#x435;&#x440;&#x43a;&#x430; &#x432;&#x435;&#x440;&#x43d;&#x443;&#x43b;&#x430; true, &#x431;&#x443;&#x434;&#x435;&#x442; &#x432;&#x44b;&#x437;&#x432;&#x430;&#x43d; &#x43c;&#x435;&#x442;&#x43e;&#x434; putForNullKey(value)" STYLE_REF="pre_last_node" ID="ID_498869982" CREATED="1435306289606" MODIFIED="1435307087978"/>
<node TEXT="&#x414;&#x430;&#x43b;&#x435;&#x435; &#x433;&#x435;&#x43d;&#x435;&#x440;&#x438;&#x440;&#x443;&#x435;&#x442;&#x441;&#x44f; &#x445;&#x44d;&#x448; &#x43d;&#x430; &#x43e;&#x441;&#x43d;&#x43e;&#x432;&#x435; &#x43a;&#x43b;&#x44e;&#x447;&#x430;. &#x414;&#x43b;&#x44f; &#x433;&#x435;&#x43d;&#x435;&#x440;&#x430;&#x446;&#x438;&#x438; &#x438;&#x441;&#x43f;&#x43e;&#x43b;&#x44c;&#x437;&#x443;&#x435;&#x442;&#x441;&#x44f; &#x43c;&#x435;&#x442;&#x43e;&#x434; hash(hashCode)" STYLE_REF="pre_last_node" ID="ID_1474260330" CREATED="1435306305558" MODIFIED="1435307087975">
<node TEXT="static int hash(int h) {&#xa;    h ^= (h &gt;&gt;&gt; 20) ^ (h &gt;&gt;&gt; 12);&#xa;    return h ^ (h &gt;&gt;&gt; 7) ^ (h &gt;&gt;&gt; 4);&#xa;}" STYLE_REF="example_node" ID="ID_828827418" CREATED="1435306311942" MODIFIED="1435307101195"/>
</node>
<node TEXT="&#x421; &#x43f;&#x43e;&#x43c;&#x43e;&#x449;&#x44c;&#x44e; &#x43c;&#x435;&#x442;&#x43e;&#x434;&#x430; indexFor(hash, tableLength), &#x43e;&#x43f;&#x440;&#x435;&#x434;&#x435;&#x43b;&#x44f;&#x435;&#x442;&#x441;&#x44f; &#x43f;&#x43e;&#x437;&#x438;&#x446;&#x438;&#x44f; &#x432; &#x43c;&#x430;&#x441;&#x441;&#x438;&#x432;&#x435;, &#x43a;&#x443;&#x434;&#x430; &#x431;&#x443;&#x434;&#x435;&#x442; &#x43f;&#x43e;&#x43c;&#x435;&#x449;&#x435;&#x43d; &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442;" STYLE_REF="pre_last_node" ID="ID_702192517" CREATED="1435306596599" MODIFIED="1435307087973">
<node TEXT="static int indexFor(int h, int length) {&#xa;    return h &amp; (length - 1);&#xa;}" STYLE_REF="example_node" ID="ID_1597302367" CREATED="1435306603606" MODIFIED="1435307101198"/>
</node>
<node TEXT="&#x422;&#x435;&#x43f;&#x435;&#x440;&#x44c;, &#x437;&#x43d;&#x430;&#x44f; &#x438;&#x43d;&#x434;&#x435;&#x43a;&#x441; &#x432; &#x43c;&#x430;&#x441;&#x441;&#x438;&#x432;&#x435;, &#x43c;&#x44b; &#x43f;&#x43e;&#x43b;&#x443;&#x447;&#x430;&#x435;&#x43c; &#x441;&#x43f;&#x438;&#x441;&#x43e;&#x43a; (&#x446;&#x435;&#x43f;&#x43e;&#x447;&#x43a;&#x443;) &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442;&#x43e;&#x432;, &#x43f;&#x440;&#x438;&#x432;&#x44f;&#x437;&#x430;&#x43d;&#x43d;&#x44b;&#x445; &#x43a; &#x44d;&#x442;&#x43e;&#x439; &#x44f;&#x447;&#x435;&#x439;&#x43a;&#x435;. &#x425;&#x44d;&#x448; &#x438; &#x43a;&#x43b;&#x44e;&#x447; &#x43d;&#x43e;&#x432;&#x43e;&#x433;&#x43e; &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442;&#x430; &#x43f;&#x43e;&#x43e;&#x447;&#x435;&#x440;&#x435;&#x434;&#x43d;&#x43e; &#x441;&#x440;&#x430;&#x432;&#x43d;&#x438;&#x432;&#x430;&#x44e;&#x442;&#x441;&#x44f; &#x441; &#x445;&#x44d;&#x448;&#x430;&#x43c;&#x438; &#x438; &#x43a;&#x43b;&#x44e;&#x447;&#x430;&#x43c;&#x438; &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442;&#x43e;&#x432; &#x438;&#x437; &#x441;&#x43f;&#x438;&#x441;&#x43a;&#x430; &#x438;, &#x43f;&#x440;&#x438; &#x441;&#x43e;&#x432;&#x43f;&#x430;&#x434;&#x435;&#x43d;&#x438;&#x438; &#x44d;&#x442;&#x438;&#x445; &#x43f;&#x430;&#x440;&#x430;&#x43c;&#x435;&#x442;&#x440;&#x43e;&#x432;, &#x437;&#x43d;&#x430;&#x447;&#x435;&#x43d;&#x438;&#x435; &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442;&#x430; &#x43f;&#x435;&#x440;&#x435;&#x437;&#x430;&#x43f;&#x438;&#x441;&#x44b;&#x432;&#x430;&#x435;&#x442;&#x441;&#x44f;" STYLE_REF="pre_last_node" ID="ID_1762452843" CREATED="1435306668638" MODIFIED="1435307087971">
<node TEXT="if (e.hash == hash &amp;&amp; (e.key == key || key.equals(e.key))) {&#xa;    V oldValue = e.value;&#xa;    e.value = value;&#xa;               &#xa;    return oldValue;&#xa;}" STYLE_REF="example_node" ID="ID_1863089272" CREATED="1435306678278" MODIFIED="1435307101199"/>
</node>
<node TEXT="&#x415;&#x441;&#x43b;&#x438; &#x436;&#x435; &#x43f;&#x440;&#x435;&#x434;&#x44b;&#x434;&#x443;&#x449;&#x438;&#x439; &#x448;&#x430;&#x433; &#x43d;&#x435; &#x432;&#x44b;&#x44f;&#x432;&#x438;&#x43b; &#x441;&#x43e;&#x432;&#x43f;&#x430;&#x434;&#x435;&#x43d;&#x438;&#x439;, &#x431;&#x443;&#x434;&#x435;&#x442; &#x432;&#x44b;&#x437;&#x432;&#x430;&#x43d; &#x43c;&#x435;&#x442;&#x43e;&#x434; addEntry(hash, key, value, index) &#x434;&#x43b;&#x44f; &#x434;&#x43e;&#x431;&#x430;&#x432;&#x43b;&#x435;&#x43d;&#x438;&#x44f; &#x43d;&#x43e;&#x432;&#x43e;&#x433;&#x43e; &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442;&#x430;" STYLE_REF="pre_last_node" ID="ID_1919035961" CREATED="1435306699374" MODIFIED="1435307087970">
<hook URI="hashmap-2.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
</node>
<node TEXT="hashmap.put(&quot;key&quot;, &quot;one&quot;);" STYLE_REF="example_node" ID="ID_732887559" CREATED="1435306756185" MODIFIED="1435307073445">
<node TEXT="&#x41f;&#x440;&#x43e;&#x43f;&#x443;&#x441;&#x43a;&#x430;&#x435;&#x442;&#x441;&#x44f;, &#x43a;&#x43b;&#x44e;&#x447; &#x43d;&#x435; &#x440;&#x430;&#x432;&#x435;&#x43d; null" STYLE_REF="pre_last_node" ID="ID_1621874809" CREATED="1435306771057" MODIFIED="1435307087969"/>
<node TEXT="&apos;&apos;key&apos;&apos;.hashCode() = 106079" STYLE_REF="pre_last_node" ID="ID_685143872" CREATED="1435306771057" MODIFIED="1435307087969"/>
<node TEXT="&#x41e;&#x43f;&#x440;&#x435;&#x434;&#x435;&#x43b;&#x435;&#x43d;&#x438;&#x435; &#x43f;&#x43e;&#x437;&#x438;&#x446;&#x438;&#x438; &#x432; &#x43c;&#x430;&#x441;&#x441;&#x438;&#x432;&#x435;" STYLE_REF="pre_last_node" ID="ID_234717453" CREATED="1435306771058" MODIFIED="1435307087968"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      h &amp; (length - 1) = 14
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&#x41f;&#x43e;&#x434;&#x43e;&#x431;&#x43d;&#x44b;&#x435; &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442;&#x44b; &#x43d;&#x435; &#x43d;&#x430;&#x439;&#x434;&#x435;&#x43d;&#x44b;" STYLE_REF="pre_last_node" ID="ID_668870800" CREATED="1435306771059" MODIFIED="1435307087968"/>
<node TEXT="&#x414;&#x43e;&#x431;&#x430;&#x432;&#x43b;&#x435;&#x43d;&#x438;&#x435; &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442;&#x430;" STYLE_REF="pre_last_node" ID="ID_287661750" CREATED="1435306804423" MODIFIED="1435307087968">
<hook URI="hashmap-3.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
</node>
<node TEXT="hashmap.put(null, null);" STYLE_REF="example_node" ID="ID_1680370618" CREATED="1435306858713" MODIFIED="1435307073445">
<node TEXT="&#x415;&#x441;&#x43b;&#x438; &#x43f;&#x440;&#x438; &#x434;&#x43e;&#x431;&#x430;&#x432;&#x43b;&#x435;&#x43d;&#x438;&#x438; &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442;&#x430; &#x432; &#x43a;&#x430;&#x447;&#x435;&#x441;&#x442;&#x432;&#x435; &#x43a;&#x43b;&#x44e;&#x447;&#x430; &#x431;&#x44b;&#x43b; &#x43f;&#x435;&#x440;&#x435;&#x434;&#x430;&#x43d; null, &#x434;&#x435;&#x439;&#x441;&#x442;&#x432;&#x438;&#x44f; &#x431;&#x443;&#x434;&#x443;&#x442; &#x43e;&#x442;&#x43b;&#x438;&#x447;&#x430;&#x442;&#x44c;&#x441;&#x44f;. &#x411;&#x443;&#x434;&#x435;&#x442; &#x432;&#x44b;&#x437;&#x432;&#x430;&#x43d; &#x43c;&#x435;&#x442;&#x43e;&#x434; putForNullKey(value), &#x432;&#x43d;&#x443;&#x442;&#x440;&#x438; &#x43a;&#x43e;&#x442;&#x43e;&#x440;&#x43e;&#x433;&#x43e; &#x43d;&#x435;&#x442; &#x432;&#x44b;&#x437;&#x43e;&#x432;&#x430; &#x43c;&#x435;&#x442;&#x43e;&#x434;&#x43e;&#x432; hash() &#x438; indexFor() (&#x43f;&#x43e;&#x442;&#x43e;&#x43c;&#x443; &#x43a;&#x430;&#x43a; &#x432;&#x441;&#x435; &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442;&#x44b; &#x441; null-&#x43a;&#x43b;&#x44e;&#x447;&#x430;&#x43c;&#x438; &#x432;&#x441;&#x435;&#x433;&#x434;&#x430; &#x43f;&#x43e;&#x43c;&#x435;&#x449;&#x430;&#x44e;&#x442;&#x441;&#x44f; &#x432; table[0])" STYLE_REF="pre_last_node" ID="ID_1962604835" CREATED="1435306873289" MODIFIED="1435307087966">
<hook URI="hashmap-4.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
</node>
<node TEXT="hashmap.put(&quot;idx&quot;, &quot;two&quot;);" STYLE_REF="example_node" ID="ID_271668026" CREATED="1435306902270" MODIFIED="1435307073445"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1050;&#1086;&#1083;&#1080;&#1079;&#1080;&#1103;
    </p>
  </body>
</html>
</richcontent>
<node TEXT="// &#x412; table[3] &#x443;&#x436;&#x435; &#x445;&#x440;&#x430;&#x43d;&#x438;&#x442;&#x441;&#x44f; &#x446;&#x435;&#x43f;&#x43e;&#x447;&#x43a;&#x430; &#x441;&#x43e;&#x441;&#x442;&#x43e;&#x44f;&#x449;&#x430;&#x44f; &#x438;&#x437; &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442;&#x430; [&quot;0&quot;, &quot;zero&quot;]&#xa;Entry&lt;K, V&gt; e = table[index];&#xa;&#xa;// &#x41d;&#x43e;&#x432;&#x44b;&#x439; &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442; &#x434;&#x43e;&#x431;&#x430;&#x432;&#x43b;&#x44f;&#x435;&#x442;&#x441;&#x44f; &#x432; &#x43d;&#x430;&#x447;&#x430;&#x43b;&#x43e; &#x446;&#x435;&#x43f;&#x43e;&#x447;&#x43a;&#x438;&#xa;table[index] = new Entry&lt;K, V&gt;(hash, key, value, e);" STYLE_REF="pre_last_node" ID="ID_588783497" CREATED="1435306976141" MODIFIED="1435307087964">
<hook URI="hashmap-5.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
</node>
</node>
<node TEXT="Resize &#x438; Transfer" STYLE_REF="gray_node" ID="ID_1879601570" CREATED="1435307034406" MODIFIED="1435314448037">
<font BOLD="false"/>
<node TEXT="&#x41a;&#x43e;&#x433;&#x434;&#x430; &#x43c;&#x430;&#x441;&#x441;&#x438;&#x432; table[] &#x437;&#x430;&#x43f;&#x43e;&#x43b;&#x43d;&#x44f;&#x435;&#x442;&#x441;&#x44f; &#x434;&#x43e; &#x43f;&#x440;&#x435;&#x434;&#x435;&#x43b;&#x44c;&#x43d;&#x43e;&#x433;&#x43e; &#x437;&#x43d;&#x430;&#x447;&#x435;&#x43d;&#x438;&#x44f;, &#x435;&#x433;&#x43e; &#x440;&#x430;&#x437;&#x43c;&#x435;&#x440; &#x443;&#x432;&#x435;&#x43b;&#x438;&#x447;&#x438;&#x432;&#x430;&#x435;&#x442;&#x441;&#x44f; &#x432;&#x434;&#x432;&#x43e;&#x435; &#x438; &#x43f;&#x440;&#x43e;&#x438;&#x441;&#x445;&#x43e;&#x434;&#x438;&#x442; &#x43f;&#x435;&#x440;&#x435;&#x440;&#x430;&#x441;&#x43f;&#x440;&#x435;&#x434;&#x435;&#x43b;&#x435;&#x43d;&#x438;&#x435; &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442;&#x43e;&#x432;" STYLE_REF="pre_last_node" ID="ID_375741949" CREATED="1435307185750" MODIFIED="1436432304177">
<node TEXT="void resize(int newCapacity) {&#xa;    if (table.length == MAXIMUM_CAPACITY) {&#xa;        threshold = Integer.MAX_VALUE;&#xa;        return;&#xa;    }&#xa;&#xa;    Entry[] newTable = new Entry[newCapacity];&#xa;    transfer(newTable);&#xa;    table = newTable;&#xa;    threshold = (int)(newCapacity * loadFactor);&#xa;}" STYLE_REF="example_node" ID="ID_207572196" CREATED="1435307244462" MODIFIED="1436432304176"/>
</node>
<node TEXT="&#x41c;&#x435;&#x442;&#x43e;&#x434; transfer() &#x43f;&#x435;&#x440;&#x435;&#x431;&#x438;&#x440;&#x430;&#x435;&#x442; &#x432;&#x441;&#x435; &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442;&#x44b; &#x442;&#x435;&#x43a;&#x443;&#x449;&#x435;&#x433;&#x43e; &#x445;&#x440;&#x430;&#x43d;&#x438;&#x43b;&#x438;&#x449;&#x430;, &#x43f;&#x435;&#x440;&#x435;&#x441;&#x447;&#x438;&#x442;&#x44b;&#x432;&#x430;&#x435;&#x442; &#x438;&#x445; &#x438;&#x43d;&#x434;&#x435;&#x43a;&#x441;&#x44b; (&#x441; &#x443;&#x447;&#x435;&#x442;&#x43e;&#x43c; &#x43d;&#x43e;&#x432;&#x43e;&#x433;&#x43e; &#x440;&#x430;&#x437;&#x43c;&#x435;&#x440;&#x430;) &#x438; &#x43f;&#x435;&#x440;&#x435;&#x440;&#x430;&#x441;&#x43f;&#x440;&#x435;&#x434;&#x435;&#x43b;&#x44f;&#x435;&#x442; &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442;&#x44b; &#x43f;&#x43e; &#x43d;&#x43e;&#x432;&#x43e;&#x43c;&#x443; &#x43c;&#x430;&#x441;&#x441;&#x438;&#x432;&#x443;." STYLE_REF="pre_last_node" ID="ID_610225119" CREATED="1435307261829" MODIFIED="1435314202568"/>
<node TEXT="&#x415;&#x441;&#x43b;&#x438; &#x432; &#x438;&#x441;&#x445;&#x43e;&#x434;&#x43d;&#x44b;&#x439; hashmap &#x434;&#x43e;&#x431;&#x430;&#x432;&#x438;&#x442;&#x44c;, &#x441;&#x43a;&#x430;&#x436;&#x435;&#x43c;, &#x435;&#x449;&#x435; 15 &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442;&#x43e;&#x432;, &#x442;&#x43e; &#x432; &#x440;&#x435;&#x437;&#x443;&#x43b;&#x44c;&#x442;&#x430;&#x442;&#x435; &#x440;&#x430;&#x437;&#x43c;&#x435;&#x440; &#x431;&#x443;&#x434;&#x435;&#x442; &#x443;&#x432;&#x435;&#x43b;&#x438;&#x447;&#x435;&#x43d; &#x438; &#x440;&#x430;&#x441;&#x43f;&#x440;&#x435;&#x434;&#x435;&#x43b;&#x435;&#x43d;&#x438;&#x435; &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442;&#x43e;&#x432; &#x438;&#x437;&#x43c;&#x435;&#x43d;&#x438;&#x442;&#x441;&#x44f;" STYLE_REF="pre_last_node" ID="ID_1131465157" CREATED="1435307271351" MODIFIED="1435314202567">
<hook URI="hashmap-6.png" SIZE="0.9756098" NAME="ExternalObject"/>
</node>
</node>
<node TEXT="&#x423;&#x434;&#x430;&#x43b;&#x435;&#x43d;&#x438;&#x435; &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442;&#x43e;&#x432;" STYLE_REF="gray_node" ID="ID_1068947794" CREATED="1435306756543" MODIFIED="1435307064483">
<font BOLD="false"/>
<node TEXT="HashMap &#x442;&#x430;&#x43a; &#x436;&#x435; &#x43a;&#x430;&#x43a; &#x438; ArrayList &#x2014; &#x43f;&#x440;&#x438; &#x443;&#x434;&#x430;&#x43b;&#x435;&#x43d;&#x438;&#x438; &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442;&#x43e;&#x432; &#x440;&#x430;&#x437;&#x43c;&#x435;&#x440; &#x43c;&#x430;&#x441;&#x441;&#x438;&#x432;&#x430; table[] &#x43d;&#x435; &#x443;&#x43c;&#x435;&#x43d;&#x44c;&#x448;&#x430;&#x435;&#x442;&#x441;&#x44f;. &#x418; &#x435;&#x441;&#x43b;&#x438; &#x432; ArrayList &#x43f;&#x440;&#x435;&#x434;&#x443;&#x441;&#x43c;&#x43e;&#x442;&#x440;&#x435;&#x43d; &#x43c;&#x435;&#x442;&#x43e;&#x434; trimToSize(), &#x442;&#x43e; &#x432; HashMap &#x442;&#x430;&#x43a;&#x438;&#x445; &#x43c;&#x435;&#x442;&#x43e;&#x434;&#x43e;&#x432; &#x43d;&#x435;&#x442;" STYLE_REF="pre_last_node" ID="ID_454029973" CREATED="1435314150111" MODIFIED="1435314202565"/>
</node>
</node>
<node TEXT="&#x418;&#x442;&#x435;&#x440;&#x430;&#x442;&#x43e;&#x440;&#x44b;" STYLE_REF="yellow_node" ID="ID_75276299" CREATED="1435314188088" MODIFIED="1436419905445">
<node TEXT="for (Map.Entry&lt;String, String&gt; entry: hashmap.entrySet())&#xa;    System.out.println(entry.getKey() + &quot; = &quot; + entry.getValue());" STYLE_REF="example_node" ID="ID_6480169" CREATED="1435314283944" MODIFIED="1435314381508"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1057;&#1087;&#1080;&#1089;&#1086;&#1082; &#1074;&#1089;&#1077;&#1093; &#1082;&#1083;&#1102;&#1095;&#1077;&#1081; keySet()
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="for (String key: hashmap.keySet())&#xa;    System.out.println(hashmap.get(key));" STYLE_REF="example_node" ID="ID_1820453541" CREATED="1435314291152" MODIFIED="1436419905445"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1042;&#1089;&#1077;&#1093; &#1079;&#1085;&#1072;&#1095;&#1077;&#1085;&#1080;&#1081; values()
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Iterator&lt;Map.Entry&lt;String, String&gt;&gt; itr = hashmap.entrySet().iterator();&#xa;while (itr.hasNext())&#xa;    System.out.println(itr.next());" STYLE_REF="example_node" ID="ID_953930260" CREATED="1435314303592" MODIFIED="1435314381513"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1042;&#1089;&#1077; &#1087;&#1072;&#1088;&#1099; &#1082;&#1083;&#1102;&#1095;/&#1079;&#1085;&#1072;&#1095;&#1077;&#1085;&#1080;&#1077; entrySet()
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="&#x418;&#x435;&#x440;&#x430;&#x440;&#x445;&#x438;&#x44f;" STYLE_REF="yellow_node" ID="ID_581076192" CREATED="1435151225089" MODIFIED="1435151256078">
<node TEXT="" STYLE_REF="pre_last_node" ID="ID_240792490" CREATED="1435151233634" MODIFIED="1435306237485">
<hook URI="hashmap-hierarchy.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
</node>
</node>
<node TEXT="LinkedHashMap" STYLE_REF="gray_node" ID="ID_581155863" CREATED="1435314684303" MODIFIED="1435314692044">
<node TEXT="&#x414;&#x430;&#x43d;&#x43d;&#x430;&#x44f; &#x441;&#x442;&#x440;&#x443;&#x43a;&#x442;&#x443;&#x440;&#x430; &#x44f;&#x432;&#x43b;&#x44f;&#x435;&#x442;&#x441;&#x44f; &#x441;&#x438;&#x43c;&#x431;&#x438;&#x43e;&#x437;&#x43e;&#x43c; &#x441;&#x432;&#x44f;&#x437;&#x430;&#x43d;&#x43d;&#x44b;&#x445; &#x441;&#x43f;&#x438;&#x441;&#x43a;&#x43e;&#x432; &#x438; &#x445;&#x44d;&#x448;-&#x43c;&#x430;&#x43f;&#x43e;&#x432;" STYLE_REF="pre_last_node" ID="ID_1533036250" CREATED="1435314730415" MODIFIED="1435320170459"/>
<node TEXT="&#x418;&#x441;&#x43f;&#x43e;&#x43b;&#x44c;&#x437;&#x43e;&#x432;&#x430;&#x43d;&#x438;&#x435;" STYLE_REF="yellow_node" ID="ID_178538916" CREATED="1435314749007" MODIFIED="1435320175616">
<node TEXT="&#x421;&#x43e;&#x437;&#x434;&#x430;&#x43d;&#x438;&#x435; &#x43e;&#x431;&#x44a;&#x435;&#x43a;&#x442;&#x430;" STYLE_REF="gray_node" ID="ID_959923913" CREATED="1435314760216" MODIFIED="1435320809822">
<font BOLD="false"/>
<node TEXT="Map&lt;Integer, String&gt; linkedHashMap = new LinkedHashMap&lt;Integer, String&gt;();" STYLE_REF="example_node" ID="ID_1691528435" CREATED="1435314753384" MODIFIED="1435315149839">
<hook URI="linked-hashmap-1.png" SIZE="0.75" NAME="ExternalObject"/>
<node TEXT="&#x423;&#x43d;&#x430;&#x441;&#x43b;&#x435;&#x434;&#x43e;&#x432;&#x430;&#x43d;&#x44b;&#x435; &#x43e;&#x442; HashMap (&#x442;&#x430;&#x43a;&#x438;&#x435; &#x43a;&#x430;&#x43a; table, loadFactor, threshold, size, entrySet &#x438; &#x442;.&#x43f;.)" STYLE_REF="pre_last_node" ID="ID_585852913" CREATED="1435314939775" MODIFIED="1435320170457"/>
<node TEXT="header &#x2014; &#xab;&#x433;&#x43e;&#x43b;&#x43e;&#x432;&#x430;&#xbb; &#x434;&#x432;&#x443;&#x441;&#x432;&#x44f;&#x437;&#x43d;&#x43e;&#x433;&#x43e; &#x441;&#x43f;&#x438;&#x441;&#x43a;&#x430;. &#x41f;&#x440;&#x438; &#x438;&#x43d;&#x438;&#x446;&#x438;&#x430;&#x43b;&#x438;&#x437;&#x430;&#x446;&#x438;&#x438; &#x443;&#x43a;&#x430;&#x437;&#x44b;&#x432;&#x430;&#x435;&#x442; &#x441;&#x430;&#x43c; &#x43d;&#x430; &#x441;&#x435;&#x431;&#x44f;" STYLE_REF="pre_last_node" ID="ID_1054451354" CREATED="1435314957008" MODIFIED="1435320170459"/>
<node TEXT="accessOrder &#x2014; &#x443;&#x43a;&#x430;&#x437;&#x44b;&#x432;&#x430;&#x435;&#x442; &#x43a;&#x430;&#x43a;&#x438;&#x43c; &#x43e;&#x431;&#x440;&#x430;&#x437;&#x43e;&#x43c; &#x431;&#x443;&#x434;&#x435;&#x442; &#x43e;&#x441;&#x443;&#x449;&#x435;&#x441;&#x442;&#x432;&#x43b;&#x44f;&#x442;&#x44c;&#x441;&#x44f; &#x434;&#x43e;&#x441;&#x442;&#x443;&#x43f; &#x43a; &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442;&#x430;&#x43c; &#x43f;&#x440;&#x438; &#x438;&#x441;&#x43f;&#x43e;&#x43b;&#x44c;&#x437;&#x43e;&#x432;&#x430;&#x43d;&#x438;&#x438; &#x438;&#x442;&#x435;&#x440;&#x430;&#x442;&#x43e;&#x440;&#x430;. &#x41f;&#x440;&#x438; &#x437;&#x43d;&#x430;&#x447;&#x435;&#x43d;&#x438;&#x438; true &#x2014; &#x43f;&#x43e; &#x43f;&#x43e;&#x440;&#x44f;&#x434;&#x43a;&#x443; &#x43f;&#x43e;&#x441;&#x43b;&#x435;&#x434;&#x43d;&#x435;&#x433;&#x43e; &#x434;&#x43e;&#x441;&#x442;&#x443;&#x43f;&#x430;" STYLE_REF="pre_last_node" ID="ID_162150080" CREATED="1435314967265" MODIFIED="1435320170519"/>
</node>
</node>
<node TEXT="&#x414;&#x43e;&#x431;&#x430;&#x432;&#x43b;&#x435;&#x43d;&#x438;&#x435; &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442;&#x43e;&#x432;" STYLE_REF="gray_node" ID="ID_656069329" CREATED="1435315122726" MODIFIED="1435320809823">
<font BOLD="false"/>
<node TEXT="linkedHashMap.put(1, &quot;obj1&quot;);" STYLE_REF="example_node" ID="ID_414423846" CREATED="1435320182515" MODIFIED="1435320749264">
<node TEXT="createEntry(hash, key, value, bucketIndex)" STYLE_REF="example_node" ID="ID_318009981" CREATED="1435320215476" MODIFIED="1435320749264">
<node TEXT="void createEntry(int hash, K key, V value, int bucketIndex) {&#xa;    HashMap.Entry&lt;K,V&gt; old = table[bucketIndex];&#xa;    Entry&lt;K,V&gt; e = new Entry&lt;K,V&gt;(hash, key, value, old);&#xa;    table[bucketIndex] = e;&#xa;    e.addBefore(header);&#xa;    size++;&#xa;}" STYLE_REF="example_node" ID="ID_1162228378" CREATED="1435320222708" MODIFIED="1435320351680"/>
<node TEXT="" STYLE_REF="pre_last_node" ID="ID_150381488" CREATED="1435320285754" MODIFIED="1435320343648">
<hook URI="linked-hashmap-2.png" SIZE="0.75" NAME="ExternalObject"/>
</node>
<node TEXT="&#x427;&#x435;&#x442;&#x432;&#x435;&#x440;&#x442;&#x430;&#x44f; &#x441;&#x442;&#x440;&#x43e;&#x43a;&#x430; &#x43f;&#x435;&#x440;&#x435;&#x43e;&#x43f;&#x440;&#x435;&#x434;&#x435;&#x43b;&#x44f;&#x435;&#x442; &#x441;&#x441;&#x44b;&#x43b;&#x43a;&#x438; &#x434;&#x432;&#x443;&#x441;&#x432;&#x44f;&#x437;&#x43d;&#x43e;&#x433;&#x43e; &#x441;&#x43f;&#x438;&#x441;&#x43a;&#x430;" STYLE_REF="pre_last_node" ID="ID_1968635872" CREATED="1435320287570" MODIFIED="1435320369967">
<hook URI="linked-hashmap-3.png" SIZE="0.75" NAME="ExternalObject"/>
</node>
</node>
</node>
<node TEXT="linkedHashMap.put(15, &quot;obj15&quot;);" STYLE_REF="example_node" ID="ID_1246702973" CREATED="1435320262835" MODIFIED="1435320749264">
<node TEXT="" STYLE_REF="pre_last_node" ID="ID_901813643" CREATED="1435320433531" MODIFIED="1435320768833">
<hook URI="linked-hashmap-4.png" SIZE="0.75" NAME="ExternalObject"/>
</node>
</node>
<node TEXT="linkedHashMap.put(4, &quot;obj4&quot;);" STYLE_REF="example_node" ID="ID_505853105" CREATED="1435320455091" MODIFIED="1435320749264">
<node TEXT="" STYLE_REF="pre_last_node" ID="ID_110165348" CREATED="1435320470810" MODIFIED="1435320768833">
<hook URI="linked-hashmap-5.png" SIZE="0.75" NAME="ExternalObject"/>
</node>
</node>
<node TEXT="linkedHashMap.put(38, &quot;obj38&quot;);" STYLE_REF="example_node" ID="ID_1416121631" CREATED="1435320573149" MODIFIED="1435320749264"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1050;&#1086;&#1083;&#1083;&#1080;&#1079;&#1080;&#1103;, &#1101;&#1083;&#1077;&#1084;&#1077;&#1085;&#1090;&#1099; &#1089; &#1082;&#1083;&#1102;&#1095;&#1072;&#1084;&#1080; 4 &#1080; 38 &#1086;&#1073;&#1088;&#1072;&#1079;&#1091;&#1102;&#1090; &#1094;&#1077;&#1087;&#1086;&#1095;&#1082;&#1091;
    </p>
  </body>
</html>
</richcontent>
<node TEXT="" STYLE_REF="pre_last_node" ID="ID_1718419776" CREATED="1435320616762" MODIFIED="1435320768833">
<hook URI="linked-hashmap-6.png" SIZE="0.75" NAME="ExternalObject"/>
</node>
</node>
<node TEXT="accessOrder == true" STYLE_REF="example_node" ID="ID_126429494" CREATED="1435320678706" MODIFIED="1435320749263"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1042; &#1090;&#1072;&#1082;&#1086;&#1081; &#1089;&#1080;&#1090;&#1091;&#1072;&#1094;&#1080;&#1080; &#1087;&#1086;&#1074;&#1077;&#1076;&#1077;&#1085;&#1080;&#1077; LinkedHashMap &#1084;&#1077;&#1085;&#1103;&#1077;&#1090;&#1089;&#1103; &#1080; &#1087;&#1088;&#1080; &#1074;&#1099;&#1079;&#1086;&#1074;&#1072;&#1093; &#1084;&#1077;&#1090;&#1086;&#1076;&#1086;&#1074; get() &#1080; put() &#1087;&#1086;&#1088;&#1103;&#1076;&#1086;&#1082; &#1101;&#1083;&#1077;&#1084;&#1077;&#1085;&#1090;&#1086;&#1074; &#1073;&#1091;&#1076;&#1077;&#1090; &#1080;&#1079;&#1084;&#1077;&#1085;&#1077;&#1085; &#8212; &#1101;&#1083;&#1077;&#1084;&#1077;&#1085;&#1090; &#1082; &#1082;&#1086;&#1090;&#1086;&#1088;&#1086;&#1084;&#1091; &#1086;&#1073;&#1088;&#1072;&#1097;&#1072;&#1077;&#1084;&#1089;&#1103; &#1073;&#1091;&#1076;&#1077;&#1090; &#1087;&#1086;&#1084;&#1077;&#1097;&#1077;&#1085; &#1074; &#1082;&#1086;&#1085;&#1077;&#1094;
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Map&lt;Integer, String&gt; linkedHashMap = new LinkedHashMap&lt;Integer, String&gt;(15, 0.75f, true) {{&#xa;    put(1, &quot;obj1&quot;);&#xa;    put(15, &quot;obj15&quot;);&#xa;    put(4, &quot;obj4&quot;);&#xa;    put(38, &quot;obj38&quot;);&#xa;}};&#xa;// {1=obj1, 15=obj15, 4=obj4, 38=obj38}&#xa;&#xa;linkedHashMap.get(1); // or linkedHashMap.put(1, &quot;Object1&quot;);&#xa;// {15=obj15, 4=obj4, 38=obj38, 1=obj1}" STYLE_REF="example_node" ID="ID_300277213" CREATED="1435320713619" MODIFIED="1435320754200"/>
<node TEXT="" STYLE_REF="pre_last_node" ID="ID_1195907280" CREATED="1435320716129" MODIFIED="1435320768832">
<hook URI="linked-hashmap-7.png" SIZE="0.75" NAME="ExternalObject"/>
</node>
</node>
</node>
</node>
<node TEXT="&#x418;&#x442;&#x435;&#x440;&#x430;&#x442;&#x43e;&#x440;&#x44b;" STYLE_REF="yellow_node" ID="ID_209781520" CREATED="1435320799130" MODIFIED="1435320815119">
<node TEXT="Iterator&lt;Entry&lt;Integer, String&gt;&gt; itr1 = linkedHashMap.entrySet().iterator();&#xa;while (itr1.hasNext()) {&#xa;    Entry&lt;Integer, String&gt; entry = itr1.next();&#xa;    System.out.println(entry.getKey() + &quot; = &quot; + entry.getValue());&#xa;}" STYLE_REF="example_node" ID="ID_1076054940" CREATED="1435320822538" MODIFIED="1435321111736"/>
<node TEXT="Iterator&lt;Integer&gt; itr2 = linkedHashMap.keySet().iterator();&#xa;while(itr2.hasNext())&#xa;    System.out.println(itr2.next());" STYLE_REF="example_node" ID="ID_323595234" CREATED="1435320828578" MODIFIED="1435321111739"/>
<node TEXT="Iterator&lt;String&gt; itr3 = linkedHashMap.values().iterator();&#xa;while (itr3.hasNext())&#xa;    System.out.println(itr3.next());" STYLE_REF="example_node" ID="ID_744316380" CREATED="1435320834666" MODIFIED="1435321111740"/>
</node>
<node TEXT="&#x418;&#x435;&#x440;&#x430;&#x440;&#x445;&#x438;&#x44f;" STYLE_REF="yellow_node" ID="ID_744833473" CREATED="1435321118218" MODIFIED="1435321184279">
<node TEXT="" STYLE_REF="pre_last_node" ID="ID_619574644" CREATED="1435321130665" MODIFIED="1435321188558">
<hook URI="linked-hashmap-hierarchy.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
</node>
<node TEXT="&#x41e;&#x441;&#x43e;&#x431;&#x435;&#x43d;&#x43d;&#x43e;&#x441;&#x442;&#x438;" STYLE_REF="yellow_node" ID="ID_776335630" CREATED="1435321229098" MODIFIED="1435321246807">
<node TEXT="&#x414;&#x430;&#x43d;&#x43d;&#x430;&#x44f; &#x441;&#x442;&#x440;&#x443;&#x43a;&#x442;&#x443;&#x440;&#x430; &#x43c;&#x43e;&#x436;&#x435;&#x442; &#x441;&#x43b;&#x435;&#x433;&#x43a;&#x430; &#x443;&#x441;&#x442;&#x443;&#x43f;&#x430;&#x442;&#x44c; &#x43f;&#x43e; &#x43f;&#x440;&#x43e;&#x438;&#x437;&#x432;&#x43e;&#x434;&#x438;&#x442;&#x435;&#x43b;&#x44c;&#x43d;&#x43e;&#x441;&#x442;&#x438; &#x440;&#x43e;&#x434;&#x438;&#x442;&#x435;&#x43b;&#x44c;&#x441;&#x43a;&#x43e;&#x43c;&#x443; HashMap, &#x43f;&#x440;&#x438; &#x44d;&#x442;&#x43e;&#x43c; &#x432;&#x440;&#x435;&#x43c;&#x44f; &#x432;&#x44b;&#x43f;&#x43e;&#x43b;&#x43d;&#x435;&#x43d;&#x438;&#x44f; &#x43e;&#x43f;&#x435;&#x440;&#x430;&#x446;&#x438;&#x439; add(), contains(), remove() &#x43e;&#x441;&#x442;&#x430;&#x435;&#x442;&#x441;&#x44f; &#x43a;&#x43e;&#x43d;&#x441;&#x442;&#x430;&#x43d;&#x442;&#x43e;&#x439; &#x2014; O(1). &#x41f;&#x43e;&#x43d;&#x430;&#x434;&#x43e;&#x431;&#x438;&#x442;&#x441;&#x44f; &#x447;&#x443;&#x442;&#x44c; &#x431;&#x43e;&#x43b;&#x44c;&#x448;&#x435; &#x43c;&#x435;&#x441;&#x442;&#x430; &#x432; &#x43f;&#x430;&#x43c;&#x44f;&#x442;&#x438; &#x434;&#x43b;&#x44f; &#x445;&#x440;&#x430;&#x43d;&#x435;&#x43d;&#x438;&#x44f; &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442;&#x43e;&#x432; &#x438; &#x438;&#x445; &#x441;&#x432;&#x44f;&#x437;&#x435;&#x439;, &#x43d;&#x43e; &#x44d;&#x442;&#x43e; &#x441;&#x43e;&#x432;&#x441;&#x435;&#x43c; &#x43d;&#x435;&#x431;&#x43e;&#x43b;&#x44c;&#x448;&#x430;&#x44f; &#x43f;&#x43b;&#x430;&#x442;&#x430; &#x437;&#x430; &#x434;&#x43e;&#x43f;&#x43e;&#x43b;&#x43d;&#x438;&#x442;&#x435;&#x43b;&#x44c;&#x43d;&#x44b;&#x435; &#x444;&#x438;&#x448;&#x435;&#x447;&#x43a;&#x438;" STYLE_REF="pre_last_node" ID="ID_681379011" CREATED="1435321233930" MODIFIED="1435321251231"/>
</node>
</node>
<node TEXT="Set" STYLE_REF="gray_node" ID="ID_775388882" CREATED="1435120999269" MODIFIED="1435121013394">
<node TEXT="Set &#x43d;&#x435; &#x434;&#x43e;&#x43f;&#x443;&#x441;&#x43a;&#x430;&#x435;&#x442; &#x445;&#x440;&#x430;&#x43d;&#x435;&#x43d;&#x438;&#x435; &#x434;&#x432;&#x443;&#x445; &#x43e;&#x434;&#x438;&#x43d;&#x430;&#x43a;&#x43e;&#x432;&#x44b;&#x445; &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442;&#x43e;&#x432;" STYLE_REF="pre_last_node" ID="ID_313255444" CREATED="1435120862005" MODIFIED="1435121027040">
<icon BUILTIN="messagebox_warning"/>
</node>
<node TEXT="TreeSet" STYLE_REF="gray_node" ID="ID_1597069603" CREATED="1435120844926" MODIFIED="1435121808578">
<node TEXT="TreeSet &#x43e;&#x431;&#x435;&#x441;&#x43f;&#x435;&#x447;&#x438;&#x432;&#x430;&#x435;&#x442; &#x443;&#x43f;&#x43e;&#x440;&#x44f;&#x434;&#x43e;&#x447;&#x435;&#x43d;&#x43d;&#x43e; &#x445;&#x440;&#x430;&#x43d;&#x435;&#x43d;&#x438;&#x435; &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442;&#x43e;&#x432; &#x432; &#x432;&#x438;&#x434;&#x435; &#x43a;&#x440;&#x430;&#x441;&#x43d;&#x43e;-&#x447;&#x435;&#x440;&#x43d;&#x43e;&#x433;&#x43e; &#x434;&#x435;&#x440;&#x435;&#x432;&#x430;" STYLE_REF="pre_last_node" ID="ID_1020802853" CREATED="1435121046949" MODIFIED="1435121816360"/>
<node TEXT="&#x421;&#x43b;&#x43e;&#x436;&#x43d;&#x43e;&#x441;&#x442;&#x44c; &#x432;&#x44b;&#x43f;&#x43e;&#x43b;&#x43d;&#x435;&#x43d;&#x438;&#x44f; &#x43e;&#x441;&#x43d;&#x43e;&#x432;&#x43d;&#x44b;&#x445; &#x43e;&#x43f;&#x435;&#x440;&#x430;&#x446;&#x438;&#x439; &#x432; TreeSet lg N" STYLE_REF="pre_last_node" ID="ID_1309770834" CREATED="1435121056318" MODIFIED="1435121816354"/>
</node>
<node TEXT="HashSet" STYLE_REF="gray_node" ID="ID_1953744466" CREATED="1435121018286" MODIFIED="1435121808577">
<node TEXT="HashSet &#x438;&#x441;&#x43f;&#x43e;&#x43b;&#x44c;&#x437;&#x443;&#x435;&#x442; &#x434;&#x43b;&#x44f; &#x445;&#x440;&#x430;&#x43d;&#x435;&#x43d;&#x438;&#x44f; &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442;&#x43e;&#x432; &#x442;&#x430;&#x43a;&#x43e;&#x439; &#x436;&#x435; &#x43f;&#x43e;&#x434;&#x445;&#x43e;&#x434;, &#x447;&#x442;&#x43e; &#x438; HashMap, &#x437;&#x430; &#x442;&#x435;&#x43c; &#x43e;&#x442;&#x43b;&#x438;&#x447;&#x438;&#x435;&#x43c;, &#x447;&#x442;&#x43e; &#x432; HashSet &#x432; &#x43a;&#x430;&#x447;&#x435;&#x441;&#x442;&#x432;&#x435; &#x43a;&#x43b;&#x44e;&#x447;&#x430; &#x432;&#x44b;&#x441;&#x442;&#x443;&#x43f;&#x430;&#x435;&#x442; &#x441;&#x430;&#x43c; &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442;" STYLE_REF="pre_last_node" ID="ID_1419110889" CREATED="1435121080597" MODIFIED="1435121816355"/>
<node TEXT="HashSet &#x43d;&#x435; &#x43f;&#x43e;&#x434;&#x434;&#x435;&#x440;&#x436;&#x438;&#x432;&#x430;&#x435;&#x442; &#x443;&#x43f;&#x43e;&#x440;&#x44f;&#x434;&#x43e;&#x447;&#x435;&#x43d;&#x43d;&#x43e;&#x435; &#x445;&#x440;&#x430;&#x43d;&#x435;&#x43d;&#x438;&#x435; &#x44d;&#x43b;&#x435;&#x43c;&#x435;&#x43d;&#x442;&#x43e;&#x432; &#x438; &#x43e;&#x431;&#x435;&#x441;&#x43f;&#x435;&#x447;&#x438;&#x432;&#x430;&#x435;&#x442; &#x432;&#x440;&#x435;&#x43c;&#x435;&#x43d;&#x43d;&#x443;&#x44e; &#x441;&#x43b;&#x43e;&#x436;&#x43d;&#x43e;&#x441;&#x442;&#x44c; &#x432;&#x44b;&#x43f;&#x43e;&#x43b;&#x43d;&#x435;&#x43d;&#x438;&#x44f; &#x43e;&#x43f;&#x435;&#x440;&#x430;&#x446;&#x438;&#x439; &#x430;&#x43d;&#x430;&#x43b;&#x43e;&#x433;&#x438;&#x447;&#x43d;&#x43e; HashMap" STYLE_REF="pre_last_node" ID="ID_97125341" CREATED="1435121087356" MODIFIED="1435121816358"/>
</node>
</node>
</node>
<node TEXT="Javadoc" STYLE_REF="second_node" POSITION="left" ID="ID_1868554812" CREATED="1436184735983" MODIFIED="1436188158696">
<edge COLOR="#7c0000"/>
<hook URI="../../Images/documentation.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Types of documentations" STYLE_REF="orange_node" ID="ID_1403969811" CREATED="1436184908615" MODIFIED="1436188088784">
<node TEXT="class" STYLE_REF="pre_last_node" ID="ID_1232781352" CREATED="1436184917991" MODIFIED="1436187902802"/>
<node TEXT="field" STYLE_REF="pre_last_node" ID="ID_1119088133" CREATED="1436184921055" MODIFIED="1436188088784"/>
<node TEXT="method" STYLE_REF="pre_last_node" ID="ID_1610114025" CREATED="1436184924598" MODIFIED="1436187902802"/>
</node>
<node TEXT="Ways to use Javadoc" STYLE_REF="orange_node" ID="ID_1902932906" CREATED="1436184771792" MODIFIED="1436187853752">
<node TEXT="Embed HTML" STYLE_REF="yellow_node" ID="ID_1757689149" CREATED="1436184773686" MODIFIED="1436187881721">
<node TEXT="Where" STYLE_REF="gray_node" ID="ID_1340628092" CREATED="1436185139359" MODIFIED="1436187945311">
<font BOLD="false"/>
<node TEXT="To format code" STYLE_REF="pre_last_node" ID="ID_1556048403" CREATED="1436185131191" MODIFIED="1436187938353">
<node TEXT="/**&#xa;* &lt;pre&gt;&#xa;* System.out.println(new Date());&#xa;* &lt;/pre&gt;&#xa;*/" STYLE_REF="example_node" ID="ID_294992192" CREATED="1436185207759" MODIFIED="1436187918728"/>
</node>
<node TEXT="To format the regular  text in descriptions" STYLE_REF="pre_last_node" ID="ID_667435262" CREATED="1436185187598" MODIFIED="1436187938352">
<node TEXT="/**&#xa;* You can &lt;em&gt;even&lt;/em&gt; insert a list:&#xa;* &lt;ol&gt;&#xa;* &lt;li&gt; Item one&#xa;* &lt;li&gt; Item two&#xa;* &lt;li&gt; Item three&#xa;* &lt;/ol&gt;&#xa;*/" STYLE_REF="example_node" ID="ID_235556579" CREATED="1436185217326" MODIFIED="1436187918730"/>
</node>
</node>
<node TEXT="Don&#x2019;t use" STYLE_REF="pre_last_node" ID="ID_1058404449" CREATED="1436185281424" MODIFIED="1436187938352"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Javadoc inserts its own headings and yours will interfere
    </p>
    <p>
      with them
    </p>
  </body>
</html>
</richcontent>
<node TEXT="&lt;h1&gt;" STYLE_REF="example_node" ID="ID_985685895" CREATED="1436185284744" MODIFIED="1436187918731"/>
<node TEXT="&lt;hr&gt;" STYLE_REF="example_node" ID="ID_1140037151" CREATED="1436185291413" MODIFIED="1436187918731"/>
</node>
</node>
<node TEXT="Doc tags" STYLE_REF="yellow_node" ID="ID_1979812318" CREATED="1436184787215" MODIFIED="1436187881720">
<node TEXT="@see" STYLE_REF="gray_node" ID="ID_1492565059" CREATED="1436185347518" MODIFIED="1436187870224">
<node TEXT="This tag allows you to refer to the documentation in other classes" STYLE_REF="pre_last_node" ID="ID_1034174725" CREATED="1436185364471" MODIFIED="1436187902792"/>
<node TEXT="@see classname&#xa;@see fully-qualified-classname&#xa;@see fully-qualified-classname#method-name" STYLE_REF="pre_last_node" ID="ID_708272787" CREATED="1436185376742" MODIFIED="1436187902793"/>
</node>
<node TEXT="@link package.class#member label" STYLE_REF="gray_node" ID="ID_118238160" CREATED="1436185431440" MODIFIED="1436187870225">
<node TEXT="Very similar to @see, except that it can be used inline and uses the label as the hyperlink text rather than &#x201c;See Also.&#x201d;" STYLE_REF="pre_last_node" ID="ID_619666149" CREATED="1436185448710" MODIFIED="1436187902794"/>
</node>
<node TEXT="@docRoot" STYLE_REF="gray_node" ID="ID_130898712" CREATED="1436185461831" MODIFIED="1436187870225">
<node TEXT="Produces the relative path to the documentation root directory. Useful for explicit hyperlinking to pages in the documentation tree." STYLE_REF="pre_last_node" ID="ID_1396385909" CREATED="1436185489159" MODIFIED="1436187902795"/>
</node>
<node TEXT="@inheritDoc" STYLE_REF="gray_node" ID="ID_861021069" CREATED="1436185510654" MODIFIED="1436187870226">
<node TEXT="Inherits the documentation from the nearest base class of this class into the current doc comment" STYLE_REF="pre_last_node" ID="ID_1489471276" CREATED="1436185522390" MODIFIED="1436187902795"/>
</node>
<node TEXT="@version" STYLE_REF="gray_node" ID="ID_1632131814" CREATED="1436185533518" MODIFIED="1436187870226">
<node TEXT="@version version-information" STYLE_REF="pre_last_node" ID="ID_401903161" CREATED="1436185556950" MODIFIED="1436187902797"/>
</node>
<node TEXT="@author" STYLE_REF="gray_node" ID="ID_1100173172" CREATED="1436185570714" MODIFIED="1436187870226">
<node TEXT="@author author-information" STYLE_REF="pre_last_node" ID="ID_311886247" CREATED="1436185579343" MODIFIED="1436187902796"/>
<node TEXT="Which author-information is, presumably, your name, but it could also include yur email address or any other appropriate information." STYLE_REF="pre_last_node" ID="ID_396272667" CREATED="1436185598990" MODIFIED="1436187902797"/>
<node TEXT="When the -author flag is placed on the Javadoc command line, the author information will be called out specially in the generated HTML documentation." STYLE_REF="pre_last_node" ID="ID_950999181" CREATED="1436185598990" MODIFIED="1436187902799"/>
</node>
<node TEXT="@since" STYLE_REF="gray_node" ID="ID_1639727809" CREATED="1436185651518" MODIFIED="1436187870226">
<node TEXT="This tag allows you to indicate the version of this code that began using a particular feature" STYLE_REF="pre_last_node" ID="ID_1457653995" CREATED="1436185664287" MODIFIED="1436187902799"/>
</node>
<node TEXT="@param" STYLE_REF="gray_node" ID="ID_1980306579" CREATED="1436185676031" MODIFIED="1436187870227">
<node TEXT="@param parameter-name description" STYLE_REF="pre_last_node" ID="ID_406201645" CREATED="1436185684367" MODIFIED="1436187902800">
<node TEXT="parameter-name is the identifier in the method parameter list" ID="ID_1739339045" CREATED="1436185697454" MODIFIED="1436185697454"/>
<node TEXT="description is text that can continue on subsequent lines. The description is considered finished when a new documentation tag is encountered" ID="ID_989546485" CREATED="1436185720669" MODIFIED="1436185753155">
<font BOLD="false"/>
</node>
</node>
</node>
<node TEXT="@return" STYLE_REF="gray_node" ID="ID_1919483071" CREATED="1436185753918" MODIFIED="1436187870227">
<node TEXT="@return description" STYLE_REF="pre_last_node" ID="ID_1404411464" CREATED="1436185765951" MODIFIED="1436187902800">
<node TEXT="In description gives you the meaning of the return value" ID="ID_413796863" CREATED="1436185777280" MODIFIED="1436185781698"/>
</node>
</node>
<node TEXT="@throws" STYLE_REF="gray_node" ID="ID_864368851" CREATED="1436185792494" MODIFIED="1436187870227">
<node TEXT="@throws fully-qualified-class-name description" STYLE_REF="pre_last_node" ID="ID_1223439375" CREATED="1436185831191" MODIFIED="1436187902801">
<node TEXT="fully-qualified-class-name gives an unambiguous name of an exception class" ID="ID_1386431411" CREATED="1436185844501" MODIFIED="1436185844501"/>
<node TEXT="description tells you why this particular type of exception can emerge from the method call" ID="ID_204233247" CREATED="1436185869614" MODIFIED="1436185877299"/>
</node>
</node>
<node TEXT="@deprecated" STYLE_REF="gray_node" ID="ID_927438708" CREATED="1436185882726" MODIFIED="1436187870227">
<node TEXT="The deprecated tag is a suggestion that you no longer use this particular feature, since sometime in the future it is likely to be removed" STYLE_REF="pre_last_node" ID="ID_1095573552" CREATED="1436185946581" MODIFIED="1436187902801"/>
</node>
</node>
</node>
<node TEXT="Features" STYLE_REF="orange_node" ID="ID_1616234442" CREATED="1436184983262" MODIFIED="1436195368086">
<node TEXT="Javadoc will process comment documentation for only" STYLE_REF="pre_last_node" ID="ID_315011467" CREATED="1436184983686" MODIFIED="1436195368086"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      You can use the -private flag to include private members as well
    </p>
  </body>
</html>
</richcontent>
<node TEXT="public" ID="ID_1302089644" CREATED="1436184989958" MODIFIED="1436184991563"/>
<node TEXT="protected" ID="ID_1929875764" CREATED="1436184993231" MODIFIED="1436184996036"/>
</node>
</node>
<node TEXT="Example" STYLE_REF="orange_node" ID="ID_1763959042" CREATED="1436432350621" MODIFIED="1436432360105">
<node TEXT="import java.util.*;&#xa; &#xa;/** The first Thinking in Java example program.&#xa; * Displays a string and today&#x2019;s date.&#xa; * @author Bruce Eckel&#xa; * @author www.MindView.net&#xa; * @version 4.0&#xa;*/&#xa;public class HelloDate {&#xa;  /** Entry point to class &amp; application.&#xa;   * @param args array of string arguments&#xa;   * @throws exceptions No exceptions thrown&#xa;  */&#xa;  public static void main(String[] args) {&#xa;    System.out.println(&quot;Hello, it&#x2019;s: &quot;);&#xa;    System.out.println(new Date());&#xa;  }&#xa;}" STYLE_REF="example_node" ID="ID_1775970142" CREATED="1436186090301" MODIFIED="1436187862400"/>
</node>
</node>
<node TEXT="Annotations" STYLE_REF="second_node" POSITION="right" ID="ID_398284777" CREATED="1436328975773" MODIFIED="1436332916803" VGAP="17">
<edge COLOR="#00007c"/>
<hook URI="../../Images/annotation.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Annotations, a form of metadata, provide data about a program that is not part of the program itself. Annotations have no direct effect on the operation of the code they annotate" STYLE_REF="main_definition" ID="ID_762076252" CREATED="1436328995597" MODIFIED="1436330054095"/>
<node TEXT="Uses" STYLE_REF="orange_node" ID="ID_1490299375" CREATED="1436329016550" MODIFIED="1436329084458">
<node TEXT="Information for the compiler" STYLE_REF="pre_last_node" ID="ID_808652012" CREATED="1436329022182" MODIFIED="1436329097233"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Annotations can be used by the compiler to detect errors or suppress warnings.
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true"/>
</node>
<node TEXT="Compile-time and deployment-time processing" STYLE_REF="pre_last_node" ID="ID_1420970545" CREATED="1436329022182" MODIFIED="1436329097234"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Software tools can process annotation information to generate code, XML files, and so forth.
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true"/>
</node>
<node TEXT="Runtime processing" STYLE_REF="pre_last_node" ID="ID_759276829" CREATED="1436329022183" MODIFIED="1436329097234"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Some annotations are available to be examined at runtime.
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true"/>
</node>
</node>
<node TEXT="Format" STYLE_REF="orange_node" ID="ID_1883580959" CREATED="1436329161517" MODIFIED="1436329607936">
<node TEXT="(@) indicates to the compiler that what follows is an annotation" STYLE_REF="pre_last_node" ID="ID_1212303191" CREATED="1436329250362" MODIFIED="1436329625996"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      @Override
    </p>
    <p>
      void mySuperMethod() { ... }
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="The annotation can include elements, which can be named or unnamed" STYLE_REF="pre_last_node" ID="ID_391734805" CREATED="1436329276796" MODIFIED="1436329625996">
<node TEXT="@Author(&#xa;   name = &quot;Benjamin Franklin&quot;,&#xa;   date = &quot;3/27/2003&quot;&#xa;)&#xa;class MyClass() { ... }" STYLE_REF="example_node" ID="ID_963269162" CREATED="1436329331277" MODIFIED="1436329669657"/>
<node TEXT="@SuppressWarnings(value = &quot;unchecked&quot;)" STYLE_REF="example_node" ID="ID_1305561734" CREATED="1436329336949" MODIFIED="1436329669657"/>
</node>
<node TEXT="If there is just one element named value, then the name can be omitted" STYLE_REF="pre_last_node" ID="ID_1599587040" CREATED="1436329345341" MODIFIED="1436329625996"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      @SuppressWarnings(&quot;unchecked&quot;)
    </p>
    <p>
      void myMethod() { ... }
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="It is also possible to use multiple annotations on the same declaration" STYLE_REF="pre_last_node" ID="ID_1898381445" CREATED="1436329372894" MODIFIED="1436329625996"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      @Author(name = &quot;Jane Doe&quot;)
    </p>
    <p>
      @EBook
    </p>
    <p>
      class MyClass { ... }
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="If the annotations have the same type, then this is called a repeating annotation" STYLE_REF="pre_last_node" ID="ID_1027319849" CREATED="1436329396620" MODIFIED="1436329625995"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      @Author(name = &quot;Jane Doe&quot;)
    </p>
    <p>
      @Author(name = &quot;John Smith&quot;)
    </p>
    <p>
      class MyClass { ... }
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Java &gt;=8" ID="ID_675497041" CREATED="1436329413877" MODIFIED="1436329418217"/>
</node>
</node>
<node TEXT="Applied" STYLE_REF="orange_node" ID="ID_363823635" CREATED="1436329505149" MODIFIED="1436329607936">
<node TEXT="Declarations" STYLE_REF="yellow_node" ID="ID_1207032676" CREATED="1436329515128" MODIFIED="1436329612713">
<node TEXT="declarations of classes" STYLE_REF="pre_last_node" ID="ID_1825792224" CREATED="1436329523432" MODIFIED="1436329625994"/>
<node STYLE_REF="pre_last_node" ID="ID_1106828100" CREATED="1436329529048" MODIFIED="1436329625994"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      fields
    </p>
  </body>
</html>
</richcontent>
</node>
<node STYLE_REF="pre_last_node" ID="ID_1245119253" CREATED="1436329533264" MODIFIED="1436329625994"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      methods
    </p>
  </body>
</html>
</richcontent>
</node>
<node STYLE_REF="pre_last_node" ID="ID_909913841" CREATED="1436329538982" MODIFIED="1436329625995"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      program elements
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Type annotation" STYLE_REF="yellow_node" ID="ID_160253233" CREATED="1436329552622" MODIFIED="1436329612713">
<node TEXT="new @Interned MyObject();" STYLE_REF="example_node" ID="ID_354970848" CREATED="1436329559268" MODIFIED="1436329630977"/>
<node TEXT="myString = (@NonNull String) str;" STYLE_REF="example_node" ID="ID_1029995910" CREATED="1436329565368" MODIFIED="1436329630978"/>
<node TEXT="class UnmodifiableList&lt;T&gt; implements @Readonly List&lt;@Readonly T&gt; { ... }" STYLE_REF="example_node" ID="ID_259808180" CREATED="1436329570052" MODIFIED="1436329630978"/>
<node TEXT="void monitorTemperature() throws @Critical TemperatureException { ... }" STYLE_REF="example_node" ID="ID_86497412" CREATED="1436329575479" MODIFIED="1436329630978"/>
</node>
</node>
<node TEXT="Declaring an Annotation Type" STYLE_REF="orange_node" ID="ID_993538004" CREATED="1436329732619" MODIFIED="1436330026096">
<node TEXT="For example we have preamble in comments" STYLE_REF="pre_last_node" ID="ID_1504788143" CREATED="1436329866909" MODIFIED="1436330078129">
<node TEXT="public class Generation3List extends Generation2List {&#xa;&#xa;   // Author: John Doe&#xa;   // Date: 3/17/2002&#xa;   // Current revision: 6&#xa;   // Last modified: 4/12/2004&#xa;   // By: Jane Doe&#xa;   // Reviewers: Alice, Bill, Cindy&#xa;&#xa;   // class code goes here&#xa;}" STYLE_REF="example_node" ID="ID_1240266010" CREATED="1436329883819" MODIFIED="1436330078129"/>
</node>
<node TEXT="To add this same metadata with an annotation, you must first define the annotation type" STYLE_REF="pre_last_node" ID="ID_1544673096" CREATED="1436329819108" MODIFIED="1436432165923">
<node TEXT="@interface ClassPreamble {&#xa;   String author();&#xa;   String date();&#xa;   int currentRevision() default 1;&#xa;   String lastModified() default &quot;N/A&quot;;&#xa;   String lastModifiedBy() default &quot;N/A&quot;;&#xa;   // Note use of array&#xa;   String[] reviewers();&#xa;}" STYLE_REF="example_node" ID="ID_1832454797" CREATED="1436329845365" MODIFIED="1436432165922"/>
</node>
<node TEXT="Use" STYLE_REF="pre_last_node" ID="ID_1125485836" CREATED="1436329924391" MODIFIED="1436330033772">
<node TEXT="@ClassPreamble (&#xa;   author = &quot;John Doe&quot;,&#xa;   date = &quot;3/17/2002&quot;,&#xa;   currentRevision = 6,&#xa;   lastModified = &quot;4/12/2004&quot;,&#xa;   lastModifiedBy = &quot;Jane Doe&quot;,&#xa;   // Note array notation&#xa;   reviewers = {&quot;Alice&quot;, &quot;Bob&quot;, &quot;Cindy&quot;}&#xa;)&#xa;public class Generation3List extends Generation2List {&#xa;&#xa;// class code goes here&#xa;&#xa;}" STYLE_REF="example_node" ID="ID_1096128990" CREATED="1436329934452" MODIFIED="1436330041001"/>
</node>
<node TEXT="To make the information in @ClassPreamble appear in Javadoc-generated documentation, you must annotate the @ClassPreamble definition with the @Documented annotation" STYLE_REF="pre_last_node" ID="ID_1405152345" CREATED="1436330001901" MODIFIED="1436330033772">
<node TEXT="// import this to use @Documented&#xa;import java.lang.annotation.*;&#xa;&#xa;@Documented&#xa;@interface ClassPreamble {&#xa;   // Annotation element definitions  &#xa;}" STYLE_REF="example_node" ID="ID_160432732" CREATED="1436330012011" MODIFIED="1436330041001"/>
</node>
</node>
<node TEXT="Predefined Annotation Types" STYLE_REF="orange_node" ID="ID_1344810824" CREATED="1436330072492" MODIFIED="1436330083456">
<node TEXT="import java.lang.annotation;" STYLE_REF="main_definition" ID="ID_937343755" CREATED="1436330114500" MODIFIED="1436331299024"/>
<node TEXT="Annotation Types Used by the Java Language" STYLE_REF="yellow_node" ID="ID_50826223" CREATED="1436330652020" MODIFIED="1436330669035">
<node TEXT="@Deprecated" STYLE_REF="gray_node" ID="ID_1503910180" CREATED="1436330122996" MODIFIED="1436330674660">
<node TEXT="Indicates that the marked element is deprecated and should no longer be used" STYLE_REF="pre_last_node" ID="ID_287891403" CREATED="1436330197332" MODIFIED="1436330692914"/>
<node TEXT="When an element is deprecated, it should also be documented using the Javadoc @deprecated tag" STYLE_REF="pre_last_node" ID="ID_1637812833" CREATED="1436330242835" MODIFIED="1436330692898"/>
<node TEXT="/**&#xa;* @deprecated&#xa;* explanation of why it was deprecated&#xa;*/&#xa;@Deprecated&#xa;static void deprecatedMethod() { }" STYLE_REF="example_node" ID="ID_1825878204" CREATED="1436330209156" MODIFIED="1436330698822"/>
</node>
<node TEXT="@Override" STYLE_REF="gray_node" ID="ID_1264990786" CREATED="1436330228036" MODIFIED="1436330674660">
<node TEXT="Informs the compiler that the element is meant to override an element declared in a superclas" STYLE_REF="pre_last_node" ID="ID_1850562730" CREATED="1436330261083" MODIFIED="1436330692916"/>
<node TEXT="If a method marked with @Override fails to correctly override a method in one of its superclasses, the compiler generates an error" STYLE_REF="pre_last_node" ID="ID_1966398185" CREATED="1436330312178" MODIFIED="1436330692918"/>
<node TEXT="@Override&#xa;int overriddenMethod() { }" STYLE_REF="example_node" ID="ID_1481514995" CREATED="1436330295131" MODIFIED="1436330698822"/>
</node>
<node TEXT="@SuppressWarnings" STYLE_REF="gray_node" ID="ID_190949646" CREATED="1436330320987" MODIFIED="1436330674660">
<node TEXT="Tells the compiler to suppress specific warnings that it would otherwise generate" STYLE_REF="pre_last_node" ID="ID_268779661" CREATED="1436330400948" MODIFIED="1436330692919"/>
<node TEXT="// use a deprecated method and tell&#xa;// compiler not to generate a warning&#xa;@SuppressWarnings(&quot;deprecation&quot;)&#xa;void useDeprecatedMethod() {&#xa;    // deprecation warning&#xa;    // - suppressed&#xa;    objectOne.deprecatedMethod();&#xa;}" STYLE_REF="pre_last_node" ID="ID_1768419254" CREATED="1436330418938" MODIFIED="1436330692920"/>
<node TEXT="unchecked warning" STYLE_REF="pre_last_node" ID="ID_416356907" CREATED="1436330483491" MODIFIED="1436330692923">
<node TEXT="Can occur when interfacing with legacy code written before the advent of generics" STYLE_REF="example_node" ID="ID_672218928" CREATED="1436330500259" MODIFIED="1436330698822"/>
</node>
<node TEXT="To suppress multiple categories of warnings" STYLE_REF="pre_last_node" ID="ID_1321391203" CREATED="1436330515419" MODIFIED="1436330692923">
<node TEXT="@SuppressWarnings({&quot;unchecked&quot;, &quot;deprecation&quot;})" ID="ID_64928173" CREATED="1436330470986" MODIFIED="1436330471698"/>
</node>
</node>
<node TEXT="@SafeVarargs" STYLE_REF="gray_node" ID="ID_314146740" CREATED="1436330528123" MODIFIED="1436330674660">
<node TEXT="Asserts that the code does not perform potentially unsafe operations on its varargs parameter" STYLE_REF="pre_last_node" ID="ID_479353616" CREATED="1436330567834" MODIFIED="1436330692923"/>
<node TEXT="When this annotation type is used, unchecked warnings relating to varargs usage are suppressed" STYLE_REF="pre_last_node" ID="ID_60866027" CREATED="1436330590698" MODIFIED="1436330692926"/>
</node>
<node TEXT="@FunctionalInterface" STYLE_REF="gray_node" ID="ID_411024569" CREATED="1436330608507" MODIFIED="1436330674660">
<node TEXT="Indicates that the type declaration is intended to be a functional interface" STYLE_REF="pre_last_node" ID="ID_652355147" CREATED="1436330621857" MODIFIED="1436330692927"/>
<node TEXT="Java &gt;= 8" STYLE_REF="pre_last_node" ID="ID_1972378748" CREATED="1436330626650" MODIFIED="1436330692928"/>
</node>
</node>
<node TEXT="Meta-annotations" STYLE_REF="yellow_node" ID="ID_744942333" CREATED="1436331265739" MODIFIED="1436331821683"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Annotations That Apply to Other Annotations
    </p>
  </body>
</html>
</richcontent>
<node TEXT="@Retention" STYLE_REF="gray_node" ID="ID_1336725679" CREATED="1436331308090" MODIFIED="1436331762703">
<node TEXT="Specifies how the marked annotation is stored" STYLE_REF="main_definition" ID="ID_119323972" CREATED="1436331397098" MODIFIED="1436331434407"/>
<node TEXT="RetentionPolicy.SOURCE" STYLE_REF="example_node" ID="ID_282956249" CREATED="1436331427218" MODIFIED="1436331807766">
<node TEXT="The marked annotation is retained only in the source level and is ignored by the compiler" STYLE_REF="pre_last_node" ID="ID_4693757" CREATED="1436331460420" MODIFIED="1436331801708"/>
</node>
<node TEXT="RetentionPolicy.CLASS" STYLE_REF="example_node" ID="ID_831054780" CREATED="1436331442348" MODIFIED="1436331807766">
<node TEXT="The marked annotation is retained by the compiler at compile time, but is ignored by the JVM" STYLE_REF="pre_last_node" ID="ID_1102693978" CREATED="1436331473838" MODIFIED="1436331801708"/>
</node>
<node TEXT="RetentionPolicy.RUNTIME" STYLE_REF="example_node" ID="ID_345038265" CREATED="1436331447317" MODIFIED="1436331807766">
<node TEXT="he marked annotation is retained by the JVM so it can be used by the runtime environment" STYLE_REF="pre_last_node" ID="ID_1796294435" CREATED="1436331489379" MODIFIED="1436331801692"/>
</node>
</node>
<node TEXT="@Documented" STYLE_REF="gray_node" ID="ID_892858122" CREATED="1436331495645" MODIFIED="1436331762704">
<node TEXT="Indicates that whenever the specified annotation is used those elements should be documented using the Javadoc tool" STYLE_REF="pre_last_node" ID="ID_1312659009" CREATED="1436331514293" MODIFIED="1436331801692"/>
</node>
<node TEXT="@Target" STYLE_REF="gray_node" ID="ID_576479604" CREATED="1436331526278" MODIFIED="1436331762704">
<node TEXT="Annotation marks another annotation to restrict what kind of Java elements the annotation can be applied to" STYLE_REF="main_definition" ID="ID_1973381941" CREATED="1436331554972" MODIFIED="1436331811563"/>
<node TEXT="ElementType.ANNOTATION_TYPE" STYLE_REF="example_node" ID="ID_1864830743" CREATED="1436331563212" MODIFIED="1436331778437">
<node TEXT="Can be applied to an annotation type" STYLE_REF="pre_last_node" ID="ID_864911632" CREATED="1436331754834" MODIFIED="1436331801692"/>
</node>
<node TEXT="ElementType.CONSTRUCTOR" STYLE_REF="example_node" ID="ID_494903631" CREATED="1436331563212" MODIFIED="1436331778438">
<node TEXT="Can be applied to a constructor" STYLE_REF="pre_last_node" ID="ID_1272687101" CREATED="1436331747313" MODIFIED="1436331801692"/>
</node>
<node TEXT="ElementType.FIELD" STYLE_REF="example_node" ID="ID_1751960386" CREATED="1436331563212" MODIFIED="1436331778438">
<node TEXT="Can be applied to a field or property" STYLE_REF="pre_last_node" ID="ID_578435766" CREATED="1436331738377" MODIFIED="1436331801692"/>
</node>
<node TEXT="ElementType.LOCAL_VARIABLE" STYLE_REF="example_node" ID="ID_616351121" CREATED="1436331563212" MODIFIED="1436331778439">
<node TEXT="Can be applied to a local variable" STYLE_REF="pre_last_node" ID="ID_1598173380" CREATED="1436331731833" MODIFIED="1436331801692"/>
</node>
<node TEXT="ElementType.METHOD" STYLE_REF="example_node" ID="ID_845691744" CREATED="1436331563213" MODIFIED="1436331778439">
<node TEXT="Can be applied to a method-level annotation" STYLE_REF="pre_last_node" ID="ID_1833336069" CREATED="1436331724793" MODIFIED="1436331801692"/>
</node>
<node TEXT="ElementType.PACKAGE" STYLE_REF="example_node" ID="ID_1867669194" CREATED="1436331563213" MODIFIED="1436331778439">
<node TEXT="Can be applied to a package declaration" STYLE_REF="pre_last_node" ID="ID_793929710" CREATED="1436331711322" MODIFIED="1436331801692"/>
</node>
<node TEXT="ElementType.PARAMETER" STYLE_REF="example_node" ID="ID_1352211741" CREATED="1436331563213" MODIFIED="1436331778439">
<node TEXT="Can be applied to the parameters of a method" STYLE_REF="pre_last_node" ID="ID_1989098760" CREATED="1436331698690" MODIFIED="1436331801692"/>
</node>
<node TEXT="ElementType.TYPE" STYLE_REF="example_node" ID="ID_647368503" CREATED="1436331563213" MODIFIED="1436331778439">
<node TEXT="Can be applied to any element of a class" STYLE_REF="pre_last_node" ID="ID_1083806023" CREATED="1436331689737" MODIFIED="1436331801692"/>
</node>
</node>
<node TEXT="@Inherited" STYLE_REF="gray_node" ID="ID_1063686959" CREATED="1436331530917" MODIFIED="1436331762704">
<node TEXT="Annotation indicates that the annotation type can be inherited from the super class" STYLE_REF="pre_last_node" ID="ID_725667203" CREATED="1436331577197" MODIFIED="1436331801692"/>
<node TEXT="This annotation applies only to class declarations" STYLE_REF="pre_last_node" ID="ID_642517365" CREATED="1436331600836" MODIFIED="1436331801692"/>
</node>
<node TEXT="@Repeatable" STYLE_REF="gray_node" ID="ID_426058466" CREATED="1436331535691" MODIFIED="1436331762704">
<node TEXT="Java &gt;= 8" ID="ID_967206663" CREATED="1436331655986" MODIFIED="1436331663668"/>
<node TEXT="Indicates that the marked annotation can be applied more than once to the same declaration or type use" STYLE_REF="pre_last_node" ID="ID_509467864" CREATED="1436331677329" MODIFIED="1436331801692"/>
</node>
</node>
</node>
<node TEXT="Type Annotations" STYLE_REF="orange_node" ID="ID_1436391442" CREATED="1436332062041" MODIFIED="1436332229748">
<node TEXT="Type annotations were created to support improved analysis of Java programs way of ensuring stronger type checking" STYLE_REF="pre_last_node" ID="ID_1678861132" CREATED="1436332086258" MODIFIED="1436332204957"/>
<node TEXT="For example, you want to ensure that a particular variable in your program is never assigned to null" STYLE_REF="pre_last_node" ID="ID_551097273" CREATED="1436332154451" MODIFIED="1436332204957">
<node TEXT="@NonNull String str;" STYLE_REF="example_node" ID="ID_277001353" CREATED="1436332159425" MODIFIED="1436332219984"/>
<node TEXT="When you compile the code, including the NonNull module at the command line, the compiler prints a warning if it detects a potential problem, allowing you to modify the code to avoid the error. After you correct the code to remove all warnings, this particular error will not occur when the program runs." STYLE_REF="pre_last_node" ID="ID_1071360752" CREATED="1436332116666" MODIFIED="1436332223736"/>
</node>
<node TEXT="http://types.cs.washington.edu/checker-framework/" STYLE_REF="yellow_node" ID="ID_864570555" CREATED="1436332093531" MODIFIED="1436332216431" LINK="http://types.cs.washington.edu/checker-framework/"/>
</node>
<node TEXT="Repeating Annotations" STYLE_REF="orange_node" ID="ID_1285525461" CREATED="1436332188225" MODIFIED="1436332194176">
<node TEXT="Applying the same annotation to a declaration without first declaring it to be repeatable results in a compile-time error" STYLE_REF="main_definition" ID="ID_1574560674" CREATED="1436332432729" MODIFIED="1436332437919"/>
<node TEXT="Declare" STYLE_REF="yellow_node" ID="ID_1996753102" CREATED="1436332364618" MODIFIED="1436332946469">
<node TEXT="@Repeatable(Schedules.class)&#xa;public @interface Schedule {&#xa;  String dayOfMonth() default &quot;first&quot;;&#xa;  String dayOfWeek() default &quot;Mon&quot;;&#xa;  int hour() default 12;&#xa;}" STYLE_REF="example_node" ID="ID_1213958848" CREATED="1436332419737" MODIFIED="1436332946468"/>
<node TEXT="Containing Annotation Type" STYLE_REF="pre_last_node" ID="ID_333724552" CREATED="1436332629817" MODIFIED="1436332901150">
<node TEXT="public @interface Schedules {&#xa;    Schedule[] value();&#xa;}" STYLE_REF="example_node" ID="ID_1599906514" CREATED="1436332636185" MODIFIED="1436332896381"/>
</node>
</node>
<node TEXT="Examples" STYLE_REF="yellow_node" ID="ID_793993072" CREATED="1436332366730" MODIFIED="1436332948014">
<node TEXT="For example, you have a class for handling unauthorized access exceptions. You annotate the class with one @Alert annotation for managers and another for admins" STYLE_REF="pre_last_node" ID="ID_1800379243" CREATED="1436332341537" MODIFIED="1436332901148">
<node TEXT="@Alert(role=&quot;Manager&quot;)&#xa;@Alert(role=&quot;Administrator&quot;)&#xa;public class UnauthorizedAccessException extends SecurityException { ... }" STYLE_REF="example_node" ID="ID_440433484" CREATED="1436332325257" MODIFIED="1436332896382"/>
</node>
<node TEXT="@Schedule(dayOfMonth=&quot;last&quot;)&#xa;@Schedule(dayOfWeek=&quot;Fri&quot;, hour=&quot;23&quot;)&#xa;public void doPeriodicCleanup() { ... }" STYLE_REF="example_node" ID="ID_425492643" CREATED="1436332331179" MODIFIED="1436332948014"/>
</node>
<node TEXT="Retrieving" STYLE_REF="yellow_node" ID="ID_1452918507" CREATED="1436332491033" MODIFIED="1436332889030">
<node TEXT="AnnotatedElement.getAnnotationByType(Class&lt;T&gt;)" STYLE_REF="example_node" ID="ID_845437193" CREATED="1436332587243" MODIFIED="1436332896385"/>
<node TEXT="AnnotatedElement.getAnnotations(Class&lt;T&gt;)" STYLE_REF="example_node" ID="ID_605031152" CREATED="1436332611249" MODIFIED="1436332896385"/>
</node>
</node>
</node>
<node TEXT="Inheritance" STYLE_REF="second_node" POSITION="right" ID="ID_1567607053" CREATED="1436332979664" MODIFIED="1436432108512">
<edge COLOR="#007c00"/>
<hook URI="../../Images/inheritance.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="When you want to create a new class and there is already a class that includes some of the code that you want, you can derive your new class from the existing class" STYLE_REF="main_definition" ID="ID_983367291" CREATED="1436431443774" MODIFIED="1436431989881"/>
<node TEXT="Inheritance" STYLE_REF="orange_node" ID="ID_814161885" CREATED="1436431618495" MODIFIED="1436431995145">
<node TEXT="A subclass inherits all the members (fields, methods, and nested classes) from its superclass. Constructors are not members, so they are not inherited by subclasses, but the constructor of the superclass can be invoked from the subclass." STYLE_REF="pre_last_node" ID="ID_400486807" CREATED="1436431457092" MODIFIED="1436432022313"/>
<node TEXT="What You Can Do in a Subclass" STYLE_REF="gray_node" ID="ID_1338182131" CREATED="1436431631976" MODIFIED="1436432002506">
<node TEXT="The inherited fields can be used directly, just like any other fields." STYLE_REF="pre_last_node" ID="ID_160108947" CREATED="1436431637380" MODIFIED="1436432022315"/>
<node TEXT="You can declare a field in the subclass with the same name as the one in the superclass, thus hiding it (not recommended)." STYLE_REF="pre_last_node" ID="ID_1352324530" CREATED="1436431637380" MODIFIED="1436432022316"/>
<node TEXT="You can declare new fields in the subclass that are not in the superclass." STYLE_REF="pre_last_node" ID="ID_1918786093" CREATED="1436431637380" MODIFIED="1436432022318"/>
<node TEXT="The inherited methods can be used directly as they are." STYLE_REF="pre_last_node" ID="ID_1594994654" CREATED="1436431637381" MODIFIED="1436432022319"/>
<node TEXT="You can write a new instance method in the subclass that has the same signature as the one in the superclass, thus overriding it." STYLE_REF="pre_last_node" ID="ID_1285130711" CREATED="1436431637382" MODIFIED="1436432022320"/>
<node TEXT="You can write a new static method in the subclass that has the same signature as the one in the superclass, thus hiding it." STYLE_REF="pre_last_node" ID="ID_1502139629" CREATED="1436431637382" MODIFIED="1436432022321"/>
<node TEXT="You can declare new methods in the subclass that are not in the superclass." STYLE_REF="pre_last_node" ID="ID_909541970" CREATED="1436431637382" MODIFIED="1436432022322"/>
<node TEXT="You can write a subclass constructor that invokes the constructor of the superclass, either implicitly or by using the keyword super." STYLE_REF="pre_last_node" ID="ID_1996949318" CREATED="1436431637383" MODIFIED="1436432022323"/>
</node>
<node TEXT="Private Members in a Superclass" STYLE_REF="gray_node" ID="ID_813634045" CREATED="1436431644642" MODIFIED="1436432002505">
<node TEXT="A subclass does not inherit the private members of its parent class. However, if the superclass has public or protected methods for accessing its private fields, these can also be used by the subclass" STYLE_REF="pre_last_node" ID="ID_1881734810" CREATED="1436431652255" MODIFIED="1436432022324"/>
<node TEXT="A nested class has access to all the private members of its enclosing class&#x2014;both fields and methods. Therefore, a public or protected nested class inherited by a subclass has indirect access to all of the private members of the superclass." STYLE_REF="pre_last_node" ID="ID_495895150" CREATED="1436431658065" MODIFIED="1436432022323"/>
</node>
<node TEXT="Casting Objects" STYLE_REF="gray_node" ID="ID_1462296070" CREATED="1436431714397" MODIFIED="1436432117025">
<node TEXT="MountainBike is descended from Bicycle and Object. Therefore, a MountainBike is a Bicycle and is also an Object, and it can be used wherever Bicycle or Object objects are called for.&#xa;The reverse is not necessarily true: a Bicycle may be a MountainBike, but it isn&apos;t necessarily. Similarly, an Object may be a Bicycle or a MountainBike, but it isn&apos;t necessarily" STYLE_REF="pre_last_node" ID="ID_450595702" CREATED="1436431977092" MODIFIED="1436432022325"/>
<node TEXT="Implicit casting" STYLE_REF="yellow_node" ID="ID_850407384" CREATED="1436431750103" MODIFIED="1436432117025">
<node TEXT="Object obj = new MountainBike();" STYLE_REF="example_node" ID="ID_296199879" CREATED="1436431756422" MODIFIED="1436432041483"/>
<node TEXT="obj is both an Object and a MountainBike (until such time as obj is assigned another object that is not a MountainBike)" STYLE_REF="example_node" ID="ID_566476146" CREATED="1436431771134" MODIFIED="1436432041484"/>
<node TEXT="MountainBike myBike = obj;" STYLE_REF="example_node" ID="ID_1645957538" CREATED="1436431800110" MODIFIED="1436432041483"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      compile-time error because obj is not known to the compiler to be a MountainBike
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Explicit casting" STYLE_REF="yellow_node" ID="ID_1726994905" CREATED="1436431826533" MODIFIED="1436432033793">
<node TEXT="MountainBike myBike = (MountainBike)obj;" STYLE_REF="example_node" ID="ID_502707411" CREATED="1436431836829" MODIFIED="1436432041481"/>
<node TEXT="If obj is not a MountainBike at runtime, an exception will be thrown" STYLE_REF="pre_last_node" ID="ID_710280057" CREATED="1436431856926" MODIFIED="1436432022326"/>
<node TEXT="You can make a logical test as to the type of a particular object using the instanceof operator. This can save you from a runtime error owing to an improper cast." STYLE_REF="pre_last_node" ID="ID_1146288525" CREATED="1436431874015" MODIFIED="1436432022327">
<node TEXT="if (obj instanceof MountainBike) {&#xa;    MountainBike myBike = (MountainBike)obj;&#xa;}" STYLE_REF="example_node" ID="ID_1296636197" CREATED="1436431891164" MODIFIED="1436432041484"/>
</node>
</node>
</node>
</node>
<node TEXT="Abstract Method" STYLE_REF="orange_node" ID="ID_53112724" CREATED="1436332989142" MODIFIED="1436429238405">
<node TEXT="An abstract method is a method that is declared without an implementation" STYLE_REF="pre_last_node" ID="ID_24624493" CREATED="1436333056239" MODIFIED="1436333307712"/>
<node TEXT="abstract void moveTo(double deltaX, double deltaY);" STYLE_REF="example_node" ID="ID_1456848522" CREATED="1436333063168" MODIFIED="1436333313325"/>
<node TEXT="Methods in an interface that are not declared as default or static are implicitly abstract" STYLE_REF="pre_last_node" ID="ID_1288874115" CREATED="1436333153191" MODIFIED="1436429238404"/>
</node>
<node TEXT="Abstract Class" STYLE_REF="orange_node" ID="ID_1109238229" CREATED="1436333020063" MODIFIED="1436333281652">
<node TEXT="Features" STYLE_REF="yellow_node" ID="ID_1778795173" CREATED="1436333263023" MODIFIED="1436333286268">
<node TEXT="Abstract classes cannot be instantiated, but they can be subclassed" STYLE_REF="pre_last_node" ID="ID_1533161186" CREATED="1436333025544" MODIFIED="1436333307709"/>
<node TEXT="If a class includes abstract methods, then the class itself must be declared abstract" STYLE_REF="pre_last_node" ID="ID_1885597998" CREATED="1436333078272" MODIFIED="1436333307708"/>
<node TEXT="public abstract class GraphicObject {&#xa;   abstract void draw();&#xa;}" STYLE_REF="example_node" ID="ID_1716027278" CREATED="1436333090911" MODIFIED="1436333313326"/>
<node TEXT="When an abstract class is subclassed, the subclass usually provides implementations for all of the abstract methods in its parent class. However, if it does not, then the subclass must also be declared abstract" STYLE_REF="pre_last_node" ID="ID_1069113992" CREATED="1436333186799" MODIFIED="1436333307707"/>
<node TEXT="It may contain a mix of methods declared with or without an implementation" STYLE_REF="pre_last_node" ID="ID_415657695" CREATED="1436343979481" MODIFIED="1436344024346"/>
<node TEXT="You can declare fields that are not static and final, and define public, protected, and private concrete methods" STYLE_REF="pre_last_node" ID="ID_368114361" CREATED="1436344001720" MODIFIED="1436344024346"/>
</node>
<node TEXT="Abstract Classes Compared to Interfaces" STYLE_REF="yellow_node" ID="ID_298181884" CREATED="1436333249439" MODIFIED="1436333286269">
<node TEXT="Use abstract class when" STYLE_REF="gray_node" ID="ID_1737148741" CREATED="1436334202845" MODIFIED="1436344013772">
<font BOLD="false"/>
<node TEXT="You want to share code among several closely related classes" STYLE_REF="pre_last_node" ID="ID_860443974" CREATED="1436334229800" MODIFIED="1436344024346"/>
<node TEXT="You expect that classes that extend your abstract class have many common methods or fields, or require access modifiers other than public" STYLE_REF="pre_last_node" ID="ID_1228647853" CREATED="1436334266766" MODIFIED="1436344024346"/>
<node TEXT="You want to declare non-static or non-final fields." STYLE_REF="pre_last_node" ID="ID_1191956550" CREATED="1436334286086" MODIFIED="1436344024346"/>
</node>
<node TEXT="Use interface class when" STYLE_REF="gray_node" ID="ID_981755368" CREATED="1436334202845" MODIFIED="1436344013772">
<font BOLD="false"/>
<node TEXT="You expect that unrelated classes would implement your interface" STYLE_REF="pre_last_node" ID="ID_1345434663" CREATED="1436334297750" MODIFIED="1436344024346">
<node TEXT="For example, the interfaces Comparable and Cloneable are implemented by many unrelated classes" ID="ID_224282158" CREATED="1436334307280" MODIFIED="1436334307280"/>
</node>
<node TEXT="You want to specify the behavior of a particular data type, but not concerned about who implements its behavior" STYLE_REF="pre_last_node" ID="ID_1833524003" CREATED="1436334326283" MODIFIED="1436344024346"/>
<node TEXT="You want to take advantage of multiple inheritance of type" STYLE_REF="pre_last_node" ID="ID_365005192" CREATED="1436334333086" MODIFIED="1436344024346"/>
</node>
</node>
<node TEXT="Abstract Class Implements an Interface" STYLE_REF="yellow_node" ID="ID_1742162072" CREATED="1436344357016" MODIFIED="1436344392069">
<node TEXT="To define a class that does not implement all of the interface&apos;s methods, provided that the class is declared to be abstract" STYLE_REF="pre_last_node" ID="ID_1150495439" CREATED="1436344383135" MODIFIED="1436344450942"/>
<node TEXT="abstract class X implements Y {&#xa;  // implements all but one method of Y&#xa;}" STYLE_REF="example_node" ID="ID_1884616736" CREATED="1436344423288" MODIFIED="1436344456938"/>
<node TEXT="class XX extends X {&#xa;  // implements the remaining method in Y&#xa;}" STYLE_REF="example_node" ID="ID_1224218975" CREATED="1436344428791" MODIFIED="1436344456937"/>
<node TEXT="Class X must be abstract because it does not fully implement Y, but class XX does, in fact, implement Y" STYLE_REF="pre_last_node" ID="ID_1608726104" CREATED="1436344439728" MODIFIED="1436344450940"/>
</node>
<node TEXT="Example" STYLE_REF="yellow_node" ID="ID_203274436" CREATED="1436344226799" MODIFIED="1436344392069">
<node TEXT="" STYLE_REF="pre_last_node" ID="ID_910739319" CREATED="1436344229567" MODIFIED="1436344450947">
<hook URI="abstract-class.jpg" SIZE="1.0" NAME="ExternalObject"/>
</node>
<node TEXT="abstract class GraphicObject {&#xa;    int x, y;&#xa;    ...&#xa;    void moveTo(int newX, int newY) {&#xa;        ...&#xa;    }&#xa;    abstract void draw();&#xa;    abstract void resize();&#xa;}" STYLE_REF="example_node" ID="ID_1855488972" CREATED="1436344249424" MODIFIED="1436344456924"/>
<node TEXT="Each nonabstract subclass of GraphicObject, such as Circle and Rectangle, must provide implementations for the draw and resize methods" STYLE_REF="pre_last_node" ID="ID_1062038399" CREATED="1436344267681" MODIFIED="1436344450945"/>
<node TEXT="class Circle extends GraphicObject {&#xa;    void draw() {&#xa;        ...&#xa;    }&#xa;    void resize() {&#xa;        ...&#xa;    }&#xa;}" STYLE_REF="example_node" ID="ID_1227866494" CREATED="1436344275040" MODIFIED="1436344456934"/>
<node TEXT="class Rectangle extends GraphicObject {&#xa;    void draw() {&#xa;        ...&#xa;    }&#xa;    void resize() {&#xa;        ...&#xa;    }&#xa;}" STYLE_REF="example_node" ID="ID_185413130" CREATED="1436344283047" MODIFIED="1436344456936"/>
</node>
</node>
<node TEXT="Multiple Inheritance" STYLE_REF="orange_node" ID="ID_1634900094" CREATED="1436356906888" MODIFIED="1436357200652">
<node TEXT="Multiple inheritance of state" STYLE_REF="gray_node" ID="ID_242107995" CREATED="1436356986581" MODIFIED="1436423708517">
<node TEXT="For example, suppose that you are able to define a new class that extends multiple classes. When you create an object by instantiating that class, that object will inherit fields from all of the class&apos;s superclasses. What if methods or constructors from different superclasses instantiate the same field? Which method or constructor will take precedence? Because interfaces do not contain fields, you do not have to worry about problems that result from multiple inheritance of state" STYLE_REF="pre_last_node" ID="ID_1019555939" CREATED="1436357052880" MODIFIED="1436423719560"/>
<node TEXT="Java doesn&apos;t support" STYLE_REF="last_node" ID="ID_1109424374" CREATED="1436356963935" MODIFIED="1436423726166"/>
</node>
<node TEXT="Multiple inheritance of implementation" STYLE_REF="gray_node" ID="ID_1118540119" CREATED="1436357075701" MODIFIED="1436423708517">
<node TEXT="Is the ability to inherit method definitions from multiple classes" STYLE_REF="pre_last_node" ID="ID_1178134820" CREATED="1436357082944" MODIFIED="1436423719560"/>
<node TEXT="A class can implement more than one interface, which can contain default methods that have the same name. The Java compiler provides some rules to determine which default method a particular class uses" STYLE_REF="pre_last_node" ID="ID_1815844298" CREATED="1436357130742" MODIFIED="1436423719559"/>
<node TEXT="Java support" STYLE_REF="last_node" ID="ID_1980394542" CREATED="1436356963935" MODIFIED="1436423726166"/>
</node>
<node TEXT="Multiple inheritance of type" STYLE_REF="gray_node" ID="ID_28539876" CREATED="1436356896294" MODIFIED="1436423708516">
<node TEXT="Is the ability of a class to implement more than one interface" STYLE_REF="pre_last_node" ID="ID_519157256" CREATED="1436356962111" MODIFIED="1436423719558"/>
<node TEXT="Java support" STYLE_REF="last_node" ID="ID_802400687" CREATED="1436356963935" MODIFIED="1436423726165"/>
</node>
</node>
<node TEXT="Overriding and Hiding Methods" STYLE_REF="orange_node" ID="ID_1070213014" CREATED="1436357178806" MODIFIED="1436357200652">
<node TEXT="Instance Methods" STYLE_REF="gray_node" ID="ID_1473011584" CREATED="1436357184311" MODIFIED="1436422673160">
<node TEXT="An instance method in a subclass with the same signature (name, plus the number and the type of its parameters) and return type as an instance method in the superclass overrides the superclass&apos;s method." STYLE_REF="pre_last_node" ID="ID_1028604732" CREATED="1436357189967" MODIFIED="1436423756006"/>
<node TEXT="An overriding method can also return a subtype of the type returned by the overridden method" STYLE_REF="pre_last_node" ID="ID_670422704" CREATED="1436420403936" MODIFIED="1436423756008"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Covariant return type
    </p>
  </body>
</html>
</richcontent>
<font BOLD="false"/>
</node>
<node TEXT="Use the @Override annotation" STYLE_REF="pre_last_node" ID="ID_1773649452" CREATED="1436420453645" MODIFIED="1436423756009"/>
</node>
<node TEXT="Static Methods" STYLE_REF="gray_node" ID="ID_1730685775" CREATED="1436420485300" MODIFIED="1436422673160">
<node TEXT="If a subclass defines a static method with the same signature as a static method in the superclass" STYLE_REF="pre_last_node" ID="ID_5028156" CREATED="1436420495825" MODIFIED="1436423756009"/>
<node TEXT="Distinction between hiding a static method and overriding an instance method" STYLE_REF="yellow_node" ID="ID_1443936749" CREATED="1436420545941" MODIFIED="1436423784837">
<node TEXT="The version of the overridden instance method that gets invoked is the one in the subclass." STYLE_REF="pre_last_node" ID="ID_1299188158" CREATED="1436420584358" MODIFIED="1436423756010"/>
<node TEXT="The version of the hidden static method that gets invoked depends on whether it is invoked from the superclass or the subclass." STYLE_REF="pre_last_node" ID="ID_878846103" CREATED="1436420584358" MODIFIED="1436423756011"/>
</node>
<node TEXT="Example" STYLE_REF="yellow_node" ID="ID_1521476930" CREATED="1436420651967" MODIFIED="1436423805494">
<node TEXT="public class Animal {&#xa;    public static void testClassMethod() {&#xa;        System.out.println(&quot;The static method in Animal&quot;);&#xa;    }&#xa;    public void testInstanceMethod() {&#xa;        System.out.println(&quot;The instance method in Animal&quot;);&#xa;    }&#xa;}" STYLE_REF="example_node" ID="ID_1754196046" CREATED="1436420654724" MODIFIED="1436423782055"/>
<node TEXT="public class Cat extends Animal {&#xa;    public static void testClassMethod() {&#xa;        System.out.println(&quot;The static method in Cat&quot;);&#xa;    }&#xa;    public void testInstanceMethod() {&#xa;        System.out.println(&quot;The instance method in Cat&quot;);&#xa;    }&#xa;&#xa;    public static void main(String[] args) {&#xa;        Cat myCat = new Cat();&#xa;        Animal myAnimal = myCat;&#xa;        Animal.testClassMethod();&#xa;        myAnimal.testInstanceMethod();&#xa;    }&#xa;}" STYLE_REF="example_node" ID="ID_1981193304" CREATED="1436420667468" MODIFIED="1436423782050"/>
<node TEXT="The static method in Animal&#xa;The instance method in Cat" STYLE_REF="pre_last_node" ID="ID_1640272016" CREATED="1436420753724" MODIFIED="1436423795725"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      output
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="Interface Methods" STYLE_REF="gray_node" ID="ID_1808881753" CREATED="1436420826509" MODIFIED="1436422673160">
<node TEXT="Default methods and abstract methods in interfaces are inherited like instance methods" STYLE_REF="pre_last_node" ID="ID_360967450" CREATED="1436420834166" MODIFIED="1436423756012"/>
<node TEXT="Resolving the name confilcts" STYLE_REF="yellow_node" ID="ID_334457521" CREATED="1436420851866" MODIFIED="1436423761421"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      However, when the supertypes of a class or interface provide multiple default methods with the same signature, the Java compiler follows inheritance rules to resolve the name conflict
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Instance methods are preferred over interface default methods" STYLE_REF="pre_last_node" ID="ID_507773971" CREATED="1436420866032" MODIFIED="1436423756019">
<node TEXT="public class Horse {&#xa;    public String identifyMyself() {&#xa;        return &quot;I am a horse.&quot;;&#xa;    }&#xa;}&#xa;&#xa;public interface Flyer {&#xa;    default public String identifyMyself() {&#xa;        return &quot;I am able to fly.&quot;;&#xa;    }&#xa;}&#xa;&#xa;public interface Mythical {&#xa;    default public String identifyMyself() {&#xa;        return &quot;I am a mythical creature.&quot;;&#xa;    }&#xa;}" STYLE_REF="example_node" ID="ID_1551194538" CREATED="1436420890285" MODIFIED="1436423782046"/>
<node TEXT="public class Pegasus extends Horse implements Flyer, Mythical {&#xa;    public static void main(String... args) {&#xa;        Pegasus myApp = new Pegasus();&#xa;        System.out.println(myApp.identifyMyself());&#xa;    }&#xa;}" STYLE_REF="example_node" ID="ID_1303018235" CREATED="1436420897635" MODIFIED="1436423782044"/>
<node TEXT="I am a horse" STYLE_REF="pre_last_node" ID="ID_692411984" CREATED="1436420920652" MODIFIED="1436423795727"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Output
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Methods that are already overridden by other candidates are ignored. This circumstance can arise when supertypes share a common ancestor" STYLE_REF="pre_last_node" ID="ID_353255840" CREATED="1436421038788" MODIFIED="1436423756016">
<node TEXT="public interface Animal {&#xa;    default public String identifyMyself() {&#xa;        return &quot;I am an animal.&quot;;&#xa;    }&#xa;}&#xa;&#xa;public interface EggLayer extends Animal {&#xa;    default public String identifyMyself() {&#xa;        return &quot;I am able to lay eggs.&quot;;&#xa;    }&#xa;}&#xa;&#xa;public interface FireBreather extends Animal { }" STYLE_REF="example_node" ID="ID_379340093" CREATED="1436421102716" MODIFIED="1436423782041"/>
<node TEXT="public class Dragon implements EggLayer, FireBreather {&#xa;    public static void main (String... args) {&#xa;        Dragon myApp = new Dragon();&#xa;        System.out.println(myApp.identifyMyself());&#xa;    }&#xa;}" STYLE_REF="example_node" ID="ID_1463097266" CREATED="1436421109300" MODIFIED="1436423782040"/>
<node TEXT="I am able to lay eggs" STYLE_REF="pre_last_node" ID="ID_344820587" CREATED="1436421124196" MODIFIED="1436423795727"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Output
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="If two or more independently defined default methods conflict, or a default method conflicts with an abstract method, then the Java compiler produces a compiler error. You must explicitly override the supertype methods." STYLE_REF="pre_last_node" ID="ID_93507790" CREATED="1436421333843" MODIFIED="1436423756015">
<node TEXT="public interface OperateCar {&#xa;    // ...&#xa;    default public int startEngine(EncryptedKey key) {&#xa;        // Implementation&#xa;    }&#xa;}&#xa;&#xa;public interface FlyCar {&#xa;    // ...&#xa;    default public int startEngine(EncryptedKey key) {&#xa;        // Implementation&#xa;    }&#xa;}" STYLE_REF="example_node" ID="ID_859617231" CREATED="1436421355212" MODIFIED="1436423782038"/>
<node TEXT="public class FlyingCar implements OperateCar, FlyCar {&#xa;    // ...&#xa;    public int startEngine(EncryptedKey key) {&#xa;        FlyCar.super.startEngine(key);&#xa;        OperateCar.super.startEngine(key);&#xa;    }&#xa;}" STYLE_REF="example_node" ID="ID_1464143547" CREATED="1436421362612" MODIFIED="1436423782037"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      You could invoke any of the of the default implementations with the super keyword
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Inherited instance methods from classes can override abstract interface methods." STYLE_REF="pre_last_node" ID="ID_288039096" CREATED="1436421394875" MODIFIED="1436423756014">
<node TEXT="public interface Mammal {&#xa;    String identifyMyself();&#xa;}&#xa;&#xa;public class Horse {&#xa;    public String identifyMyself() {&#xa;        return &quot;I am a horse.&quot;;&#xa;    }&#xa;}" STYLE_REF="example_node" ID="ID_149913586" CREATED="1436421412700" MODIFIED="1436423782032"/>
<node TEXT="public class Mustang extends Horse implements Mammal {&#xa;    public static void main(String... args) {&#xa;        Mustang myApp = new Mustang();&#xa;        System.out.println(myApp.identifyMyself());&#xa;    }&#xa;}" STYLE_REF="example_node" ID="ID_497638700" CREATED="1436421419043" MODIFIED="1436423782036"/>
<node TEXT="I am a horse" STYLE_REF="pre_last_node" ID="ID_966826541" CREATED="1436421444467" MODIFIED="1436423795727"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Output
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Static methods in interfaces are never inherited" STYLE_REF="pre_last_node" ID="ID_93477956" CREATED="1436421458964" MODIFIED="1436423756013"/>
</node>
</node>
<node TEXT="Modifiers" STYLE_REF="gray_node" ID="ID_624549054" CREATED="1436421480907" MODIFIED="1436422701343">
<node TEXT="The access specifier for an overriding method can allow more, but not less, access than the overridden method" STYLE_REF="pre_last_node" ID="ID_757585471" CREATED="1436422621417" MODIFIED="1436423767397"/>
<node TEXT="A protected instance method in the superclass can be made public, but not private, in the subclass" STYLE_REF="pre_last_node" ID="ID_1892183936" CREATED="1436422651297" MODIFIED="1436423767399"/>
<node TEXT="What happens when you define a method with the same signature as a method in a superclass" STYLE_REF="pre_last_node" ID="ID_1544367486" CREATED="1436423612839" MODIFIED="1436423767400"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <table summary="This table summarizes what happens when you define a method with the same signature as a method in a superclass" border="1">
      <tr>
        <th id="h1">
          &#160;
        </th>
        <th id="h2">
          Superclass Instance Method
        </th>
        <th id="h3">
          Superclass Static Method
        </th>
      </tr>
      <tr>
        <th id="h4">
          Subclass Instance Method
        </th>
        <td headers="h1">
          Overrides
        </td>
        <td headers="h2">
          Generates a compile-time error
        </td>
      </tr>
      <tr>
        <th id="h5">
          Subclass Static Method
        </th>
        <td headers="h1">
          Generates a compile-time error
        </td>
        <td headers="h2">
          Hides
        </td>
      </tr>
    </table>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="Polymorphism" STYLE_REF="orange_node" ID="ID_692171090" CREATED="1436423825303" MODIFIED="1436425520994">
<node TEXT="The dictionary definition of polymorphism refers to a principle in biology in which an organism or species can have many different forms or stages" STYLE_REF="main_definition" ID="ID_1401784653" CREATED="1436423945799" MODIFIED="1436425528066"/>
<node TEXT="Example" STYLE_REF="yellow_node" ID="ID_622896452" CREATED="1436425412134" MODIFIED="1436429184205">
<node TEXT="public class Bicycle {&#xa;------------&#xa;public void printDescription(){&#xa;    System.out.println(&quot;\nBike is &quot; + &quot;in gear &quot; + this.gear&#xa;        + &quot; with a cadence of &quot; + this.cadence +&#xa;        &quot; and travelling at a speed of &quot; + this.speed + &quot;. &quot;);&#xa;}&#xa;---------&#xa;}" STYLE_REF="example_node" ID="ID_652671944" CREATED="1436425416013" MODIFIED="1436425514482"/>
<node TEXT="public class MountainBike extends Bicycle {&#xa;    private String suspension;&#xa;&#xa;    public MountainBike(&#xa;               int startCadence,&#xa;               int startSpeed,&#xa;               int startGear,&#xa;               String suspensionType){&#xa;        super(startCadence,&#xa;              startSpeed,&#xa;              startGear);&#xa;        this.setSuspension(suspensionType);&#xa;    }&#xa;&#xa;    public String getSuspension(){&#xa;      return this.suspension;&#xa;    }&#xa;&#xa;    public void setSuspension(String suspensionType) {&#xa;        this.suspension = suspensionType;&#xa;    }&#xa;&#xa;    public void printDescription() {&#xa;        super.printDescription();&#xa;        System.out.println(&quot;The &quot; + &quot;MountainBike has a&quot; +&#xa;            getSuspension() + &quot; suspension.&quot;);&#xa;    }&#xa;}" STYLE_REF="example_node" ID="ID_329984954" CREATED="1436425462575" MODIFIED="1436425514474"/>
<node TEXT="public class RoadBike extends Bicycle{&#xa;    // In millimeters (mm)&#xa;    private int tireWidth;&#xa;&#xa;    public RoadBike(int startCadence,&#xa;                    int startSpeed,&#xa;                    int startGear,&#xa;                    int newTireWidth){&#xa;        super(startCadence,&#xa;              startSpeed,&#xa;              startGear);&#xa;        this.setTireWidth(newTireWidth);&#xa;    }&#xa;&#xa;    public int getTireWidth(){&#xa;      return this.tireWidth;&#xa;    }&#xa;&#xa;    public void setTireWidth(int newTireWidth){&#xa;        this.tireWidth = newTireWidth;&#xa;    }&#xa;&#xa;    public void printDescription(){&#xa;        super.printDescription();&#xa;        System.out.println(&quot;The RoadBike&quot; + &quot; has &quot; + getTireWidth() +&#xa;            &quot; MM tires.&quot;);&#xa;    }&#xa;}" STYLE_REF="example_node" ID="ID_1734651857" CREATED="1436425474605" MODIFIED="1436429184203"/>
<node TEXT="public class TestBikes {&#xa;  public static void main(String[] args){&#xa;    Bicycle bike01, bike02, bike03;&#xa;&#xa;    bike01 = new Bicycle(20, 10, 1);&#xa;    bike02 = new MountainBike(20, 10, 5, &quot;Dual&quot;);&#xa;    bike03 = new RoadBike(40, 20, 8, 23);&#xa;&#xa;    bike01.printDescription();&#xa;    bike02.printDescription();&#xa;    bike03.printDescription();&#xa;  }&#xa;}" STYLE_REF="example_node" ID="ID_1304887362" CREATED="1436425489485" MODIFIED="1436425514466"/>
<node TEXT="Bike is in gear 1 with a cadence of 20 and travelling at a speed of 10.&#xa;&#xa;Bike is in gear 5 with a cadence of 20 and travelling at a speed of 10.&#xa;The MountainBike has a Dual suspension.&#xa;&#xa;Bike is in gear 8 with a cadence of 40 and travelling at a speed of 20.&#xa;The RoadBike has 23 MM tires." STYLE_REF="yellow_node" ID="ID_342910640" CREATED="1436425498813" MODIFIED="1436425580803"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Output
    </p>
  </body>
</html>
</richcontent>
<node TEXT="(JVM) calls the appropriate method for the object that is referred to in each variable. It does not call the method that is defined by the variable&apos;s type" STYLE_REF="pre_last_node" ID="ID_1496385479" CREATED="1436425555708" MODIFIED="1436425592541"/>
<node STYLE_REF="pre_last_node" ID="ID_289283917" CREATED="1436425575405" MODIFIED="1436425684351"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      This behavior is referred to as <b>virtual method invocation</b>&#160;and demonstrates an aspect of the important polymorphism features in the Java language
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
</node>
<node TEXT="Hiding Fields" STYLE_REF="orange_node" ID="ID_1509239021" CREATED="1436425773558" MODIFIED="1436432397114">
<node TEXT="Within a class, a field that has the same name as a field in the superclass hides the superclass&apos;s field, even if their types are different." STYLE_REF="pre_last_node" ID="ID_196497115" CREATED="1436425780918" MODIFIED="1436425816243"/>
<node TEXT="Within the subclass, the field in the superclass cannot be referenced by its simple name. Instead, the field must be accessed through super." STYLE_REF="pre_last_node" ID="ID_978826138" CREATED="1436425790989" MODIFIED="1436432397114"/>
<node TEXT="We don&apos;t recommend hiding fields as it makes code difficult to read" STYLE_REF="pre_last_node" ID="ID_107679395" CREATED="1436425798396" MODIFIED="1436425816242"/>
</node>
<node TEXT="Keyword super" STYLE_REF="orange_node" ID="ID_12889841" CREATED="1436429251369" MODIFIED="1436429283861">
<node TEXT="Invocation of a superclass constructor must be the first line in the subclass constructor." STYLE_REF="pre_last_node" ID="ID_1180006767" CREATED="1436429270664" MODIFIED="1436429288133"/>
<node TEXT="If a subclass constructor invokes a constructor of its superclass, either explicitly or implicitly, you might think that there will be a whole chain of constructors called, all the way back to the constructor of Object. In fact, this is the case" STYLE_REF="pre_last_node" ID="ID_1201475855" CREATED="1436429343091" MODIFIED="1436429414037"/>
<node TEXT="public MountainBike(int startHeight,&#xa;                    int startCadence,&#xa;                    int startSpeed,&#xa;                    int startGear) {&#xa;    super(startCadence, startSpeed, startGear);&#xa;    seatHeight = startHeight;&#xa;}" STYLE_REF="example_node" ID="ID_1151422514" CREATED="1436429279648" MODIFIED="1436429292877"/>
</node>
<node TEXT="Final Classes and Methods" STYLE_REF="orange_node" ID="ID_1519379273" CREATED="1436430702815" MODIFIED="1436430930852">
<node TEXT="Methods" STYLE_REF="gray_node" ID="ID_1456025520" CREATED="1436431049574" MODIFIED="1436431604938">
<node TEXT="You use the final keyword in a method declaration to indicate that the method cannot be overridden by subclasses" STYLE_REF="pre_last_node" ID="ID_1608579680" CREATED="1436430946709" MODIFIED="1436431079173"/>
<node TEXT="You might wish to make a method final if it has an implementation that should not be changed and it is critical to the consistent state of the object." STYLE_REF="pre_last_node" ID="ID_1761253504" CREATED="1436430970600" MODIFIED="1436431079171"/>
<node TEXT="Methods called from constructors should generally be declared final. If a constructor calls a non-final method, a subclass may redefine that method with surprising or undesirable results." STYLE_REF="pre_last_node" ID="ID_404434319" CREATED="1436431109918" MODIFIED="1436431114971"/>
<node TEXT="class ChessAlgorithm {&#xa;    enum ChessPlayer { WHITE, BLACK }&#xa;    ...&#xa;    final ChessPlayer getFirstPlayer() {&#xa;        return ChessPlayer.WHITE;&#xa;    }&#xa;    ...&#xa;}" STYLE_REF="example_node" ID="ID_136881455" CREATED="1436430984935" MODIFIED="1436431604937"/>
</node>
<node TEXT="Classes" STYLE_REF="gray_node" ID="ID_1309270736" CREATED="1436431033661" MODIFIED="1436431606930">
<node TEXT="A class that is declared final cannot be subclassed" STYLE_REF="pre_last_node" ID="ID_665025610" CREATED="1436431037062" MODIFIED="1436431079175"/>
<node TEXT="This is particularly useful, for example, when creating an immutable class like the String class" STYLE_REF="pre_last_node" ID="ID_1959180975" CREATED="1436431043978" MODIFIED="1436431606930"/>
</node>
</node>
</node>
<node TEXT="Interfaces" STYLE_REF="second_node" POSITION="right" ID="ID_88778093" CREATED="1436432606021" MODIFIED="1436432631342">
<edge COLOR="#7c007c"/>
<hook URI="../../Images/interface-2.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Interface is a reference type" STYLE_REF="main_definition" ID="ID_222233426" CREATED="1436433525978" MODIFIED="1436435890037"/>
<node TEXT="Interfaces cannot be instantiated&#x2014;they can only be implemented by classes or extended by other interfaces" STYLE_REF="main_definition" ID="ID_1225996289" CREATED="1436433616397" MODIFIED="1436435890036"/>
<node TEXT="Defining an Interface" STYLE_REF="orange_node" ID="ID_288217160" CREATED="1436435154552" MODIFIED="1436435779428">
<node TEXT="An interface declaration consists of modifiers, the keyword interface, the interface name, a comma-separated list of parent interfaces (if any), and the interface body" STYLE_REF="pre_last_node" ID="ID_1382854799" CREATED="1436435212449" MODIFIED="1436435765202"/>
<node TEXT="public interface GroupedInterface extends Interface1, Interface2, Interface3 {&#xa;&#xa;    // constant declarations&#xa;   &#xa;    // base of natural logarithms&#xa;    double E = 2.718282;&#xa; &#xa;    // method signatures&#xa;    void doSomething (int i, double x);&#xa;    int doSomethingElse(String s);&#xa;}" STYLE_REF="example_node" ID="ID_979027135" CREATED="1436435195129" MODIFIED="1436435748974"/>
<node TEXT="The public access specifier indicates that the interface can be used by any class in any package. If you do not specify that the interface is public, then your interface is accessible only to classes defined in the same package as the interface." STYLE_REF="pre_last_node" ID="ID_1517898673" CREATED="1436435238568" MODIFIED="1436435765201"/>
<node TEXT="An interface can extend other interfaces, just as a class subclass or extend another class. However, whereas a class can extend only one other class, an interface can extend any number of interfaces." STYLE_REF="pre_last_node" ID="ID_1123034342" CREATED="1436435266562" MODIFIED="1436435765200"/>
</node>
<node TEXT="The Interface Body" STYLE_REF="orange_node" ID="ID_611608072" CREATED="1436435407554" MODIFIED="1436435779428">
<node TEXT="Interface can contain only" STYLE_REF="yellow_node" ID="ID_1320229750" CREATED="1436433551326" MODIFIED="1436435785141">
<node TEXT="method signatures (abstract)" STYLE_REF="pre_last_node" ID="ID_1251494568" CREATED="1436433559589" MODIFIED="1436435765199"/>
<node STYLE_REF="pre_last_node" ID="ID_1064643425" CREATED="1436433565237" MODIFIED="1436435765199"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      default methods
    </p>
  </body>
</html>
</richcontent>
</node>
<node STYLE_REF="pre_last_node" ID="ID_110429687" CREATED="1436433570741" MODIFIED="1436435765198"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      static methods
    </p>
  </body>
</html>
</richcontent>
</node>
<node STYLE_REF="pre_last_node" ID="ID_1891626906" CREATED="1436433575993" MODIFIED="1436435765198"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      nested types
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="constants" STYLE_REF="pre_last_node" ID="ID_429128506" CREATED="1436433577699" MODIFIED="1436435765198"/>
</node>
<node TEXT="All abstract, default, and static methods in an interface are implicitly public, so you can omit the public modifier." STYLE_REF="pre_last_node" ID="ID_79262343" CREATED="1436435345960" MODIFIED="1436435765197"/>
<node TEXT="An interface can contain constant declarations. All constant values defined in an interface are implicitly public, static, and final." STYLE_REF="pre_last_node" ID="ID_945595315" CREATED="1436435426114" MODIFIED="1436435765197"/>
</node>
<node TEXT="Implementing" STYLE_REF="orange_node" ID="ID_172672721" CREATED="1436433691194" MODIFIED="1436435795655">
<node TEXT="To use an interface, you write a class that implements the interface" STYLE_REF="pre_last_node" ID="ID_1053486356" CREATED="1436433695629" MODIFIED="1436435765192"/>
<node TEXT="When an instantiable class implements an interface, it provides a method body for each of the methods declared in the interface." STYLE_REF="pre_last_node" ID="ID_1851040305" CREATED="1436433703517" MODIFIED="1436435765181"/>
<node TEXT="public interface Relatable {&#xa;       &#xa;    // this (object calling isLargerThan)&#xa;    // and other must be instances of&#xa;    // the same class returns 1, 0, -1&#xa;    // if this is greater than,&#xa;    // equal to, or less than other&#xa;    public int isLargerThan(Relatable other);&#xa;}" STYLE_REF="example_node" ID="ID_343972017" CREATED="1436433750850" MODIFIED="1436435748969"/>
<node TEXT="public class RectanglePlus&#xa;    implements Relatable {&#xa;    public int width = 0;&#xa;    public int height = 0;&#xa;    public Point origin;&#xa;&#xa;    // four constructors&#xa;    public RectanglePlus() {&#xa;        origin = new Point(0, 0);&#xa;    }&#xa;    public RectanglePlus(Point p) {&#xa;        origin = p;&#xa;    }&#xa;    public RectanglePlus(int w, int h) {&#xa;        origin = new Point(0, 0);&#xa;        width = w;&#xa;        height = h;&#xa;    }&#xa;    public RectanglePlus(Point p, int w, int h) {&#xa;        origin = p;&#xa;        width = w;&#xa;        height = h;&#xa;    }&#xa;&#xa;    // a method for moving the rectangle&#xa;    public void move(int x, int y) {&#xa;        origin.x = x;&#xa;        origin.y = y;&#xa;    }&#xa;&#xa;    // a method for computing&#xa;    // the area of the rectangle&#xa;    public int getArea() {&#xa;        return width * height;&#xa;    }&#xa;   &#xa;    // a method required to implement&#xa;    // the Relatable interface&#xa;    public int isLargerThan(Relatable other) {&#xa;        RectanglePlus otherRect&#xa;            = (RectanglePlus)other;&#xa;        if (this.getArea() &lt; otherRect.getArea())&#xa;            return -1;&#xa;        else if (this.getArea() &gt; otherRect.getArea())&#xa;            return 1;&#xa;        else&#xa;            return 0;              &#xa;    }&#xa;}" STYLE_REF="example_node" ID="ID_1488738946" CREATED="1436435735062" MODIFIED="1436435795654"/>
</node>
<node TEXT="Using an Interface as a Type" STYLE_REF="orange_node" ID="ID_762306967" CREATED="1436435870274" MODIFIED="1436436156164">
<node TEXT="If you define a reference variable whose type is an interface, any object you assign to it must be an instance of a class that implements the interface." STYLE_REF="pre_last_node" ID="ID_1660306870" CREATED="1436436076256" MODIFIED="1436436169768"/>
<node TEXT="When they implement certain interface, they can be of both their own class (or superclass) type and a interface type" STYLE_REF="pre_last_node" ID="ID_681374905" CREATED="1436436054678" MODIFIED="1436436169766"/>
<node TEXT="public Object findLargest(Object object1, Object object2) {&#xa;   Relatable obj1 = (Relatable)object1;&#xa;   Relatable obj2 = (Relatable)object2;&#xa;   if ((obj1).isLargerThan(obj2) &gt; 0)&#xa;      return object1;&#xa;   else&#xa;      return object2;&#xa;}" STYLE_REF="example_node" ID="ID_1802686631" CREATED="1436436117815" MODIFIED="1436436159435">
<node TEXT="By casting object1 to a Relatable type, it can invoke the isLargerThan method." STYLE_REF="pre_last_node" ID="ID_772923186" CREATED="1436436128138" MODIFIED="1436436169765"/>
</node>
</node>
<node TEXT="Evolving Interfaces" STYLE_REF="orange_node" ID="ID_404994067" CREATED="1436436144102" MODIFIED="1436436156164">
<node TEXT="Consider an interface that you have developed called DoIt" STYLE_REF="pre_last_node" ID="ID_1931745280" CREATED="1436436320766" MODIFIED="1436436629108"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      public interface DoIt {
    </p>
    <p>
      &#160;&#160;&#160;void doSomething(int i, double x);
    </p>
    <p>
      &#160;&#160;&#160;int doSomethingElse(String s);
    </p>
    <p>
      }
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Bad practice" STYLE_REF="gray_node" ID="ID_101809306" CREATED="1436436352791" MODIFIED="1436436619091">
<node TEXT="Suppose that, at a later time, you want to add a third method to DoIt" STYLE_REF="pre_last_node" ID="ID_1122537129" CREATED="1436436342094" MODIFIED="1436436629108"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      public interface DoIt {
    </p>
    <p>
      &#160;&#160;&#160;void doSomething(int i, double x);
    </p>
    <p>
      &#160;&#160;&#160;int doSomethingElse(String s);
    </p>
    <p>
      &#160;&#160;&#160;boolean didItWork(int i, double x, String s);&#160;&#160;
    </p>
    <p>
      }
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="All classes that implement the old DoIt interface will break because they no longer implement the old interface" STYLE_REF="pre_last_node" ID="ID_907604081" CREATED="1436436389342" MODIFIED="1436436629109"/>
</node>
<node TEXT="Good practice" STYLE_REF="gray_node" ID="ID_812504786" CREATED="1436436363941" MODIFIED="1436436619092">
<node TEXT="You could create a DoItPlus interface that extends DoIt" STYLE_REF="pre_last_node" ID="ID_1194151381" CREATED="1436436426055" MODIFIED="1436436629109"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      public interface DoItPlus extends DoIt {
    </p>
    <p>
      &#160;&#160;&#160;boolean didItWork(int i, double x, String s);&#160;&#160;
    </p>
    <p>
      }
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Now users of your code can choose to continue to use the old interface or to upgrade to the new interface" ID="ID_1289859594" CREATED="1436436593457" MODIFIED="1436436593457"/>
</node>
<node TEXT="You can define your new methods as default methods" STYLE_REF="pre_last_node" ID="ID_1243216280" CREATED="1436436584375" MODIFIED="1436436629110"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      public interface DoIt {
    </p>
    <p>
      &#160;&#160;&#160;void doSomething(int i, double x);
    </p>
    <p>
      &#160;&#160;&#160;int doSomethingElse(String s);
    </p>
    <p>
      &#160;&#160;&#160;default boolean didItWork(int i, double x, String s) {
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;&#160;// Method body
    </p>
    <p>
      &#160;&#160;&#160;}&#160;&#160;
    </p>
    <p>
      }
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="Default Methods" STYLE_REF="orange_node" ID="ID_1182926243" CREATED="1436436151531" MODIFIED="1436436156163">
<node TEXT="Default methods enable you to add new functionality to the interfaces of your libraries and ensure binary compatibility with code written for older versions of those interfaces" STYLE_REF="pre_last_node" ID="ID_25819400" CREATED="1436437783533" MODIFIED="1436899018483"/>
<node TEXT="Example" STYLE_REF="yellow_node" ID="ID_1540600785" CREATED="1436437787124" MODIFIED="1436899032219">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="80" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_1243216280" STARTINCLINATION="572;0;" ENDINCLINATION="537;0;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
</node>
<node TEXT="You specify that a method definition in an interface is a default method with the default keyword at the beginning of the method signature. All method declarations in an interface, including default methods, are implicitly public, so you can omit the public modifier." STYLE_REF="pre_last_node" ID="ID_343997576" CREATED="1436437817381" MODIFIED="1436899018485"/>
</node>
</node>
<node TEXT="Classes" STYLE_REF="second_node" POSITION="right" ID="ID_198890868" CREATED="1437370856076" MODIFIED="1437370907546">
<edge COLOR="#007c7c"/>
<node TEXT="Nested Classes" STYLE_REF="orange_node" ID="ID_1762787843" CREATED="1437370865715" MODIFIED="1437370918029">
<node TEXT="Static" STYLE_REF="gray_node" ID="ID_1235127037" CREATED="1437371002234" MODIFIED="1437373855522">
<node TEXT="class OuterClass {&#xa;    ...&#xa;    static class StaticNestedClass {&#xa;        ...&#xa;    }&#xa;}" STYLE_REF="example_node" ID="ID_1827886254" CREATED="1437371020986" MODIFIED="1437373877979"/>
<node TEXT="Static nested classes do not have access to other members of the enclosing class" STYLE_REF="pre_last_node" ID="ID_1550680012" CREATED="1437371092866" MODIFIED="1437373918339"/>
<node TEXT="Static nested classes are accessed using the enclosing class name:" STYLE_REF="pre_last_node" ID="ID_1671419500" CREATED="1437371318323" MODIFIED="1437373918355"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      OuterClass.StaticNestedClass
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="OuterClass.StaticNestedClass nestedObject =&#xa;     new OuterClass.StaticNestedClass();" STYLE_REF="example_node" ID="ID_358127782" CREATED="1437371334971" MODIFIED="1437373877983"/>
</node>
<node TEXT="Non-static" STYLE_REF="gray_node" ID="ID_1814269988" CREATED="1437371005707" MODIFIED="1437373855524">
<node TEXT="class OuterClass {&#xa;    ...&#xa;    class InnerClass {&#xa;        ...&#xa;    }&#xa;}" STYLE_REF="example_node" ID="ID_1798857623" CREATED="1437371028738" MODIFIED="1437373889551">
<node TEXT="To instantiate an inner class, you must first instantiate the outer class. Then, create the inner object within the outer object with this syntax:" STYLE_REF="pre_last_node" ID="ID_488862535" CREATED="1437371430634" MODIFIED="1437373918339"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      OuterClass.InnerClass innerObject = outerObject.new InnerClass();
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="As a member of the OuterClass, a nested class can be declared private, public, protected, or package private" STYLE_REF="pre_last_node" ID="ID_682909469" CREATED="1437371110858" MODIFIED="1437373918355"/>
</node>
<node TEXT="Non-static nested classes (inner classes) have access to other members of the enclosing class, even if they are declared private." STYLE_REF="pre_last_node" ID="ID_453521287" CREATED="1437371071050" MODIFIED="1437373918339"/>
<node TEXT="It cannot define any static members itself" STYLE_REF="pre_last_node" ID="ID_1633604403" CREATED="1437371379282" MODIFIED="1437373918339"/>
<node TEXT="Types" STYLE_REF="yellow_node" ID="ID_382259456" CREATED="1437371457194" MODIFIED="1437373969274">
<node TEXT="Local classes" STYLE_REF="gray_node" ID="ID_1121094559" CREATED="1437371459521" MODIFIED="1437373997008">
<node TEXT="You typically find local classes defined in the body of a method, but it can be defined inside every {}" STYLE_REF="main_definition" ID="ID_305560237" CREATED="1437372997559" MODIFIED="1437379321130"/>
<node TEXT="Local class can only access local variables that are declared final (captured variable)" STYLE_REF="pre_last_node" ID="ID_652933202" CREATED="1437373237831" MODIFIED="1437374031906"/>
<node TEXT="Local classes are non-static because they have access to instance members of the enclosing block." STYLE_REF="pre_last_node" ID="ID_1090901390" CREATED="1437373521886" MODIFIED="1437374031906"/>
<node TEXT="You cannot declare an interface inside a block; interfaces are inherently static" STYLE_REF="pre_last_node" ID="ID_108610251" CREATED="1437373541862" MODIFIED="1437374031906"/>
<node TEXT="In JRE &gt;= 8  local class can access local variables and parameters of the enclosing block that are final or effectively final" STYLE_REF="pre_last_node" ID="ID_921379027" CREATED="1437373325926" MODIFIED="1437374031906">
<node TEXT="A variable or parameter whose value is never changed after it is initialized is effectively final" ID="ID_292989728" CREATED="1437373343776" MODIFIED="1437373343776"/>
<node STYLE_REF="example_node" ID="ID_1544963932" CREATED="1437373369143" MODIFIED="1437374012340"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      int numberLength = 10;
    </p>
    <p>
      PhoneNumber(String phoneNumber) {
    </p>
    <p>
      &#160;&#160;&#160;&#160;<b>numberLength = 7;</b>
    </p>
    <p>
      &#160;&#160;&#160;&#160;String currentNumber = phoneNumber.replaceAll(
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;regularExpression, &quot;&quot;);
    </p>
    <p>
      &#160;&#160;&#160;&#160;if (currentNumber.length() == numberLength)
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;formattedPhoneNumber = currentNumber;
    </p>
    <p>
      &#160;&#160;&#160;&#160;else
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;formattedPhoneNumber = null;
    </p>
    <p>
      }
    </p>
  </body>
</html>

</richcontent>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      The Java compiler generates an error message similar to &quot;local variables referenced from an inner class must be final or effectively final&quot;
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
<node TEXT="You cannot declare static members or methods" STYLE_REF="pre_last_node" ID="ID_869873767" CREATED="1437373731925" MODIFIED="1437374031922">
<node TEXT="public void sayGoodbyeInEnglish() {&#xa;        class EnglishGoodbye {&#xa;            public static void sayGoodbye() {&#xa;                System.out.println(&quot;Bye bye&quot;);&#xa;            }&#xa;        }&#xa;        EnglishGoodbye.sayGoodbye();&#xa;    }" STYLE_REF="example_node" ID="ID_179249621" CREATED="1437373743158" MODIFIED="1437373877984"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Compile time error
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
<node TEXT="A local class can have static members provided that they are constant variables" STYLE_REF="pre_last_node" ID="ID_1024355972" CREATED="1437373770493" MODIFIED="1437374031906">
<node TEXT="public void sayGoodbyeInEnglish() {&#xa;        class EnglishGoodbye {&#xa;            public static final String farewell = &quot;Bye bye&quot;;&#xa;            public void sayGoodbye() {&#xa;                System.out.println(farewell);&#xa;            }&#xa;        }&#xa;        EnglishGoodbye myEnglishGoodbye = new EnglishGoodbye();&#xa;        myEnglishGoodbye.sayGoodbye();&#xa;    }" STYLE_REF="example_node" ID="ID_1375119172" CREATED="1437373785310" MODIFIED="1437373877988"/>
</node>
<node TEXT="Example" STYLE_REF="yellow_node" ID="ID_367446587" CREATED="1437373082903" MODIFIED="1437374027781">
<node TEXT="public class LocalClassExample {&#xa; &#xa;    static String regularExpression = &quot;[^0-9]&quot;;&#xa; &#xa;    public static void validatePhoneNumber(String phoneNumber1, String phoneNumber2) {&#xa;     &#xa;        final int numberLength = 10;&#xa;       &#xa;        // Valid in JDK 8 and later:      &#xa;        // int numberLength = 10;&#xa;      &#xa;        class PhoneNumber {&#xa;           &#xa;            String formattedPhoneNumber = null;&#xa;&#xa;            PhoneNumber(String phoneNumber){&#xa;                // numberLength = 7;&#xa;                String currentNumber = phoneNumber.replaceAll(&#xa;                  regularExpression, &quot;&quot;);&#xa;                if (currentNumber.length() == numberLength)&#xa;                    formattedPhoneNumber = currentNumber;&#xa;                else&#xa;                    formattedPhoneNumber = null;&#xa;            }&#xa;&#xa;            public String getNumber() {&#xa;                return formattedPhoneNumber;&#xa;            }&#xa;           &#xa;            // Valid in JDK 8 and later:&#xa;&#xa;//            public void printOriginalNumbers() {&#xa;//                System.out.println(&quot;Original numbers are &quot; + phoneNumber1 +&#xa;//                    &quot; and &quot; + phoneNumber2);&#xa;//            }&#xa;        }&#xa;&#xa;        PhoneNumber myNumber1 = new PhoneNumber(phoneNumber1);&#xa;        PhoneNumber myNumber2 = new PhoneNumber(phoneNumber2);&#xa;       &#xa;        // Valid in JDK 8 and later:&#xa;&#xa;//        myNumber1.printOriginalNumbers();&#xa;&#xa;        if (myNumber1.getNumber() == null)&#xa;            System.out.println(&quot;First number is invalid&quot;);&#xa;        else&#xa;            System.out.println(&quot;First number is &quot; + myNumber1.getNumber());&#xa;        if (myNumber2.getNumber() == null)&#xa;            System.out.println(&quot;Second number is invalid&quot;);&#xa;        else&#xa;            System.out.println(&quot;Second number is &quot; + myNumber2.getNumber());&#xa;&#xa;    }&#xa;&#xa;    public static void main(String... args) {&#xa;        validatePhoneNumber(&quot;123-456-7890&quot;, &quot;456-7890&quot;);&#xa;    }&#xa;}" STYLE_REF="example_node" ID="ID_521676249" CREATED="1437373085144" MODIFIED="1437374019829"/>
</node>
</node>
<node TEXT="Anonymous Classes" STYLE_REF="gray_node" ID="ID_1174825570" CREATED="1437371476010" MODIFIED="1437373997008">
<node TEXT="Use them if you need to use a local class only once" STYLE_REF="main_definition" ID="ID_168610232" CREATED="1437378234413" MODIFIED="1437379667884"/>
<node TEXT="While local classes are class declarations, anonymous classes are expressions, which means that you define the class in another expression" STYLE_REF="main_definition" ID="ID_819056075" CREATED="1437378268926" MODIFIED="1437379667884"/>
<node TEXT="Like local classes, anonymous classes can capture variables; they have the same access to local variables of the enclosing scope" STYLE_REF="pre_last_node" ID="ID_750504557" CREATED="1437379406899" MODIFIED="1437379620756"/>
<node TEXT="You cannot declare constructors in an anonymous class" STYLE_REF="pre_last_node" ID="ID_1739628398" CREATED="1437379382043" MODIFIED="1437379620756"/>
<node TEXT="The anonymous expression syntax" STYLE_REF="yellow_node" ID="ID_1046815967" CREATED="1437378510509" MODIFIED="1437379602604">
<node TEXT="The new operator" STYLE_REF="pre_last_node" ID="ID_755758163" CREATED="1437378516853" MODIFIED="1437379620772"/>
<node TEXT="The name of an interface to implement or a class to extend." STYLE_REF="pre_last_node" ID="ID_1552836418" CREATED="1437378536908" MODIFIED="1437379620772"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      In this example, the anonymous class is implementing the interface HelloWorld.
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="Parentheses that contain the arguments to a constructor, just like a normal class instance creation expression." STYLE_REF="pre_last_node" ID="ID_1133016263" CREATED="1437378552300" MODIFIED="1437379620772"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Note: When you implement an interface, there is no constructor, so you use an empty pair of parentheses, as in this example.
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="A body, which is a class declaration body. More specifically, in the body, method declarations are allowed but statements are not." STYLE_REF="pre_last_node" ID="ID_113283315" CREATED="1437378578156" MODIFIED="1437379620772"/>
</node>
<node TEXT="You can declare the following in anonymous classes" STYLE_REF="yellow_node" ID="ID_1915107347" CREATED="1437379427083" MODIFIED="1437379625947">
<node TEXT="Fields" STYLE_REF="pre_last_node" ID="ID_497646655" CREATED="1437379433099" MODIFIED="1437379636664"/>
<node TEXT="Extra methods (even if they do not implement any methods of the supertype)" STYLE_REF="pre_last_node" ID="ID_1055471118" CREATED="1437379433099" MODIFIED="1437379636664"/>
<node TEXT="Instance initializers" STYLE_REF="pre_last_node" ID="ID_1491933776" CREATED="1437379433099" MODIFIED="1437379636664"/>
<node TEXT="Local classes" STYLE_REF="pre_last_node" ID="ID_699614140" CREATED="1437379433099" MODIFIED="1437379636664"/>
</node>
<node TEXT="Example" STYLE_REF="yellow_node" ID="ID_901171545" CREATED="1437378281101" MODIFIED="1437379598518">
<node TEXT="public class HelloWorldAnonymousClasses {&#xa; &#xa;    interface HelloWorld {&#xa;        public void greet();&#xa;        public void greetSomeone(String someone);&#xa;    }&#xa; &#xa;    public void sayHello() {&#xa;       // local class&#xa;        class EnglishGreeting implements HelloWorld {&#xa;            String name = &quot;world&quot;;&#xa;            public void greet() {&#xa;                greetSomeone(&quot;world&quot;);&#xa;            }&#xa;            public void greetSomeone(String someone) {&#xa;                name = someone;&#xa;                System.out.println(&quot;Hello &quot; + name);&#xa;            }&#xa;        }&#xa;     &#xa;        HelloWorld englishGreeting = new EnglishGreeting();&#xa;&#xa;        // anonymous class&#xa;        HelloWorld frenchGreeting = new HelloWorld() {&#xa;            String name = &quot;tout le monde&quot;;&#xa;            public void greet() {&#xa;                greetSomeone(&quot;tout le monde&quot;);&#xa;            }&#xa;            public void greetSomeone(String someone) {&#xa;                name = someone;&#xa;                System.out.println(&quot;Salut &quot; + name);&#xa;            }&#xa;        };&#xa;       &#xa;        // anonymous class&#xa;        HelloWorld spanishGreeting = new HelloWorld() {&#xa;            String name = &quot;mundo&quot;;&#xa;            public void greet() {&#xa;                greetSomeone(&quot;mundo&quot;);&#xa;            }&#xa;            public void greetSomeone(String someone) {&#xa;                name = someone;&#xa;                System.out.println(&quot;Hola, &quot; + name);&#xa;            }&#xa;        };&#xa;        englishGreeting.greet();&#xa;        frenchGreeting.greetSomeone(&quot;Fred&quot;);&#xa;        spanishGreeting.greet();&#xa;    }&#xa;&#xa;    public static void main(String... args) {&#xa;        HelloWorldAnonymousClasses myApp =&#xa;            new HelloWorldAnonymousClasses();&#xa;        myApp.sayHello();&#xa;    }           &#xa;}" STYLE_REF="example_node" ID="ID_34623937" CREATED="1437378283300" MODIFIED="1437378641283"/>
<node TEXT="  final TextField sum = new TextField() {&#xa;            @Override&#xa;            public void replaceText(int start, int end, String text) {&#xa;                if (!text.matches(&quot;[a-z, A-Z]&quot;)) {&#xa;                    super.replaceText(start, end, text);                    &#xa;                }&#xa;                label.setText(&quot;Enter a numeric value&quot;);&#xa;            }&#xa; &#xa;            @Override&#xa;            public void replaceSelection(String text) {&#xa;                if (!text.matches(&quot;[a-z, A-Z]&quot;)) {&#xa;                    super.replaceSelection(text);&#xa;                }&#xa;            }&#xa;        };" STYLE_REF="example_node" ID="ID_1244200930" CREATED="1437379575835" MODIFIED="1437379593800"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      It redefines the default implementation of the TextField class with an anonymous class by overriding the replaceText and replaceSelection methods inherited from the TextInputControl class
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
</node>
</node>
<node TEXT="Shadowing" STYLE_REF="yellow_node" ID="ID_914523564" CREATED="1437371600618" MODIFIED="1437373969274">
<node TEXT="If a declaration of a type has the same name as another declaration in the enclosing scope, then the declaration shadows the declaration of the enclosing scope" STYLE_REF="pre_last_node" ID="ID_1010514899" CREATED="1437371789209" MODIFIED="1437373990745"/>
<node TEXT="public class ShadowTest {&#xa;&#xa;    public int x = 0;&#xa;&#xa;    class FirstLevel {&#xa;&#xa;        public int x = 1;&#xa;&#xa;        void methodInFirstLevel(int x) {&#xa;            System.out.println(&quot;x = &quot; + x);&#xa;            System.out.println(&quot;this.x = &quot; + this.x);&#xa;            System.out.println(&quot;ShadowTest.this.x = &quot; + ShadowTest.this.x);&#xa;        }&#xa;    }&#xa;&#xa;    public static void main(String... args) {&#xa;        ShadowTest st = new ShadowTest();&#xa;        ShadowTest.FirstLevel fl = st.new FirstLevel();&#xa;        fl.methodInFirstLevel(23);&#xa;    }&#xa;}" STYLE_REF="example_node" ID="ID_1991331011" CREATED="1437371607906" MODIFIED="1437373983580"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      x = 23
    </p>
    <p>
      this.x = 1
    </p>
    <p>
      ShadowTest.this.x = 0
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
<node TEXT="Example" STYLE_REF="yellow_node" ID="ID_1392664979" CREATED="1437372894271" MODIFIED="1437373969274">
<node TEXT="public class DataStructure {&#xa;   &#xa;    // Create an array&#xa;    private final static int SIZE = 15;&#xa;    private int[] arrayOfInts = new int[SIZE];&#xa;   &#xa;    public DataStructure() {&#xa;        // fill the array with ascending integer values&#xa;        for (int i = 0; i &lt; SIZE; i++) {&#xa;            arrayOfInts[i] = i;&#xa;        }&#xa;    }&#xa;   &#xa;    public void printEven() {&#xa;       &#xa;        // Print out values of even indices of the array&#xa;        DataStructureIterator iterator = this.new EvenIterator();&#xa;        while (iterator.hasNext()) {&#xa;            System.out.print(iterator.next() + &quot; &quot;);&#xa;        }&#xa;        System.out.println();&#xa;    }&#xa;   &#xa;    interface DataStructureIterator extends java.util.Iterator&lt;Integer&gt; { }&#xa;&#xa;    // Inner class implements the DataStructureIterator interface,&#xa;    // which extends the Iterator&lt;Integer&gt; interface&#xa;   &#xa;    private class EvenIterator implements DataStructureIterator {&#xa;       &#xa;        // Start stepping through the array from the beginning&#xa;        private int nextIndex = 0;&#xa;       &#xa;        public boolean hasNext() {&#xa;           &#xa;            // Check if the current element is the last in the array&#xa;            return (nextIndex &lt;= SIZE - 1);&#xa;        }       &#xa;       &#xa;        public Integer next() {&#xa;           &#xa;            // Record a value of an even index of the array&#xa;            Integer retValue = Integer.valueOf(arrayOfInts[nextIndex]);&#xa;           &#xa;            // Get the next even element&#xa;            nextIndex += 2;&#xa;            return retValue;&#xa;        }&#xa;    }&#xa;   &#xa;    public static void main(String s[]) {&#xa;       &#xa;        // Fill the array with integer values and print out only&#xa;        // values of even indices&#xa;        DataStructure ds = new DataStructure();&#xa;        ds.printEven();&#xa;    }&#xa;}" STYLE_REF="example_node" ID="ID_1001294446" CREATED="1437372897800" MODIFIED="1437372904864"/>
</node>
</node>
<node TEXT="Why Use?" STYLE_REF="gray_node" ID="ID_1621733088" CREATED="1437371123530" MODIFIED="1437374042105">
<node TEXT="It is a way of logically grouping classes that are only used in one place" STYLE_REF="yellow_node" ID="ID_1733628512" CREATED="1437371171082" MODIFIED="1437373895621">
<node TEXT="If a class is useful to only one other class, then it is logical to embed it in that class and keep the two together." STYLE_REF="pre_last_node" ID="ID_723305385" CREATED="1437371226697" MODIFIED="1437373918339"/>
</node>
<node TEXT="It increases encapsulation" STYLE_REF="yellow_node" ID="ID_289174049" CREATED="1437371203754" MODIFIED="1437374042105">
<node TEXT="Consider two top-level classes, A and B, where B needs access to members of A that would otherwise be declared private. By hiding class B within class A, A&apos;s members can be declared private and B can access them. In addition, B itself can be hidden from the outside world." STYLE_REF="pre_last_node" ID="ID_1203118228" CREATED="1437371220539" MODIFIED="1437373918339"/>
</node>
<node TEXT="It can lead to more readable and maintainable code" STYLE_REF="yellow_node" ID="ID_1964014588" CREATED="1437371215107" MODIFIED="1437373895618">
<node TEXT="Nesting small classes within top-level classes places the code closer to where it is used" STYLE_REF="pre_last_node" ID="ID_1069139536" CREATED="1437371241410" MODIFIED="1437373918339"/>
</node>
</node>
</node>
<node TEXT="Lambda Expressions" STYLE_REF="orange_node" ID="ID_1419145856" CREATED="1437370867364" MODIFIED="1437374047740">
<node TEXT="Lambda expressions enable you to treat functionality as method argument, or code as data." STYLE_REF="main_definition" ID="ID_107781890" CREATED="1437370881283" MODIFIED="1437370927189"/>
<node TEXT="Lambda expressions let you express instances of single-method classes more compactly" STYLE_REF="main_definition" ID="ID_1200908363" CREATED="1437379758859" MODIFIED="1437379764880"/>
<node TEXT="To use a lambda expression, you need to implement a functional interface. A functional interface contains only one abstract method, you can omit the name of that method when you implement it" STYLE_REF="pre_last_node" ID="ID_1309257843" CREATED="1437382835388" MODIFIED="1437384568478"/>
<node TEXT="Approaches" STYLE_REF="gray_node" ID="ID_1551640328" CREATED="1437382843325" MODIFIED="1437383925239">
<node TEXT="Specify Search Criteria Code in a Local Class" STYLE_REF="yellow_node" ID="ID_782433637" CREATED="1437381785655" MODIFIED="1437384579872">
<node TEXT="public static void printPersons(&#xa;    List&lt;Person&gt; roster, CheckPerson tester) {&#xa;    for (Person p : roster) {&#xa;        if (tester.test(p)) {&#xa;            p.printPerson();&#xa;        }&#xa;    }&#xa;}" STYLE_REF="example_node" ID="ID_708477416" CREATED="1437381792845" MODIFIED="1437384607814"/>
<node TEXT="interface CheckPerson {&#xa;    boolean test(Person p);&#xa;}" STYLE_REF="example_node" ID="ID_360672581" CREATED="1437381798998" MODIFIED="1437384607814"/>
<node TEXT="class CheckPersonEligibleForSelectiveService implements CheckPerson {&#xa;    public boolean test(Person p) {&#xa;        return p.gender == Person.Sex.MALE &amp;&amp;&#xa;            p.getAge() &gt;= 18 &amp;&amp;&#xa;            p.getAge() &lt;= 25;&#xa;    }&#xa;}" STYLE_REF="example_node" ID="ID_1290840268" CREATED="1437381805678" MODIFIED="1437384607814"/>
<node TEXT="printPersons(&#xa;    roster, new CheckPersonEligibleForSelectiveService());" STYLE_REF="example_node" ID="ID_1236667900" CREATED="1437381811670" MODIFIED="1437384607829"/>
</node>
<node TEXT="Specify Search Criteria Code in an Anonymous Class" STYLE_REF="yellow_node" ID="ID_518875927" CREATED="1437381862520" MODIFIED="1437455362013">
<node TEXT="printPersons(&#xa;    roster,&#xa;    new CheckPerson() {&#xa;        public boolean test(Person p) {&#xa;            return p.getGender() == Person.Sex.MALE&#xa;                &amp;&amp; p.getAge() &gt;= 18&#xa;                &amp;&amp; p.getAge() &lt;= 25;&#xa;        }&#xa;    }&#xa;);" STYLE_REF="example_node" ID="ID_1816653779" CREATED="1437381868277" MODIFIED="1437455362012"/>
</node>
<node TEXT="Specify Search Criteria Code with a Lambda Expression" STYLE_REF="yellow_node" ID="ID_1687345756" CREATED="1437381874808" MODIFIED="1437384579887">
<node TEXT="printPersons(&#xa;    roster,&#xa;    (Person p) -&gt; p.getGender() == Person.Sex.MALE&#xa;        &amp;&amp; p.getAge() &gt;= 18&#xa;        &amp;&amp; p.getAge() &lt;= 25&#xa;);" STYLE_REF="example_node" ID="ID_662234449" CREATED="1437381931430" MODIFIED="1437384607829"/>
</node>
<node TEXT="Use Standard Functional Interfaces with Lambda Expressions" STYLE_REF="yellow_node" ID="ID_1901552449" CREATED="1437381953793" MODIFIED="1437384579887">
<node TEXT="You can use the Predicate&lt;T&gt; interface in place of CheckPerson." STYLE_REF="pre_last_node" ID="ID_599933152" CREATED="1437382693700" MODIFIED="1437384625503"/>
<node TEXT="interface Predicate&lt;T&gt; {&#xa;    boolean test(T t);&#xa;}" STYLE_REF="example_node" ID="ID_559859824" CREATED="1437382701924" MODIFIED="1437384607829">
<node TEXT="This interface contains only one type parameter, T. When you declare or instantiate a generic type with actual type arguments, you have a parameterized type" STYLE_REF="pre_last_node" ID="ID_1149681991" CREATED="1437382720493" MODIFIED="1437384625503"/>
<node TEXT="interface Predicate&lt;Person&gt; {&#xa;    boolean test(Person t);&#xa;}" STYLE_REF="example_node" ID="ID_360488281" CREATED="1437382726748" MODIFIED="1437384607829"/>
</node>
<node TEXT="public static void printPersonsWithPredicate(&#xa;    List&lt;Person&gt; roster, Predicate&lt;Person&gt; tester) {&#xa;    for (Person p : roster) {&#xa;        if (tester.test(p)) {&#xa;            p.printPerson();&#xa;        }&#xa;    }&#xa;}" STYLE_REF="example_node" ID="ID_320739411" CREATED="1437382744878" MODIFIED="1437384607829"/>
<node TEXT="printPersonsWithPredicate(&#xa;    roster,&#xa;    p -&gt; p.getGender() == Person.Sex.MALE&#xa;        &amp;&amp; p.getAge() &gt;= 18&#xa;        &amp;&amp; p.getAge() &lt;= 25&#xa;);" STYLE_REF="example_node" ID="ID_1239655686" CREATED="1437382751180" MODIFIED="1437384607829"/>
</node>
<node TEXT="Use Lambda Expressions Throughout Your Application" STYLE_REF="yellow_node" ID="ID_1154201555" CREATED="1437382975228" MODIFIED="1437384579887">
<node TEXT="Suppose you want a lambda expression similar to printPerson, one that takes one argument (an object of type Person) and returns void. Remember, to use a lambda expression, you need to implement a functional interface. In this case, you need a functional interface that contains an abstract method that can take one argument of type Person and returns void. The Consumer&lt;T&gt; interface contains the method void accept(T t), which has these characteristics. The following method replaces the invocation p.printPerson() with an instance of Consumer&lt;Person&gt; that invokes the method accept" STYLE_REF="pre_last_node" ID="ID_1428513666" CREATED="1437383048436" MODIFIED="1437384625503">
<node TEXT="public static void processPersons(&#xa;    List&lt;Person&gt; roster,&#xa;    Predicate&lt;Person&gt; tester,&#xa;    Consumer&lt;Person&gt; block) {&#xa;        for (Person p : roster) {&#xa;            if (tester.test(p)) {&#xa;                block.accept(p);&#xa;            }&#xa;        }&#xa;}" STYLE_REF="example_node" ID="ID_1478493559" CREATED="1437382989300" MODIFIED="1437384607829">
<node TEXT="processPersons(&#xa;     roster,&#xa;     p -&gt; p.getGender() == Person.Sex.MALE&#xa;         &amp;&amp; p.getAge() &gt;= 18&#xa;         &amp;&amp; p.getAge() &lt;= 25,&#xa;     p -&gt; p.printPerson()&#xa;);" STYLE_REF="example_node" ID="ID_1210768564" CREATED="1437382995204" MODIFIED="1437384607829"/>
</node>
</node>
<node TEXT="What if you want to do more with your members&apos; profiles than printing them out. Suppose that you want to validate the members&apos; profiles or retrieve their contact information? In this case, you need a functional interface that contains an abstract method that returns a value. The Function&lt;T,R&gt; interface contains the method R apply(T t). The following method retrieves the data specified by the parameter mapper, and then performs an action on it specified by the parameter block:" STYLE_REF="pre_last_node" ID="ID_1430085894" CREATED="1437383061260" MODIFIED="1437384625503">
<node TEXT="public static void processPersonsWithFunction(&#xa;    List&lt;Person&gt; roster,&#xa;    Predicate&lt;Person&gt; tester,&#xa;    Function&lt;Person, String&gt; mapper,&#xa;    Consumer&lt;String&gt; block) {&#xa;    for (Person p : roster) {&#xa;        if (tester.test(p)) {&#xa;            String data = mapper.apply(p);&#xa;            block.accept(data);&#xa;        }&#xa;    }&#xa;}" STYLE_REF="example_node" ID="ID_1221756490" CREATED="1437383001292" MODIFIED="1437384607829">
<node TEXT="processPersonsWithFunction(&#xa;    roster,&#xa;    p -&gt; p.getGender() == Person.Sex.MALE&#xa;        &amp;&amp; p.getAge() &gt;= 18&#xa;        &amp;&amp; p.getAge() &lt;= 25,&#xa;    p -&gt; p.getEmailAddress(),&#xa;    email -&gt; System.out.println(email)&#xa;);" STYLE_REF="example_node" ID="ID_248761523" CREATED="1437383007099" MODIFIED="1437384607845"/>
</node>
</node>
</node>
<node TEXT=" Use Generics More Extensively" STYLE_REF="yellow_node" ID="ID_156693679" CREATED="1437383109116" MODIFIED="1437384579887">
<node TEXT="public static &lt;X, Y&gt; void processElements(&#xa;    Iterable&lt;X&gt; source,&#xa;    Predicate&lt;X&gt; tester,&#xa;    Function &lt;X, Y&gt; mapper,&#xa;    Consumer&lt;Y&gt; block) {&#xa;    for (X p : source) {&#xa;        if (tester.test(p)) {&#xa;            Y data = mapper.apply(p);&#xa;            block.accept(data);&#xa;        }&#xa;    }&#xa;}" STYLE_REF="example_node" ID="ID_486849427" CREATED="1437383115467" MODIFIED="1437384607829"/>
<node TEXT="To print the e-mail address of members who are eligible for Selective Service, invoke the processElements method as follows" STYLE_REF="pre_last_node" ID="ID_1274411504" CREATED="1437383126692" MODIFIED="1437384625503">
<node TEXT="processElements(&#xa;    roster,&#xa;    p -&gt; p.getGender() == Person.Sex.MALE&#xa;        &amp;&amp; p.getAge() &gt;= 18&#xa;        &amp;&amp; p.getAge() &lt;= 25,&#xa;    p -&gt; p.getEmailAddress(),&#xa;    email -&gt; System.out.println(email)&#xa;);" STYLE_REF="example_node" ID="ID_716694126" CREATED="1437383148115" MODIFIED="1437384607845"/>
</node>
<node TEXT="This method invocation performs the following actions" STYLE_REF="pre_last_node" ID="ID_870091067" CREATED="1437383203531" MODIFIED="1437384625503">
<node TEXT="Obtains a source of objects from the collection source. In this example, it obtains a source of Person objects from the collection roster. Notice that the collection roster, which is a collection of type List, is also an object of type Iterable." ID="ID_396282534" CREATED="1437383210716" MODIFIED="1437383210716"/>
<node TEXT="Filters objects that match the Predicate object tester. In this example, the Predicate object is a lambda expression that specifies which members would be eligible for Selective Service." ID="ID_1384998978" CREATED="1437383210716" MODIFIED="1437383210716"/>
<node TEXT="Maps each filtered object to a value as specified by the Function object mapper. In this example, the Function object is a lambda expression that returns the e-mail address of a member." ID="ID_1025084916" CREATED="1437383210716" MODIFIED="1437383210716"/>
<node TEXT="Performs an action on each mapped object as specified by the Consumer object block. In this example, the Consumer object is a lambda expression that prints a string, which is the e-mail address returned by the Function object." ID="ID_799670482" CREATED="1437383210716" MODIFIED="1437383210716"/>
</node>
</node>
<node TEXT="Use Aggregate Operations That Accept Lambda Expressions as Parameters" STYLE_REF="yellow_node" ID="ID_130662224" CREATED="1437383230067" MODIFIED="1437384579887">
<node TEXT="roster&#xa;    .stream()&#xa;    .filter(&#xa;        p -&gt; p.getGender() == Person.Sex.MALE&#xa;            &amp;&amp; p.getAge() &gt;= 18&#xa;            &amp;&amp; p.getAge() &lt;= 25)&#xa;    .map(p -&gt; p.getEmailAddress())&#xa;    .forEach(email -&gt; System.out.println(email));" STYLE_REF="example_node" ID="ID_322622520" CREATED="1437383239811" MODIFIED="1437384607845"/>
<node TEXT="The operations filter, map, and forEach are aggregate operations. Aggregate operations process elements from a stream, not directly from a collection" STYLE_REF="pre_last_node" ID="ID_1154536058" CREATED="1437383414547" MODIFIED="1437384625503"/>
<node TEXT="A stream is a sequence of elements. Unlike a collection, it is not a data structure that stores elements. Instead, a stream carries values from a source, such as collection, through a pipeline. A pipeline is a sequence of stream operations, which in this example is filter- map-forEach" STYLE_REF="pre_last_node" ID="ID_1422431459" CREATED="1437383448436" MODIFIED="1437384625503"/>
</node>
<node TEXT="Lambda Expressions in GUI Applications" STYLE_REF="yellow_node" ID="ID_1971099896" CREATED="1437383534259" MODIFIED="1437384579887">
<node TEXT=" btn.setOnAction(new EventHandler&lt;ActionEvent&gt;() {&#xa;&#xa;            @Override&#xa;            public void handle(ActionEvent event) {&#xa;                System.out.println(&quot;Hello World!&quot;);&#xa;            }&#xa;        });" STYLE_REF="example_node" ID="ID_1998357969" CREATED="1437383539130" MODIFIED="1437384607845">
<node TEXT="Equals" STYLE_REF="pre_last_node" ID="ID_1706292011" CREATED="1437383544475" MODIFIED="1437384625503">
<node TEXT=" btn.setOnAction(&#xa;          event -&gt; System.out.println(&quot;Hello World!&quot;)&#xa;        );" STYLE_REF="example_node" ID="ID_1056345772" CREATED="1437383551066" MODIFIED="1437384607845"/>
</node>
</node>
</node>
</node>
<node TEXT="Syntax of Lambda Expressions" STYLE_REF="gray_node" ID="ID_1946839244" CREATED="1437383581954" MODIFIED="1437383925239">
<node TEXT="A comma-separated list of formal parameters enclosed in parentheses." STYLE_REF="pre_last_node" ID="ID_201628786" CREATED="1437383625722" MODIFIED="1437384475497">
<node TEXT="You can omit the data type of the parameters in a lambda expression. In addition, you can omit the parentheses if there is only one parameter. For example, the following lambda expression is also valid" ID="ID_10702953" CREATED="1437383637362" MODIFIED="1437383647264"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      p -&gt; p.getGender() == Person.Sex.MALE
    </p>
    <p>
      &#160;&#160;&#160;&#160;&amp;&amp; p.getAge() &gt;= 18
    </p>
    <p>
      &#160;&#160;&#160;&#160;&amp;&amp; p.getAge() &lt;= 25
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
<node TEXT="The arrow token, -&gt;" STYLE_REF="pre_last_node" ID="ID_506251200" CREATED="1437383659219" MODIFIED="1437384475497"/>
<node TEXT="A body, which consists of a single expression or a statement block." STYLE_REF="pre_last_node" ID="ID_1925664360" CREATED="1437383671650" MODIFIED="1437384475497"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      p.getGender() == Person.Sex.MALE
    </p>
    <p>
      &#160;&#160;&#160;&#160;&amp;&amp; p.getAge() &gt;= 18
    </p>
    <p>
      &#160;&#160;&#160;&#160;&amp;&amp; p.getAge() &lt;= 25
    </p>
  </body>
</html>

</richcontent>
<node TEXT="If you specify a single expression, then the Java runtime evaluates the expression and then returns its value. Alternatively, you can use a return statement:" ID="ID_1174848030" CREATED="1437383740147" MODIFIED="1437383754888"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      p -&gt; {
    </p>
    <p>
      &#160;&#160;&#160;&#160;return p.getGender() == Person.Sex.MALE
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&amp;&amp; p.getAge() &gt;= 18
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&amp;&amp; p.getAge() &lt;= 25;
    </p>
    <p>
      }
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="A return statement is not an expression; in a lambda expression, you must enclose statements in braces ({}). However, you do not have to enclose a void method invocation in braces. For example, the following is a valid lambda expression:" ID="ID_228440600" CREATED="1437383761163" MODIFIED="1437383774727"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      email -&gt; System.out.println(email);
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
</node>
<node TEXT="Accessing Local Variables of the Enclosing Scope" STYLE_REF="gray_node" ID="ID_895186605" CREATED="1437384065586" MODIFIED="1437384487333">
<node TEXT="Like local and anonymous classes, lambda expressions can capture variables; they have the same access to local variables of the enclosing scope. However, unlike local and anonymous classes, lambda expressions do not have any shadowing issues. Lambda expressions are lexically scoped. This means that they do not inherit any names from a supertype or introduce a new level of scoping. Declarations in a lambda expression are interpreted just as they are in the enclosing environment." STYLE_REF="pre_last_node" ID="ID_322346060" CREATED="1437384073523" MODIFIED="1437384231515">
<node TEXT="import java.util.function.Consumer;&#xa;&#xa;public class LambdaScopeTest {&#xa;&#xa;    public int x = 0;&#xa;&#xa;    class FirstLevel {&#xa;&#xa;        public int x = 1;&#xa;&#xa;        void methodInFirstLevel(int x) {&#xa;           &#xa;            // The following statement causes the compiler to generate&#xa;            // the error &quot;local variables referenced from a lambda expression&#xa;            // must be final or effectively final&quot; in statement A:&#xa;            //&#xa;            // x = 99;&#xa;           &#xa;            Consumer&lt;Integer&gt; myConsumer = (y) -&gt;&#xa;            {&#xa;                System.out.println(&quot;x = &quot; + x); // Statement A&#xa;                System.out.println(&quot;y = &quot; + y);&#xa;                System.out.println(&quot;this.x = &quot; + this.x);&#xa;                System.out.println(&quot;LambdaScopeTest.this.x = &quot; +&#xa;                    LambdaScopeTest.this.x);&#xa;            };&#xa;&#xa;            myConsumer.accept(x);&#xa;&#xa;        }&#xa;    }&#xa;&#xa;    public static void main(String... args) {&#xa;        LambdaScopeTest st = new LambdaScopeTest();&#xa;        LambdaScopeTest.FirstLevel fl = st.new FirstLevel();&#xa;        fl.methodInFirstLevel(23);&#xa;    }&#xa;}" STYLE_REF="example_node" ID="ID_1891772661" CREATED="1437384111066" MODIFIED="1437384121158"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      x = 23
    </p>
    <p>
      y = 23
    </p>
    <p>
      this.x = 1
    </p>
    <p>
      LambdaScopeTest.this.x = 0
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
<node TEXT="If you substitute the parameter x in place of y in the declaration of the lambda expression myConsumer, then the compiler generates an error:" STYLE_REF="pre_last_node" ID="ID_1060050446" CREATED="1437384148794" MODIFIED="1437384231515"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Consumer&lt;Integer&gt; myConsumer = (x) -&gt; {
    </p>
    <p>
      &#160;&#160;&#160;&#160;// ...
    </p>
    <p>
      }
    </p>
  </body>
</html>

</richcontent>
<node TEXT="The compiler generates the error &quot;variable x is already defined in method methodInFirstLevel(int)&quot; because the lambda expression does not introduce a new level of scoping." ID="ID_1973301558" CREATED="1437384166306" MODIFIED="1437384166645"/>
</node>
<node TEXT="However, like local and anonymous classes, a lambda expression can only access local variables and parameters of the enclosing block that are final or effectively final" STYLE_REF="pre_last_node" ID="ID_1112475350" CREATED="1437384180050" MODIFIED="1437384231515"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      void methodInFirstLevel(int x) {
    </p>
    <p>
      &#160;&#160;&#160;&#160;x = 99;
    </p>
    <p>
      &#160;&#160;&#160;&#160;// ...
    </p>
    <p>
      }
    </p>
  </body>
</html>

</richcontent>
<node TEXT="Because of this assignment statement, the variable FirstLevel.x is not effectively final anymore. As a result, the Java compiler generates an error message similar to &quot;local variables referenced from a lambda expression must be final or effectively final&quot;" ID="ID_973429975" CREATED="1437384223673" MODIFIED="1437384224005"/>
</node>
</node>
<node TEXT="Examples" STYLE_REF="gray_node" ID="ID_11943255" CREATED="1437383868148" MODIFIED="1437384483646">
<node TEXT="public class Calculator {&#xa; &#xa;    interface IntegerMath {&#xa;        int operation(int a, int b);  &#xa;    }&#xa; &#xa;    public int operateBinary(int a, int b, IntegerMath op) {&#xa;        return op.operation(a, b);&#xa;    }&#xa; &#xa;    public static void main(String... args) {&#xa;   &#xa;        Calculator myApp = new Calculator();&#xa;        IntegerMath addition = (a, b) -&gt; a + b;&#xa;        IntegerMath subtraction = (a, b) -&gt; a - b;&#xa;        System.out.println(&quot;40 + 2 = &quot; +&#xa;            myApp.operateBinary(40, 2, addition));&#xa;        System.out.println(&quot;20 - 10 = &quot; +&#xa;            myApp.operateBinary(20, 10, subtraction));   &#xa;    }&#xa;}" STYLE_REF="example_node" ID="ID_1322204970" CREATED="1437383871179" MODIFIED="1437384480183"/>
</node>
</node>
</node>
</node>
</map>
