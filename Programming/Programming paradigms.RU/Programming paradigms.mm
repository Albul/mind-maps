<map version="freeplane 1.3.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="&#x41f;&#x430;&#x440;&#x430;&#x434;&#x438;&#x433;&#x43c;&#x44b; &#x43f;&#x440;&#x43e;&#x433;&#x440;&#x430;&#x43c;&#x43c;&#x438;&#x440;&#x43e;&#x432;&#x430;&#x43d;&#x438;&#x44f;" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1418998316926" BACKGROUND_COLOR="#6699ff" VGAP="17">
<hook NAME="AutomaticEdgeColor" COUNTER="5"/>
<hook NAME="MapStyle">

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
<stylenode TEXT="second_node" BACKGROUND_COLOR="#00ff66" STYLE="bubble">
<font SIZE="14"/>
</stylenode>
<stylenode TEXT="pre_last_node" BACKGROUND_COLOR="#99ff99" STYLE="bubble">
<edge STYLE="bezier" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="yellow_node" BACKGROUND_COLOR="#ffff33" STYLE="bubble"/>
<stylenode TEXT="orange_node" BACKGROUND_COLOR="#ff9900">
<font BOLD="true"/>
</stylenode>
<stylenode TEXT="last_node">
<edge STYLE="bezier" WIDTH="thin"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<edge STYLE="sharp_bezier" COLOR="#660000" WIDTH="4"/>
<font SIZE="18" BOLD="true"/>
<hook URI="../../Images/paradigm-1-large.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x418;&#x43c;&#x43f;&#x435;&#x440;&#x430;&#x442;&#x438;&#x432;&#x43d;&#x43e;&#x435;" STYLE_REF="second_node" POSITION="right" ID="ID_1509032636" CREATED="1418985694056" MODIFIED="1418996819748" VGAP="18">
<edge STYLE="sharp_bezier" COLOR="#990000" WIDTH="4"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1048;&#1085;&#1089;&#1090;&#1088;&#1091;&#1082;&#1094;&#1080;&#1080; &#1080;&#1079;&#1084;&#1077;&#1085;&#1103;&#1102;&#1097;&#1080;&#1077; &#1089;&#1086;&#1089;&#1090;&#1086;&#1103;&#1085;&#1080;&#1077;
    </p>
    <p>
      &#1087;&#1088;&#1086;&#1075;&#1088;&#1072;&#1084;&#1084;&#1099; (&#1087;&#1088;&#1080;&#1082;&#1072;&#1079;&#1099;)
    </p>
  </body>
</html>

</richcontent>
<hook URI="../../Images/king-1.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x421;&#x442;&#x440;&#x443;&#x43a;&#x442;&#x443;&#x440;&#x43d;&#x43e;&#x435;" STYLE_REF="orange_node" ID="ID_1366151648" CREATED="1418986385579" MODIFIED="1418993868102">
<hook URI="../../Images/structure-1.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x41f;&#x43e;&#x448;&#x430;&#x433;&#x43e;&#x432;&#x430;&#x44f; &#x440;&#x430;&#x437;&#x440;&#x430;&#x431;&#x43e;&#x442;&#x43a;&#x430;" STYLE_REF="pre_last_node" ID="ID_148334230" CREATED="1418986769240" MODIFIED="1418986823856"/>
<node TEXT="&#x411;&#x430;&#x437;&#x43e;&#x432;&#x44b;&#x435; &#x43a;&#x43e;&#x43d;&#x441;&#x440;&#x443;&#x43a;&#x446;&#x438;&#x438;" STYLE_REF="pre_last_node" ID="ID_1720331506" CREATED="1418986784063" MODIFIED="1418993234908">
<icon BUILTIN="list"/>
<node TEXT="&#x411;&#x43b;&#x43e;&#x43a;" ID="ID_1825986055" CREATED="1418986792148" MODIFIED="1418986795826"/>
<node TEXT="&#x426;&#x438;&#x43a;&#x43b;" ID="ID_1345539118" CREATED="1418986796990" MODIFIED="1418986800269"/>
<node TEXT="&#x412;&#x435;&#x442;&#x432;&#x43b;&#x435;&#x43d;&#x438;&#x435;" ID="ID_1242899021" CREATED="1418986801648" MODIFIED="1418986806317"/>
</node>
</node>
<node TEXT="&#x41f;&#x440;&#x43e;&#x446;&#x435;&#x434;&#x443;&#x440;&#x43d;&#x43e;&#x435;" STYLE_REF="orange_node" ID="ID_1158176798" CREATED="1418986513369" MODIFIED="1418993782019">
<hook URI="../../Images/procedure-1.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x41e;&#x43f;&#x435;&#x440;&#x430;&#x442;&#x43e;&#x440;&#x44b; &#x441;&#x43e;&#x431;&#x438;&#x440;&#x430;&#x44e;&#x442;&#x441;&#x44f;&#xa;&#x432; &#x43f;&#x43e;&#x434;&#x43f;&#x440;&#x43e;&#x433;&#x440;&#x430;&#x43c;&#x43c;&#x44b;" STYLE_REF="pre_last_node" ID="ID_241495253" CREATED="1418986839124" MODIFIED="1418986917394"/>
<node TEXT="&#x41e;&#x441;&#x43d;&#x43e;&#x432;&#x43d;&#x44b;&#x435; &#x43f;&#x43e;&#x43d;&#x44f;&#x442;&#x438;&#x44f;" STYLE_REF="pre_last_node" ID="ID_283767558" CREATED="1418986906100" MODIFIED="1418993227348">
<icon BUILTIN="list"/>
<node TEXT="&#x41f;&#x440;&#x43e;&#x446;&#x435;&#x434;&#x443;&#x440;&#x430;" ID="ID_1395678366" CREATED="1418986920948" MODIFIED="1418986928564"/>
<node TEXT="&#x412;&#x44b;&#x437;&#x43e;&#x432;" ID="ID_1668093568" CREATED="1418986929573" MODIFIED="1418986932178"/>
<node TEXT="&#x412;&#x43e;&#x437;&#x432;&#x440;&#x430;&#x442;" ID="ID_1219801904" CREATED="1418986933175" MODIFIED="1418986937191"/>
<node TEXT="&#x410;&#x440;&#x433;&#x443;&#x43c;&#x435;&#x43d;&#x442;&#x44b;" ID="ID_1628492638" CREATED="1418986938290" MODIFIED="1418986945496"/>
<node TEXT="&#x420;&#x435;&#x43a;&#x443;&#x440;&#x441;&#x438;&#x44f;" ID="ID_1717391690" CREATED="1418986973386" MODIFIED="1418986978390"/>
</node>
</node>
<node TEXT="&#x41c;&#x43e;&#x434;&#x443;&#x43b;&#x44c;&#x43d;&#x43e;&#x435;" STYLE_REF="orange_node" ID="ID_1614054590" CREATED="1418986519776" MODIFIED="1418993214945">
<hook URI="../../Images/module-1.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x41c;&#x43e;&#x434;&#x443;&#x43b;&#x44c; - &#x441;&#x443;&#x449;&#x43d;&#x43e;&#x441;&#x442;&#x44c; &#x43f;&#x440;&#x43e;&#x433;&#x440;&#x430;&#x43c;&#x43c;&#x44b; &#x43e;&#x431;&#x435;&#x434;&#x438;&#x43d;&#x44f;&#x44e;&#x449;&#x430;&#x44f;&#xa;&#x434;&#x440;&#x443;&#x433;&#x438;&#x435; &#x447;&#x430;&#x441;&#x442;&#x438; &#x43f;&#x440;&#x43e;&#x433; &#x431;&#x43b;&#x438;&#x437;&#x43a;&#x438;&#x435; &#x43f;&#x43e; &#x444;&#x443;&#x43d;&#x43a;&#x446;&#x438;&#x43e;&#x43d;&#x430;&#x43b;&#x443;" STYLE_REF="pre_last_node" ID="ID_1403027785" CREATED="1418987015902" MODIFIED="1418987091413"/>
<node TEXT="&#x41e;&#x441;&#x43d;&#x43e;&#x432;&#x43d;&#x44b;&#x435; &#x43f;&#x43e;&#x43d;&#x44f;&#x442;&#x438;&#x44f;" STYLE_REF="pre_last_node" ID="ID_68878880" CREATED="1418987054239" MODIFIED="1418993223436">
<icon BUILTIN="list"/>
<node TEXT="&#x41c;&#x43e;&#x434;&#x443;&#x43b;&#x44c;" ID="ID_163223609" CREATED="1418987060240" MODIFIED="1418987063686"/>
<node TEXT="&#x41f;&#x430;&#x43a;&#x435;&#x442;" ID="ID_508673227" CREATED="1418987064888" MODIFIED="1418987079718"/>
<node TEXT="&#x418;&#x43c;&#x43f;&#x43e;&#x440;&#x442;" ID="ID_171006170" CREATED="1418987080937" MODIFIED="1418987084216"/>
</node>
</node>
<node TEXT="&#x41e;&#x431;&#x44a;&#x435;&#x43a;&#x442;&#x43d;&#x43e;-&#x43e;&#x440;&#x438;&#x435;&#x43d;&#x442;&#x438;&#x440;&#x43e;&#x432;&#x430;&#x43d;&#x43d;&#x43e;&#x435;" STYLE_REF="orange_node" ID="ID_1660760788" CREATED="1418986525622" MODIFIED="1418993590019"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1054;&#1054;&#1055;
    </p>
  </body>
</html>

</richcontent>
<hook URI="../../Images/ufo-1.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x41f;&#x440;&#x43e;&#x433;&#x440;&#x430;&#x43c;&#x43c;&#x430; &#x43a;&#x430;&#x43a; &#x43d;&#x430;&#x431;&#x43e;&#x440; &#x43e;&#x431;&#x44a;&#x435;&#x43a;&#x442;&#x43e;&#x432;&#xa;&#x438; &#x438;&#x445; &#x432;&#x437;&#x430;&#x438;&#x43c;&#x43e;&#x434;&#x435;&#x439;&#x441;&#x442;&#x432;&#x438;&#x439;" STYLE_REF="pre_last_node" ID="ID_733151663" CREATED="1418987135905" MODIFIED="1418987214751"/>
<node TEXT="&#x41e;&#x441;&#x43d;&#x43e;&#x432;&#x43d;&#x44b;&#x435; &#x43f;&#x43e;&#x43d;&#x44f;&#x442;&#x438;&#x44f;" STYLE_REF="pre_last_node" ID="ID_1268492026" CREATED="1418987157674" MODIFIED="1418993238276">
<icon BUILTIN="list"/>
<node TEXT="&#x410;&#x431;&#x441;&#x442;&#x440;&#x430;&#x43a;&#x446;&#x438;&#x44f;" ID="ID_1159895597" CREATED="1418987164458" MODIFIED="1418987175026"/>
<node TEXT="&#x418;&#x43d;&#x43a;&#x430;&#x43f;&#x441;&#x443;&#x43b;&#x44f;&#x446;&#x438;&#x44f;" ID="ID_160290044" CREATED="1418987176205" MODIFIED="1418987182167"/>
<node TEXT="&#x41d;&#x430;&#x441;&#x43b;&#x435;&#x434;&#x43e;&#x432;&#x430;&#x43d;&#x438;&#x435;" ID="ID_572642886" CREATED="1418987183166" MODIFIED="1418987188848"/>
<node TEXT="&#x41f;&#x43e;&#x43b;&#x438;&#x43c;&#x43e;&#x440;&#x444;&#x438;&#x437;&#x43c;" ID="ID_1232170658" CREATED="1418987190434" MODIFIED="1418987198099"/>
<node TEXT="&#x41a;&#x43b;&#x430;&#x441;&#x441;" ID="ID_1627753267" CREATED="1418987199292" MODIFIED="1418987204533"/>
<node TEXT="&#x41e;&#x431;&#x44a;&#x435;&#x43a;&#x442;" ID="ID_389884094" CREATED="1418987205662" MODIFIED="1418987209441"/>
</node>
</node>
<node TEXT="&#x41f;&#x440;&#x43e;&#x442;&#x43e;&#x442;&#x438;&#x43f;&#x43d;&#x43e;&#x435;" STYLE_REF="orange_node" ID="ID_298157228" CREATED="1418986539399" MODIFIED="1418993366941">
<hook URI="../../Images/proto-1.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x41e;&#x441;&#x43e;&#x431;&#x435;&#x43d;&#x43d;&#x43e;&#x441;&#x442;&#x438;" STYLE_REF="yellow_node" ID="ID_74693283" CREATED="1418987848904" MODIFIED="1418987986551">
<node TEXT="&#x41e;&#x442;&#x441;&#x443;&#x442;&#x441;&#x442;&#x432;&#x443;&#x435;&#x442; &#x43f;&#x43e;&#x43d;&#x44f;&#x442;&#x438;&#x435; &#x43a;&#x43b;&#x430;&#x441;&#x441;&#x430;" STYLE_REF="pre_last_node" ID="ID_1669424681" CREATED="1418987881038" MODIFIED="1418987979237"/>
<node TEXT="&#x41d;&#x430;&#x441;&#x43b;&#x435;&#x434;&#x43e;&#x432;&#x430;&#x43d;&#x438;&#x435; &#x43e;&#x441;&#x443;&#x449;&#x435;&#x441;&#x442;&#x432; &#x43a;&#x43b;&#x43e;&#x43d;&#x438;&#x440;&#x43e;&#x432;&#x430;&#x43d;&#x438;&#x435;&#x43c; &#x441;&#x443;&#x449;&#x435;&#x441;&#x442;&#x432;&#x443;&#x44e;&#x449;&#x435;&#x433;&#x43e;&#xa;&#x44d;&#x43a;&#x437;&#x435;&#x43c;&#x43f;&#x43b;&#x44f;&#x440;&#x430; &#x43e;&#x431;&#x44a;&#x435;&#x43a;&#x442;&#x430; - &#x43f;&#x440;&#x43e;&#x442;&#x43e;&#x442;&#x438;&#x43f;&#x430;" STYLE_REF="pre_last_node" ID="ID_466160501" CREATED="1418987900965" MODIFIED="1418987979225"/>
<node TEXT="&#x421;&#x43e;&#x437;&#x434;&#x430;&#x43d;&#x438;&#x435; &#x43d;&#x43e;&#x432;&#x43e;&#x433;&#x43e; &#x43e;&#x431;&#x44a;&#x435;&#x43a;&#x442;&#x430;" STYLE_REF="pre_last_node" ID="ID_1388258716" CREATED="1418987942641" MODIFIED="1418987979216">
<node TEXT="&#x41a;&#x43b;&#x43e;&#x43d;&#x438;&#x440;&#x43e;&#x432;&#x430;&#x43d;&#x438;&#x435; &#x441;&#x443;&#x449;&#x435;&#x441;&#x442;&#x432;&#x443;&#x44e;&#x449;&#x435;&#x433;&#x43e; &#x43e;&#x431;&#x44a;&#x435;&#x43a;&#x442;&#x430;" ID="ID_1856290434" CREATED="1418987953823" MODIFIED="1418987967586"/>
<node TEXT="&#x421;&#x43e;&#x437;&#x434;&#x430;&#x43d;&#x438;&#x435; &#x43e;&#x431;&#x44a;&#x435;&#x43a;&#x442;&#x430; &#x441; &#x43d;&#x443;&#x43b;&#x44f;" ID="ID_272098446" CREATED="1418987968730" MODIFIED="1418987974560"/>
</node>
</node>
<node TEXT="&#x412;&#x438;&#x434;&#x44b;" STYLE_REF="yellow_node" ID="ID_816457925" CREATED="1418987855523" MODIFIED="1418987986563">
<node TEXT="&#x41a;&#x430;&#x441;&#x43a;&#x430;&#x434;&#x438;&#x440;&#x43e;&#x432;&#x430;&#x43d;&#x438;&#x435;" STYLE_REF="pre_last_node" ID="ID_1230399902" CREATED="1418988007120" MODIFIED="1418993445330"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &quot;&#1063;&#1080;&#1089;&#1090;&#1086;&#1077;&quot;
    </p>
  </body>
</html>

</richcontent>
<hook URI="../../Images/mitosis.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x414;&#x43e;&#x43f;&#x43e;&#x43b;&#x43d;&#x438;&#x442;&#x435;&#x43b;&#x44c;&#x43d;&#x43e;&#x435; &#x443;&#x432;&#x435;&#x43b;&#x438;&#x447;&#x435;&#x43d;&#x438;&#x435; &#x440;&#x430;&#x441;&#x445;&#x43e;&#x434;&#x430; &#x43f;&#x430;&#x43c;&#x44f;&#x442;&#x438;" ID="ID_580306915" CREATED="1418988079817" MODIFIED="1418988109536"/>
<node TEXT="&#x41e;&#x431;&#x44a;&#x435;&#x43a;&#x442; &#x43a;&#x43e;&#x43f;&#x438;&#x44f; &#x43d;&#x435; &#x445;&#x440;&#x430;&#x43d;&#x438;&#x442;&#xa;&#x441;&#x441;&#x44b;&#x43b;&#x43e;&#x43a; &#x43d;&#x430; &#x441;&#x432;&#x43e;&#x438; &#x43f;&#x440;&#x43e;&#x442;&#x43e;&#x442;&#x438;&#x43f;&#x44b;" ID="ID_13335210" CREATED="1418988111436" MODIFIED="1418988171204"/>
<node TEXT="&#x41f;&#x440;&#x43e;&#x433; &#x43a;&#x43e;&#x43f;&#x438;&#x440;&#x443;&#x435;&#x442; &#x43e;&#x431;&#x44a;&#x435;&#x43a;&#x442; 1 &#x432; 1 &#x441; &#x43c;&#x435;&#x442;&#x43e;&#x434;&#x430;&#x43c;&#x438; &#x438; &#x430;&#x442;&#x440;&#x438;&#x431;&#x443;&#x442;&#x430;&#x43c;&#x438;,&#xa;&#x43a;&#x43e;&#x43f;&#x438;&#x438; &#x43f;&#x440;&#x438;&#x441;&#x432;&#x430;&#x438;&#x432;&#x430;&#x435;&#x442;&#x44c;&#x441;&#x44f; &#x43d;&#x43e;&#x432;&#x43e;&#x435; &#x438;&#x43c;&#x44f;" ID="ID_10581253" CREATED="1418988172734" MODIFIED="1418988261561"/>
</node>
<node TEXT="&#x414;&#x435;&#x43b;&#x435;&#x433;&#x438;&#x440;&#x43e;&#x432;&#x430;&#x43d;&#x438;&#x435;" STYLE_REF="pre_last_node" ID="ID_240320206" CREATED="1418988017273" MODIFIED="1418988072590">
<node TEXT="&#x41e;&#x431;&#x44a;&#x435;&#x43a;&#x442;-&#x43a;&#x43e;&#x43f;&#x438;&#x44f; &#x445;&#x440;&#x430;&#x43d;&#x438;&#x442; &#x441;&#x441;&#x44b;&#x43b;&#x43a;&#x443;&#xa;&#x43d;&#x430; &#x43e;&#x431;&#x44a;&#x435;&#x43a;&#x442; &#x43f;&#x440;&#x43e;&#x442;&#x43e;&#x442;&#x438;&#x43f;" ID="ID_1134356128" CREATED="1418988266118" MODIFIED="1418988297050"/>
<node TEXT="&#x41f;&#x440;&#x438; &#x432;&#x44b;&#x437;&#x43e;&#x432;&#x435; &#x43c;&#x435;&#x442;&#x43e;&#x434;&#x430; &#x441;&#x440;&#x435;&#x434;&#x430; &#x441;&#x43b;&#x435;&#x434;&#x443;&#x435;&#x442; &#x43f;&#x43e; &#x446;&#x435;&#x43f;&#x43e;&#x447;&#x43a;&#x435;&#xa;&#x434;&#x435;&#x43b;&#x435;&#x433;&#x438;&#x440;&#x43e;&#x432;&#x430;&#x43d;&#x438;&#x44f; &#x443;&#x43a;&#x430;&#x437;&#x430;&#x442;&#x435;&#x43b;&#x435;&#x439; &#x434;&#x43e; &#x441;&#x43e;&#x432;&#x43f;&#x430;&#x434;&#x435;&#x43d;&#x438;&#x44f;" ID="ID_1237711208" CREATED="1418988300317" MODIFIED="1418988342938"/>
</node>
</node>
<node TEXT="&#x41e;&#x442;&#x43b;&#x438;&#x447;&#x438;&#x44f; &#x43e;&#x442; &#x41e;&#x41e;&#x41f;" STYLE_REF="yellow_node" ID="ID_1354101150" CREATED="1418987861496" MODIFIED="1418993632471">
<icon BUILTIN="males"/>
<node TEXT="&#x412;&#x43e;&#x437;&#x43c;&#x43e;&#x436;&#x43d;&#x43e; &#x438;&#x437;&#x43c;&#x435;&#x43d;&#x44f;&#x442;&#x44c; &#x43f;&#x440;&#x43e;&#x442;&#x43e;&#x442;&#x438;&#x43f; &#x432;&#x43e; &#x432;&#x440;&#x435;&#x43c;&#x44f; &#x432;&#x44b;&#x43f;&#x43e;&#x43b;&#x43d;&#x435;&#x43d;&#x438;&#x44f;.&#xa;&#x41a;&#x43b;&#x430;&#x441;&#x441;&#x438; &#x43d;&#x435;&#x432;&#x43e;&#x437;&#x43c;&#x43e;&#x436;&#x43d;&#x43e; (&#x442;&#x43e;&#x43b;&#x44c;&#x43a;&#x43e; &#x432; Ruby)" STYLE_REF="pre_last_node" ID="ID_1930517982" CREATED="1418988406519" MODIFIED="1418988592190"/>
<node TEXT="&#x41e;&#x431;&#x44a;&#x435;&#x43a;&#x442;&#x44b; &#x43f;&#x43e;&#x442;&#x43e;&#x43c;&#x43a;&#x438; &#x43c;&#x43e;&#x433;&#x443;&#x442; &#x43d;&#x435; &#x441;&#x43e;&#x445;&#x440;&#x430;&#x43d;&#x44f;&#x442;&#x44c;&#xa;&#x441;&#x442;&#x440;&#x443;&#x43a;&#x442;&#x443;&#x440;&#x43d;&#x43e;&#x435; &#x43f;&#x43e;&#x434;&#x43e;&#x431;&#x438;&#x435; &#x441;&#x43e; &#x441;&#x432;&#x43e;&#x438;&#x43c; &#x43f;&#x440;&#x43e;&#x442;&#x43e;&#x442;&#x438;&#x43f;&#x43e;&#x43c;" STYLE_REF="pre_last_node" ID="ID_1757707405" CREATED="1418988441755" MODIFIED="1418988592216"/>
<node TEXT="&#x41c;&#x435;&#x43d;&#x438;&#x435; &#x43f;&#x43e;&#x43f;&#x443;&#x43b;&#x44f;&#x440;&#x43d;&#x44b;" STYLE_REF="pre_last_node" ID="ID_430007629" CREATED="1418988479804" MODIFIED="1418988592227"/>
<node TEXT="&#x423;&#x441;&#x442;&#x443;&#x43f;&#x430;&#x44e;&#x442; &#x41e;&#x41e;&#x41f; &#x432; &#x43f;&#x440;&#x43e;&#x438;&#x437;&#x432;&#x43e;&#x434;&#x438;&#x442;&#x435;&#x43b;&#x44c;&#x43d;&#x43e;&#x441;&#x442;&#x438;" STYLE_REF="pre_last_node" ID="ID_356720519" CREATED="1418988492317" MODIFIED="1418988592231"/>
<node TEXT="&#x418;&#x43c;&#x435;&#x44e;&#x442; &#x442;&#x43e;&#x43b;&#x44c;&#x43a;&#x43e; &#x434;&#x438;&#x43d;&#x430;&#x43c;&#x438;&#x447;&#x435;&#x441;&#x43a;&#x443;&#x44e; &#x442;&#x438;&#x43f;&#x438;&#x437;&#x430;&#x446;&#x438;&#x44e;" STYLE_REF="pre_last_node" ID="ID_1601837002" CREATED="1418988509364" MODIFIED="1418988592234"/>
</node>
</node>
<node TEXT="&#x410;&#x441;&#x43f;&#x435;&#x43a;&#x442;&#x43d;&#x43e;-&#x43e;&#x440;&#x438;&#x435;&#x43d;&#x442;&#x438;&#x440;&#x43e;&#x432;&#x430;&#x43d;&#x43d;&#x43e;&#x435;" STYLE_REF="orange_node" ID="ID_95751516" CREATED="1418986551322" MODIFIED="1418986574537">
<node TEXT="&#x412;&#x44b;&#x434;&#x435;&#x43b;&#x435;&#x43d;&#x438;&#x435; &#x441;&#x43a;&#x432;&#x43e;&#x437;&#x43d;&#x43e;&#x439; &#x444;&#x443;&#x43d;&#x43a;&#x446;&#x438;&#x43e;&#x43d;&#x430;&#x43b;&#x44c;&#x43d;&#x43e;&#x441;&#x442;&#x438;&#xa;&#x432; &#x43e;&#x442;&#x434;&#x435;&#x43b;&#x44c;&#x43d;&#x44b;&#x435; &#x441;&#x443;&#x449;&#x43d;&#x43e;&#x441;&#x442;&#x438;" STYLE_REF="pre_last_node" ID="ID_1607974811" CREATED="1418987321755" MODIFIED="1418987476565">
<node TEXT="&#x41e;&#x431;&#x440;&#x430;&#x431;&#x43e;&#x442;&#x43a;&#x430; &#x438;&#x441;&#x43a;&#x43b;&#x44e;&#x447;&#x435;&#x43d;&#x438;&#x439;" ID="ID_465425294" CREATED="1418987358030" MODIFIED="1418987365399"/>
<node TEXT="&#x412;&#x435;&#x434;&#x435;&#x43d;&#x438;&#x435; &#x43b;&#x43e;&#x433;&#x430;" ID="ID_1017774055" CREATED="1418987366587" MODIFIED="1418987373617"/>
</node>
<node TEXT="&#x420;&#x430;&#x437;&#x434;&#x435;&#x43b;&#x435;&#x43d;&#x438;&#x435; &#x444;&#x443;&#x43d;&#x43a;&#x446;&#x438;&#x43e;&#x43d;&#x430;&#x43b;&#x430; &#x434;&#x43b;&#x44f;&#xa;&#x443;&#x43b;&#x443;&#x442;&#x448;&#x435;&#x43d;&#x438;&#x44f; &#x440;&#x430;&#x437;&#x431;&#x438;&#x435;&#x43d;&#x438;&#x44f; &#x43d;&#x430; &#x43c;&#x43e;&#x434;&#x443;&#x43b;&#x438;" STYLE_REF="pre_last_node" ID="ID_904561796" CREATED="1418987675505" MODIFIED="1418987715374"/>
<node TEXT="&#x41e;&#x441;&#x43d;&#x43e;&#x432;&#x43d;&#x44b;&#x435; &#x43f;&#x43e;&#x43d;&#x44f;&#x442;&#x438;&#x44f;" STYLE_REF="pre_last_node" ID="ID_817260585" CREATED="1418987375238" MODIFIED="1418993243996">
<icon BUILTIN="list"/>
<node TEXT="&#x410;&#x441;&#x43f;&#x435;&#x43a;" STYLE_REF="last_node" ID="ID_1621198239" CREATED="1418987403973" MODIFIED="1418987618459">
<node TEXT="&#x41c;&#x43e;&#x434;&#x443;&#x43b;&#x44c; &#x438;&#x43b;&#x438; &#x43a;&#x43b;&#x430;&#x441;&#x441; &#x440;&#x435;&#x430;&#x43b;&#x438;&#x437;&#x443;&#x44e;&#x449;&#x438;&#x439;&#xa;&#x441;&#x43a;&#x432;&#x43e;&#x437;&#x43d;&#x443;&#x44e; &#x444;&#x443;&#x43d;&#x43a;&#x446;&#x438;&#x43e;&#x43d;&#x430;&#x43b;&#x44c;&#x43d;&#x43e;&#x441;&#x442;&#x44c;" ID="ID_1499973124" CREATED="1418987522775" MODIFIED="1418987563742"/>
<node TEXT="&#x41f;&#x440;&#x438;&#x43c;&#x435;&#x43d;&#x44f;&#x435;&#x442; &#x441;&#x43e;&#x432;&#x435;&#x442;&#x44b;&#xa;&#x432; &#x442;&#x43e;&#x447;&#x43a;&#x430;&#x445; &#x441;&#x43e;&#x435;&#x434;&#x438;&#x43d;&#x435;&#x43d;&#x438;&#x44f;" ID="ID_105279301" CREATED="1418987564909" MODIFIED="1418987583245"/>
</node>
<node TEXT="&#x421;&#x43e;&#x432;&#x435;&#x442;" STYLE_REF="last_node" ID="ID_1865681501" CREATED="1418987410133" MODIFIED="1418987618469"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      == &#1084;&#1077;&#1090;&#1086;&#1076;&#1091;
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="&#x422;&#x43e;&#x447;&#x43a;&#x430; &#x441;&#x43e;&#x435;&#x434;&#x438;&#x43d;&#x435;&#x43d;&#x438;&#x44f;" STYLE_REF="last_node" ID="ID_932227769" CREATED="1418987414702" MODIFIED="1418987618473">
<node TEXT="&#x412;&#x44b;&#x437;&#x43e;&#x432; &#x43c;&#x435;&#x442;&#x43e;&#x434;&#x430;" ID="ID_1335662183" CREATED="1418987454871" MODIFIED="1418987459855"/>
<node TEXT="&#x41e;&#x431;&#x440;&#x430;&#x449;&#x435;&#x43d;&#x438;&#x435; &#x43a; &#x43f;&#x43e;&#x43b;&#x44f;&#x43c; &#x43e;&#x431;&#x44a;&#x435;&#x43a;&#x442;&#x430;" ID="ID_1783502173" CREATED="1418987461051" MODIFIED="1418987470372"/>
</node>
<node TEXT="&#x421;&#x440;&#x435;&#x437;" STYLE_REF="last_node" ID="ID_998338968" CREATED="1418987421438" MODIFIED="1418987618478"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1053;&#1072;&#1073;&#1086;&#1088; &#1090;&#1086;&#1095;&#1077;&#1082; &#1089;&#1086;&#1077;&#1076;&#1080;&#1085;&#1077;&#1085;&#1080;&#1103;
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="&#x412;&#x43d;&#x435;&#x434;&#x440;&#x435;&#x43d;&#x438;&#x435;" STYLE_REF="last_node" ID="ID_899631450" CREATED="1418987424660" MODIFIED="1418987618481"/>
</node>
</node>
<node TEXT="&#x421;&#x43e;&#x431;&#x44b;&#x442;&#x438;&#x439;&#x43d;&#x43e;-&#x43e;&#x440;&#x438;&#x435;&#x43d;&#x442;&#x438;&#x440;&#x43e;&#x432;&#x430;&#x43d;&#x43d;&#x43e;&#x435;" STYLE_REF="orange_node" ID="ID_264849539" CREATED="1418996755696" MODIFIED="1418999191114">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="80" FONT_SIZE="10" FONT_FAMILY="SansSerif" DESTINATION="ID_216425460" MIDDLE_LABEL="&#x41a;&#x43e;&#x43c;&#x431;&#x438;&#x43d;&#x438;&#x440;&#x443;&#x435;&#x442;&#x441;&#x44f; &#x441; &#x430;&#x432;&#x442;&#x43e;&#x43c;&#x430;&#x442;&#x43d;&#x44b;&#x43c;.&#xa;&#x41d;&#x435;&#x43b;&#x44c;&#x437;&#x44f; &#x434;&#x43e;&#x43b;&#x433;&#x43e; &#x432;&#x44b;&#x43f;&#x43e;&#x43b;&#x43d;&#x44f;&#x442;&#x44c; &#x43e;&#x434;&#x438;&#x43d; &#x43e;&#x431;&#x440;&#x430;&#x431;&#x43e;&#x442;&#x447;&#x438;&#x43a;" STARTINCLINATION="683;72;" ENDINCLINATION="683;72;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
<hook URI="../../Images/event.jpg" SIZE="1.0" NAME="ExternalObject"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Event-driven programming
    </p>
  </body>
</html>

</richcontent>
<node TEXT="&#x421;&#x444;&#x435;&#x440;&#x430; &#x43f;&#x440;&#x438;&#x43c;&#x435;&#x43d;&#x435;&#x43d;&#x438;&#x44f;" STYLE_REF="pre_last_node" ID="ID_481897195" CREATED="1418997034762" MODIFIED="1418997103434">
<node TEXT="UI" ID="ID_56339591" CREATED="1418997048524" MODIFIED="1418997052467"/>
<node TEXT="&#x421;&#x435;&#x440;&#x432;&#x435;&#x440;&#x43d;&#x43e;&#x435; &#x41f;&#x41e;" ID="ID_58062532" CREATED="1418997053661" MODIFIED="1418997059866"/>
<node TEXT="&#x418;&#x433;&#x440;&#x44b;" ID="ID_1000953052" CREATED="1418997060907" MODIFIED="1418997062464"/>
</node>
<node TEXT="&#x421;&#x43e;&#x431;&#x44b;&#x442;&#x438;&#x44f;" STYLE_REF="pre_last_node" ID="ID_1119925070" CREATED="1418997064062" MODIFIED="1418997103427">
<node TEXT="&#x414;&#x435;&#x439;&#x441;&#x442;&#x432;&#x438;&#x44f; &#x43f;&#x43e;&#x43b;&#x44c;&#x437;&#x43e;&#x432;&#x430;&#x442;&#x435;&#x43b;&#x44f;" ID="ID_499450207" CREATED="1418997069993" MODIFIED="1418997077088"/>
<node TEXT="&#x421;&#x43e;&#x43e;&#x431;&#x449;&#x435;&#x43d;&#x438;&#x44f; &#x43e;&#x442; &#x434;&#x440;&#x443;&#x433;&#x438;&#x445; &#x43f;&#x440;&#x43e;&#x433;" ID="ID_648699897" CREATED="1418997078330" MODIFIED="1418997090400"/>
<node TEXT="&#x421;&#x43e;&#x431;&#x44b;&#x442;&#x438;&#x44f; &#x41e;&#x421;" ID="ID_408362969" CREATED="1418997091318" MODIFIED="1418997113525"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1055;&#1086;&#1089;&#1090;&#1091;&#1087;&#1083;&#1077;&#1085;&#1080;&#1077; &#1089;&#1077;&#1090;&#1077;&#1074;&#1086;&#1075;&#1086; &#1087;&#1072;&#1082;&#1077;&#1090;&#1072;
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
<node TEXT="&#x41a;&#x43e;&#x43c;&#x43f;&#x43e;&#x43d;&#x435;&#x43d;&#x442;&#x44b;" STYLE_REF="pre_last_node" ID="ID_688168358" CREATED="1418997148316" MODIFIED="1418997195753">
<node TEXT="&#x413;&#x43b;&#x430;&#x432;&#x43d;&#x44b;&#x439; &#x446;&#x438;&#x43a;&#x43b;" STYLE_REF="yellow_node" ID="ID_1918414942" CREATED="1418997159474" MODIFIED="1418997301401">
<font BOLD="false"/>
</node>
<node TEXT="&#x422;&#x435;&#x43b;&#x43e;" STYLE_REF="yellow_node" ID="ID_1445560523" CREATED="1418997165601" MODIFIED="1418997301406">
<font BOLD="false"/>
<node TEXT="1. &#x412;&#x44b;&#x431;&#x43e;&#x440;&#x43a;&#x430; &#x441;&#x43e;&#x431;&#x44b;&#x442;&#x438;&#x44f;" ID="ID_1116291734" CREATED="1418997168326" MODIFIED="1418997176047"/>
<node TEXT="2. &#x41e;&#x431;&#x440;&#x430;&#x431;&#x43e;&#x442;&#x43a;&#x430; &#x441;&#x43e;&#x431;&#x44b;&#x442;&#x438;&#x44f;" ID="ID_441408729" CREATED="1418997177428" MODIFIED="1418997190578"/>
</node>
</node>
</node>
<node TEXT="&#x410;&#x432;&#x442;&#x43e;&#x43c;&#x430;&#x442;&#x43d;&#x43e;&#x435;" STYLE_REF="orange_node" ID="ID_216425460" CREATED="1418996787874" MODIFIED="1418997984554">
<hook URI="../../Images/ak-47.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x41f;&#x440;&#x43e;&#x433;&#x440;&#x430;&#x43c;&#x430; == &#x446;&#x438;&#x43a;&#x43b; &#x448;&#x430;&#x433;&#x43e;&#x432; &#x430;&#x432;&#x442;&#x43e;&#x43c;&#x430;&#x442;&#x430;" STYLE_REF="pre_last_node" ID="ID_1380248185" CREATED="1418996921859" MODIFIED="1418996975008"/>
<node TEXT="&#x41e;&#x431;&#x449;&#x435;&#x43d;&#x438;&#x435; &#x43c;&#x435;&#x436;&#x434;&#x443; &#x448;&#x430;&#x433;&#x430;&#x43c;&#x438; &#x430;&#x432;&#x442;&#x43e;&#x43c;&#x430;&#x442;&#x430; &#x447;&#x435;&#x440;&#x435;&#x437;&#xa;&#x43c;&#x43d;&#x43e;&#x436;&#x435;&#x441;&#x442;&#x432;&#x43e; &#x43f;&#x435;&#x440;&#x435;&#x43c;&#x435;&#x43d;&#x43d;&#x44b;&#x445;" STYLE_REF="pre_last_node" ID="ID_1631658652" CREATED="1418996869597" MODIFIED="1418996975022"/>
<node TEXT="&#x412;&#x440;&#x435;&#x43c;&#x435;&#x43d;&#x43d;&#x44b;&#x435; &#x43f;&#x435;&#x440;&#x438;&#x43e;&#x434;&#x44b; &#x432;&#x44b;&#x43f;&#x43e;&#x43b;&#x43d;&#x435;&#x43d;&#x438;&#x44f; &#x43f;&#x440;&#x43e;&#x433;&#xa;&#x440;&#x430;&#x437;&#x431;&#x438;&#x432;&#x430;&#x44e;&#x442;&#x441;&#x44f; &#x43d;&#x430; &#x448;&#x430;&#x433;&#x438; &#x430;&#x432;&#x442;&#x43e;&#x43c;&#x430;&#x442;&#x430;" STYLE_REF="pre_last_node" ID="ID_1199738023" CREATED="1418996836899" MODIFIED="1418996975036"/>
<node TEXT="&#x41f;&#x440;&#x43e;&#x433;&#x440;&#x430;&#x43c;&#x43c;&#x430; &#x43a;&#x430;&#x43a; &#x43c;&#x43e;&#x434;&#x435;&#x43b;&#x44c; &#x444;&#x43e;&#x440;&#x43c;&#x430;&#x43b;&#x44c;&#x43d;&#x43e;&#x433;&#x43e; &#x430;&#x432;&#x442;&#x43e;&#x43c;&#x430;&#x442;&#x430;" STYLE_REF="pre_last_node" ID="ID_739848371" CREATED="1418996822626" MODIFIED="1418996975045"/>
<node TEXT="&#x41d;&#x435;&#x44f;&#x432;&#x43d;&#x430;&#x44f; &#x447;&#x430;&#x441;&#x442;&#x44c; &#x441;&#x43e;&#x441;&#x442;&#x43e;&#x44f;&#x43d;&#x438;&#x44f; &#x441;&#x432;&#x435;&#x434;&#x435;&#x43d;&#x430; &#x43a; &#x43c;&#x438;&#x43d;&#x438;&#x43c;&#x443;&#x43c;&#x443;" STYLE_REF="pre_last_node" ID="ID_1387681563" CREATED="1418996979569" MODIFIED="1418996997797"/>
<node TEXT="&#x41a;&#x43e;&#x43c;&#x43f;&#x43e;&#x43d;&#x435;&#x43d;&#x442;&#x44b;" STYLE_REF="pre_last_node" ID="ID_49792737" CREATED="1418997348541" MODIFIED="1418997365214">
<node TEXT="&#x421;&#x43e;&#x441;&#x442;&#x43e;&#x44f;&#x43d;&#x438;&#x44f;" STYLE_REF="yellow_node" ID="ID_392428914" CREATED="1418997354206" MODIFIED="1418997370165"/>
<node TEXT="&#x428;&#x430;&#x433;&#x438;" STYLE_REF="yellow_node" ID="ID_1256838706" CREATED="1418997359948" MODIFIED="1418997370171"/>
</node>
</node>
<node TEXT="&#x41f;&#x430;&#x440;&#x430;&#x43b;&#x43b;&#x435;&#x43b;&#x44c;&#x43d;&#x43e;&#x435;" STYLE_REF="orange_node" ID="ID_1166639098" CREATED="1418996806678" MODIFIED="1418999301475">
<hook URI="../../Images/parallel.jpg" SIZE="1.0" NAME="ExternalObject"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Concurrent
    </p>
  </body>
</html>

</richcontent>
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="140" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_264849539" MIDDLE_LABEL="&#x41f;&#x440;&#x43e;&#x431;&#x43b;&#x435;&#x43c;&#x430; &#x43c;&#x430;&#x448;&#x442;&#x430;&#x431;&#x438;&#x440;&#x443;&#x435;&#x43c;&#x43e;&#x441;&#x442;&#x438;.&#xa;&#x41f;&#x440;&#x438; &gt; 10000 &#x43f;&#x43e;&#x434;&#x43a;&#x43b;&#x44e;&#x447;&#x435;&#x43d;&#x438;&#x439;, &#x43f;&#x440;&#x438; 1 &#x43f;&#x43e;&#x442;&#x43e;&#x43a; &#x43d;&#x430; &#x441;&#x43e;&#x435;&#x434;&#x438;&#x43d;&#x435;&#x43d;&#x438;&#x435;.&#xa;&#x412; &#x44d;&#x442;&#x43e;&#x43c; &#x441;&#x43b;&#x443;&#x447;&#x430;&#x435; &#x438;&#x441;&#x43f;&#x43e;&#x43b;&#x44c;&#x437;&#x443;&#x44e;&#x442; &#x441;&#x43e;&#x431;&#x44b;&#x442;&#x438;&#x439;&#x43d;&#x43e;&#x435; &#x43f;&#x440;&#x43e;&#x433;" STARTINCLINATION="891;185;" ENDINCLINATION="891;185;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
<node TEXT="&#x412;&#x44b;&#x447;&#x438;&#x441;&#x43b;&#x435;&#x43d;&#x438;&#x44f; &#x432;&#x44b;&#x43f;&#x43e;&#x43b;&#x43d;&#x44f;&#x44e;&#x442;&#x441;&#x44f; &#x43f;&#x430;&#x440;&#x430;&#x43b;&#x435;&#x43b;&#x44c;&#x43d;&#x43e;&#xa;&#x43f;&#x440;&#x438; &#x44d;&#x442;&#x43e;&#x43c; &#x43c;&#x43e;&#x433;&#x443;&#x442; &#x432;&#x437;&#x430;&#x438;&#x43c;&#x43e;&#x434;&#x435;&#x439;&#x441;&#x442;&#x432;&#x443;&#x432;&#x430;&#x442;&#x44c;" STYLE_REF="yellow_node" ID="ID_695794616" CREATED="1418997401594" MODIFIED="1418997603971"/>
<node TEXT="&#x412;&#x44b;&#x43f;&#x43e;&#x43b;&#x43d;&#x435;&#x43d;&#x438;&#x435; &#x43c;&#x43e;&#x436;&#x435;&#x442; &#x431;&#x44b;&#x442;&#x44c;" STYLE_REF="yellow_node" ID="ID_307531075" CREATED="1418997434889" MODIFIED="1418997603968">
<node TEXT="&#x41d;&#x430; &#x43e;&#x434;&#x43d;&#x43e;&#x43c; &#x43f;&#x440;&#x43e;&#x446;&#x435;&#x441;&#x43e;&#x440;&#x435; &#x438;&#x437; &#x432;&#x44b;&#x442;&#x435;&#x441;&#x43d;&#x44f;&#x44e;&#x449;&#x438;&#x43c;&#xa;&#x440;&#x430;&#x437;&#x434;&#x435;&#x43b;&#x435;&#x43d;&#x438;&#x435;&#x43c; &#x432;&#x440;&#x435;&#x43c;&#x435;&#x43d;&#x438;" STYLE_REF="pre_last_node" ID="ID_1346248819" CREATED="1418997456940" MODIFIED="1418997617728"/>
<node TEXT="&#x41d;&#x430; &#x440;&#x430;&#x437;&#x43d;&#x44b;&#x445; &#x43f;&#x440;&#x43e;&#x446;&#x435;&#x441;&#x43e;&#x440;&#x430;&#x445;" STYLE_REF="pre_last_node" ID="ID_24955154" CREATED="1418997477819" MODIFIED="1418997617741"/>
</node>
<node TEXT="&#x41c;&#x43e;&#x434;&#x435;&#x43b;&#x438; &#x43f;&#x430;&#x440;&#x430;&#x43b;&#x435;&#x43b;&#x44c;&#x43d;&#x44b;&#x445; &#x441;&#x438;&#x441;&#x442;&#x435;&#x43c;" STYLE_REF="yellow_node" ID="ID_1169092159" CREATED="1418997488826" MODIFIED="1418997603966">
<node TEXT="&#x41f;&#x430;&#x440;&#x430;&#x43b;&#x435;&#x43b;&#x44c;&#x43d;&#x44b;&#x439; &#x441;&#x43b;&#x443;&#x447;&#x430;&#x439;&#x43d;&#x44b;&#x439; &#x434;&#x43e;&#x441;&#x442;&#x443;&#x43f; &#x43a; &#x41f;&#x41a;" STYLE_REF="pre_last_node" ID="ID_1438395056" CREATED="1418997511936" MODIFIED="1418997629290"/>
<node TEXT="&#x41c;&#x43e;&#x434;&#x435;&#x43b;&#x44c; &#x430;&#x43a;&#x442;&#x43e;&#x440;&#x43e;&#x432;" STYLE_REF="pre_last_node" ID="ID_1797478929" CREATED="1418997529175" MODIFIED="1418997617748"/>
<node TEXT="&#x412;&#x44b;&#x447;&#x438;&#x441;&#x43b;&#x438;&#x442;&#x435;&#x43b;&#x44c;&#x43d;&#x44b;&#x435; &#x441;&#x432;&#x44f;&#x437;&#x43d;&#x44b;&#x435; &#x441;&#x438;&#x441;&#x442;&#x435;&#x43c;&#x44b;" STYLE_REF="pre_last_node" ID="ID_1379299814" CREATED="1418997535934" MODIFIED="1418997617752"/>
<node TEXT="&#x421;&#x435;&#x442;&#x438; &#x41f;&#x435;&#x442;&#x440;&#x438;" STYLE_REF="pre_last_node" ID="ID_1166779791" CREATED="1418997556153" MODIFIED="1418997617756"/>
<node TEXT="&#x41f;&#x440;&#x43e;&#x441;&#x442;&#x43e;&#x435; &#x43f;&#x430;&#x440;&#x430;&#x43b;&#x435;&#x43b;&#x44c;&#x43d;&#x43e;&#x435; &#x41e;&#x41e;&#x41f;" STYLE_REF="pre_last_node" ID="ID_1715385798" CREATED="1418997563187" MODIFIED="1418997617759"/>
<node TEXT="&#x418;&#x441;&#x447;&#x435;&#x441;&#x43b;&#x435;&#x43d;&#x438;&#x435; &#x43f;&#x440;&#x43e;&#x446;&#x435;&#x441;&#x43e;&#x432;" STYLE_REF="pre_last_node" ID="ID_1151784149" CREATED="1418997575062" MODIFIED="1418997617762"/>
<node TEXT="&#x41f;&#x440;&#x43e;&#x441;&#x442;&#x440;&#x430;&#x43d;&#x441;&#x442;&#x432;&#x43e; &#x43a;&#x43e;&#x440;&#x442;&#x435;&#x436;&#x435;&#x439;" STYLE_REF="pre_last_node" ID="ID_1321756807" CREATED="1418997586366" MODIFIED="1418997617765"/>
</node>
<node TEXT="&#x41f;&#x435;&#x440;&#x435;&#x43a;&#x43b;&#x44e;&#x447;&#x435;&#x43d;&#x438;&#x435; &#x43a;&#x43e;&#x43d;&#x442;&#x435;&#x43a;&#x441;&#x442;&#x43e;&#x432;" STYLE_REF="yellow_node" ID="ID_1975154808" CREATED="1418997501092" MODIFIED="1418997603960"/>
</node>
</node>
<node TEXT="&#x414;&#x435;&#x43a;&#x43b;&#x430;&#x440;&#x430;&#x442;&#x438;&#x432;&#x43d;&#x43e;&#x435;" STYLE_REF="second_node" POSITION="left" ID="ID_1905760120" CREATED="1418986679682" MODIFIED="1418992509596" VGAP="27">
<edge STYLE="sharp_bezier" COLOR="#660000" WIDTH="4"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1054;&#1087;&#1080;&#1089;&#1099;&#1074;&#1072;&#1077;&#1090; &#1082;&#1072;&#1082;&#1086;&#1081; &#1088;&#1077;&#1079;&#1091;&#1083;&#1100;&#1090;&#1072;&#1090;
    </p>
    <p>
      &#1085;&#1091;&#1078;&#1085;&#1086; &#1087;&#1086;&#1083;&#1091;&#1095;&#1080;&#1090;&#1100;
    </p>
  </body>
</html>

</richcontent>
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="80" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_1509032636" SOURCE_LABEL="&#x427;&#x442;&#x43e; &#x43d;&#x443;&#x436;&#x43d;&#x43e; &#x441;&#x434;&#x435;&#x43b;&#x430;&#x442;&#x44c;" TARGET_LABEL="&#x41a;&#x430;&#x43a; &#x438;&#x43c;&#x435;&#x43d;&#x43d;&#x43e; &#x44d;&#x442;&#x43e; &#x441;&#x434;&#x435;&#x43b;&#x430;&#x442;&#x44c;" STARTINCLINATION="-181;-294;" ENDINCLINATION="37;-80;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
<hook URI="../../Images/declaration.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x41f;&#x43e;&#x442;&#x43e;&#x43a;&#x43e;&#x432;&#x43e;&#x435;" STYLE_REF="orange_node" ID="ID_1388191502" CREATED="1418989321085" MODIFIED="1418989854874">
<hook URI="../../Images/stream-1.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x414;&#x430;&#x43d;&#x43d;&#x44b;&#x435; &#x438; &#x43e;&#x442;&#x43d;&#x43e;&#x448;&#x435;&#x43d;&#x438;&#x44f;&#xa;&#x43c;&#x435;&#x436;&#x434;&#x443; &#x43d;&#x438;&#x43c;&#x438; &#x43f;&#x435;&#x440;&#x432;&#x438;&#x447;&#x43d;&#x44b;" STYLE_REF="pre_last_node" ID="ID_423908777" CREATED="1418989365738" MODIFIED="1418989640379"/>
<node TEXT="&#x414;&#x435;&#x439;&#x441;&#x442;&#x432;&#x438;&#x44f; &#x432;&#x442;&#x43e;&#x440;&#x438;&#x447;&#x43d;&#x44b;" STYLE_REF="pre_last_node" ID="ID_937699863" CREATED="1418989392632" MODIFIED="1418989640375"/>
<node TEXT="&#x41f;&#x440;&#x43e;&#x433;&#x440;&#x430;&#x43c;&#x43c;&#x44b; &#x432; &#x432;&#x438;&#x434;&#x435; &#x441;&#x445;&#x435;&#x43c; &#x430; &#x43d;&#x435; &#x43a;&#x43e;&#x434;&#x430;" STYLE_REF="pre_last_node" ID="ID_508078243" CREATED="1418989402833" MODIFIED="1418989640372"/>
<node TEXT="&#x413;&#x440;&#x430;&#x444;" STYLE_REF="pre_last_node" ID="ID_805879971" CREATED="1418989594514" MODIFIED="1418992188456">
<hook URI="../../Images/graph-1.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x420;&#x435;&#x431;&#x440;&#x430; - &#x43f;&#x43e;&#x442;&#x43e;&#x43a;&#x438; &#x434;&#x430;&#x43d;&#x43d;&#x44b;&#x445;" ID="ID_875738725" CREATED="1418989612707" MODIFIED="1418989622464"/>
<node TEXT="&#x412;&#x435;&#x440;&#x448;&#x438;&#x43d;&#x44b; - &#x434;&#x435;&#x439;&#x441;&#x442;&#x432;&#x438;&#x44f; &#x43d;&#x430;&#x434; &#x43d;&#x438;&#x43c;&#x438;" ID="ID_483114081" CREATED="1418989624115" MODIFIED="1418989634957"/>
</node>
</node>
<node TEXT="&#x424;&#x443;&#x43d;&#x43a;&#x446;&#x438;&#x43e;&#x43d;&#x430;&#x43b;&#x44c;&#x43d;&#x43e;&#x435;" STYLE_REF="orange_node" ID="ID_325626589" CREATED="1418989331393" MODIFIED="1418991428235">
<hook URI="../../Images/func-1.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x412;&#x44b;&#x447;&#x438;&#x441;&#x43b;&#x435;&#x43d;&#x438;&#x435; &#x437;&#x43d;&#x430;&#x447;&#x435;&#x43d;&#x438;&#x439; &#x444;&#x443;&#x43d;&#x43a;&#x446;&#x438;&#x438;&#xa;&#x432; &#x43c;&#x430;&#x442;&#x435;&#x43c;&#x430;&#x442;&#x438;&#x447;&#x435;&#x441;&#x43a;&#x43e;&#x43c; &#x43f;&#x43e;&#x43d;&#x438;&#x43c;&#x430;&#x43d;&#x438;&#x438;" STYLE_REF="pre_last_node" ID="ID_165947740" CREATED="1418989857894" MODIFIED="1418990200529"/>
<node TEXT="&#x424;&#x443;&#x43d;&#x43a;&#x446;&#x438;&#x438; &#x43d;&#x435; &#x438;&#x43c;&#x435;&#x44e;&#x442; &#x43f;&#x43e;&#x431;&#x43e;&#x447;&#x43d;&#x44b;&#x445; &#x44d;&#x444;&#x435;&#x43a;&#x442;&#x43e;&#x432;" STYLE_REF="pre_last_node" ID="ID_860288334" CREATED="1418989893263" MODIFIED="1418990200545">
<node TEXT="&#x424;&#x443;&#x43d;&#x43a;&#x446;&#x438;&#x438; &#x432;&#x43e;&#x437;&#x432;&#x440;&#x430;&#x449;&#x430;&#x44e;&#x442; &#x434;&#x430;&#x43d;&#x43d;&#x44b;&#x435;&#xa;&#x437;&#x430;&#x432;&#x438;&#x441;&#x44f;&#x449;&#x438;&#x435; &#x442;&#x43e;&#x43b;&#x44c;&#x43a;&#x43e; &#x43e;&#x442; &#x432;&#x445;&#x43e;&#x434;&#x44f;&#x449;&#x438;&#x445;" ID="ID_951267168" CREATED="1418989926215" MODIFIED="1418989950905"/>
<node TEXT="&#x420;&#x435;&#x437;&#x443;&#x43b;&#x44c;&#x442;&#x430;&#x442;&#x438; &#x444;&#x443;&#x43d;&#x43a;&#x446;&#x438;&#x438; &#x43a;&#x435;&#x448;&#x438;&#x440;&#x443;&#x44e;&#x442;&#x441;&#x44f;" ID="ID_1347493029" CREATED="1418989952261" MODIFIED="1418989963378"/>
</node>
<node TEXT="&#x41d;&#x435;&#x442; &#x441;&#x43e;&#x441;&#x442;&#x43e;&#x44f;&#x43d;&#x438;&#x44f; &#x43f;&#x440;&#x43e;&#x433;&#x440;&#x430;&#x43c;&#x43c;&#x44b;" STYLE_REF="pre_last_node" ID="ID_74705784" CREATED="1418989966445" MODIFIED="1418990200551"/>
<node TEXT="&#x41a;&#x43e;&#x43d;&#x446;&#x435;&#x43f;&#x446;&#x438;&#x438;" STYLE_REF="yellow_node" ID="ID_625025120" CREATED="1418989987449" MODIFIED="1418992288577">
<icon BUILTIN="idea"/>
<node TEXT="&#x424;&#x443;&#x43d;&#x43a;&#x446;&#x438;&#x438; &#x432;&#x44b;&#x441;&#x448;&#x438;&#x445; &#x43f;&#x43e;&#x440;&#x44f;&#x434;&#x43a;&#x43e;&#x432;" STYLE_REF="pre_last_node" ID="ID_1534270774" CREATED="1418990016426" MODIFIED="1418990217895"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1048;&#1093; &#1072;&#1088;&#1075;&#1091;&#1084;&#1077;&#1085;&#1090;&#1099; &#1101;&#1090;&#1086; &#1076;&#1088;&#1091;&#1075;&#1080;&#1077; &#1092;&#1091;&#1085;&#1082;&#1094;&#1080;&#1080;
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="&#x427;&#x438;&#x441;&#x442;&#x44b;&#x435; &#x444;&#x443;&#x43d;&#x43a;&#x446;&#x438;&#x438;" STYLE_REF="pre_last_node" ID="ID_1295736029" CREATED="1418990047281" MODIFIED="1418990318045">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="80" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_860288334" STARTINCLINATION="227;40;" ENDINCLINATION="262;40;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
</node>
<node TEXT="&#x420;&#x435;&#x43a;&#x443;&#x440;&#x441;&#x438;&#x44f;" STYLE_REF="pre_last_node" ID="ID_603026690" CREATED="1418990055766" MODIFIED="1418990217911"/>
<node TEXT="&#x41f;&#x43e;&#x434;&#x445;&#x43e;&#x434; &#x43a; &#x432;&#x44b;&#x447;&#x438;&#x441;&#x43b;&#x435;&#x43d;&#x438;&#x44e; &#x430;&#x440;&#x433;&#x443;&#x43c;&#x435;&#x43d;&#x442;&#x43e;&#x432;" STYLE_REF="pre_last_node" ID="ID_113422229" CREATED="1418990060850" MODIFIED="1418990217915">
<node TEXT="&#x421;&#x442;&#x440;&#x43e;&#x433;&#x438;&#x439;" ID="ID_62835324" CREATED="1418990078201" MODIFIED="1418992488365">
<icon BUILTIN="male2"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1042;&#1099;&#1095;&#1080;&#1089;&#1083;&#1103;&#1102;&#1090;&#1089;&#1103; &#1074;&#1089;&#1077;
    </p>
    <p>
      &#1072;&#1088;&#1075;&#1091;&#1084;&#1077;&#1085;&#1090;&#1099; &#1086;&#1073;&#1103;&#1079;&#1072;&#1090;&#1077;&#1083;&#1100;&#1085;&#1086;
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="&#x41d;&#x435; &#x441;&#x442;&#x440;&#x43e;&#x433;&#x438;&#x439;" ID="ID_1208376520" CREATED="1418990084288" MODIFIED="1418992495668">
<icon BUILTIN="female2"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1053;&#1077; &#1074;&#1099;&#1095;&#1080;&#1089;&#1083;&#1103;&#1102;&#1090;&#1089;&#1103; &#1077;&#1089;&#1083;&#1080; &#1085;&#1077; &#1085;&#1091;&#1078;&#1085;&#1086;.
    </p>
    <p>
      &#1051;&#1077;&#1085;&#1080;&#1074;&#1099;&#1077; &#1074;&#1099;&#1095;&#1080;&#1089;&#1083;&#1077;&#1085;&#1080;&#1103;
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
</node>
<node TEXT="&#x414;&#x43e;&#x441;&#x442;&#x43e;&#x438;&#x43d;&#x441;&#x442;&#x432;&#x430;" STYLE_REF="yellow_node" ID="ID_578784927" CREATED="1418989997707" MODIFIED="1418990227202">
<icon BUILTIN="addition"/>
<node TEXT="&#x41d;&#x430;&#x434;&#x435;&#x436;&#x43d;&#x44b;&#x439; &#x43a;&#x43e;&#x434;" STYLE_REF="pre_last_node" ID="ID_833961600" CREATED="1418990093523" MODIFIED="1418990217918"/>
<node TEXT="&#x423;&#x434;&#x43e;&#x431;&#x441;&#x442;&#x432;&#x43e; &#x442;&#x435;&#x441;&#x442;&#x438;&#x440;&#x43e;&#x432;&#x430;&#x43d;&#x438;&#x44f;" STYLE_REF="pre_last_node" ID="ID_1158733114" CREATED="1418990098715" MODIFIED="1418990217922"/>
<node TEXT="&#x411;&#x43e;&#x43b;&#x44c;&#x448;&#x438;&#x435; &#x43e;&#x43f;&#x442;&#x438;&#x43c;&#x438;&#x437;&#x430;&#x446;&#x438;&#x438; &#x43f;&#x440;&#x438; &#x43a;&#x43e;&#x43c;&#x43f;&#x438;&#x43b;&#x44f;&#x446;&#x438;&#x438;" STYLE_REF="pre_last_node" ID="ID_1987118135" CREATED="1418990109996" MODIFIED="1418990217926"/>
<node TEXT="&#x410;&#x432;&#x442;&#x43e;&#x43c;&#x430;&#x442;&#x438;&#x447;&#x435;&#x441;&#x43a;&#x438;&#x439; &#x445;&#x43e;&#x440;&#x43e;&#x448;&#x438;&#x439; &#x43f;&#x430;&#x440;&#x430;&#x43b;&#x435;&#x43b;&#x438;&#x437;&#x43c;" STYLE_REF="pre_last_node" ID="ID_768441943" CREATED="1418990126738" MODIFIED="1418990237106"/>
</node>
</node>
<node TEXT="&#x41b;&#x43e;&#x433;&#x438;&#x447;&#x435;&#x441;&#x43a;&#x43e;&#x435;" STYLE_REF="orange_node" ID="ID_896540044" CREATED="1418989340690" MODIFIED="1418991962352">
<hook URI="../../Images/math-logic.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x41c;&#x430;&#x442;&#x435;&#x43c;&#x430;&#x442;&#x438;&#x447;&#x435;&#x441;&#x43a;&#x430;&#x44f; &#x43b;&#x43e;&#x433;&#x438;&#x43a;&#x430; &#x434;&#x43b;&#x44f; &#x440;&#x430;&#x437;&#x440;&#x430;&#x431;&#x43e;&#x442;&#x43a;&#x438; &#x43f;&#x440;&#x43e;&#x433;&#x440;&#x430;&#x43c;&#x43c;&#x44b;" STYLE_REF="pre_last_node" ID="ID_872224610" CREATED="1418990585761" MODIFIED="1418991605692"/>
<node TEXT="&#x427;&#x442;&#x43e;&#x431;&#x44b; &#x434;&#x43e;&#x43a;&#x430;&#x437;&#x430;&#x442;&#x44c; / &#x440;&#x435;&#x448;&#x438;&#x442;&#x44c; H,  &#x441;&#x43b;&#x435;&#x434;&#x443;&#x435;&#x442;&#xa;&#x434;&#x43e;&#x43a;&#x430;&#x437;&#x430;&#x442;&#x44c; /  &#x440;&#x435;&#x448;&#x438;&#x442;&#x44c; &#x412;1, ... &#x412;n" STYLE_REF="pre_last_node" ID="ID_1173288219" CREATED="1418990605595" MODIFIED="1418991605705"/>
<node TEXT="Prolog" STYLE_REF="pre_last_node" ID="ID_1044845933" CREATED="1418991507484" MODIFIED="1418991605714"/>
</node>
<node TEXT="&#x41f;&#x440;&#x43e;&#x433;&#x440;&#x430;&#x43c;&#x43c;&#x438;&#x440;&#x43e;&#x432;&#x430;&#x43d;&#x438;&#x435; &#x432; &#x43e;&#x433;&#x440;&#x430;&#x43d;&#x438;&#x447;&#x435;&#x43d;&#x438;&#x44f;&#x445;" STYLE_REF="orange_node" ID="ID_1082770719" CREATED="1418989346257" MODIFIED="1418991724629">
<hook URI="../../Images/prison-1.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x41e;&#x442;&#x43d;&#x43e;&#x448;&#x435;&#x43d;&#x438;&#x44f; &#x43c;&#x435;&#x436;&#x434;&#x443; &#x43f;&#x435;&#x440;&#x435;&#x43c;&#x435;&#x43d;&#x43d;&#x44b;&#x43c;&#x438;&#xa;&#x443;&#x43a;&#x430;&#x437;&#x44b;&#x432;&#x430;&#x44e;&#x442;&#x44c;&#x441;&#x44f; &#x432; &#x444;&#x43e;&#x440;&#x43c;&#x435; &#x43e;&#x433;&#x440;&#x430;&#x43d;&#x438;&#x447;&#x435;&#x43d;&#x438;&#x439;" STYLE_REF="pre_last_node" ID="ID_907889696" CREATED="1418991528331" MODIFIED="1418991593787"/>
<node TEXT="&#x411;&#x430;&#x437;&#x438;&#x440;&#x443;&#x435;&#x442;&#x441;&#x44f; &#x43d;&#x430; &#x442;&#x435;&#x43e;&#x440;&#x438;&#x438;&#xa;&#x443;&#x434;&#x43e;&#x432;&#x43b;&#x435;&#x442;&#x432;&#x43e;&#x440;&#x435;&#x43d;&#x438;&#x44f; &#x43e;&#x433;&#x440;&#x430;&#x43d;&#x438;&#x447;&#x435;&#x43d;&#x438;&#x439;" STYLE_REF="pre_last_node" ID="ID_859396762" CREATED="1418991556142" MODIFIED="1418991593805"/>
<node TEXT="Prolog III, GNU Prolog" STYLE_REF="pre_last_node" ID="ID_1320520747" CREATED="1418991576215" MODIFIED="1418991593815"/>
</node>
</node>
<node TEXT="&#x41c;&#x435;&#x442;&#x430;&#x43f;&#x440;&#x43e;&#x433;&#x440;&#x430;&#x43c;&#x43c;&#x438;&#x440;&#x43e;&#x432;&#x430;&#x43d;&#x438;&#x435;" STYLE_REF="second_node" POSITION="right" ID="ID_959020362" CREATED="1418994448605" MODIFIED="1418994749399">
<edge COLOR="#660000"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1053;&#1072;&#1087;&#1080;&#1089;&#1072;&#1085;&#1080;&#1077; &#1087;&#1088;&#1086;&#1075; &#1082;&#1086;&#1090;&#1086;&#1088;&#1099;&#1077; &#1088;&#1072;&#1073;&#1086;&#1090;&#1072;&#1102;&#1090;
    </p>
    <p>
      &#1089; &#1076;&#1088;&#1091;&#1075;&#1080;&#1084;&#1080; &#1087;&#1088;&#1086;&#1075; &#1074; &#1082;&#1072;&#1095;&#1077;&#1089;&#1090;&#1074;&#1077; &#1076;&#1072;&#1085;&#1085;&#1099;&#1093;
    </p>
  </body>
</html>

</richcontent>
<hook URI="../../Images/in-yan.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x41c;&#x435;&#x445;&#x430;&#x43d;&#x438;&#x437;&#x43c;&#x44b;" STYLE_REF="orange_node" ID="ID_1600975135" CREATED="1418994475014" MODIFIED="1418995371811" VGAP="21">
<icon BUILTIN="executable"/>
<node TEXT="&#x413;&#x435;&#x43d;&#x435;&#x440;&#x430;&#x446;&#x438;&#x44f; &#x43a;&#x43e;&#x434;&#x430;" STYLE_REF="yellow_node" ID="ID_1768220880" CREATED="1418994757798" MODIFIED="1418994856804">
<node TEXT="&#x428;&#x430;&#x431;&#x43b;&#x43e;&#x43d;&#x44b;" STYLE_REF="pre_last_node" ID="ID_1981471322" CREATED="1418994783690" MODIFIED="1418994843500"/>
<node TEXT="&#x41c;&#x430;&#x43a;&#x440;&#x43e;&#x441;&#x44b;" STYLE_REF="pre_last_node" ID="ID_1247325339" CREATED="1418994791373" MODIFIED="1418994843509"/>
<node TEXT="&#x412;&#x43d;&#x435;&#x44f;&#x437;&#x44b;&#x43a;&#x43e;&#x432;&#x44b;&#x435; &#x441;&#x440;&#x435;&#x434;&#x441;&#x442;&#x432;&#x430;" STYLE_REF="pre_last_node" ID="ID_1902391493" CREATED="1418994796769" MODIFIED="1418994843512"/>
</node>
<node TEXT="&#x421;&#x430;&#x43c;&#x43e;&#x43c;&#x43e;&#x434;&#x438;&#x444;&#x438;&#x446;&#x438;&#x440;&#x443;&#x44e;&#x449;&#x438;&#x439;&#x441;&#x44f; &#x43a;&#x43e;&#x434;" STYLE_REF="yellow_node" ID="ID_340980398" CREATED="1418994764719" MODIFIED="1418994836755">
<node TEXT="&#x418;&#x43d;&#x442;&#x440;&#x43e;&#x441;&#x43f;&#x435;&#x43a;&#x446;&#x438;&#x44f;" STYLE_REF="pre_last_node" ID="ID_88713724" CREATED="1418994810378" MODIFIED="1418994911908"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1042;&#1086;&#1079;&#1084;&#1086;&#1078;&#1085;&#1086;&#1089;&#1090;&#1100; &#1086;&#1087;&#1088;&#1077;&#1076;&#1080;&#1083;&#1080;&#1090;&#1100; &#1090;&#1080;&#1087; &#1080; &#1089;&#1090;&#1088;&#1091;&#1082;&#1090;&#1091;&#1088;&#1091;
    </p>
    <p>
      &#1086;&#1073;&#1098;&#1077;&#1082;&#1090;&#1072; &#1074;&#1086; &#1074;&#1088;&#1077;&#1084;&#1103; &#1074;&#1099;&#1087;&#1086;&#1083;&#1085;&#1077;&#1085;&#1080;&#1103; &#1087;&#1088;&#1086;&#1075;
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="&#x418;&#x441;&#x43f;&#x43e;&#x43b;&#x43d;&#x435;&#x43d;&#x438;&#x435; &#x441;&#x442;&#x440;&#x43e;&#x43a;&#x438;" STYLE_REF="pre_last_node" ID="ID_797130141" CREATED="1418994820087" MODIFIED="1418994869475"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      eval(s);
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
</node>
<node TEXT="&#x41f;&#x43e;&#x434;&#x432;&#x438;&#x434;&#x44b;" STYLE_REF="orange_node" ID="ID_1219425938" CREATED="1418994483711" MODIFIED="1418995219325" VGAP="25">
<node TEXT="&#x410;&#x432;&#x442;&#x43e;&#x43c;&#x430;&#x442;&#x438;&#x437;&#x438;&#x440;&#x43e;&#x432;&#x430;&#x43d;&#x43d;&#x43e;&#x435;" STYLE_REF="yellow_node" ID="ID_1620006819" CREATED="1418994923807" MODIFIED="1418995219317">
<font BOLD="true"/>
<hook URI="../../Images/robot.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x41e;&#x442;&#x441;&#x442;&#x440;&#x430;&#x43d;&#x435;&#x43d;&#x438;&#x435; &#x447;&#x435;&#x43b;&#x43e;&#x432;&#x435;&#x43a;&#x430; &#x43e;&#x442; &#x43d;&#x430;&#x43f;&#x438;&#x441;&#x430;&#x43d;&#x438;&#x44f; &#x43a;&#x43e;&#x434;&#x430;" STYLE_REF="pre_last_node" ID="ID_1339337453" CREATED="1418995037919" MODIFIED="1418995074211"/>
<node TEXT="&#x412;&#x44b;&#x441;&#x43e;&#x43a;&#x438;&#x439; &#x443;&#x440;&#x43e;&#x432;&#x435;&#x43d;&#x44c; &#x430;&#x431;&#x441;&#x442;&#x440;&#x430;&#x43a;&#x446;&#x438;&#x438;" STYLE_REF="pre_last_node" ID="ID_1209928036" CREATED="1418995048102" MODIFIED="1418995074208"/>
<node TEXT="&#x41f;&#x43e;&#x441;&#x442;&#x430;&#x43d;&#x43e;&#x432;&#x43a;&#x430; &#x437;&#x430;&#x434;&#x430;&#x447;&#x438; &#x43e;&#x442; &#x447;&#x435;&#x43b;&#x43e;&#x432;&#x435;&#x43a;&#x430; &#x43a; &#x43c;&#x430;&#x448;&#x438;&#x43d;&#x435;" STYLE_REF="pre_last_node" ID="ID_1665233681" CREATED="1418995057401" MODIFIED="1418995074200"/>
</node>
<node TEXT="&#x420;&#x435;&#x444;&#x43b;&#x435;&#x43a;&#x441;&#x438;&#x432;&#x43d;&#x43e;&#x435;" STYLE_REF="yellow_node" ID="ID_1751089403" CREATED="1418994955074" MODIFIED="1418996006949">
<font BOLD="true"/>
<hook URI="../../Images/reflection-1.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x41e;&#x431;&#x440;&#x430;&#x431;&#x43e;&#x442;&#x43a;&#x430; &#x438;&#x43d;&#x441;&#x442;&#x440;&#x443;&#x43a;&#x446;&#x438;&#x439; &#x43f;&#x435;&#x440;&#x435;&#x434; &#x438;&#x445; &#x432;&#x44b;&#x43f;&#x43e;&#x43b;&#x43d;&#x435;&#x43d;&#x438;&#x435;&#x43c;" STYLE_REF="pre_last_node" ID="ID_1179524732" CREATED="1418995221951" MODIFIED="1418995273192"/>
<node TEXT="&#x418;&#x43d;&#x441;&#x442;&#x440;&#x443;&#x43a;&#x446;&#x438;&#x438; &#x43a;&#x430;&#x43a; &#x434;&#x430;&#x43d;&#x43d;&#x44b;&#x435;" STYLE_REF="pre_last_node" ID="ID_710072848" CREATED="1418995242029" MODIFIED="1418995273189"/>
<node TEXT="&#x414;&#x438;&#x43d;&#x430;&#x43c;&#x438;&#x447;&#x435;&#x441;&#x43a;&#x43e;&#x435; &#x438;&#x437;&#x43c;&#x435;&#x43d;&#x435;&#x43d;&#x438;&#x435; &#x43a;&#x43e;&#x434;&#x430;" STYLE_REF="pre_last_node" ID="ID_481706037" CREATED="1418995251589" MODIFIED="1418995273185"/>
<node TEXT="&#x412;&#x43e;&#x437;&#x43c;&#x43e;&#x436;&#x43d;&#x43e;&#x441;&#x442;&#x438;" STYLE_REF="pre_last_node" ID="ID_1323860948" CREATED="1418995263326" MODIFIED="1418995273180">
<node TEXT="&#x418;&#x437;&#x43c;&#x435;&#x43d;&#x435;&#x43d;&#x438;&#x435; &#x438;&#x43c;&#x435;&#x43d;&#x438; &#x43a;&#x43b;&#x430;&#x441;&#x441;&#x43e;&#x432; &#x438; &#x444;&#x443;&#x43d;&#x43a;&#x446;&#x438;&#x439;&#xa;&#x432;&#x43e; &#x432;&#x440;&#x435;&#x43c;&#x44f; &#x438;&#x441;&#x43f;&#x43e;&#x43b;&#x43d;&#x435;&#x43d;&#x438;&#x44f;" ID="ID_96012620" CREATED="1418995275194" MODIFIED="1418995299594"/>
<node TEXT="&#x410;&#x43d;&#x430;&#x43b;&#x438;&#x437; &#x438; &#x432;&#x44b;&#x43f;&#x43e;&#x43b;&#x43d;&#x435;&#x43d;&#x438;&#x435; &#x441;&#x442;&#x440;&#x43e;&#x43a; &#x43a;&#x43e;&#x434;&#x430;&#xa;&#x43f;&#x43e;&#x441;&#x442;&#x443;&#x43f;&#x430;&#x44e;&#x449;&#x438;&#x445; &#x438;&#x437;&#x432;&#x43d;&#x435;" ID="ID_366624577" CREATED="1418995300571" MODIFIED="1418995321107"/>
<node TEXT="&#x412;&#x43e;&#x437;&#x43c;&#x43e;&#x436;&#x43d;&#x43e;&#x441;&#x442;&#x44c; &#x441;&#x43e;&#x437;&#x434;&#x430;&#x43d;&#x438;&#x44f; &#x438;&#x43d;&#x442;&#x435;&#x440;&#x43f;&#x440;&#x438;&#x442;&#x430;&#x442;&#x43e;&#x440;&#x430;&#xa;&#x431;&#x430;&#x439;&#x442; &#x43a;&#x43e;&#x434;&#x430; &#x43d;&#x43e;&#x432;&#x43e;&#x433;&#x43e; &#x44f;&#x437;&#x44b;&#x43a;&#x430;" ID="ID_1783917051" CREATED="1418995322309" MODIFIED="1418995348570"/>
</node>
</node>
<node TEXT="&#x42f;&#x437;&#x44b;&#x43a;&#x43e;&#x432;&#x43e; &#x43e;&#x440;&#x438;&#x435;&#x43d;&#x442;&#x438;&#x440;&#x43e;&#x432;&#x430;&#x43d;&#x43d;&#x43e;&#x435;" STYLE_REF="yellow_node" ID="ID_1613093187" CREATED="1418994960726" MODIFIED="1418995952376">
<font BOLD="true"/>
<hook URI="../../Images/tongue.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x42d;&#x442;&#x430;&#x43f;&#x44b;" STYLE_REF="pre_last_node" ID="ID_1749553683" CREATED="1418995455511" MODIFIED="1418995545539">
<node TEXT="1. &#x421;&#x43e;&#x437;&#x434;&#x430;&#x43d;&#x438;&#x435; &#x43f;&#x440;&#x435;&#x434;&#x43c;&#x435;&#x442;&#x43d;&#x43e; &#x441;&#x43f;&#x435;&#x446;&#x435;&#x444;&#x438;&#x447;&#x435;&#x441;&#x43a;&#x43e;&#x433;&#x43e; &#x44f;&#x437;&#x44b;&#x43a;&#x430;" ID="ID_1166209050" CREATED="1418995547929" MODIFIED="1418995564782"/>
<node TEXT="2. &#x420;&#x435;&#x448;&#x435;&#x43d;&#x438;&#x435; &#x437;&#x430;&#x434;&#x430;&#x447; &#x441; &#x435;&#x433;&#x43e; &#x438;&#x441;&#x43f;&#x43e;&#x43b;&#x44c;&#x437;&#x43e;&#x432;&#x430;&#x43d;&#x438;&#x435;&#x43c;" ID="ID_1100330955" CREATED="1418995565968" MODIFIED="1418995582455"/>
</node>
<node TEXT="&#x418;&#x441;&#x43f;&#x43e;&#x43b;&#x44c;&#x437;&#x43e;&#x432;&#x430;&#x43d;&#x438;&#x435; &#x43f;&#x440;&#x435;&#x434;&#x43c;&#x435;&#x442;&#x43d;&#x43e; &#x441;&#x43f;&#x435;&#x446;&#x435;&#x444;&#x438;&#x447;&#x435;&#x441;&#x43a;&#x438;&#x445; &#x44f;&#x437;&#x438;&#x43a;&#x43e;&#x432;" STYLE_REF="pre_last_node" ID="ID_32836357" CREATED="1418995460936" MODIFIED="1418995545534"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      DSL
    </p>
  </body>
</html>

</richcontent>
<node TEXT="&#x421;&#x43e;&#x437;&#x434;&#x430;&#x43d;&#x438;&#x435; &#x43f;&#x43e;&#x43b;&#x44c;&#x437;&#x43e;&#x432;&#x430;&#x442;&#x435;&#x43b;&#x44c;&#x441;&#x43a;&#x438;&#x445; &#x43a;&#x43b;&#x430;&#x441;&#x43e;&#x432;&#xa;&#x43d;&#x435; &#x43f;&#x440;&#x435;&#x434;&#x443;&#x441;&#x43c;&#x43e;&#x442;&#x440;&#x435;&#x43d;&#x43d;&#x430;" ID="ID_305373628" CREATED="1418995492879" MODIFIED="1418995518627"/>
<node TEXT="Tex, HTML, SQL, Maple" ID="ID_859723434" CREATED="1418995519693" MODIFIED="1418995540072">
<font BOLD="true"/>
</node>
</node>
</node>
<node TEXT="&#x41e;&#x431;&#x43e;&#x431;&#x449;&#x435;&#x43d;&#x43d;&#x43e;&#x435;" STYLE_REF="yellow_node" ID="ID_1249239274" CREATED="1418994973067" MODIFIED="1418996696882">
<font BOLD="true"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Generic
    </p>
  </body>
</html>

</richcontent>
<hook URI="../../Images/human-1.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x41e;&#x431;&#x43e;&#x431;&#x449;&#x435;&#x43d;&#x43d;&#x44b;&#x439; &#x430;&#x43b;&#x433;&#x43e;&#x440;&#x438;&#x442;&#x43c; &#x43f;&#x440;&#x438;&#x43c;&#x435;&#x43d;&#x44f;&#x435;&#x442;&#x441;&#x44f; &#x434;&#x43b;&#x44f;&#xa;&#x43b;&#x44e;&#x431;&#x44b;&#x445; &#x442;&#x438;&#x43f;&#x43e;&#x432; &#x443;&#x434;&#x43e;&#x432;&#x43b;&#x435;&#x442;&#x432;&#x43e;&#x440;&#x44f;&#x44e;&#x449;&#x438;&#x445; &#x435;&#x433;&#x43e;&#xa;&#x441;&#x432;&#x43e;&#x438;&#x43c;&#x438; &#x43a;&#x43e;&#x43d;&#x446;&#x435;&#x43f;&#x446;&#x438;&#x44f;&#x43c;&#x438;" STYLE_REF="pre_last_node" ID="ID_1957490942" CREATED="1418996011637" MODIFIED="1418996180011"/>
<node TEXT="&#x422;&#x440;&#x435;&#x431;&#x43e;&#x432;&#x430;&#x43d;&#x438;&#x44f; &#x43a; &#x43a;&#x43e;&#x43d;&#x446;&#x435;&#x43f;&#x446;&#x438;&#x44f;&#x43c;" STYLE_REF="pre_last_node" ID="ID_1091976181" CREATED="1418996183794" MODIFIED="1418996212667">
<node TEXT="&#x414;&#x43e;&#x43f;&#x443;&#x441;&#x442;&#x438;&#x43c;&#x44b;&#x435; &#x432;&#x44b;&#x440;&#x430;&#x436;&#x435;&#x43d;&#x438;&#x44f;" ID="ID_965259606" CREATED="1418996213952" MODIFIED="1418996223291"/>
<node TEXT="&#x410;&#x441;&#x43e;&#x446;&#x438;&#x438;&#x440;&#x43e;&#x432;&#x430;&#x43d;&#x43d;&#x44b;&#x435; &#x442;&#x438;&#x43f;&#x44b;" ID="ID_1273497267" CREATED="1418996224594" MODIFIED="1418996233345"/>
<node TEXT="&#x418;&#x43d;&#x432;&#x430;&#x440;&#x438;&#x430;&#x43d;&#x442;&#x44b;" ID="ID_1592250188" CREATED="1418996234424" MODIFIED="1418996241333">
<node TEXT="&#x41f;&#x440;&#x435;&#x434;&#x443;&#x441;&#x43b;&#x43e;&#x432;&#x438;&#x44f;" ID="ID_1280842086" CREATED="1418996254121" MODIFIED="1418996259426"/>
<node TEXT="&#x41f;&#x43e;&#x441;&#x442;&#x443;&#x441;&#x43b;&#x43e;&#x432;&#x438;&#x44f;" ID="ID_412782501" CREATED="1418996260313" MODIFIED="1418996266027"/>
</node>
<node TEXT="&#x413;&#x430;&#x440;&#x430;&#x43d;&#x442;&#x438;&#x438; &#x441;&#x43b;&#x43e;&#x436;&#x43d;&#x43e;&#x441;&#x442;&#x438;" ID="ID_184759303" CREATED="1418996242384" MODIFIED="1418996252959"/>
</node>
<node TEXT="STL &#x432; C++" STYLE_REF="pre_last_node" ID="ID_848941771" CREATED="1418996144292" MODIFIED="1418996179995"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Standard template library
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="&#x423;&#x43c;&#x435;&#x43d;&#x448;&#x430;&#x435;&#x442;&#x441;&#x44f; &#x434;&#x443;&#x431;&#x43b;&#x438;&#x440;&#x43e;&#x432;&#x430;&#x43d;&#x438;&#x435; &#x43a;&#x43e;&#x434;&#x430;" STYLE_REF="pre_last_node" ID="ID_425458432" CREATED="1418996108559" MODIFIED="1418996180001"/>
<node TEXT="&#x423;&#x43d;&#x438;&#x432;&#x435;&#x440;&#x441;&#x430;&#x43b;&#x44c;&#x43d;&#x44b;&#x435; &#x43a;&#x43e;&#x43d;&#x442;&#x435;&#x439;&#x43d;&#x435;&#x440;&#x430; &#x438; &#x430;&#x43b;&#x433;&#x43e;&#x440;&#x438;&#x442;&#x43c;&#x44b;" STYLE_REF="pre_last_node" ID="ID_1530398865" CREATED="1418996091321" MODIFIED="1418996180004"/>
<node TEXT="&#x41f;&#x430;&#x440;&#x430;&#x43c;&#x435;&#x442;&#x440;&#x438;&#x447;&#x435;&#x441;&#x43a;&#x438;&#x439; &#x43f;&#x43e;&#x43b;&#x438;&#x43c;&#x43e;&#x440;&#x444;&#x438;&#x437;&#x43c;" STYLE_REF="pre_last_node" ID="ID_441246494" CREATED="1418996078797" MODIFIED="1418996180007"/>
</node>
</node>
</node>
<node TEXT="&#x414;&#x440;&#x443;&#x433;&#x438;&#x435;" STYLE_REF="second_node" POSITION="left" ID="ID_1630395728" CREATED="1418996703096" MODIFIED="1418998321225" VGAP="13">
<edge COLOR="#660000"/>
<node TEXT="&#x420;&#x435;&#x430;&#x43a;&#x442;&#x438;&#x432;&#x43d;&#x43e;&#x435;" STYLE_REF="orange_node" ID="ID_1321421654" CREATED="1418998090023" MODIFIED="1418998321220">
<hook URI="../../Images/rocket-1.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x41f;&#x43e;&#x442;&#x43e;&#x43a;&#x438; &#x434;&#x430;&#x43d;&#x43d;&#x44b;&#x445;" STYLE_REF="pre_last_node" ID="ID_35633126" CREATED="1418998136579" MODIFIED="1418998196708"/>
<node TEXT="&#x420;&#x430;&#x441;&#x43f;&#x43e;&#x441;&#x442;&#x440;&#x430;&#x43d;&#x435;&#x43d;&#x438;&#x435; &#x438;&#x437;&#x43c;&#x435;&#x43d;&#x435;&#x43d;&#x438;&#x439;" STYLE_REF="pre_last_node" ID="ID_1520591900" CREATED="1418998147253" MODIFIED="1418998196699"/>
<node TEXT="&#x41f;&#x440;&#x438;&#x43c;&#x435;&#x440;: Exel" STYLE_REF="pre_last_node" ID="ID_1684253199" CREATED="1418998159187" MODIFIED="1418998201346">
<font BOLD="true"/>
</node>
<node TEXT="&#x41a;&#x43b;&#x435;&#x442;&#x43e;&#x447;&#x43a;&#x430; &#x431;&#x443;&#x434;&#x435;&#x442; &#x43e;&#x431;&#x43d;&#x43e;&#x432;&#x43b;&#x435;&#x43d;&#x430;&#xa;&#x432; &#x437;&#x430;&#x432;&#x438;&#x441;&#x438;&#x43c;&#x43e;&#x441;&#x442;&#x438; &#x43e;&#x442; &#x434;&#x440;&#x443;&#x433;&#x438;&#x445; &#x43a;&#x43b;&#x435;&#x442;&#x43e;&#x43a;" STYLE_REF="pre_last_node" ID="ID_1637472246" CREATED="1418998173585" MODIFIED="1418998196673"/>
</node>
<node TEXT="&#x41a;&#x43e;&#x43d;&#x43a;&#x430;&#x442;&#x435;&#x43d;&#x430;&#x442;&#x438;&#x432;&#x43d;&#x43e;&#x435; /&#xa;&#x441;&#x442;&#x435;&#x43a;&#x43e;&#x432;&#x43e;&#x435;" STYLE_REF="orange_node" ID="ID_1836884486" CREATED="1418998095719" MODIFIED="1418998852293"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Stack based
    </p>
  </body>
</html>

</richcontent>
<hook URI="../../Images/stack-1.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x41a;&#x43e;&#x43d;&#x43a;&#x430;&#x442;&#x430;&#x43d;&#x430;&#x446;&#x438;&#x44f; &#x434;&#x432;&#x443;&#x445; &#x447;&#x430;&#x441;&#x442;&#x435;&#x439; &#x43a;&#x43e;&#x434;&#x430;&#xa;&#x432;&#x44b;&#x440;&#x430;&#x436;&#x430;&#x435;&#x442; &#x438;&#x445; &#x43a;&#x43e;&#x43c;&#x43f;&#x43e;&#x437;&#x438;&#x446;&#x438;&#x44e;" STYLE_REF="pre_last_node" ID="ID_882497506" CREATED="1418998373399" MODIFIED="1418998430033"/>
<node TEXT="&#x41f;&#x43e;&#x441;&#x442;&#x444;&#x438;&#x43a;&#x441;&#x43d;&#x430;&#x44f; &#x43d;&#x43e;&#x442;&#x430;&#x446;&#x438;&#x44f;" STYLE_REF="pre_last_node" ID="ID_1602089361" CREATED="1418998397944" MODIFIED="1418998430030"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      23+.
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="&#x41f;&#x440;&#x438;&#x43c;&#x435;&#x440;: PostScript" STYLE_REF="pre_last_node" ID="ID_540225069" CREATED="1418998414395" MODIFIED="1418998430021">
<font BOLD="true"/>
</node>
</node>
<node TEXT="&#x42d;&#x437;&#x43e;&#x442;&#x435;&#x440;&#x438;&#x447;&#x435;&#x441;&#x43a;&#x43e;&#x435;" STYLE_REF="orange_node" ID="ID_778666301" CREATED="1418998110253" MODIFIED="1418998633038">
<hook URI="../../Images/chakra.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x412; &#x43a;&#x430;&#x447;&#x435;&#x441;&#x442;&#x432;&#x435; &#x448;&#x443;&#x442;&#x43a;&#x438;" STYLE_REF="pre_last_node" ID="ID_517593399" CREATED="1418998441187" MODIFIED="1418998506921"/>
<node TEXT="&#x42d;&#x43a;&#x441;&#x43f;&#x435;&#x440;&#x438;&#x43c;&#x435;&#x43d;&#x442;" STYLE_REF="pre_last_node" ID="ID_41573578" CREATED="1418998454663" MODIFIED="1418998506919"/>
<node TEXT="&#x41f;&#x43e;&#x43f;&#x44b;&#x442;&#x43a;&#x430; &#x434;&#x43e;&#x432;&#x435;&#x441;&#x442;&#x438; &#x438;&#x434;&#x435;&#x44e; &#x434;&#x43e; &#x430;&#x431;&#x441;&#x43e;&#x43b;&#x44e;&#x442;&#x430;" STYLE_REF="pre_last_node" ID="ID_1738516592" CREATED="1418998460906" MODIFIED="1418998506916"/>
<node TEXT="&#x412;&#x438;&#x434;&#x44b; &#x44f;&#x437;&#x44b;&#x43a;&#x43e;&#x432;" STYLE_REF="pre_last_node" ID="ID_626535006" CREATED="1418998478614" MODIFIED="1418998506908">
<node TEXT="&#x41e;&#x433;&#x440;&#x430;&#x43d;&#x438;&#x447;&#x435;&#x43d;&#x43d;&#x44b;&#x435;" ID="ID_1389940462" CREATED="1418998483146" MODIFIED="1418998487162"/>
<node TEXT="&#x422;&#x44e;&#x440;&#x438;&#x43d;&#x433; &#x43f;&#x43e;&#x43b;&#x43d;&#x44b;&#x435;" ID="ID_737514593" CREATED="1418998488618" MODIFIED="1418998550112"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1052;&#1086;&#1078;&#1085;&#1072; &#1085;&#1072;&#1087;&#1080;&#1089;&#1072;&#1090;&#1100; &#1076;&#1072;&#1078;&#1077; &#1054;&#1057;
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
</node>
</node>
</node>
</map>
