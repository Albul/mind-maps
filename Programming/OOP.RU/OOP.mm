<map version="freeplane 1.3.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="&#x41e;&#x41e;&#x41f;" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1419107511712" BACKGROUND_COLOR="#66ff33" VGAP="13">
<font SIZE="18" BOLD="true"/>
<hook NAME="AutomaticEdgeColor" COUNTER="4"/>
<hook NAME="MapStyle">
    <properties show_icon_for_attributes="true" show_note_icons="true"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
<stylenode TEXT="second_node" BACKGROUND_COLOR="#00ff66" STYLE="bubble">
<font SIZE="14"/>
</stylenode>
<stylenode TEXT="pre_last_node" BACKGROUND_COLOR="#99ff99" STYLE="bubble">
<edge STYLE="bezier" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="yellow_node" BACKGROUND_COLOR="#ffff33" STYLE="bubble"/>
<stylenode TEXT="orange_node" BACKGROUND_COLOR="#ff9900">
<font BOLD="true"/>
</stylenode>
<stylenode TEXT="last_node">
<edge STYLE="bezier" WIDTH="thin"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<edge STYLE="sharp_bezier" WIDTH="4"/>
<hook URI="../../Images/oop-large.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x41e;&#x441;&#x43d;&#x43e;&#x432;&#x44b;&#x435; &#x43f;&#x43e;&#x43d;&#x44f;&#x442;&#x438;&#x44f;" STYLE_REF="second_node" POSITION="right" ID="ID_1964108315" CREATED="1419066227063" MODIFIED="1419107511707" VGAP="23" HGAP="40" VSHIFT="-110">
<edge COLOR="#990000"/>
<hook URI="../../Images/dictionary.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x410;&#x431;&#x441;&#x442;&#x440;&#x430;&#x43a;&#x446;&#x438;&#x44f;" STYLE_REF="orange_node" ID="ID_1323933084" CREATED="1419069288820" MODIFIED="1419144644739"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1040;&#1073;&#1089;&#1090;&#1088;&#1072;&#1075;&#1080;&#1088;&#1086;&#1074;&#1072;&#1085;&#1080;&#1077; - &#1089;&#1087;&#1086;&#1089;&#1086;&#1073; &#1074;&#1099;&#1076;&#1077;&#1083;&#1080;&#1090;&#1100; &#1085;&#1072;&#1073;&#1086;&#1088;
    </p>
    <p>
      &#1079;&#1085;&#1072;&#1095;&#1080;&#1084;&#1099;&#1093; &#1093;&#1072;&#1088;&#1072;&#1082;&#1090;&#1077;&#1088;&#1080;&#1089;&#1090;&#1080;&#1082; &#1086;&#1073;&#1098;&#1077;&#1082;&#1090;&#1072; &#1080;&#1089;&#1082;&#1083;&#1102;&#1095;&#1072;&#1103;
    </p>
    <p>
      &#1080;&#1079; &#1088;&#1072;&#1089;&#1089;&#1084;&#1086;&#1090;&#1088;&#1077;&#1085;&#1080;&#1103; &#1085;&#1077;&#1079;&#1085;&#1072;&#1095;&#1080;&#1084;&#1099;&#1077;
    </p>
  </body>
</html>
</richcontent>
<hook URI="../../Images/abstraction.jpg" SIZE="1.0" NAME="ExternalObject"/>
</node>
<node TEXT="&#x418;&#x43d;&#x43a;&#x430;&#x43f;&#x441;&#x443;&#x43b;&#x44f;&#x446;&#x438;&#x44f;" STYLE_REF="orange_node" ID="ID_291249224" CREATED="1419069306433" MODIFIED="1419144160485"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1057;&#1074;&#1086;&#1081;&#1089;&#1090;&#1074;&#1086; &#1089;&#1080;&#1089;&#1090;&#1077;&#1084;&#1099; &#1087;&#1086;&#1079;&#1074;&#1086;&#1083;&#1103;&#1102;&#1097;&#1077;&#1077; &#1086;&#1073;&#1098;&#1077;&#1076;&#1080;&#1085;&#1080;&#1090;&#1100;
    </p>
    <p>
      &#1076;&#1072;&#1085;&#1085;&#1099;&#1077; &#1080; &#1084;&#1077;&#1090;&#1086;&#1076;&#1099; &#1074; &#1082;&#1083;&#1072;&#1089;&#1089;&#1077;, &#1080; &#1089;&#1082;&#1088;&#1099;&#1090;&#1100; &#1076;&#1077;&#1090;&#1072;&#1083;&#1080;
    </p>
    <p>
      &#1088;&#1077;&#1072;&#1083;&#1080;&#1079;&#1072;&#1094;&#1080;&#1080; &#1086;&#1090; &#1087;&#1086;&#1083;&#1100;&#1079;&#1086;&#1074;&#1072;&#1090;&#1077;&#1083;&#1103;
    </p>
  </body>
</html>
</richcontent>
<hook URI="../../Images/capsule.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x41a;&#x43e;&#x43d;&#x442;&#x440;&#x43e;&#x43b;&#x44c; &#x434;&#x43e;&#x441;&#x442;&#x443;&#x43f;&#x430;" STYLE_REF="yellow_node" ID="ID_1868250400" CREATED="1419103887020" MODIFIED="1419103931590">
<node TEXT="Public" STYLE_REF="pre_last_node" ID="ID_181977543" CREATED="1419103938286" MODIFIED="1419103996087"/>
<node TEXT="Protected" STYLE_REF="pre_last_node" ID="ID_1452584140" CREATED="1419103942406" MODIFIED="1419103996095"/>
<node TEXT="Private" STYLE_REF="pre_last_node" ID="ID_1162842392" CREATED="1419103948738" MODIFIED="1419103996099"/>
</node>
<node TEXT="&#x41c;&#x435;&#x442;&#x43e;&#x434;&#x44b; &#x434;&#x43e;&#x441;&#x442;&#x443;&#x43f;&#x430;" STYLE_REF="yellow_node" ID="ID_1480291738" CREATED="1419103908804" MODIFIED="1419103931595"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1040;&#1082;&#1089;&#1077;&#1089;&#1089;&#1086;&#1088;&#1099;
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Get" STYLE_REF="pre_last_node" ID="ID_1485717732" CREATED="1419103953437" MODIFIED="1419103996102"/>
<node TEXT="Set" STYLE_REF="pre_last_node" ID="ID_1684699608" CREATED="1419103956166" MODIFIED="1419103996104"/>
</node>
<node TEXT="&#x421;&#x432;&#x43e;&#x439;&#x441;&#x442;&#x432;&#x430; &#x43e;&#x431;&#x44a;&#x435;&#x43a;&#x442;&#x430;" STYLE_REF="yellow_node" ID="ID_742921799" CREATED="1419103918240" MODIFIED="1419103931597">
<node TEXT="&#x41f;&#x441;&#x435;&#x432;&#x434;&#x43e;&#x43f;&#x43e;&#x43b;&#x44f;" STYLE_REF="pre_last_node" ID="ID_1820763754" CREATED="1419103962890" MODIFIED="1419103996106"/>
<node TEXT="&#x421;&#x43a;&#x440;&#x44b;&#x432;&#x430;&#x44e;&#x442; &#x432;&#x44b;&#x437;&#x43e;&#x432; &#x43c;&#x435;&#x442;&#x43e;&#x434;&#x43e;&#x432; &#x434;&#x43e;&#x441;&#x442;&#x443;&#x43f;&#x430;" STYLE_REF="pre_last_node" ID="ID_1479485199" CREATED="1419103969713" MODIFIED="1419103996108"/>
</node>
</node>
<node TEXT="&#x41f;&#x43e;&#x43b;&#x438;&#x43c;&#x43e;&#x440;&#x444;&#x438;&#x437;&#x43c;" STYLE_REF="orange_node" ID="ID_1369594709" CREATED="1419069299389" MODIFIED="1419144440541"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1054;&#1090; &#1075;&#1088;&#1077;&#1095;&#1077;&#1089;&#1082;&#1086;&#1075;&#1086; &quot;&#1080;&#1084;&#1077;&#1102;&#1097;&#1080;&#1081; &#1084;&#1085;&#1086;&#1075;&#1086; &#1092;&#1086;&#1088;&#1084;&quot;
    </p>
  </body>
</html>
</richcontent>
<hook URI="../../Images/poly.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x415;&#x434;&#x438;&#x43d;&#x43e;&#x43e;&#x431;&#x440;&#x430;&#x437;&#x43d;&#x430;&#x44f; &#x43e;&#x431;&#x440;&#x430;&#x431;&#x43e;&#x442;&#x43a;&#x430; &#x440;&#x430;&#x437;&#x43d;&#x43e;&#x440;&#x43e;&#x434;&#x43d;&#x44b;&#x445; &#x434;&#x430;&#x43d;&#x43d;&#x44b;&#x445;" STYLE_REF="pre_last_node" ID="ID_1066115912" CREATED="1419144235502" MODIFIED="1436535109614"/>
<node TEXT="&#x412;&#x43e;&#x437;&#x43c;&#x43e;&#x436;&#x43d;&#x43e;&#x441;&#x442;&#x44c; &#x437;&#x430;&#x43f;&#x438;&#x441;&#x438;&#x432;&#x430;&#x442;&#x44c; &#x440;&#x430;&#x437;&#x43d;&#x44b;&#x435; &#x43e;&#x431;&#x44a;&#x435;&#x43a;&#x442;&#x44b; &#x432; &#x43f;&#x435;&#x440;&#x435;&#x43c;&#x435;&#x43d;&#x43d;&#x44b;&#x435;&#xa;&#x43e;&#x434;&#x43d;&#x43e;&#x433;&#x43e; &#x438; &#x442;&#x43e;&#x433;&#x43e; &#x436;&#x435; &#x442;&#x438;&#x43f;&#x430;" STYLE_REF="pre_last_node" ID="ID_1468974810" CREATED="1419105363141" MODIFIED="1419105460573"/>
<node TEXT="&#x412;&#x43e;&#x437;&#x43c;&#x43e;&#x436;&#x43d;&#x43e;&#x441;&#x442;&#x44c; &#x438;&#x441;&#x43f;&#x43e;&#x43b;&#x44c;&#x437;&#x43e;&#x432;&#x430;&#x43d;&#x438;&#x44f; &#x43e;&#x431;&#x44a;&#x435;&#x43a;&#x442;&#x430; &#x441; &#x43e;&#x434;&#x438;&#x43d;&#x430;&#x43a;&#x43e;&#x432;&#x44b;&#x43c;&#xa;&#x438;&#x43d;&#x442;&#x435;&#x440;&#x444;&#x435;&#x439;&#x441;&#x43e;&#x43c;, &#x431;&#x435;&#x437; &#x438;&#x43d;&#x444;&#x43e;&#x440;&#x43c;&#x430;&#x446;&#x438;&#x438; &#x43e; &#x435;&#x433;&#x43e; &#x442;&#x438;&#x43f;&#x435; &#x438; &#x441;&#x442;&#x440;&#x443;&#x43a;&#x442;&#x443;&#x440;&#x435;" STYLE_REF="pre_last_node" ID="ID_68565327" CREATED="1419105395975" MODIFIED="1419105486002"/>
<node TEXT="&#x412;&#x438;&#x434;&#x44b;" STYLE_REF="pre_last_node" ID="ID_1976347962" CREATED="1419105248910" MODIFIED="1419105460602">
<node TEXT="&#x421;&#x43f;&#x435;&#x446;&#x438;&#x430;&#x43b;&#x438;&#x437;&#x438;&#x440;&#x43e;&#x432;&#x430;&#x43d;&#x43d;&#x44b;&#x439;" ID="ID_746924274" CREATED="1419105328496" MODIFIED="1419105338070"/>
<node TEXT="&#x41f;&#x430;&#x440;&#x430;&#x43c;&#x435;&#x442;&#x440;&#x438;&#x447;&#x435;&#x441;&#x43a;&#x438;&#x439;" ID="ID_1209222111" CREATED="1419105339234" MODIFIED="1419105343400"/>
</node>
</node>
<node TEXT="&#x41a;&#x43b;&#x430;&#x441;&#x441;" STYLE_REF="orange_node" ID="ID_1832667403" CREATED="1419069311569" MODIFIED="1419143063882">
<hook URI="../../Images/class-2.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x41e;&#x43f;&#x438;&#x441;&#x430;&#x43d;&#x438;&#x435;" STYLE_REF="yellow_node" ID="ID_390739105" CREATED="1419105518867" MODIFIED="1419105589091">
<node TEXT="&#x41c;&#x43e;&#x434;&#x435;&#x43b;&#x44c; (&#x447;&#x435;&#x440;&#x442;&#x435;&#x436;) &#x435;&#x449;&#x435; &#x43d;&#x435; &#x441;&#x443;&#x449;&#x435;&#x441;&#x442;&#x432;&#x443;&#x44e;&#x449;&#x435;&#x439; &#x441;&#x443;&#x449;&#x43d;&#x43e;&#x441;&#x442;&#x438; (&#x43e;&#x431;&#x44a;&#x435;&#x43a;&#x442;&#x430;)" STYLE_REF="pre_last_node" ID="ID_990158029" CREATED="1419105542948" MODIFIED="1419105651833"/>
<node TEXT="&#x410;&#x431;&#x441;&#x442;&#x440;&#x430;&#x43a;&#x442;&#x43d;&#x44b;&#x439; &#x442;&#x438;&#x43f; &#x434;&#x430;&#x43d;&#x43d;&#x44b;&#x445;" STYLE_REF="pre_last_node" ID="ID_1439294153" CREATED="1419105571699" MODIFIED="1419105651831"/>
</node>
<node TEXT="&#x421;&#x43e;&#x441;&#x442;&#x430;&#x432;" STYLE_REF="yellow_node" ID="ID_79827953" CREATED="1419105524299" MODIFIED="1419105589105">
<node TEXT="&#x41f;&#x43e;&#x43b;&#x44f; &#x434;&#x430;&#x43d;&#x43d;&#x44b;&#x445;" STYLE_REF="pre_last_node" ID="ID_384114502" CREATED="1419105603438" MODIFIED="1419105651828"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1047;&#1072;&#1076;&#1072;&#1102;&#1090; &#1077;&#1075;&#1086; &#1089;&#1086;&#1089;&#1090;&#1086;&#1103;&#1085;&#1080;&#1077;
    </p>
  </body>
</html>
</richcontent>
<node TEXT="&#x41f;&#x435;&#x440;&#x435;&#x43c;&#x435;&#x43d;&#x43d;&#x44b;&#x435;" ID="ID_881709662" CREATED="1419105620133" MODIFIED="1419105624165"/>
<node TEXT="&#x41a;&#x43e;&#x43d;&#x441;&#x442;&#x430;&#x43d;&#x442;&#x44b;" ID="ID_1304447502" CREATED="1419105625344" MODIFIED="1419105628332"/>
</node>
<node TEXT="&#x41c;&#x435;&#x442;&#x43e;&#x434;&#x44b;" STYLE_REF="pre_last_node" ID="ID_1969311442" CREATED="1419105609447" MODIFIED="1419105651821">
<node TEXT="&#x424;&#x443;&#x43d;&#x43a;&#x446;&#x438;&#x438;" ID="ID_358562694" CREATED="1419105639534" MODIFIED="1419105643140"/>
<node TEXT="&#x41f;&#x440;&#x43e;&#x446;&#x435;&#x434;&#x443;&#x440;&#x44b;" ID="ID_490928381" CREATED="1419105644304" MODIFIED="1419105646852"/>
</node>
</node>
<node TEXT="&#x412;&#x438;&#x434;&#x44b;" STYLE_REF="yellow_node" ID="ID_968221853" CREATED="1419105515028" MODIFIED="1419105589110">
<node TEXT="&#x410;&#x431;&#x441;&#x442;&#x440;&#x430;&#x43a;&#x442;&#x43d;&#x44b;&#x439; &#x43a;&#x43b;&#x430;&#x441;&#x441;" STYLE_REF="pre_last_node" ID="ID_820220336" CREATED="1419105688953" MODIFIED="1419105708562"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1053;&#1077; &#1084;&#1086;&#1078;&#1077;&#1090; &#1073;&#1099;&#1090;&#1100; &#1089;&#1086;&#1079;&#1076;&#1072;&#1085;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="&#x41e;&#x431;&#x44a;&#x435;&#x43a;&#x442;" STYLE_REF="orange_node" ID="ID_901286743" CREATED="1419069315675" MODIFIED="1419143088235"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1069;&#1082;&#1079;&#1077;&#1084;&#1087;&#1083;&#1103;&#1088; &#1082;&#1083;&#1072;&#1089;&#1089;&#1072;
    </p>
  </body>
</html>
</richcontent>
<hook URI="../../Images/ufo-1.jpg" SIZE="1.0" NAME="ExternalObject"/>
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="120" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_1832667403" MIDDLE_LABEL="&#x42d;&#x43a;&#x437;&#x435;&#x43c;&#x43f;&#x43b;&#x44f;&#x440;" STARTINCLINATION="-40;-34;" ENDINCLINATION="14;19;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
<node TEXT="&#x418;&#x43d;&#x441;&#x442;&#x430;&#x43d;&#x446;&#x438;&#x438;&#x440;&#x43e;&#x432;&#x430;&#x43d;&#x438;&#x435;" STYLE_REF="pre_last_node" ID="ID_1432809605" CREATED="1419107274483" MODIFIED="1419107317102"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1057;&#1086;&#1079;&#1076;&#1072;&#1085;&#1080;&#1077; &#1101;&#1082;&#1079;&#1077;&#1084;&#1087;&#1083;&#1103;&#1088;&#1072; &#1082;&#1083;&#1072;&#1089;&#1089;&#1072;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&#x418;&#x43d;&#x438;&#x446;&#x438;&#x430;&#x43b;&#x438;&#x437;&#x430;&#x446;&#x438;&#x44f;" STYLE_REF="pre_last_node" ID="ID_757231880" CREATED="1419107284444" MODIFIED="1419107355797"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1055;&#1088;&#1080;&#1089;&#1074;&#1072;&#1080;&#1074;&#1072;&#1085;&#1080;&#1077; &#1085;&#1072;&#1095;&#1072;&#1083;&#1100;&#1085;&#1099;&#1093; &#1079;&#1085;&#1072;&#1095;&#1077;&#1085;&#1080;&#1081;
    </p>
    <p>
      &#1087;&#1086;&#1083;&#1103;&#1084; &#1086;&#1073;&#1098;&#1077;&#1082;&#1090;&#1072;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="&#x41f;&#x440;&#x43e;&#x442;&#x43e;&#x442;&#x438;&#x43f;" STYLE_REF="orange_node" ID="ID_1768262674" CREATED="1419069278733" MODIFIED="1419143050143">
<hook URI="../../Images/proto-1.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x41e;&#x431;&#x44a;&#x435;&#x43a;&#x442; &#x43e;&#x431;&#x440;&#x430;&#x437;&#x435;&#x446; &#x43f;&#x43e; &#x43e;&#x431;&#x440;&#x430;&#x437;&#x443; &#x438; &#x43f;&#x43e;&#x434;&#x43e;&#x431;&#x438;&#x44e;&#xa;&#x43a;&#x43e;&#x442;&#x43e;&#x440;&#x43e;&#x433;&#x43e; &#x441;&#x43e;&#x437;&#x434;&#x430;&#x44e;&#x442;&#x441;&#x44f; &#x434;&#x440;&#x443;&#x433;&#x438;&#x435; &#x43e;&#x431;&#x44a;&#x435;&#x43a;&#x442;&#x44b;" STYLE_REF="pre_last_node" ID="ID_586850394" CREATED="1419142971505" MODIFIED="1419143034555"/>
<node TEXT="&#x41e;&#x431;&#x44a;&#x435;&#x43a;&#x442;&#x44b; &#x43a;&#x43e;&#x43f;&#x438;&#x438; &#x43c;&#x43e;&#x433;&#x443;&#x442; &#x430;&#x432;&#x442;&#x43e;&#x43c;&#x430;&#x442;&#x438;&#x447;&#x435;&#x441;&#x43a;&#x438;&#xa;&#x43d;&#x430;&#x441;&#x43b;&#x435;&#x434;&#x43e;&#x432;&#x430;&#x442;&#x44c; &#x438;&#x437;&#x43c;&#x435;&#x43d;&#x435;&#x43d;&#x438;&#x44f; &#x432; &#x43f;&#x440;&#x43e;&#x442;&#x43e;&#x442;&#x438;&#x43f;&#x435;" STYLE_REF="pre_last_node" ID="ID_1827219352" CREATED="1419143001575" MODIFIED="1419143034575"/>
</node>
<node TEXT="&#x41d;&#x430;&#x441;&#x43b;&#x435;&#x434;&#x43e;&#x432;&#x430;&#x43d;&#x438;&#x435;" STYLE_REF="orange_node" ID="ID_724696759" CREATED="1419069320478" MODIFIED="1419144732713" VGAP="17"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1057;&#1074;&#1086;&#1081;&#1089;&#1090;&#1074;&#1086; &#1089;&#1080;&#1089;&#1090;&#1077;&#1084;&#1099; &#1087;&#1086;&#1079;&#1074;&#1086;&#1083;&#1103;&#1102;&#1097;&#1077;&#1077; &#1086;&#1087;&#1080;&#1089;&#1099;&#1074;&#1072;&#1090;&#1100;
    </p>
    <p>
      &#1085;&#1086;&#1074;&#1099;&#1081; &#1082;&#1083;&#1072;&#1089;&#1089; &#1085;&#1072; &#1086;&#1089;&#1085;&#1086;&#1074;&#1077; &#1091;&#1078;&#1077; &#1089;&#1091;&#1097;&#1077;&#1089;&#1090;&#1074;&#1091;&#1102;&#1097;&#1077;&#1075;&#1086;
    </p>
    <p>
      &#1089; &#1095;&#1072;&#1089;&#1090;&#1080;&#1095;&#1085;&#1099;&#1084; &#1080;&#1083;&#1080; &#1087;&#1086;&#1083;&#1085;&#1099;&#1084; &#1082;&#1086;&#1087;&#1080;&#1088;&#1086;&#1074;&#1072;&#1085;&#1080;&#1077;&#1084;
    </p>
    <p>
      &#1092;&#1091;&#1085;&#1082;&#1094;&#1080;&#1086;&#1085;&#1072;&#1083;&#1100;&#1085;&#1086;&#1089;&#1090;&#1080;
    </p>
  </body>
</html>
</richcontent>
<hook URI="../../Images/inheritance.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x422;&#x438;&#x43f;&#x44b; &#x43d;&#x430;&#x441;&#x43b;&#x435;&#x434;&#x43e;&#x432;&#x430;&#x43d;&#x438;&#x44f;" STYLE_REF="yellow_node" ID="ID_1930292803" CREATED="1419143102247" MODIFIED="1419143734209">
<node TEXT="&#x41f;&#x440;&#x43e;&#x441;&#x442;&#x43e;&#x435;" STYLE_REF="pre_last_node" ID="ID_1173927754" CREATED="1419143642917" MODIFIED="1419143676781"/>
<node TEXT="&#x41c;&#x43d;&#x43e;&#x436;&#x435;&#x441;&#x442;&#x432;&#x435;&#x43d;&#x43d;&#x43e;&#x435;" STYLE_REF="pre_last_node" ID="ID_252931666" CREATED="1419143648659" MODIFIED="1419143676773"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1050;&#1083;&#1072;&#1089;&#1089; &#1087;&#1086;&#1083;&#1091;&#1095;&#1072;&#1077;&#1090; &#1084;&#1077;&#1090;&#1086;&#1076;&#1099; &#1074;&#1089;&#1077;&#1093; &#1087;&#1088;&#1077;&#1076;&#1082;&#1086;&#1074;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="&#x41e;&#x442;&#x43d;&#x43e;&#x448;&#x435;&#x43d;&#x438;&#x435; &#x43c;&#x435;&#x436;&#x434;&#x443; &#x43a;&#x43b;&#x430;&#x441;&#x441;&#x430;&#x43c;&#x438;" STYLE_REF="yellow_node" ID="ID_1242452309" CREATED="1419143123580" MODIFIED="1419143135363">
<node TEXT="&#x411;&#x430;&#x437;&#x43e;&#x432;&#x44b;&#x439;, &#x440;&#x43e;&#x434;&#x438;&#x442;&#x435;&#x43b;&#x44c;&#x441;&#x43a;&#x438;&#x439;, &#x441;&#x443;&#x43f;&#x435;&#x440;&#x43a;&#x43b;&#x430;&#x441;&#x441;" STYLE_REF="pre_last_node" ID="ID_751279475" CREATED="1419143687466" MODIFIED="1419143723591"/>
<node TEXT="&#x41f;&#x43e;&#x442;&#x43e;&#x43c;&#x43e;&#x43a;, &#x43d;&#x430;&#x441;&#x43b;&#x435;&#x434;&#x43d;&#x438;&#x43a;, &#x43f;&#x440;&#x43e;&#x438;&#x437;&#x432;&#x43e;&#x434;&#x43d;&#x44b;&#x439;" STYLE_REF="pre_last_node" ID="ID_298286830" CREATED="1419143705412" MODIFIED="1419143723599"/>
</node>
</node>
</node>
<node TEXT="&#x41f;&#x440;&#x43e;&#x435;&#x43a;&#x442;&#x438;&#x440;&#x43e;&#x432;&#x430;&#x43d;&#x438;&#x435;" STYLE_REF="second_node" POSITION="left" ID="ID_1579153365" CREATED="1419066233586" MODIFIED="1419107491986" VGAP="8" HGAP="60">
<edge COLOR="#000033"/>
<hook URI="../../Images/design.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x42d;&#x442;&#x430;&#x43f;&#x44b;" STYLE_REF="yellow_node" ID="ID_981681733" CREATED="1419066401162" MODIFIED="1419066507306">
<node TEXT="1. &#x418;&#x437; &#x43a;&#x430;&#x43a;&#x438;&#x445; &#x447;&#x430;&#x441;&#x442;&#x435;&#x439; &#x441;&#x43e;&#x441;&#x442;&#x43e;&#x438;&#x442;" STYLE_REF="pre_last_node" ID="ID_1567270765" CREATED="1419066419647" MODIFIED="1419066496171"/>
<node TEXT="2. &#x41a;&#x430;&#x43a;&#x430;&#x44f; &#x43e;&#x442;&#x432;&#x435;&#x442;&#x441;&#x442;&#x432;&#x435;&#x43d;&#x43d;&#x43e;&#x441;&#x442;&#x44c; &#x43d;&#x430; &#x43a;&#x430;&#x436;&#x434;&#x43e;&#x439; &#x447;&#x430;&#x441;&#x442;&#x438;" STYLE_REF="pre_last_node" ID="ID_713826051" CREATED="1419066443178" MODIFIED="1419066532237"/>
</node>
<node TEXT="&#x41f;&#x440;&#x43e;&#x431;&#x43b;&#x435;&#x43c;&#x44b;" STYLE_REF="yellow_node" ID="ID_486839992" CREATED="1419066412431" MODIFIED="1419066518282">
<node TEXT="&#x425;&#x440;&#x443;&#x43f;&#x43a;&#x43e;&#x441;&#x442;&#x44c; &#x431;&#x430;&#x437;&#x43e;&#x432;&#x43e;&#x433;&#x43e; &#x43a;&#x43b;&#x430;&#x441;&#x441;&#x430;" STYLE_REF="pre_last_node" ID="ID_981874511" CREATED="1419066468033" MODIFIED="1419066496180"/>
</node>
</node>
<node TEXT="SOLID" STYLE_REF="second_node" POSITION="left" ID="ID_77804900" CREATED="1419066247353" MODIFIED="1419107487809" HGAP="70" VSHIFT="-10">
<edge COLOR="#006666"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1055;&#1088;&#1080;&#1085;&#1094;&#1080;&#1087;&#1099; &#1076;&#1080;&#1079;&#1072;&#1081;&#1085;&#1072; &#1087;&#1088;&#1086;&#1077;&#1082;&#1090;&#1080;&#1088;&#1086;&#1074;&#1072;&#1085;&#1080;&#1103; &#1082;&#1083;&#1072;&#1089;&#1089;&#1086;&#1074;
    </p>
  </body>
</html>
</richcontent>
<hook URI="../../Images/design-1.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Single responsibility principe" STYLE_REF="pre_last_node" ID="ID_1815051901" CREATED="1419068614365" MODIFIED="1419068770835">
<font BOLD="true"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1050;&#1072;&#1078;&#1076;&#1099;&#1081; &#1082;&#1083;&#1072;&#1089;&#1089; &#1076;&#1086;&#1083;&#1078;&#1077;&#1085; &#1080;&#1084;&#1077;&#1090;&#1100; &#1086;&#1076;&#1085;&#1091; &#1077;&#1076;&#1080;&#1085;&#1089;&#1090;&#1074;&#1077;&#1085;&#1085;&#1091;&#1102;
    </p>
    <p>
      &#1086;&#1073;&#1103;&#1079;&#1072;&#1085;&#1086;&#1089;&#1090;&#1100; (&#1087;&#1088;&#1080;&#1095;&#1080;&#1085;&#1091; &#1076;&#1083;&#1103; &#1084;&#1086;&#1076;&#1080;&#1092;&#1080;&#1082;&#1072;&#1094;&#1080;&#1080;)
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Open closed principe" STYLE_REF="pre_last_node" ID="ID_1728326483" CREATED="1419068635411" MODIFIED="1419068804154">
<font BOLD="true"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1057;&#1091;&#1097;&#1085;&#1086;&#1089;&#1090;&#1080; &#1086;&#1090;&#1082;&#1088;&#1099;&#1090;&#1099; &#1076;&#1083;&#1103; &#1088;&#1072;&#1089;&#1096;&#1080;&#1088;&#1077;&#1085;&#1080;&#1103;
    </p>
    <p>
      &#1085;&#1086; &#1079;&#1072;&#1082;&#1088;&#1099;&#1090;&#1099; &#1076;&#1083;&#1103; &#1080;&#1079;&#1084;&#1077;&#1085;&#1077;&#1085;&#1080;&#1103;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Liskov substitution principe" STYLE_REF="pre_last_node" ID="ID_164413131" CREATED="1419068647215" MODIFIED="1419068846117">
<font BOLD="true"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1054;&#1073;&#1098;&#1077;&#1082;&#1090;&#1099; &#1074; &#1087;&#1088;&#1086;&#1075; &#1084;&#1086;&#1075;&#1091;&#1090; &#1073;&#1099;&#1090;&#1100; &#1079;&#1072;&#1084;&#1077;&#1085;&#1077;&#1085;&#1099; &#1080;&#1093;
    </p>
    <p>
      &#1085;&#1072;&#1089;&#1083;&#1077;&#1076;&#1085;&#1080;&#1082;&#1072;&#1084;&#1080; &#1073;&#1077;&#1079; &#1080;&#1079;&#1084;&#1077;&#1085;&#1077;&#1085;&#1080;&#1103; &#1089;&#1074;&#1086;&#1081;&#1089;&#1090;&#1074; &#1087;&#1088;&#1086;&#1075;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Interface segregation principe" STYLE_REF="pre_last_node" ID="ID_756718961" CREATED="1419068663471" MODIFIED="1419069008560">
<font BOLD="true"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1052;&#1085;&#1086;&#1075;&#1086; &#1089;&#1087;&#1077;&#1094;&#1080;&#1072;&#1083;&#1080;&#1079;&#1080;&#1088;&#1086;&#1074;&#1072;&#1085;&#1085;&#1099;&#1093; &#1080;&#1085;&#1090;&#1077;&#1088;&#1092;&#1077;&#1081;&#1089;&#1086;&#1074; &#1083;&#1091;&#1090;&#1096;&#1077; &#1095;&#1077;&#1084; 1 &#1091;&#1085;&#1080;&#1074;&#1077;&#1088;&#1089;&#1072;&#1083;&#1100;&#1085;&#1099;&#1081;.
    </p>
    <p>
      &#1042; &#1083;&#1102;&#1073;&#1086;&#1084; &#1080;&#1085;&#1090;&#1077;&#1088;&#1092;&#1077;&#1081;&#1089;&#1077; &#1085;&#1077; &#1076;&#1086;&#1083;&#1078;&#1085;&#1086; &#1073;&#1099;&#1090;&#1100; &#1085;&#1077; &#1080;&#1089;&#1087;&#1086;&#1083;&#1100;&#1079;&#1091;&#1074;&#1072;&#1077;&#1084;&#1099;&#1093; &#1084;&#1077;&#1090;&#1086;&#1076;&#1086;&#1074;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Dependency inversion principe" STYLE_REF="pre_last_node" ID="ID_1498316613" CREATED="1419068679178" MODIFIED="1419068962158">
<font BOLD="true"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1052;&#1086;&#1076;&#1091;&#1083;&#1080; &#1074;&#1077;&#1088;&#1093;&#1085;&#1077;&#1075;&#1086; &#1091;&#1088;&#1086;&#1074;&#1085;&#1103; &#1085;&#1077; &#1079;&#1072;&#1074;&#1080;&#1089;&#1103;&#1090; &#1086;&#1090; &#1084;&#1086;&#1076;&#1091;&#1083;&#1077;&#1081; &#1085;&#1080;&#1078;&#1085;&#1077;&#1075;&#1086; &#1091;&#1088;&#1086;&#1074;&#1085;&#1103;.
    </p>
    <p>
      &#1040;&#1073;&#1089;&#1090;&#1088;&#1072;&#1082;&#1094;&#1080;&#1080; &#1085;&#1077; &#1079;&#1072;&#1074;&#1080;&#1089;&#1103;&#1090; &#1086;&#1090; &#1076;&#1077;&#1090;&#1072;&#1083;&#1077;&#1081;, &#1076;&#1077;&#1090;&#1072;&#1083;&#1080; &#1079;&#1072;&#1074;&#1080;&#1089;&#1103;&#1090; &#1086;&#1090; &#1072;&#1073;&#1089;&#1090;&#1088;&#1072;&#1082;&#1094;&#1080;&#1081;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="&#x421;&#x43d;&#x438;&#x436;&#x435;&#x43d;&#x438;&#x435; &#x43f;&#x440;&#x43e;&#x438;&#x437;&#x432;&#x43e;&#x434;&#x438;&#x442;&#x435;&#x43b;&#x44c;&#x43d;&#x43e;&#x441;&#x442;&#x438;" STYLE_REF="second_node" POSITION="left" ID="ID_179553713" CREATED="1419066330091" MODIFIED="1419107481334" HGAP="80">
<edge COLOR="#660066"/>
<hook URI="../../Images/tortoise.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="&#x414;&#x438;&#x43d;&#x430;&#x43c;&#x438;&#x447;&#x435;&#x441;&#x43a;&#x43e;&#x435; &#x441;&#x432;&#x44f;&#x437;&#x44b;&#x432;&#x430;&#x43d;&#x438;&#x435; &#x43c;&#x435;&#x442;&#x43e;&#x434;&#x43e;&#x432;" STYLE_REF="pre_last_node" ID="ID_1854405631" CREATED="1419068270154" MODIFIED="1419068509316"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1044;&#1083;&#1103; &#1086;&#1073;&#1077;&#1089;&#1087;&#1077;&#1095;&#1077;&#1085;&#1080;&#1103; &#1087;&#1086;&#1083;&#1080;&#1084;&#1086;&#1088;&#1092;&#1080;&#1079;&#1084;&#1072;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&#x417;&#x43d;&#x430;&#x447;&#x438;&#x442;&#x435;&#x43b;&#x44c;&#x43d;&#x430;&#x44f; &#x433;&#x43b;&#x443;&#x431;&#x438;&#x43d;&#x430; &#x430;&#x431;&#x441;&#x442;&#x440;&#x430;&#x43a;&#x446;&#x438;&#x438;" STYLE_REF="pre_last_node" ID="ID_309970634" CREATED="1419068329913" MODIFIED="1419068509328"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      1 &#1076;&#1077;&#1081;&#1089;&#1090;&#1074;&#1080;&#1077; =&gt; &#1084;&#1085;&#1086;&#1078;&#1077;&#1089;&#1090;&#1074;&#1086; &#1086;&#1073;&#1088;&#1072;&#1097;&#1077;&#1085;&#1080;&#1081;
    </p>
    <p>
      &#1082; &#1084;&#1077;&#1090;&#1086;&#1076;&#1072;&#1084; &#1085;&#1080;&#1079;&#1096;&#1080;&#1093; &#1091;&#1088;&#1086;&#1074;&#1085;&#1077;&#1081;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&#x41d;&#x430;&#x441;&#x43b;&#x435;&#x434;&#x43e;&#x432;&#x430;&#x43d;&#x438;&#x435; &#x440;&#x430;&#x437;&#x43c;&#x44b;&#x432;&#x430;&#x435;&#x442; &#x43a;&#x43e;&#x434;" STYLE_REF="pre_last_node" ID="ID_615298536" CREATED="1419068391471" MODIFIED="1419068509336">
<node TEXT="&#x41f;&#x430;&#x434;&#x430;&#x435;&#x442; &#x43f;&#x440;&#x43e;&#x438;&#x437;&#x432;&#x43e;&#x434;&#x438;&#x442;&#x435;&#x43b;&#x44c;&#x43d;&#x43e;&#x441;&#x442;&#x44c; &#x432; &#x441;&#x438;&#x441;&#x442;&#x435;&#x43c;&#x430;&#x445;&#xa;&#x441;&#x43e; &#x441;&#x442;&#x440;&#x430;&#x43d;&#x438;&#x447;&#x43d;&#x43e;&#x439; &#x43f;&#x430;&#x43c;&#x44f;&#x442;&#x44c;&#x44e;" ID="ID_1879378243" CREATED="1419068450441" MODIFIED="1419068489656"/>
<node TEXT="&#x421;&#x43d;&#x438;&#x436;&#x430;&#x435;&#x442; &#x441;&#x43a;&#x43e;&#x440;&#x43e;&#x441;&#x442;&#x44c; &#x442;&#x440;&#x430;&#x43d;&#x441;&#x43b;&#x44f;&#x446;&#x438;&#x438;" ID="ID_317068362" CREATED="1419068490943" MODIFIED="1419068502773"/>
</node>
<node TEXT="&#x418;&#x43d;&#x43a;&#x430;&#x43f;&#x441;&#x443;&#x43b;&#x44f;&#x446;&#x438;&#x44f; &#x441;&#x43d;&#x438;&#x436;&#x430;&#x435;&#x442; &#x441;&#x43a;&#x43e;&#x440;&#x43e;&#x441;&#x442;&#x44c;&#xa;&#x434;&#x43e;&#x441;&#x442;&#x443;&#x43f;&#x430; &#x43a; &#x434;&#x430;&#x43d;&#x43d;&#x44b;&#x43c;" STYLE_REF="pre_last_node" ID="ID_1409192876" CREATED="1419068404363" MODIFIED="1419068509343"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1058;&#1072;&#1082; &#1082;&#1072;&#1082; &#1080;&#1089;&#1087;&#1086;&#1083;&#1100;&#1079;&#1091;&#1077;&#1090; &#1072;&#1082;&#1089;&#1077;&#1089;&#1089;&#1086;&#1088;&#1099;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
</map>
