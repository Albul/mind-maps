<map version="freeplane 1.3.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="XML" STYLE_REF="main_node" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1430148156312" VGAP="23">
<hook NAME="AutomaticEdgeColor" COUNTER="1"/>
<hook NAME="MapStyle" zoom="1.003">
    <properties show_icon_for_attributes="true" show_note_icons="true"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
<stylenode TEXT="main_node" BACKGROUND_COLOR="#ccccff">
<font SIZE="18" BOLD="true"/>
<edge STYLE="sharp_bezier" WIDTH="4"/>
</stylenode>
<stylenode TEXT="second_node" BACKGROUND_COLOR="#00cccc" STYLE="bubble">
<font SIZE="14"/>
</stylenode>
<stylenode TEXT="third_node" BACKGROUND_COLOR="#ffbdf4" STYLE="bubble">
<font SIZE="11" BOLD="true"/>
</stylenode>
<stylenode TEXT="orange_node" BACKGROUND_COLOR="#ff9900" STYLE="bubble">
<font BOLD="true"/>
</stylenode>
<stylenode TEXT="gray_node" BACKGROUND_COLOR="#9ec19e">
<font BOLD="true"/>
</stylenode>
<stylenode TEXT="yellow_node" BACKGROUND_COLOR="#ffff66" STYLE="bubble">
<edge WIDTH="1"/>
</stylenode>
<stylenode TEXT="example_node" BACKGROUND_COLOR="#afcdfa">
<edge STYLE="bezier" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="pre_last_node" BACKGROUND_COLOR="#99ff99" STYLE="bubble">
<edge STYLE="bezier" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="last_node" BACKGROUND_COLOR="#ffffff">
<edge STYLE="bezier" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="warning_node" BACKGROUND_COLOR="#fda6a6">
<icon BUILTIN="messagebox_warning"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook URI="../../Images/xml-large.png" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Introduction" STYLE_REF="second_node" POSITION="left" ID="ID_915650721" CREATED="1430107064235" MODIFIED="1430148648166" VSHIFT="-30">
<edge COLOR="#0000ff"/>
<hook URI="../../Images/beginning.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="XML is" STYLE_REF="yellow_node" ID="ID_1725614367" CREATED="1430107117339" MODIFIED="1430107334039">
<node TEXT="Stands for EXtensible Markup Language" STYLE_REF="pre_last_node" ID="ID_397847035" CREATED="1430107148009" MODIFIED="1430107329701"/>
<node TEXT="A markup language much like HTML" STYLE_REF="pre_last_node" ID="ID_1781532373" CREATED="1430107160634" MODIFIED="1430107329701"/>
<node TEXT="Was designed to describe data, not to display data" STYLE_REF="pre_last_node" ID="ID_1646932036" CREATED="1430107177369" MODIFIED="1430107329701"/>
<node TEXT="Tags are not predefined. You must define your own tags" STYLE_REF="pre_last_node" ID="ID_567265217" CREATED="1430107187993" MODIFIED="1430107329701"/>
<node TEXT="Designed to be self-descriptive" STYLE_REF="pre_last_node" ID="ID_311879015" CREATED="1430107196249" MODIFIED="1430107329701"/>
<node TEXT="A W3C Recommendation" STYLE_REF="pre_last_node" ID="ID_511905847" CREATED="1430107209728" MODIFIED="1430107329701"/>
<node TEXT="A software- and hardware-independent tool for carrying information" STYLE_REF="pre_last_node" ID="ID_1179712304" CREATED="1430107269689" MODIFIED="1430107493640"/>
<node TEXT="A document which does not DO anything, it is just information wrapped in tags" STYLE_REF="pre_last_node" ID="ID_1679381597" CREATED="1430107399753" MODIFIED="1430107524758"/>
</node>
<node TEXT="The Difference Between XML and HTML" STYLE_REF="yellow_node" ID="ID_1905691659" CREATED="1430107280009" MODIFIED="1430107334054">
<node TEXT="XML is not a replacement for HTML" STYLE_REF="pre_last_node" ID="ID_1505337633" CREATED="1430107286554" MODIFIED="1430107360124"/>
<node TEXT="XML was designed to describe data, with focus on what data is" STYLE_REF="pre_last_node" ID="ID_882463133" CREATED="1430107314746" MODIFIED="1430107360124"/>
<node TEXT="HTML was designed to display data, with focus on how data looks" STYLE_REF="pre_last_node" ID="ID_1334465244" CREATED="1430107321641" MODIFIED="1430107360124"/>
<node TEXT="HTML is about displaying information, while XML is about carrying information" STYLE_REF="pre_last_node" ID="ID_1069505775" CREATED="1430107301778" MODIFIED="1430107360124"/>
</node>
</node>
<node TEXT="Attributes" STYLE_REF="second_node" POSITION="right" ID="ID_404728604" CREATED="1430119466948" MODIFIED="1430138876903">
<edge COLOR="#7c0000"/>
<node TEXT="Attributes" STYLE_REF="orange_node" ID="ID_575522299" CREATED="1430125471828" MODIFIED="1430129567094">
<node TEXT="Attributes provide additional information about elements" STYLE_REF="pre_last_node" ID="ID_676576714" CREATED="1430125478709" MODIFIED="1430129782237"/>
<node TEXT="Attributes often provide information that is not a part of the data, but can be important to the software that wants to manipulate the element" STYLE_REF="pre_last_node" ID="ID_472153936" CREATED="1430125494763" MODIFIED="1430129777636"/>
<node TEXT="&lt;file type=&quot;gif&quot;&gt;computer.gif&lt;/file&gt;" STYLE_REF="example_node" ID="ID_815832892" CREATED="1430125523087" MODIFIED="1430129690263"/>
</node>
<node TEXT="Attributes Must be Quoted" STYLE_REF="orange_node" ID="ID_843269482" CREATED="1430126289106" MODIFIED="1430129567095">
<node TEXT="Either single or double quotes can be used" STYLE_REF="pre_last_node" ID="ID_1524277804" CREATED="1430126296221" MODIFIED="1430129777636"/>
<node TEXT="&lt;person gender=&quot;female&quot;&gt;" STYLE_REF="example_node" ID="ID_1789274023" CREATED="1430126305954" MODIFIED="1430129690263">
<hook NAME="FirstGroupNode"/>
</node>
<node TEXT="&lt;person gender=&apos;female&apos;&gt;" STYLE_REF="example_node" ID="ID_1459088837" CREATED="1430126313202" MODIFIED="1430129690263"/>
<node TEXT="The same" STYLE_REF="pre_last_node" ID="ID_1165857091" CREATED="1430126320343" MODIFIED="1430129777636">
<hook NAME="SummaryNode"/>
</node>
<node TEXT="&lt;gangster name=&apos;George &quot;Shotgun&quot; Ziegler&apos;&gt;" STYLE_REF="example_node" ID="ID_849270606" CREATED="1430126334100" MODIFIED="1430129690263"/>
<node TEXT="&lt;gangster name=&quot;George &amp;quot;Shotgun&amp;quot; Ziegler&quot;&gt;" STYLE_REF="example_node" ID="ID_7822951" CREATED="1430126339555" MODIFIED="1430129690263"/>
</node>
<node TEXT="Elements vs. Attributes" STYLE_REF="orange_node" ID="ID_1582503791" CREATED="1430126821801" MODIFIED="1430129567095">
<node TEXT="&lt;person gender=&quot;female&quot;&gt;&#xa;  &lt;firstname&gt;Anna&lt;/firstname&gt;&#xa;  &lt;lastname&gt;Smith&lt;/lastname&gt;&#xa;&lt;/person&gt;" STYLE_REF="example_node" ID="ID_1424276683" CREATED="1430126830938" MODIFIED="1430129690259">
<hook NAME="FirstGroupNode"/>
</node>
<node TEXT="&lt;person&gt;&#xa;  &lt;gender&gt;female&lt;/gender&gt;&#xa;  &lt;firstname&gt;Anna&lt;/firstname&gt;&#xa;  &lt;lastname&gt;Smith&lt;/lastname&gt;&#xa;&lt;/person&gt;" STYLE_REF="example_node" ID="ID_1894450185" CREATED="1430126836818" MODIFIED="1430129690261"/>
<node TEXT="In the first example gender is an attribute. In the last, gender is an element. Both examples provide the same information" STYLE_REF="pre_last_node" ID="ID_1419352675" CREATED="1430127087638" MODIFIED="1430129777635">
<hook NAME="SummaryNode"/>
</node>
<node TEXT="There are no rules about when to use attributes or when to use elements" STYLE_REF="pre_last_node" ID="ID_1566144986" CREATED="1430127111364" MODIFIED="1430129777634"/>
<node TEXT="Attributes are handy in HTML. In XML my advice is to avoid them. Use elements instead" STYLE_REF="pre_last_node" ID="ID_198755479" CREATED="1430127121276" MODIFIED="1430129777634"/>
</node>
<node TEXT="Avoid XML Attributes?" STYLE_REF="orange_node" ID="ID_893883277" CREATED="1430128478395" MODIFIED="1430129567096">
<node TEXT="Problems with using attributes" STYLE_REF="yellow_node" ID="ID_1725131216" CREATED="1430129476509" MODIFIED="1430129788662">
<node TEXT="Attributes cannot contain multiple values (elements can)" STYLE_REF="pre_last_node" ID="ID_949509539" CREATED="1430129482371" MODIFIED="1430129777634"/>
<node TEXT="Attributes cannot contain tree structures (elements can)" STYLE_REF="pre_last_node" ID="ID_1076276316" CREATED="1430129482371" MODIFIED="1430129777634"/>
<node TEXT="Attributes are not easily expandable (for future changes)" STYLE_REF="pre_last_node" ID="ID_1177699852" CREATED="1430129482371" MODIFIED="1430129777634"/>
<node TEXT="Attributes are difficult to read and maintain" STYLE_REF="pre_last_node" ID="ID_1345683748" CREATED="1430129511875" MODIFIED="1430129777633"/>
</node>
<node TEXT="Use elements for data. Use attributes for information that is not relevant to the data" STYLE_REF="pre_last_node" ID="ID_473952271" CREATED="1430129521123" MODIFIED="1430129777632"/>
<node TEXT="&lt;note day=&quot;10&quot; month=&quot;01&quot; year=&quot;2008&quot;&#xa;to=&quot;Tove&quot; from=&quot;Jani&quot; heading=&quot;Reminder&quot;&#xa;body=&quot;Don&apos;t forget me this weekend!&quot;&gt;&#xa;&lt;/note&gt;" STYLE_REF="example_node" ID="ID_1228747986" CREATED="1430129535768" MODIFIED="1430129690258"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Don't do this
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
<node TEXT="Attributes for Metadata" STYLE_REF="orange_node" ID="ID_1274013312" CREATED="1430129554883" MODIFIED="1430129793055">
<node TEXT="The id attributes above are for identifying the different notes. It is not a part of the note itself" STYLE_REF="pre_last_node" ID="ID_447478698" CREATED="1430129649284" MODIFIED="1430129777630"/>
<node TEXT="&lt;messages&gt;&#xa;  &lt;note id=&quot;501&quot;&gt;&#xa;    &lt;to&gt;Tove&lt;/to&gt;&#xa;    &lt;from&gt;Jani&lt;/from&gt;&#xa;    &lt;heading&gt;Reminder&lt;/heading&gt;&#xa;    &lt;body&gt;Don&apos;t forget me this weekend!&lt;/body&gt;&#xa;  &lt;/note&gt;&#xa;  &lt;note id=&quot;502&quot;&gt;&#xa;    &lt;to&gt;Jani&lt;/to&gt;&#xa;    &lt;from&gt;Tove&lt;/from&gt;&#xa;    &lt;heading&gt;Re: Reminder&lt;/heading&gt;&#xa;    &lt;body&gt;I will not&lt;/body&gt;&#xa;  &lt;/note&gt;&#xa;&lt;/messages&gt;" STYLE_REF="example_node" ID="ID_652473461" CREATED="1430129654801" MODIFIED="1430129793054"/>
</node>
<node TEXT="Ways" STYLE_REF="orange_node" ID="ID_1616400510" CREATED="1430129715073" MODIFIED="1430129762390">
<node TEXT="&lt;note date=&quot;2008-01-10&quot;&gt;&#xa;  &lt;to&gt;Tove&lt;/to&gt;&#xa;  &lt;from&gt;Jani&lt;/from&gt;&#xa;  &lt;heading&gt;Reminder&lt;/heading&gt;&#xa;  &lt;body&gt;Don&apos;t forget me this weekend!&lt;/body&gt;&#xa;&lt;/note&gt;" STYLE_REF="example_node" ID="ID_1130892986" CREATED="1430129721106" MODIFIED="1430129756576"/>
<node TEXT="&lt;note&gt;&#xa;  &lt;date&gt;2008-01-10&lt;/date&gt;&#xa;  &lt;to&gt;Tove&lt;/to&gt;&#xa;  &lt;from&gt;Jani&lt;/from&gt;&#xa;  &lt;heading&gt;Reminder&lt;/heading&gt;&#xa;  &lt;body&gt;Don&apos;t forget me this weekend!&lt;/body&gt;&#xa;&lt;/note&gt;" STYLE_REF="example_node" ID="ID_893479615" CREATED="1430129738043" MODIFIED="1430129762389"/>
<node TEXT="&lt;note&gt;&#xa;  &lt;date&gt;&#xa;    &lt;year&gt;2008&lt;/year&gt;&#xa;    &lt;month&gt;01&lt;/month&gt;&#xa;    &lt;day&gt;10&lt;/day&gt;&#xa;  &lt;/date&gt;&#xa;  &lt;to&gt;Tove&lt;/to&gt;&#xa;  &lt;from&gt;Jani&lt;/from&gt;&#xa;  &lt;heading&gt;Reminder&lt;/heading&gt;&#xa;  &lt;body&gt;Don&apos;t forget me this weekend!&lt;/body&gt;&#xa;&lt;/note&gt;" STYLE_REF="example_node" ID="ID_1480089983" CREATED="1430129745281" MODIFIED="1430129756574"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Prefered
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
</node>
<node TEXT="How to use" STYLE_REF="second_node" POSITION="left" ID="ID_42720966" CREATED="1430107463929" MODIFIED="1430148573508">
<edge COLOR="#009933"/>
<hook URI="../../Images/howto.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="XML Simplifies" STYLE_REF="orange_node" ID="ID_1640230150" CREATED="1430107639577" MODIFIED="1430107815504">
<node TEXT="XML data is stored in plain text format" STYLE_REF="pre_last_node" ID="ID_1552622825" CREATED="1430107651074" MODIFIED="1430107827223"/>
<node TEXT="Data Sharing" STYLE_REF="yellow_node" ID="ID_591824195" CREATED="1430107688833" MODIFIED="1430107820504">
<node TEXT="This makes it much easier to create data&#xa;that can be shared by different applications" STYLE_REF="pre_last_node" ID="ID_1484274699" CREATED="1430107665617" MODIFIED="1430107827223"/>
</node>
<node TEXT="Data Transport" STYLE_REF="yellow_node" ID="ID_257526573" CREATED="1430107696809" MODIFIED="1430107820504">
<node TEXT="One of the most time-consuming challenges for developers is to exchange data between incompatible systems over the Internet" STYLE_REF="pre_last_node" ID="ID_1191612578" CREATED="1430107730017" MODIFIED="1430107827223"/>
<node TEXT="Exchanging data as XML greatly reduces this complexity, since the data can be read by different incompatible applications" STYLE_REF="pre_last_node" ID="ID_695024686" CREATED="1430107739225" MODIFIED="1430107827207"/>
</node>
<node TEXT="Platform Changes" STYLE_REF="yellow_node" ID="ID_131724596" CREATED="1430107751081" MODIFIED="1430107820504">
<node TEXT="This makes it easier to expand or upgrade to new operating systems, new applications, or new browsers, without losing data" STYLE_REF="pre_last_node" ID="ID_1744437186" CREATED="1430107789354" MODIFIED="1430107854310"/>
</node>
</node>
<node TEXT="Internet Languages Written in XML" STYLE_REF="orange_node" ID="ID_687703678" CREATED="1430107886233" MODIFIED="1430139263275">
<node TEXT="XHTML" STYLE_REF="pre_last_node" ID="ID_863197726" CREATED="1430107891594" MODIFIED="1430107903441"/>
<node TEXT="XML Schema" STYLE_REF="pre_last_node" ID="ID_55207601" CREATED="1430107891594" MODIFIED="1430107903441"/>
<node TEXT="SVG" STYLE_REF="pre_last_node" ID="ID_1761704343" CREATED="1430107891594" MODIFIED="1430107903441"/>
<node TEXT="WSDL" STYLE_REF="pre_last_node" ID="ID_85730615" CREATED="1430107891594" MODIFIED="1430107903441"/>
<node TEXT="RSS" STYLE_REF="pre_last_node" ID="ID_1162563422" CREATED="1430107891594" MODIFIED="1430107903441"/>
</node>
</node>
<node TEXT="XML Tree" STYLE_REF="second_node" POSITION="left" ID="ID_1775315473" CREATED="1430118146782" MODIFIED="1430148327696">
<edge COLOR="#ff00ff"/>
<hook URI="../../Images/tree.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Tree Structure" STYLE_REF="orange_node" ID="ID_1008645406" CREATED="1430118279854" MODIFIED="1430139252509">
<node TEXT="XML documents form a tree structure that starts at &quot;the root&quot; and branches to &quot;the leaves&quot;" STYLE_REF="pre_last_node" ID="ID_719738597" CREATED="1430118165591" MODIFIED="1430118760154"/>
<node TEXT="XML documents must contain a root element.&#xa;This element is &quot;the parent&quot; of all other elements" STYLE_REF="pre_last_node" ID="ID_753381948" CREATED="1430118289397" MODIFIED="1430118736860"/>
<node TEXT="The terms parent, child, and sibling are used to describe the relationships between elements. Parent elements have children. Children on the same level are called siblings" STYLE_REF="pre_last_node" ID="ID_639428532" CREATED="1430118497240" MODIFIED="1430118748610"/>
<node TEXT="All elements can have text content and attributes" STYLE_REF="pre_last_node" ID="ID_1093088437" CREATED="1430118340357" MODIFIED="1430118455273"/>
</node>
<node TEXT="Examples" STYLE_REF="orange_node" ID="ID_1609329494" CREATED="1430118387942" MODIFIED="1430139252508">
<node TEXT="&lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot;?&gt;&#xa;&lt;note&gt;&#xa;  &lt;to&gt;Tove&lt;/to&gt;&#xa;  &lt;from&gt;Jani&lt;/from&gt;&#xa;  &lt;heading&gt;Reminder&lt;/heading&gt;&#xa;  &lt;body&gt;Don&apos;t forget me this weekend!&lt;/body&gt;&#xa;&lt;/note&gt;" STYLE_REF="example_node" ID="ID_54960248" CREATED="1430118202660" MODIFIED="1430118448195">
<node TEXT="The first line is the XML declaration. It defines the XML version (1.0)" STYLE_REF="pre_last_node" ID="ID_991246939" CREATED="1430118209071" MODIFIED="1430118455273">
<node TEXT="&lt;note&gt;" ID="ID_779807721" CREATED="1430118250550" MODIFIED="1430118250800"/>
</node>
<node TEXT="The next line describes the root element of the document" STYLE_REF="pre_last_node" ID="ID_1543148550" CREATED="1430118218189" MODIFIED="1430118455273"/>
<node TEXT="The next 4 lines describe 4 child elements of the root" STYLE_REF="pre_last_node" ID="ID_1041462202" CREATED="1430118229485" MODIFIED="1430118455273"/>
<node TEXT="And finally the last line defines the end of the root element" STYLE_REF="pre_last_node" ID="ID_1403085528" CREATED="1430118239181" MODIFIED="1430118455273">
<node TEXT="&lt;/note&gt;" ID="ID_1818516219" CREATED="1430118245437" MODIFIED="1430118245875"/>
</node>
</node>
<node TEXT="&lt;bookstore&gt;&#xa;  &lt;book category=&quot;COOKING&quot;&gt;&#xa;    &lt;title lang=&quot;en&quot;&gt;Everyday Italian&lt;/title&gt;&#xa;    &lt;author&gt;Giada De Laurentiis&lt;/author&gt;&#xa;    &lt;year&gt;2005&lt;/year&gt;&#xa;    &lt;price&gt;30.00&lt;/price&gt;&#xa;  &lt;/book&gt;&#xa;  &lt;book category=&quot;CHILDREN&quot;&gt;&#xa;    &lt;title lang=&quot;en&quot;&gt;Harry Potter&lt;/title&gt;&#xa;    &lt;author&gt;J K. Rowling&lt;/author&gt;&#xa;    &lt;year&gt;2005&lt;/year&gt;&#xa;    &lt;price&gt;29.99&lt;/price&gt;&#xa;  &lt;/book&gt;&#xa;  &lt;book category=&quot;WEB&quot;&gt;&#xa;    &lt;title lang=&quot;en&quot;&gt;Learning XML&lt;/title&gt;&#xa;    &lt;author&gt;Erik T. Ray&lt;/author&gt;&#xa;    &lt;year&gt;2003&lt;/year&gt;&#xa;    &lt;price&gt;39.95&lt;/price&gt;&#xa;  &lt;/book&gt;&#xa;&lt;/bookstore&gt;" STYLE_REF="example_node" ID="ID_374898061" CREATED="1430118396565" MODIFIED="1430139243011">
<node TEXT="" STYLE_REF="pre_last_node" ID="ID_1588808183" CREATED="1430118404717" MODIFIED="1430118455273">
<hook URI="nodetree.gif" SIZE="1.0" NAME="ExternalObject"/>
</node>
</node>
</node>
</node>
<node TEXT="Elements" STYLE_REF="second_node" POSITION="left" ID="ID_1765800407" CREATED="1430119207758" MODIFIED="1430148888789">
<edge COLOR="#cccc00"/>
<hook URI="../../Images/periodic-table.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="XML Element" STYLE_REF="orange_node" ID="ID_8990906" CREATED="1430121011811" MODIFIED="1430129581214">
<node TEXT="An XML element is everything from (including) the element&apos;s start tag to (including) the element&apos;s end tag" STYLE_REF="pre_last_node" ID="ID_867467039" CREATED="1430121020566" MODIFIED="1430124670871"/>
<node TEXT="An element can contain" STYLE_REF="yellow_node" ID="ID_1153065451" CREATED="1430121027125" MODIFIED="1430124694576">
<node TEXT="Other elements" STYLE_REF="pre_last_node" ID="ID_6145167" CREATED="1430121035845" MODIFIED="1430124678679"/>
<node TEXT="Text" STYLE_REF="pre_last_node" ID="ID_222587356" CREATED="1430121035845" MODIFIED="1430124681407"/>
<node TEXT="Attributes" STYLE_REF="pre_last_node" ID="ID_1454391749" CREATED="1430121035845" MODIFIED="1430124684342"/>
<node TEXT="Mix of all of the above" STYLE_REF="pre_last_node" ID="ID_1830544840" CREATED="1430121035845" MODIFIED="1430124691054"/>
</node>
</node>
<node TEXT="Empty Elements" STYLE_REF="orange_node" ID="ID_123311979" CREATED="1430121138902" MODIFIED="1430129581214">
<node TEXT="An element with no content is said to be empty." STYLE_REF="pre_last_node" ID="ID_1542113084" CREATED="1430121154071" MODIFIED="1430124670872"/>
<node TEXT="Empty elements do not have any content, but they can have attributes" STYLE_REF="pre_last_node" ID="ID_1703287686" CREATED="1430121226093" MODIFIED="1430124670873"/>
<node TEXT="Forms of empty element" STYLE_REF="pre_last_node" ID="ID_1396768635" CREATED="1430121180499" MODIFIED="1430124670873">
<node TEXT="&lt;element&gt;&lt;/element&gt;" STYLE_REF="example_node" ID="ID_1181498252" CREATED="1430121165030" MODIFIED="1430124650497">
<hook NAME="FirstGroupNode"/>
</node>
<node TEXT="&lt;element /&gt;" STYLE_REF="example_node" ID="ID_675910433" CREATED="1430121191559" MODIFIED="1430124650497"/>
<node TEXT="The two forms above produce identical results in an XML parser" STYLE_REF="pre_last_node" ID="ID_1785491275" CREATED="1430121204944" MODIFIED="1430124670873">
<hook NAME="SummaryNode"/>
</node>
</node>
</node>
<node TEXT="Naming Rules" STYLE_REF="orange_node" ID="ID_1275168115" CREATED="1430121233959" MODIFIED="1430148737494">
<node TEXT="Element names are case-sensitive" STYLE_REF="pre_last_node" ID="ID_1471110145" CREATED="1430121565533" MODIFIED="1430124670873"/>
<node TEXT="Element names must start with a letter or underscore" STYLE_REF="pre_last_node" ID="ID_145958232" CREATED="1430121565533" MODIFIED="1430124670873"/>
<node TEXT="Element names cannot start with the letters xml (or XML, or Xml, etc)" STYLE_REF="pre_last_node" ID="ID_1306568267" CREATED="1430121565535" MODIFIED="1430148737494"/>
<node TEXT="Element names can contain letters, digits, hyphens, underscores, and periods" STYLE_REF="pre_last_node" ID="ID_1956027702" CREATED="1430121565535" MODIFIED="1430124670874"/>
<node TEXT="Element names cannot contain spaces" STYLE_REF="pre_last_node" ID="ID_1662257699" CREATED="1430121565536" MODIFIED="1430124670874"/>
<node TEXT="Any name can be used, no words are reserved (except xml)" STYLE_REF="pre_last_node" ID="ID_1773334144" CREATED="1430121588580" MODIFIED="1430124670874"/>
</node>
<node TEXT="Best Naming Practices" STYLE_REF="orange_node" ID="ID_68303270" CREATED="1430123294643" MODIFIED="1430129581215">
<node TEXT="Create descriptive names, like this: &lt;person&gt;, &lt;firstname&gt;, &lt;lastname&gt;" STYLE_REF="pre_last_node" ID="ID_1072185262" CREATED="1430123301301" MODIFIED="1430124670874"/>
<node TEXT="Create short and simple names, like this: &lt;book_title&gt; not like this: &lt;the_title_of_the_book&gt;" STYLE_REF="pre_last_node" ID="ID_1935240984" CREATED="1430123306739" MODIFIED="1430124670876"/>
<node TEXT="Non-English letters like &#xe9;&#xf2;&#xe1; are perfectly legal in XML, but watch out for problems if your software doesn&apos;t support them" STYLE_REF="pre_last_node" ID="ID_84816141" CREATED="1430123312841" MODIFIED="1430124670881"/>
<node TEXT="Avoid" STYLE_REF="yellow_node" ID="ID_244299569" CREATED="1430124701642" MODIFIED="1430124764712">
<node TEXT="&quot;-&quot;" STYLE_REF="pre_last_node" ID="ID_1903847563" CREATED="1430124707051" MODIFIED="1430124795306" BACKGROUND_COLOR="#00cc66">
<node STYLE_REF="pre_last_node" ID="ID_12352153" CREATED="1430124741461" MODIFIED="1430124779559"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      If you name something &quot;first-name&quot;, some software may think you want to subtract &quot;name&quot; from &quot;first&quot;
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
<node TEXT="&quot;.&quot;" STYLE_REF="pre_last_node" ID="ID_1074469485" CREATED="1430124725757" MODIFIED="1430124795307" BACKGROUND_COLOR="#00cc66">
<node STYLE_REF="pre_last_node" ID="ID_1674635687" CREATED="1430124747445" MODIFIED="1430124779560"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      If you name something &quot;first.name&quot;, some software may think that &quot;name&quot; is a property of the object &quot;first&quot;
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
<node STYLE_REF="pre_last_node" ID="ID_1175774384" CREATED="1430124732897" MODIFIED="1430124795307" BACKGROUND_COLOR="#00cc66"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &quot;:&quot;
    </p>
  </body>
</html>

</richcontent>
<node TEXT="Colons are reserved for namespaces" STYLE_REF="pre_last_node" ID="ID_368560672" CREATED="1430124752508" MODIFIED="1430124779560"/>
</node>
</node>
</node>
<node TEXT="Naming Styles" STYLE_REF="orange_node" ID="ID_1399583032" CREATED="1430123351909" MODIFIED="1430129581215">
<node TEXT="XML documents often have a corresponding database. A good practice is to use the naming rules of your database for the elements in the XML documents." STYLE_REF="pre_last_node" ID="ID_991178681" CREATED="1430124180445" MODIFIED="1430124670882"/>
<node TEXT="Lower case" STYLE_REF="yellow_node" ID="ID_1800191706" CREATED="1430124111888" MODIFIED="1430124187896">
<node TEXT="All letters lower case" STYLE_REF="pre_last_node" ID="ID_1345792734" CREATED="1430124120418" MODIFIED="1430124670883"/>
<node TEXT="&lt;firstname&gt;" STYLE_REF="example_node" ID="ID_1291185222" CREATED="1430124125218" MODIFIED="1430124650497"/>
</node>
<node TEXT="Upper case" STYLE_REF="yellow_node" ID="ID_1449273165" CREATED="1430124111888" MODIFIED="1430124187897">
<node TEXT="All letters upper case" STYLE_REF="pre_last_node" ID="ID_1828252872" CREATED="1430124130723" MODIFIED="1430124670883"/>
<node TEXT="&lt;FIRSTNAME&gt;" STYLE_REF="example_node" ID="ID_1106028729" CREATED="1430124136825" MODIFIED="1430124650497"/>
</node>
<node TEXT="Underscore" STYLE_REF="yellow_node" ID="ID_911559709" CREATED="1430124111890" MODIFIED="1430124187897">
<node TEXT="Underscore separates words" STYLE_REF="pre_last_node" ID="ID_113845866" CREATED="1430124142961" MODIFIED="1430124670883"/>
<node TEXT="&lt;first_name&gt;" STYLE_REF="example_node" ID="ID_1725826379" CREATED="1430124147570" MODIFIED="1430124650497"/>
</node>
<node TEXT="Pascal case" STYLE_REF="yellow_node" ID="ID_1631937307" CREATED="1430124111890" MODIFIED="1430124187897">
<node TEXT="Uppercase first letter in each word" STYLE_REF="pre_last_node" ID="ID_843994940" CREATED="1430124153187" MODIFIED="1430124670883"/>
<node TEXT="&lt;FirstName&gt;" STYLE_REF="example_node" ID="ID_1732981327" CREATED="1430124158755" MODIFIED="1430124650497"/>
</node>
<node TEXT="Camel case" STYLE_REF="yellow_node" ID="ID_591948871" CREATED="1430124111890" MODIFIED="1430124187897">
<node TEXT="Uppercase first letter in each word except the first" STYLE_REF="pre_last_node" ID="ID_166178639" CREATED="1430124164459" MODIFIED="1430124670883"/>
<node STYLE_REF="example_node" ID="ID_157444244" CREATED="1430124170316" MODIFIED="1430124650496"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &lt;firstName&gt;
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
</node>
<node TEXT="Elements are Extensible" STYLE_REF="orange_node" ID="ID_1009912749" CREATED="1430124620514" MODIFIED="1430129581215">
<node TEXT="Your app constructs some view based on below data" STYLE_REF="pre_last_node" ID="ID_1607869455" CREATED="1430124952498" MODIFIED="1430125159134"/>
<node TEXT="&lt;note&gt;&#xa;  &lt;to&gt;Tove&lt;/to&gt;&#xa;  &lt;from&gt;Jani&lt;/from&gt;&#xa;  &lt;body&gt;Don&apos;t forget me this weekend!&lt;/body&gt;&#xa;&lt;/note&gt;" STYLE_REF="example_node" ID="ID_1475527691" CREATED="1430124904475" MODIFIED="1430125154455"/>
<node TEXT="Imagine that the author of the XML document added some extra information to it:" STYLE_REF="pre_last_node" ID="ID_1058443016" CREATED="1430124933816" MODIFIED="1430125159131"/>
<node TEXT="&lt;note&gt;&#xa;  &lt;date&gt;2008-01-10&lt;/date&gt;&#xa;  &lt;to&gt;Tove&lt;/to&gt;&#xa;  &lt;from&gt;Jani&lt;/from&gt;&#xa;  &lt;heading&gt;Reminder&lt;/heading&gt;&#xa;  &lt;body&gt;Don&apos;t forget me this weekend!&lt;/body&gt;&#xa;&lt;/note&gt;" STYLE_REF="example_node" ID="ID_1890458354" CREATED="1430124938914" MODIFIED="1430125154447"/>
<node TEXT="The application should still be able to find the &lt;to&gt;, &lt;from&gt;, and &lt;body&gt; elements in the XML document and produce the same output" STYLE_REF="pre_last_node" ID="ID_1682647211" CREATED="1430125036667" MODIFIED="1430125159129"/>
</node>
</node>
<node TEXT="Namespaces" STYLE_REF="second_node" POSITION="right" ID="ID_226543534" CREATED="1430119473980" MODIFIED="1430135469436" VGAP="13">
<edge COLOR="#00007c"/>
<hook URI="../../Images/namespace.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Conflicts" STYLE_REF="orange_node" ID="ID_1756432148" CREATED="1430129895682" MODIFIED="1430135469436">
<node TEXT="In XML, element names are defined by the developer. This often results in a conflict when trying to mix XML documents from different XML applications." STYLE_REF="pre_last_node" ID="ID_522912712" CREATED="1430130045320" MODIFIED="1430131842375"/>
<node TEXT="&lt;table&gt;&#xa;  &lt;tr&gt;&#xa;    &lt;td&gt;Apples&lt;/td&gt;&#xa;    &lt;td&gt;Bananas&lt;/td&gt;&#xa;  &lt;/tr&gt;&#xa;&lt;/table&gt;" STYLE_REF="example_node" ID="ID_21687472" CREATED="1430130052088" MODIFIED="1430131835891">
<hook NAME="FirstGroupNode"/>
</node>
<node TEXT="&lt;table&gt;&#xa;  &lt;name&gt;African Coffee Table&lt;/name&gt;&#xa;  &lt;width&gt;80&lt;/width&gt;&#xa;  &lt;length&gt;120&lt;/length&gt;&#xa;&lt;/table&gt;" STYLE_REF="example_node" ID="ID_1447743320" CREATED="1430130066473" MODIFIED="1430131835875"/>
<node TEXT="If these XML fragments were added together, there would be a name conflict. Both contain a &lt;table&gt; element, but the elements have different content and meaning" STYLE_REF="pre_last_node" ID="ID_807550433" CREATED="1430130081526" MODIFIED="1430131842375">
<hook NAME="SummaryNode"/>
</node>
</node>
<node TEXT="Solving the Name Conflict Using a Prefix" STYLE_REF="orange_node" ID="ID_1192428097" CREATED="1430130093372" MODIFIED="1430135520389">
<node TEXT="Name conflicts in XML can easily be avoided using a name prefix" STYLE_REF="pre_last_node" ID="ID_924696021" CREATED="1430130117377" MODIFIED="1430131842360"/>
<node TEXT="&lt;h:table&gt;&#xa;  &lt;h:tr&gt;&#xa;    &lt;h:td&gt;Apples&lt;/h:td&gt;&#xa;    &lt;h:td&gt;Bananas&lt;/h:td&gt;&#xa;  &lt;/h:tr&gt;&#xa;&lt;/h:table&gt;" STYLE_REF="example_node" ID="ID_751532367" CREATED="1430130126137" MODIFIED="1430135520389">
<hook NAME="FirstGroupNode"/>
</node>
<node TEXT="&lt;f:table&gt;&#xa;  &lt;f:name&gt;African Coffee Table&lt;/f:name&gt;&#xa;  &lt;f:width&gt;80&lt;/f:width&gt;&#xa;  &lt;f:length&gt;120&lt;/f:length&gt;&#xa;&lt;/f:table&gt;" STYLE_REF="example_node" ID="ID_701635243" CREATED="1430130161458" MODIFIED="1430131835875"/>
<node TEXT="There will be no conflict because the two &lt;table&gt; elements have different names" STYLE_REF="pre_last_node" ID="ID_296462233" CREATED="1430130167474" MODIFIED="1430131842360">
<hook NAME="SummaryNode"/>
</node>
</node>
<node TEXT="The xmlns Attribute" STYLE_REF="orange_node" ID="ID_120358727" CREATED="1430130188665" MODIFIED="1430135526028">
<node TEXT="When using prefixes in XML, a so-called namespace for the prefix must be defined" STYLE_REF="pre_last_node" ID="ID_1705715737" CREATED="1430130559776" MODIFIED="1430135232969"/>
<node TEXT="Syntax" STYLE_REF="yellow_node" ID="ID_1723196103" CREATED="1430135192295" MODIFIED="1430135271053">
<node TEXT="xmlns:prefix=&quot;URI&quot;" STYLE_REF="example_node" ID="ID_590365941" CREATED="1430131024848" MODIFIED="1430135144089"/>
</node>
<node TEXT="Namespace declaration" STYLE_REF="yellow_node" ID="ID_1146116063" CREATED="1430135332871" MODIFIED="1430135526028" VGAP="20">
<node TEXT="Start tag of an element" STYLE_REF="gray_node" ID="ID_1716206484" CREATED="1430135364831" MODIFIED="1430135431918">
<font BOLD="false"/>
<node TEXT="The namespace is defined by the xmlns attribute in the start tag of an element" STYLE_REF="pre_last_node" ID="ID_460863856" CREATED="1430130570704" MODIFIED="1430135232984"/>
<node TEXT="&lt;root&gt;&#xa;&#xa;&lt;h:table xmlns:h=&quot;http://www.w3.org/TR/html4/&quot;&gt;&#xa;  &lt;h:tr&gt;&#xa;    &lt;h:td&gt;Apples&lt;/h:td&gt;&#xa;    &lt;h:td&gt;Bananas&lt;/h:td&gt;&#xa;  &lt;/h:tr&gt;&#xa;&lt;/h:table&gt;&#xa;&#xa;&lt;f:table xmlns:f=&quot;http://www.w3schools.com/furniture&quot;&gt;&#xa;  &lt;f:name&gt;African Coffee Table&lt;/f:name&gt;&#xa;  &lt;f:width&gt;80&lt;/f:width&gt;&#xa;  &lt;f:length&gt;120&lt;/f:length&gt;&#xa;&lt;/f:table&gt;&#xa;&#xa;&lt;/root&gt;" STYLE_REF="example_node" ID="ID_1548048222" CREATED="1430131039784" MODIFIED="1430135144089"/>
<node TEXT="The xmlns attribute in the &lt;table&gt; tag give the h: and f: prefixes a qualified namespace" STYLE_REF="pre_last_node" ID="ID_1912237295" CREATED="1430131692385" MODIFIED="1430135232984"/>
<node TEXT="When a namespace is defined for an element, all child elements with the same prefix are associated with the same namespace" STYLE_REF="pre_last_node" ID="ID_1647181823" CREATED="1430131717001" MODIFIED="1430135232984"/>
</node>
<node TEXT="Root element" STYLE_REF="gray_node" ID="ID_300059484" CREATED="1430135375423" MODIFIED="1430135409910">
<font BOLD="false"/>
<node TEXT="Namespaces can be declared in the elements where they are used or in the XML root element:" STYLE_REF="pre_last_node" ID="ID_1088291351" CREATED="1430131731561" MODIFIED="1430135232984"/>
<node TEXT="&lt;root xmlns:h=&quot;http://www.w3.org/TR/html4/&quot;&#xa;xmlns:f=&quot;http://www.w3schools.com/furniture&quot;&gt;&#xa;&#xa;&lt;h:table&gt;&#xa;  &lt;h:tr&gt;&#xa;    &lt;h:td&gt;Apples&lt;/h:td&gt;&#xa;    &lt;h:td&gt;Bananas&lt;/h:td&gt;&#xa;  &lt;/h:tr&gt;&#xa;&lt;/h:table&gt;&#xa;&#xa;&lt;f:table&gt;&#xa;  &lt;f:name&gt;African Coffee Table&lt;/f:name&gt;&#xa;  &lt;f:width&gt;80&lt;/f:width&gt;&#xa;  &lt;f:length&gt;120&lt;/f:length&gt;&#xa;&lt;/f:table&gt;&#xa;&#xa;&lt;/root&gt;" STYLE_REF="example_node" ID="ID_1344668136" CREATED="1430131757288" MODIFIED="1430135144089"/>
</node>
</node>
<node TEXT="The namespace URI is not used by the parser to look up information.&#xa;The purpose is to give the namespace a unique name. However, often companies use the namespace as a pointer to a web page containing namespace information" STYLE_REF="pre_last_node" ID="ID_91103991" CREATED="1430131786714" MODIFIED="1430135480823"/>
</node>
<node TEXT="Uniform Resource Identifier" STYLE_REF="orange_node" ID="ID_447452791" CREATED="1430131817210" MODIFIED="1430132911257">
<node TEXT="URI is a string of characters which identifies an Internet Resource" STYLE_REF="pre_last_node" ID="ID_1808665993" CREATED="1430131892536" MODIFIED="1430132816390"/>
<node TEXT="URI types" STYLE_REF="yellow_node" ID="ID_998863959" CREATED="1430132773936" MODIFIED="1430132811349">
<node TEXT="Uniform Resource Locator (URL)" STYLE_REF="pre_last_node" ID="ID_696740159" CREATED="1430132785265" MODIFIED="1430132838399"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Which identifies an Internet domain address
    </p>
  </body>
</html>

</richcontent>
<font BOLD="true"/>
</node>
<node TEXT="Universal Resource Name (URN)" STYLE_REF="pre_last_node" ID="ID_547096504" CREATED="1430132804552" MODIFIED="1430132828614">
<font BOLD="true"/>
</node>
</node>
</node>
<node TEXT="Default Namespaces" STYLE_REF="orange_node" ID="ID_1936052571" CREATED="1430132882616" MODIFIED="1430132911257">
<node TEXT="Defining a default namespace for an element saves us from using prefixes in all the child elements" STYLE_REF="pre_last_node" ID="ID_123017417" CREATED="1430134240846" MODIFIED="1430135156457"/>
<node TEXT="Syntax" STYLE_REF="yellow_node" ID="ID_566179773" CREATED="1430135171280" MODIFIED="1430135177553">
<node TEXT="xmlns=&quot;namespaceURI&quot;" STYLE_REF="example_node" ID="ID_618596805" CREATED="1430134312998" MODIFIED="1430135170384"/>
</node>
<node TEXT="Examples" STYLE_REF="yellow_node" ID="ID_156975272" CREATED="1430134938438" MODIFIED="1430135165368">
<node TEXT="&lt;table xmlns=&quot;http://www.w3.org/TR/html4/&quot;&gt;&#xa;  &lt;tr&gt;&#xa;    &lt;td&gt;Apples&lt;/td&gt;&#xa;    &lt;td&gt;Bananas&lt;/td&gt;&#xa;  &lt;/tr&gt;&#xa;&lt;/table&gt;" STYLE_REF="example_node" ID="ID_1250541128" CREATED="1430134307742" MODIFIED="1430135165368"/>
<node TEXT="&lt;table xmlns=&quot;http://www.w3schools.com/furniture&quot;&gt;&#xa;  &lt;name&gt;African Coffee Table&lt;/name&gt;&#xa;  &lt;width&gt;80&lt;/width&gt;&#xa;  &lt;length&gt;120&lt;/length&gt;&#xa;&lt;/table&gt;" STYLE_REF="example_node" ID="ID_83210366" CREATED="1430134341976" MODIFIED="1430135144089"/>
</node>
</node>
<node TEXT="Namespaces in Real Use" STYLE_REF="orange_node" ID="ID_275313191" CREATED="1430132901744" MODIFIED="1430135215820">
<node TEXT="XSLT is an XML language that can be used to transform XML documents into other formats, like HTML" STYLE_REF="pre_last_node" ID="ID_1079417364" CREATED="1430134969177" MODIFIED="1430135156457"/>
<node TEXT="The tags that are not HTML tags have the prefix xsl, identified by the namespace xmlns:xsl=&quot;http://www.w3.org/1999/XSL/Transform&quot;" STYLE_REF="pre_last_node" ID="ID_793325658" CREATED="1430135115608" MODIFIED="1430135156473" LINK="http://www.w3.org/1999/XSL/Transform"/>
<node TEXT="&lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot;?&gt;&#xa;&#xa;&lt;xsl:stylesheet version=&quot;1.0&quot;&#xa;xmlns:xsl=&quot;http://www.w3.org/1999/XSL/Transform&quot;&gt;&#xa;&#xa;&lt;xsl:template match=&quot;/&quot;&gt;&#xa;&lt;html&gt;&#xa;&lt;body&gt;&#xa;  &lt;h2&gt;My CD Collection&lt;/h2&gt;&#xa;  &lt;table border=&quot;1&quot;&gt;&#xa;    &lt;tr&gt;&#xa;      &lt;th style=&quot;text-align:left&quot;&gt;Title&lt;/th&gt;&#xa;      &lt;th style=&quot;text-align:left&quot;&gt;Artist&lt;/th&gt;&#xa;    &lt;/tr&gt;&#xa;    &lt;xsl:for-each select=&quot;catalog/cd&quot;&gt;&#xa;    &lt;tr&gt;&#xa;      &lt;td&gt;&lt;xsl:value-of select=&quot;title&quot;/&gt;&lt;/td&gt;&#xa;      &lt;td&gt;&lt;xsl:value-of select=&quot;artist&quot;/&gt;&lt;/td&gt;&#xa;    &lt;/tr&gt;&#xa;    &lt;/xsl:for-each&gt;&#xa;  &lt;/table&gt;&#xa;&lt;/body&gt;&#xa;&lt;/html&gt;&#xa;&lt;/xsl:template&gt;&#xa;&#xa;&lt;/xsl:stylesheet&gt;" STYLE_REF="example_node" ID="ID_455146015" CREATED="1430135122393" MODIFIED="1430135215820"/>
</node>
</node>
<node TEXT="Encoding" STYLE_REF="second_node" POSITION="right" ID="ID_1090803793" CREATED="1430119483589" MODIFIED="1430148257367">
<edge COLOR="#007c00"/>
<hook URI="../../Images/encoding.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Character Encoding" STYLE_REF="orange_node" ID="ID_1669017580" CREATED="1430138336454" MODIFIED="1430138836427">
<node TEXT="Character encoding defines a unique binary code for each different character used in a document" STYLE_REF="pre_last_node" ID="ID_1322112252" CREATED="1430138344136" MODIFIED="1430138853752"/>
<node TEXT="To avoid errors, you should specify the encoding used, or save your XML files as UTF-8" STYLE_REF="pre_last_node" ID="ID_205561334" CREATED="1430138349665" MODIFIED="1430138853752"/>
<node TEXT="The &#xfb01;rst line in an XML document is called the prolog" STYLE_REF="pre_last_node" ID="ID_544969756" CREATED="1430138757166" MODIFIED="1430138853752">
<node TEXT="The prolog is optional" ID="ID_1689379724" CREATED="1430138770423" MODIFIED="1430138770423"/>
<node TEXT="&lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot;?&gt;" ID="ID_37689453" CREATED="1430138763271" MODIFIED="1430138763271"/>
</node>
</node>
<node TEXT="The Unicode Character Sets" STYLE_REF="orange_node" ID="ID_215644139" CREATED="1430138591949" MODIFIED="1430138836411">
<node TEXT="UTF-8" STYLE_REF="yellow_node" ID="ID_1845146698" CREATED="1430138604678" MODIFIED="1430138841292">
<node TEXT="Uses 1 byte" STYLE_REF="pre_last_node" ID="ID_682286485" CREATED="1430138618135" MODIFIED="1430138848252">
<node TEXT="To represent basic Latin characters, and two, three, or four bytes for the rest" ID="ID_924146993" CREATED="1430138640903" MODIFIED="1430138663724"/>
</node>
<node TEXT="Is the default character encoding for" STYLE_REF="pre_last_node" ID="ID_916107382" CREATED="1430138688574" MODIFIED="1430138867869">
<node TEXT="HTML5" ID="ID_1904377559" CREATED="1430138693505" MODIFIED="1430138693505"/>
<node TEXT="CSS" ID="ID_974900988" CREATED="1430138697286" MODIFIED="1430138697286"/>
<node TEXT="JavaScript" ID="ID_1550504354" CREATED="1430138701126" MODIFIED="1430138701126"/>
<node TEXT="PHP" ID="ID_1972736745" CREATED="1430138704758" MODIFIED="1430138704758"/>
<node TEXT="SQL" ID="ID_1686414564" CREATED="1430138708768" MODIFIED="1430138708768"/>
<node TEXT="XML" ID="ID_128166153" CREATED="1430138712606" MODIFIED="1430138712606"/>
</node>
</node>
<node TEXT="UTF-16" STYLE_REF="yellow_node" ID="ID_286436684" CREATED="1430138610423" MODIFIED="1430138841292">
<node TEXT="Uses 2 bytes" STYLE_REF="pre_last_node" ID="ID_297277054" CREATED="1430138647151" MODIFIED="1430138848236">
<node TEXT="For most characters, and four bytes for the rest" ID="ID_1748537976" CREATED="1430138654865" MODIFIED="1430138665958"/>
</node>
</node>
</node>
</node>
<node TEXT="CSS" STYLE_REF="second_node" POSITION="right" ID="ID_911461319" CREATED="1430119501526" MODIFIED="1430148357561">
<edge COLOR="#007c7c"/>
<hook URI="../../Images/css.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Formatting XML with CSS is not the most common method." STYLE_REF="pre_last_node" ID="ID_440816075" CREATED="1430139049854" MODIFIED="1430139078390"/>
<node TEXT="W3C recommends using XSLT instead" STYLE_REF="pre_last_node" ID="ID_162692327" CREATED="1430139064711" MODIFIED="1430139078390"/>
<node TEXT="Example" STYLE_REF="yellow_node" ID="ID_1399498417" CREATED="1430139044478" MODIFIED="1430139072905">
<node TEXT="&lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot;?&gt;&#xa;&lt;?xml-stylesheet type=&quot;text/css&quot; href=&quot;cd_catalog.css&quot;?&gt;&#xa;&lt;CATALOG&gt;&#xa;  &lt;CD&gt;&#xa;    &lt;TITLE&gt;Empire Burlesque&lt;/TITLE&gt;&#xa;    &lt;ARTIST&gt;Bob Dylan&lt;/ARTIST&gt;&#xa;    &lt;COUNTRY&gt;USA&lt;/COUNTRY&gt;&#xa;    &lt;COMPANY&gt;Columbia&lt;/COMPANY&gt;&#xa;    &lt;PRICE&gt;10.90&lt;/PRICE&gt;&#xa;    &lt;YEAR&gt;1985&lt;/YEAR&gt;&#xa;  &lt;/CD&gt;&#xa;  &lt;CD&gt;&#xa;    &lt;TITLE&gt;Hide your heart&lt;/TITLE&gt;&#xa;    &lt;ARTIST&gt;Bonnie Tyler&lt;/ARTIST&gt;&#xa;    &lt;COUNTRY&gt;UK&lt;/COUNTRY&gt;&#xa;    &lt;COMPANY&gt;CBS Records&lt;/COMPANY&gt;&#xa;    &lt;PRICE&gt;9.90&lt;/PRICE&gt;&#xa;    &lt;YEAR&gt;1988&lt;/YEAR&gt;&#xa;  &lt;/CD&gt;&#xa;.&#xa;.&#xa;.&#xa;&lt;/CATALOG&gt;" STYLE_REF="example_node" ID="ID_1776639920" CREATED="1430138965224" MODIFIED="1430138980932"/>
<node TEXT="CATALOG&#xa;{&#xa;background-color: #ffffff;&#xa;width: 100%;&#xa;}&#xa;CD&#xa;{&#xa;display: block;&#xa;margin-bottom: 30pt;&#xa;margin-left: 0;&#xa;}&#xa;TITLE&#xa;{&#xa;color: #FF0000;&#xa;font-size: 20pt;&#xa;}&#xa;ARTIST&#xa;{&#xa;color: #0000FF;&#xa;font-size: 20pt;&#xa;}&#xa;COUNTRY,PRICE,YEAR,COMPANY&#xa;{&#xa;display: block;&#xa;color: #000000;&#xa;margin-left: 20pt;&#xa;}" STYLE_REF="example_node" ID="ID_760992259" CREATED="1430138974567" MODIFIED="1430138980930"/>
</node>
</node>
<node TEXT="Document Types" STYLE_REF="second_node" POSITION="right" ID="ID_517456640" CREATED="1430142580099" MODIFIED="1430148345906">
<edge COLOR="#ff0000"/>
<hook URI="../../Images/type-2.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Well Formed XML Documents" STYLE_REF="orange_node" ID="ID_87674446" CREATED="1430142715476" MODIFIED="1430142934036">
<node TEXT="An XML document with correct syntax is &quot;Well Formed&quot;" STYLE_REF="pre_last_node" ID="ID_203851200" CREATED="1430142723484" MODIFIED="1430143753301"/>
</node>
<node TEXT="Valid XML Documents" STYLE_REF="orange_node" ID="ID_701887376" CREATED="1430142780013" MODIFIED="1430142934036">
<node TEXT="A &quot;valid&quot; XML document must be well formed" STYLE_REF="pre_last_node" ID="ID_790684907" CREATED="1430142790334" MODIFIED="1430143753300"/>
<node TEXT="In addition it must conform to a document type definition" STYLE_REF="pre_last_node" ID="ID_1160412459" CREATED="1430142800446" MODIFIED="1430143753299"/>
<node TEXT="Rules that defines the legal elements and attributes for XML documents are called Document Type Definitions or XML Schemas" STYLE_REF="pre_last_node" ID="ID_1536932336" CREATED="1430142817977" MODIFIED="1430143753298"/>
<node TEXT="Document type definitions" STYLE_REF="yellow_node" ID="ID_659676747" CREATED="1430142845989" MODIFIED="1430143736704">
<node TEXT="DTD" STYLE_REF="gray_node" ID="ID_1930896193" CREATED="1430142850615" MODIFIED="1430143746394">
<node TEXT="The original Document Type Definition" STYLE_REF="pre_last_node" ID="ID_1343799230" CREATED="1430142864189" MODIFIED="1430143904754"/>
</node>
<node TEXT="XML Schema" STYLE_REF="gray_node" ID="ID_371703320" CREATED="1430142855597" MODIFIED="1430143746393">
<node TEXT="An XML-based alternative to DTD" STYLE_REF="pre_last_node" ID="ID_463454969" CREATED="1430142870054" MODIFIED="1430143753297"/>
</node>
</node>
<node TEXT="http://www.w3schools.com/xml/xml_validator.asp" STYLE_REF="pre_last_node" ID="ID_1452226662" CREATED="1430143673197" MODIFIED="1430143753298"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      XML Validator
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
<node TEXT="When to Use a DTD/Schema?" STYLE_REF="orange_node" ID="ID_558582718" CREATED="1430143810651" MODIFIED="1430143896656">
<node TEXT="With a DTD, independent groups of people can agree to use a standard DTD for interchanging data" STYLE_REF="pre_last_node" ID="ID_771902848" CREATED="1430143821011" MODIFIED="1430143888106"/>
<node TEXT="Your application can use a standard DTD to verify that the data you receive from the outside world is valid" STYLE_REF="pre_last_node" ID="ID_1603871549" CREATED="1430143838327" MODIFIED="1430143888105"/>
</node>
<node TEXT="When to NOT to Use a DTD/Schema?" STYLE_REF="orange_node" ID="ID_460295614" CREATED="1430143846525" MODIFIED="1430143896658">
<node TEXT="XML does not require a DTD/Schema" STYLE_REF="pre_last_node" ID="ID_627894657" CREATED="1430143867903" MODIFIED="1430143890955"/>
<node TEXT="When you are experimenting with XML, or when you are working with small XML files, creating DTDs may be a waste of time" STYLE_REF="pre_last_node" ID="ID_1527363407" CREATED="1430142928100" MODIFIED="1430143892657"/>
</node>
<node TEXT="XML DTD" STYLE_REF="orange_node" ID="ID_564129664" CREATED="1430144090091" MODIFIED="1430148726798">
<node TEXT="The purpose of a DTD is to define the structure of an XML document. It defines the structure with a list of legal element" STYLE_REF="pre_last_node" ID="ID_1470698332" CREATED="1430144825987" MODIFIED="1430146279506"/>
<node TEXT="Example" STYLE_REF="yellow_node" ID="ID_931867140" CREATED="1430147536762" MODIFIED="1430148726798">
<node TEXT="&lt;!DOCTYPE note&#xa;[&#xa;&lt;!ELEMENT note (to,from,heading,body)&gt;&#xa;&lt;!ELEMENT to (#PCDATA)&gt;&#xa;&lt;!ELEMENT from (#PCDATA)&gt;&#xa;&lt;!ELEMENT heading (#PCDATA)&gt;&#xa;&lt;!ELEMENT body (#PCDATA)&gt;&#xa;]&gt;" STYLE_REF="example_node" ID="ID_428822505" CREATED="1430144976387" MODIFIED="1430145279251"/>
<node TEXT="&lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot;?&gt;&#xa;&lt;!DOCTYPE note SYSTEM &quot;Note.dtd&quot;&gt;&#xa;&lt;note&gt;&#xa;&lt;to&gt;Tove&lt;/to&gt;&#xa;&lt;from&gt;Jani&lt;/from&gt;&#xa;&lt;heading&gt;Reminder&lt;/heading&gt;&#xa;&lt;body&gt;Don&apos;t forget me this weekend!&lt;/body&gt;&#xa;&lt;/note&gt;" STYLE_REF="example_node" ID="ID_116357669" CREATED="1430147827211" MODIFIED="1430147863400"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      to include an external DTD file
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="Meaning" STYLE_REF="gray_node" ID="ID_1834754835" CREATED="1430147546786" MODIFIED="1430147566551">
<font BOLD="false"/>
<node TEXT="!DOCTYPE note defines that the root element of the document is note" STYLE_REF="pre_last_node" ID="ID_717245687" CREATED="1430145036791" MODIFIED="1430146279510"/>
<node TEXT="!ELEMENT note defines that the note element must contain four elements: &quot;to, from, heading, body&quot;" STYLE_REF="pre_last_node" ID="ID_1829814951" CREATED="1430145036791" MODIFIED="1430146279511"/>
<node TEXT="!ELEMENT to defines the to element to be of type &quot;#PCDATA&quot;" STYLE_REF="pre_last_node" ID="ID_214417071" CREATED="1430145036792" MODIFIED="1430146279512"/>
<node TEXT="!ELEMENT from defines the from element to be of type &quot;#PCDATA&quot;" STYLE_REF="pre_last_node" ID="ID_495739873" CREATED="1430145036793" MODIFIED="1430147566550"/>
<node TEXT="!ELEMENT heading defines the heading element to be of type &quot;#PCDATA&quot;" STYLE_REF="pre_last_node" ID="ID_1062812915" CREATED="1430145036793" MODIFIED="1430146279513"/>
<node TEXT="!ELEMENT body defines the body element to be of type &quot;#PCDATA&quot;" STYLE_REF="pre_last_node" ID="ID_1650413842" CREATED="1430145036793" MODIFIED="1430146279513"/>
<node TEXT="#PCDATA means parse-able text data" STYLE_REF="pre_last_node" ID="ID_488968915" CREATED="1430145044571" MODIFIED="1430147369319"/>
</node>
</node>
<node TEXT="Using DTD for Entity Declaration" STYLE_REF="yellow_node" ID="ID_1089750764" CREATED="1430145252516" MODIFIED="1430146514320">
<node TEXT="A doctype declaration can also be used to define special characters and character strings, used in the document" STYLE_REF="pre_last_node" ID="ID_1001262707" CREATED="1430146507580" MODIFIED="1430146518224"/>
<node TEXT="&lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot;?&gt;&#xa;&#xa;&lt;!DOCTYPE note [&#xa;&lt;!ENTITY nbsp &quot;&amp;#xA0;&quot;&gt;&#xa;&lt;!ENTITY writer &quot;Writer: Donald Duck.&quot;&gt;&#xa;&lt;!ENTITY copyright &quot;Copyright: W3Schools.&quot;&gt;&#xa;]&gt;&#xa;&#xa;&lt;note&gt;&#xa;&lt;to&gt;Tove&lt;/to&gt;&#xa;&lt;from&gt;Jani&lt;/from&gt;&#xa;&lt;heading&gt;Reminder&lt;/heading&gt;&#xa;&lt;body&gt;Don&apos;t forget me this weekend!&lt;/body&gt;&#xa;&lt;footer&gt;&amp;writer;&amp;nbsp;&amp;copyright;&lt;/footer&gt;&#xa;&lt;/note&gt;" STYLE_REF="example_node" ID="ID_1738624404" CREATED="1430145263444" MODIFIED="1430145279249"/>
</node>
</node>
<node TEXT="XML Schema" STYLE_REF="orange_node" ID="ID_1558728302" CREATED="1430144098726" MODIFIED="1430145274425">
<node TEXT="XML Schema is an XML-based alternative to DTD" STYLE_REF="pre_last_node" ID="ID_842285963" CREATED="1430147333883" MODIFIED="1430147458191"/>
<node TEXT="Example" STYLE_REF="yellow_node" ID="ID_184104567" CREATED="1430147404514" MODIFIED="1430147434974">
<node TEXT="&lt;?xml version=&quot;1.0&quot;?&gt;&#xa;&lt;xs:schema xmlns:xs=&quot;http://www.w3.org/2001/XMLSchema&quot;&gt;&#xa;&#xa;&lt;xs:element name=&quot;note&quot;&gt;&#xa;&lt;xs:complexType&gt;&#xa;  &lt;xs:sequence&gt;&#xa;    &lt;xs:element name=&quot;to&quot; type=&quot;xs:string&quot;/&gt;&#xa;    &lt;xs:element name=&quot;from&quot; type=&quot;xs:string&quot;/&gt;&#xa;    &lt;xs:element name=&quot;heading&quot; type=&quot;xs:string&quot;/&gt;&#xa;    &lt;xs:element name=&quot;body&quot; type=&quot;xs:string&quot;/&gt;&#xa;  &lt;/xs:sequence&gt;&#xa;&lt;/xs:complexType&gt;&#xa;&lt;/xs:element&gt;&#xa;&#xa;&lt;/xs:schema&gt;" STYLE_REF="example_node" ID="ID_1971965528" CREATED="1430147383833" MODIFIED="1430147784671"/>
<node TEXT="Meaning" STYLE_REF="gray_node" ID="ID_1671242245" CREATED="1430147417459" MODIFIED="1430147442014">
<font BOLD="false"/>
<node TEXT="&lt;xs:element name=&quot;note&quot;&gt; defines the element called &quot;note&quot;" STYLE_REF="pre_last_node" ID="ID_1053549633" CREATED="1430147398828" MODIFIED="1430147458195"/>
<node TEXT="&lt;xs:complexType&gt; the &quot;note&quot; element is a complex type" STYLE_REF="pre_last_node" ID="ID_1099684307" CREATED="1430147398828" MODIFIED="1430147458196"/>
<node TEXT="&lt;xs:sequence&gt; the complex type is a sequence of elements" STYLE_REF="pre_last_node" ID="ID_1110675091" CREATED="1430147398829" MODIFIED="1430147458196"/>
<node TEXT="&lt;xs:element name=&quot;to&quot; type=&quot;xs:string&quot;&gt; the element &quot;to&quot; is of type string (text)" STYLE_REF="pre_last_node" ID="ID_265720076" CREATED="1430147398829" MODIFIED="1430147458197"/>
<node TEXT="&lt;xs:element name=&quot;from&quot; type=&quot;xs:string&quot;&gt; the element &quot;from&quot; is of type string" STYLE_REF="pre_last_node" ID="ID_686481287" CREATED="1430147398829" MODIFIED="1430147458197"/>
<node TEXT="&lt;xs:element name=&quot;heading&quot; type=&quot;xs:string&quot;&gt; the element &quot;heading&quot; is of type string" STYLE_REF="pre_last_node" ID="ID_1960531011" CREATED="1430147398829" MODIFIED="1430147458198"/>
<node TEXT="&lt;xs:element name=&quot;body&quot; type=&quot;xs:string&quot;&gt; the element &quot;body&quot; is of type string" STYLE_REF="pre_last_node" ID="ID_1833855665" CREATED="1430147398830" MODIFIED="1430147458198"/>
</node>
</node>
<node TEXT="XML Schemas are More Powerful than DTD" STYLE_REF="yellow_node" ID="ID_1428073582" CREATED="1430147584618" MODIFIED="1430147713431">
<node TEXT="XML Schemas are written in XML" STYLE_REF="pre_last_node" ID="ID_843282676" CREATED="1430147605780" MODIFIED="1430147727480"/>
<node TEXT="XML Schemas are extensible to additions" STYLE_REF="pre_last_node" ID="ID_1022104865" CREATED="1430147605780" MODIFIED="1430147727481"/>
<node TEXT="XML Schemas support data types" STYLE_REF="pre_last_node" ID="ID_628997649" CREATED="1430147605780" MODIFIED="1430147727481"/>
<node TEXT="XML Schemas support namespaces" STYLE_REF="pre_last_node" ID="ID_480225379" CREATED="1430147605780" MODIFIED="1430147727481"/>
</node>
<node TEXT="Advantages" STYLE_REF="yellow_node" ID="ID_233799859" CREATED="1430147657188" MODIFIED="1430147713431">
<node TEXT="XML Schemas Support Data Types" STYLE_REF="gray_node" ID="ID_839502926" CREATED="1430147674400" MODIFIED="1430147719182">
<font BOLD="false"/>
<node TEXT="It is easier to describe document content" STYLE_REF="pre_last_node" ID="ID_314861480" CREATED="1430147685068" MODIFIED="1430147727481"/>
<node TEXT="It is easier to define restrictions on data" STYLE_REF="pre_last_node" ID="ID_440892648" CREATED="1430147685068" MODIFIED="1430147727481"/>
<node TEXT="It is easier to validate the correctness of data" STYLE_REF="pre_last_node" ID="ID_1378878048" CREATED="1430147685068" MODIFIED="1430147727481"/>
<node TEXT="It is easier to convert data between different data types" STYLE_REF="pre_last_node" ID="ID_1068423480" CREATED="1430147685068" MODIFIED="1430147727481"/>
</node>
<node TEXT="XML Schemas use XML Syntax" STYLE_REF="gray_node" ID="ID_1911147422" CREATED="1430147679876" MODIFIED="1430147719183">
<font BOLD="false"/>
<node TEXT="You don&apos;t have to learn a new language" STYLE_REF="pre_last_node" ID="ID_307211260" CREATED="1430147707988" MODIFIED="1430147727482"/>
<node TEXT="You can use your XML editor to edit your Schema files" STYLE_REF="pre_last_node" ID="ID_20008822" CREATED="1430147707989" MODIFIED="1430147727482"/>
<node TEXT="You can use your XML parser to parse your Schema files" STYLE_REF="pre_last_node" ID="ID_636305886" CREATED="1430147707990" MODIFIED="1430147727482"/>
<node TEXT="You can manipulate your Schemas with the XML DOM" STYLE_REF="pre_last_node" ID="ID_555062068" CREATED="1430147707990" MODIFIED="1430147727482"/>
<node TEXT="You can transform your Schemas with XSLT" STYLE_REF="pre_last_node" ID="ID_1536872699" CREATED="1430147707990" MODIFIED="1430147727482"/>
</node>
</node>
</node>
</node>
<node TEXT="CDATA" STYLE_REF="second_node" POSITION="right" ID="ID_921823601" CREATED="1430139438494" MODIFIED="1430148975713">
<edge COLOR="#7c7c00"/>
<hook URI="../../Images/script.png" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Parsed Character Data" STYLE_REF="orange_node" ID="ID_1167698699" CREATED="1430140284468" MODIFIED="1430140692105">
<node TEXT="XML parsers normally parse all the text in an XML document" STYLE_REF="pre_last_node" ID="ID_800483904" CREATED="1430140293143" MODIFIED="1430140839425"/>
<node TEXT="When an XML element is parsed, the text between the XML tags is also parsed" STYLE_REF="pre_last_node" ID="ID_1954984336" CREATED="1430140297494" MODIFIED="1430140845961">
<node TEXT="&lt;message&gt;This text is also parsed&lt;/message&gt;" STYLE_REF="example_node" ID="ID_774363998" CREATED="1430140300952" MODIFIED="1430140829755"/>
</node>
<node TEXT="The parser does this because XML elements can contain other elements" STYLE_REF="pre_last_node" ID="ID_197782700" CREATED="1430140633863" MODIFIED="1430140680556">
<node TEXT="&lt;name&gt;&lt;first&gt;Bill&lt;/first&gt;&lt;last&gt;Gates&lt;/last&gt;&lt;/name&gt;" STYLE_REF="example_node" ID="ID_11088587" CREATED="1430140638928" MODIFIED="1430140829756"/>
</node>
<node TEXT="Parsed Character Data (PCDATA) is a term used about text data that will be parsed by the XML parser" STYLE_REF="pre_last_node" ID="ID_986021689" CREATED="1430140663278" MODIFIED="1430140680556"/>
</node>
<node TEXT="CDATA - (Unparsed) Character Data" STYLE_REF="orange_node" ID="ID_860865266" CREATED="1430140863906" MODIFIED="1430140949699">
<node TEXT="The term CDATA is used about text data that should not be parsed by the XML parser. Everything inside a CDATA section is ignored by the parser" STYLE_REF="pre_last_node" ID="ID_843684469" CREATED="1430140946269" MODIFIED="1430142594465"/>
<node TEXT="A CDATA section" STYLE_REF="yellow_node" ID="ID_1411897556" CREATED="1430141161437" MODIFIED="1430141348153">
<node TEXT="&lt;![CDATA[" STYLE_REF="pre_last_node" ID="ID_355471706" CREATED="1430141168133" MODIFIED="1430142594467"/>
<node TEXT="]]&gt;" STYLE_REF="pre_last_node" ID="ID_948320912" CREATED="1430141177523" MODIFIED="1430142594468"/>
</node>
<node TEXT="Restrictions" STYLE_REF="yellow_node" ID="ID_45326400" CREATED="1430141533685" MODIFIED="1430142585986">
<node TEXT="Nested CDATA sections are not allowed" STYLE_REF="pre_last_node" ID="ID_1141623558" CREATED="1430141363479" MODIFIED="1430142594468"/>
<node TEXT="The &quot;]]&gt;&quot; that marks the end of the CDATA section cannot contain spaces or line breaks" STYLE_REF="pre_last_node" ID="ID_1574624626" CREATED="1430141379344" MODIFIED="1430142594468"/>
<node TEXT="A CDATA section cannot contain the string &quot;]]&gt;&quot;" STYLE_REF="pre_last_node" ID="ID_645065039" CREATED="1430141521325" MODIFIED="1430142594469"/>
</node>
<node TEXT="Example" STYLE_REF="yellow_node" ID="ID_95681355" CREATED="1430142599572" MODIFIED="1430142608289">
<node TEXT="&lt;script&gt;&#xa;&lt;![CDATA[&#xa;function matchwo(a,b)&#xa;{&#xa;if (a &lt; b &amp;&amp; a &lt; 0) then&#xa;  {&#xa;  return 1;&#xa;  }&#xa;else&#xa;  {&#xa;  return 0;&#xa;  }&#xa;}&#xa;]]&gt;&#xa;&lt;/script&gt;" STYLE_REF="example_node" ID="ID_1622936524" CREATED="1430141343692" MODIFIED="1430142597761"/>
</node>
</node>
</node>
<node TEXT="Syntax Rules" STYLE_REF="second_node" POSITION="left" ID="ID_2871174" CREATED="1430118578694" MODIFIED="1430148447485">
<edge COLOR="#009999"/>
<hook URI="../../Images/syntax.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="All XML Elements Must Have a Closing Tag" STYLE_REF="gray_node" ID="ID_69991049" CREATED="1430118603605" MODIFIED="1430129574965">
<node TEXT="&lt;p&gt;This is a paragraph.&lt;/p&gt;&#xa;&lt;br /&gt;" STYLE_REF="example_node" ID="ID_798230179" CREATED="1430118635598" MODIFIED="1430119150893"/>
<node TEXT="XML declaration did not have a closing tag. This is not an error. The declaration is not a part of the XML document itself, and it has no closing tag" STYLE_REF="pre_last_node" ID="ID_96892752" CREATED="1430118646341" MODIFIED="1430119172266"/>
</node>
<node TEXT="XML Tags are Case Sensitive" STYLE_REF="gray_node" ID="ID_8997558" CREATED="1430118654709" MODIFIED="1430129574966">
<node TEXT="The tag &lt;Letter&gt; is different from the tag &lt;letter&gt;" STYLE_REF="pre_last_node" ID="ID_593931422" CREATED="1430118664101" MODIFIED="1430119172266"/>
<node TEXT="Opening and closing tags must be written with the same case" STYLE_REF="pre_last_node" ID="ID_878941795" CREATED="1430118674837" MODIFIED="1430119172266"/>
<node TEXT="&lt;Message&gt;This is incorrect&lt;/message&gt;&#xa;&lt;message&gt;This is correct&lt;/message&gt;" STYLE_REF="example_node" ID="ID_90879855" CREATED="1430118686757" MODIFIED="1430119440154"/>
</node>
<node TEXT="XML Elements Must be Properly Nested" STYLE_REF="gray_node" ID="ID_51865095" CREATED="1430118694181" MODIFIED="1430129574966">
<node TEXT="&lt;b&gt;&lt;i&gt;This text is bold and italic&lt;/i&gt;&lt;/b&gt;" STYLE_REF="example_node" ID="ID_427441020" CREATED="1430118795853" MODIFIED="1430119150893"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      correct
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="&lt;b&gt;&lt;i&gt;This text is bold and italic&lt;/b&gt;&lt;/i&gt;" STYLE_REF="example_node" ID="ID_843207524" CREATED="1430118810717" MODIFIED="1430119150893"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      incorrect
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
<node TEXT="XML Documents Must Have a Root Element" STYLE_REF="gray_node" ID="ID_367534561" CREATED="1430118849750" MODIFIED="1430129574967">
<node TEXT="&lt;root&gt;&#xa;  &lt;child&gt;&#xa;    &lt;subchild&gt;.....&lt;/subchild&gt;&#xa;  &lt;/child&gt;&#xa;&lt;/root&gt;" STYLE_REF="example_node" ID="ID_1151972566" CREATED="1430118867974" MODIFIED="1430119150893"/>
</node>
<node TEXT="XML Attribute Values Must be Quoted" STYLE_REF="gray_node" ID="ID_1973582824" CREATED="1430118876366" MODIFIED="1430129574967">
<node TEXT="&lt;note date=12/11/2007&gt;&#xa;  &lt;to&gt;Tove&lt;/to&gt;&#xa;  &lt;from&gt;Jani&lt;/from&gt;&#xa;&lt;/note&gt;" STYLE_REF="example_node" ID="ID_1909458053" CREATED="1430118892117" MODIFIED="1430119150893"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      incorrect
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="&lt;note date=&quot;12/11/2007&quot;&gt;&#xa;  &lt;to&gt;Tove&lt;/to&gt;&#xa;  &lt;from&gt;Jani&lt;/from&gt;&#xa;&lt;/note&gt;" STYLE_REF="example_node" ID="ID_1900570456" CREATED="1430118898005" MODIFIED="1430119150893"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      correct
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
<node TEXT="Entity References" STYLE_REF="gray_node" ID="ID_459418410" CREATED="1430118923749" MODIFIED="1430129574967">
<node TEXT="Some characters have a special meaning in XML.&#xa;If you place a character like &quot;&lt;&quot; inside an XML element, it will generate an error because the parser interprets it as the start of a new element." STYLE_REF="pre_last_node" ID="ID_466987587" CREATED="1430118949158" MODIFIED="1430119172266"/>
<node TEXT="There are 5 pre-defined entity references in XML" STYLE_REF="yellow_node" ID="ID_1061471763" CREATED="1430118964917" MODIFIED="1430119187483">
<node TEXT="&amp;lt;" STYLE_REF="pre_last_node" ID="ID_1431426567" CREATED="1430118970701" MODIFIED="1430119172266"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &lt;
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="&amp;gt;" STYLE_REF="pre_last_node" ID="ID_717236551" CREATED="1430118970701" MODIFIED="1430119172266"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &gt;&#160;
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="&amp;amp;" STYLE_REF="pre_last_node" ID="ID_975799221" CREATED="1430118970701" MODIFIED="1430119172266"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &amp;
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="&amp;apos;" STYLE_REF="pre_last_node" ID="ID_1506835629" CREATED="1430118970701" MODIFIED="1430119172266"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      '
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="&amp;quot;" STYLE_REF="pre_last_node" ID="ID_347754453" CREATED="1430118970701" MODIFIED="1430119172266"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &quot;
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
</node>
<node TEXT="Comments in XML" STYLE_REF="gray_node" ID="ID_1224961631" CREATED="1430119043685" MODIFIED="1430129574967">
<node TEXT="&lt;!-- This is a comment --&gt;" STYLE_REF="example_node" ID="ID_292246599" CREATED="1430119051719" MODIFIED="1430120928927"/>
</node>
<node TEXT="White-space is Preserved in XML" STYLE_REF="gray_node" ID="ID_1914210294" CREATED="1430119066774" MODIFIED="1430129574967">
<node TEXT="XML does not truncate multiple white-spaces in a document" STYLE_REF="pre_last_node" ID="ID_1826695742" CREATED="1430119083294" MODIFIED="1430119448368"/>
</node>
<node TEXT="XML Stores New Line as LF" STYLE_REF="gray_node" ID="ID_1734755432" CREATED="1430119110366" MODIFIED="1430129574967">
<node TEXT="Windows applications store a new line as: carriage return and line feed (CR+LF)" STYLE_REF="pre_last_node" ID="ID_1663561033" CREATED="1430119123622" MODIFIED="1430119175264"/>
<node TEXT="Unix and Mac OSX uses LF" STYLE_REF="pre_last_node" ID="ID_11596231" CREATED="1430119123622" MODIFIED="1430119177904"/>
<node TEXT="Old Mac systems uses CR" STYLE_REF="pre_last_node" ID="ID_978671369" CREATED="1430119123622" MODIFIED="1430119180545"/>
<node TEXT="XML stores a new line as LF" STYLE_REF="pre_last_node" ID="ID_5141429" CREATED="1430119123622" MODIFIED="1430119183936"/>
</node>
</node>
</node>
</map>
