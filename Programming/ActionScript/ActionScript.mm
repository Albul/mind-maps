<map version="freeplane 1.3.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="ActionScript" STYLE_REF="main_node" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1424957961985" VGAP="24">
<hook NAME="AutomaticEdgeColor" COUNTER="11"/>
<hook URI="../../Images/as3-large.jpg" SIZE="1.0" NAME="ExternalObject"/>
<hook NAME="MapStyle" zoom="1.003">
    <properties show_icon_for_attributes="true" show_note_icons="true"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
<stylenode TEXT="main_node" BACKGROUND_COLOR="#ccccff">
<font SIZE="18" BOLD="true"/>
<edge STYLE="sharp_bezier" WIDTH="4"/>
</stylenode>
<stylenode TEXT="second_node" BACKGROUND_COLOR="#00cccc" STYLE="bubble">
<font SIZE="14"/>
</stylenode>
<stylenode TEXT="third_node" BACKGROUND_COLOR="#ffbdf4" STYLE="bubble">
<font SIZE="11" BOLD="true"/>
</stylenode>
<stylenode TEXT="orange_node" BACKGROUND_COLOR="#ff9900" STYLE="bubble">
<font BOLD="true"/>
</stylenode>
<stylenode TEXT="gray_node" BACKGROUND_COLOR="#9ec19e">
<font BOLD="true"/>
</stylenode>
<stylenode TEXT="yellow_node" BACKGROUND_COLOR="#ffff66" STYLE="bubble">
<edge WIDTH="1"/>
</stylenode>
<stylenode TEXT="example_node" BACKGROUND_COLOR="#afcdfa">
<edge STYLE="bezier" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="pre_last_node" BACKGROUND_COLOR="#99ff99" STYLE="bubble">
<edge STYLE="bezier" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="last_node" BACKGROUND_COLOR="#ffffff">
<edge STYLE="bezier" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="warning_node" BACKGROUND_COLOR="#fda6a6">
<icon BUILTIN="messagebox_warning"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<node TEXT="Interfaces" STYLE_REF="second_node" POSITION="right" ID="ID_649086059" CREATED="1424934357540" MODIFIED="1424953014859">
<edge COLOR="#ff0000"/>
<hook URI="../../Images/interface-2.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="General" STYLE_REF="orange_node" ID="ID_1164844419" CREATED="1424934418644" MODIFIED="1424934989602">
<node TEXT="Why" STYLE_REF="yellow_node" ID="ID_1735677438" CREATED="1424934901599" MODIFIED="1424935002379">
<node TEXT="A single class cannot extend more than one class." STYLE_REF="pre_last_node" ID="ID_1577831852" CREATED="1424934918596" MODIFIED="1424935600641"/>
<node TEXT="Class can inherit from another class while also implementing an interface. Instances of the subclass belong to both the superclass&#x2019;s datatype and the interface&#x2019;s datatype." STYLE_REF="pre_last_node" ID="ID_1101745696" CREATED="1424934936557" MODIFIED="1424935600644"/>
</node>
<node TEXT="Definition" STYLE_REF="yellow_node" ID="ID_241696239" CREATED="1424934426452" MODIFIED="1436775854282">
<node TEXT="Interface defines a new datatype without implementing any of the&#xa;methods of that datatype." STYLE_REF="pre_last_node" ID="ID_1884428759" CREATED="1424934450564" MODIFIED="1436775854281"/>
<node TEXT="Syntactically, an interface is simply a list of methods." STYLE_REF="pre_last_node" ID="ID_250611762" CREATED="1424934464299" MODIFIED="1424935600648"/>
</node>
<node TEXT="Syntax" STYLE_REF="yellow_node" ID="ID_1219907743" CREATED="1424934481972" MODIFIED="1424935002371">
<node TEXT="interface SomeName&#xa;     function method1 (param1:datatype,...paramn:datatype):returnType;&#xa;     function method2 (param1:datatype,...paramn:datatype):returnType;&#xa;}" STYLE_REF="example_node" ID="ID_1877989917" CREATED="1424934484964" MODIFIED="1425207366235"/>
</node>
<node TEXT="Not allowed" STYLE_REF="yellow_node" ID="ID_721566625" CREATED="1424934537755" MODIFIED="1424935002370">
<node TEXT="Variable definitions" STYLE_REF="pre_last_node" ID="ID_592538240" CREATED="1424934552140" MODIFIED="1424935600651"/>
<node TEXT="Nested interface definitions" STYLE_REF="pre_last_node" ID="ID_1824411051" CREATED="1424934575709" MODIFIED="1424935600651"/>
<node TEXT="Access-control modifier" STYLE_REF="pre_last_node" ID="ID_1342340594" CREATED="1424934688276" MODIFIED="1424935600652"/>
</node>
<node TEXT="Implemented methods" STYLE_REF="yellow_node" ID="ID_484687451" CREATED="1424934772084" MODIFIED="1424935002368">
<node TEXT="Public access control identifier" STYLE_REF="pre_last_node" ID="ID_117990075" CREATED="1424934797300" MODIFIED="1424935600652"/>
<node TEXT="The same name as the interface method" STYLE_REF="pre_last_node" ID="ID_1312763990" CREATED="1424934817020" MODIFIED="1424935600653"/>
<node TEXT="The same number of parameters" STYLE_REF="pre_last_node" ID="ID_215681606" CREATED="1424934837853" MODIFIED="1424935600653">
<node TEXT="Each with data types that match the interface method parameter data types" ID="ID_1489295061" CREATED="1424934862196" MODIFIED="1424934865800"/>
</node>
<node TEXT="The same return type" STYLE_REF="pre_last_node" ID="ID_1625674287" CREATED="1424934873647" MODIFIED="1424935600653"/>
</node>
</node>
<node TEXT="Multi-implements" STYLE_REF="orange_node" ID="ID_1581519411" CREATED="1424934952372" MODIFIED="1424935050379"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Class can legally implement more than one interface
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Syntax" STYLE_REF="yellow_node" ID="ID_850347708" CREATED="1424934965708" MODIFIED="1424935002362">
<node TEXT="class SomeName implements SomeInterface, SomeOtherInterface {&#xa;}" STYLE_REF="example_node" ID="ID_1616225737" CREATED="1424934970044" MODIFIED="1425207363385"/>
</node>
</node>
<node TEXT="Interface Inheritance" STYLE_REF="orange_node" ID="ID_355981" CREATED="1424935111284" MODIFIED="1424935315617">
<node TEXT="Syntax" STYLE_REF="yellow_node" ID="ID_880854573" CREATED="1424935458668" MODIFIED="1424935526099">
<node TEXT="Classes that implement interface IntB must provide&#xa;definitions for both methodA( ) and methodB( )" STYLE_REF="example_node" ID="ID_271707365" CREATED="1424935463908" MODIFIED="1425207381865"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      public interface IntA {
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;function methodA ( ):void;
    </p>
    <p>
      }
    </p>
    <p>
      public interface IntB extends IntA {
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;function methodB ( ):void;
    </p>
    <p>
      }
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Interface can extend more than one interface" STYLE_REF="example_node" ID="ID_233808684" CREATED="1424935499668" MODIFIED="1425207381868"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      public interface IntC {
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;function methodC ( ):void;
    </p>
    <p>
      }
    </p>
    <p>
      public interface IntD {
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;function methodD ( ):void;
    </p>
    <p>
      }
    </p>
    <p>
      public interface IntE extends IntC, IntD {
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;function methodE ( ):void;
    </p>
    <p>
      }
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="Marker Interfaces" STYLE_REF="orange_node" ID="ID_1614563393" CREATED="1424935122970" MODIFIED="1424935188772"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Empty interfaces, called marker interfaces is useful
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Marked class is a class which implements the marker interface" STYLE_REF="yellow_node" ID="ID_1838956020" CREATED="1424935224276" MODIFIED="1424935552014"/>
<node TEXT="Requirements for the marked classes are provided&#xa;by the documentation for the marker interface" STYLE_REF="yellow_node" ID="ID_1790694183" CREATED="1424935208382" MODIFIED="1424952781275"/>
<node TEXT="Examples" STYLE_REF="yellow_node" ID="ID_1110892792" CREATED="1424935144572" MODIFIED="1424935552010">
<node TEXT=" IBitmapDrawable, which designates a class as eligible for drawing into a BitmapData object" STYLE_REF="pre_last_node" ID="ID_113824703" CREATED="1424935158124" MODIFIED="1424935570490"/>
</node>
</node>
<node TEXT="Use" STYLE_REF="orange_node" ID="ID_915703402" CREATED="1424935322108" MODIFIED="1424935529390">
<node TEXT="Any class that is expected to be subclassed&#xa;should be an implementation of an interface" STYLE_REF="yellow_node" ID="ID_215274174" CREATED="1424935341588" MODIFIED="1424952776151">
<node TEXT="It can be subclassed directly" STYLE_REF="pre_last_node" ID="ID_207554081" CREATED="1424935366532" MODIFIED="1424935570492"/>
<node TEXT="It can be used via composition by a class that inherits from another class" STYLE_REF="pre_last_node" ID="ID_30661734" CREATED="1424935382332" MODIFIED="1424935570493"/>
</node>
<node TEXT="Examples" STYLE_REF="yellow_node" ID="ID_1575412071" CREATED="1424935412036" MODIFIED="1424935526102">
<node TEXT="" STYLE_REF="example_node" ID="ID_1114186903" CREATED="1424935963220" MODIFIED="1425207394475">
<hook URI="interface1.jpg" SIZE="1.0" NAME="ExternalObject"/>
</node>
<node TEXT="" STYLE_REF="example_node" ID="ID_1727695780" CREATED="1424935975148" MODIFIED="1425207394476">
<hook URI="interface2.jpg" SIZE="1.0" NAME="ExternalObject"/>
</node>
</node>
</node>
</node>
<node TEXT="Conditionals &amp; loops" STYLE_REF="second_node" POSITION="right" ID="ID_183229642" CREATED="1424953187935" MODIFIED="1424957961983">
<edge COLOR="#046b04"/>
<node TEXT="Conditionals" ID="ID_624534782" CREATED="1424955737292" MODIFIED="1424955783691" BACKGROUND_COLOR="#9ec19e">
<font BOLD="true"/>
<hook URI="../../Images/if.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="IF" STYLE_REF="orange_node" ID="ID_495361719" CREATED="1424953222639" MODIFIED="1424953317763"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      if (testExpr) {
    </p>
    <p>
      &#160;&#160;&#160;&#160;codeBlock1
    </p>
    <p>
      } else {
    </p>
    <p>
      &#160;&#160;&#160;&#160;codeBlock2
    </p>
    <p>
      }
    </p>
  </body>
</html>
</richcontent>
<node TEXT="testExpr" STYLE_REF="yellow_node" ID="ID_1196057543" CREATED="1424953259887" MODIFIED="1424953470795">
<node TEXT="Shold be Boolean type" STYLE_REF="pre_last_node" ID="ID_407961246" CREATED="1424953413984" MODIFIED="1424953477818"/>
<node TEXT="if it isn&apos;t Boolean, system will convert it to Boolean automatically" STYLE_REF="pre_last_node" ID="ID_1470479020" CREATED="1424953427056" MODIFIED="1424953477822"/>
</node>
<node TEXT="Else clause" STYLE_REF="yellow_node" ID="ID_583081428" CREATED="1424953270231" MODIFIED="1424953470801">
<node TEXT="Can be omitted" STYLE_REF="pre_last_node" ID="ID_1574955077" CREATED="1424953445273" MODIFIED="1424953477823"/>
</node>
<node TEXT="Chaining if statements" STYLE_REF="yellow_node" ID="ID_1710289547" CREATED="1424953550122" MODIFIED="1424953585677">
<node TEXT="if (testExpression1) {&#xa;    codeBlock1&#xa;} else if (testExpression2) {&#xa;    codeBlock2&#xa;} else {&#xa;    codeBlock3&#xa;}" STYLE_REF="example_node" ID="ID_63359765" CREATED="1424953559514" MODIFIED="1425207296577"/>
</node>
</node>
<node TEXT="Switch" STYLE_REF="orange_node" ID="ID_98315403" CREATED="1424953225023" MODIFIED="1424953657685"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p style="margin-top: 0">
      switch (testExpression) {
    </p>
    <p style="margin-top: 0">
      case expression1:
    </p>
    <p style="margin-top: 0">
      &#160;&#160;&#160;&#160;codeBlock1
    </p>
    <p style="margin-top: 0">
      &#160;&#160;&#160;&#160;break;
    </p>
    <p style="margin-top: 0">
      case expression2:
    </p>
    <p style="margin-top: 0">
      &#160;&#160;&#160;&#160;codeBlock2
    </p>
    <p style="margin-top: 0">
      &#160;&#160;&#160;&#160;break;
    </p>
    <p style="margin-top: 0">
      default:
    </p>
    <p style="margin-top: 0">
      &#160;&#160;&#160;&#160;codeBlock3
    </p>
    <p style="margin-top: 0">
      }
    </p>
  </body>
</html>
</richcontent>
<node TEXT="testExpression" STYLE_REF="yellow_node" ID="ID_1156540014" CREATED="1424954165406" MODIFIED="1436776006490">
<node ID="ID_840738782" CREATED="1424954180727" MODIFIED="1424954547481"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Compares with case expression use&#160; <b>strict equality operator ===</b>
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Matches a case expression, all statements immediately following that&#xa;case label are executed" ID="ID_591301282" CREATED="1424954284265" MODIFIED="1436776006489"/>
</node>
<node TEXT="default label" STYLE_REF="yellow_node" ID="ID_385395733" CREATED="1424953881916" MODIFIED="1424954366296">
<node TEXT="Normally listed last" ID="ID_939309757" CREATED="1424953956325" MODIFIED="1424954104267"/>
<node TEXT="Is not mandatory" ID="ID_1827671700" CREATED="1424953994958" MODIFIED="1424954108067"/>
<node TEXT="All statements after this label are executed If no case expression matches testExpression" ID="ID_569325735" CREATED="1424954039838" MODIFIED="1424954083659"/>
</node>
</node>
</node>
<node TEXT="Loops" ID="ID_1797562350" CREATED="1424955733276" MODIFIED="1424956254619" BACKGROUND_COLOR="#9ec19e">
<font BOLD="true"/>
<hook URI="../../Images/loop.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="while" STYLE_REF="orange_node" ID="ID_495262317" CREATED="1424954496242" MODIFIED="1436776074456"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      while (testExpression) {
    </p>
    <p>
      &#160;&#160;&#160;&#160;codeBlock
    </p>
    <p>
      }
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Principle" STYLE_REF="yellow_node" ID="ID_842936179" CREATED="1424954823324" MODIFIED="1424954839916">
<node TEXT="If testExpression is true, the code in codeBlock is executed" STYLE_REF="pre_last_node" ID="ID_737779310" CREATED="1424954582226" MODIFIED="1424954861637"/>
<node TEXT="When the codeBlock is finished, execution begins again at&#xa;the beginning of the while statement" STYLE_REF="pre_last_node" ID="ID_1764968518" CREATED="1424954626195" MODIFIED="1424954861634"/>
<node TEXT="This process continues until testExpression becomes false" STYLE_REF="pre_last_node" ID="ID_660098447" CREATED="1424954672251" MODIFIED="1424954861633"/>
</node>
<node TEXT="Infinite" STYLE_REF="yellow_node" ID="ID_28132660" CREATED="1424954740131" MODIFIED="1424954839919">
<node TEXT="If the loop executes infinitely, eventually causing the Flash runtime&#xa;to generate an error, which stops the loop" STYLE_REF="pre_last_node" ID="ID_1305244805" CREATED="1424954749428" MODIFIED="1424954861629"/>
</node>
<node TEXT="Interation" STYLE_REF="yellow_node" ID="ID_703291021" CREATED="1424955036910" MODIFIED="1436776074456">
<node TEXT="Iteration" STYLE_REF="pre_last_node" ID="ID_294861418" CREATED="1424955163735" MODIFIED="1436776037713">
<font BOLD="true"/>
<node TEXT="One execution of a loop body" ID="ID_1845542023" CREATED="1424955170815" MODIFIED="1424955174212"/>
</node>
<node TEXT="Iterator" STYLE_REF="pre_last_node" ID="ID_644294343" CREATED="1424955140191" MODIFIED="1424955186680">
<font BOLD="true"/>
<node TEXT="A variable, such as counter, that controls the number of times&#xa;a given loop iterates" ID="ID_1441840467" CREATED="1424955144031" MODIFIED="1424955158285"/>
</node>
</node>
<node TEXT="break;" STYLE_REF="yellow_node" ID="ID_1640112684" CREATED="1424956051159" MODIFIED="1424956096559"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      while (expr) {
    </p>
    <p>
      &#160;&#160;&#160;&#160;if (some) break;
    </p>
    <p>
      }
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Immediately terminates a loop" STYLE_REF="pre_last_node" ID="ID_1811526570" CREATED="1424956054711" MODIFIED="1424956108713"/>
</node>
<node TEXT="continue;" STYLE_REF="yellow_node" ID="ID_729392194" CREATED="1424956799165" MODIFIED="1424956836405">
<node TEXT="The continue statement pauses the execution of the loop body,&#xa;applies the loop&apos;s update, and then restarts the code in the loop body" STYLE_REF="pre_last_node" ID="ID_1925374311" CREATED="1424956803629" MODIFIED="1424956845207"/>
</node>
</node>
<node TEXT="do...while" STYLE_REF="orange_node" ID="ID_1087662879" CREATED="1424956166376" MODIFIED="1429800897592"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      do {
    </p>
    <p>
      &#160;&#160;&#160;&#160;codeBlock
    </p>
    <p>
      } while (testExpression);
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Similar to while loop" STYLE_REF="pre_last_node" ID="ID_1154635304" CREATED="1424956177154" MODIFIED="1424956269066"/>
<node TEXT="But, codeBlock will be executed at least once" STYLE_REF="pre_last_node" ID="ID_1501788511" CREATED="1424956223344" MODIFIED="1424956269070"/>
</node>
<node TEXT="for" STYLE_REF="orange_node" ID="ID_1313964829" CREATED="1424956272481" MODIFIED="1429800894776"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      for (initialization; testExpression; update) {
    </p>
    <p>
      &#160;&#160;&#160;&#160;codeBlock
    </p>
    <p>
      }
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Initialization statement" STYLE_REF="pre_last_node" ID="ID_1620199843" CREATED="1424956396721" MODIFIED="1424956987809">
<node TEXT="Is performed only once" ID="ID_1470553342" CREATED="1424956489714" MODIFIED="1424956979404"/>
</node>
<node TEXT="Update statement" STYLE_REF="pre_last_node" ID="ID_1481952509" CREATED="1424956473338" MODIFIED="1424956987813">
<node TEXT="Is executed at the end of each loop iteration" ID="ID_1986876332" CREATED="1424956479219" MODIFIED="1424956982340"/>
</node>
</node>
<node TEXT="for..in" STYLE_REF="orange_node" ID="ID_243790545" CREATED="1424956913710" MODIFIED="1429800892656"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Iterates via name of the property
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Iterates through" STYLE_REF="yellow_node" ID="ID_1391307683" CREATED="1424956948550" MODIFIED="1424957078081">
<node TEXT="The properties of an object" STYLE_REF="pre_last_node" ID="ID_1517752977" CREATED="1424956953326" MODIFIED="1424957286682">
<node TEXT="Properties may appear in a seemingly random order" ID="ID_1227025884" CREATED="1424957094904" MODIFIED="1424957098869"/>
</node>
<node TEXT="The elements of an array" STYLE_REF="pre_last_node" ID="ID_1011196012" CREATED="1424956962886" MODIFIED="1424957286680"/>
</node>
<node TEXT="Iterate only" STYLE_REF="yellow_node" ID="ID_508560589" CREATED="1424957258433" MODIFIED="1424957280651">
<node TEXT="Through properties that are added dynamically" STYLE_REF="pre_last_node" ID="ID_1550062787" CREATED="1424957269705" MODIFIED="1424957286676"/>
</node>
<node TEXT="Examples" STYLE_REF="yellow_node" ID="ID_1661231537" CREATED="1424957044079" MODIFIED="1424957078086">
<node TEXT="var myObj:Object = {x:20, y:30};&#xa;for (var i:String in myObj) {&#xa;    trace(i + &quot;: &quot; + myObj[i]);&#xa;}" STYLE_REF="example_node" ID="ID_1956864631" CREATED="1424957046663" MODIFIED="1425207337580">
<node TEXT="// output:&#xa;// x: 20&#xa;// y: 30" ID="ID_1307886427" CREATED="1424957149072" MODIFIED="1424957150316"/>
</node>
<node TEXT="var myArray:Array = [&quot;one&quot;, &quot;two&quot;, &quot;three&quot;];&#xa;for (var i:String in myArray) {&#xa;    trace(myArray[i]);&#xa;}" STYLE_REF="example_node" ID="ID_1093585580" CREATED="1424957058847" MODIFIED="1425207337588"/>
</node>
</node>
<node TEXT="for each..in" STYLE_REF="orange_node" ID="ID_1034845067" CREATED="1424957303864" MODIFIED="1424957511876"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Iterates via property
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Iterates through" STYLE_REF="yellow_node" ID="ID_803256511" CREATED="1424957389914" MODIFIED="1429800916524">
<node TEXT="Items of a collection" STYLE_REF="pre_last_node" ID="ID_1884015345" CREATED="1424957405250" MODIFIED="1429800916524">
<node TEXT="Tags in an XML" ID="ID_1861010290" CREATED="1424957427178" MODIFIED="1424957430392"/>
<node TEXT="Properties in generic object" ID="ID_1093692454" CREATED="1424957457387" MODIFIED="1424957467503"/>
</node>
</node>
<node TEXT="Cannot iterate" STYLE_REF="yellow_node" ID="ID_1656242350" CREATED="1424957675469" MODIFIED="1424957704591">
<node TEXT="Through any fixed properties, which are properties&#xa;defined as part of the class definition" STYLE_REF="pre_last_node" ID="ID_1964675936" CREATED="1424957688317" MODIFIED="1424957708831"/>
</node>
<node TEXT="Examples" STYLE_REF="yellow_node" ID="ID_379290547" CREATED="1424957525643" MODIFIED="1424957584397">
<node TEXT="var myObj:Object = {x:20, y:30};&#xa;for each (var num in myObj) {&#xa;    trace(num);&#xa;}" STYLE_REF="example_node" ID="ID_915533821" CREATED="1424957528291" MODIFIED="1425207351822"/>
<node TEXT="var myXML:XML = &lt;users&gt;&#xa;    &lt;fname&gt;Jane&lt;/fname&gt;&#xa;    &lt;fname&gt;Susan&lt;/fname&gt;&#xa;    &lt;fname&gt;John&lt;/fname&gt;&#xa;&lt;/users&gt;;&#xa; &#xa;for each (var item in myXML.fname) {&#xa;    trace(item);&#xa;}" STYLE_REF="example_node" ID="ID_1403404230" CREATED="1424957548499" MODIFIED="1425207351819">
<node TEXT="// output :&#xa;Jane&#xa;Susan&#xa;John" ID="ID_1275216021" CREATED="1424957555084" MODIFIED="1424957569864"/>
</node>
<node TEXT="var myArray:Array = [&quot;one&quot;, &quot;two&quot;, &quot;three&quot;];&#xa;for each (var item in myArray)  {&#xa;    trace(item);&#xa;}" STYLE_REF="example_node" ID="ID_1238442493" CREATED="1424957636540" MODIFIED="1425207351817"/>
</node>
</node>
<node TEXT="continue;" STYLE_REF="orange_node" ID="ID_672149924" CREATED="1429800882760" MODIFIED="1436776114506">
<node TEXT="outerLoop: for (var i:int = 0; i &lt; _usersList.dataProvider.length; i++) {&#xa;    var user:Object = _usersList.dataProvider[i];&#xa;    for each (var onlineUser:OnlineUser in _presentationOnlineUsers) {&#xa;        if (user.id == onlineUser.user.id) {&#xa;            _usersList.setUserWatching(i, true);&#xa;            continue outerLoop;&#xa;        }&#xa;    }&#xa;    _usersList.setUserWatching(i, false);&#xa;}" STYLE_REF="example_node" ID="ID_1840390994" CREATED="1429800819568" MODIFIED="1436776114505">
<node TEXT="Exit to outer loop" STYLE_REF="pre_last_node" ID="ID_1321205954" CREATED="1429800977832" MODIFIED="1429801023934"/>
</node>
</node>
<node TEXT="break;" STYLE_REF="orange_node" ID="ID_738111590" CREATED="1429800933800" MODIFIED="1429801016230"/>
<node TEXT="Performance" STYLE_REF="gray_node" ID="ID_1706074079" CREATED="1432356428393" MODIFIED="1436776132000">
<node TEXT="Traversing array with 1000000 items" STYLE_REF="yellow_node" ID="ID_1854729126" CREATED="1432356524210" MODIFIED="1436776132000">
<node TEXT="for" STYLE_REF="pre_last_node" ID="ID_144761094" CREATED="1432356573378" MODIFIED="1432356709288">
<node TEXT="520 ms" ID="ID_1710516428" CREATED="1432356583243" MODIFIED="1432356599390"/>
</node>
<node TEXT="for each..in" STYLE_REF="pre_last_node" ID="ID_987452115" CREATED="1432356601234" MODIFIED="1432356709289">
<node TEXT="1753 ms" ID="ID_809861313" CREATED="1432356612554" MODIFIED="1432356613628"/>
</node>
<node TEXT="while" STYLE_REF="pre_last_node" ID="ID_1270782707" CREATED="1432356616690" MODIFIED="1432356709290">
<node TEXT="2775 ms" ID="ID_890293159" CREATED="1432356633657" MODIFIED="1432356635419"/>
</node>
<node TEXT="util loop" STYLE_REF="pre_last_node" ID="ID_1937136973" CREATED="1432356642098" MODIFIED="1436776130856"><richcontent TYPE="DETAILS" HIDDEN="true">

<html>
  <head>
    
  </head>
  <body>
    <p>
      myVector.forEach(arrayConverter);
    </p>
    <p>
      
    </p>
    <p>
      function arrayConverter(element:*, index:int, array:Array):void{
    </p>
    <p>
      &#160;&#160;&#160;&#160;myArray[myArray.length] = element;
    </p>
    <p>
      }
    </p>
  </body>
</html>
</richcontent>
<node TEXT="4059 ms" ID="ID_1728795272" CREATED="1432356649515" MODIFIED="1432356650870"/>
</node>
</node>
</node>
</node>
</node>
<node TEXT="Operators" STYLE_REF="second_node" POSITION="left" ID="ID_1770462886" CREATED="1428499999733" MODIFIED="1428500947834">
<edge COLOR="#7c007c"/>
<hook URI="../../../../../D:/Documents/MindMaps/Images/operator.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Precedence" STYLE_REF="orange_node" ID="ID_937118164" CREATED="1428500015305" MODIFIED="1428565231011" VSHIFT="19">
<edge WIDTH="thin"/>
<node TEXT="Primary" STYLE_REF="yellow_node" ID="ID_1303228651" CREATED="1428500031277" MODIFIED="1428565249011">
<edge STYLE="bezier" WIDTH="thin"/>
<node TEXT="[]" STYLE_REF="pre_last_node" ID="ID_1657808744" CREATED="1428500031277" MODIFIED="1428500474438"/>
<node TEXT="{x:y}" STYLE_REF="pre_last_node" ID="ID_1830146059" CREATED="1428500475230" MODIFIED="1428500549010"/>
<node TEXT="()" STYLE_REF="pre_last_node" ID="ID_1206469912" CREATED="1428500479016" MODIFIED="1428500549011"/>
<node TEXT="f(x)" STYLE_REF="pre_last_node" ID="ID_550635230" CREATED="1428500484536" MODIFIED="1428500549011"/>
<node TEXT="new" STYLE_REF="pre_last_node" ID="ID_707310147" CREATED="1428500489440" MODIFIED="1428500549012"/>
<node TEXT="x.y" STYLE_REF="pre_last_node" ID="ID_92480315" CREATED="1428500493250" MODIFIED="1428500549012"/>
<node TEXT="x[y]" STYLE_REF="pre_last_node" ID="ID_1790473251" CREATED="1428500522422" MODIFIED="1428500549012"/>
<node TEXT="&lt;&gt;&lt;/&gt;" STYLE_REF="pre_last_node" ID="ID_1345725347" CREATED="1428500529327" MODIFIED="1428500549012"/>
<node TEXT="@" STYLE_REF="pre_last_node" ID="ID_368158000" CREATED="1428500533816" MODIFIED="1428500549013"/>
<node TEXT="::" STYLE_REF="pre_last_node" ID="ID_538658285" CREATED="1428500512919" MODIFIED="1428500549013"/>
<node TEXT=".." STYLE_REF="pre_last_node" ID="ID_276193292" CREATED="1428500516313" MODIFIED="1428500549013"/>
</node>
<node TEXT="Postfix" STYLE_REF="yellow_node" ID="ID_1722108" CREATED="1428500031279" MODIFIED="1428565249013">
<edge STYLE="bezier" WIDTH="thin"/>
<node TEXT="x++" STYLE_REF="pre_last_node" ID="ID_375209249" CREATED="1428500031281" MODIFIED="1428500146245"/>
<node STYLE_REF="pre_last_node" ID="ID_950754883" CREATED="1428500147962" MODIFIED="1428500210277"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      x--
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Unary" STYLE_REF="yellow_node" ID="ID_1623908210" CREATED="1428500031282" MODIFIED="1428565249013">
<edge STYLE="bezier" WIDTH="thin"/>
<node TEXT="++x" STYLE_REF="pre_last_node" ID="ID_1457689314" CREATED="1428500031283" MODIFIED="1428500205051"/>
<node STYLE_REF="pre_last_node" ID="ID_1399992611" CREATED="1428500155841" MODIFIED="1428500205052"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      --x
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="+" STYLE_REF="pre_last_node" ID="ID_1765832774" CREATED="1428500162785" MODIFIED="1428500205053"/>
<node TEXT="-" STYLE_REF="pre_last_node" ID="ID_561224183" CREATED="1428500169495" MODIFIED="1428500205053"/>
<node TEXT="~" STYLE_REF="pre_last_node" ID="ID_1009627159" CREATED="1428500173141" MODIFIED="1428500205053"/>
<node TEXT="!" STYLE_REF="pre_last_node" ID="ID_1800057027" CREATED="1428500178385" MODIFIED="1428500205054"/>
<node TEXT="delete" STYLE_REF="pre_last_node" ID="ID_1878344055" CREATED="1428500182343" MODIFIED="1428500205054"/>
<node TEXT="typeof" STYLE_REF="pre_last_node" ID="ID_1275707887" CREATED="1428500186359" MODIFIED="1428500205054"/>
<node TEXT="void" STYLE_REF="pre_last_node" ID="ID_1331681996" CREATED="1428500191078" MODIFIED="1428500205055"/>
</node>
<node TEXT="Multiplicative" STYLE_REF="yellow_node" ID="ID_1570905949" CREATED="1428500031284" MODIFIED="1428565249014">
<edge STYLE="bezier" WIDTH="thin"/>
<node TEXT="*" STYLE_REF="pre_last_node" ID="ID_25237154" CREATED="1428500031285" MODIFIED="1428500224228"/>
<node TEXT="/" STYLE_REF="pre_last_node" ID="ID_421353767" CREATED="1428500214904" MODIFIED="1428500405596"/>
<node TEXT="%" STYLE_REF="pre_last_node" ID="ID_826272317" CREATED="1428500218133" MODIFIED="1428500405595"/>
</node>
<node TEXT="Additive" STYLE_REF="yellow_node" ID="ID_1162501775" CREATED="1428500031286" MODIFIED="1428565249015">
<edge STYLE="bezier" WIDTH="thin"/>
<node TEXT="+" STYLE_REF="pre_last_node" ID="ID_1290578091" CREATED="1428500031288" MODIFIED="1428500231739"/>
<node TEXT="-" STYLE_REF="pre_last_node" ID="ID_1939220874" CREATED="1428500226745" MODIFIED="1428500405595"/>
</node>
<node TEXT="Bitwise shift" STYLE_REF="yellow_node" ID="ID_1674476022" CREATED="1428500031289" MODIFIED="1428565249015">
<edge STYLE="bezier" WIDTH="thin"/>
<node TEXT="&lt;&lt;" STYLE_REF="pre_last_node" ID="ID_1513325381" CREATED="1428500031290" MODIFIED="1428500251220"/>
<node TEXT="&gt;&gt;" STYLE_REF="pre_last_node" ID="ID_1630240384" CREATED="1428500241493" MODIFIED="1428500254468"/>
<node TEXT="&gt;&gt;&gt;" STYLE_REF="pre_last_node" ID="ID_1495198531" CREATED="1428500246382" MODIFIED="1428500251219"/>
</node>
<node TEXT="Relational" STYLE_REF="yellow_node" ID="ID_1646949301" CREATED="1428500031291" MODIFIED="1428565249016">
<edge STYLE="bezier" WIDTH="thin"/>
<node TEXT="&lt;" STYLE_REF="pre_last_node" ID="ID_1882963553" CREATED="1428500031292" MODIFIED="1428500415074"/>
<node TEXT="&gt;" STYLE_REF="pre_last_node" ID="ID_1350867925" CREATED="1428500416127" MODIFIED="1428500460091"/>
<node TEXT="&lt;=" STYLE_REF="pre_last_node" ID="ID_237636319" CREATED="1428500421073" MODIFIED="1428500460092"/>
<node TEXT="&gt;=" STYLE_REF="pre_last_node" ID="ID_1110110604" CREATED="1428500427129" MODIFIED="1428500460092"/>
<node TEXT="as" STYLE_REF="pre_last_node" ID="ID_84006618" CREATED="1428500434543" MODIFIED="1428500460093"/>
<node TEXT="in" STYLE_REF="pre_last_node" ID="ID_528873365" CREATED="1428500442678" MODIFIED="1428500460093"/>
<node TEXT="instanceof" STYLE_REF="pre_last_node" ID="ID_530981298" CREATED="1428500447994" MODIFIED="1428500460093"/>
<node TEXT="is" STYLE_REF="pre_last_node" ID="ID_369235641" CREATED="1428500453072" MODIFIED="1428500460093"/>
</node>
<node TEXT="Equality" STYLE_REF="yellow_node" ID="ID_328214285" CREATED="1428500031293" MODIFIED="1428565249016">
<edge STYLE="bezier" WIDTH="thin"/>
<node TEXT="==" STYLE_REF="pre_last_node" ID="ID_847422337" CREATED="1428500031295" MODIFIED="1428500287420"/>
<node TEXT="!=" STYLE_REF="pre_last_node" ID="ID_1660563301" CREATED="1428500269416" MODIFIED="1428500297316"/>
<node TEXT="===" STYLE_REF="pre_last_node" ID="ID_1970513287" CREATED="1428500281094" MODIFIED="1428500297317"/>
<node TEXT="!==" STYLE_REF="pre_last_node" ID="ID_1899771975" CREATED="1428500290993" MODIFIED="1428500297315"/>
</node>
<node TEXT="Bitwise AND" STYLE_REF="yellow_node" ID="ID_1434319617" CREATED="1428500031296" MODIFIED="1428565249017">
<edge STYLE="bezier" WIDTH="thin"/>
<node TEXT="&amp;" STYLE_REF="pre_last_node" ID="ID_497679738" CREATED="1428500031298" MODIFIED="1428500119744"/>
</node>
<node TEXT="Bitwise XOR" STYLE_REF="yellow_node" ID="ID_1421588850" CREATED="1428500031299" MODIFIED="1428565249017">
<edge STYLE="bezier" WIDTH="thin"/>
<node TEXT="^" STYLE_REF="pre_last_node" ID="ID_1803484547" CREATED="1428500031300" MODIFIED="1428500119744"/>
</node>
<node TEXT="Bitwise OR" STYLE_REF="yellow_node" ID="ID_515968286" CREATED="1428500031301" MODIFIED="1428565249017">
<edge STYLE="bezier" WIDTH="thin"/>
<node TEXT="|" STYLE_REF="pre_last_node" ID="ID_253243474" CREATED="1428500031304" MODIFIED="1428500119743"/>
</node>
<node TEXT="Logical AND" STYLE_REF="yellow_node" ID="ID_1361264245" CREATED="1428500031307" MODIFIED="1428565249017">
<edge STYLE="bezier" WIDTH="thin"/>
<node TEXT="&amp;&amp;" STYLE_REF="pre_last_node" ID="ID_1961126619" CREATED="1428500031308" MODIFIED="1428500119742"/>
</node>
<node TEXT="Logical OR" STYLE_REF="yellow_node" ID="ID_78514495" CREATED="1428500031309" MODIFIED="1428565249017">
<edge STYLE="bezier" WIDTH="thin"/>
<node TEXT="||" STYLE_REF="pre_last_node" ID="ID_1485255741" CREATED="1428500031311" MODIFIED="1428500119743"/>
</node>
<node TEXT="Conditional" STYLE_REF="yellow_node" ID="ID_244418117" CREATED="1428500031312" MODIFIED="1428565249018">
<edge STYLE="bezier" WIDTH="thin"/>
<node TEXT="?:" STYLE_REF="pre_last_node" ID="ID_1108459755" CREATED="1428500031312" MODIFIED="1428500119744"/>
</node>
<node TEXT="Assignment" STYLE_REF="yellow_node" ID="ID_443408376" CREATED="1428500031313" MODIFIED="1428565249018">
<edge STYLE="bezier" WIDTH="thin"/>
<node TEXT="=" STYLE_REF="pre_last_node" ID="ID_490645764" CREATED="1428500306158" MODIFIED="1428500396668"/>
<node TEXT="*=" STYLE_REF="pre_last_node" ID="ID_9385479" CREATED="1428500031314" MODIFIED="1428500396669"/>
<node TEXT="/=" STYLE_REF="pre_last_node" ID="ID_220723827" CREATED="1428500315262" MODIFIED="1428500396669"/>
<node TEXT="%=" STYLE_REF="pre_last_node" ID="ID_1519447512" CREATED="1428500324897" MODIFIED="1428500396670"/>
<node TEXT="+=" STYLE_REF="pre_last_node" ID="ID_1714573045" CREATED="1428500345554" MODIFIED="1428500396670"/>
<node TEXT=" -=" STYLE_REF="pre_last_node" ID="ID_667684664" CREATED="1428500351636" MODIFIED="1428500396670"/>
<node TEXT="&lt;&lt;=" STYLE_REF="pre_last_node" ID="ID_1284609412" CREATED="1428500358453" MODIFIED="1428500396670"/>
<node TEXT="&gt;&gt;=" STYLE_REF="pre_last_node" ID="ID_1144061224" CREATED="1428500365525" MODIFIED="1428500396671"/>
<node TEXT="&gt;&gt;&gt;=" STYLE_REF="pre_last_node" ID="ID_458310050" CREATED="1428500374952" MODIFIED="1428500396671"/>
<node TEXT="&amp;=" STYLE_REF="pre_last_node" ID="ID_1747451059" CREATED="1428500381625" MODIFIED="1428500396671"/>
<node TEXT=" ^=" STYLE_REF="pre_last_node" ID="ID_1710426436" CREATED="1428500388158" MODIFIED="1428500396671"/>
<node TEXT="|=" STYLE_REF="pre_last_node" ID="ID_13999655" CREATED="1428500331952" MODIFIED="1428500396671"/>
</node>
<node TEXT="Comma" STYLE_REF="yellow_node" ID="ID_1052513334" CREATED="1428500031315" MODIFIED="1428565249019">
<edge STYLE="bezier" WIDTH="thin"/>
<node TEXT="," STYLE_REF="pre_last_node" ID="ID_1581458837" CREATED="1428500032092" MODIFIED="1428500119741"/>
</node>
</node>
<node TEXT="Equality and comparison operators" STYLE_REF="orange_node" ID="ID_1145725585" CREATED="1428565332477" MODIFIED="1428565653706">
<node TEXT="==" STYLE_REF="yellow_node" ID="ID_1970081852" CREATED="1428565340856" MODIFIED="1428565668433">
<edge STYLE="bezier"/>
<node TEXT="Equality : Tests two expressions for equality. The&#xa;result is true if the expressions are equal" STYLE_REF="pre_last_node" ID="ID_1145353935" CREATED="1428565353880" MODIFIED="1428565682042"/>
<node TEXT="var a:uint = 13;&#xa;var b:uint = 13;&#xa;trace(a==b); // true&#xa;var c:String = &quot;Hello&quot;;&#xa;var d:String = &quot;World&quot;;&#xa;trace(c==d); // false" STYLE_REF="example_node" ID="ID_879405477" CREATED="1428565374180" MODIFIED="1428565693657"/>
</node>
<node TEXT="!=" STYLE_REF="yellow_node" ID="ID_70276559" CREATED="1428565386936" MODIFIED="1428565668435">
<edge STYLE="bezier"/>
<node TEXT="Inequality : Tests for the exact opposite of the equality ( == ) operator. If expression1 is equal to expression2 , the result is false" STYLE_REF="pre_last_node" ID="ID_406563016" CREATED="1428565397217" MODIFIED="1428565682044"/>
<node TEXT="var a:uint = 13;&#xa;var b:uint = 13;&#xa;trace(a!=b); // false&#xa;var c:String = &quot;Hello&quot;;&#xa;var d:String = &quot;World&quot;;&#xa;trace(c==d); // true" STYLE_REF="example_node" ID="ID_844168756" CREATED="1428565421591" MODIFIED="1428565693656"/>
</node>
<node TEXT="===" STYLE_REF="yellow_node" ID="ID_857455788" CREATED="1428565469704" MODIFIED="1428565668437">
<edge STYLE="bezier"/>
<node TEXT="Strict equality : Tests two expressions for equality, but does not perform automatic data conversion. The result is true if both expressions, including their data types, are equal" STYLE_REF="pre_last_node" ID="ID_1715831998" CREATED="1428565481512" MODIFIED="1428565682045"/>
<node TEXT="var a:uint = 13;&#xa;var b:Number = 13;&#xa;trace(a===b); // false&#xa;var c:uint = 22;&#xa;var d:uint = 22;&#xa;trace(c===d); // true" STYLE_REF="example_node" ID="ID_1366990532" CREATED="1428565490959" MODIFIED="1428565693655"/>
</node>
<node TEXT="!==" STYLE_REF="yellow_node" ID="ID_1141551505" CREATED="1428565508710" MODIFIED="1428565668439">
<edge STYLE="bezier"/>
<node TEXT="Strict inequality : Tests for the exact opposite of the strict equality ( === ) operator. If expression1 is equal to expression2 , and their data types are equal, the result is false" STYLE_REF="pre_last_node" ID="ID_617807232" CREATED="1428565519744" MODIFIED="1428565682048"/>
<node TEXT="var a:uint = 13;&#xa;var b:Number = 13;&#xa;trace(a!==b); // true&#xa;var c:uint = 22;&#xa;var d:uint = 22;&#xa;trace(c!==d); // false" STYLE_REF="example_node" ID="ID_1741302089" CREATED="1428565526528" MODIFIED="1428565693654"/>
</node>
<node TEXT="&lt;" STYLE_REF="yellow_node" ID="ID_1114487738" CREATED="1428565553046" MODIFIED="1428565668440">
<edge STYLE="bezier"/>
<node TEXT="Less than : Compares two expressions and determines whether expression1 is less than expression2 ; if it is, the result is true" STYLE_REF="pre_last_node" ID="ID_1164845320" CREATED="1428565554485" MODIFIED="1428565682050"/>
<node TEXT="var x:int = 27;&#xa;var y:int = 42;&#xa;var z:int = 101;&#xa;trace(x&lt;y); // true&#xa;trace(z&lt;y); // false" STYLE_REF="example_node" ID="ID_1790450676" CREATED="1428565569432" MODIFIED="1428565693653"/>
</node>
<node TEXT="&gt;" STYLE_REF="yellow_node" ID="ID_1455023823" CREATED="1428565578000" MODIFIED="1428565668442">
<edge STYLE="bezier"/>
<node TEXT="Greater than : Compares two expressions and determines whether expression1 is greater than expression2 ; if it is, the result is true" STYLE_REF="pre_last_node" ID="ID_172865709" CREATED="1428565584584" MODIFIED="1428565682051"/>
<node TEXT="var x:int = 27;&#xa;var y:int = 42;&#xa;var z:int = 101;&#xa;trace(x&gt;y); // false&#xa;trace(z&gt;y); // true" STYLE_REF="example_node" ID="ID_153746011" CREATED="1428565596519" MODIFIED="1428565693652"/>
</node>
<node TEXT="&lt;=" STYLE_REF="yellow_node" ID="ID_1702435844" CREATED="1428565605200" MODIFIED="1428565668444">
<edge STYLE="bezier"/>
<node TEXT="Less than or equal to : Compares two expressions and determines whether expression1 is less than or equal to expression2 ; if it is, the result is true" STYLE_REF="pre_last_node" ID="ID_868482020" CREATED="1428565611872" MODIFIED="1428565682053"/>
<node TEXT="var x:int = 27;&#xa;var y:int = 42;&#xa;var z:int = 42;&#xa;trace(x&lt;=y); // true&#xa;trace(y&lt;=z); // true" STYLE_REF="example_node" ID="ID_1341732642" CREATED="1428565621563" MODIFIED="1428565693650"/>
</node>
<node TEXT="&gt;=" STYLE_REF="yellow_node" ID="ID_308174615" CREATED="1428565628031" MODIFIED="1428565712713">
<edge STYLE="bezier"/>
<node TEXT="Greater than or equal to : Compares two expressions and determines whether expression1 is greater than or equal to expression2 ; if it is, the result is true" STYLE_REF="pre_last_node" ID="ID_584683742" CREATED="1428565633726" MODIFIED="1428565682054"/>
<node TEXT="var x:int = 27;&#xa;var y:int = 42;&#xa;var z:int = 42;&#xa;trace(x&gt;=y); // false&#xa;trace(y&gt;=z); // true" STYLE_REF="example_node" ID="ID_1349051261" CREATED="1428565645463" MODIFIED="1428565712713"/>
</node>
</node>
</node>
<node TEXT="Data types" STYLE_REF="second_node" POSITION="left" ID="ID_1473494454" CREATED="1425136253050" MODIFIED="1428566419931" VGAP="22">
<edge COLOR="#890c89"/>
<hook URI="../../Images/type-2.jpg" SIZE="1.0" NAME="ExternalObject"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      A data type defines a set of values
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Primitive data types" STYLE_REF="orange_node" ID="ID_340906709" CREATED="1425136354947" MODIFIED="1425219903522">
<hook URI="../../Images/primitive.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Boolean" STYLE_REF="gray_node" ID="ID_1870956503" CREATED="1425136383090" MODIFIED="1425209115803">
<node TEXT="Values" STYLE_REF="yellow_node" ID="ID_1585489715" CREATED="1425209095772" MODIFIED="1425209127348">
<node TEXT="true" STYLE_REF="pre_last_node" ID="ID_1894006709" CREATED="1425209105197" MODIFIED="1425209134099"/>
<node TEXT="false" STYLE_REF="pre_last_node" ID="ID_113465287" CREATED="1425209107277" MODIFIED="1425209134100"/>
</node>
<node TEXT="Default value" STYLE_REF="yellow_node" ID="ID_420739446" CREATED="1425209098726" MODIFIED="1425209127350">
<node TEXT="false" STYLE_REF="pre_last_node" ID="ID_736468159" CREATED="1425209102661" MODIFIED="1425209134100"/>
</node>
</node>
<node TEXT="int" STYLE_REF="gray_node" ID="ID_540291242" CREATED="1425136389052" MODIFIED="1425209115802">
<node TEXT="Values" STYLE_REF="yellow_node" ID="ID_378490728" CREATED="1425209186525" MODIFIED="1425209274157">
<node TEXT="-2,147,483,648 ...  2,147,483,647" STYLE_REF="pre_last_node" ID="ID_626556774" CREATED="1425209189260" MODIFIED="1425209239895"/>
<node TEXT="-2^31 ... 2^31 - 1" STYLE_REF="pre_last_node" ID="ID_1043357548" CREATED="1425209198844" MODIFIED="1425209239894"/>
<node TEXT="32-bit" STYLE_REF="pre_last_node" ID="ID_1822056678" CREATED="1425209232757" MODIFIED="1425209239892"/>
</node>
<node TEXT="Default value" STYLE_REF="yellow_node" ID="ID_404204991" CREATED="1425209098726" MODIFIED="1425209127350">
<node TEXT="0" OBJECT="java.lang.Long|0" STYLE_REF="pre_last_node" ID="ID_99413201" CREATED="1425209102661" MODIFIED="1425209269966"/>
</node>
<node TEXT="Tips" STYLE_REF="yellow_node" ID="ID_1024362557" CREATED="1425209343677" MODIFIED="1425209352065">
<node TEXT="Using int instead of the Number data type should be faster and more efficient" STYLE_REF="pre_last_node" ID="ID_1170652264" CREATED="1425209293373" MODIFIED="1425209356284"/>
</node>
</node>
<node TEXT="uint" STYLE_REF="gray_node" ID="ID_756050756" CREATED="1425136399498" MODIFIED="1425209115801">
<node TEXT="Values" STYLE_REF="yellow_node" ID="ID_1086636466" CREATED="1425209571094" MODIFIED="1425209660979">
<node TEXT="0 ... 4 294 967 295" STYLE_REF="pre_last_node" ID="ID_1548057205" CREATED="1425209574061" MODIFIED="1425209667131"/>
<node TEXT="0 ... 2^32 - 1" STYLE_REF="pre_last_node" ID="ID_1051529701" CREATED="1425209587813" MODIFIED="1425209667131"/>
</node>
<node TEXT="Default value" STYLE_REF="yellow_node" ID="ID_1653232563" CREATED="1425209098726" MODIFIED="1436172149021">
<node TEXT="0" OBJECT="java.lang.Long|0" STYLE_REF="pre_last_node" ID="ID_709131388" CREATED="1425209102661" MODIFIED="1436172149021"/>
</node>
<node TEXT="Tips" STYLE_REF="yellow_node" ID="ID_1188138501" CREATED="1425209608535" MODIFIED="1425209660978">
<node TEXT="You must use the uint data type to represent pixel color values" STYLE_REF="pre_last_node" ID="ID_1734877001" CREATED="1425209624270" MODIFIED="1425209667130"/>
</node>
</node>
<node TEXT="Number" STYLE_REF="gray_node" ID="ID_655841305" CREATED="1425136392339" MODIFIED="1425211584219">
<node TEXT="Values" STYLE_REF="yellow_node" ID="ID_963805805" CREATED="1425210090158" MODIFIED="1425211388696">
<node TEXT="4.940656458412467e-324 ...  1.79769313486231e+308" STYLE_REF="pre_last_node" ID="ID_188671774" CREATED="1425211092213" MODIFIED="1425211466842"/>
<node TEXT="64-bit double-precision" STYLE_REF="pre_last_node" ID="ID_1354403129" CREATED="1425210092413" MODIFIED="1425211466843">
<node TEXT="1 bit for sign" ID="ID_350126921" CREATED="1425211252006" MODIFIED="1425211268034"/>
<node TEXT="11 bits are used for the exponent, which is stored as base 2" ID="ID_683393577" CREATED="1425211239270" MODIFIED="1425211249690"/>
<node TEXT="52 bits for storing the significand" ID="ID_370787895" CREATED="1425211193870" MODIFIED="1425211205459"/>
</node>
<node TEXT="Number.MIN_VALUE" STYLE_REF="pre_last_node" ID="ID_12775362" CREATED="1425211124358" MODIFIED="1425211466843"/>
<node TEXT="Number.MAX_VALUE" STYLE_REF="pre_last_node" ID="ID_1452737860" CREATED="1425211131942" MODIFIED="1425211466844"/>
<node TEXT="For integer" STYLE_REF="pre_last_node" ID="ID_967751838" CREATED="1425211396414" MODIFIED="1425211466844">
<node TEXT="-9 007 199 254 740 992 ...  9 007 199 254 740 992" ID="ID_1294634962" CREATED="1425211409445" MODIFIED="1425211455539"/>
<node TEXT="-2^53 ... 2^53" ID="ID_820144432" CREATED="1425211414766" MODIFIED="1425211423059"/>
</node>
</node>
<node TEXT="Default value" STYLE_REF="yellow_node" ID="ID_123313838" CREATED="1425211344198" MODIFIED="1436764432458">
<node TEXT="NaN" STYLE_REF="pre_last_node" ID="ID_244325049" CREATED="1425211351374" MODIFIED="1437060349626">
<node TEXT="Use the NaN also as the result of any operation&#xa;that should return a number but does not" ID="ID_843564057" CREATED="1425211548590" MODIFIED="1437060349625"/>
</node>
</node>
<node TEXT="Tips" STYLE_REF="yellow_node" ID="ID_24794652" CREATED="1425211336662" MODIFIED="1436172141485">
<node TEXT="When you store integer values with the Number data type,&#xa;only the 52 bits of the significand are used." STYLE_REF="pre_last_node" ID="ID_831619488" CREATED="1425211339195" MODIFIED="1436172141485"/>
</node>
</node>
<node TEXT="String" STYLE_REF="gray_node" ID="ID_941932297" CREATED="1425136395938" MODIFIED="1437060286497">
<node TEXT="Values" STYLE_REF="yellow_node" ID="ID_1016224615" CREATED="1425209819997" MODIFIED="1425209912413">
<node TEXT="Sequence of 16-bit characters" STYLE_REF="pre_last_node" ID="ID_526999840" CREATED="1425209834350" MODIFIED="1425209921620"/>
</node>
<node TEXT="Default value" STYLE_REF="yellow_node" ID="ID_481492317" CREATED="1425209866757" MODIFIED="1437060286496">
<node TEXT="null" STYLE_REF="pre_last_node" ID="ID_363021404" CREATED="1425209870894" MODIFIED="1425209921620">
<node TEXT="Not the same as empty string &quot;&quot;" ID="ID_1506774693" CREATED="1425209875013" MODIFIED="1425209889169"/>
</node>
</node>
<node TEXT="Tips" STYLE_REF="yellow_node" ID="ID_753917147" CREATED="1425209902493" MODIFIED="1425209912412">
<node TEXT="An operation on a String value returns a new instance of the string" STYLE_REF="pre_last_node" ID="ID_1210942114" CREATED="1425209903990" MODIFIED="1425209921619"/>
<node TEXT="Strings are stored internally as Unicode characters, using the UTF-16 format" STYLE_REF="pre_last_node" ID="ID_659044779" CREATED="1425210020461" MODIFIED="1425210028834"/>
</node>
</node>
<node TEXT="void" STYLE_REF="gray_node" ID="ID_1260955196" CREATED="1425209044773" MODIFIED="1425209115798">
<node TEXT="Values" STYLE_REF="yellow_node" ID="ID_343216376" CREATED="1425209681221" MODIFIED="1425209754159">
<node TEXT="undefined" STYLE_REF="pre_last_node" ID="ID_966084340" CREATED="1425209683477" MODIFIED="1425209762380"/>
</node>
<node TEXT="Tips" STYLE_REF="yellow_node" ID="ID_65273218" CREATED="1425209730989" MODIFIED="1425209754156">
<node TEXT="You can use void only as a return type annotation" STYLE_REF="pre_last_node" ID="ID_1961806558" CREATED="1425209734287" MODIFIED="1425211976779"/>
<node TEXT="If you attempt to assign the value undefined to an&#xa;instance of the Object class, run-time will convert the value to null" STYLE_REF="pre_last_node" ID="ID_1697342640" CREATED="1425209777157" MODIFIED="1425209800908"/>
<node TEXT="Only untyped variables can hold the value undefined" STYLE_REF="pre_last_node" ID="ID_1880967893" CREATED="1425211768310" MODIFIED="1425211831104"/>
</node>
<node TEXT="Untyped declaration" STYLE_REF="yellow_node" ID="ID_350461985" CREATED="1425211788318" MODIFIED="1425211824895">
<node TEXT="var x" STYLE_REF="pre_last_node" ID="ID_737871529" CREATED="1425211794859" MODIFIED="1425211831103"/>
<node TEXT="var x:*" STYLE_REF="pre_last_node" ID="ID_1615623942" CREATED="1425211794859" MODIFIED="1425211831101"/>
</node>
</node>
<node TEXT="Null" STYLE_REF="gray_node" ID="ID_1948640821" CREATED="1425209053893" MODIFIED="1425209115795">
<node TEXT="Values" STYLE_REF="yellow_node" ID="ID_922185949" CREATED="1425209398989" MODIFIED="1425209519658">
<node TEXT="null" STYLE_REF="pre_last_node" ID="ID_1160880233" CREATED="1425209401781" MODIFIED="1425209527544"/>
</node>
<node TEXT="Tips" STYLE_REF="yellow_node" ID="ID_1042451714" CREATED="1425209420661" MODIFIED="1425209519657">
<node TEXT="This is the default value for the String data type and&#xa;all classes that define complex data types" STYLE_REF="pre_last_node" ID="ID_619258529" CREATED="1425209424685" MODIFIED="1425209527540"/>
<node TEXT="None of the primitive data types, like Boolean, Number,&#xa;int and uint, contain the value null" STYLE_REF="pre_last_node" ID="ID_1759964474" CREATED="1425209446301" MODIFIED="1425209527537"/>
<node TEXT="Run-time will convert the value null to the appropriate&#xa;default value if you attempt to assign null to variables of primitive type" STYLE_REF="pre_last_node" ID="ID_287049558" CREATED="1425209484845" MODIFIED="1425209527532"/>
</node>
</node>
</node>
<node TEXT="Complex data types" STYLE_REF="orange_node" ID="ID_1329212599" CREATED="1425136369957" MODIFIED="1425220002459">
<hook URI="../../Images/complex.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Object" STYLE_REF="gray_node" ID="ID_444565073" CREATED="1425211850438" MODIFIED="1425211861332">
<node TEXT="Default value" STYLE_REF="yellow_node" ID="ID_1094179989" CREATED="1425211863766" MODIFIED="1425211875154">
<node TEXT="null" STYLE_REF="pre_last_node" ID="ID_1195555127" CREATED="1425211869054" MODIFIED="1425211878299"/>
</node>
<node TEXT="Tips" STYLE_REF="yellow_node" ID="ID_1221961698" CREATED="1425211902902" MODIFIED="1425211919957">
<node TEXT="The Object class serves as the base class for&#xa;all class definitions in ActionScript" STYLE_REF="pre_last_node" ID="ID_1945859341" CREATED="1425211942575" MODIFIED="1425211954909"/>
<node TEXT="The Object data type is no longer the default data type&#xa;assigned to variables with no type annotation" STYLE_REF="pre_last_node" ID="ID_537381767" CREATED="1425211904663" MODIFIED="1425211923917"/>
</node>
</node>
<node TEXT="Array" STYLE_REF="gray_node" ID="ID_1429400901" CREATED="1425136417538" MODIFIED="1425211598965"/>
<node TEXT="Date" STYLE_REF="gray_node" ID="ID_178470531" CREATED="1425136428978" MODIFIED="1425211598968"/>
<node TEXT="Error" STYLE_REF="gray_node" ID="ID_237908605" CREATED="1425136435955" MODIFIED="1425211598969"/>
<node TEXT="Function" STYLE_REF="gray_node" ID="ID_1763891677" CREATED="1425136442620" MODIFIED="1425211598970"/>
<node TEXT="RegExp" STYLE_REF="gray_node" ID="ID_850151467" CREATED="1425136451619" MODIFIED="1425211598971"/>
<node TEXT="XML" STYLE_REF="gray_node" ID="ID_866222369" CREATED="1425136458136" MODIFIED="1425211598972"/>
<node TEXT="XMLList" STYLE_REF="gray_node" ID="ID_915180577" CREATED="1425136464499" MODIFIED="1425211598973"/>
</node>
<node TEXT="Wrappers" STYLE_REF="orange_node" ID="ID_236541680" CREATED="1425136671571" MODIFIED="1428566419931">
<hook URI="../../Images/wrapper.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Primitive values and their wrapper objects are indistinguishable" STYLE_REF="pre_last_node" ID="ID_1294812928" CREATED="1425136606691" MODIFIED="1425206461328"/>
<node TEXT="All values, even primitive values, are objects" STYLE_REF="pre_last_node" ID="ID_31050690" CREATED="1425136633331" MODIFIED="1425206461324"/>
<node TEXT="The same" STYLE_REF="pre_last_node" ID="ID_546199189" CREATED="1425149500913" MODIFIED="1425206461329">
<node TEXT="var someInt:int = 3;" ID="ID_1539610083" CREATED="1425149509786" MODIFIED="1425149509786"/>
<node TEXT="var someInt:int = new int(3);" ID="ID_1173664589" CREATED="1425149509786" MODIFIED="1425149509786"/>
</node>
</node>
<node TEXT=" Type checking" STYLE_REF="orange_node" ID="ID_1408630825" CREATED="1425149558056" MODIFIED="1425219751210">
<hook URI="../../Images/checking.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="AS3 dynamically typed language" STYLE_REF="yellow_node" ID="ID_1519843606" CREATED="1425205523148" MODIFIED="1425205653545"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      It has run-time type checking
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Supports compile-time type checking" STYLE_REF="yellow_node" ID="ID_1660314430" CREATED="1425205563211" MODIFIED="1425205653544">
<node TEXT="With a special compiler mode called strict mode" STYLE_REF="pre_last_node" ID="ID_265884073" CREATED="1425205606260" MODIFIED="1425205669912"/>
<node TEXT="In strict mode, type checking occurs at both" STYLE_REF="pre_last_node" ID="ID_1379170904" CREATED="1425205622059" MODIFIED="1425205669908">
<node TEXT="compile time" STYLE_REF="gray_node" ID="ID_7119346" CREATED="1425205634475" MODIFIED="1428566099900">
<node TEXT="var xParam:Object;&#xa;var myStr:String = xParam; // compiler error in strict mode" STYLE_REF="example_node" ID="ID_329060880" CREATED="1425206007651" MODIFIED="1428566099899"/>
<node TEXT="var xParam:*;&#xa;var myStr:String = xParam; // ok" STYLE_REF="example_node" ID="ID_691172141" CREATED="1425206035733" MODIFIED="1428566098643"/>
</node>
<node TEXT="run time" STYLE_REF="gray_node" ID="ID_1900596245" CREATED="1425205641019" MODIFIED="1425206551377">
<node TEXT="var xParam = 3;&#xa;var arr:Array = xParam; // run-time error" STYLE_REF="example_node" ID="ID_1250979368" CREATED="1425206254444" MODIFIED="1425207277249"/>
<node TEXT="var myClass:ClassBase = new ClassExtender();&#xa;myClass.someString = &quot;hello&quot;;&#xa;// no error in ActionScript 3.0 standard mode" STYLE_REF="example_node" ID="ID_922922942" CREATED="1425206425772" MODIFIED="1425207277253">
<node TEXT="An upcast is considered a safe operation" ID="ID_1664962522" CREATED="1425206637668" MODIFIED="1425206639679"/>
</node>
</node>
</node>
</node>
<node TEXT="Operators" STYLE_REF="yellow_node" ID="ID_1674531504" CREATED="1425206718980" MODIFIED="1425206731057">
<node TEXT="is" STYLE_REF="gray_node" ID="ID_758359007" CREATED="1425206722187" MODIFIED="1425206743467">
<node TEXT="For manual type checking" STYLE_REF="pre_last_node" ID="ID_612197599" CREATED="1425206805508" MODIFIED="1425206921801"/>
<node TEXT="Should be used instead of" STYLE_REF="pre_last_node" ID="ID_963633104" CREATED="1425206763788" MODIFIED="1425206921802">
<node TEXT="instanceof" STYLE_REF="gray_node" ID="ID_1994199013" CREATED="1425206821484" MODIFIED="1425206930314">
<node TEXT="Because the expression: x instanceof y" STYLE_REF="pre_last_node" ID="ID_1385357299" CREATED="1425206834619" MODIFIED="1425206935681">
<node TEXT="Checks the prototype chain of x for the existence of y" ID="ID_1381760833" CREATED="1425206883196" MODIFIED="1425206885881"/>
<node TEXT="in AS3, the prototype chain does not provide a&#xa;complete picture of the inheritance hierarchy" ID="ID_221986457" CREATED="1425206896835" MODIFIED="1425206908776"/>
</node>
<node TEXT="trace(mySprite instanceof Sprite); // true&#xa;trace(mySprite instanceof DisplayObject);// true&#xa;trace(mySprite instanceof IEventDispatcher); // false" STYLE_REF="example_node" ID="ID_1452058737" CREATED="1425207153380" MODIFIED="1425207266915"/>
</node>
</node>
<node TEXT="Examines that object" STYLE_REF="pre_last_node" ID="ID_1835778610" CREATED="1425207006188" MODIFIED="1425207169472">
<node TEXT="Is an instance of a particular class" ID="ID_307072923" CREATED="1425207047524" MODIFIED="1425207051929"/>
<node TEXT="Is an instance of a class that implements a particular interface" ID="ID_1447476280" CREATED="1425207061316" MODIFIED="1425207063848"/>
<node TEXT="var mySprite:Sprite = new Sprite();&#xa;trace(mySprite is Sprite); // true&#xa;trace(mySprite is DisplayObject);// true&#xa;trace(mySprite is IEventDispatcher); // true" ID="ID_792524171" CREATED="1425207065932" MODIFIED="1425207244584" BACKGROUND_COLOR="#afcdfa"/>
</node>
</node>
<node TEXT="as" STYLE_REF="gray_node" ID="ID_1176296450" CREATED="1425206733611" MODIFIED="1425206743465">
<node TEXT="Check whether an expression is a member of a given data type" STYLE_REF="pre_last_node" ID="ID_1151161079" CREATED="1425207459436" MODIFIED="1425208032397"/>
<node TEXT="Returns" STYLE_REF="pre_last_node" ID="ID_910817394" CREATED="1425207472436" MODIFIED="1425208024060">
<node TEXT="The value of the expression" ID="ID_621139906" CREATED="1425207487660" MODIFIED="1425207514400">
<node TEXT="When an expression is a member of a given data type" ID="ID_24149275" CREATED="1425207515180" MODIFIED="1425207987552"/>
</node>
<node TEXT="null" ID="ID_1855485031" CREATED="1425207495140" MODIFIED="1425207496792">
<node TEXT="Otherwise" ID="ID_101269246" CREATED="1425207991925" MODIFIED="1425207996528"/>
</node>
</node>
<node TEXT="var mySprite:Sprite = new Sprite();&#xa;trace(mySprite as Sprite);                      // [object Sprite]&#xa;trace(mySprite as DisplayObject);        // [object Sprite]&#xa;trace(mySprite as IEventDispatcher);  // [object Sprite]&#xa;trace(mySprite as Number);                 // null" STYLE_REF="example_node" ID="ID_135794390" CREATED="1425207538028" MODIFIED="1425208054064">
<edge WIDTH="1"/>
</node>
</node>
</node>
</node>
<node TEXT="Dynamic class" STYLE_REF="orange_node" ID="ID_1847374706" CREATED="1425208332060" MODIFIED="1425220057334">
<hook URI="../../Images/dynamic.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="A dynamic class defines an object that can be altered&#xa;at run time by adding or changing properties and methods" STYLE_REF="pre_last_node" ID="ID_1448148434" CREATED="1425208452413" MODIFIED="1425208470067"/>
<node TEXT="To create dynamic classes use the dynamic attribute" STYLE_REF="pre_last_node" ID="ID_1233326574" CREATED="1425208386885" MODIFIED="1425208696618">
<node TEXT="dynamic class Protean {&#xa;    private var privateGreeting:String = &quot;hi&quot;;&#xa;    public var publicGreeting:String = &quot;hello&quot;;&#xa;    function Protean() {&#xa;        trace(&quot;Protean instance created&quot;);&#xa;    }&#xa;}" STYLE_REF="example_node" ID="ID_1768441635" CREATED="1425208416684" MODIFIED="1425208567158"/>
</node>
<node TEXT="You can add properties" STYLE_REF="pre_last_node" ID="ID_1225946161" CREATED="1425208667940" MODIFIED="1425208696618">
<node TEXT="var myProtean:Protean = new Protean();&#xa;myProtean.aString = &quot;testing&quot;;&#xa;myProtean.aNumber = 3;&#xa;trace(myProtean.aString, myProtean.aNumber); // testing 3" STYLE_REF="example_node" ID="ID_1689461255" CREATED="1425208568141" MODIFIED="1425208580465"/>
</node>
<node TEXT="You can also add a method" STYLE_REF="pre_last_node" ID="ID_1529643664" CREATED="1425208662292" MODIFIED="1425208696617">
<node TEXT="myProtean.traceProtean = function () {&#xa;    trace(this.aString, this.aNumber);&#xa;};&#xa;myProtean.traceProtean(); // testing 3" STYLE_REF="example_node" ID="ID_209190406" CREATED="1425208558965" MODIFIED="1425208580470"/>
<node TEXT="Methods created in this way, do not have access to&#xa;any private properties or methods of the Protean class" ID="ID_343680029" CREATED="1425208724165" MODIFIED="1425208733385">
<node TEXT="myProtean.traceProtean = function () {&#xa;    trace(myProtean.privateGreeting); // undefined&#xa;    trace(myProtean.publicGreeting); // hello&#xa;};&#xa;myProtean.traceProtean();" STYLE_REF="example_node" ID="ID_1283804243" CREATED="1425208747716" MODIFIED="1425208754827"/>
</node>
</node>
<node TEXT="A class that is not dynamic is named" STYLE_REF="pre_last_node" ID="ID_1599739537" CREATED="1425208355605" MODIFIED="1425208788508">
<node TEXT="Sealed class" STYLE_REF="gray_node" ID="ID_1092596462" CREATED="1425208367093" MODIFIED="1425208805515">
<node TEXT="String for example" ID="ID_765642384" CREATED="1425208794645" MODIFIED="1425208800098"/>
</node>
</node>
</node>
<node TEXT="Type conversions" STYLE_REF="orange_node" ID="ID_955815580" CREATED="1425212350406" MODIFIED="1425219813981">
<hook URI="../../Images/conversion.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="A type conversion occurs when a value is transformed&#xa;into a value of a different data type" STYLE_REF="pre_last_node" ID="ID_1824474297" CREATED="1425212356623" MODIFIED="1425212967939" VSHIFT="-10"/>
<node TEXT="Implicit" STYLE_REF="gray_node" ID="ID_570065567" CREATED="1425212452726" MODIFIED="1425212960764"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Coercion
    </p>
  </body>
</html>
</richcontent>
<node TEXT="When?" STYLE_REF="yellow_node" ID="ID_1063918770" CREATED="1425212491360" MODIFIED="1425212960763" VSHIFT="-19">
<node TEXT="In assignment statements" STYLE_REF="pre_last_node" ID="ID_1095673645" CREATED="1425212575454" MODIFIED="1425212612217"/>
<node TEXT="When values are passed as function arguments" STYLE_REF="pre_last_node" ID="ID_4180625" CREATED="1425212581063" MODIFIED="1425212612216"/>
<node TEXT="When values are returned from functions" STYLE_REF="pre_last_node" ID="ID_575500989" CREATED="1425212593679" MODIFIED="1425212612215"/>
<node TEXT="In expressions using certain operators, such as the addition ( + ) operator" STYLE_REF="pre_last_node" ID="ID_1181181065" CREATED="1425212604318" MODIFIED="1425212612213"/>
</node>
<node TEXT="Errors" STYLE_REF="yellow_node" ID="ID_1234079555" CREATED="1425212649527" MODIFIED="1425212948756">
<node TEXT="If an implicit conversion is unsuccessful, an error occurs" STYLE_REF="pre_last_node" ID="ID_1533563016" CREATED="1425212652982" MODIFIED="1425212686971"/>
<node TEXT="class A {}&#xa;class B extends A {}&#xa; &#xa;var objA:A = new A();&#xa;var objB:B = new B();&#xa;var arr:Array = new Array();&#xa; &#xa;objA = objB;// Conversion succeeds&#xa;objB = arr; // Conversion fails" STYLE_REF="example_node" ID="ID_1205283348" CREATED="1425212679271" MODIFIED="1425212738566">
<edge WIDTH="thin"/>
</node>
</node>
</node>
<node TEXT="Explicit" STYLE_REF="gray_node" ID="ID_1794603139" CREATED="1425212464766" MODIFIED="1426233648609"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Casting
    </p>
  </body>
</html>
</richcontent>
<node TEXT="(SomeObj as Type);" STYLE_REF="yellow_node" ID="ID_544575818" CREATED="1425206733611" MODIFIED="1426233914013">
<font BOLD="true"/>
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="80" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_1176296450" STARTINCLINATION="768;0;" ENDINCLINATION="768;0;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="80" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_263962219" MIDDLE_LABEL="as keyword is about 4x-4.5x faster than the function call approach" STARTINCLINATION="137;0;" ENDINCLINATION="137;0;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
<node TEXT="Returns" STYLE_REF="pre_last_node" ID="ID_1275668098" CREATED="1425207472436" MODIFIED="1425208024060">
<node TEXT="Needed type" ID="ID_748515801" CREATED="1425207487660" MODIFIED="1426233845052"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      When an expression is a
    </p>
    <p>
      member of a given data type
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="null" ID="ID_326330380" CREATED="1425207495140" MODIFIED="1426233955291"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Otherwise
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true"/>
</node>
</node>
<node TEXT="var mySprite:Sprite = new Sprite();&#xa;trace(mySprite as Sprite);                      // [object Sprite]&#xa;trace(mySprite as DisplayObject);        // [object Sprite]&#xa;trace(mySprite as IEventDispatcher);  // [object Sprite]&#xa;trace(mySprite as Number);                 // null" STYLE_REF="example_node" ID="ID_1070861541" CREATED="1425207538028" MODIFIED="1425208054064">
<edge WIDTH="1"/>
</node>
</node>
<node TEXT="Type(SomeObj);" STYLE_REF="yellow_node" ID="ID_263962219" CREATED="1426076568723" MODIFIED="1426233886192" VSHIFT="69">
<font BOLD="true"/>
<node TEXT="var quantityField:String = &quot;3&quot;;&#xa;var quantity:int = int(quantityField); // Explicit conversion succeeds" STYLE_REF="example_node" ID="ID_1561129840" CREATED="1425212796919" MODIFIED="1425212940644"/>
<node TEXT="Returns" STYLE_REF="pre_last_node" ID="ID_655918082" CREATED="1426233536586" MODIFIED="1426233931841">
<node TEXT="Needed type" ID="ID_982331552" CREATED="1426233795633" MODIFIED="1426233849613"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      When an expression is a
    </p>
    <p>
      member of a given data type
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="TypeError is thrown" ID="ID_64636800" CREATED="1426233540933" MODIFIED="1426233957536"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Otherwise
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true"/>
</node>
</node>
</node>
</node>
<node TEXT="Casting to" STYLE_REF="gray_node" ID="ID_1574517823" CREATED="1425213000679" MODIFIED="1425220117027">
<hook URI="../../Images/casting.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="int, uint, and Number" STYLE_REF="yellow_node" ID="ID_1788058018" CREATED="1425212990574" MODIFIED="1425218000944">
<font BOLD="true"/>
<node TEXT="From" STYLE_REF="yellow_node" ID="ID_1044099076" CREATED="1425218227625" MODIFIED="1425218247296">
<node TEXT="Boolean" STYLE_REF="gray_node" ID="ID_1328594187" CREATED="1425213026735" MODIFIED="1425218236173">
<node TEXT="1 If the value is true" STYLE_REF="pre_last_node" ID="ID_1192160835" CREATED="1425213035438" MODIFIED="1425219267174"/>
<node TEXT="0 otherwise" STYLE_REF="pre_last_node" ID="ID_979869877" CREATED="1425213056158" MODIFIED="1425219267168"/>
<node TEXT="var myBoolean:Boolean = true;&#xa;var myUINT:uint = uint(myBoolean);&#xa;var myINT:int = int(myBoolean);&#xa;trace(myUINT, myINT); // 1 1 1&#xa;&#xa;myBoolean = false;&#xa;myINT = int(myBoolean);&#xa;myNum = Number(myBoolean);&#xa;trace(myINT, myNum); // 0 0 0" STYLE_REF="example_node" ID="ID_835496768" CREATED="1425213091446" MODIFIED="1425213133907"/>
</node>
<node TEXT="String" STYLE_REF="gray_node" ID="ID_1722855172" CREATED="1425213173383" MODIFIED="1425213294139">
<node TEXT="String values that contain only digits can be&#xa;successfully converted into one of the number types" STYLE_REF="pre_last_node" ID="ID_884039958" CREATED="1425213188143" MODIFIED="1425213300291">
<node TEXT="trace(uint(&quot;5&quot;)); // 5&#xa;trace(uint(&quot;-5&quot;)); // 4294967291 == MAX_VALUE&#xa;trace(uint(&quot; 27 &quot;)); // 27&#xa;trace(uint(&quot;3.7&quot;)); // 3&#xa;trace(int(&quot;3.7&quot;)); // 3&#xa;trace(int(&quot;0x1A&quot;)); // 26&#xa;trace(Number(&quot;3.7&quot;)); // 3.7" STYLE_REF="example_node" ID="ID_593110164" CREATED="1425213268319" MODIFIED="1425213318380"/>
</node>
<node TEXT="0 when sring contains non-numeric characters" STYLE_REF="pre_last_node" ID="ID_984767230" CREATED="1425213248623" MODIFIED="1425219329726">
<node TEXT="trace(uint(&quot;5a&quot;)); // 0&#xa;trace(uint(&quot;ten&quot;)); // 0&#xa;trace(uint(&quot;17 63&quot;)); // 0" STYLE_REF="example_node" ID="ID_1811383910" CREATED="1425213286334" MODIFIED="1425213308031"/>
</node>
</node>
<node TEXT="Number, uint, int" STYLE_REF="gray_node" ID="ID_937791004" CREATED="1425213396119" MODIFIED="1425219346800">
<node TEXT="Casting is not necessary when a value of one numeric type&#xa;is assigned to a variable of a different numeric type" STYLE_REF="pre_last_node" ID="ID_985636393" CREATED="1425213407551" MODIFIED="1425219346799">
<node TEXT="var myUInt:uint = -3;&#xa;trace(myUInt); // 4294967293&#xa;var myNum:Number = sampleUINT;&#xa;trace(myNum) // 4294967293&#xa; &#xa;var myInt:int = uint.MAX_VALUE + 1;&#xa;trace(myInt); // 0&#xa; &#xa;myInt = int.MAX_VALUE + 1;&#xa;trace(myInt); // -2147483648" STYLE_REF="example_node" ID="ID_1076181636" CREATED="1425213438151" MODIFIED="1425213459900"/>
</node>
</node>
<node TEXT="Object" STYLE_REF="gray_node" ID="ID_1379849630" CREATED="1425213524814" MODIFIED="1425213623056">
<node TEXT="NaN If the instance is null and converted to Number" STYLE_REF="pre_last_node" ID="ID_1702552003" CREATED="1425213537727" MODIFIED="1425219374151"/>
<node TEXT="0 otherwise" STYLE_REF="pre_last_node" ID="ID_832758606" CREATED="1425213547087" MODIFIED="1425219362199"/>
</node>
<node TEXT="undefined" STYLE_REF="gray_node" ID="ID_135102455" CREATED="1425213580655" MODIFIED="1425213623055">
<node TEXT="NaN If converted to Number" STYLE_REF="pre_last_node" ID="ID_1275582041" CREATED="1425213594658" MODIFIED="1425219402613"/>
<node TEXT="0 If converted to int or uint" STYLE_REF="pre_last_node" ID="ID_260681513" CREATED="1425213639583" MODIFIED="1425219397174"/>
</node>
<node TEXT="Date" STYLE_REF="gray_node" ID="ID_1481386303" CREATED="1425213599975" MODIFIED="1425213623053">
<node TEXT="The internal representation of the Date object, which is the number&#xa;of milliseconds since midnight January 1, 1970, universal time." STYLE_REF="pre_last_node" ID="ID_843865596" CREATED="1425213604941" MODIFIED="1425213629581"/>
</node>
</node>
</node>
<node TEXT="Boolean" STYLE_REF="yellow_node" ID="ID_1518847814" CREATED="1425217988777" MODIFIED="1425218006825">
<font BOLD="true"/>
<node TEXT="From" STYLE_REF="yellow_node" ID="ID_753728551" CREATED="1425218253177" MODIFIED="1425218280514">
<node TEXT="uint, int, and Number" STYLE_REF="gray_node" ID="ID_408878095" CREATED="1425218023953" MODIFIED="1425218270613">
<node TEXT="false if the numberic is 0" STYLE_REF="pre_last_node" ID="ID_1988752716" CREATED="1425218026337" MODIFIED="1425218149430"/>
<node TEXT="true otherwise" STYLE_REF="pre_last_node" ID="ID_987089670" CREATED="1425218053585" MODIFIED="1425218149432"/>
<node TEXT="Boolean(-1) is true&#xa;Boolean(0) is false&#xa;Boolean(1) is true" ID="ID_1811433012" CREATED="1425218080408" MODIFIED="1425218081262"/>
</node>
<node TEXT="String" STYLE_REF="gray_node" ID="ID_408439816" CREATED="1425218096785" MODIFIED="1425218273757">
<node TEXT=" false if the string is either" STYLE_REF="pre_last_node" ID="ID_139329155" CREATED="1425218102297" MODIFIED="1425218149434">
<node TEXT="null" ID="ID_111255962" CREATED="1425218124409" MODIFIED="1425218125653"/>
<node TEXT="empty string &quot;&quot;" ID="ID_1107744626" CREATED="1425218126513" MODIFIED="1425218130909"/>
</node>
<node TEXT="true otherwise" STYLE_REF="pre_last_node" ID="ID_758239747" CREATED="1425218108833" MODIFIED="1425218149434"/>
<node TEXT="var str1:String;&#xa;trace(Boolean(str1)); // false&#xa;var str2:String = &quot;&quot;;&#xa;trace(Boolean(str2)); // false&#xa;var str3:String = &quot; &quot;;&#xa;trace(Boolean(str3)); // true" STYLE_REF="example_node" ID="ID_1903386338" CREATED="1425218168929" MODIFIED="1425218185837"/>
</node>
<node TEXT="Object" STYLE_REF="gray_node" ID="ID_228066469" CREATED="1425218188513" MODIFIED="1425218320302">
<node TEXT="false if the instance is null" STYLE_REF="pre_last_node" ID="ID_313485284" CREATED="1425218298075" MODIFIED="1425218396966"/>
<node TEXT="true otherwise" STYLE_REF="pre_last_node" ID="ID_838986663" CREATED="1425218305777" MODIFIED="1425218396973"/>
<node TEXT="var myObj:Object;&#xa;trace(Boolean(myObj));   &#xa;myObj = new Object();&#xa;trace(Boolean(myObj));" STYLE_REF="example_node" ID="ID_238878537" CREATED="1425218373241" MODIFIED="1425218390207"/>
</node>
</node>
<node TEXT="In strict mode you can assign values of any&#xa;data type to a Boolean variable without casting" STYLE_REF="pre_last_node" ID="ID_741200665" CREATED="1425218421897" MODIFIED="1425218457280">
<node TEXT="var myObj:Object = new Object();&#xa;var bool:Boolean = myObj;&#xa;trace(bool); // true&#xa;bool = &quot;random string&quot;;&#xa;trace(bool); // true&#xa;bool = new Array();&#xa;trace(bool); // true&#xa;bool = NaN;&#xa;trace(bool); // false" STYLE_REF="example_node" ID="ID_20284371" CREATED="1425218444489" MODIFIED="1425218460416"/>
</node>
</node>
<node TEXT="String" STYLE_REF="yellow_node" ID="ID_1129128770" CREATED="1425218472001" MODIFIED="1425218479933">
<font BOLD="true"/>
<node TEXT="From" STYLE_REF="yellow_node" ID="ID_689291333" CREATED="1425218480977" MODIFIED="1425218520534">
<node TEXT="Boolean" STYLE_REF="gray_node" ID="ID_972504787" CREATED="1425218801401" MODIFIED="1425219142298">
<node TEXT="&quot;true&quot;" STYLE_REF="pre_last_node" ID="ID_307484534" CREATED="1425218805065" MODIFIED="1425219151776"/>
<node TEXT="&quot;false&quot;" STYLE_REF="pre_last_node" ID="ID_926520727" CREATED="1425218810033" MODIFIED="1425219151778"/>
</node>
<node TEXT="Number, int or uint" STYLE_REF="gray_node" ID="ID_1149557144" CREATED="1425218823170" MODIFIED="1425219142298">
<node TEXT="A string representation of the number" STYLE_REF="pre_last_node" ID="ID_108788482" CREATED="1425218832513" MODIFIED="1425219151779"/>
</node>
<node TEXT="Object" STYLE_REF="gray_node" ID="ID_1485833741" CREATED="1425218552737" MODIFIED="1425219142297">
<node TEXT="&quot;null&quot; if the instance is null" STYLE_REF="pre_last_node" ID="ID_703159363" CREATED="1425218556009" MODIFIED="1425218607258"/>
<node TEXT="&quot;[object Object]&quot; otherwise" STYLE_REF="pre_last_node" ID="ID_1286005523" CREATED="1425218591665" MODIFIED="1425218607256"/>
</node>
<node TEXT="Array" STYLE_REF="gray_node" ID="ID_921788981" CREATED="1425218723401" MODIFIED="1425219142297">
<node TEXT="String comprising a comma-delimited&#xa;list of all the array elements" STYLE_REF="pre_last_node" ID="ID_999206106" CREATED="1425218738913" MODIFIED="1425218781568">
<node TEXT="var myArray:Array = [&quot;primary&quot;, &quot;secondary&quot;, &quot;tertiary&quot;];&#xa;trace(String(myArray)); // primary,secondary,tertiary" STYLE_REF="example_node" ID="ID_1433670692" CREATED="1425218765585" MODIFIED="1425218786839"/>
</node>
<node TEXT="&quot;null&quot; otherwise" STYLE_REF="pre_last_node" ID="ID_1740719129" CREATED="1425218758737" MODIFIED="1425218781572"/>
</node>
<node TEXT="Date" STYLE_REF="gray_node" ID="ID_496634885" CREATED="1425219117793" MODIFIED="1425219142296">
<node TEXT="A string representation of the Date object" STYLE_REF="pre_last_node" ID="ID_1956163950" CREATED="1425219119930" MODIFIED="1425219151780">
<node TEXT="var myDate:Date = new Date(2005,6,1);&#xa;trace(String(myDate)); // Fri Jul 1 00:00:00 GMT-0700 2005" STYLE_REF="example_node" ID="ID_1640125087" CREATED="1425219133954" MODIFIED="1425219160944"/>
</node>
</node>
</node>
</node>
</node>
</node>
</node>
<node TEXT="Functions" STYLE_REF="second_node" POSITION="left" ID="ID_970453397" CREATED="1425221338626" MODIFIED="1425224484671" VGAP="26">
<edge COLOR="#7e0d83"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Functions are blocks of code that carry out specific
    </p>
    <p>
      tasks and can be reused in your program
    </p>
  </body>
</html>
</richcontent>
<hook URI="../../Images/func-1.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Basic concept" STYLE_REF="orange_node" ID="ID_1144177197" CREATED="1425221542122" MODIFIED="1425224180042">
<node TEXT="Defining functions" STYLE_REF="yellow_node" ID="ID_1328143346" CREATED="1425221873371" MODIFIED="1425222264372"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      As a general rule, use a function statement unless
    </p>
    <p>
      specific circumstances call for the use of an expression
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Function statement" STYLE_REF="gray_node" ID="ID_1190213590" CREATED="1425221886691" MODIFIED="1425221940936"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Better for strict mode
    </p>
  </body>
</html>
</richcontent>
<node TEXT="function traceParameter(aParam:String):void {&#xa;    trace(aParam);&#xa;};&#xa;traceParameter(&quot;hello&quot;); // hello" STYLE_REF="example_node" ID="ID_243126129" CREATED="1425222048363" MODIFIED="1436774256950">
<edge STYLE="bezier"/>
</node>
<node TEXT="Features" STYLE_REF="yellow_node" ID="ID_1329641922" CREATED="1425222832840" MODIFIED="1425223956728">
<font BOLD="true"/>
<node TEXT="Less verbose" STYLE_REF="pre_last_node" ID="ID_1212619511" CREATED="1425222214883" MODIFIED="1425222344936"/>
<node TEXT="Make your code more concise" STYLE_REF="pre_last_node" ID="ID_896569872" CREATED="1425222245571" MODIFIED="1425222344929"/>
<node TEXT="Provide a more consistent experience&#xa;between the two compiler modes" STYLE_REF="pre_last_node" ID="ID_1227422340" CREATED="1425222325683" MODIFIED="1425222344927"/>
<node TEXT="It exists as its own object and continues to exist even&#xa;after you delete the property to which it is attached" STYLE_REF="pre_last_node" ID="ID_381833099" CREATED="1425222900531" MODIFIED="1425224055833">
<node TEXT="function stateFunc() { trace(&quot;Function statement&quot;) }&#xa;delete stateFunc; // no effect&#xa;stateFunc();// Function statement" STYLE_REF="example_node" ID="ID_1899023759" CREATED="1425223312427" MODIFIED="1425223332736"/>
</node>
<node TEXT="Function statements exist throughout&#xa;the scope in which they are defined" STYLE_REF="pre_last_node" ID="ID_1250866648" CREATED="1425224033628" MODIFIED="1425224060455">
<node TEXT="statementTest(); // statementTest&#xa; &#xa;function statementTest():void {&#xa;    trace(&quot;statementTest&quot;);&#xa;}" STYLE_REF="example_node" ID="ID_569400207" CREATED="1425224044387" MODIFIED="1425224051514"/>
</node>
</node>
</node>
<node TEXT="Function expression" STYLE_REF="gray_node" ID="ID_1632034380" CREATED="1425221898514" MODIFIED="1425221951930"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Better for standard mode
    </p>
  </body>
</html>
</richcontent>
<node TEXT="var traceParameter:Function = function (aParam:String) {&#xa;    trace(aParam);&#xa;};&#xa;traceParameter(&quot;hello&quot;); // hello" STYLE_REF="example_node" ID="ID_805015318" CREATED="1425222127363" MODIFIED="1425222135649"/>
<node TEXT="Features" STYLE_REF="yellow_node" ID="ID_320619849" CREATED="1425222676051" MODIFIED="1425223950879">
<font BOLD="true"/>
<node TEXT="When you assign a function expression to another object, you create the only reference to that function expression in your code.&#xa;If the array or object is deleted, the memory that the function expression uses will become eligible and will be reclaimed and reused for other purposes" STYLE_REF="pre_last_node" ID="ID_1668299232" CREATED="1425222707219" MODIFIED="1425223914674">
<node TEXT="dynamic class Test {}&#xa;var myTest:Test = new Test();&#xa;// function expression &#xa;myTest.functionExp = function () { trace(&quot;Function expression&quot;) };&#xa;myTest.functionExp(); // Function expression&#xa;delete myTest.functionExp;&#xa;myTest.functionExp(); // error" STYLE_REF="example_node" ID="ID_1015873508" CREATED="1425222815295" MODIFIED="1425222824585"/>
</node>
<node TEXT="Function expressions are not available before they are defined" STYLE_REF="pre_last_node" ID="ID_1616820618" CREATED="1425223801827" MODIFIED="1436774359108">
<node TEXT="expressionTest(); // run-time error&#xa; &#xa;var expressionTest:Function = function () {&#xa;    trace(&quot;expressionTest&quot;);&#xa;}" STYLE_REF="example_node" ID="ID_1580362024" CREATED="1425223812619" MODIFIED="1436774359107"/>
</node>
<node TEXT="Can be used only as a part of a statement,&#xa;usually an assignment statement" STYLE_REF="pre_last_node" ID="ID_1431048903" CREATED="1425222158067" MODIFIED="1425222171467"/>
</node>
<node TEXT="Use" STYLE_REF="yellow_node" ID="ID_433065036" CREATED="1425222433876" MODIFIED="1425222455446">
<font BOLD="true"/>
<node TEXT="For functions that are used only once and then discarded" STYLE_REF="pre_last_node" ID="ID_522116543" CREATED="1425222438315" MODIFIED="1425222459794"/>
<node TEXT="For attaching a function to a prototype property" STYLE_REF="pre_last_node" ID="ID_707876118" CREATED="1425222494443" MODIFIED="1425222500928"/>
</node>
<node TEXT="Examples" STYLE_REF="yellow_node" ID="ID_1750281446" CREATED="1425222462579" MODIFIED="1436774374788">
<font BOLD="true"/>
<node TEXT="var traceArray:Array = new Array();&#xa;traceArray[0] = function (aParam:String) {&#xa;    trace(aParam);&#xa;};&#xa;traceArray[0](&quot;hello&quot;);" STYLE_REF="example_node" ID="ID_1163253429" CREATED="1425222059378" MODIFIED="1425222106352"/>
<node TEXT="class Example {&#xa;var methodExpression = function() {}&#xa;function methodStatement() {}&#xa;}&#xa; &#xa;var myEx:Example = new Example();&#xa;myEx.methodExpression(); // error in strict mode; okay in standard mode&#xa;myEx.methodStatement(); // okay in strict and standard modes" STYLE_REF="example_node" ID="ID_1748793553" CREATED="1425222618476" MODIFIED="1436774374786"/>
</node>
</node>
</node>
<node TEXT="Nested functions" STYLE_REF="yellow_node" ID="ID_1294015431" CREATED="1425223470731" MODIFIED="1425224111688">
<node TEXT="When nested functions are passed to external code, they are passed as function closures, which means that the function retains any definitions that are in scope when the function is defined" STYLE_REF="pre_last_node" ID="ID_1111737919" CREATED="1425223531283" MODIFIED="1425224127129"/>
<node TEXT="function getNameAndVersion():String {&#xa;    function getVersion():String {&#xa;        return &quot;10&quot;;&#xa;    }&#xa;    function getProductName():String {&#xa;        return &quot;Flash Player&quot;;&#xa;    }&#xa;    return (getProductName() + &quot; &quot; + getVersion());&#xa;}&#xa;trace(getNameAndVersion()); // Flash Player 10" STYLE_REF="example_node" ID="ID_308091928" CREATED="1425223547612" MODIFIED="1425224111688"/>
</node>
</node>
<node TEXT="Types" STYLE_REF="orange_node" ID="ID_1983884001" CREATED="1425221360371" MODIFIED="1425221402601">
<node TEXT="Method" STYLE_REF="yellow_node" ID="ID_37509909" CREATED="1425221382538" MODIFIED="1425223700631">
<font BOLD="true"/>
<node TEXT="If it is defined as part of a class definition&#xa;or attach it to an instance of an object" STYLE_REF="pre_last_node" ID="ID_1295074578" CREATED="1425221434779" MODIFIED="1425221472505"/>
</node>
<node TEXT="Function closure" STYLE_REF="yellow_node" ID="ID_1756087357" CREATED="1425221387546" MODIFIED="1425223700633">
<font BOLD="true"/>
<node TEXT="If it is defined in any other way" STYLE_REF="pre_last_node" ID="ID_1516091095" CREATED="1425221462466" MODIFIED="1425221472504"/>
<node TEXT="Consist" STYLE_REF="pre_last_node" ID="ID_1645436598" CREATED="1425639877034" MODIFIED="1425639987015">
<node TEXT="Snapshot of a function" ID="ID_421633604" CREATED="1425639424659" MODIFIED="1425639426823"/>
<node TEXT="Lexical environment" ID="ID_1711308559" CREATED="1425639410498" MODIFIED="1425639507021"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Includes all the variables, properties, methods, and objects
    </p>
    <p>
      in the function&#8217;s scope chain, along with their values
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="function foo():Function {&#xa;    var x:int = 40;&#xa;    function rectArea(y:int):int {&#xa;        return x * y&#xa;    } &#xa;    return rectArea;&#xa;}&#xa;function bar():void {&#xa;    var x:int = 2;&#xa;    var y:int = 4;&#xa;    var myProduct:Function = foo();&#xa;    trace(myProduct(4)); // function closure called&#xa;}&#xa;bar(); // 160" STYLE_REF="example_node" ID="ID_100291506" CREATED="1425639992466" MODIFIED="1425640048671"/>
</node>
</node>
<node TEXT="Parameters" STYLE_REF="orange_node" ID="ID_366569608" CREATED="1425231409911" MODIFIED="1428478124748">
<node TEXT="Passing arguments" STYLE_REF="yellow_node" ID="ID_282537631" CREATED="1425452120485" MODIFIED="1425452144457">
<font BOLD="true"/>
<node TEXT="By value" STYLE_REF="gray_node" ID="ID_232947922" CREATED="1425452097055" MODIFIED="1436774501844">
<node TEXT="The value of the argument is copied into&#xa;a local variable for use within the function" STYLE_REF="pre_last_node" ID="ID_1689322722" CREATED="1425452168501" MODIFIED="1425452468411"/>
<node TEXT="For primitive data types" STYLE_REF="pre_last_node" ID="ID_628510231" CREATED="1425452343501" MODIFIED="1425452463770">
<node TEXT="Boolean" ID="ID_1608687346" CREATED="1425452346967" MODIFIED="1425452397833"/>
<node ID="ID_1129544259" CREATED="1425452354244" MODIFIED="1425452354244"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Number
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_579975428" CREATED="1425452359370" MODIFIED="1425452359370"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      int
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_1892063342" CREATED="1425452388808" MODIFIED="1425452388808"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      uint
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_1156831775" CREATED="1425452394124" MODIFIED="1425452394124"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      String
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="function passPrimitives(xParam:int, yParam:int):void {&#xa;    xParam++;&#xa;    yParam++;&#xa;    trace(xParam, yParam);&#xa;}&#xa; &#xa;var xValue:int = 10;&#xa;var yValue:int = 15;&#xa;trace(xValue, yValue);// 10 15&#xa;passPrimitives(xValue, yValue); // 11 16&#xa;trace(xValue, yValue);// 10 15" STYLE_REF="example_node" ID="ID_431346108" CREATED="1425452449637" MODIFIED="1436774501843"/>
</node>
<node TEXT="By reference" STYLE_REF="gray_node" ID="ID_1510100558" CREATED="1425452112365" MODIFIED="1425452243858">
<node TEXT="Only a reference to the argument is passed instead of the actual value" STYLE_REF="pre_last_node" ID="ID_164358570" CREATED="1425452195997" MODIFIED="1425452497506"/>
<node TEXT="Gives you the ability to change the value of the original variable" STYLE_REF="pre_last_node" ID="ID_428965035" CREATED="1425452236637" MODIFIED="1425452497512"/>
<node TEXT="function passByRef(objParam:Object):void {&#xa;    objParam.x++;&#xa;    objParam.y++;&#xa;    trace(objParam.x, objParam.y);&#xa;}&#xa;var objVar:Object = {x:10, y:15};&#xa;trace(objVar.x, objVar.y); // 10 15&#xa;passByRef(objVar); // 11 16&#xa;trace(objVar.x, objVar.y); // 11 16" STYLE_REF="example_node" ID="ID_1074480013" CREATED="1425452475277" MODIFIED="1425452490331"/>
</node>
</node>
<node TEXT="Default parameter values" STYLE_REF="yellow_node" ID="ID_461960272" CREATED="1425452602477" MODIFIED="1428478124748">
<font BOLD="true"/>
<node TEXT="All parameters with default values must be placed at the end of the parameter list" STYLE_REF="pre_last_node" ID="ID_123058096" CREATED="1425452614845" MODIFIED="1425452635810"/>
<node TEXT="function defaultValues(x:int, y:int = 3, z:int = 5):void {&#xa;    trace(x, y, z);&#xa;}&#xa;defaultValues(1); // 1 3 5" STYLE_REF="example_node" ID="ID_1110380987" CREATED="1425452624725" MODIFIED="1425452640282"/>
</node>
<node TEXT="Arguments object" STYLE_REF="yellow_node" ID="ID_1084073120" CREATED="1425452671701" MODIFIED="1425453023889">
<font BOLD="true"/>
<node TEXT="The arguments object is an array that includes&#xa;all the parameters passed to the function" STYLE_REF="pre_last_node" ID="ID_1437273353" CREATED="1425452684573" MODIFIED="1425453029586"/>
<node TEXT="The arguments.length property reports the&#xa;number of parameters passed to the function" STYLE_REF="pre_last_node" ID="ID_1684323330" CREATED="1425452718894" MODIFIED="1425453029602">
<node TEXT="function traceArgArray(x:int):void {&#xa;    for (var i:uint = 0; i &lt; arguments.length; i++) {&#xa;        trace(arguments[i]);&#xa;    }&#xa;}&#xa; &#xa;traceArgArray(1, 2, 3);" STYLE_REF="example_node" ID="ID_1478164204" CREATED="1425452887677" MODIFIED="1436775766813">
<node TEXT="// output:&#xa;// 1&#xa;// 2&#xa;// 3" ID="ID_1973324755" CREATED="1425452894845" MODIFIED="1436775766812"/>
</node>
</node>
<node TEXT="The arguments.callee property provides a reference to the function&#xa;itself, which is useful for recursive calls to function expressions" STYLE_REF="pre_last_node" ID="ID_1979988403" CREATED="1425452763486" MODIFIED="1425453029607">
<node TEXT="var factorial:Function = function (x:uint) {&#xa;    if(x == 0) {&#xa;        return 1;&#xa;    } else {&#xa;        return (x * arguments.callee(x - 1));&#xa;    }&#xa;}&#xa; &#xa;trace(factorial(5)); // 120" STYLE_REF="example_node" ID="ID_1423892959" CREATED="1425452919142" MODIFIED="1425452945596"/>
</node>
<node TEXT="The arguments object is not available if any parameter&#xa;is named arguments or if you use the ... (rest) parameter" STYLE_REF="pre_last_node" ID="ID_1534107870" CREATED="1425452875997" MODIFIED="1425453029612"/>
</node>
<node TEXT="The ... (rest) parameter" STYLE_REF="yellow_node" ID="ID_1801574075" CREATED="1425453169726" MODIFIED="1425453318041">
<font BOLD="true"/>
<node TEXT="Must be the last parameter specified" STYLE_REF="pre_last_node" ID="ID_1152200083" CREATED="1425453171765" MODIFIED="1425453322134"/>
<node TEXT="function traceArgArray(x: int, ... args) {&#xa;    for (var i:uint = 0; i &lt; args.length; i++) {&#xa;        trace(args[i]);&#xa;    }&#xa;}&#xa; &#xa;traceArgArray(1, 2, 3, 4);" STYLE_REF="example_node" ID="ID_1154839121" CREATED="1425453268806" MODIFIED="1425453325042">
<node TEXT="// output:&#xa;// 2&#xa;// 3&#xa;// 4" ID="ID_1966125130" CREATED="1425453287765" MODIFIED="1425453313562"/>
</node>
</node>
</node>
<node TEXT="Functions as objects" STYLE_REF="orange_node" ID="ID_1294189354" CREATED="1425454266597" MODIFIED="1425454334586">
<node TEXT="Function can be passed as a parameter to another function" STYLE_REF="pre_last_node" ID="ID_1345977412" CREATED="1425454291942" MODIFIED="1425454342062"/>
<node TEXT="Function can have properties and methods attached to it" STYLE_REF="pre_last_node" ID="ID_1691872990" CREATED="1425454312438" MODIFIED="1425454342065">
<node TEXT="Function properties can serve as quasi-static properties&#xa;that allow you to save the state of a variable related to the function" ID="ID_1967138536" CREATED="1425457327383" MODIFIED="1425457332923"/>
<node TEXT="// Compiles only in standard mode&#xa;var someFunction:Function = function ():void {&#xa;    someFunction.counter++;&#xa;}&#xa; &#xa;someFunction.counter = 0;&#xa; &#xa;someFunction();&#xa;someFunction();&#xa;trace(someFunction.counter); // 2" STYLE_REF="example_node" ID="ID_1616366859" CREATED="1425457299439" MODIFIED="1425457336867"/>
</node>
<node TEXT="Every function has a read-only property named length&#xa;that stores the number of parameters defined for the function" STYLE_REF="pre_last_node" ID="ID_1423665776" CREATED="1425457364127" MODIFIED="1425457379661">
<node TEXT="// Compiles only in standard mode&#xa;function traceLength(x:uint, y:uint):void {&#xa;    trace(&quot;arguments received: &quot; + arguments.length);&#xa;    trace(&quot;arguments expected: &quot; + traceLength.length);&#xa;}&#xa; &#xa;traceLength(3, 5, 7, 11);" STYLE_REF="example_node" ID="ID_337716323" CREATED="1425457385815" MODIFIED="1425457402900">
<node TEXT="/* output:&#xa;arguments received: 4&#xa;arguments expected: 2 */" ID="ID_247539599" CREATED="1425457398408" MODIFIED="1425457399365"/>
</node>
</node>
</node>
<node TEXT="Function scope" STYLE_REF="orange_node" ID="ID_412038754" CREATED="1425639394754" MODIFIED="1428478127834">
<node TEXT="First created" STYLE_REF="yellow_node" ID="ID_178889944" CREATED="1425639832346" MODIFIED="1428478127834">
<node TEXT="Activation object" STYLE_REF="gray_node" ID="ID_1128640297" CREATED="1425639646362" MODIFIED="1425639902361">
<node TEXT="Stores the parameters and any local variables&#xa;or functions declared in the function body" STYLE_REF="last_node" ID="ID_1965244448" CREATED="1425639663098" MODIFIED="1425639908458"/>
</node>
</node>
<node TEXT="Second created" STYLE_REF="yellow_node" ID="ID_696412520" CREATED="1425639835739" MODIFIED="1425639934456">
<node TEXT="Scope chain" STYLE_REF="gray_node" ID="ID_120719008" CREATED="1425639687586" MODIFIED="1425639902363">
<node TEXT="Starts with its own activation object, followed by its parent&#xa;function&#x2019;s AO, and it continues until it reaches the global object" STYLE_REF="last_node" ID="ID_501368059" CREATED="1425639754779" MODIFIED="1425639908460"/>
</node>
</node>
</node>
</node>
<node TEXT="Handling events" STYLE_REF="second_node" POSITION="right" ID="ID_1957109357" CREATED="1426157895158" MODIFIED="1426160710246">
<edge COLOR="#8f8f02"/>
<hook URI="../../Images/event.jpg" SIZE="1.0" NAME="ExternalObject"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Event model is based on the Document Object
    </p>
    <p>
      Model (DOM) Level 3 Events Specification
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Important concepts and terms" STYLE_REF="orange_node" ID="ID_1904613135" CREATED="1426157927146" MODIFIED="1426158151499">
<node TEXT="Dispatch" STYLE_REF="yellow_node" ID="ID_1239560963" CREATED="1426158172870" MODIFIED="1426158396530">
<node TEXT="To notify event listeners that an event has occurred" STYLE_REF="pre_last_node" ID="ID_211301656" CREATED="1426158178864" MODIFIED="1426158863214">
<icon BUILTIN="idea"/>
</node>
</node>
<node TEXT="Event" STYLE_REF="yellow_node" ID="ID_1068067486" CREATED="1426158190338" MODIFIED="1426158396529">
<node TEXT="Something that happens to an object that the object can tell other objects about" STYLE_REF="pre_last_node" ID="ID_78826541" CREATED="1426158200225" MODIFIED="1426158863217">
<icon BUILTIN="idea"/>
</node>
</node>
<node TEXT="Event target" STYLE_REF="yellow_node" ID="ID_490802024" CREATED="1426158329610" MODIFIED="1426158396521">
<node TEXT="The object that actually dispatches an event. For example, if the user clicks a button that is inside a Sprite that is in turn inside the Stage, all those objects dispatch events, but the event target is the one where the event actually happened&#x2014;in this case, the clicked button" STYLE_REF="pre_last_node" ID="ID_86899094" CREATED="1426158366295" MODIFIED="1426158863219">
<icon BUILTIN="idea"/>
</node>
</node>
<node TEXT="Listener" STYLE_REF="yellow_node" ID="ID_60123696" CREATED="1426158373808" MODIFIED="1426158396516">
<node TEXT="An object or function that has registered itself with an object, to indicate that it should be notified when a specific event takes place" STYLE_REF="pre_last_node" ID="ID_83027295" CREATED="1426158388974" MODIFIED="1426158863219">
<icon BUILTIN="idea"/>
</node>
</node>
<node TEXT="Error events" STYLE_REF="yellow_node" ID="ID_416145970" CREATED="1426233279675" MODIFIED="1426233405897">
<node TEXT="Exceptions, rather than events, are the primary&#xa;mechanism for error handling in ActionScript 3.0" STYLE_REF="pre_last_node" ID="ID_1716079937" CREATED="1426233309766" MODIFIED="1426233410460"/>
<node TEXT="But exception handling does not work for&#xa;asynchronous operations such as loading files" STYLE_REF="pre_last_node" ID="ID_1731064249" CREATED="1426233349609" MODIFIED="1426233410466"/>
<node TEXT="If you do not create a listener for the error event, the debugger&#xa;versions of FP will bring up a dialog box with information about the error" STYLE_REF="pre_last_node" ID="ID_744309566" CREATED="1426233368249" MODIFIED="1426233410468"/>
</node>
</node>
<node TEXT="Default behavior" STYLE_REF="orange_node" ID="ID_2345594" CREATED="1426158124935" MODIFIED="1436777162008">
<node TEXT="Some events include a behavior that normally happens along with the event, known as the default behavior. For example, when a user types text in a text field, a text input event is raised. The default behavior for that event is to actually display the character that was typed into the text field&#x2014;but you can override that default behavior" STYLE_REF="pre_last_node" ID="ID_1500126302" CREATED="1426158144886" MODIFIED="1426158856478">
<icon BUILTIN="idea"/>
</node>
<node TEXT="Many types of event objects do not have associated default behaviors" STYLE_REF="pre_last_node" ID="ID_210378867" CREATED="1426158741523" MODIFIED="1436777162008"/>
<node TEXT="Default behaviors are associated only with event objects dispatched by Flash Player or AIR, and do not exist for event objects dispatched programmatically through ActionScript" STYLE_REF="pre_last_node" ID="ID_225141433" CREATED="1426158809224" MODIFIED="1426158873159"/>
<node TEXT="To prevent" STYLE_REF="yellow_node" ID="ID_1292380356" CREATED="1426159044008" MODIFIED="1426228019908">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="80" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_388620633" STARTINCLINATION="1598;0;" ENDINCLINATION="1598;0;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
</node>
</node>
<node TEXT="Event flow" STYLE_REF="orange_node" ID="ID_419566431" CREATED="1426158209853" MODIFIED="1426158957203">
<node TEXT="The event flow describes how an event object moves through the display list" STYLE_REF="pre_last_node" ID="ID_1928664252" CREATED="1426158244675" MODIFIED="1426159346089">
<icon BUILTIN="idea"/>
</node>
<node TEXT="If the event target is" STYLE_REF="yellow_node" ID="ID_1676581800" CREATED="1426158939622" MODIFIED="1426159467631">
<font BOLD="true"/>
<node TEXT="Not on the display list" STYLE_REF="gray_node" ID="ID_1835123484" CREATED="1426158959141" MODIFIED="1426160032659">
<font BOLD="false"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Or the events are: enterFrame, init
    </p>
  </body>
</html>
</richcontent>
<node TEXT="FP dispatches the event object directly to the event target" STYLE_REF="pre_last_node" ID="ID_391648179" CREATED="1426158990283" MODIFIED="1426159278038"/>
</node>
<node TEXT="On the display list" STYLE_REF="gray_node" ID="ID_1911619901" CREATED="1426158971784" MODIFIED="1426159273461">
<font BOLD="false"/>
<node TEXT="FP dispatches the event object into the display list, and the event object travels through the display list to the event target" STYLE_REF="pre_last_node" ID="ID_1575680670" CREATED="1426159250251" MODIFIED="1426159278035">
<node TEXT="It means you can add event listeners not only to a target node,&#xa;but also to any node along the event flow" ID="ID_1333047923" CREATED="1426159902464" MODIFIED="1426159915817"/>
</node>
</node>
</node>
<node TEXT="Event flow parts" STYLE_REF="yellow_node" ID="ID_767753568" CREATED="1426159456168" MODIFIED="1426159529705">
<font BOLD="true"/>
<hook URI="event-flow.png" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Capture phase" STYLE_REF="gray_node" ID="ID_949159195" CREATED="1426159585684" MODIFIED="1426159652871">
<node TEXT="Event goes through all of the nodes from the&#xa;Stage to the parent of the target node" ID="ID_1917887691" CREATED="1426159593727" MODIFIED="1426159779146"/>
</node>
<node TEXT="Target phase" STYLE_REF="gray_node" ID="ID_1827494806" CREATED="1426159625130" MODIFIED="1426159652868">
<node TEXT="It notifies solely the target node" ID="ID_336052466" CREATED="1426159670555" MODIFIED="1426159714450"/>
</node>
<node TEXT="Bubbling phase" STYLE_REF="gray_node" ID="ID_276411577" CREATED="1426159644495" MODIFIED="1426159652864">
<node TEXT="Event returns from the parent of the target node back to the Stage" ID="ID_668810067" CREATED="1426159753535" MODIFIED="1426159791546"/>
</node>
</node>
</node>
<node TEXT="Event object" STYLE_REF="orange_node" ID="ID_268075783" CREATED="1426158266354" MODIFIED="1426160223872">
<node TEXT="An object that contains information about a particular event&#x2019;s occurrence, which is sent to all listeners when an event is dispatched" STYLE_REF="pre_last_node" ID="ID_1643492298" CREATED="1426158317257" MODIFIED="1426158863218">
<icon BUILTIN="idea"/>
</node>
<node TEXT="An object contains a set of methods that allow you to manipulate event objects and affect the behavior of the event-handling system" STYLE_REF="pre_last_node" ID="ID_737407868" CREATED="1426160615285" MODIFIED="1426160638991">
<icon BUILTIN="idea"/>
</node>
<node TEXT="Event class properties" STYLE_REF="gray_node" ID="ID_511292825" CREATED="1426160727341" MODIFIED="1426160877500">
<node TEXT="Event.type" STYLE_REF="yellow_node" ID="ID_1718991092" CREATED="1426160761720" MODIFIED="1426161363286">
<font BOLD="true"/>
<node TEXT="Event object types are represented by constants as string values" STYLE_REF="pre_last_node" ID="ID_1381753485" CREATED="1426161367324" MODIFIED="1426161375319">
<icon BUILTIN="idea"/>
</node>
<node TEXT="These constants provide an easy way to refer to specific event types.&#xa;You should use these constants instead of the strings they represent" STYLE_REF="pre_last_node" ID="ID_674579769" CREATED="1426160966132" MODIFIED="1426161020027">
<node TEXT="myDisplayObject.addEventListener(MouseEvent.CLICK, clickHandler);" ID="ID_1789075665" CREATED="1426161025980" MODIFIED="1426161026817"/>
<node TEXT="myDisplayObject.addEventListener(&quot;click&quot;, clickHandler);" ID="ID_1178269580" CREATED="1426161030851" MODIFIED="1426161049158"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Not recommended
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="package flash.events  {&#xa;    public class Event  {&#xa;        // class constants&#xa;        public static const ACTIVATE:String = &quot;activate&quot;;&#xa;        public static const ADDED:String= &quot;added&quot;;&#xa;    }&#xa;}" STYLE_REF="pre_last_node" ID="ID_592953433" CREATED="1426160972573" MODIFIED="1426160996803"/>
</node>
<node TEXT="Event.cancelable" STYLE_REF="yellow_node" ID="ID_997080008" CREATED="1426160792344" MODIFIED="1426161349463">
<font BOLD="true"/>
<node TEXT="An event&#x2019;s default behavior can be prevented&#xa;is represented by this Boolean value" STYLE_REF="pre_last_node" ID="ID_1593526078" CREATED="1426161352146" MODIFIED="1426161360367">
<icon BUILTIN="idea"/>
</node>
</node>
<node TEXT="Event.bubbles" STYLE_REF="yellow_node" ID="ID_302858861" CREATED="1426161207177" MODIFIED="1426161344487">
<font BOLD="true"/>
<node TEXT="Property stores a Boolean value that indicates whether&#xa;the event object participates in the bubbling phase" STYLE_REF="pre_last_node" ID="ID_1291882163" CREATED="1426161225774" MODIFIED="1426161451482">
<icon BUILTIN="idea"/>
</node>
</node>
<node TEXT="Event.eventPhase" STYLE_REF="yellow_node" ID="ID_872678956" CREATED="1426161249591" MODIFIED="1426161344489">
<font BOLD="true"/>
<node TEXT="Property indicates the current phase in the event flow" STYLE_REF="pre_last_node" ID="ID_582721302" CREATED="1426161262387" MODIFIED="1426161334344">
<icon BUILTIN="idea"/>
</node>
<node TEXT="package flash.events  {&#xa;    public final class EventPhase {&#xa;        public static const CAPTURING_PHASE:uint = 1;&#xa;        public static const AT_TARGET:uint = 2;&#xa;        public static const BUBBLING_PHASE:uint= 3;&#xa;    }&#xa;}" STYLE_REF="example_node" ID="ID_1115513371" CREATED="1426161502649" MODIFIED="1426161531310"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Use these constants
    </p>
  </body>
</html>
</richcontent>
<node TEXT="if (event.eventPhase == EventPhase.AT_TARGET)  {&#xa;    myFunc();&#xa;}" STYLE_REF="example_node" ID="ID_1291610293" CREATED="1426161554662" MODIFIED="1426161560816"/>
</node>
</node>
<node TEXT="Event.target" STYLE_REF="yellow_node" ID="ID_491279614" CREATED="1426161271906" MODIFIED="1426161344490">
<font BOLD="true"/>
<node TEXT="Property stores a reference to the event target" STYLE_REF="pre_last_node" ID="ID_1830094552" CREATED="1426161283020" MODIFIED="1426161334344">
<icon BUILTIN="idea"/>
</node>
<node TEXT="if a user inputs a mouse click on a point that includes overlapping display list objects, FP always choose the object that is farthest away from the Stage as the event target" STYLE_REF="pre_last_node" ID="ID_1447993003" CREATED="1426161624351" MODIFIED="1426161651922"/>
<node TEXT="If button are routinely decorated with smaller child objects, the target property may not be used frequently because it will often point to a button&#x2019;s child object instead of the button. In these situations, the common practice is to add event listeners to the button and use the currentTarget property because it points to the button" STYLE_REF="pre_last_node" ID="ID_618651570" CREATED="1426161703392" MODIFIED="1426161735025"/>
</node>
<node TEXT="Event.currentTarget" STYLE_REF="yellow_node" ID="ID_1676770874" CREATED="1426161296745" MODIFIED="1426161344490">
<font BOLD="true"/>
<node TEXT="Property stores a reference to the display list&#xa;object that is currently processing the event object" STYLE_REF="pre_last_node" ID="ID_1377872070" CREATED="1426161315186" MODIFIED="1426161334343">
<icon BUILTIN="idea"/>
</node>
</node>
</node>
<node TEXT="Event class methods" STYLE_REF="gray_node" ID="ID_1003493785" CREATED="1426161797845" MODIFIED="1426162034952">
<node TEXT="Event class utility methods" STYLE_REF="yellow_node" ID="ID_1730914290" CREATED="1426162756459" MODIFIED="1426162806960">
<font BOLD="true"/>
<node TEXT="clone()" STYLE_REF="gray_node" ID="ID_1155562265" CREATED="1426227082904" MODIFIED="1426227214439">
<hook NAME="FirstGroupNode"/>
<node TEXT="Allows you to create copies of an event object" STYLE_REF="pre_last_node" ID="ID_422304200" CREATED="1426227086264" MODIFIED="1426227252826"/>
</node>
<node TEXT="toString()" STYLE_REF="gray_node" ID="ID_1579369929" CREATED="1426227123271" MODIFIED="1426227177241">
<node TEXT="Allows you to generate a string representation of the&#xa;properties of an event object along with their values" STYLE_REF="pre_last_node" ID="ID_1768488039" CREATED="1426227138351" MODIFIED="1426227252829"/>
</node>
<node TEXT="For creating subclasses of the Event class, you&#xa;must override and implement both these methods" STYLE_REF="warning_node" ID="ID_1499472803" CREATED="1426227214438" MODIFIED="1426227375969">
<hook NAME="SummaryNode"/>
</node>
</node>
<node TEXT="Stopping event flow" STYLE_REF="yellow_node" ID="ID_1049123761" CREATED="1426162762698" MODIFIED="1426162806961">
<font BOLD="true"/>
<node TEXT="stopPropagation()" STYLE_REF="gray_node" ID="ID_1846019590" CREATED="1426227284773" MODIFIED="1426227371824">
<node TEXT="Prevents the event object from moving on to the next node, but only&#xa;after any other event listeners on the current node are allowed to execute" STYLE_REF="pre_last_node" ID="ID_1239678613" CREATED="1426227323902" MODIFIED="1426227382833"/>
<node TEXT="" ID="ID_1439315325" CREATED="1426227506717" MODIFIED="1426227514552">
<hook URI="stop-propagation.jpg" SIZE="1.0" NAME="ExternalObject"/>
</node>
</node>
<node TEXT="stopImmediatePropagation()" STYLE_REF="gray_node" ID="ID_228546052" CREATED="1426227295573" MODIFIED="1426227371826">
<node TEXT="Prevents the event object from moving on to the next node, but does&#xa;not allow any other event listeners on the current node to execute" STYLE_REF="pre_last_node" ID="ID_1882738531" CREATED="1426227355497" MODIFIED="1426227382838"/>
<node TEXT="" ID="ID_745367057" CREATED="1426227516667" MODIFIED="1426227525544">
<hook URI="stop-immediate-propagation.jpg" SIZE="1.0" NAME="ExternalObject"/>
</node>
</node>
</node>
<node TEXT="Cancelling default event behavior" STYLE_REF="yellow_node" ID="ID_388620633" CREATED="1426162771213" MODIFIED="1437060125959">
<font BOLD="true"/>
<node TEXT="preventDefault()" STYLE_REF="gray_node" ID="ID_1689357869" CREATED="1426227783334" MODIFIED="1437060125276">
<node TEXT="Method to cancel the default behavior associated with an event" STYLE_REF="pre_last_node" ID="ID_1489094449" CREATED="1426227847756" MODIFIED="1426227986945"/>
<node TEXT="Method will work only if the event&#x2019;s default behavior can be cancelled" STYLE_REF="pre_last_node" ID="ID_1609673652" CREATED="1426227933993" MODIFIED="1426227986945"/>
</node>
<node TEXT="isDefaultPrevented()" STYLE_REF="gray_node" ID="ID_28812621" CREATED="1426227762697" MODIFIED="1437060125959">
<node TEXT="true" STYLE_REF="pre_last_node" ID="ID_446551594" CREATED="1426227880323" MODIFIED="1426227986944"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      If the method has already been called
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="false" STYLE_REF="pre_last_node" ID="ID_1340620689" CREATED="1426227885374" MODIFIED="1426227986945"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Otherwise
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
</node>
</node>
<node TEXT="Event listeners" STYLE_REF="orange_node" ID="ID_1967305244" CREATED="1426230946328" MODIFIED="1428469402471">
<node TEXT="Functions that FP execute in response to specific events" STYLE_REF="pre_last_node" ID="ID_1678203589" CREATED="1426230976001" MODIFIED="1426231613367">
<icon BUILTIN="idea"/>
</node>
<node TEXT="Can be defined" STYLE_REF="yellow_node" ID="ID_486837344" CREATED="1426231565628" MODIFIED="1428469402471">
<font BOLD="true"/>
<node TEXT="Outside a class" STYLE_REF="pre_last_node" ID="ID_1845129539" CREATED="1426231580173" MODIFIED="1426231609844"/>
<node TEXT="As part of a class" STYLE_REF="pre_last_node" ID="ID_1940975668" CREATED="1426231592902" MODIFIED="1426231609848"/>
<node TEXT="Event listener that should not be used" STYLE_REF="pre_last_node" ID="ID_875860640" CREATED="1426231897795" MODIFIED="1426232004211">
<node TEXT="To create a generic object with a property that&#xa;points to a dynamically assigned listener function" STYLE_REF="pre_last_node" ID="ID_1074392113" CREATED="1426231919479" MODIFIED="1426232011073"/>
<node TEXT="class ChildSprite extends Sprite  {&#xa;    public function ChildSprite() {&#xa;        graphics.beginFill(0xFF0000);&#xa;        graphics.drawRect(0,0,100,100);&#xa;        graphics.endFill();&#xa;        addEventListener(MouseEvent.CLICK, myListenerObj.clickHandler);&#xa;    }&#xa;}&#xa; &#xa;var myListenerObj:Object = new Object();&#xa;myListenerObj.clickHandler = function (event:MouseEvent):void {&#xa;        trace(&quot;clickHandler detected an event of type: &quot; + event.type);&#xa;        trace(&quot;the this keyword refers to: &quot; + this);&#xa;}" STYLE_REF="example_node" ID="ID_318453727" CREATED="1426231941502" MODIFIED="1426232007562">
<node TEXT="You would expect that this would refer to myListenerObj,&#xa;but instead it refers to the global object" ID="ID_15522575" CREATED="1426231983231" MODIFIED="1426231998618"/>
</node>
</node>
</node>
<node TEXT="Managing" STYLE_REF="yellow_node" ID="ID_989034435" CREATED="1426232028455" MODIFIED="1426232081351">
<font BOLD="true"/>
<node TEXT="addEventListener" STYLE_REF="pre_last_node" ID="ID_867056700" CREATED="1426232070259" MODIFIED="1426232817275"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      function addEventListener(type:String,&#160;
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;listener:Object,
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;useCapture:Boolean=false,
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;priority:Integer=0,
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;useWeakReference:Boolean=false):Boolean;
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true"/>
<node TEXT="type" STYLE_REF="gray_node" ID="ID_82091512" CREATED="1426232101809" MODIFIED="1426232703099"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      It specifies the type of event
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="listener" STYLE_REF="gray_node" ID="ID_1010653906" CREATED="1426232352628" MODIFIED="1426232703097"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      It specifies the listener function that will
    </p>
    <p>
      execute when the event occurs
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="useCapture" STYLE_REF="gray_node" ID="ID_663059920" CREATED="1426232410038" MODIFIED="1426232703096">
<node TEXT="true" STYLE_REF="pre_last_node" ID="ID_554695561" CREATED="1426232445917" MODIFIED="1426232714514"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Your listener will be active during the
    </p>
    <p>
      capture phase of the event flow
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="false" STYLE_REF="pre_last_node" ID="ID_355603981" CREATED="1426232481602" MODIFIED="1426232714516"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Your listener will be active during the target
    </p>
    <p>
      and bubbling phases of the event flow
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="You can listen for events during all event phases by calling addEventListener() twice, once with useCapture set to true, and then again with it set to false" ID="ID_137733928" CREATED="1426233141294" MODIFIED="1426233154301"/>
</node>
<node TEXT="priority" STYLE_REF="gray_node" ID="ID_929419488" CREATED="1426232508411" MODIFIED="1426232703094"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      The default value is 0
    </p>
  </body>
</html>
</richcontent>
<node TEXT="The higher the number, the sooner that&#xa;event listener will be executed" STYLE_REF="pre_last_node" ID="ID_1671408680" CREATED="1426232557297" MODIFIED="1426232714517"/>
<node TEXT="Event listeners with the same priority are executed&#xa;in the order that they were added" STYLE_REF="pre_last_node" ID="ID_1775557969" CREATED="1426232581074" MODIFIED="1426232714522"/>
</node>
<node TEXT="useWeakReference" STYLE_REF="gray_node" ID="ID_322952169" CREATED="1426232599984" MODIFIED="1426232703091">
<node TEXT="Garbage collection will clear objects from memory that are no longer in use" STYLE_REF="pre_last_node" ID="ID_773568043" CREATED="1426232663972" MODIFIED="1426232714524"/>
<node TEXT="An object is considered no longer in use if no references to it exist" STYLE_REF="pre_last_node" ID="ID_177719241" CREATED="1426232644030" MODIFIED="1426232714525"/>
<node TEXT="The garbage collector disregards weak references, which means that a listener function that has only a weak reference pointing to it is eligible for garbage collection" STYLE_REF="pre_last_node" ID="ID_1982718437" CREATED="1426232688442" MODIFIED="1426232714526"/>
</node>
</node>
<node TEXT="removeEventListener" STYLE_REF="pre_last_node" ID="ID_989190520" CREATED="1426232758841" MODIFIED="1426232817274"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      function removeEventListener(eventName:String,&#160;
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;listener:Object,
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;useCapture:Boolean=false):Boolean;
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true"/>
<node TEXT="Method to remove an event listener that you no longer need" STYLE_REF="pre_last_node" ID="ID_890252505" CREATED="1426233075368" MODIFIED="1426233191147"/>
<node TEXT="Required parameters include the eventName and listener parameters, which&#xa;are the same as the required parameters for the addEventListener() method" STYLE_REF="pre_last_node" ID="ID_1454052073" CREATED="1426233097855" MODIFIED="1426233191144"/>
<node TEXT="To remove both event listeners, you would need to call removeEventListener() twice, once with useCapture set to true, and then again with it set to false" STYLE_REF="pre_last_node" ID="ID_1761783670" CREATED="1426233175727" MODIFIED="1426233191148"/>
</node>
<node TEXT="dispatchEvent" STYLE_REF="pre_last_node" ID="ID_1531051390" CREATED="1426232772893" MODIFIED="1426232817274"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      function dispatchEvent(eventObject:Event):Boolean;
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true"/>
<node TEXT="Method can be used to dispatch a custom event object into the event flow" STYLE_REF="pre_last_node" ID="ID_165831383" CREATED="1426232990802" MODIFIED="1426233040892"/>
<node TEXT="The target property of the event object is set&#xa;to the object on which dispatchEvent() was called" STYLE_REF="pre_last_node" ID="ID_682399321" CREATED="1426233015021" MODIFIED="1426233040891"/>
</node>
<node TEXT="hasEventListener" STYLE_REF="pre_last_node" ID="ID_838931324" CREATED="1426232786318" MODIFIED="1426232817273"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      function hasEventListener(eventName:String):Boolean;
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true"/>
<node TEXT="true" STYLE_REF="yellow_node" ID="ID_380306158" CREATED="1426232843509" MODIFIED="1426233046452">
<node TEXT="if an event listener is found for a specific&#xa;event type on a particular display list object" STYLE_REF="pre_last_node" ID="ID_8939003" CREATED="1426233028739" MODIFIED="1426233040889"/>
</node>
</node>
<node TEXT="willTrigger" STYLE_REF="pre_last_node" ID="ID_1067925854" CREATED="1426232800730" MODIFIED="1426232817272"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      function willTrigger(eventName:String):Boolean;
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true"/>
<node TEXT="true" STYLE_REF="yellow_node" ID="ID_1941595846" CREATED="1426232890501" MODIFIED="1437060243490">
<node TEXT="if a listener is found for a particular display list object" STYLE_REF="pre_last_node" ID="ID_1701296529" CREATED="1426232900733" MODIFIED="1426233040888"/>
<node TEXT="but it checks for listeners not only on that display object, but also on all&#xa;of that display list object&#x2019;s ancestors for all phases of the event flow" STYLE_REF="pre_last_node" ID="ID_1971003081" CREATED="1426232909909" MODIFIED="1437060243489"/>
</node>
</node>
</node>
</node>
</node>
<node TEXT="Display programming" STYLE_REF="second_node" POSITION="left" ID="ID_1904623360" CREATED="1428386776725" MODIFIED="1428501015580">
<edge COLOR="#7c0000"/>
<hook URI="../../../../../D:/Documents/MindMaps/Images/display.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Hierarchy" STYLE_REF="pre_last_node" ID="ID_1404604304" CREATED="1428389438413" MODIFIED="1428392836354">
<hook URI="../../../../../D:/Documents/MindMaps/Programming/ActionScript/displayobject-subclasses.png" SIZE="0.7352941" NAME="ExternalObject"/>
</node>
<node TEXT="DisplayObject" STYLE_REF="orange_node" ID="ID_975752799" CREATED="1428389607363" MODIFIED="1436778502166">
<node TEXT="You cannot create a DisplayObject instance using&#xa;the DisplayObject class constructor" STYLE_REF="pre_last_node" ID="ID_1570467161" CREATED="1428389619821" MODIFIED="1428390488899"/>
<node TEXT="Position" STYLE_REF="gray_node" ID="ID_1327489473" CREATED="1428469409805" MODIFIED="1428469431365">
<node TEXT="x and y properties always refer to the position of the display object relative to the 0,0 coordinate of its parent display object&#x2019;s axes" STYLE_REF="pre_last_node" ID="ID_31969437" CREATED="1428469413400" MODIFIED="1428469791448"/>
<node TEXT="globalToLocal()" STYLE_REF="yellow_node" ID="ID_495589638" CREATED="1428469520529" MODIFIED="1428469786583">
<node TEXT="Convert coordinates from global (Stage)&#xa;coordinates to local coordinates" STYLE_REF="pre_last_node" ID="ID_444611445" CREATED="1428469566905" MODIFIED="1428469796381"/>
</node>
<node TEXT="localToGlobal()" STYLE_REF="yellow_node" ID="ID_306088804" CREATED="1428469532706" MODIFIED="1428469786582">
<node TEXT="Convert local coordinates to Stage coordinates" STYLE_REF="pre_last_node" ID="ID_1450921244" CREATED="1428469549929" MODIFIED="1428469799621"/>
</node>
</node>
<node TEXT="Drag-and-drop" STYLE_REF="gray_node" ID="ID_1546177355" CREATED="1428469705889" MODIFIED="1428491492693">
<node TEXT="Native drag" STYLE_REF="yellow_node" ID="ID_1343101955" CREATED="1428469724394" MODIFIED="1436778420023">
<node TEXT="Only one item at a time can be dragged using startDrag(). If one display object is being dragged and the startDrag() method is called on another display object, the first display object stops following the mouse immediately" STYLE_REF="pre_last_node" ID="ID_65418655" CREATED="1428469761433" MODIFIED="1428469813374"/>
<node TEXT="function startDragging(event:MouseEvent):void {&#xa;    square.startDrag();&#xa;}&#xa;&#xa;function stopDragging(event:MouseEvent):void {&#xa;    square.stopDrag();&#xa;}&#xa; &#xa;square.addEventListener(MouseEvent.MOUSE_DOWN, startDragging);&#xa;square.addEventListener(MouseEvent.MOUSE_UP, stopDragging);" STYLE_REF="example_node" ID="ID_1740323682" CREATED="1428469732969" MODIFIED="1436778420022"/>
</node>
<node TEXT="Another way" STYLE_REF="yellow_node" ID="ID_1820695587" CREATED="1428469991595" MODIFIED="1428653620658">
<node TEXT="var offsetX:Number;&#xa;var offsetY:Number;&#xa;var draggedObject:DisplayObject;&#xa; &#xa;function startDragging(event:MouseEvent):void {&#xa;    draggedObject = DisplayObject(event.target);&#xa;    offsetX = event.stageX - draggedObject.x;&#xa;    offsetY = event.stageY - draggedObject.y;&#xa;    &#xa;    stage.addChild(draggedObject);&#xa;    stage.addEventListener(MouseEvent.MOUSE_MOVE, dragObject);&#xa;}&#xa;&#xa;function stopDragging(event:MouseEvent):void {&#xa;    stage.removeEventListener(MouseEvent.MOUSE_MOVE, dragObject);&#xa;}&#xa;&#xa;function dragObject(event:MouseEvent):void {&#xa;    draggedObject.x = event.stageX - offsetX;&#xa;    draggedObject.y = event.stageY - offsetY;&#xa;    event.updateAfterEvent();&#xa;}&#xa; &#xa;circle.addEventListener(MouseEvent.MOUSE_DOWN, startDragging);&#xa;circle.addEventListener(MouseEvent.MOUSE_UP, stopDragging);&#xa; &#xa;square.addEventListener(MouseEvent.MOUSE_DOWN, startDragging);&#xa;square.addEventListener(MouseEvent.MOUSE_UP, stopDragging);" STYLE_REF="example_node" ID="ID_7212297" CREATED="1428469995971" MODIFIED="1428653620655"/>
</node>
</node>
<node TEXT="Scrolling" STYLE_REF="gray_node" ID="ID_1566729027" CREATED="1428470268738" MODIFIED="1436778502166">
<node TEXT="Use scrollRect property to define the viewable area of the display object" STYLE_REF="pre_last_node" ID="ID_1440525574" CREATED="1428470280208" MODIFIED="1428470418526"/>
<node TEXT="bigText.scrollRect = new Rectangle(0, 0, bigText.width, 350);&#xa;bigText.cacheAsBitmap = true;&#xa; &#xa;function scrollUp(event:MouseEvent):void {&#xa;    var rect:Rectangle = bigText.scrollRect;&#xa;    rect.y -= 20;&#xa;    bigText.scrollRect = rect;&#xa;}&#xa; &#xa;function scrollDown(event:MouseEvent):void {&#xa;    var rect:Rectangle = bigText.scrollRect;&#xa;    rect.y += 20;&#xa;    bigText.scrollRect = rect;&#xa;}&#xa; &#xa;up.addEventListener(MouseEvent.CLICK, scrollUp);&#xa;down.addEventListener(MouseEvent.CLICK, scrollDown);" STYLE_REF="example_node" ID="ID_1342096727" CREATED="1428470370843" MODIFIED="1428470407238"/>
</node>
<node TEXT="Scaling" STYLE_REF="gray_node" ID="ID_1420907557" CREATED="1428470448673" MODIFIED="1428470734013">
<node TEXT="Changing the height or width --&gt; stretch or squeeze to fit in the new area" STYLE_REF="pre_last_node" ID="ID_1037698463" CREATED="1428470604155" MODIFIED="1428471117296"/>
<node TEXT="Vector shapes will be redrawen" STYLE_REF="pre_last_node" ID="ID_671996404" CREATED="1428470656874" MODIFIED="1428471117295"/>
<node TEXT="Bitmap will be scaled" STYLE_REF="pre_last_node" ID="ID_1109499667" CREATED="1428470693474" MODIFIED="1428471117294"/>
<node TEXT="Use" STYLE_REF="orange_node" ID="ID_230513977" CREATED="1428470819803" MODIFIED="1428471845132">
<font BOLD="false"/>
<node TEXT="trace(circle.scaleX); // output: 1&#xa;trace(circle.scaleY); // output: 1&#xa;circle.width = 100;&#xa;circle.height = 75;&#xa;trace(circle.scaleX); // output: 0.6622516556291391&#xa;trace(circle.scaleY); // output: 0.4966887417218543" STYLE_REF="example_node" ID="ID_1097804912" CREATED="1428470821930" MODIFIED="1428471134288"/>
<node TEXT="// Change the width directly.&#xa;square.width = 150;&#xa; &#xa;// Change the vertical scale to match the horizontal scale, &#xa;// to keep the size proportional.&#xa;square.scaleY = square.scaleX;" STYLE_REF="example_node" ID="ID_695508500" CREATED="1428470885673" MODIFIED="1428471134286"/>
</node>
<node TEXT="Controlling distortion" STYLE_REF="orange_node" ID="ID_1301875531" CREATED="1428471108254" MODIFIED="1428471889517">
<font BOLD="false"/>
<node TEXT="When a display object is scaled, the resulting&#xa;distortion is spread equally across the object" STYLE_REF="pre_last_node" ID="ID_894396880" CREATED="1428471320464" MODIFIED="1428471470061">
<hook URI="../../../../../D:/Documents/MindMaps/Programming/ActionScript/dp-slice-scale-wrong.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
<node TEXT="It would be preferable to have control over the scaling&#x2014;to be able to designate certain areas which should scale (the straight sides and middle) and areas which shouldn&#x2019;t (the corners). It is posible with scale9Grid" STYLE_REF="pre_last_node" ID="ID_227418411" CREATED="1428471420813" MODIFIED="1428471470063">
<hook URI="../../../../../D:/Documents/MindMaps/Programming/ActionScript/dp-slice-scale-right.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
<node TEXT="With 9-slice scaling, the display object is&#xa;divided into nine separate rectangles" STYLE_REF="yellow_node" ID="ID_378932822" CREATED="1428471512705" MODIFIED="1428471852524">
<hook URI="../../../../../D:/Documents/MindMaps/Programming/ActionScript/dp-slice-scale-grid.png" SIZE="1.0" NAME="ExternalObject"/>
<font BOLD="false"/>
<node TEXT="Any content that lies in the four corner rectangles (such as&#xa;the rounded corners of a button) will not be stretched" STYLE_REF="pre_last_node" ID="ID_996346415" CREATED="1428471546015" MODIFIED="1428471860326"/>
<node TEXT="The top-center and bottom-center rectangles&#xa;will scale horizontally but not vertically" STYLE_REF="pre_last_node" ID="ID_914629665" CREATED="1428471559425" MODIFIED="1428471860330"/>
<node TEXT="the left-middle and right-middle rectangles&#xa;will scale vertically but not horizontally" STYLE_REF="pre_last_node" ID="ID_792757561" CREATED="1428471571490" MODIFIED="1428471860332"/>
<node TEXT="The center rectangle will scale both horizontally and vertically" STYLE_REF="pre_last_node" ID="ID_797716158" CREATED="1428471578552" MODIFIED="1428471864340"/>
</node>
<node TEXT="Use scale9Grid" STYLE_REF="yellow_node" ID="ID_1295125113" CREATED="1428471667385" MODIFIED="1428471852526">
<font BOLD="false"/>
<node TEXT="myButton.scale9Grid = new Rectangle(32, 27, 71, 64);" STYLE_REF="pre_last_node" ID="ID_208730897" CREATED="1428471675595" MODIFIED="1428471870229"/>
<node TEXT="Area contained in the region defined by the Rectangle&#xa;instance represents the center rectangle of the Scale-9 grid" STYLE_REF="pre_last_node" ID="ID_1835542326" CREATED="1428471729169" MODIFIED="1428471870231">
<hook URI="../../../../../D:/Documents/MindMaps/Programming/ActionScript/dp-slice-scale-as-rectangle.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
<node TEXT="The other rectangles are calculated by FP" STYLE_REF="pre_last_node" ID="ID_313588794" CREATED="1428471752841" MODIFIED="1428471870231">
<hook URI="../../../../../D:/Documents/MindMaps/Programming/ActionScript/dp-slice-scale-as-grid-extended.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
</node>
</node>
</node>
<node TEXT="Caching" STYLE_REF="gray_node" ID="ID_1938943693" CREATED="1428475863064" MODIFIED="1428477551899">
<node TEXT="When you change the position of the rectangle,&#xa;FP redraws the entire Shape instance" STYLE_REF="pre_last_node" ID="ID_286222521" CREATED="1428475884711" MODIFIED="1428476738870"/>
<node TEXT="Instances with caching turned on are not continually redrawn" STYLE_REF="pre_last_node" ID="ID_1095770817" CREATED="1428475956158" MODIFIED="1428476738873"/>
<node TEXT="The opaqueBackground and scrollRect properties&#xa;work quicker when you set cacheAsBitmap to true" STYLE_REF="pre_last_node" ID="ID_922120163" CREATED="1428476010920" MODIFIED="1428476910331"/>
<node TEXT="Whenever you apply a filter to a display object, cacheAsBitmap&#xa;is automatically set to true, even if you explicitly set it to false" STYLE_REF="pre_last_node" ID="ID_1713005499" CREATED="1428476613687" MODIFIED="1428477551898"/>
<node TEXT="Surface - DisplayObject with setting the cacheAsBitmap property to true" STYLE_REF="pre_last_node" ID="ID_322749133" CREATED="1428476711145" MODIFIED="1428476738869"/>
<node TEXT="When to use caching" STYLE_REF="yellow_node" ID="ID_972572245" CREATED="1428476180969" MODIFIED="1428476189330">
<node TEXT="Complex background image" STYLE_REF="pre_last_node" ID="ID_186938970" CREATED="1428476254166" MODIFIED="1428476555067">
<node TEXT="Set opaqueBackground color like Stage has" ID="ID_1083800551" CREATED="1428476386023" MODIFIED="1428477297747"/>
</node>
<node TEXT="Scrolling text field" STYLE_REF="pre_last_node" ID="ID_1870421978" CREATED="1428476433263" MODIFIED="1428476555068">
<node TEXT="You can place the text field in a display object that you set&#xa;as scrollable with scrolling bounds (the scrollRect property)" ID="ID_302971529" CREATED="1428476453967" MODIFIED="1428476649261"/>
</node>
<node TEXT="Windowing system" STYLE_REF="pre_last_node" ID="ID_1546388942" CREATED="1428476498544" MODIFIED="1428476555068">
<node TEXT=" If you mark each window as a surface (by&#xa;setting the cacheAsBitmap property to true)" ID="ID_155734384" CREATED="1428476520976" MODIFIED="1428476542291"/>
<node TEXT="Users can drag the windows so that they overlap each other, and&#xa;each window doesn&#x2019;t need to regenerate the vector content" ID="ID_903520941" CREATED="1428476533799" MODIFIED="1428476547291"/>
</node>
<node TEXT="Alpha channel masking" STYLE_REF="pre_last_node" ID="ID_1440561887" CREATED="1428476570990" MODIFIED="1428476892516"/>
<node TEXT="For display object instances that&#xa;are largely static (non-animating)" STYLE_REF="pre_last_node" ID="ID_627482419" CREATED="1428476879095" MODIFIED="1428476987218"/>
</node>
<node TEXT="When to avoid caching" STYLE_REF="yellow_node" ID="ID_48920945" CREATED="1428476616303" MODIFIED="1428476643252">
<node TEXT="Do not overuse surfaces" STYLE_REF="pre_last_node" ID="ID_181724544" CREATED="1428476683527" MODIFIED="1428476892517">
<node TEXT="Each surface uses more memory than a regular display object, which means that you should only enable surfaces when you need to improve rendering performance." ID="ID_1635477678" CREATED="1428476696368" MODIFIED="1428476698083"/>
</node>
<node TEXT="Avoid zooming into cached surfaces" STYLE_REF="pre_last_node" ID="ID_1297749245" CREATED="1428476779646" MODIFIED="1428476892518"/>
<node TEXT="If you mix surfaces with vector data, it increases&#xa;the amount of processing that FP need to do" STYLE_REF="pre_last_node" ID="ID_1945991897" CREATED="1428476846471" MODIFIED="1428476892519"/>
</node>
<node TEXT="A surface is not created" STYLE_REF="yellow_node" ID="ID_1305735317" CREATED="1428477055866" MODIFIED="1428477085139">
<node TEXT="The bitmap is greater than 2880 pixels in height or width" STYLE_REF="pre_last_node" ID="ID_1896901883" CREATED="1428477066551" MODIFIED="1428477093307"/>
<node TEXT="The bitmap fails to allocate (because of an out-of-memory error)" STYLE_REF="pre_last_node" ID="ID_788139015" CREATED="1428477081667" MODIFIED="1428477093306"/>
</node>
<node TEXT="opaqueBackground" STYLE_REF="yellow_node" ID="ID_1173306878" CREATED="1428477378992" MODIFIED="1428491533438">
<node TEXT="myShape.cacheAsBitmap = true;&#xa;myShape.opaqueBackground = 0xFFFFFF; //This is color of the Stage" STYLE_REF="example_node" ID="ID_735934896" CREATED="1428477318959" MODIFIED="1428491533437"/>
<node TEXT="The background is then treated as a bitmap, which helps optimize performance" STYLE_REF="pre_last_node" ID="ID_1722845715" CREATED="1428477392359" MODIFIED="1428477450356"/>
</node>
</node>
<node TEXT="Fading" STYLE_REF="gray_node" ID="ID_1851810764" CREATED="1428477959996" MODIFIED="1428478054610">
<node TEXT="myBall.alpha = .5;" STYLE_REF="example_node" ID="ID_774032884" CREATED="1428477983921" MODIFIED="1428478059603"/>
</node>
<node TEXT="Rotating" STYLE_REF="gray_node" ID="ID_1324825820" CREATED="1428477999168" MODIFIED="1428478054611">
<node TEXT="square.rotation = 45;" STYLE_REF="example_node" ID="ID_1179156635" CREATED="1428478006846" MODIFIED="1428478059604"/>
</node>
<node TEXT="Blending modes" STYLE_REF="gray_node" ID="ID_1528936240" CREATED="1428477555207" MODIFIED="1428477568106">
<node TEXT="Combining the colors of one image (the base image) with the colors of another image (the blend image) to produce a third image&#x2014;the resulting image" STYLE_REF="pre_last_node" ID="ID_1030216114" CREATED="1428477596734" MODIFIED="1428477628212"/>
<node TEXT="blendMode" STYLE_REF="yellow_node" ID="ID_557193667" CREATED="1428477615288" MODIFIED="1428477896035">
<node TEXT="BlendMode.ADD" STYLE_REF="pre_last_node" ID="ID_420960354" CREATED="1428477622228" MODIFIED="1428477905884"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Commonly used to create an animated
    </p>
    <p>
      lightening dissolve effect between two images
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="BlendMode.ALPHA" STYLE_REF="pre_last_node" ID="ID_1596740238" CREATED="1428477622228" MODIFIED="1428477905885"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Commonly used to apply the transparency of
    </p>
    <p>
      the foreground on the background
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="BlendMode.DARKEN" STYLE_REF="pre_last_node" ID="ID_1310111942" CREATED="1428477622229" MODIFIED="1428477905885"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Commonly used to superimpose type
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="BlendMode.DIFFERENCE" STYLE_REF="pre_last_node" ID="ID_1595663935" CREATED="1428477622230" MODIFIED="1428477905886"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Commonly used to create more vibrant colors
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="BlendMode.ERASE" STYLE_REF="pre_last_node" ID="ID_861912841" CREATED="1428477622230" MODIFIED="1428477905887"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Commonly used to cut out (erase) part of the
    </p>
    <p>
      background using the foreground alpha
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="BlendMode.HARDLIGHT" STYLE_REF="pre_last_node" ID="ID_1687791955" CREATED="1428477622231" MODIFIED="1428477905888"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Commonly used to create shading effects
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="BlendMode.INVERT" STYLE_REF="pre_last_node" ID="ID_1908772716" CREATED="1428477622233" MODIFIED="1428477905889"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#160;Used to invert the background
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="BlendMode.LAYER" STYLE_REF="pre_last_node" ID="ID_237910717" CREATED="1428477622234" MODIFIED="1428477905890"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Used to force the creation of a temporary buffer
    </p>
    <p>
      for precomposition for a particular display object
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="BlendMode.LIGHTEN" STYLE_REF="pre_last_node" ID="ID_930625371" CREATED="1428477622234" MODIFIED="1428477905890"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Commonly used to superimpose type
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="BlendMode.MULTIPLY" STYLE_REF="pre_last_node" ID="ID_970638326" CREATED="1428477622235" MODIFIED="1428477905891"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Commonly used to create shadows and depth effects
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="BlendMode.NORMAL" STYLE_REF="pre_last_node" ID="ID_1506480887" CREATED="1428477622236" MODIFIED="1428477905892"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Used to specify that the pixel values
    </p>
    <p>
      of the blend image override those of the base image
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="BlendMode.OVERLAY" STYLE_REF="pre_last_node" ID="ID_336087483" CREATED="1428477622237" MODIFIED="1428477905892"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#160;Commonly used to create shading effects
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="BlendMode.SCREEN" STYLE_REF="pre_last_node" ID="ID_231391798" CREATED="1428477622238" MODIFIED="1428477905893"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Commonly used to create highlights and lens flares
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="BlendMode.SHADER" STYLE_REF="pre_last_node" ID="ID_884851839" CREATED="1428477622238" MODIFIED="1428477905884"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Used to specify that a Pixel Bender shader
    </p>
    <p>
      is used to create a custom blending effect
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="BlendMode.SUBTRACT" STYLE_REF="pre_last_node" ID="ID_549126122" CREATED="1428477622239" MODIFIED="1428477905883"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Commonly used to create an animated
    </p>
    <p>
      darkening dissolve effect between two images
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="Transform" STYLE_REF="gray_node" ID="ID_251242839" CREATED="1428483141325" MODIFIED="1428488143696">
<node TEXT="Each display object has a transform property, which is an instance of the Transform class, and contains information about various transformations that are applied to the display object" STYLE_REF="pre_last_node" ID="ID_898707486" CREATED="1428483145461" MODIFIED="1428488073650"/>
<node TEXT="Transform class also includes a colorTransform property, which is an instance of the ColorTransform class, and provides access to make color adjustments to the display object" STYLE_REF="pre_last_node" ID="ID_1027614369" CREATED="1428483173996" MODIFIED="1428488143695"/>
<node TEXT="Set color" STYLE_REF="yellow_node" ID="ID_1757594478" CREATED="1428484600354" MODIFIED="1428488061095">
<node TEXT="var colorInfo:ColorTransform = square.transform.colorTransform;&#xa;&#xa;function makeBlue(event:MouseEvent):void {&#xa;    colorInfo.color = 0x003399;&#xa;    square.transform.colorTransform = colorInfo;&#xa;}&#xa;blueBtn.addEventListener(MouseEvent.CLICK, makeBlue);" STYLE_REF="example_node" ID="ID_1888556038" CREATED="1428484608948" MODIFIED="1428484633992"/>
<node TEXT="It completely changes the color of the entire object.&#xa;All children will be painted too" STYLE_REF="pre_last_node" ID="ID_1559527967" CREATED="1428484945513" MODIFIED="1428488067880"/>
</node>
<node TEXT="Adjust color" STYLE_REF="yellow_node" ID="ID_156398158" CREATED="1428487820643" MODIFIED="1428488088550">
<node TEXT="Properties" STYLE_REF="pre_last_node" ID="ID_321344754" CREATED="1428487947531" MODIFIED="1428488067881">
<node TEXT="redMultiplier" ID="ID_1075244213" CREATED="1428487954987" MODIFIED="1428487960385"/>
<node TEXT="greenMultiplier" ID="ID_1967578189" CREATED="1428487961168" MODIFIED="1428487965746"/>
<node TEXT="blueMultiplier" ID="ID_1572437104" CREATED="1428487966579" MODIFIED="1428487971091"/>
<node TEXT="alphaMultiplier" ID="ID_927688946" CREATED="1428487971812" MODIFIED="1428487989201"/>
<node TEXT="redOffset" ID="ID_1010687853" CREATED="1428488116931" MODIFIED="1428488118678"/>
<node TEXT="greenOffset" ID="ID_1339584288" CREATED="1428488122096" MODIFIED="1428488127433"/>
<node TEXT="blueOffset" ID="ID_1212180856" CREATED="1428488128323" MODIFIED="1428488133233"/>
<node TEXT="alphaOffset" ID="ID_1232512196" CREATED="1428488134027" MODIFIED="1428488138429"/>
</node>
<node TEXT="var loader:Loader = new Loader();&#xa;var url:URLRequest = new URLRequest(&quot;http://www.helpexamples.com/flash/images/image1.jpg&quot;);&#xa;loader.load(url);&#xa;this.addChild(loader);&#xa;&#xa;function adjustColor(event:MouseEvent):void {  &#xa;    var colorTransformer:ColorTransform = loader.transform.colorTransform;&#xa;    &#xa;    // Set the red and green multipliers according to the mouse position.&#xa;    // The red value ranges from 0% (no red) when the cursor is at the left&#xa;    // to 100% red (normal image appearance) when the cursor is at the right.&#xa;    colorTransformer.redMultiplier = (loader.mouseX / loader.width) * 1;&#xa;    colorTransformer.greenMultiplier = (loader.mouseY / loader.height) * 1;&#xa;    loader.transform.colorTransform = colorTransformer;&#xa;}&#xa; &#xa;loader.addEventListener(MouseEvent.MOUSE_MOVE, adjustColor);" STYLE_REF="example_node" ID="ID_1412199322" CREATED="1428487989930" MODIFIED="1428488049078"/>
</node>
</node>
<node TEXT="Masking" STYLE_REF="gray_node" ID="ID_525020589" CREATED="1428488278248" MODIFIED="1428491556636">
<node TEXT="You can use a display object as a mask to create a hole&#xa;through which the contents of another display object are visible" STYLE_REF="pre_last_node" ID="ID_1208811770" CREATED="1428488283457" MODIFIED="1428488776368"/>
<node TEXT="The masked display object is revealed under all opaque&#xa;(nontransparent) areas of the display object acting as the mask" STYLE_REF="pre_last_node" ID="ID_388919277" CREATED="1428488348594" MODIFIED="1428488791455"/>
<node TEXT="mySprite.mask = maskSprite;" STYLE_REF="example_node" ID="ID_408369172" CREATED="1428488431162" MODIFIED="1428491556636"/>
<node TEXT="mySprite.mask = null;" STYLE_REF="example_node" ID="ID_1783641765" CREATED="1428488438716" MODIFIED="1428488735616"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Remove mask
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Restrictions" STYLE_REF="yellow_node" ID="ID_443380481" CREATED="1428488597329" MODIFIED="1428488820664">
<node TEXT="You cannot use a mask to mask another mask" STYLE_REF="pre_last_node" ID="ID_1692444888" CREATED="1428488476233" MODIFIED="1428488730244"/>
<node TEXT="You cannot set the alpha property of a mask" STYLE_REF="pre_last_node" ID="ID_1647011547" CREATED="1428488481329" MODIFIED="1428488730245"/>
<node TEXT="Strokes in mask are ignored" STYLE_REF="pre_last_node" ID="ID_174831911" CREATED="1428488503826" MODIFIED="1428488730245"/>
</node>
<node TEXT="Alpha channel masking" STYLE_REF="yellow_node" ID="ID_1782788370" CREATED="1428488608948" MODIFIED="1428491551941">
<node TEXT="Alpha channel masking is supported if both the&#xa;mask and the masked display objects use bitmap" STYLE_REF="pre_last_node" ID="ID_1304842860" CREATED="1428488620434" MODIFIED="1428488730240"/>
<node TEXT="mySprite.cacheAsBitmap = true;&#xa;maskShape.cacheAsBitmap = true;&#xa;mySprite.mask = maskShape;" STYLE_REF="example_node" ID="ID_1022721072" CREATED="1428488642921" MODIFIED="1428491551940"/>
</node>
</node>
</node>
<node TEXT="DisplayObjectContainer" STYLE_REF="orange_node" ID="ID_790978371" CREATED="1428389764507" MODIFIED="1436778497982">
<node TEXT="Zero-based index positions, the front-to-back order" STYLE_REF="pre_last_node" ID="ID_485405790" CREATED="1428389864244" MODIFIED="1428390488898"/>
<node TEXT="Off-list display objects" STYLE_REF="pre_last_node" ID="ID_780578226" CREATED="1428389430516" MODIFIED="1428392836355"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Display objects that are not on the visible display list
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Methods" STYLE_REF="gray_node" ID="ID_1636569314" CREATED="1428390069085" MODIFIED="1436778497982" VGAP="10">
<node TEXT="removeChild()" STYLE_REF="yellow_node" ID="ID_1543174153" CREATED="1428390071902" MODIFIED="1428390463778">
<hook NAME="FirstGroupNode"/>
</node>
<node TEXT="removeChildAt()" STYLE_REF="yellow_node" ID="ID_328290853" CREATED="1428390082751" MODIFIED="1428390463786"/>
<node TEXT="Do not delete a display object instance entirely. They&#xa;simply remove it from the child list of the container" STYLE_REF="pre_last_node" ID="ID_1423273504" CREATED="1428390092657" MODIFIED="1428390473745">
<hook NAME="SummaryNode"/>
</node>
<node TEXT="addChildAt()" STYLE_REF="yellow_node" ID="ID_1223256323" CREATED="1428389853355" MODIFIED="1428390913076">
<hook NAME="FirstGroupNode"/>
</node>
<node TEXT="addChild()" STYLE_REF="yellow_node" ID="ID_1475506034" CREATED="1428390159214" MODIFIED="1428390482329"/>
<node TEXT="You can add an instance of a display object to only one display object container.&#xa;If it was contained in one display object container, and you add it to another container. It will be removed from the first display object container&#x2019;s child list" STYLE_REF="pre_last_node" ID="ID_342937896" CREATED="1428390166136" MODIFIED="1428390473763">
<hook NAME="SummaryNode"/>
</node>
<node TEXT="contains();" STYLE_REF="yellow_node" ID="ID_1868609548" CREATED="1428390362369" MODIFIED="1428390463804">
<node TEXT="Determines whether a display object is a child of a DisplayObjectContainer" STYLE_REF="pre_last_node" ID="ID_1914759959" CREATED="1428390374669" MODIFIED="1428390473768"/>
</node>
<node TEXT="getChildByName();" STYLE_REF="yellow_node" ID="ID_802944090" CREATED="1428390362369" MODIFIED="1428390945439">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="80" FONT_SIZE="12" FONT_FAMILY="SansSerif" DESTINATION="ID_1223256323" MIDDLE_LABEL="Slower performance than" STARTINCLINATION="545;46;" ENDINCLINATION="343;38;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
<node TEXT="Retrieves a display object by name" STYLE_REF="pre_last_node" ID="ID_1444620660" CREATED="1428390385179" MODIFIED="1428390473769"/>
<node TEXT="var url:URLRequest = new URLRequest(&quot;banana.jpg&quot;);&#xa;pict.load(url);&#xa;pict.name = &quot;banana loader&quot;;&#xa;container.addChild(pict);&#xa;trace(container.getChildByName(&quot;banana loader&quot;) is Loader); // true" STYLE_REF="example_node" ID="ID_504980908" CREATED="1428390809348" MODIFIED="1428390892618"/>
</node>
<node TEXT="getChildIndex();" STYLE_REF="yellow_node" ID="ID_1896976113" CREATED="1428390362376" MODIFIED="1428390463806">
<node TEXT="Returns the index position of a display object" STYLE_REF="pre_last_node" ID="ID_765848277" CREATED="1428390398643" MODIFIED="1428390473770"/>
</node>
<node TEXT="setChildIndex();" STYLE_REF="yellow_node" ID="ID_481504203" CREATED="1428390362379" MODIFIED="1428390463807">
<node TEXT="Changes the position of a child display object" STYLE_REF="pre_last_node" ID="ID_1277199637" CREATED="1428390413134" MODIFIED="1428390473771"/>
</node>
<node TEXT="swapChildren();" STYLE_REF="yellow_node" ID="ID_1499663753" CREATED="1428390362380" MODIFIED="1428390463808">
<node STYLE_REF="pre_last_node" ID="ID_975065645" CREATED="1428390427469" MODIFIED="1428390473772"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Swaps the front-to-back order of two display objects
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="swapChildrenAt();" STYLE_REF="yellow_node" ID="ID_121575566" CREATED="1428390362382" MODIFIED="1428390463809">
<node TEXT="Swaps the front-to-back order of two display objects,&#xa;specified by their index values." STYLE_REF="pre_last_node" ID="ID_21838414" CREATED="1428390438190" MODIFIED="1428390473752"/>
</node>
</node>
<node TEXT="Use" STYLE_REF="gray_node" ID="ID_299489727" CREATED="1428390513100" MODIFIED="1428390665688">
<node TEXT="ball_A = new Ball(0xFFCC00, &quot;a&quot;);&#xa;ball_A.name = &quot;ball_A&quot;;&#xa;ball_A.x = 20;&#xa;ball_A.y = 20;&#xa;container.addChild(ball_A);&#xa; &#xa;ball_B = new Ball(0xFFCC00, &quot;b&quot;);&#xa;ball_B.name = &quot;ball_B&quot;;&#xa;ball_B.x = 70;&#xa;ball_B.y = 20;&#xa;container.addChild(ball_B);&#xa; &#xa;ball_C = new Ball(0xFFCC00, &quot;c&quot;);&#xa;ball_C.name = &quot;ball_C&quot;;&#xa;ball_C.x = 40;&#xa;ball_C.y = 60;&#xa;container.addChildAt(ball_C, 1);" STYLE_REF="example_node" ID="ID_422584923" CREATED="1428390515494" MODIFIED="1428390660642"/>
<node TEXT="" STYLE_REF="example_node" ID="ID_977772522" CREATED="1428390543052" MODIFIED="1428390660652">
<hook URI="../../../../../D:/Documents/MindMaps/Programming/ActionScript/displayobject-layers.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
<node TEXT="container.addChild(ball_A);" STYLE_REF="example_node" ID="ID_1793771369" CREATED="1428390550965" MODIFIED="1428390660655"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      To move ball_A to the top of the stack
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="trace(container.getChildAt(0).name); // ball_A&#xa;trace(container.getChildAt(1).name); // ball_B&#xa;trace(container.getChildAt(2).name); // ball_C" STYLE_REF="example_node" ID="ID_1332985583" CREATED="1428390594870" MODIFIED="1428390660659"/>
<node TEXT="container.removeChild(ball_B);&#xa;trace(container.getChildAt(0).name); // ball_A&#xa;trace(container.getChildAt(1).name); // ball_C" STYLE_REF="example_node" ID="ID_1592846134" CREATED="1428390638773" MODIFIED="1428390660665"/>
</node>
<node TEXT="Traversing the display list" STYLE_REF="gray_node" ID="ID_1740944145" CREATED="1428391090244" MODIFIED="1436778478727">
<node TEXT="function traceDisplayList(container:DisplayObjectContainer, indentString:String = &quot;&quot;):void {&#xa;    var child:DisplayObject;&#xa;    for (var i:uint=0; i &lt; container.numChildren; i++) {&#xa;        child = container.getChildAt(i);&#xa;        trace(indentString, child, child.name); &#xa;        if (container.getChildAt(i) is DisplayObjectContainer) {&#xa;            traceDisplayList(DisplayObjectContainer(child), indentString + &quot;    &quot;)&#xa;        }&#xa;    }&#xa;}" STYLE_REF="example_node" ID="ID_1498644965" CREATED="1428391104924" MODIFIED="1436778478727"/>
</node>
</node>
<node TEXT="Stage" STYLE_REF="orange_node" ID="ID_550746039" CREATED="1428391253980" MODIFIED="1428391316689">
<node TEXT="Extends DisplayObject" STYLE_REF="pre_last_node" ID="ID_871444944" CREATED="1428391265477" MODIFIED="1428391331729"/>
<node TEXT="It overrides most properties and methods&#xa;of the DisplayObject class" STYLE_REF="pre_last_node" ID="ID_1618807845" CREATED="1428391283333" MODIFIED="1428391331732"/>
<node TEXT="If you call one of these overridden properties or methods,&#xa;FP throw an exception. For example x and y" STYLE_REF="pre_last_node" ID="ID_1467842832" CREATED="1428391297269" MODIFIED="1428391331734"/>
<node TEXT="framerate" STYLE_REF="gray_node" ID="ID_355250832" CREATED="1428391497997" MODIFIED="1428391522872">
<node TEXT="Is used to set the frame rate for all SWF&#xa;files loaded into the application" STYLE_REF="pre_last_node" ID="ID_984867826" CREATED="1428391509285" MODIFIED="1428391931177"/>
<node TEXT="Maximum 60" STYLE_REF="pre_last_node" ID="ID_187875216" CREATED="1428392770717" MODIFIED="1428392779784"/>
</node>
<node TEXT="scaleMode" STYLE_REF="gray_node" ID="ID_189669629" CREATED="1428391653751" MODIFIED="1428391915833">
<node TEXT="StageScaleMode.EXACT_FIT" STYLE_REF="yellow_node" ID="ID_611134285" CREATED="1428391774692" MODIFIED="1428391921337">
<node TEXT="Scales the SWFproportionally" STYLE_REF="pre_last_node" ID="ID_530336780" CREATED="1428391780812" MODIFIED="1428392823297"/>
</node>
<node TEXT="StageScaleMode.SHOW_ALL" STYLE_REF="yellow_node" ID="ID_243761801" CREATED="1428391791440" MODIFIED="1428391921337">
<node TEXT="Determines whether a border appears, like the black bars that appear when viewing a wide-screen movie on a standard television" STYLE_REF="pre_last_node" ID="ID_1568503549" CREATED="1428391805101" MODIFIED="1428391927201"/>
</node>
<node TEXT="StageScaleMode.NO_BORDER" STYLE_REF="yellow_node" ID="ID_1629004294" CREATED="1428391824979" MODIFIED="1428391921338">
<node TEXT="Determines whether the content can be partially cropped or not" STYLE_REF="pre_last_node" ID="ID_1065027629" CREATED="1428391835045" MODIFIED="1428391927206"/>
</node>
<node TEXT="StageScaleMode.NO_SCALE" STYLE_REF="yellow_node" ID="ID_1855857075" CREATED="1428391845421" MODIFIED="1428391921338">
<node TEXT="Only, the stageWidth and stageHeight properties of the Stage class can be used to determine the actual pixel dimensions of the resized window" STYLE_REF="pre_last_node" ID="ID_1283682497" CREATED="1428391885652" MODIFIED="1428391927204"/>
<node TEXT="The Stage class&#x2019;s resize event is dispatched, allowing you to make adjustments accordingly." STYLE_REF="pre_last_node" ID="ID_692377935" CREATED="1428391905873" MODIFIED="1428391927203"/>
</node>
</node>
<node TEXT="displayState" STYLE_REF="gray_node" ID="ID_242111161" CREATED="1428391999013" MODIFIED="1428491586231">
<node TEXT="StageDisplayState.FULL_SCREEN" STYLE_REF="yellow_node" ID="ID_1929224417" CREATED="1428392006858" MODIFIED="1428491586228">
<node TEXT="In Flash Player, full-screen mode can only be initiated through ActionScript in response to a mouse click or keypress" STYLE_REF="pre_last_node" ID="ID_1140898773" CREATED="1428392044731" MODIFIED="1428392798546"/>
<node TEXT="When Flash Player runs in a browser, all keyboard-related ActionScript, such as keyboard events and text entry in TextField instances, is disabled in full-screen mode" STYLE_REF="pre_last_node" ID="ID_242607227" CREATED="1428392435772" MODIFIED="1428392798545"/>
<node TEXT="Enabling full-screen mode in FP" STYLE_REF="pre_last_node" ID="ID_1264331607" CREATED="1428392133223" MODIFIED="1428392798548">
<node TEXT="&lt;object&gt;&#xa;    ...&#xa;    &lt;param name=&quot;allowFullScreen&quot; value=&quot;true&quot; /&gt;&#xa;    &lt;embed ... allowfullscreen=&quot;true&quot; /&gt;&#xa;&lt;/object&gt;" STYLE_REF="example_node" ID="ID_1971714966" CREATED="1428392191565" MODIFIED="1428392790710"/>
</node>
<node TEXT="FullScreenEvent" STYLE_REF="pre_last_node" ID="ID_1637318150" CREATED="1428392308981" MODIFIED="1428392798549">
<node TEXT="import flash.events.FullScreenEvent;&#xa; &#xa;function fullScreenRedraw(event:FullScreenEvent):void {&#xa;    if (event.fullScreen) {&#xa;&#xa;    } else {&#xa;&#xa;    }&#xa;}&#xa; &#xa;mySprite.stage.addEventListener(FullScreenEvent.FULL_SCREEN, fullScreenRedraw);" STYLE_REF="example_node" ID="ID_564106244" CREATED="1428392310998" MODIFIED="1428392790707"/>
</node>
<node TEXT="Hardware scaling in full-screen mode" STYLE_REF="pre_last_node" ID="ID_1760609848" CREATED="1428392572676" MODIFIED="1428392798544">
<node TEXT="You can use the Stage class&#x2019;s fullScreenSourceRect property to set FP to scale a specific region of the stage to full-screen mode" ID="ID_1095160145" CREATED="1428392597975" MODIFIED="1428392612311"/>
<node TEXT="{&#xa;    stage.fullScreenSourceRect = new Rectangle(0,0,320,240);&#xa;    stage.displayState = StageDisplayState.FULL_SCREEN;&#xa;}" STYLE_REF="example_node" ID="ID_434024874" CREATED="1428392628380" MODIFIED="1428392790706"/>
</node>
</node>
<node TEXT="StageDisplayState.NORMAL" STYLE_REF="yellow_node" ID="ID_929375180" CREATED="1428392121141" MODIFIED="1428392332120"/>
</node>
</node>
<node TEXT="Mouse" STYLE_REF="orange_node" ID="ID_230264533" CREATED="1435149875016" MODIFIED="1435150057168">
<node TEXT="Native mouse cursors" STYLE_REF="gray_node" ID="ID_296553378" CREATED="1435149908860" MODIFIED="1435226664800">
<node TEXT="var cursorData:MouseCursorData = new MouseCursorData();&#xa;cursorData.hotSpot = new Point(15, 15);&#xa;var bitmapDatas:Vector.&lt;BitmapData&gt; = new Vector.&lt;BitmapData&gt;(1, true);&#xa;bitmapDatas[0] = new com.twenty_one_learning.dpp_flex_lib.model.Assets.resizeIcon().bitmapData;&#xa;cursorData.data = bitmapDatas;&#xa;Mouse.registerCursor(&quot;myCursor&quot;, cursorData);" STYLE_REF="example_node" ID="ID_285287055" CREATED="1435149912650" MODIFIED="1435150075001"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Custom cursor
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="var cursorData:MouseCursorData = new MouseCursorData();&#xa;// Specify the hotspot&#xa;cursorData.hotSpot = new Point(15,15);&#xa;// Pass the cursor&apos;s bitmap to a BitmapData Vector&#xa;var bitmapDatas:Vector.&lt;BitmapData&gt; = new Vector.&lt;BitmapData&gt;(3, true);&#xa;// Create the bitmap cursor frames&#xa;// Bitmaps must be 32 x 32 pixels or less, due to an OS limitation&#xa;var frame1Bitmap:Bitmap = new frame1();&#xa;var frame2Bitmap:Bitmap = new frame2();&#xa;var frame3Bitmap:Bitmap = new frame3();&#xa;// Pass the values of the bitmap files to the bitmapDatas vector&#xa;bitmapDatas[0] = frame1Bitmap.bitmapData;&#xa;bitmapDatas[1] = frame2Bitmap.bitmapData;&#xa;bitmapDatas[2] = frame3Bitmap.bitmapData;&#xa;// Assign the bitmap data to the MouseCursor object&#xa;cursorData.data = bitmapDatas;&#xa;// Pass the frame rate of the animated cursor (1fps)&#xa;cursorData.frameRate = 1;&#xa;// Register the MouseCursorData to the Mouse object&#xa;Mouse.registerCursor(&quot;myCursor&quot;, cursorData);&#xa;// When needed for display, pass the alias to the existing cursor property&#xa;Mouse.cursor = &quot;myCursor&quot;;" STYLE_REF="example_node" ID="ID_610255089" CREATED="1435149972139" MODIFIED="1435226664703"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Custom animated cursor
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="private function rollOverHandler(event:MouseEvent):void {&#xa;    Mouse.cursor = &quot;myCursor&quot;;&#xa;}&#xa;&#xa;private function rollOutHandler(event:MouseEvent):void {&#xa;    if (!event.buttonDown) {&#xa;        Mouse.cursor = MouseCursor.AUTO;&#xa;    }&#xa;}" STYLE_REF="example_node" ID="ID_1304130775" CREATED="1435149941666" MODIFIED="1435150075012"/>
</node>
<node TEXT="Not native mouse cursor" STYLE_REF="gray_node" ID="ID_347847491" CREATED="1435150223730" MODIFIED="1435150278392">
<node TEXT=" public class MouseExample extends Sprite {&#xa;         private var cursor:CustomCursor;&#xa;         private var child:CustomButton;&#xa;         private var gutter:uint = 10;&#xa;&#xa;        public function MouseExample() {&#xa;            child = new CustomButton();&#xa;            child.x = gutter;&#xa;            child.y = gutter;&#xa;            addChild(child);&#xa;&#xa;            child.addEventListener(MouseEvent.MOUSE_OVER, mouseOverHandler);&#xa;            child.addEventListener(MouseEvent.MOUSE_OUT, mouseOutHandler);&#xa;&#xa;            cursor = new CustomCursor();&#xa;            addChild(cursor);&#xa;&#xa;            stage.addEventListener(Event.MOUSE_LEAVE, mouseLeaveHandler);&#xa;        }&#xa;&#xa;        private function mouseOverHandler(event:MouseEvent):void {&#xa;            Mouse.hide();&#xa;            child.addEventListener(MouseEvent.MOUSE_MOVE, mouseMoveHandler);&#xa;        }&#xa;&#xa;        private function mouseOutHandler(event:MouseEvent):void {&#xa;            Mouse.show();&#xa;            child.removeEventListener(MouseEvent.MOUSE_MOVE, mouseMoveHandler);&#xa;            cursor.visible = false;&#xa;        }&#xa;&#xa;        private function mouseMoveHandler(event:MouseEvent):void {&#xa;            cursor.x = event.localX;&#xa;            cursor.y = event.localY;&#xa;            event.updateAfterEvent();&#xa;            cursor.visible = true;&#xa;        }&#xa;&#xa;        private function mouseLeaveHandler(event:Event):void {&#xa;            mouseOutHandler(new MouseEvent(MouseEvent.MOUSE_MOVE));&#xa;        }&#xa;    }" STYLE_REF="example_node" ID="ID_1185803448" CREATED="1435150235035" MODIFIED="1435150284456"/>
<node TEXT="class CustomCursor extends Shape {&#xa;    var bgColor:uint = 0x333333;&#xa;    var size:uint = 10;&#xa;   &#xa;    public function CustomCursor() {&#xa;        visible = false;&#xa;        draw();&#xa;    }&#xa;   &#xa;    private function draw():void {&#xa;        graphics.clear();&#xa;        graphics.beginFill(bgColor);&#xa;        graphics.drawRect(0, 0, size, size);&#xa;        graphics.endFill();&#xa;    }" STYLE_REF="example_node" ID="ID_69301470" CREATED="1435150269835" MODIFIED="1435150284463"/>
</node>
</node>
</node>
<node TEXT="Collections" STYLE_REF="second_node" POSITION="right" ID="ID_1863218458" CREATED="1428398671272" MODIFIED="1428500857865">
<edge COLOR="#00007c"/>
<hook URI="../../../../../D:/Documents/MindMaps/Images/collection-2.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Maps" STYLE_REF="orange_node" ID="ID_790523718" CREATED="1428500705677" MODIFIED="1428501731459">
<hook URI="../../../../../D:/Documents/MindMaps/Images/map.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Based on Object" STYLE_REF="gray_node" ID="ID_255200080" CREATED="1428398685384" MODIFIED="1428500727700">
<node TEXT="There is one value associated with each key" STYLE_REF="yellow_node" ID="ID_405280992" CREATED="1428491690586" MODIFIED="1428491843278">
<node TEXT="var map:Object = new Object();&#xa;map.name1 = &quot;Lee&quot;;&#xa;trace(map.name1); //Lee&#xa;map.name1 = 1026;&#xa;trace(map.name1); //1026" STYLE_REF="example_node" ID="ID_1062951799" CREATED="1428491698097" MODIFIED="1428491848901"/>
</node>
<node TEXT="Dot syntax" STYLE_REF="yellow_node" ID="ID_717740037" CREATED="1428491778002" MODIFIED="1436777045336">
<node TEXT="var map:Object = new Object();&#xa;map.name1 = &quot;Lee&quot;;&#xa;trace( map.name1 ); //Lee trace&#xa;trace( map.name2 ); //undefined" STYLE_REF="example_node" ID="ID_1311837271" CREATED="1428491633019" MODIFIED="1436777045336"/>
</node>
<node TEXT="Array syntax" STYLE_REF="yellow_node" ID="ID_1701740063" CREATED="1428491748042" MODIFIED="1429093639002">
<node TEXT="ar map:Object = new Object();&#xa;map[&quot;password please&quot;] = &quot;Fireplace&quot;;" STYLE_REF="example_node" ID="ID_1646886723" CREATED="1428491801039" MODIFIED="1429093639001"/>
</node>
<node TEXT="Literal syntax" STYLE_REF="yellow_node" ID="ID_244706132" CREATED="1428491827680" MODIFIED="1428491843277">
<node TEXT=" var map:Object = {};&#xa;var map:Object = {name1:&quot;Alan&quot;, name2:&quot;Lee&quot;};" STYLE_REF="example_node" ID="ID_1808090348" CREATED="1428491865393" MODIFIED="1428491927686"/>
</node>
<node TEXT="Deleting keys" STYLE_REF="yellow_node" ID="ID_455440818" CREATED="1428491880833" MODIFIED="1428491922198">
<node TEXT="var map:Object = {name1:&quot;Alan&quot;, name2:&quot;Lee&quot;};&#xa;delete map[ &quot;name1&quot; ];&#xa;trace( map.name1 ); //undefined" STYLE_REF="example_node" ID="ID_71976013" CREATED="1428491897559" MODIFIED="1428491927689"/>
</node>
<node TEXT="Key Existence" STYLE_REF="yellow_node" ID="ID_78264968" CREATED="1428491965941" MODIFIED="1428496693541">
<node TEXT="var map:Object = {name1:&quot;Alan&quot;, name2:&quot;Lee&quot;};&#xa;map.hasOwnProperty(&quot;name1&quot;); //true&#xa;map.hasOwnProperty(&quot;name2&quot;); //true&#xa;map.hasOwnProperty(&quot;name3&quot;); //false&#xa;&#xa;delete map[&quot;name1&quot;];&#xa;map.hasOwnProperty(&quot;name1&quot;); //false&#xa;map.hasOwnProperty(&quot;name2&quot;); //true&#xa;map.hasOwnProperty(&quot;name3&quot;); //false" STYLE_REF="example_node" ID="ID_71481788" CREATED="1428491975608" MODIFIED="1428496693540"/>
</node>
</node>
<node TEXT="Based on Dictionary" STYLE_REF="gray_node" ID="ID_194141337" CREATED="1428495216881" MODIFIED="1428500727699">
<node TEXT="Differences from Object" STYLE_REF="yellow_node" ID="ID_486555767" CREATED="1428495477593" MODIFIED="1428495555157">
<node TEXT="Object syntax {} cannot be used with dictionaries" STYLE_REF="pre_last_node" ID="ID_470903995" CREATED="1428495518786" MODIFIED="1428496561988"/>
<node TEXT="Dictionaries can use objects for their keys" STYLE_REF="pre_last_node" ID="ID_166107644" CREATED="1428495534988" MODIFIED="1428496561988"/>
<node TEXT="Dictionaries have an optional constructor argument to use weak key" STYLE_REF="pre_last_node" ID="ID_178090017" CREATED="1428495548534" MODIFIED="1428496561988"/>
</node>
<node TEXT="Objects as keys" STYLE_REF="yellow_node" ID="ID_1783515957" CREATED="1428495938095" MODIFIED="1436777125737">
<node TEXT="var mapDict:Dictionary = new Dictionary();&#xa;var mySprite1:Sprite = new Sprite();&#xa;var mySprite2:Sprite = new Sprite();&#xa;mapDict[&quot;name1&quot;] = &quot;One&quot;;&#xa;mapDict[mySprite1] = &quot;Two&quot;;&#xa;mapDict[mySprite2] = &quot;Three&quot;;&#xa;&#xa;var mapObj:Object = new Object();&#xa;mapObj[&quot;name1&quot;] = &quot;One&quot;;&#xa;mapObj[mySprite1] = &quot;Two&quot;;&#xa;mapObj[mySprite2] = &quot;Three&quot;;" STYLE_REF="example_node" ID="ID_1882636576" CREATED="1428495839767" MODIFIED="1436777125736">
<node TEXT="for(var key:* in mapDict) {&#xa;    trace(key + &quot;: is String - &quot; + (key is String) );&#xa;    trace(key + &quot;: is Sprite - &quot; + (key is Sprite) );&#xa;}" STYLE_REF="example_node" ID="ID_1067276087" CREATED="1428496038888" MODIFIED="1428496131655">
<node TEXT="//Output&#xa;mySprite2: is String - false&#xa;mySprite2: is Sprite - true&#xa;mySprite1: is String - false&#xa;mySprite1: is Sprite - true&#xa;name1: is String - true&#xa;name1: is Sprite - false" STYLE_REF="example_node" ID="ID_235062871" CREATED="1428496077273" MODIFIED="1428496179044"/>
</node>
<node TEXT="for(var key:* in mapObj) {&#xa;    trace(key + &quot;: is String - &quot; + (key is String) );&#xa;    trace(key + &quot;: is Sprite - &quot; + (key is Sprite) );&#xa;}" STYLE_REF="example_node" ID="ID_1144039175" CREATED="1428496038888" MODIFIED="1428496131657">
<node TEXT="//Output&#xa;[object Sprite]: is String - true&#xa;[object Sprite]: is Sprite - false&#xa;name1: is String - true&#xa;name1: is Sprite - false" STYLE_REF="example_node" ID="ID_1383903835" CREATED="1428496077273" MODIFIED="1428496131658"/>
</node>
</node>
</node>
<node TEXT="Weak Keys" STYLE_REF="yellow_node" ID="ID_12344851" CREATED="1428495932600" MODIFIED="1428496703146">
<node TEXT="Basically, GC destroys objects when they are no longer in use in your program" STYLE_REF="pre_last_node" ID="ID_1418118624" CREATED="1428496219058" MODIFIED="1428496533587"/>
<node TEXT="Using an object as a key in a dictionary means that object is in use" STYLE_REF="pre_last_node" ID="ID_1806656031" CREATED="1428496249858" MODIFIED="1428496533588"/>
<node TEXT="Without weak keys" STYLE_REF="pre_last_node" ID="ID_1267874014" CREATED="1428496374744" MODIFIED="1428496703146">
<node TEXT="var map:Dictionary = new Dictionary();&#xa;var mySprite:Sprite = new Sprite();&#xa;map[mySprite] = &quot;I have a Sprite&quot;;" STYLE_REF="example_node" ID="ID_488575209" CREATED="1428496293704" MODIFIED="1428496543532">
<hook URI="../../../../../D:/Documents/MindMaps/Programming/ActionScript/weak-dict-1.jpg" SIZE="1.0" NAME="ExternalObject"/>
</node>
<node TEXT=" mySprite = null;" STYLE_REF="example_node" ID="ID_940867722" CREATED="1428496399746" MODIFIED="1428496543534">
<hook URI="../../../../../D:/Documents/MindMaps/Programming/ActionScript/weak-dict-2.jpg" SIZE="1.0" NAME="ExternalObject"/>
</node>
</node>
<node TEXT="Weak keys" STYLE_REF="pre_last_node" ID="ID_1246712678" CREATED="1428496465306" MODIFIED="1428496533589">
<node TEXT="var map:Dictionary = new Dictionary(true);&#xa;var mySprite:Sprite = new Sprite();&#xa;map[mySprite] = &quot;I have a Sprite&quot;;" STYLE_REF="example_node" ID="ID_1462991439" CREATED="1428496473265" MODIFIED="1428496543535">
<hook URI="../../../../../D:/Documents/MindMaps/Programming/ActionScript/weak-dict-3.jpg" SIZE="1.0" NAME="ExternalObject"/>
</node>
<node TEXT=" mySprite = null;" STYLE_REF="example_node" ID="ID_1247017693" CREATED="1428496499922" MODIFIED="1428496543537">
<hook URI="../../../../../D:/Documents/MindMaps/Programming/ActionScript/weak-dict-4.jpg" SIZE="1.0" NAME="ExternalObject"/>
</node>
<node TEXT="Sprite will be deleted by GC" STYLE_REF="example_node" ID="ID_1152095517" CREATED="1428496641841" MODIFIED="1428496654284"/>
</node>
</node>
</node>
</node>
<node TEXT="Vector" STYLE_REF="orange_node" ID="ID_1179710723" CREATED="1428497316783" MODIFIED="1428501792980">
<hook URI="../../../../../D:/Documents/MindMaps/Images/vector.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Features" STYLE_REF="yellow_node" ID="ID_605548035" CREATED="1428497991406" MODIFIED="1428498021124">
<node TEXT="Vectors are arrays that contain only one predefined type of data" STYLE_REF="pre_last_node" ID="ID_407107678" CREATED="1428497320619" MODIFIED="1428497940885"/>
<node TEXT="var myVector:Vector.&lt;Number&gt; = new Vector.&lt;Number&gt;();&#xa;var vector2:Vector.&lt;Sprite&gt; = new Vector.&lt;Sprite&gt;();&#xa;var vector3:Vector.&lt;IEventDispatcher&gt; = new Vector.&lt;IEventDispatcher&gt;();" STYLE_REF="example_node" ID="ID_44704555" CREATED="1428497507577" MODIFIED="1428497949380"/>
</node>
<node TEXT="Advantages" STYLE_REF="yellow_node" ID="ID_1659717567" CREATED="1428497359955" MODIFIED="1428497931516">
<node TEXT="They are performing better than standard arrays" STYLE_REF="pre_last_node" ID="ID_560429623" CREATED="1428497376712" MODIFIED="1428497959982"/>
<node TEXT="They can help catch coding errors by only&#xa;allowing a single data type to be added" STYLE_REF="pre_last_node" ID="ID_281374088" CREATED="1428497421412" MODIFIED="1428497959981"/>
</node>
<node TEXT="To specify an initial size" STYLE_REF="yellow_node" ID="ID_644176931" CREATED="1428497606311" MODIFIED="1428497931517">
<node TEXT="var days:Vector.&lt;String&gt; = new Vector.&lt;String&gt;(7);&#xa;trace( days.length ); //output: 7" STYLE_REF="example_node" ID="ID_6454029" CREATED="1428497621257" MODIFIED="1428497949382"/>
</node>
<node TEXT="To fix the size" STYLE_REF="yellow_node" ID="ID_617683129" CREATED="1428497730840" MODIFIED="1428497931517">
<node TEXT=" var days:Vector.&lt;String&gt; = new Vector.&lt;String&gt;( 7, true );" STYLE_REF="example_node" ID="ID_521456383" CREATED="1428497740241" MODIFIED="1428497949382">
<node TEXT="days.fixed = false; //you can change the length&#xa;days.fixed = true; //you can NOT change the length" STYLE_REF="example_node" ID="ID_421567884" CREATED="1428497810977" MODIFIED="1428497949383"/>
</node>
<node TEXT="It will optimize access, making updating and retrieving data faster" STYLE_REF="pre_last_node" ID="ID_1690975894" CREATED="1428497763720" MODIFIED="1428497959983"/>
</node>
<node TEXT="Every element has to have a value" STYLE_REF="yellow_node" ID="ID_985856650" CREATED="1428497922600" MODIFIED="1428498114755">
<node TEXT="var elements:Array = new Array();&#xa;elements[ 1 ] = &quot;B&quot;;" STYLE_REF="example_node" ID="ID_904289068" CREATED="1428498072827" MODIFIED="1428498123446"/>
<node TEXT="var elements:Vector.&lt;String&gt; = new Vector.&lt;String&gt;();&#xa;elements[ 1 ] = &quot;B&quot;; //Error&#xa;RangeError: Error #1125: The index 1 is out of range 0." STYLE_REF="example_node" ID="ID_931735217" CREATED="1428498092264" MODIFIED="1428498123444"/>
</node>
</node>
<node TEXT="ByteArray" STYLE_REF="orange_node" ID="ID_1094685891" CREATED="1428497508696" MODIFIED="1428501854677">
<hook URI="../../../../../D:/Documents/MindMaps/Images/byte.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Features" STYLE_REF="yellow_node" ID="ID_1181085573" CREATED="1428499840343" MODIFIED="1428499851507">
<node TEXT="ByteArray allows to work directly with binary data" STYLE_REF="pre_last_node" ID="ID_843212757" CREATED="1428498204839" MODIFIED="1428498313436"/>
<node TEXT="In a ByteArray, each slot is a fixed size&#x2014;a byte&#x2014;and the&#xa;items you write into the ByteArray span multiple bytes" STYLE_REF="pre_last_node" ID="ID_1373127250" CREATED="1428498435769" MODIFIED="1428499851507"/>
</node>
<node TEXT="Use" STYLE_REF="yellow_node" ID="ID_1424788207" CREATED="1428498315225" MODIFIED="1428498324477">
<node TEXT="var bytes:ByteArray = new ByteArray();&#xa;bytes.writeBoolean( true );&#xa;bytes.writeBoolean( false );&#xa;bytes.writeBoolean( true );&#xa;bytes.position=0;&#xa;bytes.readBoolean(); //Output: true" STYLE_REF="example_node" ID="ID_1132751074" CREATED="1428498278904" MODIFIED="1428498351133">
<hook URI="../../../../../D:/Documents/MindMaps/Programming/ActionScript/byte-array-1.jpg" SIZE="1.0" NAME="ExternalObject"/>
</node>
<node TEXT="var bytes:ByteArray = new ByteArray();&#xa;bytes.writeUnsignedInt(10);&#xa;bytes.writeBoolean(true);&#xa;bytes.writeUnsignedInt(26);&#xa;trace( bytes.length ); //output: 9" STYLE_REF="example_node" ID="ID_1026159944" CREATED="1428498482295" MODIFIED="1428498539852">
<hook URI="../../../../../D:/Documents/MindMaps/Programming/ActionScript/byte-array-2.jpg" SIZE="0.9302326" NAME="ExternalObject"/>
</node>
</node>
<node TEXT="Position" STYLE_REF="yellow_node" ID="ID_325611457" CREATED="1428498573866" MODIFIED="1428498584228">
<node TEXT="Each byte within the ByteArray is a position" STYLE_REF="pre_last_node" ID="ID_1969716923" CREATED="1428498603543" MODIFIED="1428498722366"/>
<node TEXT="When you execute an operation that reads or writes, it does so at the current position and then moves to the next position" STYLE_REF="pre_last_node" ID="ID_701636496" CREATED="1428498622523" MODIFIED="1428498722363"/>
<node TEXT="If you read one byte, the position moves forward by one" STYLE_REF="pre_last_node" ID="ID_110219908" CREATED="1428498640157" MODIFIED="1428498722365"/>
<node TEXT="var bytes:ByteArray = new ByteArray(); //Position is 0 bytes.writeUnsignedInt(10); //After write method, position is 4&#xa;bytes.writeBoolean(true); //pos is 5&#xa;bytes.writeUnsignedInt( 26 ); //pos is 9&#xa;bytes.position = 0; //Position is 0&#xa;trace( bytes.readUnsignedInt() ); //Output 10, After read method, position is 4&#xa;trace( bytes.readBoolean() ); //Output true, pos is 5&#xa;trace( bytes.readUnsignedInt() ); //Output 26, pos is 9" STYLE_REF="example_node" ID="ID_1815238296" CREATED="1428498730832" MODIFIED="1428498815211"/>
</node>
<node TEXT="Looping" STYLE_REF="yellow_node" ID="ID_791851427" CREATED="1428498823992" MODIFIED="1428498884612">
<node TEXT="bytesAvailable property always gives you the number&#xa;of bytes from the current position to the end" STYLE_REF="pre_last_node" ID="ID_1463438656" CREATED="1428498876063" MODIFIED="1428498891868"/>
<node TEXT="bytes.position = 0;&#xa;while ( bytes.bytesAvailable ) {&#xa;    trace( bytes.readByte() );&#xa;}" STYLE_REF="example_node" ID="ID_1187208810" CREATED="1428498854153" MODIFIED="1428498887827"/>
</node>
<node TEXT="Compressing" STYLE_REF="yellow_node" ID="ID_1980211652" CREATED="1428498965895" MODIFIED="1428499054154">
<node TEXT="Algorithms" STYLE_REF="gray_node" ID="ID_1394282720" CREATED="1428498997078" MODIFIED="1428499062577">
<font BOLD="false"/>
<node TEXT="deflate" STYLE_REF="pre_last_node" ID="ID_1931520820" CREATED="1428499005576" MODIFIED="1428499067452">
<node TEXT="deflate();" ID="ID_1208351184" CREATED="1428499019928" MODIFIED="1428499022362"/>
<node TEXT="inflate();" ID="ID_206201102" CREATED="1428499023136" MODIFIED="1428499026603"/>
</node>
<node TEXT="zlib" STYLE_REF="pre_last_node" ID="ID_323207531" CREATED="1428499011729" MODIFIED="1428499067452">
<node TEXT="compress();" ID="ID_1970837559" CREATED="1428499031885" MODIFIED="1428499047429"/>
<node TEXT="uncompress();" ID="ID_98000001" CREATED="1428499038762" MODIFIED="1428499051115"/>
</node>
</node>
<node TEXT="var bytes:ByteArray = new ByteArray();&#xa;bytes.writeUnsignedInt( 10 ); bytes.writeUnsignedInt( 26 ); bytes.writeUnsignedInt( 11 ); bytes.writeUnsignedInt( 12 ); bytes.writeUnsignedInt( 11 ); bytes.writeUnsignedInt( 23 ); bytes.writeUnsignedInt( 01 ); bytes.writeUnsignedInt( 16 ); bytes.writeUnsignedInt( 02 ); bytes.writeUnsignedInt( 17 ); bytes.writeUnsignedInt( 03 ); bytes.writeUnsignedInt( 16 );&#xa;trace( bytes.length ); //48&#xa;bytes.deflate();&#xa;trace( bytes.length ); //32&#xa;bytes.inflate();&#xa;trace( bytes.length ); //48" STYLE_REF="example_node" ID="ID_371941897" CREATED="1428499082028" MODIFIED="1428499122579"/>
</node>
<node TEXT="Object Serialization" STYLE_REF="yellow_node" ID="ID_372932855" CREATED="1428499225761" MODIFIED="1428499251396">
<node TEXT="When instances of ActionScript classes like Sprite or TextField are written into a ByteArray, they are stored as a Sprite or TextField (in AMF format)" STYLE_REF="pre_last_node" ID="ID_834717579" CREATED="1428499438180" MODIFIED="1428499683636"/>
<node TEXT="AMF natively supports core ActionScript data types" STYLE_REF="pre_last_node" ID="ID_1735386174" CREATED="1428499294656" MODIFIED="1428499644819"/>
<node TEXT="If you are writing an object of a custom type into a ByteArray, it will be stored as a generic Object . This means that when you read it, it will be of type Object and you will not be able to cast it back to its correct type. To avoid this problem, you must register the class" STYLE_REF="pre_last_node" ID="ID_1001471737" CREATED="1428499415504" MODIFIED="1428499644822"/>
<node TEXT="The object which was written to ByteArray and then&#xa;was read from it, is a copy of original instance" STYLE_REF="pre_last_node" ID="ID_1588985106" CREATED="1428499710416" MODIFIED="1428499797563"/>
<node TEXT="Use" STYLE_REF="yellow_node" ID="ID_1136894215" CREATED="1428499799392" MODIFIED="1428499814162">
<node TEXT="package vo {&#xa;    public class Person {&#xa;    public var firstName:String;&#xa;    public var lastName:String;&#xa;    public var age:Number;&#xa;    public function Person() { }&#xa;    }&#xa;}" STYLE_REF="example_node" ID="ID_683451214" CREATED="1428499468810" MODIFIED="1428499810836"/>
<node TEXT="registerClassAlias( &quot;vo.Person&quot;, vo.Person );" STYLE_REF="example_node" ID="ID_1470258623" CREATED="1428499562643" MODIFIED="1428499810839"/>
<node TEXT="var bytes:ByteArray = new ByteArray();&#xa;var person:Person = new Person();&#xa;person.firstName = &quot;Lee&quot;;&#xa;person.lastName = &quot;Alan&quot;;&#xa;person.age = 37;&#xa;bytes.writeObject( person );" STYLE_REF="example_node" ID="ID_91687550" CREATED="1428499581879" MODIFIED="1428499810841"/>
<node TEXT="bytes.position = 0;&#xa;var person1:Person = bytes.readObject();&#xa;trace( person1 is Person ); //true&#xa;trace( person1.firstName ); //Lee&#xa;trace( person === person1 ); //false" STYLE_REF="example_node" ID="ID_1178865388" CREATED="1428499624809" MODIFIED="1428499810843"/>
</node>
</node>
</node>
</node>
<node TEXT="Loaders" STYLE_REF="second_node" POSITION="right" ID="ID_519668594" CREATED="1428488900459" MODIFIED="1436777180823">
<edge COLOR="#007c00"/>
<hook URI="../../../../../D:/Documents/MindMaps/Images/loader-1.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="The Loader class is a subclass of the DisplayObjectContainer class" STYLE_REF="pre_last_node" ID="ID_65299331" CREATED="1428489094851" MODIFIED="1436777180823"/>
<node TEXT="Display assets" STYLE_REF="orange_node" ID="ID_806541743" CREATED="1428488917635" MODIFIED="1428491386028">
<node TEXT="A SWF file" STYLE_REF="yellow_node" ID="ID_858724605" CREATED="1428488932026" MODIFIED="1428489000784"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Authored in ActionScript 3.0
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="An image file" STYLE_REF="yellow_node" ID="ID_1658914220" CREATED="1428488939451" MODIFIED="1428491386028">
<node TEXT="JPG" STYLE_REF="pre_last_node" ID="ID_1114998689" CREATED="1428488945536" MODIFIED="1428489011160"/>
<node TEXT="PNG" STYLE_REF="pre_last_node" ID="ID_1931978507" CREATED="1428488947841" MODIFIED="1428489011159"/>
<node TEXT="GIF" STYLE_REF="pre_last_node" ID="ID_1846733340" CREATED="1428488950601" MODIFIED="1428489011161"/>
</node>
<node TEXT="An AVM1 SWF file" STYLE_REF="yellow_node" ID="ID_1160758031" CREATED="1428488959427" MODIFIED="1428489000784"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      This is a SWF file written in ActionScript 1.0 or 2.0
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Use" STYLE_REF="orange_node" ID="ID_1090808153" CREATED="1428489075146" MODIFIED="1428490666945">
<node TEXT="var pictLdr:Loader = new Loader();&#xa;var pictURL:String = &quot;banana.jpg&quot;&#xa;var pictURLReq:URLRequest = new URLRequest(pictURL);&#xa;pictLdr.load(pictURLReq);&#xa;this.addChild(pictLdr);" STYLE_REF="example_node" ID="ID_1462724572" CREATED="1428489078187" MODIFIED="1428490666944"/>
</node>
<node TEXT=" Monitoring loading progress" STYLE_REF="orange_node" ID="ID_1899096500" CREATED="1428490113884" MODIFIED="1428490183765">
<node TEXT="LoaderInfo" STYLE_REF="gray_node" ID="ID_577891554" CREATED="1428490340802" MODIFIED="1428490568014">
<node TEXT="Once the file has started loading, a LoaderInfo object is created" STYLE_REF="pre_last_node" ID="ID_1697998882" CREATED="1428490220627" MODIFIED="1428490578469"/>
<node TEXT="Provides information" STYLE_REF="yellow_node" ID="ID_650331240" CREATED="1428490236497" MODIFIED="1428490574991">
<node TEXT="Load progress" STYLE_REF="pre_last_node" ID="ID_1770690149" CREATED="1428490244946" MODIFIED="1428490587449"/>
<node TEXT="URLs of the loader" STYLE_REF="pre_last_node" ID="ID_1736407072" CREATED="1428490256355" MODIFIED="1428490587449"/>
<node TEXT="The number of bytes total for the media" STYLE_REF="pre_last_node" ID="ID_956518395" CREATED="1428490270980" MODIFIED="1428490587449"/>
<node TEXT="Dispatches events for monitoring the progress of the load" STYLE_REF="pre_last_node" ID="ID_1994753863" CREATED="1428490318978" MODIFIED="1428490587448"/>
</node>
<node TEXT="Can be accessed through" STYLE_REF="yellow_node" ID="ID_1958471587" CREATED="1428490501035" MODIFIED="1428490574990">
<node TEXT="loader.contentLoaderInfo" STYLE_REF="example_node" ID="ID_1084406520" CREATED="1428490422355" MODIFIED="1428490583199"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      When loading begins
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="displayObj.loaderInfo" STYLE_REF="example_node" ID="ID_1622809861" CREATED="1428490511889" MODIFIED="1428490583199"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      When display object has finished loading
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="Use" STYLE_REF="gray_node" ID="ID_624692185" CREATED="1428490617123" MODIFIED="1428490629326">
<node TEXT="var ldr:Loader = new Loader();&#xa;var urlReq:URLRequest = new URLRequest(&quot;Circle.swf&quot;);&#xa;ldr.load(urlReq);&#xa;ldr.contentLoaderInfo.addEventListener(Event.COMPLETE, loaded);&#xa;addChild(ldr);&#xa; &#xa;function loaded(event:Event):void {&#xa;    var content:Sprite = event.target.content;&#xa;    content.scaleX = 2;&#xa;}" STYLE_REF="example_node" ID="ID_1074774151" CREATED="1428490619917" MODIFIED="1428490634525"/>
</node>
</node>
<node TEXT="LoaderContext" STYLE_REF="orange_node" ID="ID_1857643062" CREATED="1428490714817" MODIFIED="1428490761726">
<node TEXT="Can be specified through" STYLE_REF="yellow_node" ID="ID_159462485" CREATED="1428490729321" MODIFIED="1428491297861">
<node TEXT="load()" STYLE_REF="pre_last_node" ID="ID_1396512266" CREATED="1428490737553" MODIFIED="1428491274558"/>
<node STYLE_REF="pre_last_node" ID="ID_1666086650" CREATED="1428490743946" MODIFIED="1428491274558"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#160;loadBytes()
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Properties" STYLE_REF="gray_node" ID="ID_656606900" CREATED="1428490777947" MODIFIED="1428491149870">
<node TEXT="checkPolicyFile" STYLE_REF="yellow_node" ID="ID_1336774375" CREATED="1428490786096" MODIFIED="1428491157071">
<node TEXT="Only for image files" STYLE_REF="pre_last_node" ID="ID_928396290" CREATED="1428490849385" MODIFIED="1428491164839"/>
<node TEXT="If true, the Loader checks the origin server for a policy file" STYLE_REF="pre_last_node" ID="ID_1121875395" CREATED="1428490866674" MODIFIED="1428491164840"/>
<node TEXT="If the server grants permission to the Loader domain, ActionScript from SWF files in the Loader domain can access data in the loaded image; in other words, you can use the BitmapData.draw()" STYLE_REF="pre_last_node" ID="ID_888875327" CREATED="1428491359930" MODIFIED="1428491379494"/>
</node>
<node TEXT="securityDomain" STYLE_REF="yellow_node" ID="ID_724248283" CREATED="1428490881250" MODIFIED="1428491157070">
<node TEXT="Only for SWF files" STYLE_REF="pre_last_node" ID="ID_1203578460" CREATED="1428490888537" MODIFIED="1428491164840"/>
<node TEXT="Specify this for a SWF file from a domain other than yours" STYLE_REF="pre_last_node" ID="ID_1603269769" CREATED="1428490977897" MODIFIED="1428491164842"/>
<node TEXT="FP will check for the existence of a policy file, and if one exists, SWF files from the domains permitted in the cross-policy file can cross-script the loaded SWF content" STYLE_REF="pre_last_node" ID="ID_330156309" CREATED="1428490951972" MODIFIED="1428491164843"/>
</node>
<node TEXT="applicationDomain" STYLE_REF="yellow_node" ID="ID_1822182763" CREATED="1428491065055" MODIFIED="1428491157071">
<node TEXT="Use this property only when loading a SWF file written in ActionScript 3.0" STYLE_REF="pre_last_node" ID="ID_1871704686" CREATED="1428491071639" MODIFIED="1428491164844"/>
<node TEXT="When loading the file, you can specify that the file be included in the same application domain as that of the Loader object, by setting the applicationDomain parameter to flash.system.ApplicationDomain.currentDomain" STYLE_REF="pre_last_node" ID="ID_1530677572" CREATED="1428491088633" MODIFIED="1428491164845">
<node TEXT="In this case you can access its classes directly" ID="ID_787365234" CREATED="1428491101427" MODIFIED="1428491106661"/>
</node>
</node>
</node>
<node TEXT="Use" STYLE_REF="gray_node" ID="ID_1803072528" CREATED="1428491230816" MODIFIED="1428491248797">
<node TEXT="var context:LoaderContext = new LoaderContext();&#xa;context.checkPolicyFile = true;&#xa;var urlReq:URLRequest = new URLRequest(&quot;http://www.[your_domain_here].com/photo11.jpg&quot;);&#xa;var ldr:Loader = new Loader();&#xa;ldr.load(urlReq, context);" STYLE_REF="example_node" ID="ID_1376281520" CREATED="1428491239484" MODIFIED="1428491269304"/>
<node TEXT="var context:LoaderContext = new LoaderContext();&#xa;context.securityDomain = SecurityDomain.currentDomain;&#xa;context.applicationDomain = ApplicationDomain.currentDomain;&#xa;var urlReq:URLRequest = new URLRequest(&quot;http://www.[your_domain_here].com/library.swf&quot;);&#xa;var ldr:Loader = new Loader();&#xa;ldr.load(urlReq, context);" STYLE_REF="example_node" ID="ID_999699817" CREATED="1428491233219" MODIFIED="1428491269302"/>
</node>
</node>
</node>
<node TEXT="Garbage collector" STYLE_REF="second_node" POSITION="left" ID="ID_543108447" CREATED="1428501553628" MODIFIED="1428501663186">
<edge COLOR="#007c7c"/>
<hook URI="../../../../../D:/Documents/MindMaps/Images/gc-1.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Memory allocation" STYLE_REF="orange_node" ID="ID_637159158" CREATED="1428502850428" MODIFIED="1436777939329">
<node TEXT="Flash Player uses a page allocator (GCHeap) to allocate large blocks (megabytes) of memory from the OS. The GCHeap then breaks the large block into smaller 4K pages and gives the pages to the garbage collection (GC) memory manager as needed" STYLE_REF="pre_last_node" ID="ID_1038049456" CREATED="1428503202925" MODIFIED="1428503243570">
<hook URI="../../../../../D:/Documents/MindMaps/Programming/ActionScript/gc-heap-1.jpg" SIZE="1.0" NAME="ExternalObject"/>
</node>
<node TEXT="The GC then uses those 4K pages to provide memory for objects up to 2K in size in the system" STYLE_REF="pre_last_node" ID="ID_1493114876" CREATED="1428503346167" MODIFIED="1436777939327">
<hook URI="../../../../../D:/Documents/MindMaps/Programming/ActionScript/gc-2.jpg" SIZE="1.0" NAME="ExternalObject"/>
</node>
<node TEXT="For objects larger than 2K (bitmaps, videos, files, etc.), GCHeap provides groups of contiguous 4K blocks to a large memory allocator" STYLE_REF="pre_last_node" ID="ID_175917390" CREATED="1428503427133" MODIFIED="1428503789647"/>
<node TEXT="When almost all of the 4K pages in a large chunk are allocated, Flash Player runs garbage collection to reclaim unused memory before the GCHeap attempts to allocate more memory from the OS" STYLE_REF="pre_last_node" ID="ID_570810816" CREATED="1428503446811" MODIFIED="1428503789647"/>
</node>
<node TEXT="Heap and stack" STYLE_REF="orange_node" ID="ID_1792935612" CREATED="1428503499118" MODIFIED="1428503507433">
<node TEXT="The heap is the memory allocated for any object created or initialized at runtime. Objects on the heap exist until they are garbage collected" STYLE_REF="pre_last_node" ID="ID_916752225" CREATED="1428503524932" MODIFIED="1428503789645">
<hook URI="../../../../../D:/Documents/MindMaps/Programming/ActionScript/gc-3.jpg" SIZE="1.0" NAME="ExternalObject"/>
</node>
<node TEXT="The stack is memory that stores all variables that are defined at compile time. Stack memory is used and reused in a sequential manner. Push adds something to the top of the stack. Pop removes something from the top of the stack. The only way to access something in the middle of the stack is to remove all of the things above it." STYLE_REF="pre_last_node" ID="ID_1049190961" CREATED="1428503561244" MODIFIED="1428503789643"/>
<node TEXT="Local method variables, arguments, and information about where to return when a method is complete are pushed onto the stack as the methods run. Changes to the stack occur very quickly. Stack references to objects tend to be very temporary. Those references to objects may exist on the stack, but the memory allocated to those objects comes from the heap." STYLE_REF="pre_last_node" ID="ID_1871746909" CREATED="1428503731995" MODIFIED="1428503789642">
<hook URI="../../../../../D:/Documents/MindMaps/Programming/ActionScript/gc-4.jpg" SIZE="1.0" NAME="ExternalObject"/>
</node>
</node>
<node TEXT="GC implementation" STYLE_REF="orange_node" ID="ID_1640000129" CREATED="1428503767259" MODIFIED="1428503883858"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      FP uses combination of deferred reference
    </p>
    <p>
      counting and conservative mark-and-sweep
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Deferred reference counting" STYLE_REF="gray_node" ID="ID_722980055" CREATED="1428503876587" MODIFIED="1428503891162">
<node TEXT="Reference counting is not performed on stack references" STYLE_REF="pre_last_node" ID="ID_1275232717" CREATED="1428504044742" MODIFIED="1428504444573"/>
<node TEXT="Each object on the heap keeps track of the number of things pointing to it" STYLE_REF="pre_last_node" ID="ID_1017669967" CREATED="1428503953637" MODIFIED="1428504444576"/>
<node TEXT="If the object has a zero reference count (nothing is pointing to it), it is added to the Zero Count Table (ZCT). When the ZCT is full, the stack is scanned to find any references from the stack to an object on the ZCT. Any object on the ZCT without a stack reference is deleted." STYLE_REF="pre_last_node" ID="ID_1472011601" CREATED="1428503994814" MODIFIED="1428504444570"/>
<node TEXT="One of the problems of deferred reference counting is circular references. If ObjectA and ObjectB refer to each other but no other objects in the system point to them, they will never have a zero reference count and will therefore never be eligible for garbage collection using reference counting. This is where mark and sweep garbage collection helps." STYLE_REF="pre_last_node" ID="ID_761206774" CREATED="1428504101248" MODIFIED="1428504444578">
<hook URI="../../../../../D:/Documents/MindMaps/Programming/ActionScript/gc-5.jpg" SIZE="1.0" NAME="ExternalObject"/>
</node>
</node>
<node TEXT="Mark/Sweep" STYLE_REF="gray_node" ID="ID_1490220056" CREATED="1428504134428" MODIFIED="1428504142713">
<node TEXT="GCRoots" STYLE_REF="yellow_node" ID="ID_1561308577" CREATED="1428504207952" MODIFIED="1428504282241">
<node TEXT="Applications that run in FP have multiple GCRoots" STYLE_REF="pre_last_node" ID="ID_73895003" CREATED="1428504203919" MODIFIED="1428504287916"/>
<node TEXT="GCRoots are never garbage collected" STYLE_REF="pre_last_node" ID="ID_1075082538" CREATED="1428504273084" MODIFIED="1428504287916"/>
<node TEXT="GCRoots are" STYLE_REF="pre_last_node" ID="ID_407865099" CREATED="1428504237815" MODIFIED="1428504287915">
<node TEXT="Stages" ID="ID_1809881290" CREATED="1428504228092" MODIFIED="1428504230944"/>
<node TEXT="Loaders" ID="ID_673361008" CREATED="1428504231694" MODIFIED="1428504234729"/>
</node>
</node>
<node TEXT="Mark phase" STYLE_REF="yellow_node" ID="ID_908552243" CREATED="1428504373261" MODIFIED="1428504390235">
<node TEXT="Every object in an application has a &quot;mark bit.&quot; When the Mark phase of garbage collection begins, all of those mark bits are cleared. he garbage collector starts from those roots, traces through each object and sets the mark bit for every object it reaches. Any object that is no longer reachable from any of the roots is no longer reachable from anywhere in the application &#x2013; its mark bit does not get set during the Mark phase." STYLE_REF="pre_last_node" ID="ID_986931171" CREATED="1428504323615" MODIFIED="1428504397120"/>
</node>
<node TEXT="Sweep phase" STYLE_REF="yellow_node" ID="ID_823882880" CREATED="1428504378417" MODIFIED="1428504390235">
<node TEXT="Once the collector is done marking all of the objects it finds, the Sweep phase begins. Any object that doesn&apos;t have a set mark bit is destroyed and its memory reclaimed." STYLE_REF="pre_last_node" ID="ID_1155933269" CREATED="1428504370254" MODIFIED="1428504397123"/>
</node>
<node TEXT="" ID="ID_1247811721" CREATED="1428504421347" MODIFIED="1428504425383">
<hook URI="../../../../../D:/Documents/MindMaps/Programming/ActionScript/gc-6.jpg" SIZE="1.0" NAME="ExternalObject"/>
</node>
</node>
<node TEXT="Conservative collection" STYLE_REF="gray_node" ID="ID_14887625" CREATED="1428504560708" MODIFIED="1428504612745">
<node TEXT="The MMgc can&apos;t tell if certain values in memory are object pointers (memory addresses) or a numeric value. In order to prevent accidentally collecting objects the values may point to, the MMgc assumes that every value could be a pointer. Therefore, some objects that are not really being pointed to will never be collected and will be considered a memory leak." STYLE_REF="pre_last_node" ID="ID_40329982" CREATED="1428504598973" MODIFIED="1428504615585"/>
</node>
<node TEXT="Incremental collection" STYLE_REF="gray_node" ID="ID_1688295391" CREATED="1428504719237" MODIFIED="1428505594801">
<node TEXT="The Mark phase is the most time intensive part of the garbage collection process. Due to this fact, the marking process has been incrementalized using a work queue and a tri-color algorithm. The queue maintains the marking state between marking increments." STYLE_REF="pre_last_node" ID="ID_1110826077" CREATED="1428504765660" MODIFIED="1428504962723"/>
<node TEXT="Tri-color algorithm" STYLE_REF="yellow_node" ID="ID_1887755273" CREATED="1428504776452" MODIFIED="1428505594800">
<node TEXT="Black objects have been marked and are no longer in the queue" STYLE_REF="pre_last_node" ID="ID_61312459" CREATED="1428504781631" MODIFIED="1428504962722"/>
<node TEXT="Gray objects are in the queue and have not yet been marked" STYLE_REF="pre_last_node" ID="ID_868142923" CREATED="1428504787102" MODIFIED="1428504962722"/>
<node TEXT="White objects are neither marked nor in the queue" STYLE_REF="pre_last_node" ID="ID_1765495019" CREATED="1428504795911" MODIFIED="1428504962722"/>
</node>
<node TEXT="Process" STYLE_REF="yellow_node" ID="ID_682558075" CREATED="1428504903518" MODIFIED="1428504957115">
<node TEXT="" ID="ID_761984974" CREATED="1428504908686" MODIFIED="1428504918141">
<hook URI="../../../../../D:/Documents/MindMaps/Programming/ActionScript/gc-color-1.jpg" SIZE="1.0" NAME="ExternalObject"/>
</node>
<node TEXT="" ID="ID_1005705804" CREATED="1428504921094" MODIFIED="1428504925321">
<hook URI="../../../../../D:/Documents/MindMaps/Programming/ActionScript/gc-color-2.jpg" SIZE="1.0" NAME="ExternalObject"/>
</node>
<node TEXT="" ID="ID_566980074" CREATED="1428504926692" MODIFIED="1428504930252">
<hook URI="../../../../../D:/Documents/MindMaps/Programming/ActionScript/gc-color-3.jpg" SIZE="1.0" NAME="ExternalObject"/>
</node>
<node TEXT="" ID="ID_646354538" CREATED="1428504931699" MODIFIED="1428504940757">
<hook URI="../../../../../D:/Documents/MindMaps/Programming/ActionScript/gc-color-4.jpg" SIZE="1.0" NAME="ExternalObject"/>
</node>
</node>
<node TEXT="By using the work queue and the tri-color algorithm, the Mark phase can be started and stopped to help prevent large, unwanted garbage collection pauses." STYLE_REF="pre_last_node" ID="ID_1743198634" CREATED="1428504856046" MODIFIED="1428504962721"/>
</node>
<node TEXT="Imminence" STYLE_REF="gray_node" ID="ID_363009070" CREATED="1428505134373" MODIFIED="1428505141096">
<node TEXT="public static function pauseForGCIfCollectionImminent(imminence:Number = 0.75):void" STYLE_REF="example_node" ID="ID_681768607" CREATED="1428505506302" MODIFIED="1428505522441"/>
<node TEXT="System.pauseForGCIfCollectionImminent(0.25);" STYLE_REF="example_node" ID="ID_21582721" CREATED="1428505165837" MODIFIED="1428505516857"/>
<node TEXT="To give an advise garbage collector that it&apos;s a good time&#xa;to both complete the marking and perform collection" STYLE_REF="pre_last_node" ID="ID_988414338" CREATED="1428505211064" MODIFIED="1428505555396"/>
<node TEXT="The garbage collector must be at least 25% of the way through the process before recognizing this request to pause for collection" STYLE_REF="pre_last_node" ID="ID_206451504" CREATED="1428505349133" MODIFIED="1428505555395"/>
<node TEXT="Value" STYLE_REF="yellow_node" ID="ID_1733666803" CREATED="1428505529499" MODIFIED="1428505548448">
<node TEXT="Passing a small value (although greater than .25) will most likely&#xa;force collection and cause the application to pause" STYLE_REF="pre_last_node" ID="ID_489301330" CREATED="1428505403614" MODIFIED="1428505570225"/>
<node TEXT="Passing a larger value tells the garbage collector to finish&#xa;collection only if it was going to happen soon anyway" STYLE_REF="pre_last_node" ID="ID_1576869987" CREATED="1428505450490" MODIFIED="1428505583625"/>
</node>
<node TEXT="When use" STYLE_REF="yellow_node" ID="ID_422059956" CREATED="1428505459445" MODIFIED="1428505469514">
<node TEXT="For example, a game might call this function&#xa;upon the completion of a level in a game" STYLE_REF="pre_last_node" ID="ID_629517728" CREATED="1428505266767" MODIFIED="1428505474216"/>
</node>
</node>
<node TEXT="System.gc()" STYLE_REF="gray_node" ID="ID_1041792111" CREATED="1428505701093" MODIFIED="1428505737416">
<node TEXT="The System.gc() function can force the runtime to garbage collect, however it doesn&#x2019;t always have 100% predictable results across all platforms, and forcing garbage collection on a regular basis is usually overkill" STYLE_REF="pre_last_node" ID="ID_480784208" CREATED="1428505698213" MODIFIED="1428505737416"/>
</node>
</node>
</node>
</node>
</map>
