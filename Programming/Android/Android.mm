<map version="freeplane 1.3.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Android" STYLE_REF="main_node" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1425649974289">
<hook NAME="AutomaticEdgeColor" COUNTER="4"/>
<hook URI="../../Images/android-large.jpg" SIZE="1.0" NAME="ExternalObject"/>
<hook NAME="MapStyle" zoom="1.003">
    <properties show_icon_for_attributes="true" show_note_icons="true"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
<stylenode TEXT="second_node" BACKGROUND_COLOR="#00cccc" STYLE="bubble">
<font SIZE="14"/>
</stylenode>
<stylenode TEXT="pre_last_node" BACKGROUND_COLOR="#99ff99" STYLE="bubble">
<edge STYLE="bezier" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="yellow_node" BACKGROUND_COLOR="#ffff66" STYLE="bubble">
<edge WIDTH="1"/>
</stylenode>
<stylenode TEXT="orange_node" BACKGROUND_COLOR="#ff9900" STYLE="bubble">
<font BOLD="true"/>
</stylenode>
<stylenode TEXT="last_node" BACKGROUND_COLOR="#ffffff">
<edge STYLE="bezier" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="main_node" BACKGROUND_COLOR="#ccccff">
<font SIZE="18" BOLD="true"/>
<edge STYLE="sharp_bezier" WIDTH="4"/>
</stylenode>
<stylenode TEXT="gray_node" BACKGROUND_COLOR="#9ec19e">
<font BOLD="true"/>
</stylenode>
<stylenode TEXT="example_node" BACKGROUND_COLOR="#afcdfa">
<edge STYLE="bezier" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="main_definition" BACKGROUND_COLOR="#ffccff">
<edge STYLE="bezier" WIDTH="thin"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<node TEXT="Solutions" STYLE_REF="second_node" POSITION="right" ID="ID_222666650" CREATED="1425649324143" MODIFIED="1425649466827">
<edge COLOR="#a10c0c"/>
<hook URI="../../Images/solution.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Set padding to List in PreferenceFragment" STYLE_REF="yellow_node" ID="ID_1219413923" CREATED="1425649549287" MODIFIED="1448285345925">
<node TEXT="@Override&#xa;public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {&#xa;    View v = super.onCreateView(inflater, container, savedInstanceState);&#xa;    if(v != null) {&#xa;        ListView lv = (ListView) v.findViewById(android.R.id.list);&#xa;        lv.setPadding(10, 10, 10, 10);&#xa;    }&#xa;    return v;&#xa;}" STYLE_REF="example_node" ID="ID_1176477073" CREATED="1425649557487" MODIFIED="1448285345918"/>
<node TEXT="@Override&#xa;public onResume:&#xa;    int paddingSize = 0;&#xa;    if (Build.VERSION.SDK_INT &lt; 14) {&#xa;        paddingSize = (int) (-32 * CLS_Gfx.scale);&#xa;    } else {&#xa;        paddingSize = (int) (-16 * CLS_Gfx.scale);&#xa;    }&#xa;    final View v = getView();&#xa;    v.setPadding(paddingSize, 0, paddingSize, 0);" STYLE_REF="example_node" ID="ID_842859110" CREATED="1425649583222" MODIFIED="1425649597571"/>
</node>
<node TEXT="Run some code only once" STYLE_REF="yellow_node" ID="ID_594023661" CREATED="1425649614039" MODIFIED="1425649627028">
<node TEXT="public class App extends Application {&#xa;  @Override&#xa;  public void onCreate() {&#xa;    super.onCreate();&#xa;    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);&#xa;    if (!prefs.getBoolean(&quot;firstTime&quot;, false)) {&#xa;      // run your one time code here&#xa;      databaseSetup();&#xa;&#xa;      // mark first time has runned.&#xa;      SharedPreferences.Editor editor = prefs.edit();&#xa;      editor.putBoolean(&quot;firstTime&quot;, true);&#xa;      editor.commit();&#xa;    }&#xa;}" STYLE_REF="example_node" ID="ID_1508930691" CREATED="1425649621503" MODIFIED="1425649629516"/>
</node>
<node TEXT="SQLite row count" STYLE_REF="yellow_node" ID="ID_1984510439" CREATED="1425649641369" MODIFIED="1448285342996">
<node TEXT="private static final String DB_TABLE_PLACES = &quot;my_table&quot;;&#xa;private SQLiteDatabase mDatabase;&#xa;&#xa;private long getRowCount() {&#xa;    String sql = &quot;SELECT COUNT(*) FROM &quot; + DB_TABLE_PLACES;&#xa;    SQLiteStatement statement = mDatabase.compileStatement(sql);&#xa;    long count = statement.simpleQueryForLong();&#xa;    return count;&#xa;}" STYLE_REF="example_node" ID="ID_1066178606" CREATED="1425649656295" MODIFIED="1448285342992"/>
</node>
<node TEXT="Avoid restart Activity when screen&#xa;orientation has changed" STYLE_REF="yellow_node" ID="ID_755854450" CREATED="1425650021399" MODIFIED="1436425355083">
<node TEXT="Add to manifest file, to &lt;activity&gt; tag" STYLE_REF="pre_last_node" ID="ID_1909647053" CREATED="1425650035095" MODIFIED="1425650095398">
<node TEXT="android:configChanges=&quot;orientation|screenSize&quot;" STYLE_REF="example_node" ID="ID_351159989" CREATED="1425650043639" MODIFIED="1425650105821"/>
</node>
<node TEXT="And override callback in activity" STYLE_REF="pre_last_node" ID="ID_1770776384" CREATED="1425650050791" MODIFIED="1436425355083">
<node TEXT="@Override&#xa;public void onConfigurationChanged(Configuration newConfig) {&#xa;super.onConfigurationChanged(newConfig);&#xa;    // This overrides default action&#xa;}" STYLE_REF="example_node" ID="ID_802312471" CREATED="1425650059087" MODIFIED="1425650105823"/>
</node>
<node TEXT="To invalidate ViewPager, in this case&#xa;viewPager will recreate all pages" STYLE_REF="pre_last_node" ID="ID_1249725087" CREATED="1425650070087" MODIFIED="1425650100043">
<node TEXT="mViewPager.setAdapter(mPagerAdapter);&#xa;mViewPager.setCurrentItem(mPagerAdapter.getCurrPosition());" STYLE_REF="example_node" ID="ID_8415055" CREATED="1425650079567" MODIFIED="1425650105827"/>
</node>
</node>
<node TEXT="How to create round button" STYLE_REF="yellow_node" ID="ID_845798376" CREATED="1425650124927" MODIFIED="1430818840243">
<node TEXT="&lt;?xml version=&quot;1.0&quot; encoding=&quot;utf-8&quot;?&gt;&#xa;&lt;selector xmlns:android=&quot;http://schemas.android.com/apk/res/android&quot;&gt;&#xa;    &lt;item android:state_pressed=&quot;true&quot;&gt;&#xa;        &lt;shape xmlns:android=&quot;http://schemas.android.com/apk/res/android&quot; android:shape=&quot;oval&quot;&gt;&#xa;            &lt;solid android:color=&quot;@color/accent_dark&quot;/&gt;&#xa;            &lt;stroke android:width=&quot;1dp&quot; android:color=&quot;@color/darkish_btn_stroke&quot; /&gt;&#xa;        &lt;/shape&gt;&#xa;    &lt;/item&gt;&#xa;    &lt;item&gt;&#xa;        &lt;shape xmlns:android=&quot;http://schemas.android.com/apk/res/android&quot; android:shape=&quot;oval&quot;&gt;&#xa;            &lt;solid android:color=&quot;@color/accent&quot;/&gt;&#xa;            &lt;stroke android:width=&quot;1dp&quot; android:color=&quot;@color/darkish_btn_stroke&quot; /&gt;&#xa;        &lt;/shape&gt;&#xa;    &lt;/item&gt;&#xa;&lt;/selector&gt;" STYLE_REF="example_node" ID="ID_43539786" CREATED="1425650129719" MODIFIED="1430818840241"/>
</node>
<node TEXT="How to create timer" STYLE_REF="yellow_node" ID="ID_842003317" CREATED="1425650157903" MODIFIED="1425650367627">
<node TEXT="Java ways" STYLE_REF="gray_node" ID="ID_1747746419" CREATED="1425650166391" MODIFIED="1425650247269">
<node TEXT="import java.util.Timer;&#xa;&#xa;Timertimer = new Timer();&#xa;&#xa;// To run once after 2 minutes&#xa;timer.schedule(new TimerTask() {&#xa;  @Override&#xa;  public void run() {&#xa;    // Your code here&#xa;  }&#xa;}, 2*60*1000);&#xa;&#xa;// Repeat every 2 minutes&#xa;timer.scheduleAtFixedRate(new TimerTask() {&#xa;  @Override&#xa;  public void run() {&#xa;    // Your code here&#xa;  }&#xa;}, 2*60*1000, 2*60*1000);" STYLE_REF="example_node" ID="ID_1152305961" CREATED="1425650191023" MODIFIED="1425650255229"/>
<node TEXT="ExecutorService service = Executors.newSingleThreadExecutor();&#xa;try {&#xa;    Runnable r = new Runnable() {&#xa;        @Override&#xa;        public void run() {&#xa;            // Your code here&#xa;        }&#xa;    };&#xa;    Future&lt;?&gt; f = service.submit(r);&#xa;    f.get(2, TimeUnit.MINUTES);     // attempt the task for two minutes&#xa;}&#xa;catch (final InterruptedException e) {&#xa;    // The thread was interrupted during sleep, wait or join&#xa;}&#xa;catch (final TimeoutException e) {&#xa;    // Took too long!&#xa;}&#xa;catch (final ExecutionException e) {&#xa;    // An exception from within the Runnable task&#xa;}&#xa;finally {&#xa;    service.shutdown();&#xa;}" STYLE_REF="example_node" ID="ID_1530651194" CREATED="1425650202209" MODIFIED="1425650255220"/>
<node TEXT="FutureTask&lt;Void&gt; task = new FutureTask&lt;Void&gt;(new Callable&lt;Void&gt;() {&#xa;  @Override&#xa;  public Void call() throws Exception {&#xa;    // Do DB stuff&#xa;    return null;&#xa;  }&#xa;});&#xa;Executor executor = Executors.newSingleThreadScheduledExecutor();&#xa;executor.execute(task);&#xa;try {&#xa;  task.get(5, TimeUnit.SECONDS);&#xa;}&#xa;catch(Exception ex) {&#xa;  // Handle your exception&#xa;}" STYLE_REF="example_node" ID="ID_81503831" CREATED="1425650219527" MODIFIED="1425650255223"/>
</node>
<node TEXT="Android ways" STYLE_REF="gray_node" ID="ID_1426620643" CREATED="1425650177399" MODIFIED="1425650247271">
<node TEXT="Handler timerHandler = new Handler();&#xa;Runnable timerRunnable = new Runnable() {&#xa;        @Override&#xa;        public void run() {&#xa;          // Your code here          &#xa;            timerHandler.postDelayed(this, 500);&#xa;        }&#xa;};&#xa;&#xa;// Start immediately&#xa;timerHandler.postDelayed(timerRunnable, 0);&#xa;&#xa;// Stop&#xa;timerHandler.removeCallbacks(timerRunnable);" STYLE_REF="example_node" ID="ID_833858602" CREATED="1425650228231" MODIFIED="1425650255225"/>
<node TEXT="// 30000 all millis, 1000 - interval between them&#xa;new CountDownTimer(30000, 1000) {&#xa;&#xa;     public void onTick(long millisUntilFinished) {&#xa;         mTextField.setText(&quot;seconds remaining: &quot; + millisUntilFinished / 1000);&#xa;     }&#xa;     public void onFinish() {&#xa;         mTextField.setText(&quot;done!&quot;);&#xa;     }&#xa;  }.start();" STYLE_REF="example_node" ID="ID_1391013250" CREATED="1425650237775" MODIFIED="1425650255227"/>
</node>
</node>
<node TEXT="Change language programatically" STYLE_REF="yellow_node" ID="ID_1004882074" CREATED="1430818357279" MODIFIED="1430818826860">
<node TEXT="String languageToLoad  = &quot;uk&quot;;&#xa;Locale locale = new Locale(languageToLoad);&#xa;Locale.setDefault(locale);&#xa;Configuration config = getBaseContext().getResources().getConfiguration();&#xa;config.locale = locale;&#xa;getBaseContext().getResources().updateConfiguration(config,&#xa;    getBaseContext().getResources().getDisplayMetrics());" STYLE_REF="example_node" ID="ID_493070077" CREATED="1430818388291" MODIFIED="1430818826858"/>
</node>
<node TEXT="Update summary in PreferenceActivity/Fragment" STYLE_REF="yellow_node" ID="ID_234831621" CREATED="1430818556819" MODIFIED="1436188415448">
<node TEXT="@Override&#xa;protected void onStart() {&#xa;    super.onStart();&#xa;    if (Build.VERSION.SDK_INT &lt; Build.VERSION_CODES.HONEYCOMB) {&#xa;        mPrefScreen = getPreferenceScreen();&#xa;    } else {&#xa;        mPrefScreen = mPrefsFragment.getPreferenceScreen();&#xa;    }&#xa;&#xa;        // Get a reference to the preferences widgets&#xa;    mLangPref = (ListPreference) mPrefScreen.findPreference(PrefHelper.LANG_PREF);&#xa;    mScreenOnOffPref = (ListPreference)   mPrefScreen.findPreference(PrefHelper.SCREEN_ON_OFF_PREF);&#xa;    mSoundStylePref = (ListPreference) mPrefScreen.findPreference(PrefHelper.SOUND_STYLE_PREF);&#xa;}&#xa;&#xa;@Override&#xa;protected void onResume() {&#xa;    super.onResume();&#xa;&#xa;    setSummaries();&#xa;  mPrefScreen.getSharedPreferences().registerOnSharedPreferenceChangeListener(this);&#xa;}&#xa;&#xa;@Override&#xa;protected void onPause() {&#xa;    super.onPause();&#xa;&#xa;mPrefScreen.getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);&#xa;}&#xa;&#xa;@Override&#xa;public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {&#xa;    Preference pref = mPrefScreen.findPreference(key);&#xa;&#xa;    if (pref instanceof ListPreference) {&#xa;        ListPreference listPref = (ListPreference) pref;&#xa;        pref.setSummary(listPref.getEntry());&#xa;    }&#xa;}&#xa;&#xa;private void setSummaries() {&#xa;    mLangPref.setSummary(mLangPref.getEntry());&#xa;    mScreenOnOffPref.setSummary(mScreenOnOffPref.getEntry().toString());&#xa;    mSoundStylePref.setSummary(mSoundStylePref.getEntry().toString());&#xa;}" STYLE_REF="example_node" ID="ID_1537671126" CREATED="1430818620004" MODIFIED="1436188415446"/>
</node>
<node TEXT="Make TextView Scrollable" STYLE_REF="yellow_node" ID="ID_879202128" CREATED="1430970643888" MODIFIED="1448285349813">
<node TEXT="&lt;TextView ...&#xa;android:maxLines = &quot;AN_INTEGER&quot;&#xa;android:scrollbars = &quot;vertical&quot;/&gt;&#xa;&#xa;yourTextView.setMovementMethod(new ScrollingMovementMethod());" STYLE_REF="example_node" ID="ID_817741095" CREATED="1430970652203" MODIFIED="1448285349811"/>
</node>
<node TEXT="Disable landscape mode on small screens" STYLE_REF="yellow_node" ID="ID_1934132279" CREATED="1436424899183" MODIFIED="1436424940146">
<font BOLD="false"/>
<node TEXT="@Override&#xa;public void onCreate(Bundle savedInstanceState) {&#xa;// Disable changing orientation on small screens&#xa;int screenSize = ResHelper.getResources().getConfiguration().screenLayout &amp;&#xa;                        Configuration.SCREENLAYOUT_SIZE_MASK;&#xa;if (screenSize == Configuration.SCREENLAYOUT_SIZE_SMALL&#xa;    || screenSize == Configuration.SCREENLAYOUT_SIZE_NORMAL) {&#xa;    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);&#xa;}&#xa;&#xa;super.onCreate(savedInstanceState);&#xa;setContentView(R.layout.main);&#xa;}" STYLE_REF="example_node" ID="ID_518971658" CREATED="1436424953006" MODIFIED="1436425096395"/>
</node>
</node>
<node TEXT="Performance" STYLE_REF="second_node" POSITION="left" ID="ID_103699522" CREATED="1432539138843" MODIFIED="1436188367167">
<edge COLOR="#0000ff"/>
<hook URI="../../Images/performance.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="DataBase" STYLE_REF="yellow_node" ID="ID_490415947" CREATED="1432539142672" MODIFIED="1436188450063">
<node TEXT=" long start = System.currentTimeMillis();&#xa;        for (int i = 0; i &lt; 1000; i++) {&#xa;            this.getWritableDatabase();&#xa;        }&#xa;        Log.d(&quot;TIIME&quot;, &quot;tuta &quot; + String.valueOf(System.currentTimeMillis() - start));" STYLE_REF="example_node" ID="ID_1631423917" CREATED="1432539146994" MODIFIED="1436188379599">
<node TEXT="35 mili" ID="ID_74645382" CREATED="1432539151609" MODIFIED="1432539154213"/>
</node>
</node>
</node>
<node TEXT="Styles and Themes" STYLE_REF="second_node" POSITION="right" ID="ID_1747937016" CREATED="1436242411029" MODIFIED="1436242424558">
<edge COLOR="#006600"/>
<node TEXT="A style is a collection of properties that specify the look and format for a View or window" STYLE_REF="main_definition" ID="ID_372470282" CREATED="1436245427785" MODIFIED="1436245462127"/>
<node TEXT="A theme is a style applied to an entire Activity or application, rather than an individual View" STYLE_REF="main_definition" ID="ID_433951702" CREATED="1436245441721" MODIFIED="1436245462126"/>
<node TEXT="Defining Styles" STYLE_REF="orange_node" ID="ID_777233119" CREATED="1436242629583" MODIFIED="1436245488233">
<node TEXT="&lt;?xml version=&quot;1.0&quot; encoding=&quot;utf-8&quot;?&gt;&#xa;&lt;resources&gt;&#xa;    &lt;style name=&quot;CodeFont&quot; parent=&quot;@android:style/TextAppearance.Medium&quot;&gt;&#xa;        &lt;item name=&quot;android:layout_width&quot;&gt;fill_parent&lt;/item&gt;&#xa;        &lt;item name=&quot;android:layout_height&quot;&gt;wrap_content&lt;/item&gt;&#xa;        &lt;item name=&quot;android:textColor&quot;&gt;#00FF00&lt;/item&gt;&#xa;        &lt;item name=&quot;android:typeface&quot;&gt;monospace&lt;/item&gt;&#xa;    &lt;/style&gt;&#xa;&lt;/resources&gt;" STYLE_REF="example_node" ID="ID_940984821" CREATED="1436242637654" MODIFIED="1436245472398"/>
<node TEXT="The parent attribute in the &lt;style&gt; element is optional and specifies the resource ID of another style from which this style should inherit properties." STYLE_REF="pre_last_node" ID="ID_1224541750" CREATED="1436242651645" MODIFIED="1436246035321"/>
</node>
<node TEXT="Inheritance" STYLE_REF="orange_node" ID="ID_1028200533" CREATED="1436242664933" MODIFIED="1436245488233">
<node TEXT="The parent attribute in the &lt;style&gt; element lets you specify a style from which your style should inherit properties." STYLE_REF="pre_last_node" ID="ID_1269067159" CREATED="1436242836629" MODIFIED="1436246035320"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &lt;style name=&quot;GreenText&quot; parent=&quot;@android:style/TextAppearance&quot;&gt;
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&lt;item name=&quot;android:textColor&quot;&gt;#00FF00&lt;/item&gt;
    </p>
    <p>
      &#160;&#160;&#160;&#160;&lt;/style&gt;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="If you want to inherit from styles that you&apos;ve defined yourself, you do not have to use the parent attribute. Instead, just prefix the name of the style you want to inherit to the name of your new style, separated by a period." STYLE_REF="pre_last_node" ID="ID_1698038549" CREATED="1436242764558" MODIFIED="1436246035319"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &lt;style name=&quot;CodeFont.Red&quot;&gt;
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&lt;item name=&quot;android:textColor&quot;&gt;#FF0000&lt;/item&gt;
    </p>
    <p>
      &#160;&#160;&#160;&#160;&lt;/style&gt;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Example" STYLE_REF="orange_node" ID="ID_602521939" CREATED="1436242461582" MODIFIED="1436245488233">
<node TEXT="&lt;TextView&#xa;    android:layout_width=&quot;fill_parent&quot;&#xa;    android:layout_height=&quot;wrap_content&quot;&#xa;    android:textColor=&quot;#00FF00&quot;&#xa;    android:typeface=&quot;monospace&quot;&#xa;    android:text=&quot;@string/hello&quot; /&gt;" STYLE_REF="example_node" ID="ID_131501051" CREATED="1436242464197" MODIFIED="1436245479127"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &lt;TextView
    </p>
    <p>
      &#160;&#160;&#160;&#160;style=&quot;@style/CodeFont&quot;
    </p>
    <p>
      &#160;&#160;&#160;&#160;android:text=&quot;@string/hello&quot; /&gt;
    </p>
  </body>
</html>
</richcontent>
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="80" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_940984821" STARTINCLINATION="654;0;" ENDINCLINATION="654;0;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
</node>
<node TEXT="&lt;EditText&#xa;    android:inputType=&quot;number&quot;&#xa;    ... /&gt;" STYLE_REF="example_node" ID="ID_1946722514" CREATED="1436245296442" MODIFIED="1436245345488"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &lt;EditText
    </p>
    <p>
      &#160;&#160;&#160;&#160;style=&quot;@style/Numbers&quot;
    </p>
    <p>
      &#160;&#160;&#160;&#160;... /&gt;
    </p>
  </body>
</html>
</richcontent>
<node TEXT="&lt;style name=&quot;Numbers&quot;&gt;&#xa;  &lt;item name=&quot;android:inputType&quot;&gt;number&lt;/item&gt;&#xa;  ...&#xa;&lt;/style&gt;" STYLE_REF="example_node" ID="ID_1194786855" CREATED="1436245307930" MODIFIED="1436245345493"/>
</node>
</node>
<node TEXT="Style properties" STYLE_REF="orange_node" ID="ID_970820825" CREATED="1436245256130" MODIFIED="1436245488234">
<node TEXT="The best place to find properties that apply to a specific View is the corresponding class reference, which lists all of the supported XML attributes." STYLE_REF="pre_last_node" ID="ID_1914609710" CREATED="1436245285474" MODIFIED="1436246035310"/>
<node TEXT="If you apply a style to a View that does not support all of the style properties, the View will apply only those properties that are supported and simply ignore the others" STYLE_REF="pre_last_node" ID="ID_1710080286" CREATED="1436245261298" MODIFIED="1436246054390"/>
</node>
<node TEXT="Applying Styles and Themes" STYLE_REF="orange_node" ID="ID_792760308" CREATED="1436245538178" MODIFIED="1436246009231">
<node TEXT="Style" STYLE_REF="yellow_node" ID="ID_1872589486" CREATED="1436245708914" MODIFIED="1436246014927">
<node TEXT="To an individual View, by adding the style attribute to a View element in the XML for your layout" STYLE_REF="pre_last_node" ID="ID_1786897144" CREATED="1436245548929" MODIFIED="1436246035312"/>
<node TEXT="If a style is applied to a ViewGroup, the child View elements will not inherit the style properties&#x2014;only the element to which you directly apply the style will apply its properties" STYLE_REF="pre_last_node" ID="ID_1317428385" CREATED="1436245590081" MODIFIED="1436246035314"/>
</node>
<node TEXT="Theme" STYLE_REF="yellow_node" ID="ID_1096345419" CREATED="1436245705042" MODIFIED="1448285361406">
<node TEXT="To an entire Activity or application, by adding the android:theme attribute to the &lt;activity&gt; or &lt;application&gt; element in the Android manifest." STYLE_REF="pre_last_node" ID="ID_759247831" CREATED="1436245557545" MODIFIED="1436246035314"/>
<node TEXT="To set a theme for all the activities of your application, open the AndroidManifest.xml file and edit the &lt;application&gt; tag to include the android:theme attribute with the style name" STYLE_REF="pre_last_node" ID="ID_821487271" CREATED="1436245662449" MODIFIED="1448285361403"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &lt;application android:theme=&quot;@style/CustomTheme&quot;&gt;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="However, you can apply a style so that it applies to all View elements&#x2014;by applying the style as a theme." STYLE_REF="pre_last_node" ID="ID_1943310127" CREATED="1436245603313" MODIFIED="1436246035317"/>
<node TEXT="Or if you want the background to be transparent, use the Translucent theme:" STYLE_REF="pre_last_node" ID="ID_183208612" CREATED="1436245711953" MODIFIED="1436246035318"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &lt;activity android:theme=&quot;@android:style/Theme.Translucent&quot;&gt;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="Using Platform Styles and Themes" STYLE_REF="orange_node" ID="ID_1134780391" CREATED="1436245957674" MODIFIED="1436425388186">
<node TEXT="For a better reference to the Android styles and themes, see the following source code" STYLE_REF="pre_last_node" ID="ID_1341133180" CREATED="1436245968896" MODIFIED="1436246035318">
<node TEXT="https://android.googlesource.com/platform/frameworks/base/+/refs/heads/master/core/res/res/values/styles.xml" ID="ID_351650412" CREATED="1436245991530" MODIFIED="1436245992221"/>
<node TEXT="https://android.googlesource.com/platform/frameworks/base/+/refs/heads/master/core/res/res/values/themes.xml" ID="ID_819973410" CREATED="1436245999649" MODIFIED="1436246001101"/>
</node>
<node TEXT="These files will help you learn through example. For instance, in the Android themes source code, you&apos;ll find a declaration for &lt;style name=&quot;Theme.Dialog&quot;&gt;. In this definition, you&apos;ll see all of the properties that are used to style dialogs that are used by the Android framework." STYLE_REF="pre_last_node" ID="ID_827487179" CREATED="1436245983976" MODIFIED="1436425388186"/>
</node>
</node>
<node TEXT="Main components" POSITION="left" ID="ID_67967991" CREATED="1441296505114" MODIFIED="1441296525233">
<edge COLOR="#ff00ff"/>
<node TEXT="&#x412;&#x430;&#x436;&#x43d;&#x43e;&#x441;&#x442;&#x44c;" ID="ID_1307377462" CREATED="1441296526137" MODIFIED="1441296531158">
<node TEXT="&#x41f;&#x440;&#x43e;&#x446;&#x435;&#x441;&#x441; &#x43f;&#x435;&#x440;&#x435;&#x434;&#x43d;&#x435;&#x433;&#x43e; &#x43f;&#x43b;&#x430;&#x43d;&#x430;" ID="ID_296338248" CREATED="1441296592776" MODIFIED="1441296600901"/>
<node TEXT="&#x412;&#x438;&#x434;&#x438;&#x43c;&#x44b;&#x439; &#x43f;&#x440;&#x43e;&#x446;&#x435;&#x441;&#x441;" ID="ID_135353363" CREATED="1441296585217" MODIFIED="1441296726554"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1055;&#1086;&#1083;&#1100;&#1079;&#1086;&#1074;&#1072;&#1090;&#1077;&#1083;&#1100; &#1089; &#1085;&#1080;&#1084;&#1080; &#1085;&#1077; &#1074;&#1079;&#1072;&#1080;&#1084;&#1086;&#1076;&#1077;&#1081;&#1089;&#1090;&#1074;&#1091;&#1077;&#1090;, &#1085;&#1086; &#1082;&#1072;&#1082;&#1080;&#1077;&#1090;&#1086; &#1095;&#1072;&#1089;&#1090;&#1080; &#1074;&#1080;&#1076;&#1080;&#1084;&#1099;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&#x421;&#x435;&#x440;&#x432;&#x438;&#x441;&#x43d;&#x44b;&#x439; &#x43f;&#x440;&#x43e;&#x446;&#x435;&#x441;&#x441;" ID="ID_1524892831" CREATED="1441296571553" MODIFIED="1441296693480"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1042; &#1082;&#1086;&#1090;&#1086;&#1088;&#1086;&#1084; &#1079;&#1072;&#1087;&#1091;&#1097;&#1077;&#1085; &#1093;&#1086;&#1090;&#1103;&#1073;&#1099; &#1086;&#1076;&#1080;&#1085; &#1089;&#1077;&#1088;&#1074;&#1080;&#1089;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&#x41f;&#x440;&#x43e;&#x446;&#x435;&#x441;&#x441; &#x437;&#x430;&#x434;&#x43d;&#x435;&#x433;&#x43e; &#x43f;&#x43b;&#x430;&#x43d;&#x430;" ID="ID_1960800273" CREATED="1441296563592" MODIFIED="1441296671505"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1055;&#1088;&#1086;&#1094;&#1077;&#1089;&#1089;&#1080; &#1082;&#1086;&#1090;&#1086;&#1088;&#1099;&#1077; &#1089;&#1086;&#1076;&#1077;&#1088;&#1078;&#1072;&#1090; &#1082;&#1072;&#1082;&#1080;&#1077;&#1090;&#1086; &#1072;&#1082;&#1090;&#1080;&#1074;&#1080;&#1090;&#1080; &#1085;&#1086; &#1086;&#1085;&#1080; &#1085;&#1077; &#1074;&#1080;&#1076;&#1085;&#1099; &#1087;&#1086;&#1083;&#1100;&#1079;&#1086;&#1074;&#1072;&#1090;&#1077;&#1083;&#1102;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&#x41f;&#x443;&#x441;&#x442;&#x43e;&#x439; &#x43f;&#x440;&#x43e;&#x446;&#x435;&#x441;&#x441;" ID="ID_1636428330" CREATED="1441296532128" MODIFIED="1441296630273"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#1055;&#1088;&#1086;&#1094;&#1077;&#1089;&#1089;&#1080; &#1082;&#1086;&#1090;&#1086;&#1088;&#1099;&#1077; &#1085;&#1077; &#1089;&#1086;&#1076;&#1077;&#1088;&#1078;&#1072;&#1090; &#1085;&#1077; &#1086;&#1076;&#1085;&#1086;&#1075;&#1086; &#1082;&#1086;&#1084;&#1087;&#1086;&#1085;&#1077;&#1085;&#1090;&#1072;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
</node>
</map>
